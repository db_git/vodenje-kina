%%%%% funkcionalnost web aplikacije
\section{FUNKCIONALNOST \textit{WEB} APLIKACIJE}

Kako bi se mogla razviti i implementirati \textit{web} aplikacija se mora razdvojiti na pojedine dijelove.
Tako se aplikacija može brže i jednostavnije razviti jer su slojevi neovisni, a kada dođe novi poslovni zahtjev promjene su minimalne.
Raspodjela uvelike ovisi o odabiru arhitekture.
U ovom poglavlju opisana je odabrana arhitektura aplikacije, njezini slojevi i programska implementacija tih slojeva te načini i razlozi zašto je funkcionalnost implementirana na određen način.

%%%%% arhitektura
\subsection{Arhitektura}

Arhitektura aplikacije opisuje obrasce i tehnike koje se koriste za dizajniranje i izgradnju aplikacije. Daje savjete i najbolje prakse koje treba slijediti prilikom izgradnje aplikacije kako bi rezultat bio dobro strukturirana aplikacija.
Vrste arhitektura koje postoje su:
{
	\setlength{\parskip}{5pt}

	\begin{itemize}
		\item Slojevita ili N-slojna arhitektura
		\item Monolitna arhitektura
		\item Arhitektura mikroservisa
		\item Arhitektura vođena događajima (engl. \textit{event-driven})
		\item Servisno orijentirana arhitektura \cite{w_app_arch}
	\end{itemize}
}

\noindent
Za izradu završnog rada odabrana je izmijenjena vrsta slojevite arhitekture zvana \textit{onion} arhitektura (arhitektura luka). Arhitekturu je prvi predstavio Jeffrey Palermo kako bi ponudio bolji način za stvaranje rješenja za bolje održavanje, testiranje i pouzdanost \cite{w_onion}.

\noindent
Glavni cilj je kontrola spajanja (engl.\ \textit{coupling}).
Temeljno pravilo je da kod u određenom sloju može ovisiti o nižim slojevima, ali ne može ovisiti o slojevima koji se nalaze iznad njega.
Model domene je središte arhitekture i sadrži sve entitete domene.
Ti entiteti nemaju nikakvu vrstu ovisnosti.
Sloj usluga domene odgovoran je za definiranje potrebnih sučelja za pohranu i dohvaćanje objekata.
Vanjski sloj luka namijenjen je komponentama koje se često mijenjaju.
Ovdje se nalaze korisničko sučelje sa svim svojim pogledima, kontrolerima i svim stvarima vezanim uz logiku prezentacije \cite{w_onion}.
Slika \ref{f:onion} prikazuje pojednostavljeni prikaz arhitekture luka.

\newpage
\begin{figure}[ht!]
	\centering
	\includegraphics[scale=0.6]{images/4/onion.png}
	\caption{Slojevi arhitekture luka \cite{w_onion}. \label{f:onion}}
\end{figure}

\bigskip
\noindent
Arhitektura se uvelike oslanja na DIP načelo.
Poslužiteljski dio aplikacije, s kontrolerima i testovima, predstavlja središnji, odnosno unutarnji dio arhitekture luka.
Klijentski dio isključivo predstavlja korisničko sučelje vanjskog sloja.

%%%%% domain
\subsection{Sloj domene}

Za početak se stvara novi \textit{solution} s projektima \textit{Domain}, \textit{Repository}, \textit{Service} i \textit{API}.
Ti projekti će služiti kao slojevi arhitekture.
Svaki sloj ima dodatan projekt koji sadrži testove specifične za taj sloj.
Također, \textit{Repository} i \textit{Service} slojevi imaju dodatne projekte, a to su \textit{Repository.Contracts} i \textit{Service.Contracts}.
\textit{Repository} i \textit{Service} sadrže konkretne implementacije, dok se \textit{Repository.Contracts} i \textit{Service.Contracts} projekti sastoje isključivo od \textit{interfacea}.
S implementacijom se najčešće kreće od sloja domene.

\noindent
Domena sadrži entitete, objekt konteksta koji predstavlja konekciju s bazom, konfiguracije entiteta i migracije.
Prvo se definiraju entiteti.
Entiteti predstavljaju tablice u bazi, a također sadržavaju i dodatne atribute koji pomažu prilikom korištenja EF Core ORM alata.
Slika \ref{f:movie_entity} prikazuje zapečaćenu (engl.\ \textit{sealed}) klasu entiteta za film.
U C\# programskom jeziku ključna riječ \textit{sealed} označava klasu koja se ne može naslijediti.
Entiteti su središte sloja domene, oko njih se razvijaju ostali slojevi pa ih nema smisla nasljeđivati.

\bigskip
\begin{figure}[ht!]
	\centering
	\includegraphics[scale=0.635]{images/4/movie_entity.png}
	\caption{Entitet film. \label{f:movie_entity}}
\end{figure}

\noindent
Prije korištenja mogućnosti koje pruža EF Core potrebno je definirati objekt konteksta.
Objekt konteksta predstavlja sesiju s bazom podataka i omogućava izvršavanje naredbi i upita nad instancama entiteta.
U kontekstu se definiraju veze između entiteta, njihova ograničenja koja će se primijeniti na bazu i reference na entitete u bazi.
Na slici \ref{f:genre_map} je prikazana konfiguracija žanr tablice.
Prilikom konfiguracije entiteta, ako se eksplicitno ne postavi veličina određenih tipova podataka, EF Core će postaviti duljinu na najveću koju baza podržava.
Također, klasa je definirana s ključnom riječi \textit{internal}.
Kako se spomenuta klasa koristi samo u sloju domene za konfiguraciju konteksta nema potrebe da bude vidljiva u ostalim slojevima.

\bigskip
\begin{figure}[ht!]
	\centering
	\includegraphics[scale=0.75]{images/4/genre_map.png}
	\caption{Ograničenja u tablici. \label{f:genre_map}}
\end{figure}

\noindent
Nakon konfiguriranja konteksta potrebno je napraviti migracije.
Migracije predstavljaju promjene u shemi baze podataka.
Kada se promijeni bilo koji atribut, tip podatka ili ime entiteta potrebno je napraviti migraciju kako bi se te promijene primijenile i u bazi.
EF Core vodi brigu o kreiranju i primijeni migracija.
Može generirati migracije, ali one neće biti automatski primijenjene.

%%%%% repository
\subsection{\textit{Repository} sloj}

\textit{Repository} sloj koristi se za izvršavanje upita nad bazom podataka.
Ti upiti su osnovne operacije, a one uključuju dohvaćanje, sortiranje, filtriranje, ažuriranje i brisanje podataka.
\textit{Repository} sloj ovisi o sloju domene jer se sve operacije obavljaju kroz objekt konteksta.
Za lakšu manipulaciju podacima koriste se \textit{Unit of Work} i \textit{Specification} oblikovni obrasci.

\noindent
\textit{Specification} oblikovni obrazac omogućuje enkapsulaciju pojedinog znanja o domeni u specifikaciju.
Specifikacija opisuje logiku upita, sortiranja i paginacije (engl.\ \textit{pagination}) jer su svi upiti pohranjeni u nju.
Slika \ref{f:specification} prikazuje \textit{interface} za specifikaciju.
Sve metode (osim paginacije) kao parametar primaju LINQ izraze.
Nakon primjene specifikacije EF Core će LINQ izraze pretvoriti u efikasne SQL upite.
Za primjenu specifikacije koristi se \textit{Specification Evaluator} klasa.
\textit{Specification Evaluator} primjenjuje zadane upite iz specifikacije na objekt konteksta entiteta.

\bigskip
\begin{figure}[ht!]
	\centering
	\includegraphics[scale=0.55]{images/4/specification.png}
	\caption{Generička specifikacija. \label{f:specification}}
\end{figure}

\newpage
\noindent
Na slici \ref{f:repository} je prikazana \textit{GetAsync} metoda generičke \textit{repository} klase.
U metodi se na objekt konteksta entiteta primjenjuje specifikacija i vraća dobiveni rezultat izvršenog upita.
\textit{Repository} klasa je također definirana s ključnom riječi \textit{internal} jer se sve \textit{repository} klase nalaze unutar \textit{Unit of Work} obrasca.
One se ne koriste direktno nego se \textit{Unit of Work} preko \textit{dependency injectiona} ubrizgava u klase servisa.

\bigskip
\begin{figure}[ht!]
	\centering
	\includegraphics[scale=0.8]{images/4/repository.png}
	\caption{Primjena specifikacije prije dohvaćanja podataka. \label{f:repository}}
\end{figure}

%%%%% service
\subsection{Sloj servisa}

Sloj servisa služi za primjenu poslovnih pravila i predstavlja apstrakciju pristupa podacima.
U ovom sloju definirani su filtri i servisi za svaki entitet te moguće iznimke (engl. \textit{exceptions}).
Filtri sadrže atribute kao što su broj stranice, broj podataka po stranici, vrijednost sortiranja i ključna riječ za pretraživanje.
Ovisno o entitetu tih atributa može biti i više.
Servisi su podijeljeni u dvije kategorije: \textit{QueryService} i \textit{CommandService}.
\textit{Query} servisi se brinu isključivo o načinu dohvaćanja podataka iz baze, dok se \textit{Command} servisi brinu isključivo o modifikaciji tih podataka.

\noindent
Slika \ref{f:service} prikazuje par metoda iz generičke apstraktne klase \textit{QueryServiceBase}.
Većina metoda se može primijeniti na velik broj entiteta bez promjene njihove implementacije.
No zbog određenih poslovnih pravila metode se mogu razlikovati u nekoliko linija koda te su zbog toga označene s ključnom riječi \textit{virtual}.
Virtualne metode imaju implementaciju i mogu se nadjačati (engl.\ \textit{override}) ako poslovna pravila zahtijevaju određenu logiku.
Najbolji primjer za poslovnu logiku su projekcije.
Film ne može imati projekciju u istoj dvorani unutar vremena trajanja druge projekcije.
U tom slučaju potrebno je nadjačati metodu, obaviti potrebnu provjeru i ponovno pozvati metodu naslijeđene klase pomoću ključne riječi \textit{base}.

\bigskip
\begin{figure}[ht!]
	\centering
	\includegraphics[scale=0.6]{images/4/service.png}
	\caption{Virtualne i apstraktne metode servisne klase. \label{f:service}}
\end{figure}

\noindent
Apstraktne metode ne smiju imati implementaciju i moraju se nadjačati.
Metoda \textit{GetAllAsync} je označena s ključnom riječi \textit{abstract}.
Iako se mogla definirati kao virtualna metoda s jednostavnom implementacijom, postoje dva problema s takvim rješenjem.
Prvi problem je u tome što se ne može znati koje sve atribute filter može sadržavati.
Naravno, postoje četiri osnovna atributa, ali kako je klasa generička nije moguće uzeti u obzir i dodatne atribute.

\noindent
Drugi problem je vezan uz način kako EF Core funkcionira.
Za primjer se može uzeti film.
Film ima relaciju sa žanrovima.
I film i žanr su u bazi spremljeni u dvije različite tablice.
Ako se EF Core alatu eksplicitno ne kaže da film uključuje žanrove, prilikom dohvaćanja filmova, žanrovi će imati \textit{null} vrijednosti.
Problem se može riješiti tako što se u specifikaciji navede koje dodatne tablice se žele uključiti.
Metoda \textit{GetAllAsync} bi se uvijek morala nadjačati, bez obzira imala ona implementaciju ili ne.
Zbog toga je bolje ostaviti ju apstraktnom.

\noindent
Iako metoda \textit{GetAsync} sadrži implementaciju za dohvaćanje jednog podatka iz baze ona također ne zna koje dodatne tablice treba uključiti.
Razlog zašto metoda \textit{GetAllAsync} nema implementaciju je u tome što ima jedan problem više koji ovisi o entitetu.
Implementacija metode \textit{GetAsync} izbjegava pisanje istog koda za entitete koji ne uključuju dodatne tablice.
Isto tako, \textit{GetAsync} metoda ne ovisi o specifičnom filtru.

\noindent
Sloj servisa nije ograničen samo za apstrakciju nad \textit{repository} slojem.
On može sadržavati dodatne klase koje rješavaju određenu poslovnu logiku ili komuniciraju s vanjskim servisima.
Primjeri takvih servisa su servis za generiranje \textit{JWT} tokena, slanja e-pošte i prijenos slika.
Slika \ref{f:facade} prikazuje metodu jednog takvog servisa.
Isječak servisa predstavlja oblikovni obrazac fasadu, a prikazana metoda služi kao omotač oko vanjskog servisa za prijenos slika te uvelike olakšava njegovo korištenje.

\begin{figure}[ht!]
	\centering
	\includegraphics[scale=0.6]{images/4/facade.png}
	\caption{Metoda fasade za prijenos slika. \label{f:facade}}
\end{figure}

%%%%% api
\subsection{API sloj}

API sloj pruža odvojeno sučelje za podatke aplikacije te omogućuje interakciju s aplikacijom neovisno o programskom jeziku.
Dvije najčešće korištene vrste API-ja su REST (engl. \textit{Representational State Transfer}) i SOAP.
REST API-ji su dizajnirani da iskoriste prednosti postojećih protokola, a u slučaju Web API-ja to je HTTP protokol.
Za razliku od SOAP-a, REST nije ograničen samo na XML, već umjesto toga može vratiti XML, JSON, YAML ili bilo koji drugi format, ovisno o tome što klijent zahtijeva \cite{w_rest}.

\noindent
REST definira 6 arhitekturalnih ograničenja koje web usluge moraju poštovati:
{
	\setlength{\parskip}{5pt}

	\begin{enumerate}
		\item klijent-poslužitelj - klijent i poslužitelj trebaju biti odvojeni jedan od drugog i dopušteno im je da se razvijaju pojedinačno
		\item bez stanja (engl. \textit{stateless}) - pozivi se mogu obavljati neovisno jedan o drugom, a svaki poziv sadrži sve podatke potrebne da se uspješno završi
		\item predmemoriranje (engl. \textit{cacheing}) - treba biti osmišljen tako da potiče pohranu podataka koji se mogu predmemorirati
		\item ujednačeno sučelje (engl. \textit{uniform interface}) - omogućuje razvoj aplikacije bez potrebe za čvrstom povezanošću sa samim API slojem
		\item slojeviti sustav - različiti slojevi arhitekture rade zajedno na izgradnji hijerarhije koja pomaže u stvaranju skalabilnije aplikacije
		\item kod na zahtjev (nije obavezno) - omogućuje prijenos koda putem API-ja za korištenje unutar aplikacije \cite{w_rest}
	\end{enumerate}
}

\noindent
Tip API-ja u ovom završnom radu je REST API, a radi jednostavnosti implementacije tip podatka koji vraća je JSON.

\noindent
Najvažniji dio svakog REST API-ja je kontroler.
Kontroler obrađuje dolazne HTTP zahtjeve i šalje odgovor pozivatelju.
Odgovor se sastoji od tri stvari: zaglavlja i statusnog koda koji su obavezni te tijela odgovora koje, ovisno o zahtjevu, može biti neobavezno.
Kontroler nije ništa drugo nego obična klasa, mora biti javna, mora naslijediti apstraktnu klasu \textit{ControllerBase} i mora završavati s riječi \textit{"Controller"}.
Sve javne metode kontrolera nazivaju se akcijske metode.
Prema slici \ref{f:controller}, na kojoj je isječak kontroler klase, odmah su uočljive dvije stvari: kojoj ruti kontroler pripada te koji mu je povratni tip.
Svaka akcijska metoda unutar kontrolera može nadjačati te parametre.

\begin{figure}[ht!]
	\centering
	\includegraphics[scale=0.6]{images/4/controller.png}
	\caption{Klasa kontrolera. \label{f:controller}}
\end{figure}

\noindent
Na slici \ref{f:get} prikazana je \textit{GetAsync} akcijska metoda.
Ovisno o tome kako je ruta u metodi napisana, ona može proširiti postojeću rutu kontrolera, kao što je prikazano na slici, ili ako počinje sa znakom '/', onda će ju nadjačati.
Atribut \textit{HttpGet} označava da će se metoda izvršiti samo na \textit{GET} zahtjeve.
\textit{GetAsync} metoda nema tijela zahtijeva, tj. ne prima nikakve podatke nego samo vraća odgovor.
Također, atribut \textit{AllowAnonymous} omogućuje svima da pošalju zahtjev i dobiju odgovor.

\bigskip
\begin{figure}[ht!]
	\centering
	\includegraphics[scale=0.55]{images/4/get.png}
	\caption{GET Akcijska metoda. \label{f:get}}
\end{figure}

\noindent
Zatim imamo \textit{CreateAsync} akcijsku metodu prikazanu slikom \ref{f:post}.
Ona odgovara samo na \textit{POST} zahtjeve te mora sadržavati tijelo zahtjeva koje je u JSON obliku.
Također, samo registrirani korisnici koji imaju "Admin" ovlasti mogu pozvati metodu.

\bigskip
\begin{figure}[ht!]
	\centering
	\includegraphics[scale=0.6]{images/4/post.png}
	\caption{POST akcijska metoda. \label{f:post}}
\end{figure}

\noindent
Može se uočiti kako se u tijelu metode nigdje ne provjeravaju ovlasti korisnika, niti se vraća drugačiji status kod.
Kako kontroler zna da korisnik ima pristup?
Što ako korisnik nije registriran?
Što ako nema "Admin" ovlasti?
Hoće li mu kontroler uvijek vratiti status 201?
Zbog toga što kontroler nasljeđuje apstraktnu klasu \textit{ControllerBase} .NET Core će se sam pobrinuti o tim stvarima.
Npr. ako korisnik nije registriran kontroler će mu vratiti status 401, a ako nema "Admin" ovlasti status 403.
Isto tako, ako validacije tijela zahtjeva na prođu, .NET Core će vratiti status 400.
To su zadane postavke, no one se mogu nadjačati.
Status 400 se ne čini prikladnim kao rezultat neuspješne validacije te je bolje vratiti 422.
To se može učiniti tako da se definira vlastita \textit{ActionFilter} klasa, kao što je prikazano slikom \ref{f:filter}.
U \textit{ActionFilter} klasi može se definirati status kod koji će se vratiti zajedno s tijelom odgovora.

\begin{figure}[ht!]
	\centering
	\includegraphics[scale=0.6]{images/4/filter.png}
	\caption{Akcijski filtar. \label{f:filter}}
\end{figure}

\noindent
Što je s \textit{GET} metodom sa slike \ref{f:get}?
Nema nikakve provjere postoji li film s traženim ID-jem, a ako se bolje pogleda slika \ref{f:service} može se vidjeti da metoda \textit{GetAsync} baca iznimku \textit{EntityNotFoundException} ako je traženi entitet \textit{null}.
Također, metoda se ne nalazi u \textit{try-catch} bloku.
Prema zadanim postavkama .NET Core će za sve neuhvaćene iznimke vratiti isti status kod.
To nije dobra stvar jer onda se ne zna u kojem dijelu aplikacije je problem nastao i zbog čega je nastao.
U tom slučaju može se definirati vlastiti \textit{middleware} koji će hvatati iznimke te ovisno o vrsti iznimke vratiti određeni status i poruku.
Slika \ref{f:middleware} sadrži pojednostavljeni prikaz jednog takvog \textit{middlewarea}.
Svaki \textit{middleware} sadrži \textit{Invoke} metodu u kojoj se izvršava slijedeći \textit{middleware}.
Ako se u nekom od slijedećih dogodi iznimka, \textit{exception middleware} će ju uhvatiti i vratiti odgovarajuću poruku.
Zbog toga mora biti prvi u nizu, a ovisno o vrsti iznimke mogu se definirati različita tijela odgovora te različiti status kodovi.
\textit{Middleware} se može smatrati oblikovnim obrascem lanac odgovornosti.

\begin{figure}[ht!]
	\centering
	\includegraphics[scale=0.497]{images/4/middleware.png}
	\caption{Exception middleware. \label{f:middleware}}
\end{figure}

\newpage
\noindent
Na slici \ref{f:movie_dto} je prikazan DTO za entitet film.
Prikazani DTO služi za slanje podataka o filmu prilikom \textit{GET} zahtjeva za dohvaćanje svih filmova.
DTO za dohvaćanje pojedinog filma se razlikuje od onog za dohvaćanje njih svih.
Nadovezujući se na sliku \ref{f:movie_entity} vidljiva je razlika u imenima i tipovima podataka par atributa, a neki nisu prisutni.
Film ima relaciju više na više prema entitetima \textit{Genres} i \textit{People} te je potrebna treća tablica u bazi kako bi se relacije uspješno spremile.
Prilikom \textit{GET} zahtjeva u objektu za prijenos podataka informacija o trećoj tablici neće se slati.

\begin{figure}[ht!]
	\centering
	\includegraphics[scale=0.65]{images/4/movie_dto.png}
	\caption{DTO za film. \label{f:movie_dto}}
\end{figure}

\noindent
Za kopiranje podataka iz objekta za prijenos podataka u objekt entiteta koristi se Automapper.
Kopiranje podataka između jednostavnih objekata Automapper može odraditi automatski, ali u slučaju filma gdje se koristi dodatna tablica, gdje postoje različiti tipovi podataka između atributa i gdje postoje različita imena atributa, potrebno je napraviti dodatnu konfiguraciju kako bi se pomoglo Automapperu.
Stvari se dodatno kompliciraju kada treća tablica, koja povezuje entitete, sadrži i dodatne atribute.
Na slici \ref{f:movie_automapper} prikazana je metoda koja kopira podatke u entitet koji će se kasnije spremiti u bazu.
U bazi, glumci i redatelji su prikazani istom tablicom, a razlika se čini samo prilikom dohvaćanja podataka.
Razlog tome je što glumci mogu biti i redatelji te nema potrebe za udvostručavanjem podataka.
Zbog toga tablica koja povezuje filmove s osobama ima stupac \textit{Type} koji određuje glumca ili redatelja.
U slučaju glumca dodatno se spremaju i redoslijed te ime uloge u filmu.

\begin{figure}[ht!]
	\centering
	\includegraphics[scale=0.574]{images/4/movie_automapper.png}
	\caption{Isječak metode iz klase koja kopira podatke iz DTO u entitet. \label{f:movie_automapper}}
\end{figure}

\noindent
Dalje, potrebno je definirati DTO za \textit{POST}, \textit{PUT} i \textit{PATCH} zahtjeve.
Kako ovi zahtjevi služe za stvaranje i ažuriranje podataka od važnosti je pobrinuti se da dolazni podaci odgovaraju određenim kriterijima.
Za utvrđivanje podataka koriste se validacije.
Validacije se pišu iznad \textit{propertiesa}, slično anotacijama u \textit{Spring Bootu}.
Sa slike \ref{f:genre_dto}, koja prikazuje DTO žanra, odmah je uočljivo kako ime žanra ne smije sadržavati brojeve, mora biti između 2 i 32 znaka te mora započeti velikim slovom.
Navedene validacije odnose se samo na DTO.
Za validacije u bazi koristi se drugačija konfiguracija.

\bigskip
\begin{figure}[ht!]
	\centering
	\includegraphics[scale=0.58]{images/4/genre_dto.png}
	\caption{Validacija atributa za kreiranje žanra. \label{f:genre_dto}}
\end{figure}

%\noindent
%DTO koristi tip podatka \textit{record} umjesto klase.
%Kao i klasa, \textit{record} je referentni tip, ali specifičnost ima u tome što se jednom zadane vrijednosti ne mogu mijenjati, dakle nepromjenjive (engl.\ \textit{immutable}) su.
%U C\# programskom jeziku to se radi tako da se u \textit{properties} umjesto \textit{set} stavi ključna riječ \textit{init} ili definiraju svi parametri u konstruktoru.
%U drugim programskim jezicima to je isto kao da se definira klasa bez \textit{settera} i gdje se sve vrijednosti inicijaliziraju preko konstruktora.
%DTO vrijednosti ne smiju se mijenjati dok ne dođu do servisnog sloja.
