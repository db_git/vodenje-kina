//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Domain.Entities;
using Service.Contracts.Filters;

namespace Service.Contracts;

/// <inheritdoc />
public interface IMovieQueryService : IQueryServiceBase<Movie, MovieFilter>
{
	Task<IEnumerable<TDto>> GetFeaturedMoviesAsync<TDto>(int? limit) where TDto : class, new();
	Task<IEnumerable<TDto>> GetNewMoviesAsync<TDto>(int? limit) where TDto : class, new();
	Task<IEnumerable<TDto>> GetRunningMoviesAsync<TDto>(int day, int month, int? limit) where TDto : class, new();
	Task<IEnumerable<TDto>> GetRecommendedMoviesAsync<TDto>(int? limit) where TDto : class, new();
	Task<IEnumerable<TDto>> GetRecommendationsAsync<TDto>(string id) where TDto : class, new();
	Task<IEnumerable<TDto>> GetBestRatedMoviesAsync<TDto>() where TDto : class, new();
	Task<IEnumerable<TDto>> GetWorstRatedMoviesAsync<TDto>() where TDto : class, new();
}
