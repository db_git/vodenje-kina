//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Domain.Entities;

namespace Service.Contracts;

/// <inheritdoc />
public interface IHallCommandService : ICommandServiceBase<Hall>
{
	/// <summary>
	///     Inserts a <see cref="Hall"/> into database.
	/// </summary>
	/// <param name="hall">
	///     <see cref="Hall"/> to be added.
	/// </param>
	/// <exception cref="Service.Contracts.Exceptions.IEntityNotFoundException">
	///     Thrown when trying to save a hall whose cinema doesn't exist.
	/// </exception>
	/// <returns>
	///     A task that represents the asynchronous operation.
	/// </returns>
	new Task AddAsync(Hall hall);

	/// <summary>
	///     Inserts a collection of <see cref="Hall"/> into database.
	/// </summary>
	/// <param name="halls">
	///     <see cref="IEnumerable{Hall}"/> of <see cref="Hall"/> to be added.
	/// </param>
	/// <exception cref="Service.Contracts.Exceptions.IEntityNotFoundException">
	///     Thrown when trying to save a hall whose cinema doesn't exist.
	/// </exception>
	/// <returns>
	///     A task that represents the asynchronous operation.
	/// </returns>
	new Task AddRangeAsync(IEnumerable<Hall> halls);

	/// <summary>
	///     Updates a <see cref="Hall"/> in database.
	/// </summary>
	/// <param name="hall">
	///     <see cref="Hall"/> to be updated.
	/// </param>
	/// <exception cref="Service.Contracts.Exceptions.IEntityNotFoundException">
	///     Thrown when trying to save a hall whose cinema doesn't exist.
	/// </exception>
	/// <returns>
	///     A task that represents the asynchronous operation.
	/// </returns>
	new Task UpdateAsync(Hall hall);
}
