//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Service.Contracts.Storage;

/// <summary>
///     Facade around <see cref="ImageMagick.MagickImage"/>
///     and <see cref="Imagekit.Sdk.ImagekitClient"/>.
/// </summary>
public interface IImageFacade
{
	/// <summary>
	///     Uploads image specified in <paramref name="request"/> to remote storage.
	/// </summary>
	/// <param name="request">
	///     An instance of <see cref="IImageRequest"/> that contains
	///     the parameters needed to transform and save an image.
	/// </param>
	/// <exception cref="ImageMagick.MagickException">
	///     Thrown when conversion fails.
	/// </exception>
	/// <exception cref="Imagekit.Helper.ImagekitException">
	///     Thrown when saving fails.
	/// </exception>
	/// <returns>
	///     A task that represents the asynchronous operation.
	///     The task result contains response after successful saving.
	/// </returns>
	Task<Result> UploadImageAsync(IImageRequest request);

	/// <summary>
	///     Deletes an image from remote storage.
	/// </summary>
	/// <param name="imageId">
	///     A unique identifier that will be used to search for image.
	/// </param>
	/// <exception cref="Imagekit.Helper.ImagekitException">
	///     Thrown when deletion fails.
	/// </exception>
	/// <returns>
	///     A task that represents the asynchronous operation.
	///     The task result contains response after successful deletion.
	/// </returns>
	Task<ResultDelete> DeleteImageAsync(string imageId);

	/// <summary>
	///     Tries deleting an image from remote storage. Exception
	///     <see cref="Imagekit.Helper.ImagekitException"/>
	///     is not thrown and the failure is logged.
	/// </summary>
	/// <param name="imageId">
	///     A unique identifier that will be used to search for image.
	/// </param>
	/// <returns>
	///     A task that represents the asynchronous operation.
	///     The task result contains response after successful
	///     deletion or null if attempt is unsuccessful.
	/// </returns>
	Task<ResultDelete?> TryDeleteImageAsync(string imageId);

	/// <summary>
	///     Deletes directory from remote storage.
	/// </summary>
	/// <param name="directoryPath">
	///     A path that will be used to search for directory.
	///     Relative to URL endpoint.
	/// </param>
	/// <exception cref="Imagekit.Helper.ImagekitException">
	///     Thrown when deletion fails.
	/// </exception>
	/// <returns>
	///     A task that represents the asynchronous operation.
	///     The task result contains response after successful deletion.
	/// </returns>
	Task<ResultNoContent> DeleteDirectoryAsync(string directoryPath);
}
