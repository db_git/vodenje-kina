//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Service.Contracts.Storage;

/// <summary>
///     Required parameters for saving the image.
/// </summary>
public interface IImageRequest
{
	/// <summary>
	///     Directory path of the image under which it will be saved. Relative to URL endpoint.
	/// </summary>
	string Path { get; init; }

	/// <summary>
	///     Name of the image under which it will be saved. Relative to <see cref="Path">path</see>.
	/// </summary>
	string Name { get; init; }

	/// <summary>
	///     Base64 encoded image representation or URL pointing to the image.
	/// </summary>
	string Image { get; init; }

	/// <summary>
	///     The width dimension of the image. Value must be positive and less
	///     than or equal to 1920. If not positive, the value is set to 420.
	///     Ignored if <see cref="Image">image</see> is URL.
	/// </summary>
	int Width { get; init; }

	/// <summary>
	///     The height dimension of the image. Value must be positive and less
	///     than or equal to 1080. If not positive, the value is set to 630.
	///     Ignored if <see cref="Image">image</see> is URL.
	/// </summary>
	int Height { get; init; }

	/// <summary>
	///     Encoder quality. Value must be greater than or equal
	///     to 1 and less than or equal to 100. Default value is 90.
	///     Ignored if <see cref="Image">image</see> is URL.
	/// </summary>
	int Quality { get; init; }
}
