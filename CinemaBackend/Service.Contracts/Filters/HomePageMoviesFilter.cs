//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.ComponentModel.DataAnnotations;

namespace Service.Contracts.Filters;

public sealed record HomePageMoviesFilter
{
	[Range(1, 10)]
	public int FeaturedMoviesLimit { get; init; }

	public int? NewMoviesLimit { get; init; }

	[Range(1, 31)]
	public int RunningMoviesDay { get; init; }

	[Range(1, 12)]
	public int RunningMoviesMonth { get; init; }

	public int? RunningMoviesLimit { get; init; }
	public int? RecommendedMoviesLimit { get; init; }
}
