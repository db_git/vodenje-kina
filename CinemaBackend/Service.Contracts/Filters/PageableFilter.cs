//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.ComponentModel.DataAnnotations;

namespace Service.Contracts.Filters;

public record PageableFilter
{
	/// <summary>
	///     Requested page.
	/// </summary>
	[Range(1, int.MaxValue)]
	public int Page { get; init; }

	/// <summary>
	///     Number of records per page.
	/// </summary>
	[Range(1, 500)]
	public int Size { get; init; }
}
