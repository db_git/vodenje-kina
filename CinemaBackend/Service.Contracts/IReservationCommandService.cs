//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Domain.Entities;

namespace Service.Contracts;

/// <inheritdoc />
public interface IReservationCommandService : ICommandServiceBase<Reservation>
{
	/// <summary>
	///     Inserts a <see cref="Reservation"/> into database.
	/// </summary>
	/// <param name="reservation">
	///     <see cref="Reservation"/> to be added.
	/// </param>
	/// <exception cref="Service.Contracts.Exceptions.ISeatTakenException">
	///     Thrown when trying to make a reservation with taken seats.
	/// </exception>
	/// <exception cref="Service.Contracts.Exceptions.IOutOfStockException">
	///     Thrown when trying to make a reservation with food or souvenir that is out of stock.
	/// </exception>
	/// <exception cref="Service.Contracts.Exceptions.IProjectionTimeException">
	///     Thrown when trying to reserve projection after allowed timespan.
	/// </exception>
	/// <returns>
	///     A task that represents the asynchronous operation.
	/// </returns>
	new Task AddAsync(Reservation reservation);

	/// <summary>
	///     Inserts a collection of <see cref="Reservation"/> into database.
	/// </summary>
	/// <param name="reservations">
	///     <see cref="IEnumerable{Reservation}"/> of <see cref="Reservation"/> to be added.
	/// </param>
	/// <exception cref="Service.Contracts.Exceptions.ISeatTakenException">
	///     Thrown when trying to make a reservation with taken seats.
	/// </exception>
	/// <exception cref="Service.Contracts.Exceptions.IOutOfStockException">
	///     Thrown when trying to make a reservation with food or souvenir that is out of stock.
	/// </exception>
	/// <exception cref="Service.Contracts.Exceptions.IProjectionTimeException">
	///     Thrown when trying to reserve projection after allowed timespan.
	/// </exception>
	/// <returns>
	///     A task that represents the asynchronous operation.
	/// </returns>
	new Task AddRangeAsync(IEnumerable<Reservation> reservations);
}
