//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Domain.Entities;

namespace Service.Contracts;

/// <inheritdoc />
public interface ISeatCommandService : ICommandServiceBase<Seat>
{
	/// <summary>
	///     Inserts a <see cref="Seat"/> into database.
	/// </summary>
	/// <param name="seat">
	///     <see cref="Seat"/> to be added.
	/// </param>
	/// <exception cref="Service.Contracts.Exceptions.IDuplicateEntityException">
	///     Thrown when trying to save seat which violates unique constraint.
	/// </exception>
	/// <exception cref="Service.Contracts.Exceptions.IEntityNotFoundException">
	///     Thrown when trying to save a seat whose hall doesn't exist.
	/// </exception>
	/// <returns>
	///     A task that represents the asynchronous operation.
	/// </returns>
	new Task AddAsync(Seat seat);

	/// <summary>
	///     Inserts a collection of <see cref="Seat"/> into database.
	/// </summary>
	/// <param name="seats">
	///     <see cref="IEnumerable{Seat}"/> of <see cref="Seat"/> to be added.
	/// </param>
	/// <exception cref="Service.Contracts.Exceptions.IDuplicateEntityException">
	///     Thrown when trying to save seat which violates unique constraint.
	/// </exception>
	/// <exception cref="Service.Contracts.Exceptions.IEntityNotFoundException">
	///     Thrown when trying to save a seat whose hall doesn't exist.
	/// </exception>
	/// <returns>
	///     A task that represents the asynchronous operation.
	/// </returns>
	new Task AddRangeAsync(IEnumerable<Seat> seats);

	/// <summary>
	///     Updates a <see cref="Seat"/> in database.
	/// </summary>
	/// <param name="seat">
	///     <see cref="Seat"/> to be updated.
	/// </param>
	/// <exception cref="Service.Contracts.Exceptions.IDuplicateEntityException">
	///     Thrown when trying to save seat which violates unique constraint.
	/// </exception>
	/// <exception cref="Service.Contracts.Exceptions.IEntityNotFoundException">
	///     Thrown when trying to save a seat whose hall doesn't exist.
	/// </exception>
	/// <returns>
	///     A task that represents the asynchronous operation.
	/// </returns>
	new Task UpdateAsync(Seat seat);
}
