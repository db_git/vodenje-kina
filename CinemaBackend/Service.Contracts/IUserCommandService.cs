//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Domain.Entities.Identity;

namespace Service.Contracts;

public interface IUserCommandService
{
	/// <summary>
	///     Login user with provided credentials.
	/// </summary>
	/// <param name="email">
	///     Email to login with.
	/// </param>
	/// <param name="password">
	///     Password to login with.
	/// </param>
	/// <exception cref="Service.Contracts.Exceptions.IInvalidCredentialsException">
	///     Thrown when email or password is incorrect.
	/// </exception>
	/// <exception cref="Service.Contracts.Exceptions.IEmailNotConfirmedException">
	///     Thrown when email is not confirmed.
	/// </exception>
	/// <returns>
	///     A task that represents the asynchronous operation.
	/// </returns>
	Task LoginAsync(string email, string password);

	/// <summary>
	///     Register new user.
	/// </summary>
	/// <param name="user">
	///     <see cref="User"/> to register.
	/// </param>
	/// <param name="password">
	///     Password to register with.
	/// </param>
	/// <exception cref="Service.Contracts.Exceptions.IEmailAlreadyInUseException">
	///     Thrown when registering with existing email.
	/// </exception>
	/// <exception cref="Service.Contracts.Exceptions.IRegistrationException">
	///     Thrown when registration is unsuccessful.
	/// </exception>
	/// <exception cref="Service.Contracts.Exceptions.IEmailSenderException">
	///     Thrown when there is an error with email sender.
	/// </exception>
	/// <returns>
	///     A task that represents the asynchronous operation.
	/// </returns>
	Task RegisterAsync(User user, string password);

	/// <summary>
	///     Confirm user email address.
	/// </summary>
	/// <param name="userId">
	///     Unique identifier that will be used to search for user.
	/// </param>
	/// <param name="verificationCode">
	///     Generated confirmation code a user has received in email.
	/// </param>
	/// <exception cref="Service.Contracts.Exceptions.IInvalidCredentialsException">
	///     Thrown when user can't be found.
	/// </exception>
	/// <exception cref="Service.Contracts.Exceptions.ICodeExpiredException">
	///     Thrown when verification code is expired.
	/// </exception>
	/// <returns>
	///     A task that represents the asynchronous operation.
	/// </returns>
	Task VerifyEmailAsync(Guid userId, string verificationCode);

	/// <summary>
	///     Update existing user.
	/// </summary>
	/// <param name="user">
	///     <see cref="User"/> to update.
	/// </param>
	/// <exception cref="Service.Contracts.Exceptions.IRegistrationException">
	///     Thrown when update fails.
	/// </exception>
	/// <returns>
	///     A task that represents the asynchronous operation.
	/// </returns>
	Task UpdateUserAsync(User user);

	/// <summary>
	///     Send an email to reset password.
	/// </summary>
	/// <param name="email">
	///     Email to send password reset link.
	/// </param>
	/// <exception cref="Service.Contracts.Exceptions.IInvalidCredentialsException">
	///     Thrown when user can't be found.
	/// </exception>
	/// <exception cref="Service.Contracts.Exceptions.IEmailSenderException">
	///     Thrown when there is an error with email sender.
	/// </exception>
	/// <returns>
	///     A task that represents the asynchronous operation.
	/// </returns>
	Task ForgotPasswordAsync(string email);

	/// <summary>
	///     Update forgotten password.
	/// </summary>
	/// <param name="email">
	///     Email belonging to user.
	/// </param>
	/// <param name="resetCode">
	///     Generated reset code a user has received in email.
	/// </param>
	/// <param name="newPassword">
	///     New password for account.
	/// </param>
	/// <exception cref="Service.Contracts.Exceptions.IInvalidCredentialsException">
	///     Thrown when user can't be found.
	/// </exception>
	/// <exception cref="Service.Contracts.Exceptions.ICodeExpiredException">
	///     Thrown when verification code is expired.
	/// </exception>
	/// <returns>
	///     A task that represents the asynchronous operation.
	/// </returns>
	Task ResetPasswordAsync(string email, string resetCode, string newPassword);

	/// <summary>
	///     Change password.
	/// </summary>
	/// <param name="userId">
	///     Unique identifier that will be used to search for user.
	/// </param>
	/// <param name="oldPassword">
	///     Existing password.
	/// </param>
	/// <param name="newPassword">
	///     New password to replace old one.
	/// </param>
	/// <exception cref="Service.Contracts.Exceptions.IInvalidCredentialsException">
	///     Thrown when user can't be found or invalid <paramref name="oldPassword"/> is provided.
	/// </exception>
	/// <exception cref="Service.Contracts.Exceptions.IRegistrationException">
	///     Thrown when password change fails.
	/// </exception>
	/// <returns>
	///     A task that represents the asynchronous operation.
	/// </returns>
	Task UpdatePasswordAsync(Guid userId, string oldPassword, string newPassword);
}
