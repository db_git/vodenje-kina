//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Linq.Expressions;
using Domain;
using Service.Contracts.Filters;

namespace Service.Contracts;

/// <summary>
///     Query service abstracts data sorting, filtering, paging and querying.
/// </summary>
/// <typeparam name="TEntity">
///     The type of entity being operated on.
/// </typeparam>
/// <typeparam name="TFilter">
///     <see cref="QueryFilter"/> class for entity specific filtering.
/// </typeparam>
public interface IQueryServiceBase<TEntity, in TFilter>
	where TEntity : class, IEntity
	where TFilter : QueryFilter
{
	/// <summary>
	///     Count the number of entities.
	/// </summary>
	/// <returns>
	///     A task that represents the asynchronous operation. The task result
	///     contains the total number of entities stored in database.
	/// </returns>
	Task<int> CountAsync();

	/// <summary>
	///     Count the number of entities.
	/// </summary>
	/// <param name="entityFilter">
	///     <see cref="QueryFilter"/> instance that specifies filtering.
	/// </param>
	/// <returns>
	///     A task that represents the asynchronous operation. The task result
	///     contains the total number of matching entities stored in database.
	/// </returns>
	Task<int> CountAsync(TFilter entityFilter);

	/// <summary>
	///     Count the number of entities.
	/// </summary>
	/// <param name="expression">
	///     Expression that specifies filtering.
	/// </param>
	/// <returns>
	///     A task that represents the asynchronous operation. The task result
	///     contains the total number of matching entities stored in database.
	/// </returns>
	Task<int> CountAsync(Expression<Func<TEntity, bool>> expression);

	/// <summary>
	///    Retrieves ordered and filtered <see cref="TEntity"/> collection from database.
	/// </summary>
	/// <param name="entityFilter">
	///     <see cref="QueryFilter"/> instance that specifies filtering.
	/// </param>
	/// <returns>
	///     A task that represents the asynchronous operation. The task result
	///     contains a collection of <see cref="TEntity"/> or empty collection.
	/// </returns>
	Task<ICollection<TEntity>> GetAllAsync(TFilter entityFilter);

	/// <summary>
	///    Retrieves ordered and filtered <see cref="TDto"/> collection from database.
	/// </summary>
	/// <param name="entityFilter">
	///     <see cref="QueryFilter"/> instance that specifies filtering.
	/// </param>
	/// <returns>
	///     A task that represents the asynchronous operation. The task result
	///     contains a collection of <see cref="TDto"/> or empty collection.
	/// </returns>
	Task<ICollection<TDto>> GetAllAsync<TDto>(TFilter entityFilter) where TDto : class, new();

	/// <summary>
	///     Retrieves first instance of <see cref="TEntity"/> matching <see cref="expression"/>.
	/// </summary>
	/// <param name="expression">
	///     Expression for searching an entity with a unique parameter.
	/// </param>
	/// <param name="withTracking">
	///     Determine if entity should be tracked by context or not.
	/// </param>
	/// <exception cref="Service.Contracts.Exceptions.IEntityNotFoundException">
	///     Thrown when <see cref="TEntity"/> could not be found.
	/// </exception>
	/// <returns>
	///     A task that represents the asynchronous operation. The task
	///     result contains a single instance of <see cref="TEntity"/>.
	/// </returns>
	Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> expression, bool withTracking = false);

	/// <summary>
	///     Retrieves first instance of <see cref="TDto"/> matching <see cref="expression"/>.
	/// </summary>
	/// <param name="expression">
	///     Expression for searching an entity with a unique parameter.
	/// </param>
	/// <exception cref="Service.Contracts.Exceptions.IEntityNotFoundException">
	///     Thrown when <see cref="TEntity"/> could not be found.
	/// </exception>
	/// <returns>
	///     A task that represents the asynchronous operation. The task
	///     result contains a single instance of <see cref="TDto"/>.
	/// </returns>
	Task<TDto> GetAsync<TDto>(Expression<Func<TEntity, bool>> expression) where TDto : class, new();

	/// <summary>
	///     Retrieves first instance of <see cref="TEntity"/> matching <see cref="expression"/>.
	/// </summary>
	/// <param name="expression">
	///     Expression for searching an entity with a unique parameter.
	/// </param>
	/// <param name="withTracking">
	///     Determine if entity should be tracked by context or not.
	/// </param>
	/// <returns>
	///     A task that represents the asynchronous operation. The task result contains
	///     a single instance of <see cref="TEntity"/> or null if not found.
	/// </returns>
	Task<TEntity?> TryGetAsync(Expression<Func<TEntity, bool>> expression, bool withTracking = false);

	/// <summary>
	///     Retrieves first instance of <see cref="TDto"/> matching <see cref="expression"/>.
	/// </summary>
	/// <param name="expression">
	///     Expression for searching an entity with a unique parameter.
	/// </param>
	/// <returns>
	///     A task that represents the asynchronous operation. The task result contains
	///     a single instance of <see cref="TDto"/> or null if not found.
	/// </returns>
	Task<TDto?> TryGetAsync<TDto>(Expression<Func<TEntity, bool>> expression) where TDto : class, new();
}
