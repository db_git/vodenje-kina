//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Domain.Entities.Identity;
using SendGrid;

namespace Service.Contracts.Email;

/// <summary>
///     Service for sending e-mails.
/// </summary>
public interface IEmailSender
{
	/// <summary>
	///     Send confirmation e-mail containing verification link.
	/// </summary>
	/// <param name="user">
	///     <see cref="Microsoft.AspNetCore.Identity.IdentityUser{Guid}"/> to verify.
	/// </param>
	/// <param name="data">
	///     Data required to populate <c>Handlebars</c> template.
	/// </param>
	/// <returns>
	///     A task that represents the asynchronous operation.
	///     The task result contains <see cref="SendGrid.Response"/> from an API call to e-mail provider.
	/// </returns>
	Task<Response> SendEmailConfirmationAsync(User user, object data);

	/// <summary>
	///     Send e-mail containing password reset link.
	/// </summary>
	/// <param name="user">
	///     <see cref="Microsoft.AspNetCore.Identity.IdentityUser{Guid}"/> to reset password.
	/// </param>
	/// <param name="data">
	///     Data required to populate <c>Handlebars</c> template.
	/// </param>
	/// <returns>
	///     A task that represents the asynchronous operation.
	///     The task result contains <see cref="SendGrid.Response"/> from an API call to e-mail provider.
	/// </returns>
	Task<Response> SendResetPasswordAsync(User user, object data);

	/// <summary>
	///     Send e-mail containing orders.
	/// </summary>
	/// <param name="user">
	///     <see cref="Microsoft.AspNetCore.Identity.IdentityUser{Guid}"/> to reset password.
	/// </param>
	/// <param name="reservationIds">
	///     Unique identifiers of users reservations.
	/// </param>
	/// <returns>
	///     A task that represents the asynchronous operation.
	///     The task result contains <see cref="SendGrid.Response"/> from an API call to e-mail provider.
	/// </returns>
	Task<Response> SendReservationDetailsAsync(User user, IEnumerable<string> reservationIds);
}
