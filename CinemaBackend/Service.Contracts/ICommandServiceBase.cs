//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Domain;

namespace Service.Contracts;

/// <summary>
///     Command service abstracts business logic and data operations.
/// </summary>
/// <typeparam name="TEntity">
///     The type of entity being operated on.
/// </typeparam>
public interface ICommandServiceBase<in TEntity> where TEntity : class, IEntity
{
	/// <summary>
	///     Inserts a <see cref="TEntity"/> into database.
	/// </summary>
	/// <param name="entity">
	///     <see cref="TEntity"/> to be added.
	/// </param>
	/// <returns>
	///     A task that represents the asynchronous operation.
	/// </returns>
	Task AddAsync(TEntity entity);

	/// <summary>
	///     Inserts a collection of <see cref="TEntity"/> into database.
	/// </summary>
	/// <param name="entities">
	///     <see cref="IEnumerable{TEntity}"/> of <see cref="TEntity"/> to be added.
	/// </param>
	/// <returns>
	///     A task that represents the asynchronous operation.
	/// </returns>
	Task AddRangeAsync(IEnumerable<TEntity> entities);

	/// <summary>
	///     Updates a <see cref="TEntity"/> in database.
	/// </summary>
	/// <param name="entity">
	///     <see cref="TEntity"/> to be updated.
	/// </param>
	Task UpdateAsync(TEntity entity);

	/// <summary>
	///     Removes <see cref="TEntity"/> from database.
	/// </summary>
	/// <param name="entity">
	///     <see cref="TEntity"/> to be deleted.
	/// </param>
	/// <returns>
	///     A task that represents the asynchronous operation.
	/// </returns>
	Task DeleteAsync(TEntity entity);

	/// <summary>
	///     Removes a collection of <see cref="TEntity"/> from database.
	/// </summary>
	/// <param name="entities">
	///     <see cref="IEnumerable{TEntity}"/> of <see cref="TEntity"/> to be deleted.
	/// </param>
	/// <returns>
	///     A task that represents the asynchronous operation.
	/// </returns>
	Task DeleteRangeAsync(IEnumerable<TEntity> entities);
}
