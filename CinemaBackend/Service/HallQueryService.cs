//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Service;

/// <inheritdoc cref="IHallQueryService" />
public sealed class HallQueryService : QueryServiceBase<Hall, QueryFilter>, IHallQueryService
{
	public HallQueryService(IUnitOfWork unitOfWork, ISpecification<Hall> specification)
	{
		UnitOfWork = unitOfWork;
		Repository = UnitOfWork.HallRepository;
		Specification = specification;
	}

	public override async Task<ICollection<Hall>> GetAllAsync(QueryFilter hallFilter)
	{
		Expression<Func<Hall, bool>>? filter = null;

		ApplyFilter(ref filter, ref hallFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(hallFilter);
		ApplyFilterOrderBy(hallFilter);

		Specification.AddInclude(query => query
			.Include(hall => hall.Cinema)
		);

		return await Repository.GetAllAsync(Specification);
	}

	public override async Task<ICollection<TDto>> GetAllAsync<TDto>(QueryFilter hallFilter)
	{
		Expression<Func<Hall, bool>>? filter = null;

		ApplyFilter(ref filter, ref hallFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(hallFilter);
		ApplyFilterOrderBy(hallFilter);

		Specification.AddInclude(query => query
			.Include(hall => hall.Cinema)
		);

		return await Repository.GetAllAsync<TDto>(Specification);
	}

	public override async Task<Hall> GetAsync(Expression<Func<Hall, bool>> expression, bool withTracking = false)
	{
		var hall = await TryGetAsync(expression, withTracking);
		if (hall is null) throw new EntityNotFoundException();
		return hall;
	}

	public override async Task<TDto> GetAsync<TDto>(Expression<Func<Hall, bool>> expression)
	{
		var hallDto = await TryGetAsync<TDto>(expression);
		if (hallDto is null) throw new EntityNotFoundException();
		return hallDto;
	}

	public override async Task<Hall?> TryGetAsync(Expression<Func<Hall, bool>> expression, bool withTracking = false)
	{
		Specification.AddFilter(expression);
		Specification.WithTracking(withTracking);
		Specification.AddInclude(query => query
			.Include(hall => hall.Cinema)
			.Include(hall => hall.Projections)
			.Include(hall => hall.HallSeats)
			.ThenInclude(hallSeat => hallSeat.Seat)
		);

		return await Repository.GetAsync(Specification);
	}

	public override async Task<TDto?> TryGetAsync<TDto>(Expression<Func<Hall, bool>> expression) where TDto : class
	{
		Specification.AddFilter(expression);
		Specification.AddInclude(query => query
			.Include(hall => hall.Cinema)
			.Include(hall => hall.Projections)
			.Include(hall => hall.HallSeats)
			.ThenInclude(hallSeat => hallSeat.Seat)
		);

		return await Repository.GetAsync<TDto>(Specification);
	}

	protected override void ApplyFilter(
		ref Expression<Func<Hall, bool>>? filter,
		ref QueryFilter hallFilter
	)
	{
		if (hallFilter.Search == null) return;

		var lowercaseSearch = hallFilter.Search.ToLower();
		var searchWords = lowercaseSearch.Split(' ');

		filter = hall => hall.Name.ToLower().Contains(lowercaseSearch);
		filter = searchWords.Aggregate(filter, (current, word)
			=> current.Or(hall => hall.Name.ToLower().Contains(word)));
	}

	private void ApplyFilterOrderBy(in QueryFilter hallFilter)
	{
		Expression<Func<Hall, object>> order = hall => hall.Name;
		ApplyFilterOrderBy(in hallFilter, in order);
	}
}
