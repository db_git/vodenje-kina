//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Service;

/// <inheritdoc cref="ICinemaQueryService" />
public sealed class CinemaQueryService : QueryServiceBase<Cinema, QueryFilter>, ICinemaQueryService
{
	public CinemaQueryService(IUnitOfWork unitOfWork, ISpecification<Cinema> specification)
	{
		UnitOfWork = unitOfWork;
		Repository = UnitOfWork.CinemaRepository;
		Specification = specification;
	}

	public override async Task<ICollection<Cinema>> GetAllAsync(QueryFilter cinemaFilter)
	{
		Expression<Func<Cinema, bool>>? filter = null;

		ApplyFilter(ref filter, ref cinemaFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(cinemaFilter);
		ApplyFilterOrderBy(cinemaFilter);

		Specification.AddInclude(query => query
			.Include(cinema => cinema.Halls)
			.ThenInclude(hall => hall.HallSeats)
		);

		return await Repository.GetAllAsync(Specification);
	}

	public override async Task<ICollection<TDto>> GetAllAsync<TDto>(QueryFilter cinemaFilter)
	{
		Expression<Func<Cinema, bool>>? filter = null;

		ApplyFilter(ref filter, ref cinemaFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(cinemaFilter);
		ApplyFilterOrderBy(cinemaFilter);

		Specification.AddInclude(query => query
			.Include(cinema => cinema.Halls)
			.ThenInclude(hall => hall.HallSeats)
		);

		return await Repository.GetAllAsync<TDto>(Specification);
	}

	public override async Task<Cinema> GetAsync(Expression<Func<Cinema, bool>> expression, bool withTracking = false)
	{
		var cinema = await TryGetAsync(expression, withTracking);
		if (cinema is null) throw new EntityNotFoundException();
		return cinema;
	}

	public override async Task<TDto> GetAsync<TDto>(Expression<Func<Cinema, bool>> expression)
	{
		var cinemaDto = await TryGetAsync<TDto>(expression);
		if (cinemaDto is null) throw new EntityNotFoundException();
		return cinemaDto;
	}

	public override async Task<Cinema?> TryGetAsync(Expression<Func<Cinema, bool>> expression, bool withTracking = false)
	{
		Specification.AddFilter(expression);
		Specification.WithTracking(withTracking);
		Specification.AddInclude(query => query
			.Include(cinema => cinema.Halls)
			.ThenInclude(hall => hall.HallSeats)
		);

		return await Repository.GetAsync(Specification);
	}

	public override async Task<TDto?> TryGetAsync<TDto>(Expression<Func<Cinema, bool>> expression) where TDto : class
	{
		Specification.AddFilter(expression);
		Specification.AddInclude(query => query
			.Include(cinema => cinema.Halls)
			.ThenInclude(hall => hall.HallSeats)
		);

		return await Repository.GetAsync<TDto>(Specification);
	}

	protected override void ApplyFilter(
		ref Expression<Func<Cinema, bool>>? filter,
		ref QueryFilter cinemaFilter
	)
	{
		if (cinemaFilter.Search == null) return;

		var lowercaseSearch = cinemaFilter.Search.ToLower();
		var searchWords = lowercaseSearch.Split(' ');

		filter = cinema => cinema.Name.ToLower().Contains(lowercaseSearch);
		filter.Or(cinema => cinema.City.ToLower().Contains(lowercaseSearch));

		filter = searchWords.Aggregate(filter, (current, word)
			=> current.Or(cinema => cinema.Name.ToLower().Contains(word)));
		filter = searchWords.Aggregate(filter, (current, word)
			=> current.Or(cinema => cinema.City.ToLower().Contains(word)));
	}

	private void ApplyFilterOrderBy(in QueryFilter cinemaFilter)
	{
		Expression<Func<Cinema, object>> order = cinemaFilter.OrderBy switch
		{
			(int) CinemaOrderBy.CityAscending or (int) CinemaOrderBy.CityDescending => cinema => cinema.City,
			(int) CinemaOrderBy.TicketPriceAscending => cinema => cinema.TicketPrice,
			(int) CinemaOrderBy.TicketPriceDescending => cinema => cinema.TicketPrice,
			_ => cinema => cinema.Name
		};

		ApplyFilterOrderBy(in cinemaFilter, in order);
	}
}
