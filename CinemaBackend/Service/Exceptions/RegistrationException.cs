//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Runtime.Serialization;
using Service.Contracts.Exceptions;

namespace Service.Exceptions;

/// <inheritdoc cref="IRegistrationException" />
[Serializable]
public sealed class RegistrationException : Exception, IRegistrationException
{
	public IDictionary<string, IEnumerable<string>> Errors { get; } = null!;

	public RegistrationException(IDictionary<string, IEnumerable<string>> errors)
	{
		Errors = errors;
	}

	private RegistrationException(SerializationInfo info, StreamingContext context) : base(info, context) {}
}
