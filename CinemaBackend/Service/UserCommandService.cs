//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Domain.Entities.Identity;
using Domain.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Service.Contracts.Email;

namespace Service;

/// <inheritdoc cref="IUserCommandService" />
public sealed class UserCommandService : IUserCommandService
{
	private readonly SignInManager<User> _signInManager;
	private readonly UserManager<User> _userManager;
	private readonly IClock _clock;
	private readonly IEmailSender _emailSender;
	private readonly IConfiguration _configuration;

	public UserCommandService(
		UserManager<User> userManager,
		SignInManager<User> signInManager,
		IClock clock,
		IEmailSender emailSender,
		IConfiguration configuration
	)
	{
		_userManager = userManager;
		_signInManager = signInManager;
		_emailSender = emailSender;
		_configuration = configuration;
		_clock = clock;
	}

	public async Task LoginAsync(string email, string password)
	{
		var user = await _userManager.FindByEmailAsync(email);
		if (user is null) throw new InvalidCredentialsException();

		var result = await _signInManager.PasswordSignInAsync(
			email,
			password,
			false,
			false
		);

		switch (result.Succeeded)
		{
			case false when !user.EmailConfirmed:
				throw new EmailNotConfirmedException();
			case false when user.EmailConfirmed:
				throw new InvalidCredentialsException();
		}
	}

	public async Task RegisterAsync(User user, string password)
	{
		var existingUser = await _userManager.FindByEmailAsync(user.Email!);
		if (existingUser is not null && existingUser.EmailConfirmed) throw new EmailAlreadyInUseException();

		user.CreatedAt = _clock.GetCurrentInstant();
		user.EmailConfirmed = false;
		var result = await _userManager.CreateAsync(user, password);

		if (!result.Succeeded)
		{
			if (existingUser is not null && !existingUser.EmailConfirmed)
				await _userManager.DeleteAsync(existingUser);

			throw new RegistrationException(MapErrors(result.Errors));
		}

		await _userManager.AddToRoleAsync(user, nameof(Roles.User));

		var verificationCode = await _userManager.GenerateEmailConfirmationTokenAsync(user);
		verificationCode = System.Text.Encodings.Web.UrlEncoder.Default.Encode(verificationCode);

		var emailVerificationLink = _configuration
			.GetSection("SendGrid")
			.GetSection("EmailVerificationLink")
			.Value!;

		var query = $"userId={user.Id}&verificationCode={verificationCode}";

		var senderResponse = await _emailSender.SendEmailConfirmationAsync(user, new
		{
			fullName = user.Name,
			verificationLink = $"{emailVerificationLink}?{query}"
		});

		if (!senderResponse.IsSuccessStatusCode) throw new EmailSenderException(senderResponse);
	}

	public async Task ForgotPasswordAsync(string email)
	{
		var user = await _userManager.FindByEmailAsync(email);
		if (user is null) throw new InvalidCredentialsException();

		var passwordResetCode = await _userManager.GeneratePasswordResetTokenAsync(user);
		passwordResetCode = System.Text.Encodings.Web.UrlEncoder.Default.Encode(passwordResetCode);

		var passwordResetLink = _configuration
			.GetSection("SendGrid")
			.GetSection("PasswordResetLink")
			.Value!;

		var query = $"userEmail={user.Email}&passwordResetCode={passwordResetCode}";

		var senderResponse = await _emailSender.SendResetPasswordAsync(user, new
		{
			resetLink = $"{passwordResetLink}?{query}"
		});

		if (!senderResponse.IsSuccessStatusCode) throw new EmailSenderException(senderResponse);
	}

	public async Task VerifyEmailAsync(Guid userId, string verificationCode)
	{
		var user = await _userManager.FindByIdAsync(userId.ToString());

		if (user is null) throw new InvalidCredentialsException();
		if (user.EmailConfirmed) return;

		var result = await _userManager.ConfirmEmailAsync(user, verificationCode);
		if (!result.Succeeded && !user.EmailConfirmed) throw new CodeExpiredException();
	}

	public async Task UpdateUserAsync(User user)
	{
		var result = await _userManager.UpdateAsync(user);
		if (!result.Succeeded) throw new RegistrationException(MapErrors(result.Errors));
	}

	public async Task ResetPasswordAsync(string email, string resetCode, string newPassword)
	{
		var user = await _userManager.FindByEmailAsync(email);
		if (user is null) throw new InvalidCredentialsException();

		var result = await _userManager.ResetPasswordAsync(user, resetCode, newPassword);
		if (!result.Succeeded) throw new CodeExpiredException();
	}

	public async Task UpdatePasswordAsync(Guid userId, string oldPassword, string newPassword)
	{
		var user = await _userManager.FindByIdAsync(userId.ToString());
		if (user is null) throw new InvalidCredentialsException();

		var signInResult = await _signInManager.PasswordSignInAsync(
			user.Email,
			oldPassword,
			false,
			false
		);
		if (!signInResult.Succeeded) throw new InvalidCredentialsException();

		var passwordChangeResult = await _userManager.ChangePasswordAsync(
			user,
			oldPassword,
			newPassword
		);

		if (!passwordChangeResult.Succeeded) throw new RegistrationException(MapErrors(passwordChangeResult.Errors));
	}

	private static Dictionary<string, IEnumerable<string>> MapErrors(in IEnumerable<IdentityError> identityErrors)
	{
		var errors = new Dictionary<string, List<string>>(identityErrors.Count());

		foreach (var error in identityErrors)
		{
			var errorCode = error.Code.ToUpperInvariant();
			string key;

			if (errorCode.Contains("PASSWORD")) key = "Password";
			else if (errorCode.Contains("NAME")) key = "Name";
			else if (errorCode.Contains("EMAIL")) key = "Email";
			else if (errorCode.Contains("USERNAME")) continue;
			else key = "User";

			if (!errors.ContainsKey(key)) errors.Add(key, new List<string>(1));
			errors[key].Add(error.Description);
		}

		return errors.ToDictionary(
			kvp => kvp.Key,
			kvp => kvp.Value.Select(e => e)
		);
	}
}
