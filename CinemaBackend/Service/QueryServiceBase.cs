//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Domain;

namespace Service;

/// <inheritdoc />
public abstract class QueryServiceBase<TEntity, TFilter> : IQueryServiceBase<TEntity, TFilter>
	where TEntity : class, IEntity
	where TFilter : QueryFilter
{
	protected IUnitOfWork UnitOfWork { get; init; } = null!;
	protected IRepositoryBase<TEntity> Repository { get; init; } = null!;
	protected ISpecification<TEntity> Specification { get; init; } = null!;

	/// <inheritdoc />
	public virtual Task<int> CountAsync()
	{
		return Repository.CountAsync();
	}

	/// <inheritdoc />
	public virtual async Task<int> CountAsync(TFilter entityFilter)
	{
		Expression<Func<TEntity, bool>>? filter = null;
		ApplyFilter(ref filter, ref entityFilter);
		return await Repository.CountAsync(filter);
	}

	/// <inheritdoc />
	public virtual async Task<int> CountAsync(Expression<Func<TEntity, bool>> expression)
	{
		Specification.AddFilter(expression);
		return await Repository.CountAsync(Specification);
	}

	/// <inheritdoc />
	public abstract Task<ICollection<TEntity>> GetAllAsync(TFilter entityFilter);

	/// <inheritdoc />
	public abstract Task<ICollection<TDto>> GetAllAsync<TDto>(TFilter entityFilter) where TDto : class, new();

	/// <inheritdoc />
	public virtual async Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> expression, bool withTracking = false)
	{
		var entity = await TryGetAsync(expression, withTracking);
		if (entity is null) throw new EntityNotFoundException();
		return entity;
	}

	/// <inheritdoc />
	public virtual async Task<TDto> GetAsync<TDto>(Expression<Func<TEntity, bool>> expression) where TDto : class, new()
	{
		var dto = await TryGetAsync<TDto>(expression);
		if (dto is null) throw new EntityNotFoundException();
		return dto;
	}

	/// <inheritdoc />
	public virtual async Task<TEntity?> TryGetAsync(Expression<Func<TEntity, bool>> expression, bool withTracking = false)
	{
		Specification.AddFilter(expression);
		Specification.WithTracking(withTracking);
		return await Repository.GetAsync(Specification);
	}

	/// <inheritdoc />
	public virtual async Task<TDto?> TryGetAsync<TDto>(Expression<Func<TEntity, bool>> expression) where TDto : class, new()
	{
		Specification.AddFilter(expression);
		return await Repository.GetAsync<TDto>(Specification);
	}

	/// <summary>
	///     Apply specific <see cref="TEntity"/> filter parameters.
	/// </summary>
	/// <param name="filter">
	///     An expression containing filtering information.
	/// </param>
	/// <param name="entityFilter">
	///     <see cref="TEntity"/> specific filter that specifies various parameters.
	/// </param>
	protected abstract void ApplyFilter(ref Expression<Func<TEntity, bool>>? filter, ref TFilter entityFilter);

	/// <summary>
	///     Apply pagination from filter parameters.
	/// </summary>
	/// <param name="entityFilter">
	///     <see cref="QueryFilter"/> instance that specifies pagination parameters.
	/// </param>
	protected void ApplyFilterPagination(in QueryFilter entityFilter)
	{
		Specification.AddPagination(entityFilter.Page, entityFilter.Size);
	}

	/// <summary>
	///     Apply ordering according to filter parameter.
	/// </summary>
	/// <param name="entityFilter">
	///     A <see cref="QueryFilter"/> instance that specifies the order parameter.
	/// </param>
	/// <param name="order">
	///     A query that determines which parameter to order by.
	/// </param>
	protected void ApplyFilterOrderBy(in QueryFilter entityFilter, in Expression<Func<TEntity, object>> order)
	{
		if (entityFilter.OrderBy.HasValue && entityFilter.OrderBy.Value % 2 == 0)
		{
			Specification.AddOrderByDescending(order);
		}
		else
		{
			Specification.AddOrderBy(order);
		}
	}

	/// <summary>
	///     Apply additional ordering according to filter parameter.
	/// </summary>
	/// <param name="entityFilter">
	///     A <see cref="QueryFilter"/> instance that specifies the order parameter.
	/// </param>
	/// <param name="order">
	///     A query that determines which parameter to order by.
	/// </param>
	/// <remarks>
	///     Not applicable if <see cref="ApplyFilterOrderBy"/> is not specified.
	/// </remarks>
	protected void ApplyFilterThenOrderBy(in QueryFilter entityFilter, in Expression<Func<TEntity, object>> order)
	{
		if (entityFilter.OrderBy.HasValue && entityFilter.OrderBy.Value % 2 == 0)
		{
			Specification.AddThenOrderByDescending(order);
		}
		else
		{
			Specification.AddThenOrderBy(order);
		}
	}
}
