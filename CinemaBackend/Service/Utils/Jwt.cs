//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Domain.Entities.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Service.Enums;

namespace Service.Utils;

/// <summary>
///     Service for generating Json Web Tokens.
/// </summary>
public sealed class Jwt
{
	private readonly IConfiguration _configuration;

	public Jwt(IConfiguration configuration)
	{
		_configuration = configuration;
	}

	public string CreateToken(in User user)
	{
		var jwtConfig = _configuration.GetSection("Jwt");
		var signingCredentials = GetTokenSigningCredentials(jwtConfig.GetSection("Key").Value!);
		var claims = GetClaims(user);
		var tokenOptions = GenerateTokenOptions(in jwtConfig, in signingCredentials, in claims);

		return new JwtSecurityTokenHandler().WriteToken(tokenOptions);
	}

	public static SymmetricSecurityKey GetSecret(in string key)
	{
		return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
	}

	private static JwtSecurityToken GenerateTokenOptions(
		in IConfigurationSection jwtConfig,
		in SigningCredentials signingCredentials,
		in IEnumerable<Claim> claims
	)
	{
		var token = new JwtSecurityToken(
			issuer: jwtConfig.GetSection("Issuer").Value,
			claims: claims,
			expires: DateTime.UtcNow.AddMinutes(double.Parse(jwtConfig.GetSection("Lifetime").Value!)),
			signingCredentials: signingCredentials
		);

		return token;
	}

	private static IEnumerable<Claim> GetClaims(in User user)
	{
		var claims = new List<Claim>(user.UserRoles.Count + 2)
		{
			new (JwtClaimType.Name, user.Name),
			new (JwtClaimType.Subject, user.Id.ToString())
		};

		claims.AddRange(user.UserRoles.Select(userRole
			=> new Claim(JwtClaimType.Role, userRole.Role.Name!)));

		return claims;
	}

	private static SigningCredentials GetTokenSigningCredentials(
		in string key,
		in string algorithm = SecurityAlgorithms.HmacSha512
	)
	{
		var secret = GetSecret(in key);
		return new SigningCredentials(secret, algorithm);
	}
}
