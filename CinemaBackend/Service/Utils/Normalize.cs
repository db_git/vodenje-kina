//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Text;

namespace Service.Utils;

internal static class Normalize
{
	/// <summary>
	///     Normalizes Unicode string to ASCII, replacing whitespace and other special
	///     characters with minus sign. This normalized string can safely be used inside URL.
	/// </summary>
	/// <param name="sb">
	///     <see cref="StringBuilder"/> instance.
	/// </param>
	/// <param name="text">
	///     <see cref="string"/> to normalise.
	/// </param>
	/// <param name="form">
	///     The type of <see cref="NormalizationForm">normalization</see> to perform.
	/// </param>
	public static void ToAscii(ref StringBuilder sb, ref string text, NormalizationForm form = NormalizationForm.FormKD)
	{
		foreach (var character in text.Normalize(form).Select(char.ToLowerInvariant))
		{
			if (char.IsWhiteSpace(character))
				sb.Append('-');
			else if (char.IsAscii(character) && char.IsLetterOrDigit(character))
				sb.Append(character);
		}
	}
}
