//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Service.Utils;

/// <summary>
///     Singleton instance of <see cref="Random"/> class.
/// </summary>
internal sealed class RandomNumberGenerator
{
	private static readonly object _instanceLock = new();
	private static readonly object _randNextRangeLock = new();

	private static RandomNumberGenerator? _instance;
	private readonly Random _random;

	private RandomNumberGenerator()
	{
		_random = new Random();
	}

	/// <summary>
	///     Return an instance of <see cref="RandomNumberGenerator"/> class.
	/// </summary>
	/// <returns>
	///     Instance of <see cref="RandomNumberGenerator"/>.
	/// </returns>
	public static RandomNumberGenerator GetInstance()
	{
		lock (_instanceLock) return _instance ??= new RandomNumberGenerator();
	}

	/// <summary>
	///     Return a random integer within a range.
	/// </summary>
	/// <param name="lowerBound">
	///     Inclusive lower bound.
	/// </param>
	/// <param name="upperBound">
	///     Inclusive upper bound.
	/// </param>
	/// <exception cref="ArgumentOutOfRangeException">
	///     Thrown when <paramref name="lowerBound" /> is
	///     greater than <paramref name="upperBound" />.
	/// </exception>
	/// <returns>
	///     Random integer greater than or equal to <see cref="lowerBound"/>
	///     and less than or equal to <see cref="upperBound"/>.
	/// </returns>
	public int Next(int lowerBound, int upperBound)
	{
		lock (_randNextRangeLock) return _random.Next(lowerBound, upperBound + 1);
	}
}
