//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Service;

/// <inheritdoc cref="ISouvenirQueryService" />
public sealed class SouvenirQueryService : QueryServiceBase<Souvenir, SouvenirFilter>, ISouvenirQueryService
{
	public SouvenirQueryService(IUnitOfWork unitOfWork, ISpecification<Souvenir> specification)
	{
		UnitOfWork = unitOfWork;
		Repository = UnitOfWork.SouvenirRepository;
		Specification = specification;
	}

	public override async Task<ICollection<Souvenir>> GetAllAsync(SouvenirFilter souvenirFilter)
	{
		Expression<Func<Souvenir, bool>>? filter = null;

		ApplyFilter(ref filter, ref souvenirFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(souvenirFilter);
		ApplyFilterOrderBy(souvenirFilter);

		return await Repository.GetAllAsync(Specification);
	}

	public override async Task<ICollection<TDto>> GetAllAsync<TDto>(SouvenirFilter souvenirFilter)
	{
		Expression<Func<Souvenir, bool>>? filter = null;

		ApplyFilter(ref filter, ref souvenirFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(souvenirFilter);
		ApplyFilterOrderBy(souvenirFilter);

		return await Repository.GetAllAsync<TDto>(Specification);
	}

	protected override void ApplyFilter(
		ref Expression<Func<Souvenir, bool>>? filter,
		ref SouvenirFilter souvenirFilter
	)
	{
		ApplySearchFilter(ref filter, ref souvenirFilter);
		ApplyAvailableFilter(ref filter, ref souvenirFilter);
	}

	private static void ApplySearchFilter(
		ref Expression<Func<Souvenir, bool>>? filter,
		ref SouvenirFilter souvenirFilter
	)
	{
		if (souvenirFilter.Search is null) return;

		var lowercaseSearch = souvenirFilter.Search.ToLower();
		var searchWords = lowercaseSearch.Split(' ');

		filter = souvenir => souvenir.Name.ToLower().Contains(lowercaseSearch);
		filter = searchWords.Aggregate(filter, (current, word)
			=> current.Or(souvenir => souvenir.Name.ToLower().Contains(word)));
	}

	private static void ApplyAvailableFilter(
		ref Expression<Func<Souvenir, bool>>? filter,
		ref SouvenirFilter souvenirFilter
	)
	{
		if (souvenirFilter.AvailableOnly.HasValue is false || souvenirFilter.AvailableOnly.Value is 0)
			return;

		filter = filter.And(souvenir => souvenir.AvailableQuantity > 1);
	}

	private void ApplyFilterOrderBy(in SouvenirFilter souvenirFilter)
	{
		Expression<Func<Souvenir, object>> order = souvenirFilter.OrderBy switch
		{
			(int) SouvenirOrderBy.PriceAscending => souvenir => souvenir.Price,
			(int) SouvenirOrderBy.PriceDescending => souvenir => souvenir.Price,
			(int) SouvenirOrderBy.AvailableQuantityAscending => souvenir => souvenir.AvailableQuantity,
			(int) SouvenirOrderBy.AvailableQuantityDescending => souvenir => souvenir.AvailableQuantity,
			_ => souvenir => souvenir.Name
		};

		ApplyFilterOrderBy(souvenirFilter, in order);
	}

	private Task<ICollection<TDto>> GetMonthSouvenirsAsync<TDto>(int month, int year) where TDto : class, new()
	{
		var specification = new Specification<Souvenir>();

		specification.AddFilter(s => s.SouvenirOrders.Any(o =>
			o.CreatedAt.InUtc().Month == month && o.CreatedAt.InUtc().Year == year
		));

		specification.AddOrderByDescending(s => s.SouvenirOrders.Count);
		specification.AddPagination(1, 10);

		return Repository.GetAllAsync<TDto>(specification);
	}

	public Task<ICollection<TDto>> GetCurrentMonthSouvenirsAsync<TDto>() where TDto : class, new()
	{
		var date = SystemClock.Instance.GetCurrentInstant().InUtc();
		var month = date.Month;
		var year = date.Year;

		return GetMonthSouvenirsAsync<TDto>(month, year);
	}

	public Task<ICollection<TDto>> GetPreviousMonthSouvenirsAsync<TDto>() where TDto : class, new()
	{
		var date = SystemClock.Instance.GetCurrentInstant().InUtc();
		var month = date.Month;
		var year = date.Year;

		int previousMonth, previousYear;

		if (month is 1)
		{
			previousMonth = 12;
			previousYear = year - 1;
		}
		else
		{
			previousMonth = month - 1;
			previousYear = year;
		}

		return GetMonthSouvenirsAsync<TDto>(previousMonth, previousYear);
	}
}
