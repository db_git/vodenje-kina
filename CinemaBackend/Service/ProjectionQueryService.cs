//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Service;

/// <inheritdoc cref="IProjectionQueryService" />
public sealed class ProjectionQueryService : QueryServiceBase<Projection, ProjectionFilter>, IProjectionQueryService
{
	public ProjectionQueryService(
		IUnitOfWork unitOfWork,
		ISpecification<Projection> specification
	)
	{
		UnitOfWork = unitOfWork;
		Repository = UnitOfWork.ProjectionRepository;
		Specification = specification;
	}

	public override async Task<ICollection<Projection>> GetAllAsync(ProjectionFilter projectionFilter)
	{
		Expression<Func<Projection, bool>>? filter = null;

		ApplyFilter(ref filter, ref projectionFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(projectionFilter);
		ApplyFilterOrderBy(projectionFilter);

		Specification.AddInclude(query => query
			.Include(projection => projection.Movie)
			.Include(projection => projection.Hall)
			.Include(projection => projection.Hall.Cinema)
			.Include(projection => projection.Hall.HallSeats)
			.ThenInclude(hallSeat => hallSeat.Seat)
		);

		return await Repository.GetAllAsync(Specification);
	}

	public override async Task<ICollection<TDto>> GetAllAsync<TDto>(ProjectionFilter projectionFilter)
	{
		Expression<Func<Projection, bool>>? filter = null;

		ApplyFilter(ref filter, ref projectionFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(projectionFilter);
		ApplyFilterOrderBy(projectionFilter);

		Specification.AddInclude(query => query
			.Include(projection => projection.Movie)
			.Include(projection => projection.Hall)
			.Include(projection => projection.Hall.Cinema)
			.Include(projection => projection.Hall.HallSeats)
			.ThenInclude(hallSeat => hallSeat.Seat)
		);

		return await Repository.GetAllAsync<TDto>(Specification);
	}

	public override async Task<Projection> GetAsync(Expression<Func<Projection, bool>> expression, bool withTracking = false)
	{
		var projection = await TryGetAsync(expression, withTracking);
		if (projection is null) throw new EntityNotFoundException();
		return projection;
	}

	public override async Task<TDto> GetAsync<TDto>(Expression<Func<Projection, bool>> expression)
	{
		var projectionDto = await TryGetAsync<TDto>(expression);
		if (projectionDto is null) throw new EntityNotFoundException();
		return projectionDto;
	}

	public override async Task<Projection?> TryGetAsync(Expression<Func<Projection, bool>> expression, bool withTracking = false)
	{
		Specification.AddFilter(expression);
		Specification.WithTracking(withTracking);
		Specification.AddInclude(query => query
			.Include(projection => projection.Movie)
			.Include(projection => projection.Hall)
			.Include(projection => projection.Hall.Cinema)
			.Include(projection => projection.Hall.HallSeats)
			.ThenInclude(hallSeat => hallSeat.Seat)
		);

		return await Repository.GetAsync(Specification);
	}

	public override async Task<TDto?> TryGetAsync<TDto>(Expression<Func<Projection, bool>> expression) where TDto : class
	{
		Specification.AddFilter(expression);
		Specification.AddInclude(query => query
			.Include(projection => projection.Movie)
			.Include(projection => projection.Hall)
			.Include(projection => projection.Hall.Cinema)
			.Include(projection => projection.Hall.HallSeats)
			.ThenInclude(hallSeat => hallSeat.Seat)
		);

		return await Repository.GetAsync<TDto>(Specification);
	}

	protected override void ApplyFilter(
		ref Expression<Func<Projection, bool>>? filter,
		ref ProjectionFilter projectionFilter
	)
	{
		ApplySearchFilter(ref filter, ref projectionFilter);
		ApplyMovieFilter(ref filter, ref projectionFilter);
		ApplyHallFilter(ref filter, ref projectionFilter);
		ApplyTimeFilter(ref filter, ref projectionFilter);
	}

	private static void ApplySearchFilter(
		ref Expression<Func<Projection, bool>>? filter,
		ref ProjectionFilter projectionFilter
	)
	{
		if (projectionFilter.Search == null) return;

		var lowercaseSearch = projectionFilter.Search.ToLower();
		var searchWords = lowercaseSearch.Split(' ');

		filter = projection => projection.Movie.Title.ToLower().Contains(lowercaseSearch);
		filter = filter.Or(projection => projection.Hall.Name.ToLower().Contains(lowercaseSearch));

		filter = searchWords.Aggregate(filter, (current, word)
			=> current.Or(projection => projection.Movie.Title.ToLower().Contains(word)));
		filter = searchWords.Aggregate(filter, (current, word)
			=> current.Or(projection => projection.Hall.Name.ToLower().Contains(word)));
	}

	private static void ApplyMovieFilter(
		ref Expression<Func<Projection, bool>>? filter,
		ref ProjectionFilter projectionFilter
	)
	{
		var movieId = projectionFilter.Movie;
		if (movieId is null) return;

		filter = filter.And(projection => projection.Movie.Id == movieId);
	}

	private static void ApplyHallFilter(
		ref Expression<Func<Projection, bool>>? filter,
		ref ProjectionFilter projectionFilter
	)
	{
		var hallId = projectionFilter.Hall;
		if (hallId is null || hallId == Guid.Empty) return;

		filter = filter.And(projection => projection.Hall.Id == hallId);
	}

	private static void ApplyTimeFilter(
		ref Expression<Func<Projection, bool>>? filter,
		ref ProjectionFilter projectionFilter
	)
	{
		var startDate = projectionFilter.StartDate;
		var endDate = projectionFilter.EndDate;
		if (!startDate.HasValue || !endDate.HasValue) return;

		filter = filter
			.And(projection => projection.Time.Date.CompareTo(startDate.Value) >= 0)
			.And(projection => projection.Time.Date.CompareTo(endDate.Value) <= 0);
	}

	private void ApplyFilterOrderBy(in QueryFilter projectionFilter)
	{
		Expression<Func<Projection, object>> order = projectionFilter.OrderBy switch
		{
			(int) ProjectionOrderBy.MovieTitleAscending => projection => projection.Movie.Title,
			(int) ProjectionOrderBy.MovieTitleDescending => projection => projection.Movie.Title,
			(int) ProjectionOrderBy.HallNameAscending => projection => projection.Hall.Name,
			(int) ProjectionOrderBy.HallNameDescending => projection => projection.Hall.Name,
			_ => projection => projection.Time
		};

		ApplyFilterOrderBy(in projectionFilter, in order);
	}
}
