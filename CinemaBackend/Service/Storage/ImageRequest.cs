//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Service.Contracts.Storage;

namespace Service.Storage;

/// <inheritdoc />
public sealed record ImageRequest : IImageRequest
{
	private readonly int _width;
	private readonly int _height;
	private readonly int _quality = 90;

	public required string Name { get; init; }
	public required string Path { get; init; }
	public required string Image { get; init; }

	public required int Width
	{
		get => _width;
		init => _width = value switch
			{
				> 1920 => 1920,
				< 0 => 420,
				_ => value
			};
	}

	public required int Height
	{
		get => _height;
		init => _height = value switch
			{
				> 1080 => 1080,
				< 0 => 630,
				_ => value
			};
	}

	public int Quality
	{
		get => _quality;
		init
		{
			if (value is > 0 and <= 100) _quality = value;
		}
	}
}
