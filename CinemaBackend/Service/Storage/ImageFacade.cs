//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Imagekit.Helper;
using Imagekit.Models;
using Imagekit.Sdk;
using ImageMagick;
using Microsoft.Extensions.Logging;
using Service.Contracts.Storage;

namespace Service.Storage;

/// <inheritdoc />
public sealed class ImageFacade : IImageFacade
{
	private const string DeleteFailureLogMessage =
		"Failed to delete image with id='{ImageId}'. Manual action is needed";

	private readonly ImagekitClient _imagekit;
	private readonly ILogger<ImageFacade> _logger;

	public ImageFacade(ImagekitClient imagekit, ILogger<ImageFacade> logger)
	{
		_imagekit = imagekit;
		_logger = logger;
	}

	public async Task<Result> UploadImageAsync(IImageRequest request)
	{
		var imageHeader = request.Image[..10];

		var image = imageHeader.Contains("https://") || imageHeader.Contains("http://")
			? request.Image
			: MagickConvert(request);

		try
		{
			return await _imagekit.UploadAsync(new FileCreateRequest
			{
				file = image,
				fileName = request.Name,
				folder = request.Path,
				useUniqueFileName = true
			});
		}
		catch (Exception e)
		{
			throw new ImagekitException("Failed to save image.", e);
		}
	}

	public async Task<ResultDelete> DeleteImageAsync(string imageId)
	{
		try
		{
			return await _imagekit.DeleteFileAsync(imageId);
		}
		catch (Exception e)
		{
			throw new ImagekitException("Failed to delete image.", e);
		}
	}

	public async Task<ResultDelete?> TryDeleteImageAsync(string imageId)
	{
		try
		{
			return await DeleteImageAsync(imageId);
		}
		catch (ImagekitException e)
		{
			_logger.LogError(e, DeleteFailureLogMessage, imageId);
		}

		return null;
	}

	public async Task<ResultNoContent> DeleteDirectoryAsync(string directoryPath)
	{
		try
		{
			return await _imagekit.DeleteFolderAsync(new DeleteFolderRequest
			{
				folderPath = directoryPath
			});
		}
		catch (Exception e)
		{
			throw new ImagekitException("Failed to delete folder.", e);
		}
	}

	private static byte[] ConvertFromBase64(in string value)
	{
		try
		{
			return Convert.FromBase64String(value);
		}
		catch
		{
			throw new MagickErrorException("Failed to convert from base64.");
		}
	}

	private static string MagickConvert(in IImageRequest request)
	{
		var bytes = ConvertFromBase64(request.Image);

		using var magickImage = new MagickImage(bytes);
		magickImage.Format = MagickFormat.WebP;
		magickImage.Quality = request.Quality;
		magickImage.Thumbnail(new MagickGeometry(request.Width, request.Height));

		return magickImage.ToBase64();
	}
}
