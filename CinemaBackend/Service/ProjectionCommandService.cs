//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Service;

/// <inheritdoc cref="IProjectionCommandService" />
public sealed class ProjectionCommandService : CommandServiceBase<Projection>, IProjectionCommandService
{
	private readonly IMovieQueryService _movieQueryService;
	private readonly IProjectionQueryService _queryService;

	public ProjectionCommandService(
		IClock clock,
		IUnitOfWork unitOfWork,
		IMovieQueryService movieService,
		IProjectionQueryService queryService
	)
	{
		Clock = clock;
		UnitOfWork = unitOfWork;
		Repository = UnitOfWork.ProjectionRepository;
		_movieQueryService = movieService;
		_queryService = queryService;
	}

	/// <inheritdoc cref="IProjectionCommandService.AddAsync" />
	public override async Task AddAsync(Projection projection)
	{
		await CanAddProjection(projection);
		await base.AddAsync(projection);
	}

	/// <inheritdoc cref="IProjectionCommandService.AddRangeAsync" />
	public override async Task AddRangeAsync(IEnumerable<Projection> projections)
	{
		var projectionsArray = projections.ToArray();
		foreach (var projection in projectionsArray) await CanAddProjection(projection);
		await base.AddRangeAsync(projectionsArray);
	}

	/// <inheritdoc cref="IProjectionCommandService.UpdateAsync" />
	public override async Task UpdateAsync(Projection projection)
	{
		await CanAddProjection(projection);
		await base.UpdateAsync(projection);
	}

	private async Task CanAddProjection(Projection projection)
	{
		var movie = await _movieQueryService.GetAsync(movie => movie.Id == projection.MovieId);

		var overlapStart = projection.Time;
		var overlapEnd = projection.Time.PlusMinutes(movie.Duration);

		Expression<Func<Projection, bool>> filter = p => p.Time < overlapEnd;
		filter = filter.And(p => p.Time + Period.FromMinutes(movie.Duration) > overlapStart);
		filter = filter.And(p => p.HallId == projection.HallId);

		var projectionsCount = await _queryService.CountAsync(filter);
		if (projectionsCount is not 0) throw new ProjectionOverlapException();
	}
}
