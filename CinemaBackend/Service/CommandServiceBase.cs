//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Domain;

namespace Service;

/// <inheritdoc />
public abstract class CommandServiceBase<TEntity> : ICommandServiceBase<TEntity> where TEntity : class, IEntity
{
	protected IUnitOfWork UnitOfWork { get; init; } = null!;
	protected IRepositoryBase<TEntity> Repository { get; init; } = null!;
	protected IClock Clock { get; init; } = null!;

	/// <inheritdoc />
	public virtual async Task AddAsync(TEntity entity)
	{
		entity.CreatedAt = Clock.GetCurrentInstant();
		entity.ModifiedAt = entity.CreatedAt;

		Repository.Add(entity);
		await UnitOfWork.SaveAsync();
	}

	/// <inheritdoc />
	public virtual async Task AddRangeAsync(IEnumerable<TEntity> entities)
	{
		var entitiesArray = entities.ToArray();
		foreach (var entity in entitiesArray)
		{
			entity.CreatedAt = Clock.GetCurrentInstant();
			entity.ModifiedAt = entity.CreatedAt;
		}

		Repository.AddRange(entitiesArray);
		await UnitOfWork.SaveAsync();
	}

	/// <inheritdoc />
	public virtual async Task UpdateAsync(TEntity entity)
	{
		entity.ModifiedAt = Clock.GetCurrentInstant();

		Repository.Update(entity);
		await UnitOfWork.SaveAsync();
	}

	/// <inheritdoc />
	public virtual async Task DeleteAsync(TEntity entity)
	{
		Repository.Delete(entity);
		await UnitOfWork.SaveAsync();
	}

	/// <inheritdoc />
	public virtual async Task DeleteRangeAsync(IEnumerable<TEntity> entities)
	{
		Repository.DeleteRange(entities);
		await UnitOfWork.SaveAsync();
	}
}
