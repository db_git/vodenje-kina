//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Service;

/// <inheritdoc cref="ISeatQueryService" />
public sealed class SeatQueryService : QueryServiceBase<Seat, SeatFilter>, ISeatQueryService
{
	public SeatQueryService(IUnitOfWork unitOfWork, ISpecification<Seat> specification)
	{
		UnitOfWork = unitOfWork;
		Repository = UnitOfWork.SeatRepository;
		Specification = specification;
	}

	public override async Task<ICollection<Seat>> GetAllAsync(SeatFilter seatFilter)
	{
		Expression<Func<Seat, bool>>? filter = null;

		ApplyFilter(ref filter, ref seatFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(seatFilter);
		ApplyFilterOrderBy(seatFilter);

		return await Repository.GetAllAsync(Specification);
	}

	public override async Task<ICollection<TDto>> GetAllAsync<TDto>(SeatFilter seatFilter)
	{
		Expression<Func<Seat, bool>>? filter = null;

		ApplyFilter(ref filter, ref seatFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(seatFilter);
		ApplyFilterOrderBy(seatFilter);

		return await Repository.GetAllAsync<TDto>(Specification);
	}

	public override async Task<Seat> GetAsync(Expression<Func<Seat, bool>> expression, bool withTracking = false)
	{
		var seat = await TryGetAsync(expression, withTracking);
		if (seat is null) throw new EntityNotFoundException();
		return seat;
	}

	public override async Task<TDto> GetAsync<TDto>(Expression<Func<Seat, bool>> expression)
	{
		var seatDto = await TryGetAsync<TDto>(expression);
		if (seatDto is null) throw new EntityNotFoundException();
		return seatDto;
	}

	public override async Task<Seat?> TryGetAsync(Expression<Func<Seat, bool>> expression, bool withTracking = false)
	{
		Specification.AddFilter(expression);
		Specification.WithTracking(withTracking);
		Specification.AddInclude(query => query
			.Include(seat => seat.HallSeats)
			.ThenInclude(hallSeat => hallSeat.Hall)
		);

		return await Repository.GetAsync(Specification);
	}

	public override async Task<TDto?> TryGetAsync<TDto>(Expression<Func<Seat, bool>> expression) where TDto : class
	{
		Specification.AddFilter(expression);
		Specification.AddInclude(query => query
			.Include(seat => seat.HallSeats)
			.ThenInclude(hallSeat => hallSeat.Hall)
		);

		return await Repository.GetAsync<TDto>(Specification);
	}

	public async Task<IEnumerable<TDto>> GetTakenSeatsAsync<TDto>(Guid projectionId) where TDto : class, new()
	{
		Expression<Func<Seat, bool>>? filter = null;

		filter = filter
				.And(s => s.ReservationSeats
					.Any(rs => rs.Reservation.ProjectionId == projectionId))
				.And(s => s.ReservationSeats
					.Any(rs => rs.SeatId == s.Id));

		Specification.AddFilter(filter);

		return await Repository.GetAllAsync<TDto>(Specification);
	}

	public async Task<IEnumerable<Seat>> GetTakenSeatsAsync(Guid projectionId)
	{
		Expression<Func<Seat, bool>>? filter = null;

		filter = filter
			.And(s => s.ReservationSeats
				.Any(rs => rs.Reservation.ProjectionId == projectionId))
			.And(s => s.ReservationSeats
				.Any(rs => rs.SeatId == s.Id));

		Specification.AddFilter(filter);

		return await Repository.GetAllAsync(Specification);
	}

	protected override void ApplyFilter(
		ref Expression<Func<Seat, bool>>? filter,
		ref SeatFilter seatFilter
	)
	{
		ApplySearchFilter(ref filter, ref seatFilter);
		ApplyHallFilter(ref filter, ref seatFilter);
	}

	private static void ApplySearchFilter(
		ref Expression<Func<Seat, bool>>? filter,
		ref SeatFilter seatFilter
	)
	{
		if (seatFilter.Search is null) return;

		var lowercaseSearch = seatFilter.Search.ToLower();
		var searchWords = lowercaseSearch.Split(' ');

		filter = seat => seat.Number.ToString().Contains(lowercaseSearch);

		filter = searchWords.Aggregate(filter, (current, word)
			=> current.Or(seat => seat.Number.ToString().Contains(word)));
		filter = searchWords.Aggregate(filter, (current, word)
			=> current.Or(seat => seat.HallSeats.Any(hallSeat
				=> hallSeat.Hall.Name.ToLower().Contains(word))));
	}

	private static void ApplyHallFilter(
		ref Expression<Func<Seat, bool>>? filter,
		ref SeatFilter seatFilter
	)
	{
		if (!seatFilter.Hall.HasValue) return;

		var hallId = seatFilter.Hall.Value;
		filter = filter.And(seat => seat.HallSeats.Any(hs => hs.HallId == hallId));
	}

	private void ApplyFilterOrderBy(in SeatFilter seatFilter)
	{
		Expression<Func<Seat, object>> order = seat => seat.Number;
		ApplyFilterOrderBy(seatFilter, in order);
	}
}
