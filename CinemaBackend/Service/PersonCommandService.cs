//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Text;

namespace Service;

/// <inheritdoc cref="IPersonCommandService" />
public sealed class PersonCommandService : CommandServiceBase<Person>, IPersonCommandService
{
	public PersonCommandService(IClock clock, IUnitOfWork unitOfWork)
	{
		Clock = clock;
		UnitOfWork = unitOfWork;
		Repository = UnitOfWork.PersonRepository;
	}

	public override async Task AddAsync(Person person)
	{
		// ReSharper disable once NullCoalescingConditionIsAlwaysNotNullAccordingToAPIContract
		person.Id ??= await GenerateIdFromName(person.Name);
		await base.AddAsync(person);
	}

	public override async Task AddRangeAsync(IEnumerable<Person> people)
	{
		var peopleArray = people.ToArray();

		// ReSharper disable once ConditionIsAlwaysTrueOrFalseAccordingToNullableAPIContract
		foreach (var person in peopleArray.Where(p => p.Id is null))
		{
			person.Id = await GenerateIdFromName(person.Name);
		}

		Repository.AddRange(peopleArray);
		await UnitOfWork.SaveAsync();
	}

	public async Task<string> GenerateIdFromName(string name)
	{
		var rng = RandomNumberGenerator.GetInstance();
		var number = rng.Next(1000, 99999);

		var sb = new StringBuilder(name.Length);
		Normalize.ToAscii(ref sb, ref name);
		sb.Append('-').Append(number);

		while (true)
		{
			var person = await Repository.CountAsync(person => person.Id == sb.ToString());
			if (person == 0) break;

			number = rng.Next(100, 99999);
			sb.Length -= (int) Math.Floor(Math.Log10(number) + 1);

			sb.Append(number);
		}

		return sb.ToString();
	}
}
