//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

global using System.Linq.Expressions;
global using Domain.Entities;
global using NodaTime;
global using Microsoft.EntityFrameworkCore;
global using Repository.Contracts;
global using Repository.Utils;
global using Service.Contracts;
global using Service.Contracts.Filters;
global using Service.Exceptions;
global using Service.OrderBy;
global using Service.Utils;
