//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Service;

/// <inheritdoc cref="IUserCommentQueryService"/>
public sealed class UserCommentQueryService : QueryServiceBase<UserComment, UserCommentFilter>, IUserCommentQueryService
{
	public UserCommentQueryService(IUnitOfWork unitOfWork, ISpecification<UserComment> specification)
	{
		UnitOfWork = unitOfWork;
		Repository = UnitOfWork.UserCommentRepository;
		Specification = specification;
	}

	public override async Task<ICollection<UserComment>> GetAllAsync(UserCommentFilter commentFilter)
	{
		Expression<Func<UserComment, bool>>? filter = null;

		ApplyFilter(ref filter, ref commentFilter);
		ApplyFilterPagination(commentFilter);
		if (filter != null) Specification.AddFilter(filter);

		Specification.AddOrderByDescending(comment => comment.CreatedAt);
		Specification.AddInclude(query => query
			.Include(comment => comment.Movie)
			.Include(comment => comment.User)
		);

		return await Repository.GetAllAsync(Specification);
	}

	public override async Task<ICollection<TDto>> GetAllAsync<TDto>(UserCommentFilter commentFilter)
	{
		Expression<Func<UserComment, bool>>? filter = null;

		ApplyFilter(ref filter, ref commentFilter);
		ApplyFilterPagination(commentFilter);
		if (filter != null) Specification.AddFilter(filter);

		Specification.AddOrderByDescending(comment => comment.CreatedAt);
		Specification.AddInclude(query => query
			.Include(comment => comment.Movie)
			.Include(comment => comment.User)
		);

		return await Repository.GetAllAsync<TDto>(Specification);
	}

	protected override void ApplyFilter(
		ref Expression<Func<UserComment, bool>>? filter,
		ref UserCommentFilter commentFilter
	)
	{
		ApplySearchFilter(ref filter, ref commentFilter);
		ApplyMovieFilter(ref filter, ref commentFilter);
	}

	private static void ApplySearchFilter(
		ref Expression<Func<UserComment, bool>>? filter,
		ref UserCommentFilter commentFilter
	)
	{
		if (commentFilter.Search == null) return;

		var lowercaseSearch = commentFilter.Search.ToLower();
		var searchWords = lowercaseSearch.Split(' ');

		filter = comment => comment.Comment.ToLower().Contains(lowercaseSearch);
		filter = comment => comment.Movie.Title.ToLower().Contains(lowercaseSearch);

		filter = searchWords.Aggregate(filter, (current, word)
			=> current.Or(comment => comment.Comment.ToLower().Contains(word)));
		filter = searchWords.Aggregate(filter, (current, word)
			=> current.Or(comment => comment.Movie.Title.ToLower().Contains(word)));
	}

	private static void ApplyMovieFilter(
		ref Expression<Func<UserComment, bool>>? filter,
		ref UserCommentFilter commentFilter
	)
	{
		var movieId = commentFilter.Movie;
		if (movieId is null) return;

		filter = filter.And(comment => comment.MovieId == movieId);
	}
}
