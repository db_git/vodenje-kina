//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Service.Contracts.Email;

namespace Service;

/// <inheritdoc cref="IReservationCommandService" />
public sealed class ReservationCommandService : CommandServiceBase<Reservation>, IReservationCommandService
{
	private readonly IEmailSender _emailSender;
	private readonly IFoodRepository _foodRepository;
	private readonly ISouvenirRepository _souvenirRepository;

	private readonly IFoodQueryService _foodQueryService;
	private readonly IProjectionQueryService _projectionQueryService;
	private readonly ISeatQueryService _seatQueryService;
	private readonly ISouvenirQueryService _souvenirQueryService;
	private readonly IUserQueryService _userQueryService;

	public ReservationCommandService(
		IClock clock,
		IUnitOfWork unitOfWork,
		IEmailSender emailSender,
		IProjectionQueryService projectionQueryService,
		IFoodQueryService foodQueryService,
		ISeatQueryService seatQueryService,
		ISouvenirQueryService souvenirQueryService,
		IUserQueryService userQueryService
	)
	{
		Clock = clock;
		UnitOfWork = unitOfWork;
		Repository = UnitOfWork.ReservationRepository;

		_emailSender = emailSender;

		_foodRepository = UnitOfWork.FoodRepository;
		_souvenirRepository = UnitOfWork.SouvenirRepository;

		_foodQueryService = foodQueryService;
		_projectionQueryService = projectionQueryService;
		_seatQueryService = seatQueryService;
		_souvenirQueryService = souvenirQueryService;
		_userQueryService = userQueryService;
	}

	private async Task CanAddReservation(Reservation reservation)
	{
		var now = Clock.GetCurrentInstant().InUtc().LocalDateTime;

		var projectionCount = await _projectionQueryService.CountAsync(
			p => p.Id == reservation.ProjectionId &&
			     now.CompareTo(p.Time + Period.FromMinutes(20)) < 0
		);

		if (projectionCount is 0) throw new ProjectionTimeException();

		var takenSeats = (await _seatQueryService.GetTakenSeatsAsync(reservation.ProjectionId)).ToArray();

		var hasTakenSeats = reservation.ReservationSeats
			.Select(reservationSeat => reservationSeat.SeatId)
			.Intersect(takenSeats.Select(seat => seat.Id))
			.Any();

		if (hasTakenSeats)
		{
			var seats = takenSeats
				.Where(s => reservation.ReservationSeats.Any(rs => rs.SeatId == s.Id))
				.Select(s => s.Number)
				.Order();

			var seatNumbers = string.Join(", ", seats);

			throw new SeatTakenException($"Following seats are taken: {seatNumbers}.");
		}

		foreach (var order in reservation.FoodOrders)
		{
			var food = await _foodQueryService.GetAsync(f => f.Id == order.FoodId);
			if (food.AvailableQuantity < 10)
				throw new OutOfStockException($"{food.Name} is out of stock.");
		}
		foreach (var order in reservation.SouvenirOrders)
		{
			var souvenir = await _souvenirQueryService.GetAsync(s => s.Id == order.SouvenirId);
			if (souvenir.AvailableQuantity < 10)
				throw new OutOfStockException($"{souvenir.Name} is out of stock.");
		}
	}

	private async Task CalculateNewQuantities(Reservation reservation)
	{
		var foods = new List<Food>(reservation.FoodOrders.Count);
		foreach (var order in reservation.FoodOrders)
		{
			var food = await _foodQueryService
				.GetAsync(f => f.Id == order.FoodId, true);

			food.AvailableQuantity -= order.OrderedQuantity;
			food.ModifiedAt = Clock.GetCurrentInstant();
			foods.Add(food);
		}

		var souvenirs = new List<Souvenir>(reservation.SouvenirOrders.Count);
		foreach (var order in reservation.SouvenirOrders)
		{
			var souvenir = await _souvenirQueryService
				.GetAsync(s => s.Id == order.SouvenirId, true);

			souvenir.AvailableQuantity -= order.OrderedQuantity;
			souvenir.ModifiedAt = Clock.GetCurrentInstant();
			souvenirs.Add(souvenir);
		}

		_foodRepository.UpdateRange(foods);
		_souvenirRepository.UpdateRange(souvenirs);
	}

	/// <inheritdoc cref="IReservationCommandService.AddAsync" />
	public override async Task AddAsync(Reservation reservation)
	{
		await CanAddReservation(reservation);
		await CalculateNewQuantities(reservation);

		reservation.CreatedAt = Clock.GetCurrentInstant();
		reservation.ModifiedAt = reservation.CreatedAt;

		Repository.Add(reservation);
		await UnitOfWork.SaveAsync();
		await _emailSender.SendReservationDetailsAsync(
			await _userQueryService.GetAsync(user => user.Id == reservation.UserId),
			new List<string> { reservation.Id.ToString() }
		);
	}

	/// <inheritdoc cref="IReservationCommandService.AddRangeAsync" />
	public override async Task AddRangeAsync(IEnumerable<Reservation> reservations)
	{
		var reservationsArray = reservations.ToArray();
		foreach (var reservation in reservationsArray) await CanAddReservation(reservation);

		foreach (var reservation in reservationsArray)
		{
			await CalculateNewQuantities(reservation);
			reservation.CreatedAt = Clock.GetCurrentInstant();
			reservation.ModifiedAt = reservation.CreatedAt;
		}

		Repository.AddRange(reservationsArray);
		await UnitOfWork.SaveAsync();
		await _emailSender.SendReservationDetailsAsync(
			await _userQueryService.GetAsync(user => user.Id == reservationsArray.First().UserId),
			reservationsArray.Select(reservation => reservation.Id.ToString())
		);
	}
}
