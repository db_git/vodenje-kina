//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Service;

/// <inheritdoc cref="IGenreQueryService" />
public sealed class GenreQueryService : QueryServiceBase<Genre, QueryFilter>, IGenreQueryService
{
	public GenreQueryService(IUnitOfWork unitOfWork, ISpecification<Genre> specification)
	{
		UnitOfWork = unitOfWork;
		Repository = UnitOfWork.GenreRepository;
		Specification = specification;
	}

	public override async Task<ICollection<Genre>> GetAllAsync(QueryFilter genreFilter)
	{
		Expression<Func<Genre, bool>>? filter = null;

		ApplyFilter(ref filter, ref genreFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(genreFilter);
		ApplyFilterOrderBy(genreFilter);

		return await Repository.GetAllAsync(Specification);
	}

	public override async Task<ICollection<TDto>> GetAllAsync<TDto>(QueryFilter genreFilter)
	{
		Expression<Func<Genre, bool>>? filter = null;

		ApplyFilter(ref filter, ref genreFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(genreFilter);
		ApplyFilterOrderBy(genreFilter);

		return await Repository.GetAllAsync<TDto>(Specification);
	}

	protected override void ApplyFilter(
		ref Expression<Func<Genre, bool>>? filter,
		ref QueryFilter genreFilter
	)
	{
		if (genreFilter.Search == null) return;

		var lowercaseSearch = genreFilter.Search.ToLower();
		var searchWords = lowercaseSearch.Split(' ');

		filter = genre => genre.Name.ToLower().Contains(lowercaseSearch);
		filter = searchWords.Aggregate(filter, (current, word)
			=> current.Or(genre => genre.Name.ToLower().Contains(word)));
	}

	private void ApplyFilterOrderBy(in QueryFilter genreFilter)
	{
		Expression<Func<Genre, object>> order = genre => genre.Name;
		ApplyFilterOrderBy(in genreFilter, in order);
	}
}
