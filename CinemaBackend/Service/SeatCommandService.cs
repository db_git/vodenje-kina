//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Service;

/// <inheritdoc cref="ISeatCommandService" />
public sealed class SeatCommandService : CommandServiceBase<Seat>, ISeatCommandService
{
	private readonly IHallQueryService _hallQueryService;

	public SeatCommandService(IClock clock, IUnitOfWork unitOfWork, IHallQueryService hallQueryService)
	{
		Clock = clock;
		UnitOfWork = unitOfWork;
		Repository = UnitOfWork.SeatRepository;
		_hallQueryService = hallQueryService;
	}

	/// <inheritdoc cref="ISeatCommandService.AddAsync" />
	public override async Task AddAsync(Seat seat)
	{
		await CanAddSeat(seat);
		await base.AddAsync(seat);
	}

	/// <inheritdoc cref="ISeatCommandService.AddRangeAsync" />
	public override async Task AddRangeAsync(IEnumerable<Seat> seats)
	{
		var seatsArray = seats.ToArray();
		foreach (var seat in seatsArray) await CanAddSeat(seat);
		await base.AddRangeAsync(seatsArray);
	}

	/// <inheritdoc cref="ISeatCommandService.UpdateAsync" />
	public override async Task UpdateAsync(Seat seat)
	{
		await CanUpdateSeat(seat);
		await base.UpdateAsync(seat);
	}

	private async Task CanAddSeat(Seat seat)
	{
		var seatsCount = await Repository.CountAsync(s => s.Number == seat.Number);
		if (seatsCount is not 0)
			throw new DuplicateEntityException($"Seat '{seat.Number}' already exists!");

		foreach (var hallId in seat.HallSeats.Select(hs => hs.HallId))
		{
			var hallsCount = await _hallQueryService.CountAsync(h => h.Id == hallId);
			if (hallsCount is 0)
				throw new EntityNotFoundException($"Hall with id '{hallId}' could not be found");
		}
	}

	private async Task CanUpdateSeat(Seat seat)
	{
		var existingSeat = await Repository.CountAsync(s => s.Number == seat.Number && s.Id == seat.Id);
		if (existingSeat is not 0) return;

		await CanAddSeat(seat);
	}
}
