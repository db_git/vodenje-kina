//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Text;

namespace Service;

/// <inheritdoc cref="IMovieCommandService" />
public sealed class MovieCommandService : CommandServiceBase<Movie>, IMovieCommandService
{
	public MovieCommandService(IClock clock, IUnitOfWork unitOfWork)
	{
		Clock = clock;
		UnitOfWork = unitOfWork;
		Repository = UnitOfWork.MovieRepository;
	}

	public override async Task AddAsync(Movie movie)
	{
		// ReSharper disable once NullCoalescingConditionIsAlwaysNotNullAccordingToAPIContract
		movie.Id ??= await GenerateIdFromTitle(movie.Title);
		await base.AddAsync(movie);
	}

	public override async Task AddRangeAsync(IEnumerable<Movie> movies)
	{
		var moviesArray = movies.ToArray();

		// ReSharper disable once ConditionIsAlwaysTrueOrFalseAccordingToNullableAPIContract
		foreach (var movie in moviesArray.Where(m => m.Id is null))
		{
			movie.Id = await GenerateIdFromTitle(movie.Title);
		}

		Repository.AddRange(moviesArray);
		await UnitOfWork.SaveAsync();
	}

	public async Task<string> GenerateIdFromTitle(string title)
	{
		var rng = RandomNumberGenerator.GetInstance();
		var number = rng.Next(100, 999);

		var sb = new StringBuilder(title.Length);
		Normalize.ToAscii(ref sb, ref title);
		sb.Append('-').Append(number);

		while (true)
		{
			var movie = await Repository.CountAsync(movie => movie.Id == sb.ToString());
			if (movie == 0) break;

			number = rng.Next(100, 99999);
			sb.Length -= (int) Math.Floor(Math.Log10(number) + 1);

			sb.Append(number);
		}

		return sb.ToString();
	}
}
