//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Service;

/// <inheritdoc cref="IPersonQueryService" />
public sealed class PersonQueryService : QueryServiceBase<Person, PersonFilter>, IPersonQueryService
{
	public PersonQueryService(IUnitOfWork unitOfWork, ISpecification<Person> specification)
	{
		UnitOfWork = unitOfWork;
		Repository = UnitOfWork.PersonRepository;
		Specification = specification;
	}

	public override async Task<ICollection<Person>> GetAllAsync(PersonFilter personFilter)
	{
		Expression<Func<Person, bool>>? filter = null;

		ApplyFilter(ref filter, ref personFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(personFilter);
		ApplyFilterOrderBy(personFilter);

		Specification.AddInclude(query => query
			.Include(person => person.MoviePeople)
			.ThenInclude(moviePeople => moviePeople.Movie)
		);

		return await Repository.GetAllAsync(Specification);
	}

	public override async Task<ICollection<TDto>> GetAllAsync<TDto>(PersonFilter personFilter)
	{
		Expression<Func<Person, bool>>? filter = null;

		ApplyFilter(ref filter, ref personFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(personFilter);
		ApplyFilterOrderBy(personFilter);

		Specification.AddInclude(query => query
			.Include(person => person.MoviePeople)
			.ThenInclude(moviePeople => moviePeople.Movie)
		);

		return await Repository.GetAllAsync<TDto>(Specification);
	}

	public override async Task<Person> GetAsync(Expression<Func<Person, bool>> expression, bool withTracking = false)
	{
		var person = await TryGetAsync(expression, withTracking);
		if (person is null) throw new EntityNotFoundException();
		return person;
	}

	public override async Task<TDto> GetAsync<TDto>(Expression<Func<Person, bool>> expression)
	{
		var personDto = await TryGetAsync<TDto>(expression);
		if (personDto is null) throw new EntityNotFoundException();
		return personDto;
	}

	public override async Task<Person?> TryGetAsync(Expression<Func<Person, bool>> expression, bool withTracking = false)
	{
		Specification.AddFilter(expression);
		Specification.WithTracking(withTracking);
		Specification.AddInclude(query => query
			.Include(person =>
				person.MoviePeople.OrderByDescending(mp => mp.Movie.Year))
			.ThenInclude(moviePeople => moviePeople.Movie)
		);

		return await Repository.GetAsync(Specification);
	}

	public override async Task<TDto?> TryGetAsync<TDto>(Expression<Func<Person, bool>> expression) where TDto : class
	{
		Specification.AddFilter(expression);
		Specification.AddInclude(query => query
			.Include(person =>
				person.MoviePeople.OrderByDescending(mp => mp.Movie.Year))
			.ThenInclude(moviePeople => moviePeople.Movie)
		);

		return await Repository.GetAsync<TDto>(Specification);
	}

	private static void ApplySearchFilter(
		ref Expression<Func<Person, bool>>? filter,
		in PersonFilter personFilter
	)
	{
		if (personFilter.Search == null) return;

		var lowercaseSearch = personFilter.Search.ToLower();
		var searchWords = lowercaseSearch.Split(' ');

		filter = person => person.Name.ToLower().Contains(lowercaseSearch);
		filter = searchWords.Aggregate(filter, (current, word)
			=> current.Or(person => person.Name.ToLower().Contains(word)));
	}

	private static void ApplyDateFilter(
		ref Expression<Func<Person, bool>>? filter,
		in PersonFilter personFilter
	)
	{
		if (personFilter.Day is null || personFilter.Month is null) return;

		var day = personFilter.Day.Value;
		var month = personFilter.Month.Value;

		filter = filter.And(person => person.DateOfBirth.Day == day);
		filter = filter.And(person => person.DateOfBirth.Month == month);
	}

	protected override void ApplyFilter(
		ref Expression<Func<Person, bool>>? filter,
		ref PersonFilter personFilter
	)
	{
		ApplySearchFilter(ref filter, in personFilter);
		ApplyDateFilter(ref filter, in personFilter);
	}

	private void ApplyFilterOrderBy(in QueryFilter personFilter)
	{
		Expression<Func<Person, object>> order = person => person.Name;
		ApplyFilterOrderBy(in personFilter, in order);
	}
}
