//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Service;

/// <inheritdoc cref="IFilmRatingQueryService" />
public sealed class FilmRatingQueryService : QueryServiceBase<FilmRating, QueryFilter>, IFilmRatingQueryService
{
	public FilmRatingQueryService(IUnitOfWork unitOfWork, ISpecification<FilmRating> specification)
	{
		UnitOfWork = unitOfWork;
		Repository = UnitOfWork.FilmRatingRepository;
		Specification = specification;
	}

	public override async Task<ICollection<FilmRating>> GetAllAsync(QueryFilter filmRatingFilter)
	{
		Expression<Func<FilmRating, bool>>? filter = null;

		ApplyFilter(ref filter, ref filmRatingFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(filmRatingFilter);
		ApplyFilterOrderBy(filmRatingFilter);

		return await Repository.GetAllAsync(Specification);
	}

	public override async Task<ICollection<TDto>> GetAllAsync<TDto>(QueryFilter filmRatingFilter)
	{
		Expression<Func<FilmRating, bool>>? filter = null;

		ApplyFilter(ref filter, ref filmRatingFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(filmRatingFilter);
		ApplyFilterOrderBy(filmRatingFilter);

		return await Repository.GetAllAsync<TDto>(Specification);
	}

	protected override void ApplyFilter(
		ref Expression<Func<FilmRating, bool>>? filter,
		ref QueryFilter filmRatingFilter
	)
	{
		if (filmRatingFilter.Search == null) return;

		var lowercaseSearch = filmRatingFilter.Search.ToLower();
		var searchWords = lowercaseSearch.Split(' ');

		filter = filmRating => filmRating.Type.ToLower().Contains(lowercaseSearch);
		filter = searchWords.Aggregate(filter, (current, word)
			=> current.Or(filmRating => filmRating.Type.ToLower().Contains(word)));
	}

	private void ApplyFilterOrderBy(in QueryFilter filmRatingFilter)
	{
		Expression<Func<FilmRating, object>> order = filmRating => filmRating.Type;
		ApplyFilterOrderBy(in filmRatingFilter, in order);
	}
}
