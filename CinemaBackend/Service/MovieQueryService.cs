//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Service;

/// <inheritdoc cref="IMovieQueryService" />
public sealed class MovieQueryService : QueryServiceBase<Movie, MovieFilter>, IMovieQueryService
{
	public MovieQueryService(IUnitOfWork unitOfWork, ISpecification<Movie> specification)
	{
		UnitOfWork = unitOfWork;
		Repository = UnitOfWork.MovieRepository;
		Specification = specification;
	}

	public override async Task<ICollection<Movie>> GetAllAsync(MovieFilter movieFilter)
	{
		Expression<Func<Movie, bool>>? filter = null;

		ApplyFilter(ref filter, ref movieFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(movieFilter);
		ApplyFilterOrderBy(movieFilter);

		Specification.AddInclude(query => query
			.Include(movie => movie.Ratings)
		);

		return await Repository.GetAllAsync(Specification);
	}

	public override async Task<ICollection<TDto>> GetAllAsync<TDto>(MovieFilter movieFilter)
	{
		Expression<Func<Movie, bool>>? filter = null;

		ApplyFilter(ref filter, ref movieFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(movieFilter);
		ApplyFilterOrderBy(movieFilter);

		Specification.AddInclude(query => query
			.Include(movie => movie.Ratings)
		);

		return await Repository.GetAllAsync<TDto>(Specification);
	}

	public override async Task<Movie> GetAsync(Expression<Func<Movie, bool>> expression, bool withTracking = false)
	{
		var movie = await TryGetAsync(expression, withTracking);
		if (movie is null) throw new EntityNotFoundException();
		return movie;
	}

	public override async Task<TDto> GetAsync<TDto>(Expression<Func<Movie, bool>> expression)
	{
		var movieDto = await TryGetAsync<TDto>(expression);
		if (movieDto is null) throw new EntityNotFoundException();
		return movieDto;
	}

	public override async Task<Movie?> TryGetAsync(Expression<Func<Movie, bool>> expression, bool withTracking = false)
	{
		Specification.AddFilter(expression);
		Specification.WithTracking(withTracking);
		Specification.AddInclude(query => query
			.Include(movie => movie.FilmRating)
			.Include(movie => movie.MovieGenres)
			.ThenInclude(movieGenre => movieGenre.Genre)
			.Include(movie => movie.MoviePeople)
			.ThenInclude(moviePerson => moviePerson.Person)
			.Include(movie => movie.Projections)
		);

		return await Repository.GetAsync(Specification);
	}

	public override async Task<TDto?> TryGetAsync<TDto>(Expression<Func<Movie, bool>> expression) where TDto : class
	{
		Specification.AddFilter(expression);
		Specification.AddInclude(query => query
			.Include(movie => movie.FilmRating)
			.Include(movie => movie.MovieGenres)
			.ThenInclude(movieGenre => movieGenre.Genre)
			.Include(movie => movie.MoviePeople)
			.ThenInclude(moviePerson => moviePerson.Person)
			.Include(movie => movie.Projections)
		);

		return await Repository.GetAsync<TDto>(Specification);
	}

	private static void ApplySearchFilter(
		ref Expression<Func<Movie, bool>>? filter,
		in MovieFilter movieFilter
	)
	{
		if (movieFilter.Search == null) return;

		var lowercaseSearch = movieFilter.Search.ToLower();
		var searchWords = lowercaseSearch.Split(' ');

		filter = movie => movie.Title.ToLower().Contains(lowercaseSearch);
		filter = searchWords.Aggregate(filter, (current, word)
			=> current.Or(movie => movie.Title.ToLower().Contains(word)));
	}

	private static void ApplyYearFilter(
		ref Expression<Func<Movie, bool>>? filter,
		MovieFilter movieFilter
	)
	{
		if (movieFilter.YearStart.HasValue && movieFilter.YearEnd.HasValue)
		{
			filter = filter
				.And(movie => movie.Year >= movieFilter.YearStart.Value)
				.And(movie => movie.Year <= movieFilter.YearEnd.Value);
		}
		else if (movieFilter.Year.HasValue)
		{
			filter = filter.And(movie => movie.Year == movieFilter.Year.Value);
		}
	}

	private static void ApplyUpcomingFilter(
		ref Expression<Func<Movie, bool>>? filter,
		in MovieFilter movieFilter
	)
	{
		if (movieFilter.ShowUpcoming is 1U)
			filter = filter.And(movie => movie.InCinemas);
	}

	private static void ApplyRatingFilter(
		ref Expression<Func<Movie, bool>>? filter,
		MovieFilter movieFilter
	)
	{
		if (movieFilter.FilmRating != null)
			filter = filter.And(movie => movie.FilmRating.Type == movieFilter.FilmRating);
	}

	private static void ApplyHasProjectionsFilter(
		ref Expression<Func<Movie, bool>>? filter,
		MovieFilter movieFilter
	)
	{
		if (movieFilter.HasProjections is null) return;

		var today = LocalDateTime.FromDateTime(DateTime.UtcNow);

		filter = filter.And(movie => movie.Projections.Any(p =>
			p.Time.Day == today.Day && p.Time.Month == today.Month && p.Time.Year == today.Year)
		);
	}

	private static void ApplyGenresFilter(
		ref Expression<Func<Movie, bool>>? filter,
		in MovieFilter movieFilter
	)
	{
		if (movieFilter.Genres == null) return;

		filter = movieFilter.Genres.Aggregate(filter, (current, genre)
			=> current.And(movie => movie.MovieGenres.Any(movieGenre => movieGenre.Genre.Name == genre)));
	}

	private static void ApplyDurationFilter(
		ref Expression<Func<Movie, bool>>? filter,
		MovieFilter movieFilter
	)
	{
		if (movieFilter.DurationOver.HasValue && movieFilter.DurationUnder.HasValue)
			return;

		if (movieFilter.DurationOver.HasValue)
			filter = filter.And(movie => movie.Duration > movieFilter.DurationOver.Value);
		else if (movieFilter.DurationUnder.HasValue)
			filter = filter.And(movie => movie.Duration < movieFilter.DurationUnder.Value);
	}

	protected override void ApplyFilter(
		ref Expression<Func<Movie, bool>>? filter,
		ref MovieFilter movieFilter
	)
	{
		ApplySearchFilter(ref filter, movieFilter);
		ApplyYearFilter(ref filter, movieFilter);
		ApplyUpcomingFilter(ref filter, movieFilter);
		ApplyRatingFilter(ref filter, movieFilter);
		ApplyGenresFilter(ref filter, movieFilter);
		ApplyDurationFilter(ref filter, movieFilter);
		ApplyHasProjectionsFilter(ref filter, movieFilter);
	}

	private void ApplyFilterOrderBy(in QueryFilter movieFilter)
	{
		Expression<Func<Movie, object>> order = movieFilter.OrderBy switch
		{
			(int) MovieOrderBy.TitleAscending or (int) MovieOrderBy.TitleDescending => movie => movie.Title,
			(int) MovieOrderBy.CreatedAtAscending => movie => movie.CreatedAt,
			(int) MovieOrderBy.CreatedAtDescending => movie => movie.CreatedAt,
			_ => movie => movie.Year
		};
		Expression<Func<Movie, object>> orderThen = movieFilter.OrderBy switch
		{
			(int) MovieOrderBy.TitleAscending or (int) MovieOrderBy.TitleDescending => movie => movie.Year,
			(int) MovieOrderBy.CreatedAtAscending => movie => movie.Year,
			(int) MovieOrderBy.CreatedAtDescending => movie => movie.Year,
			_ => movie => movie.Title
		};

		ApplyFilterOrderBy(in movieFilter, in order);
		ApplyFilterThenOrderBy(in movieFilter, in orderThen);
	}

	public async Task<IEnumerable<TDto>> GetFeaturedMoviesAsync<TDto>(int? limit) where TDto : class, new()
	{
		var specification = new Specification<Movie>();

		specification.AddFilter(movie => movie.IsFeatured);
		specification.AddPagination(1, limit ?? 5);

		return await Repository.GetAllAsync<TDto>(specification);
	}

	public async Task<IEnumerable<TDto>> GetNewMoviesAsync<TDto>(int? limit) where TDto : class, new()
	{
		var specification = new Specification<Movie>();

		specification.AddOrderByDescending(movie => movie.CreatedAt);
		specification.AddFilter(movie => movie.InCinemas);
		specification.AddPagination(1, limit ?? 5);

		return await Repository.GetAllAsync<TDto>(specification);
	}

	public async Task<IEnumerable<TDto>> GetRunningMoviesAsync<TDto>(int day, int month, int? limit)
		where TDto : class, new()
	{
		var specification = new Specification<Movie>();
		var today = LocalDateTime.FromDateTime(DateTime.UtcNow);

		specification.AddInclude(query => query
			.Include(movie => movie.Projections.OrderBy(p => p.Time))
		);

		specification.AddOrderByDescending(m => m.CreatedAt);
		specification.AddFilter(m => m.Projections
			.Any(p => p.Time.Day == day && p.Time.Month == month && p.Time.Year == today.Year)
		);

		specification.AddPagination(1, limit ?? 5);

		return await Repository.GetAllAsync<TDto>(specification);
	}

	public async Task<IEnumerable<TDto>> GetRecommendedMoviesAsync<TDto>(int? limit) where TDto : class, new()
	{
		var specification = new Specification<Movie>();

		specification.AddFilter(movie => movie.IsRecommended);
		specification.AddOrderByDescending(movie => movie.Year);
		specification.AddThenOrderByDescending(movie => movie.Title);
		specification.AddPagination(1, limit ?? 5);

		return await Repository.GetAllAsync<TDto>(specification);
	}

	public async Task<IEnumerable<TDto>> GetRecommendationsAsync<TDto>(string id) where TDto : class, new()
	{
		var movie = await GetAsync(movie => movie.Id == id);

		var recommendations = await GetRecommendationsByTitleAsync<TDto>(movie);
		var byDirectors = await GetRecommendationsByDirectorsAsync<TDto>(movie);
		var byActors = await GetRecommendationsByActorsAsync<TDto>(movie);
		var byGenres = await GetRecommendationsByGenresAsync<TDto>(movie);

		return recommendations
			.Concat(byDirectors)
			.Concat(byActors)
			.Concat(byGenres)
			.Distinct();
	}

	private async Task<IEnumerable<TDto>> GetRecommendationsByTitleAsync<TDto>(Movie movie) where TDto : class, new()
	{
		char[] delimiterChars = { ' ', ',', '.', ':', '-', '\t' };
		var lowercaseSearch = movie.Title.ToLower();
		var searchWords = lowercaseSearch.Split(delimiterChars).Where(s => s.Length > 3).ToArray();
		var specification = new Specification<Movie>();

		Expression<Func<Movie, bool>> titleFilter = m => m.Title.ToLower().Contains(lowercaseSearch);

		titleFilter = searchWords
			.Aggregate(titleFilter, (current, word)
				=> current.Or(m => m.Title.ToLower().Contains(word)));

		Expression<Func<Movie, bool>> movieFilter = m => m.Id != movie.Id;
		movieFilter = movieFilter.And(titleFilter);

		specification.AddFilter(movieFilter);
		specification.AddOrderByDescending(m => m.Year);
		specification.AddThenOrderByDescending(m => m.Title);
		specification.AddPagination(1, 5);
		specification.WithDistinct(true);

		return await Repository.GetAllAsync<TDto>(specification);
	}

	private async Task<IEnumerable<TDto>> GetRecommendationsByDirectorsAsync<TDto>(Movie movie)
		where TDto : class, new()
	{
		var specification = new Specification<Movie>();

		var directors = movie.MoviePeople
			.Where(mp => mp.Type == "Director")
			.Select(mp => mp.PersonId);

		Expression<Func<Movie, bool>> directorFilter = m => m.MoviePeople
			.Where(mp => mp.Type == "Director")
			.Any(mp => mp.PersonId == directors.First());

		directorFilter = directors
			.Skip(1)
			.Aggregate(directorFilter, (current, directorId)
				=> current.Or(m => m.MoviePeople
					.Where(mp => mp.Type == "Director")
					.Any(mp => mp.PersonId == directorId)));

		Expression<Func<Movie, bool>> movieFilter = m => m.Id != movie.Id;
		movieFilter = movieFilter.And(directorFilter);

		specification.AddFilter(movieFilter);
		specification.AddOrderByDescending(m => m.Year);
		specification.AddThenOrderByDescending(m => m.Title);
		specification.AddPagination(1, 5);
		specification.WithDistinct(true);

		return await Repository.GetAllAsync<TDto>(specification);
	}

	private async Task<IEnumerable<TDto>> GetRecommendationsByActorsAsync<TDto>(Movie movie)
		where TDto : class, new()
	{
		var specification = new Specification<Movie>();

		var actors = movie.MoviePeople
			.Where(mp => mp.Type == "Actor")
			.OrderBy(mp => mp.Order)
			.Skip(0)
			.Take(10)
			.Select(mp => mp.PersonId);

		Expression<Func<Movie, bool>> actorFilter = m => m.MoviePeople
			.Where(mp => mp.Type == "Actor")
			.Any(mp => mp.PersonId == actors.First());

		actorFilter = actors
			.Skip(1)
			.Aggregate(actorFilter, (current, actorId)
				=> current.Or(m => m.MoviePeople
					.Where(mp => mp.Type == "Actor")
					.Any(mp => mp.PersonId == actorId)));

		Expression<Func<Movie, bool>> movieFilter = m => m.Id != movie.Id;
		movieFilter = movieFilter.And(actorFilter);

		specification.AddFilter(movieFilter);
		specification.AddOrderByDescending(m => m.Year);
		specification.AddThenOrderByDescending(m => m.Title);
		specification.AddPagination(1, 5);
		specification.WithDistinct(true);

		return await Repository.GetAllAsync<TDto>(specification);
	}

	private async Task<IEnumerable<TDto>> GetRecommendationsByGenresAsync<TDto>(Movie movie)
		where TDto : class, new()
	{
		var specification = new Specification<Movie>();

		Expression<Func<Movie, bool>> genreFilter = m => m.InCinemas;
		genreFilter = movie.MovieGenres
			.Select(mg => mg.Genre.Name)
			.Aggregate(genreFilter, (current, genre)
				=> current.Or(m => m.MovieGenres.Any(mg => mg.Genre.Name == genre)));

		Expression<Func<Movie, bool>> movieFilter = m => m.Id != movie.Id;
		movieFilter = movieFilter.And(m => m.FilmRatingId == movie.FilmRatingId);
		movieFilter = movieFilter.And(genreFilter);

		specification.AddFilter(movieFilter);
		specification.AddPagination(1, 10);
		specification.WithDistinct(true);

		return await Repository.GetAllAsync<TDto>(specification);
	}

	public async Task<IEnumerable<TDto>> GetBestRatedMoviesAsync<TDto>() where TDto : class, new()
	{
		var specification = new Specification<Movie>();

		specification.AddFilter(m => m.Ratings.Any() &&
			(decimal) m.Ratings.Sum(r => r.Rating) / m.Ratings.Count > 3.5M);

		specification.AddPagination(1, 10);
		specification.AddOrderByDescending(m => (decimal) m.Ratings.Sum(r => r.Rating) / m.Ratings.Count);
		specification.AddThenOrderByDescending(m => m.Ratings.Count);

		return await Repository.GetAllAsync<TDto>(specification);
	}

	public async Task<IEnumerable<TDto>> GetWorstRatedMoviesAsync<TDto>() where TDto : class, new()
	{
		var specification = new Specification<Movie>();

		specification.AddFilter(m => m.Ratings.Any() &&
			(decimal) m.Ratings.Sum(r => r.Rating) / m.Ratings.Count <= 3.5M);

		specification.AddPagination(1, 10);
		specification.AddOrderBy(m => (decimal) m.Ratings.Sum(r => r.Rating) / m.Ratings.Count);
		specification.AddThenOrderBy(m => m.Ratings.Count);

		return await Repository.GetAllAsync<TDto>(specification);
	}
}
