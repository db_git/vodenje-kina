//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Service;

/// <inheritdoc cref="IHallCommandService" />
public sealed class HallCommandService : CommandServiceBase<Hall>, IHallCommandService
{
	private readonly ICinemaQueryService _cinemaQueryService;

	public HallCommandService(IClock clock, IUnitOfWork unitOfWork, ICinemaQueryService cinemaQueryService)
	{
		Clock = clock;
		UnitOfWork = unitOfWork;
		Repository = UnitOfWork.HallRepository;
		_cinemaQueryService = cinemaQueryService;
	}

	/// <inheritdoc cref="IHallCommandService.AddAsync" />
	public override async Task AddAsync(Hall hall)
	{
		await CanAddHall(hall);
		await base.AddAsync(hall);
	}

	/// <inheritdoc cref="IHallCommandService.AddRangeAsync" />
	public override async Task AddRangeAsync(IEnumerable<Hall> halls)
	{
		var hallsArray = halls.ToArray();
		foreach (var hall in hallsArray) await CanAddHall(hall);
		await base.AddRangeAsync(hallsArray);
	}

	/// <inheritdoc cref="IHallCommandService.UpdateAsync" />
	public override async Task UpdateAsync(Hall hall)
	{
		await CanAddHall(hall);
		await base.UpdateAsync(hall);
	}

	private async Task CanAddHall(Hall hall)
	{
		var cinemasCount = await _cinemaQueryService.CountAsync(c => c.Id == hall.CinemaId);
		if (cinemasCount is 0)
			throw new EntityNotFoundException($"Cinema with id '{hall.CinemaId}' could not be found");
	}
}
