//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Service;

/// <inheritdoc cref="IGenreCommandService" />
public sealed class GenreCommandService : CommandServiceBase<Genre>, IGenreCommandService
{
	public GenreCommandService(IClock clock, IUnitOfWork unitOfWork)
	{
		Clock = clock;
		UnitOfWork = unitOfWork;
		Repository = UnitOfWork.GenreRepository;
	}

	/// <inheritdoc cref="IGenreCommandService.AddAsync" />
	public override async Task AddAsync(Genre genre)
	{
		await CanAddGenre(genre);
		await base.AddAsync(genre);
	}

	/// <inheritdoc cref="IGenreCommandService.AddRangeAsync" />
	public override async Task AddRangeAsync(IEnumerable<Genre> genres)
	{
		var genresArray = genres.ToArray();
		foreach (var genre in genresArray) await CanAddGenre(genre);
		await base.AddRangeAsync(genresArray);
	}

	/// <inheritdoc cref="IGenreCommandService.UpdateAsync" />
	public override async Task UpdateAsync(Genre genre)
	{
		await CanAddGenre(genre);
		await base.UpdateAsync(genre);
	}

	private async Task CanAddGenre(Genre genre)
	{
		var genresCount = await Repository.CountAsync(g => g.Name == genre.Name);
		if (genresCount is not 0)
			throw new DuplicateEntityException($"Genre '{genre.Name}' already exists.");
	}
}
