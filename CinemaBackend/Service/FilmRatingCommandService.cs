//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Service;

/// <inheritdoc cref="IFilmRatingCommandService" />
public sealed class FilmRatingCommandService : CommandServiceBase<FilmRating>, IFilmRatingCommandService
{
	public FilmRatingCommandService(IClock clock, IUnitOfWork unitOfWork)
	{
		Clock = clock;
		UnitOfWork = unitOfWork;
		Repository = UnitOfWork.FilmRatingRepository;
	}

	/// <inheritdoc cref="IFilmRatingCommandService.AddAsync" />
	public override async Task AddAsync(FilmRating filmRating)
	{
		await CanAddFilmRating(filmRating);
		await base.AddAsync(filmRating);
	}

	/// <inheritdoc cref="IFilmRatingCommandService.AddRangeAsync" />
	public override async Task AddRangeAsync(IEnumerable<FilmRating> filmRatings)
	{
		var filmRatingsArray = filmRatings.ToArray();
		foreach (var filmRating in filmRatingsArray) await CanAddFilmRating(filmRating);
		await base.AddRangeAsync(filmRatingsArray);
	}

	/// <inheritdoc cref="IFilmRatingCommandService.UpdateAsync" />
	public override async Task UpdateAsync(FilmRating filmRating)
	{
		await CanAddFilmRating(filmRating);
		await base.UpdateAsync(filmRating);
	}

	private async Task CanAddFilmRating(FilmRating filmRating)
	{
		var filmRatingsCount = await Repository.CountAsync(fr => fr.Type == filmRating.Type);
		if (filmRatingsCount is not 0)
			throw new DuplicateEntityException($"Film rating '{filmRating.Type}' already exists.");
	}
}
