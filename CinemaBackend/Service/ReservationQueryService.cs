//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Service;

/// <inheritdoc cref="IReservationQueryService" />
public sealed class ReservationQueryService : QueryServiceBase<Reservation, ReservationFilter>, IReservationQueryService
{
	public ReservationQueryService(IUnitOfWork unitOfWork, ISpecification<Reservation> specification)
	{
		UnitOfWork = unitOfWork;
		Repository = UnitOfWork.ReservationRepository;
		Specification = specification;
	}

	public override async Task<ICollection<Reservation>> GetAllAsync(ReservationFilter reservationFilter)
	{
		Expression<Func<Reservation, bool>>? filter = null;

		ApplyFilter(ref filter, ref reservationFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(reservationFilter);
		ApplyFilterOrderBy(reservationFilter);

		Specification.AddInclude(query => query
			.Include(reservation => reservation.Projection)
			.Include(reservation => reservation.ReservationSeats)
			.ThenInclude(reservationSeat => reservationSeat.Seat)
			.Include(reservation => reservation.FoodOrders)
			.ThenInclude(foodOrder => foodOrder.Food)
			.Include(reservation => reservation.SouvenirOrders)
			.ThenInclude(souvenirOrder => souvenirOrder.Souvenir)
		);

		return await Repository.GetAllAsync(Specification);
	}

	public override async Task<ICollection<TDto>> GetAllAsync<TDto>(ReservationFilter reservationFilter)
	{
		Expression<Func<Reservation, bool>>? filter = null;

		ApplyFilter(ref filter, ref reservationFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(reservationFilter);
		ApplyFilterOrderBy(reservationFilter);

		Specification.AddInclude(query => query
			.Include(reservation => reservation.Projection)
			.Include(reservation => reservation.ReservationSeats)
			.ThenInclude(reservationSeat => reservationSeat.Seat)
			.Include(reservation => reservation.FoodOrders)
			.ThenInclude(foodOrder => foodOrder.Food)
			.Include(reservation => reservation.SouvenirOrders)
			.ThenInclude(souvenirOrder => souvenirOrder.Souvenir)
		);

		return await Repository.GetAllAsync<TDto>(Specification);
	}

	public override async Task<Reservation> GetAsync(Expression<Func<Reservation, bool>> expression, bool withTracking = false)
	{
		var reservation = await TryGetAsync(expression, withTracking);
		if (reservation is null) throw new EntityNotFoundException();
		return reservation;
	}

	public override async Task<TDto> GetAsync<TDto>(Expression<Func<Reservation, bool>> expression)
	{
		var reservationDto = await TryGetAsync<TDto>(expression);
		if (reservationDto is null) throw new EntityNotFoundException();
		return reservationDto;
	}

	public override async Task<Reservation?> TryGetAsync(Expression<Func<Reservation, bool>> expression, bool withTracking = false)
	{
		Specification.AddFilter(expression);
		Specification.WithTracking(withTracking);
		Specification.AddInclude(query => query
			.Include(reservation => reservation.Projection)
			.Include(reservation => reservation.ReservationSeats)
			.ThenInclude(reservationSeat => reservationSeat.Seat)
			.Include(reservation => reservation.FoodOrders)
			.ThenInclude(foodOrder => foodOrder.Food)
			.Include(reservation => reservation.SouvenirOrders)
			.ThenInclude(souvenirOrder => souvenirOrder.Souvenir)
		);

		return await Repository.GetAsync(Specification);
	}

	public override async Task<TDto?> TryGetAsync<TDto>(Expression<Func<Reservation, bool>> expression) where TDto : class
	{
		Specification.AddFilter(expression);
		Specification.AddInclude(query => query
			.Include(reservation => reservation.Projection)
			.Include(reservation => reservation.ReservationSeats)
			.ThenInclude(reservationSeat => reservationSeat.Seat)
			.Include(reservation => reservation.FoodOrders)
			.ThenInclude(foodOrder => foodOrder.Food)
			.Include(reservation => reservation.SouvenirOrders)
			.ThenInclude(souvenirOrder => souvenirOrder.Souvenir)
		);

		return await Repository.GetAsync<TDto>(Specification);
	}

	protected override void ApplyFilter(
		ref Expression<Func<Reservation, bool>>? filter,
		ref ReservationFilter reservationFilter
	)
	{
		ApplySearchFilter(ref filter, ref reservationFilter);
		ApplyUserFilter(ref filter, ref reservationFilter);
	}

	private static void ApplySearchFilter(
		ref Expression<Func<Reservation, bool>>? filter,
		ref ReservationFilter reservationFilter
	)
	{
		if (reservationFilter.Search is null) return;

		var lowercaseSearch = reservationFilter.Search.ToLower();
		var searchWords = lowercaseSearch.Split(' ');

		filter = reservation => reservation.User.Name.ToLower().Contains(lowercaseSearch);
		filter = filter.Or(reservation => reservation.Projection.Movie.Title.ToLower().Contains(lowercaseSearch));

		filter = searchWords.Aggregate(filter, (current, word)
			=> current.Or(reservation => reservation.User.Name.ToLower().Contains(word)));
		filter = searchWords.Aggregate(filter, (current, word)
			=> current.Or(reservation => reservation.Projection.Movie.Title.ToLower().Contains(word)));
	}

	private static void ApplyUserFilter(
		ref Expression<Func<Reservation, bool>>? filter,
		ref ReservationFilter reservationFilter
	)
	{
		var userId = reservationFilter.User;
		if (userId is null) return;

		filter = filter.And(reservation => reservation.UserId == userId);
	}

	private void ApplyFilterOrderBy(in QueryFilter reservationFilter)
	{
		Expression<Func<Reservation, object>> order = reservationFilter.OrderBy switch
		{
			(int) ReservationOrderBy.MovieTitleAscending => reservation => reservation.Projection.Movie.Title,
			(int) ReservationOrderBy.MovieTitleDescending => reservation => reservation.Projection.Movie.Title,
			(int) ReservationOrderBy.UserNameAscending => reservation => reservation.User.Name,
			(int) ReservationOrderBy.UserNameDescending => reservation => reservation.User.Name,
			_ => reservation => reservation.CreatedAt
		};

		ApplyFilterOrderBy(in reservationFilter, in order);
	}

	private Task<ICollection<TDto>> GetMonthReservationsAsync<TDto>(int month, int year) where TDto : class, new()
	{
		var specification = new Specification<Reservation>();

		specification.AddFilter(r => r.ReservationSeats.Any(s =>
			s.CreatedAt.InUtc().Month == month && s.CreatedAt.InUtc().Year == year
		));

		specification.AddOrderByDescending(s => s.SouvenirOrders.Count);
		specification.AddPagination(1, 10);

		return Repository.GetAllAsync<TDto>(specification);
	}

	public Task<ICollection<TDto>> GetCurrentMonthReservationsAsync<TDto>() where TDto : class, new()
	{
		var date = SystemClock.Instance.GetCurrentInstant().InUtc();
		var month = date.Month;
		var year = date.Year;

		return GetMonthReservationsAsync<TDto>(month, year);
	}

	public Task<ICollection<TDto>> GetPreviousMonthReservationsAsync<TDto>() where TDto : class, new()
	{
		var date = SystemClock.Instance.GetCurrentInstant().InUtc();
		var month = date.Month;
		var year = date.Year;

		int previousMonth, previousYear;

		if (month is 1)
		{
			previousMonth = 12;
			previousYear = year - 1;
		}
		else
		{
			previousMonth = month - 1;
			previousYear = year;
		}

		return GetMonthReservationsAsync<TDto>(previousMonth, previousYear);
	}
}
