//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Domain.Entities.Identity;

namespace Service;

/// <inheritdoc cref="IUserQueryService" />
public sealed class UserQueryService : QueryServiceBase<User, QueryFilter>, IUserQueryService
{
	public UserQueryService(IUnitOfWork unitOfWork, ISpecification<User> specification)
	{
		UnitOfWork = unitOfWork;
		Repository = unitOfWork.UserRepository;
		Specification = specification;
	}

	public override async Task<ICollection<User>> GetAllAsync(QueryFilter userFilter)
	{
		Expression<Func<User, bool>>? filter = null;

		ApplyFilter(ref filter, ref userFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(userFilter);
		ApplyFilterOrderBy(userFilter);

		Specification.AddInclude(query => query
			.Include(user => user.UserRoles)
			.ThenInclude(userRole => userRole.Role)
		);

		return await Repository.GetAllAsync(Specification);
	}

	public override async Task<ICollection<TDto>> GetAllAsync<TDto>(QueryFilter userFilter)
	{
		Expression<Func<User, bool>>? filter = null;

		ApplyFilter(ref filter, ref userFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(userFilter);
		ApplyFilterOrderBy(userFilter);

		Specification.AddInclude(query => query
			.Include(user => user.UserRoles)
			.ThenInclude(userRole => userRole.Role)
		);

		return await Repository.GetAllAsync<TDto>(Specification);
	}

	public override async Task<User> GetAsync(Expression<Func<User, bool>> expression, bool withTracking = false)
	{
		var user = await TryGetAsync(expression, withTracking);
		if (user is null) throw new EntityNotFoundException();
		return user;
	}

	public override async Task<TDto> GetAsync<TDto>(Expression<Func<User, bool>> expression)
	{
		var userDto = await TryGetAsync<TDto>(expression);
		if (userDto is null) throw new EntityNotFoundException();
		return userDto;
	}

	public override async Task<User?> TryGetAsync(Expression<Func<User, bool>> expression, bool withTracking = false)
	{
		Specification.AddFilter(expression);
		Specification.WithTracking(withTracking);
		Specification.AddInclude(query => query
			.Include(user => user.UserRoles)
			.ThenInclude(userRole => userRole.Role)
		);

		return await Repository.GetAsync(Specification);
	}

	public override async Task<TDto?> TryGetAsync<TDto>(Expression<Func<User, bool>> expression) where TDto : class
	{
		Specification.AddFilter(expression);
		Specification.AddInclude(query => query
			.Include(user => user.UserRoles)
			.ThenInclude(userRole => userRole.Role)
		);

		return await Repository.GetAsync<TDto>(Specification);
	}

	protected override void ApplyFilter(
		ref Expression<Func<User, bool>>? filter,
		ref QueryFilter userFilter
	)
	{
		if (userFilter.Search == null) return;

		var lowercaseSearch = userFilter.Search.ToLower();
		var searchWords = lowercaseSearch.Split(' ');

		filter = user => user.Name.ToLower().Contains(lowercaseSearch);
		filter.Or(user => user.Email!.ToLower().Contains(lowercaseSearch));

		filter = searchWords.Aggregate(filter, (current, word)
			=> current.Or(user => user.Name.ToLower().Contains(word)));
		filter = searchWords.Aggregate(filter, (current, word)
			=> current.Or(user => user.Email!.ToLower().Contains(word)));
	}

	private void ApplyFilterOrderBy(in QueryFilter userFilter)
	{
		Expression<Func<User, object>> order = user => user.Email!;
		ApplyFilterOrderBy(in userFilter, in order);
	}
}
