//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Domain.Entities.Identity;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;
using Service.Contracts.Email;

namespace Service.Email;

/// <inheritdoc />
public sealed class EmailSender : IEmailSender
{
	private readonly IConfigurationSection _configuration;
	private readonly EmailAddress _from;

	public EmailSender(IConfiguration configuration)
	{
		_configuration = configuration.GetSection("SendGrid");

		_from = new EmailAddress(
			_configuration.GetSection("SenderEmail").Value,
			_configuration.GetSection("SenderName").Value
		);
	}

	public async Task<Response> SendEmailConfirmationAsync(User user, object data)
	{
		var client = new SendGridClient(_configuration.GetSection("ApiKey").Value);
		var to = new EmailAddress(user.Email, user.Name);

		var email = MailHelper.CreateSingleTemplateEmail(
			_from,
			to,
			_configuration.GetSection("EmailVerificationTemplateId").Value,
			data
		);

		return await client.SendEmailAsync(email);
	}

	public async Task<Response> SendResetPasswordAsync(User user, object data)
	{
		var client = new SendGridClient(_configuration.GetSection("ApiKey").Value);
		var to = new EmailAddress(user.Email, user.Name);

		var email = MailHelper.CreateSingleTemplateEmail(
			_from,
			to,
			_configuration.GetSection("PasswordResetTemplateId").Value,
			data
		);

		return await client.SendEmailAsync(email);
	}

	public async Task<Response> SendReservationDetailsAsync(User user, IEnumerable<string> reservationIds)
	{
		var client = new SendGridClient(_configuration.GetSection("ApiKey").Value);
		var to = new EmailAddress(user.Email, user.Name);

		var reservationIdsArray = reservationIds.ToArray();

		var reservationIdsPlain = string.Join("\n", reservationIdsArray);
		var plainTextContent = $"""
			Here are your reservation codes:
			{reservationIdsPlain}
			Please show them at the hall entrance.
			Enjoy your movie!
		""";

		var reservationIdsHtml = string.Join(
			string.Empty,
			reservationIdsArray.Select(id => "<li><strong>" + id + "</strong></li>")
		);

		var htmlTextContent = $"""
			<!DOCTYPE html>
			<html lang="en">
				<head>
					<meta charset="utf-8">
					<title>Order details</title>
				</head>
				<body>
					<p>Here are your reservation codes:</p>
					<ul>
						{reservationIdsHtml}
					</ul>
					<p>Please show them at the hall entrance.</p>
					<p>Enjoy your movie!</p>
				</body>
			</html>
		""";

		var email = MailHelper.CreateSingleEmail(
			_from,
			to,
			"Order details",
			plainTextContent,
			htmlTextContent
		);

		return await client.SendEmailAsync(email);
	}
}
