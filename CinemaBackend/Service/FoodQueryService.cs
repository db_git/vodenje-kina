//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Service;

/// <inheritdoc cref="IFoodQueryService" />
public sealed class FoodQueryService : QueryServiceBase<Food, FoodFilter>, IFoodQueryService
{
	public FoodQueryService(IUnitOfWork unitOfWork, ISpecification<Food> specification)
	{
		UnitOfWork = unitOfWork;
		Repository = UnitOfWork.FoodRepository;
		Specification = specification;
	}

	public override async Task<ICollection<Food>> GetAllAsync(FoodFilter foodFilter)
	{
		Expression<Func<Food, bool>>? filter = null;

		ApplyFilter(ref filter, ref foodFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(foodFilter);
		ApplyFilterOrderBy(foodFilter);

		return await Repository.GetAllAsync(Specification);
	}

	public override async Task<ICollection<TDto>> GetAllAsync<TDto>(FoodFilter foodFilter)
	{
		Expression<Func<Food, bool>>? filter = null;

		ApplyFilter(ref filter, ref foodFilter);
		if (filter != null) Specification.AddFilter(filter);

		ApplyFilterPagination(foodFilter);
		ApplyFilterOrderBy(foodFilter);

		return await Repository.GetAllAsync<TDto>(Specification);
	}

	protected override void ApplyFilter(
		ref Expression<Func<Food, bool>>? filter,
		ref FoodFilter foodFilter
	)
	{
		ApplySearchFilter(ref filter, ref foodFilter);
		ApplyAvailableFilter(ref filter, ref foodFilter);
	}

	private static void ApplySearchFilter(
		ref Expression<Func<Food, bool>>? filter,
		ref FoodFilter foodFilter
	)
	{
		if (foodFilter.Search is null) return;

		var lowercaseSearch = foodFilter.Search.ToLower();
		var searchWords = lowercaseSearch.Split(' ');

		filter = food => food.Name.ToLower().Contains(lowercaseSearch);
		filter = searchWords.Aggregate(filter, (current, word)
			=> current.Or(food => food.Name.ToLower().Contains(word)));
	}

	private static void ApplyAvailableFilter(
		ref Expression<Func<Food, bool>>? filter,
		ref FoodFilter foodFilter
	)
	{
		if (foodFilter.AvailableOnly.HasValue is false || foodFilter.AvailableOnly.Value is 0)
			return;

		filter = filter.And(souvenir => souvenir.AvailableQuantity > 1);
	}

	private void ApplyFilterOrderBy(in QueryFilter foodFilter)
	{
		Expression<Func<Food, object>> order = foodFilter.OrderBy switch
		{
			(int) FoodOrderBy.PriceAscending or (int) FoodOrderBy.PriceDescending => food => food.Price,
			(int) FoodOrderBy.AvailableQuantityAscending => food => food.AvailableQuantity,
			(int) FoodOrderBy.AvailableQuantityDescending => food => food.AvailableQuantity,
			(int) FoodOrderBy.SizeAscending or (int) FoodOrderBy.SizeDescending => food => food.Size,
			(int) FoodOrderBy.TypeAscending or (int) FoodOrderBy.TypeDescending => food => food.Type,
			_ => food => food.Name
		};

		ApplyFilterOrderBy(in foodFilter, in order);
	}

	private Task<ICollection<TDto>> GetMonthFoodsAsync<TDto>(int month, int year) where TDto : class, new()
	{
		var specification = new Specification<Food>();

		specification.AddFilter(f => f.FoodOrders.Any(o =>
			o.CreatedAt.InUtc().Month == month && o.CreatedAt.InUtc().Year == year
		));

		specification.AddOrderByDescending(f => f.FoodOrders.Count);
		specification.AddPagination(1, 10);

		return Repository.GetAllAsync<TDto>(specification);
	}

	public Task<ICollection<TDto>> GetCurrentMonthFoodsAsync<TDto>() where TDto : class, new()
	{
		var date = SystemClock.Instance.GetCurrentInstant().InUtc();
		var month = date.Month;
		var year = date.Year;

		return GetMonthFoodsAsync<TDto>(month, year);
	}

	public Task<ICollection<TDto>> GetPreviousMonthFoodsAsync<TDto>() where TDto : class, new()
	{
		var date = SystemClock.Instance.GetCurrentInstant().InUtc();
		var month = date.Month;
		var year = date.Year;

		int previousMonth, previousYear;

		if (month is 1)
		{
			previousMonth = 12;
			previousYear = year - 1;
		}
		else
		{
			previousMonth = month - 1;
			previousYear = year;
		}

		return GetMonthFoodsAsync<TDto>(previousMonth, previousYear);
	}
}
