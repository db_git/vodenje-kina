//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Repository.Tests;

[TestFixture]
public sealed class GenreRepositoryTests : RepositoryTestBase<Genre, GenreRepository, GenreInMemoryContext>
{
	[TestCaseSource(typeof(GenreUtils), nameof(GenreUtils.RandomGenreNames))]
	public async Task Given_Genre_When_AddAsync_Then_SaveGenre(string name)
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var genre = new Genre { Name = name };

		Repository.Add(genre);
		await UnitOfWork.SaveAsync();

		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + 1));
	}

	[Test]
	public async Task Given_Genres_When_AddRangeAsync_Then_SaveGenres()
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var genres = new List<Genre>(GenreUtils.RandomGenreNames.Length);
		genres.AddRange(GenreUtils.RandomGenreNames
			.Select(name => new Genre { Name = name })
		);

		Repository.AddRange(genres);
		await UnitOfWork.SaveAsync();

		var newCount = await Repository.CountAsync(EmptySpecification);
		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + GenreUtils.RandomGenreNames.Length));
	}

	[TestCaseSource(typeof(GenreFakeData), nameof(GenreFakeData.GenreIds))]
	public async Task Given_GenreId_When_GetAsync_Then_ReturnGenre(Guid id)
	{
		Specification.AddFilter(g => g.Id == id);
		var genre = await Repository.GetAsync(Specification);

		Assert.That(genre, Is.Not.Null);
		Assert.That(genre!.Id, Is.EqualTo(id));
	}

	[TestCaseSource(typeof(GuidUtils), nameof(GuidUtils.NonexistentGuids))]
	public async Task Given_InvalidId_When_GetAsync_Then_ReturnNull(Guid id)
	{
		Specification.AddFilter(g => g.Id == id);
		var genre = await Repository.GetAsync(Specification);

		Assert.That(genre, Is.Null);
	}

	[TestCaseSource(typeof(GenreFakeData), nameof(GenreFakeData.GenreIds))]
	public async Task Given_Name_When_UpdateAsync_Then_UpdateGenreName(Guid id)
	{
		var name = Faker.Lorem.Sentence(1);
		var initialCount = await Repository.CountAsync(EmptySpecification);

		Specification.AddFilter(g => g.Id == id);
		var genre = await Repository.GetAsync(Specification);
		Assert.That(genre, Is.Not.Null);
		Assert.That(genre!.Name, Is.Not.EqualTo(name));

		genre.Name = name;
		genre.ModifiedAt = Clock.GetCurrentInstant();

		Repository.Update(genre);
		await UnitOfWork.SaveAsync();

		Specification.AddFilter(g => g.Name == name);
		var updatedGenre = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(updatedGenre, Is.Not.Null);
		Assert.That(updatedGenre, Is.Not.EqualTo(genre));
		Assert.That(name, Is.EqualTo(updatedGenre!.Name));
		Assert.That(initialCount, Is.EqualTo(newCount));
	}

	[TestCaseSource(typeof(GenreFakeData), nameof(GenreFakeData.GenreIds))]
	public async Task Given_GenreId_When_DeleteAsync_Then_DeleteGenre(Guid id)
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		Specification.AddFilter(g => g.Id == id);
		var genre = await Repository.GetAsync(Specification);
		Assert.That(genre, Is.Not.Null);

		Repository.Delete(genre!);
		await UnitOfWork.SaveAsync();

		var deletedGenre = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(deletedGenre, Is.Null);
		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - 1));
	}

	[Test]
	public async Task Given_GenreIds_When_DeleteRangeAsync_Then_DeleteGenres()
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var genres = new List<Genre>(GenreFakeData.GenreIds.Count);
		foreach (Guid id in GenreFakeData.GenreIds)
		{
			Specification.AddFilter(g => g.Id == id);
			var genre = await Repository.GetAsync(Specification);
			Assert.That(genre, Is.Not.Null);
			genres.Add(genre!);
		}

		Repository.DeleteRange(genres);
		await UnitOfWork.SaveAsync();
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - GenreFakeData.GenreIds.Count));
	}
}
#endif
