//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Repository.Tests;

[TestFixture]
public sealed class SpecificationTests
{
	private sealed record Box(int Value);

	private readonly IList<Box> _values = new List<Box>
	{
		new (2),
		new (3),
		new (1)
	};

	private ISpecification<Box> _specification = null!;

	[SetUp]
	public void SetUp()
	{
		_specification = new Specification<Box>();
	}

	[Test]
	public void Given_Filter_When_ValueGreaterThan_Then_ReturnFilteredValues()
	{
		_specification.AddFilter(x => x.Value > 1);

		var filteredList = SpecificationEvaluator<Box>.GetQuery(_values.AsQueryable(), _specification).ToList();

		Assert.That(filteredList, Has.Count.EqualTo(2));
		Assert.That(filteredList.ElementAt(0).Value, Is.EqualTo(2));
		Assert.That(filteredList.ElementAt(1).Value, Is.EqualTo(3));
	}

	[Test]
	public void Given_Filter_When_ValueLessThan_Then_ReturnFilteredValues()
	{
		_specification.AddFilter(x => x.Value < 3);

		var filteredList = SpecificationEvaluator<Box>.GetQuery(_values.AsQueryable(), _specification).ToList();

		Assert.That(filteredList, Has.Count.EqualTo(2));
		Assert.That(filteredList.ElementAt(0).Value, Is.EqualTo(2));
		Assert.That(filteredList.ElementAt(1).Value, Is.EqualTo(1));
	}

	[Test]
	public void Given_Filter_When_ValueEqual_Then_ReturnFilteredValues()
	{
		_specification.AddFilter(x => x.Value == 2);

		var filteredList = SpecificationEvaluator<Box>.GetQuery(_values.AsQueryable(), _specification).ToList();

		Assert.That(filteredList, Has.Count.EqualTo(1));
		Assert.That(filteredList.ElementAt(0).Value, Is.EqualTo(2));
	}

	[Test]
	public void Given_OrderBy_When_Ascending_Then_ReturnOrderedValues()
	{
		_specification.AddOrderBy(x => x.Value);

		var filteredList = SpecificationEvaluator<Box>.GetQuery(_values.AsQueryable(), _specification).ToList();

		Assert.That(filteredList, Has.Count.EqualTo(3));
		Assert.That(filteredList.ElementAt(0).Value, Is.EqualTo(1));
		Assert.That(filteredList.ElementAt(1).Value, Is.EqualTo(2));
		Assert.That(filteredList.ElementAt(2).Value, Is.EqualTo(3));
	}

	[Test]
	public void Given_OrderBy_When_Descending_Then_ReturnOrderedValues()
	{
		_specification.AddOrderByDescending(x => x.Value);

		var filteredList = SpecificationEvaluator<Box>.GetQuery(_values.AsQueryable(), _specification).ToList();

		Assert.That(filteredList, Has.Count.EqualTo(3));
		Assert.That(filteredList.ElementAt(0).Value, Is.EqualTo(3));
		Assert.That(filteredList.ElementAt(1).Value, Is.EqualTo(2));
		Assert.That(filteredList.ElementAt(2).Value, Is.EqualTo(1));
	}

	[Test]
	public void Given_Pagination_When_ValidParameters_Then_ReturnPaginatedValues()
	{
		_specification.AddPagination(3, 1);

		var filteredList = SpecificationEvaluator<Box>.GetQuery(_values.AsQueryable(), _specification).ToList();

		Assert.That(filteredList, Has.Count.EqualTo(1));
		Assert.That(filteredList.ElementAt(0).Value, Is.EqualTo(1));
	}

	[Test]
	public void Given_Pagination_When_InvalidParameters_Then_ReturnPaginatedValues()
	{
		_specification.AddPagination(-5, 1250);

		var filteredList = SpecificationEvaluator<Box>.GetQuery(_values.AsQueryable(), _specification).ToList();

		Assert.That(filteredList, Has.Count.EqualTo(3));
		Assert.That(filteredList.ElementAt(0).Value, Is.EqualTo(2));
		Assert.That(filteredList.ElementAt(1).Value, Is.EqualTo(3));
		Assert.That(filteredList.ElementAt(2).Value, Is.EqualTo(1));
	}
}
#endif
