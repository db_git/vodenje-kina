//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Repository.Tests;

[TestFixture]
public sealed class MovieRepositoryTests : RepositoryTestBase<Movie, MovieRepository, MovieInMemoryContext>
{
	[TestCaseSource(typeof(MovieUtils), nameof(MovieUtils.RandomMovieIds))]
	public async Task Given_Movie_When_AddAsync_Then_SaveMovie(string id)
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var movie = new Movie
		{
			Id = id,
			Title = Faker.Lorem.Word(),
			Year = Faker.Random.UInt(),
			Duration = Faker.Random.UInt(),
			Tagline = Faker.Lorem.Sentence(),
			Summary = Faker.Lorem.Paragraphs(2),
			InCinemas = Faker.Random.Bool(),
			PosterUrl = Faker.Internet.Url(),
			PosterId = Faker.Random.String(10),
			BackdropUrl = Faker.Internet.Url(),
			BackdropId = Faker.Random.String(10),
			TrailerUrl = Faker.Internet.Url()
		};

		Repository.Add(movie);
		await UnitOfWork.SaveAsync();

		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + 1));
	}

	[Test]
	public async Task Given_Movies_When_AddRangeAsync_Then_SaveMovies()
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var movies = new List<Movie>(MovieUtils.RandomMovieIds.Length);
		movies.AddRange(MovieUtils.RandomMovieIds
			.Select(id => new Movie
			{
				Id = id,
				Title = Faker.Lorem.Word(),
				Year = Faker.Random.UInt(),
				Duration = Faker.Random.UInt(),
				Tagline = Faker.Lorem.Sentence(),
				Summary = Faker.Lorem.Paragraphs(2),
				InCinemas = Faker.Random.Bool(),
				PosterUrl = Faker.Internet.Url(),
				PosterId = Faker.Random.String(10),
				BackdropUrl = Faker.Internet.Url(),
				BackdropId = Faker.Random.String(10),
				TrailerUrl = Faker.Internet.Url()
			})
		);

		Repository.AddRange(movies);
		await UnitOfWork.SaveAsync();

		var newCount = await Repository.CountAsync(EmptySpecification);
		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + MovieUtils.RandomMovieIds.Length));
	}

	[TestCaseSource(typeof(MovieFakeData), nameof(MovieFakeData.MovieIds))]
	public async Task Given_MovieId_When_GetAsync_Then_ReturnMovie(string id)
	{
		Specification.AddFilter(m => m.Id == id);
		var movie = await Repository.GetAsync(Specification);

		Assert.That(movie, Is.Not.Null);
		Assert.That(movie!.Id, Is.EqualTo(id));
	}

	[TestCaseSource(typeof(MovieFakeData), nameof(MovieFakeData.MovieIds))]
	public async Task Assert_MovieHasGenres(string id)
	{
		Specification.AddFilter(m => m.Id == id);
		Specification.AddInclude(im => im
			.Include(m => m.MovieGenres)
			.ThenInclude(mg => mg.Genre)
		);
		var movie = await Repository.GetAsync(Specification);

		Assert.That(movie, Is.Not.Null);
		Assert.That(movie!.MovieGenres, Is.Not.Null);
		Assert.That(movie.MovieGenres, Is.Not.Empty);
		foreach (var movieMovie in movie.MovieGenres)
		{
			Assert.That(movieMovie.Genre.Id, Is.Not.EqualTo(Guid.Empty));
			Assert.That(movieMovie.GenreId, Is.Not.EqualTo(Guid.Empty));
			Assert.That(movieMovie.Movie.Id, Is.EqualTo(movieMovie.MovieId));
			Assert.That(id, Is.EqualTo(movieMovie.Movie.Id));
		}
	}

	[TestCaseSource(typeof(MovieFakeData), nameof(MovieFakeData.MovieIds))]
	public async Task Assert_MovieHasFilmRating(string id)
	{
		Specification.AddFilter(m => m.Id == id);
		Specification.AddInclude(im => im
			.Include(m => m.FilmRating)
		);
		var movie = await Repository.GetAsync(Specification);

		Assert.That(movie, Is.Not.Null);
		Assert.That(movie!.FilmRating, Is.Not.Null);
		Assert.That(movie.FilmRating.Id, Is.EqualTo(movie.FilmRatingId));
	}

	[TestCase("wrong-id-1")]
	[TestCase("abcd-0000")]
	public async Task Given_InvalidId_When_GetAsync_Then_ReturnNull(string id)
	{
		Specification.AddFilter(m => m.Id == id);
		var movie = await Repository.GetAsync(Specification);

		Assert.That(movie, Is.Null);
	}

	[TestCaseSource(typeof(MovieFakeData), nameof(MovieFakeData.MovieIds))]
	public async Task Given_Title_When_UpdateAsync_Then_UpdateMovieTitle(string id)
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);
		var title = Faker.Lorem.Sentence(2);

		Specification.AddFilter(m => m.Id == id);
		var movie = await Repository.GetAsync(Specification);
		Assert.That(movie, Is.Not.Null);
		Assert.That(movie!.Title, Is.Not.EqualTo(title));

		movie.Title = title;
		movie.ModifiedAt = Clock.GetCurrentInstant();
		Repository.Update(movie);
		await UnitOfWork.SaveAsync();

		Specification.AddFilter(m => m.Title == title);
		var updatedMovie = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(updatedMovie, Is.Not.Null);
		Assert.That(updatedMovie, Is.Not.EqualTo(movie));
		Assert.That(title, Is.EqualTo(updatedMovie!.Title));
		Assert.That(initialCount, Is.EqualTo(newCount));
	}

	[TestCaseSource(typeof(MovieFakeData), nameof(MovieFakeData.MovieIds))]
	public async Task Given_MovieId_When_DeleteAsync_Then_DeleteMovie(string id)
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		Specification.AddFilter(m => m.Id == id);
		var movie = await Repository.GetAsync(Specification);
		Assert.That(movie, Is.Not.Null);

		Repository.Delete(movie!);
		await UnitOfWork.SaveAsync();

		var deletedMovie = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(deletedMovie, Is.Null);
		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - 1));
	}

	[Test]
	public async Task Given_MovieIds_When_DeleteRangeAsync_Then_DeleteMovies()
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var movies = new List<Movie>(MovieFakeData.MovieIds.Count);
		foreach (string id in MovieFakeData.MovieIds)
		{
			Specification.AddFilter(m => m.Id == id);
			var movie = await Repository.GetAsync(Specification);
			Assert.That(movie, Is.Not.Null);
			movies.Add(movie!);
		}

		Repository.DeleteRange(movies);
		await UnitOfWork.SaveAsync();
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - MovieFakeData.MovieIds.Count));
	}
}
#endif
