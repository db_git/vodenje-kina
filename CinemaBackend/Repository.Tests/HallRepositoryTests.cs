//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Repository.Tests;

[TestFixture]
public sealed class HallRepositoryTests : RepositoryTestBase<Hall, HallRepository, HallInMemoryContext>
{
	[TestCaseSource(typeof(HallUtils), nameof(HallUtils.RandomHallNames))]
	public async Task Given_Hall_When_AddAsync_Then_SaveHall(string name)
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var hall = new Hall { Name = name };

		Repository.Add(hall);
		await UnitOfWork.SaveAsync();

		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + 1));
	}

	[Test]
	public async Task Given_Halls_When_AddRangeAsync_Then_SaveHalls()
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var halls = new List<Hall>(HallUtils.RandomHallNames.Length);
		halls.AddRange(HallUtils.RandomHallNames
			.Select(name => new Hall { Name = name })
		);

		Repository.AddRange(halls);
		await UnitOfWork.SaveAsync();

		var newCount = await Repository.CountAsync(EmptySpecification);
		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + HallUtils.RandomHallNames.Length));
	}

	[TestCaseSource(typeof(HallFakeData), nameof(HallFakeData.HallIds))]
	public async Task Given_HallId_When_GetAsync_Then_ReturnHall(Guid id)
	{
		Specification.AddFilter(h => h.Id == id);
		var hall = await Repository.GetAsync(Specification);

		Assert.That(hall, Is.Not.Null);
		Assert.That(hall!.Id, Is.EqualTo(id));
	}

	[TestCaseSource(typeof(GuidUtils), nameof(GuidUtils.NonexistentGuids))]
	public async Task Given_InvalidId_When_GetAsync_Then_ReturnNull(Guid id)
	{
		Specification.AddFilter(h => h.Id == id);
		var hall = await Repository.GetAsync(Specification);

		Assert.That(hall, Is.Null);
	}

	[TestCaseSource(typeof(HallFakeData), nameof(HallFakeData.HallIds))]
	public async Task Assert_HallHasCinema(Guid id)
	{
		Specification.AddFilter(h => h.Id == id);
		Specification.AddInclude(ih => ih
			.Include(h => h.Cinema)
		);
		var hall = await Repository.GetAsync(Specification);

		Assert.That(hall, Is.Not.Null);
		Assert.That(hall!.Cinema, Is.Not.Null);
		Assert.That(hall.CinemaId, Is.Not.EqualTo(Guid.Empty));
		Assert.That(hall.Cinema.Id, Is.Not.EqualTo(Guid.Empty));
		Assert.That(hall.Cinema.Halls.Any(h => h.Id == id), Is.True);
	}

	[TestCaseSource(typeof(HallFakeData), nameof(HallFakeData.HallIds))]
	public async Task Given_Name_When_UpdateAsync_Then_UpdateHallName(Guid id)
	{
		var name = Faker.Lorem.Sentence(2);
		var initialCount = await Repository.CountAsync(EmptySpecification);

		Specification.AddFilter(h => h.Id == id);
		var hall = await Repository.GetAsync(Specification);
		Assert.That(hall, Is.Not.Null);
		Assert.That(hall!.Name, Is.Not.EqualTo(name));

		hall.Name = name;
		hall.ModifiedAt = Clock.GetCurrentInstant();
		Repository.Update(hall);
		await UnitOfWork.SaveAsync();

		Specification.AddFilter(h => h.Name == name);
		var updatedHall = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(updatedHall, Is.Not.Null);
		Assert.That(updatedHall, Is.Not.EqualTo(hall));
		Assert.That(name, Is.EqualTo(updatedHall!.Name));
		Assert.That(initialCount, Is.EqualTo(newCount));
	}

	[TestCaseSource(typeof(HallFakeData), nameof(HallFakeData.HallIds))]
	public async Task Given_HallId_When_DeleteAsync_Then_DeleteHall(Guid id)
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		Specification.AddFilter(h => h.Id == id);
		var hall = await Repository.GetAsync(Specification);
		Assert.That(hall, Is.Not.Null);

		Repository.Delete(hall!);
		await UnitOfWork.SaveAsync();

		var deletedHall = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(deletedHall, Is.Null);
		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - 1));
	}

	[Test]
	public async Task Given_HallIds_When_DeleteRangeAsync_Then_DeleteHalls()
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var halls = new List<Hall>(HallFakeData.HallIds.Count);
		foreach (Guid id in HallFakeData.HallIds)
		{
			Specification.AddFilter(h => h.Id == id);
			var hall = await Repository.GetAsync(Specification);
			Assert.That(hall, Is.Not.Null);
			halls.Add(hall!);
		}

		Repository.DeleteRange(halls);
		await UnitOfWork.SaveAsync();
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - HallFakeData.HallIds.Count));
	}
}
#endif
