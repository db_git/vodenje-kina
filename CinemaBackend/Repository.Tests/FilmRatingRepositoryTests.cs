//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Repository.Tests;

[TestFixture]
public sealed class FilmRatingRepositoryTests : RepositoryTestBase<FilmRating, FilmRatingRepository, FilmRatingInMemoryContext>
{
	[TestCaseSource(typeof(FilmRatingUtils), nameof(FilmRatingUtils.RandomFilmRatingTypes))]
	public async Task Given_FilmRating_When_AddAsync_Then_SaveFilmRating(string type)
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var filmRating = new FilmRating { Type = type, Description = Faker.Lorem.Sentence(2) };

		Repository.Add(filmRating);
		await UnitOfWork.SaveAsync();

		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + 1));
	}

	[Test]
	public async Task Given_FilmRatings_When_AddRangeAsync_Then_SaveFilmRatings()
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var filmRatings = new List<FilmRating>(FilmRatingUtils.RandomFilmRatingTypes.Length);
		filmRatings.AddRange(FilmRatingUtils.RandomFilmRatingTypes
			.Select(type => new FilmRating
			{
				Type = type,
				Description = Faker.Lorem.Sentence(2)
			})
		);

		Repository.AddRange(filmRatings);
		await UnitOfWork.SaveAsync();

		var newCount = await Repository.CountAsync(EmptySpecification);
		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + FilmRatingUtils.RandomFilmRatingTypes.Length));
	}

	[TestCaseSource(typeof(FilmRatingFakeData), nameof(FilmRatingFakeData.FilmRatingIds))]
	public async Task Given_FilmRatingId_When_GetAsync_Then_ReturnFilmRating(Guid id)
	{
		Specification.AddFilter(fr => fr.Id == id);
		var filmRating = await Repository.GetAsync(Specification);

		Assert.That(filmRating, Is.Not.Null);
		Assert.That(filmRating!.Id, Is.EqualTo(id));
	}

	[TestCaseSource(typeof(GuidUtils), nameof(GuidUtils.NonexistentGuids))]
	public async Task Given_InvalidId_When_GetAsync_Then_ReturnNull(Guid id)
	{
		Specification.AddFilter(fr => fr.Id == id);
		var filmRating = await Repository.GetAsync(Specification);

		Assert.That(filmRating, Is.Null);
	}

	[TestCaseSource(typeof(FilmRatingFakeData), nameof(FilmRatingFakeData.FilmRatingIds))]
	[TestCaseSource(typeof(FilmRatingFakeData), nameof(FilmRatingFakeData.FilmRatingIds))]
	public async Task Given_TypeAndDescription_When_UpdateAsync_Then_UpdateFilmRatingTypeAndDescription(Guid id)
	{
		var type = Faker.Lorem.Sentence(1);
		var description = Faker.Lorem.Sentence(2);
		var initialCount = await Repository.CountAsync(EmptySpecification);

		Specification.AddFilter(fr => fr.Id == id);
		var filmRating = await Repository.GetAsync(Specification);
		Assert.That(filmRating, Is.Not.Null);
		Assert.That(filmRating!.Type, Is.Not.EqualTo(type));

		filmRating.Type = type;
		filmRating.Description = description;
		filmRating.ModifiedAt = Clock.GetCurrentInstant();
		Repository.Update(filmRating);
		await UnitOfWork.SaveAsync();

		Specification.AddFilter(fr => fr.Type == type);
		var updatedFilmRating = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(updatedFilmRating, Is.Not.Null);
		Assert.That(updatedFilmRating, Is.Not.EqualTo(filmRating));
		Assert.That(type, Is.EqualTo(updatedFilmRating!.Type));
		Assert.That(description, Is.EqualTo(updatedFilmRating.Description));
		Assert.That(initialCount, Is.EqualTo(newCount));
	}

	[TestCaseSource(typeof(FilmRatingFakeData), nameof(FilmRatingFakeData.FilmRatingIds))]
	public async Task Given_FilmRatingId_When_DeleteAsync_Then_DeleteFilmRating(Guid id)
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		Specification.AddFilter(fr => fr.Id == id);
		var filmRating = await Repository.GetAsync(Specification);
		Assert.That(filmRating, Is.Not.Null);

		Repository.Delete(filmRating!);
		await UnitOfWork.SaveAsync();

		var deletedFilmRating = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(deletedFilmRating, Is.Null);
		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - 1));
	}

	[Test]
	public async Task Given_FilmRatingIds_When_DeleteRangeAsync_Then_DeleteFilmRatings()
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var filmRatings = new List<FilmRating>(FilmRatingFakeData.FilmRatingIds.Count);
		foreach (Guid id in FilmRatingFakeData.FilmRatingIds)
		{
			Specification.AddFilter(fr => fr.Id == id);
			var filmRating = await Repository.GetAsync(Specification);
			Assert.That(filmRating, Is.Not.Null);
			filmRatings.Add(filmRating!);
		}

		Repository.DeleteRange(filmRatings);
		await UnitOfWork.SaveAsync();
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - FilmRatingFakeData.FilmRatingIds.Count));
	}
}
#endif
