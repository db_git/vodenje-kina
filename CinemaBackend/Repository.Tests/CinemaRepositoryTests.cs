//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Repository.Tests;

[TestFixture]
public sealed class CinemaRepositoryTests : RepositoryTestBase<Cinema, CinemaRepository, CinemaInMemoryContext>
{
	[TestCaseSource(typeof(CinemaUtils), nameof(CinemaUtils.RandomCinemaNames))]
	public async Task Given_Cinema_When_AddAsync_Then_SaveCinema(string name)
	{
		var initialCount = await Repository.CountAsync(Specification);

		var cinema = new Cinema
		{
			Name = name,
			City = Faker.Address.City(),
			Street = Faker.Address.StreetAddress(),
			TicketPrice = 10
		};

		Repository.Add(cinema);
		await UnitOfWork.SaveAsync();

		var newCount = await Repository.CountAsync(Specification);

		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + 1));
	}

	[Test]
	public async Task Given_Cinemas_When_AddRangeAsync_Then_SaveCinemas()
	{
		var initialCount = await Repository.CountAsync(Specification);

		var cinemas = new List<Cinema>(CinemaUtils.RandomCinemaNames.Length);
		cinemas.AddRange(CinemaUtils.RandomCinemaNames
			.Select(name => new Cinema
			{
				Name = name,
				City = Faker.Address.City(),
				Street = Faker.Address.StreetAddress(),
				TicketPrice = 10
			})
		);

		Repository.AddRange(cinemas);
		await UnitOfWork.SaveAsync();

		var newCount = await Repository.CountAsync(Specification);
		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + CinemaUtils.RandomCinemaNames.Length));
	}

	[TestCaseSource(typeof(CinemaFakeData), nameof(CinemaFakeData.CinemaIds))]
	public async Task Given_CinemaId_When_GetAsync_Then_ReturnCinema(Guid id)
	{
		Specification.AddFilter(c => c.Id == id);
		var cinema = await Repository.GetAsync(Specification);

		Assert.That(cinema, Is.Not.Null);
		Assert.That(cinema!.Id, Is.EqualTo(id));
	}

	[TestCaseSource(typeof(GuidUtils), nameof(GuidUtils.NonexistentGuids))]
	public async Task Given_InvalidId_When_GetAsync_Then_ReturnNull(Guid id)
	{
		Specification.AddFilter(c => c.Id == id);
		var cinema = await Repository.GetAsync(Specification);

		Assert.That(cinema, Is.Null);
	}

	[TestCaseSource(typeof(CinemaFakeData), nameof(CinemaFakeData.CinemaIds))]
	[TestCaseSource(typeof(CinemaFakeData), nameof(CinemaFakeData.CinemaIds))]
	public async Task Given_Name_When_UpdateAsync_Then_UpdateCinemaName(Guid id)
	{
		var name = Faker.Lorem.Sentence(1);
		var initialCount = await Repository.CountAsync(Specification);

		Specification.AddFilter(c => c.Id == id);
		var cinema = await Repository.GetAsync(Specification);
		Assert.That(cinema, Is.Not.Null);
		Assert.That(cinema!.Name, Is.Not.EqualTo(name));

		cinema.Name = name;
		cinema.ModifiedAt = Clock.GetCurrentInstant();
		Repository.Update(cinema);
		await UnitOfWork.SaveAsync();

		Specification.AddFilter(c => c.Name == name);
		var updatedCinema = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(updatedCinema, Is.Not.Null);
		Assert.That(updatedCinema, Is.Not.EqualTo(cinema));
		Assert.That(name, Is.EqualTo(updatedCinema!.Name));
		Assert.That(initialCount, Is.EqualTo(newCount));
	}

	[TestCaseSource(typeof(CinemaFakeData), nameof(CinemaFakeData.CinemaIds))]
	[TestCaseSource(typeof(CinemaFakeData), nameof(CinemaFakeData.CinemaIds))]
	public async Task Given_City_When_UpdateAsync_Then_UpdateCinemaCity(Guid id)
	{
		var city = Faker.Address.CityPrefix() + Faker.Address.City() + Faker.Address.CitySuffix();
		var initialCount = await Repository.CountAsync(Specification);

		Specification.AddFilter(c => c.Id == id);
		var cinema = await Repository.GetAsync(Specification);
		Assert.That(cinema, Is.Not.Null);
		Assert.That(cinema!.City, Is.Not.EqualTo(city));

		cinema.City = city;
		cinema.ModifiedAt = Clock.GetCurrentInstant();
		Repository.Update(cinema);
		await UnitOfWork.SaveAsync();

		Specification.AddFilter(c => c.City == city);
		var updatedCinema = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(updatedCinema, Is.Not.Null);
		Assert.That(updatedCinema, Is.Not.EqualTo(cinema));
		Assert.That(city, Is.EqualTo(updatedCinema!.City));
		Assert.That(initialCount, Is.EqualTo(newCount));
	}

	[TestCaseSource(typeof(CinemaFakeData), nameof(CinemaFakeData.CinemaIds))]
	[TestCaseSource(typeof(CinemaFakeData), nameof(CinemaFakeData.CinemaIds))]
	public async Task Given_Street_When_UpdateAsync_Then_UpdateCinemaStreet(Guid id)
	{
		var street = Faker.Address.StreetAddress(true);
		var initialCount = await Repository.CountAsync(Specification);

		Specification.AddFilter(c => c.Id == id);
		var cinema = await Repository.GetAsync(Specification);
		Assert.That(cinema, Is.Not.Null);
		Assert.That(cinema!.Street, Is.Not.EqualTo(street));

		cinema.Street = street;
		cinema.ModifiedAt = Clock.GetCurrentInstant();
		Repository.Update(cinema);
		await UnitOfWork.SaveAsync();

		Specification.AddFilter(c => c.Street == street);
		var updatedCinema = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(updatedCinema, Is.Not.Null);
		Assert.That(updatedCinema, Is.Not.EqualTo(cinema));
		Assert.That(street, Is.EqualTo(updatedCinema!.Street));
		Assert.That(initialCount, Is.EqualTo(newCount));
	}

	[TestCaseSource(typeof(CinemaFakeData), nameof(CinemaFakeData.CinemaIds))]
	[TestCaseSource(typeof(CinemaFakeData), nameof(CinemaFakeData.CinemaIds))]
	public async Task Given_TicketPrice_When_UpdateAsync_Then_UpdateCinemaTicketPrice(Guid id)
	{
		var price = Faker.Finance.Amount(501M);
		var initialCount = await Repository.CountAsync(Specification);

		Specification.AddFilter(c => c.Id == id);
		var cinema = await Repository.GetAsync(Specification);
		Assert.That(cinema, Is.Not.Null);
		Assert.That(cinema!.TicketPrice, Is.Not.EqualTo(price));

		cinema.TicketPrice = price;
		cinema.ModifiedAt = Clock.GetCurrentInstant();
		Repository.Update(cinema);
		await UnitOfWork.SaveAsync();

		Specification.AddFilter(c => c.TicketPrice == price);
		var updatedCinema = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(updatedCinema, Is.Not.Null);
		Assert.That(updatedCinema, Is.Not.EqualTo(cinema));
		Assert.That(price, Is.EqualTo(updatedCinema!.TicketPrice));
		Assert.That(initialCount, Is.EqualTo(newCount));
	}

	[TestCaseSource(typeof(CinemaFakeData), nameof(CinemaFakeData.CinemaIds))]
	public async Task Given_CinemaId_When_DeleteAsync_Then_DeleteCinema(Guid id)
	{
		var initialCount = await Repository.CountAsync(Specification);

		Specification.AddFilter(c => c.Id == id);
		var cinema = await Repository.GetAsync(Specification);
		Assert.That(cinema, Is.Not.Null);

		Repository.Delete(cinema!);
		await UnitOfWork.SaveAsync();

		var deletedCinema = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(deletedCinema, Is.Null);
		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - 1));
	}

	[Test]
	public async Task Given_CinemaIds_When_DeleteRangeAsync_Then_DeleteCinemas()
	{
		var initialCount = await Repository.CountAsync(Specification);

		var cinemas = new List<Cinema>(CinemaFakeData.CinemaIds.Count);
		foreach (Guid id in CinemaFakeData.CinemaIds)
		{
			Specification.AddFilter(c => c.Id == id);
			var cinema = await Repository.GetAsync(Specification);
			Assert.That(cinema, Is.Not.Null);
			cinemas.Add(cinema!);
		}

		Repository.DeleteRange(cinemas);
		await UnitOfWork.SaveAsync();
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - CinemaFakeData.CinemaIds.Count));
	}
}
#endif
