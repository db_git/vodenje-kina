//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Repository.Tests;

[TestFixture]
public sealed class ProjectionRepositoryTests : RepositoryTestBase<Projection, ProjectionRepository, ProjectionInMemoryContext>
{
	private readonly LocalDateTime _time = LocalDateTime.FromDateTime(DateTime.UtcNow);

	private static Tuple<string, string>[] _movieHallIds =
	{
		new("random-movie-12", "ac9b47a8-17d7-40ec-9072-60162cf54404"),
		new("random-movie-12", "db896936-967b-4484-809d-846a9f22ae81"),
		new("yet-another-movie-movie-3456", "ac9b47a8-17d7-40ec-9072-60162cf54404"),
		new("yet-another-movie-movie-3456", "db896936-967b-4484-809d-846a9f22ae81")
	};

	[TestCaseSource(nameof(_movieHallIds))]
	public async Task Given_Projection_When_AddAsync_Then_SaveProjection(Tuple<string, string> movieHallId)
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var projection = new Projection
		{
			HallId = Guid.Parse(movieHallId.Item2),
			MovieId = movieHallId.Item1,
			Time = _time
		};

		Repository.Add(projection);
		await UnitOfWork.SaveAsync();

		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + 1));
	}

	[Test]
	public async Task Given_Projections_When_AddRangeAsync_Then_SaveProjections()
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var projections = new List<Projection>(_movieHallIds.Length);
		projections.AddRange(_movieHallIds
			.Select(movieHallId => new Projection
			{
				HallId = Guid.Parse(movieHallId.Item2),
				MovieId = movieHallId.Item1,
				Time = _time
			})
		);

		Repository.AddRange(projections);
		await UnitOfWork.SaveAsync();

		var newCount = await Repository.CountAsync(EmptySpecification);
		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + _movieHallIds.Length));
	}

	[TestCaseSource(typeof(ProjectionFakeData), nameof(ProjectionFakeData.ProjectionIds))]
	public async Task Given_ProjectionId_When_GetAsync_Then_ReturnProjection(Guid id)
	{
		Specification.AddFilter(p => p.Id == id);
		var projection = await Repository.GetAsync(Specification);

		Assert.That(projection, Is.Not.Null);
		Assert.That(projection!.Id, Is.EqualTo(id));
	}

	[TestCaseSource(typeof(ProjectionFakeData), nameof(ProjectionFakeData.ProjectionIds))]
	public async Task Assert_ProjectionHasHall(Guid id)
	{
		Specification.AddFilter(p => p.Id == id);
		Specification.AddInclude(ip => ip
			.Include(p => p.Hall)
		);
		var projection = await Repository.GetAsync(Specification);

		Assert.That(projection, Is.Not.Null);
		Assert.That(projection!.Hall, Is.Not.Null);
		Assert.That(projection.Hall.Id, Is.Not.EqualTo(Guid.Empty));
		Assert.That(projection.Hall.Id, Is.EqualTo(projection.HallId));
		Assert.That(projection.Hall.Projections.Any(p => p.Id == id), Is.True);
	}

	[TestCaseSource(typeof(ProjectionFakeData), nameof(ProjectionFakeData.ProjectionIds))]
	public async Task Assert_ProjectionHasMovie(Guid id)
	{
		Specification.AddFilter(p => p.Id == id);
		Specification.AddInclude(ip => ip
			.Include(p => p.Movie)
		);
		var projection = await Repository.GetAsync(Specification);

		Assert.That(projection, Is.Not.Null);
		Assert.That(projection!.Movie, Is.Not.Null);
		Assert.That(projection.Movie.Id, Is.Not.Null);
		Assert.That(projection.Movie.Id, Is.Not.EqualTo(string.Empty));
		Assert.That(projection.Movie.Id, Is.EqualTo(projection.MovieId));
		Assert.That(projection.Movie.Projections.Any(p => p.Id == id), Is.True);
	}

	[TestCaseSource(typeof(GuidUtils), nameof(GuidUtils.NonexistentGuids))]
	public async Task Given_InvalidId_When_GetAsync_Then_ReturnNull(Guid id)
	{
		Specification.AddFilter(p => p.Id == id);
		var projection = await Repository.GetAsync(Specification);

		Assert.That(projection, Is.Null);
	}

	[TestCaseSource(typeof(ProjectionFakeData), nameof(ProjectionFakeData.ProjectionIds))]
	public async Task Given_Time_When_UpdateAsync_Then_UpdateProjectionTime(Guid id)
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		Specification.AddFilter(p => p.Id == id);
		var projection = await Repository.GetAsync(Specification);
		Assert.That(projection, Is.Not.Null);
		Assert.That(projection!.Time, Is.Not.EqualTo(_time));

		projection.Time = _time;
		projection.ModifiedAt = Clock.GetCurrentInstant();
		Repository.Update(projection);
		await UnitOfWork.SaveAsync();

		Specification.AddFilter(p => p.Time == _time);
		var updatedProjection = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(updatedProjection, Is.Not.Null);
		Assert.That(updatedProjection, Is.Not.EqualTo(projection));
		Assert.That(_time, Is.EqualTo(updatedProjection!.Time));
		Assert.That(initialCount, Is.EqualTo(newCount));
	}

	[TestCaseSource(typeof(ProjectionFakeData), nameof(ProjectionFakeData.ProjectionIds))]
	public async Task Given_ProjectionId_When_DeleteAsync_Then_DeleteProjection(Guid id)
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		Specification.AddFilter(p => p.Id == id);
		var projection = await Repository.GetAsync(Specification);
		Assert.That(projection, Is.Not.Null);

		Repository.Delete(projection!);
		await UnitOfWork.SaveAsync();

		var deletedProjection = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(deletedProjection, Is.Null);
		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - 1));
	}

	[Test]
	public async Task Given_ProjectionIds_When_DeleteRangeAsync_Then_DeleteProjections()
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var projections = new List<Projection>(ProjectionFakeData.ProjectionIds.Count);
		foreach (Guid id in ProjectionFakeData.ProjectionIds)
		{
			Specification.AddFilter(p => p.Id == id);
			var projection = await Repository.GetAsync(Specification);
			Assert.That(projection, Is.Not.Null);
			projections.Add(projection!);
		}

		Repository.DeleteRange(projections);
		await UnitOfWork.SaveAsync();
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - ProjectionFakeData.ProjectionIds.Count));
	}
}
#endif
