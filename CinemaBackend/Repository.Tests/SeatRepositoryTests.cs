//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Repository.Tests;

[TestFixture]
public sealed class SeatRepositoryTests : RepositoryTestBase<Seat, SeatRepository, SeatInMemoryContext>
{
	[TestCaseSource(typeof(SeatUtils), nameof(SeatUtils.RandomSeatNumbers))]
	public async Task Given_Seat_When_AddAsync_Then_SaveSeat(uint number)
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var seat = new Seat { Number = number };

		Repository.Add(seat);
		await UnitOfWork.SaveAsync();

		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + 1));
	}

	[Test]
	public async Task Given_Seats_When_AddRangeAsync_Then_SaveSeats()
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var seats = new List<Seat>(SeatUtils.RandomSeatNumbers.Length);
		seats.AddRange(SeatUtils.RandomSeatNumbers
			.Select(number => new Seat { Number = number })
		);

		Repository.AddRange(seats);
		await UnitOfWork.SaveAsync();

		var newCount = await Repository.CountAsync(EmptySpecification);
		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + SeatUtils.RandomSeatNumbers.Length));
	}

	[TestCaseSource(typeof(SeatFakeData), nameof(SeatFakeData.SeatIds))]
	public async Task Given_SeatId_When_GetAsync_Then_ReturnSeat(Guid id)
	{
		Specification.AddFilter(s => s.Id == id);
		var seat = await Repository.GetAsync(Specification);

		Assert.That(seat, Is.Not.Null);
		Assert.That(seat!.Id, Is.EqualTo(id));
	}

	[TestCaseSource(typeof(SeatFakeData), nameof(SeatFakeData.SeatIds))]
	public async Task Assert_SeatHasHall(Guid id)
	{
		Specification.AddFilter(s => s.Id == id);
		Specification.AddInclude(iqs => iqs
			.Include(s => s.HallSeats)
			.ThenInclude(hs => hs.Hall)
		);
		var seat = await Repository.GetAsync(Specification);

		Assert.That(seat, Is.Not.Null);
		Assert.That(seat!.HallSeats, Is.Not.Null);
		Assert.That(seat.HallSeats, Is.Not.Empty);
		foreach (var hallSeat in seat.HallSeats)
		{
			Assert.That(hallSeat.Hall.Id, Is.Not.EqualTo(Guid.Empty));
			Assert.That(hallSeat.HallId, Is.Not.EqualTo(Guid.Empty));
			Assert.That(hallSeat.Hall.Id, Is.EqualTo(hallSeat.HallId));
			Assert.That(id, Is.EqualTo(hallSeat.Seat.Id));
		}
	}

	[TestCaseSource(typeof(GuidUtils), nameof(GuidUtils.NonexistentGuids))]
	public async Task Given_InvalidId_When_GetAsync_Then_ReturnNull(Guid id)
	{
		Specification.AddFilter(s => s.Id == id);
		var seat = await Repository.GetAsync(Specification);

		Assert.That(seat, Is.Null);
	}

	[TestCaseSource(typeof(SeatFakeData), nameof(SeatFakeData.SeatIds))]
	[TestCaseSource(typeof(SeatFakeData), nameof(SeatFakeData.SeatIds))]
	public async Task Given_Number_When_UpdateAsync_Then_UpdateSeatNumber(Guid id)
	{
		var number = Faker.Random.UInt(101, 500);
		var initialCount = await Repository.CountAsync(EmptySpecification);

		Specification.AddFilter(s => s.Id == id);
		var seat = await Repository.GetAsync(Specification);
		Assert.That(seat, Is.Not.Null);
		Assert.That(seat!.Number, Is.Not.EqualTo(number));

		seat.Number = number;
		seat.ModifiedAt = Clock.GetCurrentInstant();

		Repository.Update(seat);
		await UnitOfWork.SaveAsync();

		Specification.AddFilter(s => s.Number == number);
		var updatedSeat = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(updatedSeat, Is.Not.Null);
		Assert.That(updatedSeat, Is.Not.EqualTo(seat));
		Assert.That(number, Is.EqualTo(updatedSeat!.Number));
		Assert.That(initialCount, Is.EqualTo(newCount));
	}

	[TestCaseSource(typeof(SeatFakeData), nameof(SeatFakeData.SeatIds))]
	public async Task Given_SeatId_When_DeleteAsync_Then_DeleteSeat(Guid id)
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		Specification.AddFilter(s => s.Id == id);
		var seat = await Repository.GetAsync(Specification);
		Assert.That(seat, Is.Not.Null);

		Repository.Delete(seat!);
		await UnitOfWork.SaveAsync();

		var deletedSeat = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(deletedSeat, Is.Null);
		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - 1));
	}

	[Test]
	public async Task Given_SeatIds_When_DeleteRangeAsync_Then_DeleteSeats()
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var seats = new List<Seat>(SeatFakeData.SeatIds.Count);
		foreach (Guid id in SeatFakeData.SeatIds)
		{
			Specification.AddFilter(s => s.Id == id);
			var seat = await Repository.GetAsync(Specification);
			Assert.That(seat, Is.Not.Null);
			seats.Add(seat!);
		}

		Repository.DeleteRange(seats);
		await UnitOfWork.SaveAsync();
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - SeatFakeData.SeatIds.Count));
	}
}
#endif
