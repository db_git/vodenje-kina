//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using Person = Domain.Entities.Person;

namespace Repository.Tests;

[TestFixture]
public sealed class PersonRepositoryTests : RepositoryTestBase<Person, PersonRepository, PersonInMemoryContext>
{
	[TestCaseSource(typeof(PersonUtils), nameof(PersonUtils.RandomPersonNames))]
	public async Task Given_Person_When_AddAsync_Then_SavePerson(string name)
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var person = new Person
		{
			Id = name + Faker.Lorem.Sentence(2),
			Name = name,
			DateOfBirth = new LocalDate(),
			ImageUrl = Faker.Internet.Url(),
			ImageId = Faker.Random.String(10),
			Biography = Faker.Lorem.Paragraphs()
		};

		Repository.Add(person);
		await UnitOfWork.SaveAsync();

		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + 1));
	}

	[Test]
	public async Task Given_People_When_AddRangeAsync_Then_SavePeople()
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var people = new List<Person>(PersonUtils.RandomPersonNames.Length);
		people.AddRange(PersonUtils.RandomPersonNames
			.Select(name => new Person
			{
				Id = name + Faker.Lorem.Sentence(2),
				Name = name,
				DateOfBirth = new LocalDate(),
				ImageUrl = Faker.Internet.Url(),
				ImageId = Faker.Random.String(10),
				Biography = Faker.Lorem.Paragraphs()
			})
		);

		Repository.AddRange(people);
		await UnitOfWork.SaveAsync();

		var newCount = await Repository.CountAsync(EmptySpecification);
		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + PersonUtils.RandomPersonNames.Length));
	}

	[TestCaseSource(typeof(PersonFakeData), nameof(PersonFakeData.PeopleIds))]
	public async Task Given_PersonId_When_GetAsync_Then_ReturnPerson(string id)
	{
		Specification.AddFilter(p => p.Id == id);
		var person = await Repository.GetAsync(Specification);

		Assert.That(person, Is.Not.Null);
		Assert.That(person!.Id, Is.EqualTo(id));
	}

	[TestCase("donnie-darko-6")]
	[TestCase("peter-parker-1234")]
	public async Task Given_InvalidId_When_GetAsync_Then_ReturnNull(string id)
	{
		Specification.AddFilter(p => p.Id == id);
		var person = await Repository.GetAsync(Specification);

		Assert.That(person, Is.Null);
	}

	[TestCase("john-doe-123", "John Doe")]
	[TestCase("john-doe-123", "Jakesully")]
	[TestCase("jane-doe-456", "Jane Doe")]
	[TestCase("jane-doe-456", "Neytiri")]
	public async Task Given_Name_When_UpdateAsync_Then_UpdatePersonName(string id, string name)
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		Specification.AddFilter(p => p.Id == id);
		var person = await Repository.GetAsync(Specification);
		Assert.That(person, Is.Not.Null);
		Assert.That(person!.Name, Is.Not.EqualTo(name));

		person.Name = name;
		person.ModifiedAt = Clock.GetCurrentInstant();
		Repository.Update(person);
		await UnitOfWork.SaveAsync();

		Specification.AddFilter(p => p.Name == name);
		var updatedPerson = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(updatedPerson, Is.Not.Null);
		Assert.That(updatedPerson, Is.Not.EqualTo(person));
		Assert.That(name, Is.EqualTo(updatedPerson!.Name));
		Assert.That(initialCount, Is.EqualTo(newCount));
	}

	[TestCaseSource(typeof(PersonFakeData), nameof(PersonFakeData.PeopleIds))]
	public async Task Given_PersonId_When_DeleteAsync_Then_DeletePerson(string id)
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		Specification.AddFilter(p => p.Id == id);
		var person = await Repository.GetAsync(Specification);
		Assert.That(person, Is.Not.Null);

		Repository.Delete(person!);
		await UnitOfWork.SaveAsync();

		var deletedPerson = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(deletedPerson, Is.Null);
		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - 1));
	}

	[Test]
	public async Task Given_PeopleIds_When_DeleteRangeAsync_Then_DeletePeople()
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var people = new List<Person>(PersonFakeData.PeopleIds.Count);
		foreach (string id in PersonFakeData.PeopleIds)
		{
			Specification.AddFilter(p => p.Id == id);
			var person = await Repository.GetAsync(Specification);
			Assert.That(person, Is.Not.Null);
			people.Add(person!);
		}

		Repository.DeleteRange(people);
		await UnitOfWork.SaveAsync();
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - PersonFakeData.PeopleIds.Count));
	}
}
#endif
