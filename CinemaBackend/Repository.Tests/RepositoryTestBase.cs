//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Repository.Tests;

public abstract class RepositoryTestBase<TEntity, TRepository, TContext>
	where TEntity : class
	where TRepository : class
	where TContext : DatabaseContext
{
	private DatabaseContext _dbContext = null!;
	private readonly IMapper _mapper = new Mapper(MapperConfig.GetConfiguration());

	protected IUnitOfWork UnitOfWork = null!;
	protected TRepository Repository = null!;
	protected ISpecification<TEntity> Specification = null!;
	protected readonly ISpecification<TEntity> EmptySpecification = new Specification<TEntity>();
	protected readonly IClock Clock = SystemClock.Instance;
	protected readonly Faker Faker = FakerUtils.GetInstance();

	[SetUp]
	public async Task SetUp()
	{
		_dbContext = (TContext) Activator.CreateInstance(
			typeof(TContext),
			new DbContextOptionsBuilder<TContext>().Options
		)!;

		await _dbContext.Database.EnsureCreatedAsync();

		UnitOfWork = new UnitOfWork(_dbContext, _mapper);
		Specification = new Specification<TEntity>();
		Repository = (TRepository) UnitOfWork
			.GetType()
			.GetProperty(typeof(TRepository).Name)!.GetValue(UnitOfWork)!;
	}

	[TearDown]
	public async Task TearDown()
	{
		await _dbContext.Database.EnsureDeletedAsync();
	}
}
#endif
