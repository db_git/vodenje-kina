//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Repository.Tests;

[TestFixture]
public sealed class SouvenirRepositoryTests : RepositoryTestBase<Souvenir, SouvenirRepository, SouvenirInMemoryContext>
{
	[TestCaseSource(typeof(SouvenirUtils), nameof(SouvenirUtils.RandomSouvenirNames))]
	public async Task Given_Souvenir_When_AddAsync_Then_SaveSouvenir(string name)
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var souvenir = new Souvenir
		{
			Name = name,
			Price = Faker.Finance.Amount(),
			AvailableQuantity = Faker.Random.ULong(),
			ImageUrl = Faker.Internet.Url(),
			ImageId = Faker.Random.String(10)
		};

		Repository.Add(souvenir);
		await UnitOfWork.SaveAsync();

		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + 1));
	}

	[Test]
	public async Task Given_Souvenirs_When_AddRangeAsync_Then_SaveSouvenirs()
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var souvenirs = new List<Souvenir>(SouvenirUtils.RandomSouvenirNames.Length);
		souvenirs.AddRange(SouvenirUtils.RandomSouvenirNames
			.Select(name => new Souvenir
			{
				Name = name,
				Price = Faker.Finance.Amount(),
				AvailableQuantity = Faker.Random.ULong(),
				ImageUrl = Faker.Internet.Url(),
				ImageId = Faker.Random.String(10)
			})
		);

		Repository.AddRange(souvenirs);
		await UnitOfWork.SaveAsync();

		var newCount = await Repository.CountAsync(EmptySpecification);
		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + SouvenirUtils.RandomSouvenirNames.Length));
	}

	[TestCaseSource(typeof(SouvenirFakeData), nameof(SouvenirFakeData.SouvenirIds))]
	public async Task Given_SouvenirId_When_GetAsync_Then_ReturnSouvenir(Guid id)
	{
		Specification.AddFilter(s => s.Id == id);
		var souvenir = await Repository.GetAsync(Specification);

		Assert.That(souvenir, Is.Not.Null);
		Assert.That(souvenir!.Id, Is.EqualTo(id));
	}

	[TestCaseSource(typeof(GuidUtils), nameof(GuidUtils.NonexistentGuids))]
	public async Task Given_InvalidId_When_GetAsync_Then_ReturnNull(Guid id)
	{
		Specification.AddFilter(s => s.Id == id);
		var souvenir = await Repository.GetAsync(Specification);

		Assert.That(souvenir, Is.Null);
	}

	[TestCaseSource(typeof(SouvenirFakeData), nameof(SouvenirFakeData.SouvenirIds))]
	[TestCaseSource(typeof(SouvenirFakeData), nameof(SouvenirFakeData.SouvenirIds))]
	public async Task Given_Name_When_UpdateAsync_Then_UpdateSouvenirName(Guid id)
	{
		var name = Faker.Lorem.Sentence(1);
		var initialCount = await Repository.CountAsync(EmptySpecification);

		Specification.AddFilter(s => s.Id == id);
		var souvenir = await Repository.GetAsync(Specification);
		Assert.That(souvenir, Is.Not.Null);
		Assert.That(souvenir!.Name, Is.Not.EqualTo(name));

		souvenir.Name = name;
		souvenir.ModifiedAt = Clock.GetCurrentInstant();
		Repository.Update(souvenir);
		await UnitOfWork.SaveAsync();

		Specification.AddFilter(s => s.Name == name);
		var updatedSouvenir = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(updatedSouvenir, Is.Not.Null);
		Assert.That(updatedSouvenir, Is.Not.EqualTo(souvenir));
		Assert.That(name, Is.EqualTo(updatedSouvenir!.Name));
		Assert.That(initialCount, Is.EqualTo(newCount));
	}

	[TestCaseSource(typeof(SouvenirFakeData), nameof(SouvenirFakeData.SouvenirIds))]
	[TestCaseSource(typeof(SouvenirFakeData), nameof(SouvenirFakeData.SouvenirIds))]
	public async Task Given_Price_When_UpdateAsync_Then_UpdateSouvenirPrice(Guid id)
	{
		var price = Faker.Finance.Amount(501M);
		var initialCount = await Repository.CountAsync(EmptySpecification);

		Specification.AddFilter(s => s.Id == id);
		var souvenir = await Repository.GetAsync(Specification);
		Assert.That(souvenir, Is.Not.Null);
		Assert.That(souvenir!.Price, Is.Not.EqualTo(price));

		souvenir.Price = price;
		souvenir.ModifiedAt = Clock.GetCurrentInstant();
		Repository.Update(souvenir);
		await UnitOfWork.SaveAsync();

		Specification.AddFilter(s => s.Price == price);
		var updatedSouvenir = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(updatedSouvenir, Is.Not.Null);
		Assert.That(updatedSouvenir, Is.Not.EqualTo(souvenir));
		Assert.That(price, Is.EqualTo(updatedSouvenir!.Price));
		Assert.That(initialCount, Is.EqualTo(newCount));
	}

	[TestCaseSource(typeof(SouvenirFakeData), nameof(SouvenirFakeData.SouvenirIds))]
	public async Task Given_SouvenirId_When_DeleteAsync_Then_DeleteSouvenir(Guid id)
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		Specification.AddFilter(s => s.Id == id);
		var souvenir = await Repository.GetAsync(Specification);
		Assert.That(souvenir, Is.Not.Null);

		Repository.Delete(souvenir!);
		await UnitOfWork.SaveAsync();

		var deletedSouvenir = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(deletedSouvenir, Is.Null);
		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - 1));
	}

	[Test]
	public async Task Given_SouvenirIds_When_DeleteRangeAsync_Then_DeleteSouvenirs()
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var souvenirs = new List<Souvenir>(SouvenirFakeData.SouvenirIds.Count);
		foreach (Guid id in SouvenirFakeData.SouvenirIds)
		{
			Specification.AddFilter(s => s.Id == id);
			var souvenir = await Repository.GetAsync(Specification);
			Assert.That(souvenir, Is.Not.Null);
			souvenirs.Add(souvenir!);
		}

		Repository.DeleteRange(souvenirs);
		await UnitOfWork.SaveAsync();
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - SouvenirFakeData.SouvenirIds.Count));
	}
}
#endif
