//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Repository.Tests;

[TestFixture]
public sealed class FoodRepositoryTests : RepositoryTestBase<Food, FoodRepository, FoodInMemoryContext>
{
	[TestCaseSource(typeof(FoodUtils), nameof(FoodUtils.SpecificFoodTypes))]
	public async Task Given_Food_When_AddAsync_Then_SaveFood(string type)
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var food = new Food
		{
			Name = Faker.Lorem.Word(),
			Price = Faker.Finance.Amount(),
			AvailableQuantity = Faker.Random.ULong(),
			Type = type,
			Size = Faker.Finance.Amount(),
			ImageUrl = Faker.Internet.Url(),
			ImageId = Faker.Random.String(10)
		};

		Repository.Add(food);
		await UnitOfWork.SaveAsync();

		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + 1));
	}

	[Test]
	public async Task Given_Foods_When_AddRangeAsync_Then_SaveFoods()
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var foods = new List<Food>(FoodUtils.SpecificFoodTypes.Length);
		foods.AddRange(FoodUtils.SpecificFoodTypes
			.Select(type => new Food
			{
				Name = Faker.Lorem.Word(),
				Price = Faker.Finance.Amount(),
				AvailableQuantity = Faker.Random.ULong(),
				Type = type,
				Size = Faker.Finance.Amount(),
				ImageUrl = Faker.Internet.Url(),
				ImageId = Faker.Random.String(10)
			})
		);

		Repository.AddRange(foods);
		await UnitOfWork.SaveAsync();

		var newCount = await Repository.CountAsync(EmptySpecification);
		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + FoodUtils.SpecificFoodTypes.Length));
	}

	[TestCaseSource(typeof(FoodFakeData), nameof(FoodFakeData.FoodIds))]
	public async Task Given_FoodId_When_GetAsync_Then_ReturnFood(Guid id)
	{
		Specification.AddFilter(f => f.Id == id);
		var food = await Repository.GetAsync(Specification);

		Assert.That(food, Is.Not.Null);
		Assert.That(food!.Id, Is.EqualTo(id));
	}

	[TestCaseSource(typeof(GuidUtils), nameof(GuidUtils.NonexistentGuids))]
	public async Task Given_InvalidId_When_GetAsync_Then_ReturnNull(Guid id)
	{
		Specification.AddFilter(f => f.Id == id);
		var food = await Repository.GetAsync(Specification);

		Assert.That(food, Is.Null);
	}

	[TestCaseSource(typeof(FoodFakeData), nameof(FoodFakeData.FoodIds))]
	[TestCaseSource(typeof(FoodFakeData), nameof(FoodFakeData.FoodIds))]
	public async Task Given_Name_When_UpdateAsync_Then_UpdateFoodName(Guid id)
	{
		var name = Faker.Lorem.Sentence(1);
		var initialCount = await Repository.CountAsync(EmptySpecification);

		Specification.AddFilter(f => f.Id == id);
		var food = await Repository.GetAsync(Specification);
		Assert.That(food, Is.Not.Null);
		Assert.That(food!.Name, Is.Not.EqualTo(name));

		food.Name = name;
		food.ModifiedAt = Clock.GetCurrentInstant();
		Repository.Update(food);
		await UnitOfWork.SaveAsync();

		Specification.AddFilter(f => f.Name == name);
		var updatedFood = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(updatedFood, Is.Not.Null);
		Assert.That(updatedFood, Is.Not.EqualTo(food));
		Assert.That(name, Is.EqualTo(updatedFood!.Name));
		Assert.That(initialCount, Is.EqualTo(newCount));
	}

	[TestCaseSource(typeof(FoodFakeData), nameof(FoodFakeData.FoodIds))]
	public async Task Given_FoodId_When_DeleteAsync_Then_DeleteFood(Guid id)
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		Specification.AddFilter(f => f.Id == id);
		var food = await Repository.GetAsync(Specification);
		Assert.That(food, Is.Not.Null);

		Repository.Delete(food!);
		await UnitOfWork.SaveAsync();

		var deletedFood = await Repository.GetAsync(Specification);
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(deletedFood, Is.Null);
		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - 1));
	}

	[Test]
	public async Task Given_FoodIds_When_DeleteRangeAsync_Then_DeleteFoods()
	{
		var initialCount = await Repository.CountAsync(EmptySpecification);

		var foods = new List<Food>(FoodFakeData.FoodIds.Count);
		foreach (Guid id in FoodFakeData.FoodIds)
		{
			Specification.AddFilter(f => f.Id == id);
			var food = await Repository.GetAsync(Specification);
			Assert.That(food, Is.Not.Null);
			foods.Add(food!);
		}

		Repository.DeleteRange(foods);
		await UnitOfWork.SaveAsync();
		var newCount = await Repository.CountAsync(EmptySpecification);

		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - FoodFakeData.FoodIds.Count));
	}
}
#endif
