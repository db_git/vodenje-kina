//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
global using JetBrains.Annotations;
#endif

global using System.Net.Mime;
global using API.Responses;
global using API.Responses.Notifications;
global using API.Responses.Validation;
global using AutoMapper;
global using Autofac;
global using Domain.Entities;
global using Domain.Enums;
global using Microsoft.AspNetCore.Authorization;
global using Microsoft.AspNetCore.Mvc;
global using NodaTime;
global using Service.Contracts;
global using Service.Contracts.Filters;
global using Service.Contracts.Storage;
global using Service.Utils;
global using Service.Storage;
