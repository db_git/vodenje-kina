//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using IConfigurationProvider = AutoMapper.IConfigurationProvider;
using Imagekit.Sdk;

namespace API.Extensions.Autofac;

internal sealed class ApiModule : Module
{
	protected override void Load(ContainerBuilder builder)
	{
		builder
			.RegisterType<Jwt>()
			.AsSelf()
			.SingleInstance();

		builder
			.Register(_ => SystemClock.Instance)
			.As<IClock>()
			.SingleInstance();

		builder
			.Register(context => new Mapper(context.Resolve<IConfigurationProvider>()))
			.As<IMapper>()
			.SingleInstance();

		builder
			.Register(context =>
			{
				var configuration = context.Resolve<IConfiguration>();
				var imagekitConfig = configuration.GetSection("ImageKit");

				return new ImagekitClient(
					imagekitConfig.GetSection("PublicKey").Value,
					imagekitConfig.GetSection("PrivateKey").Value,
					imagekitConfig.GetSection("Endpoint").Value
				);
			})
			.As<ImagekitClient>()
			.SingleInstance();
	}
}
