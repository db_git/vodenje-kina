//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using API.DTO.Mappings;
using Domain.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using IConfigurationProvider = AutoMapper.IConfigurationProvider;

namespace API.Extensions.Autofac;

internal sealed class DomainModule : Module
{
	private readonly bool _isDevelopment;

	public DomainModule(bool isDevelopment)
	{
		_isDevelopment = isDevelopment;
	}

	protected override void Load(ContainerBuilder builder)
	{
		builder
			.Register(context =>
			{
				var configuration = context.Resolve<IConfiguration>();
				var loggerFactory = context.Resolve<ILoggerFactory>();

				var connection = configuration.GetConnectionString("POSTGRESQL")!;

				var options = new DbContextOptionsBuilder<DatabaseContext>()
					.UseNpgsql(connection, o => o.UseNodaTime())
					.EnableSensitiveDataLogging(_isDevelopment)
					.EnableDetailedErrors(_isDevelopment)
					.UseLoggerFactory(loggerFactory)
					.LogTo(_ => {}, LogLevel.Information, DbContextLoggerOptions.DefaultWithLocalTime);

				return new DatabaseContext(options.Options);
			})
			.AsSelf()
			.InstancePerLifetimeScope();

		builder.Register(_ => new MapperConfiguration(config =>
			{
				config.AddProfile<CinemaMapper>();
				config.AddProfile<FilmRatingMapper>();
				config.AddProfile<FoodMapper>();
				config.AddProfile<GenreMapper>();
				config.AddProfile<HallMapper>();
				config.AddProfile<MovieMapper>();
				config.AddProfile<PersonMapper>();
				config.AddProfile<ProjectionMapper>();
				config.AddProfile<ReservationMapper>();
				config.AddProfile<SeatMapper>();
				config.AddProfile<SouvenirMapper>();
				config.AddProfile<UserMapper>();
				config.AddProfile<UserCommentMapper>();
				config.AddProfile<UserRatingMapper>();
			}))
			.As<IConfigurationProvider>()
			.SingleInstance();
	}
}
