//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Domain.Entities.Identity;
using Repository.Contracts;
using Repository.Utils;
using Service;
using Service.Contracts.Email;
using Service.Email;

namespace API.Extensions.Autofac;

internal sealed class ServiceModule : Module
{
	protected override void Load(ContainerBuilder builder)
	{
		builder.RegisterType<CinemaQueryService>()
			.As<ICinemaQueryService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<CinemaCommandService>()
			.As<ICinemaCommandService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<Specification<Cinema>>()
			.AsImplementedInterfaces()
			.As<ISpecification<Cinema>>()
			.InstancePerDependency();

		builder.RegisterType<FilmRatingQueryService>()
			.As<IFilmRatingQueryService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<FilmRatingCommandService>()
			.As<IFilmRatingCommandService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<Specification<FilmRating>>()
			.AsImplementedInterfaces()
			.As<ISpecification<FilmRating>>()
			.InstancePerDependency();

		builder.RegisterType<FoodQueryService>()
			.As<IFoodQueryService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<FoodCommandService>()
			.As<IFoodCommandService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<Specification<Food>>()
			.AsImplementedInterfaces()
			.As<ISpecification<Food>>()
			.InstancePerDependency();

		builder.RegisterType<GenreQueryService>()
			.As<IGenreQueryService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<GenreCommandService>()
			.As<IGenreCommandService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<Specification<Genre>>()
			.AsImplementedInterfaces()
			.As<ISpecification<Genre>>()
			.InstancePerDependency();

		builder.RegisterType<HallQueryService>()
			.As<IHallQueryService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<HallCommandService>()
			.As<IHallCommandService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<Specification<Hall>>()
			.AsImplementedInterfaces()
			.As<ISpecification<Hall>>()
			.InstancePerDependency();

		builder.RegisterType<MovieQueryService>()
			.As<IMovieQueryService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<MovieCommandService>()
			.As<IMovieCommandService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<Specification<Movie>>()
			.AsImplementedInterfaces()
			.As<ISpecification<Movie>>()
			.InstancePerDependency();

		builder.RegisterType<PersonQueryService>()
			.As<IPersonQueryService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<PersonCommandService>()
			.As<IPersonCommandService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<Specification<Person>>()
			.AsImplementedInterfaces()
			.As<ISpecification<Person>>()
			.InstancePerDependency();

		builder.RegisterType<ProjectionQueryService>()
			.As<IProjectionQueryService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<ProjectionCommandService>()
			.As<IProjectionCommandService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<Specification<Projection>>()
			.AsImplementedInterfaces()
			.As<ISpecification<Projection>>()
			.InstancePerDependency();

		builder.RegisterType<ReservationQueryService>()
			.As<IReservationQueryService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<ReservationCommandService>()
			.As<IReservationCommandService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<Specification<Reservation>>()
			.AsImplementedInterfaces()
			.As<ISpecification<Reservation>>()
			.InstancePerDependency();

		builder.RegisterType<SeatQueryService>()
			.As<ISeatQueryService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<SeatCommandService>()
			.As<ISeatCommandService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<Specification<Seat>>()
			.AsImplementedInterfaces()
			.As<ISpecification<Seat>>()
			.InstancePerDependency();

		builder.RegisterType<SouvenirQueryService>()
			.As<ISouvenirQueryService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<SouvenirCommandService>()
			.As<ISouvenirCommandService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<Specification<Souvenir>>()
			.AsImplementedInterfaces()
			.As<ISpecification<Souvenir>>()
			.InstancePerDependency();

		builder.RegisterType<UserQueryService>()
			.As<IUserQueryService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<UserCommandService>()
			.As<IUserCommandService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<Specification<User>>()
			.AsImplementedInterfaces()
			.As<ISpecification<User>>()
			.InstancePerDependency();

		builder.RegisterType<UserCommentQueryService>()
			.As<IUserCommentQueryService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<UserCommentCommandService>()
			.As<IUserCommentCommandService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<Specification<UserComment>>()
			.AsImplementedInterfaces()
			.As<ISpecification<UserComment>>()
			.InstancePerDependency();

		builder.RegisterType<UserRatingQueryService>()
			.As<IUserRatingQueryService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<UserRatingCommandService>()
			.As<IUserRatingCommandService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
		builder.RegisterType<Specification<UserRating>>()
			.AsImplementedInterfaces()
			.As<ISpecification<UserRating>>()
			.InstancePerDependency();

		builder.RegisterType<ImageFacade>()
			.As<IImageFacade>()
			.AsImplementedInterfaces()
			.InstancePerDependency();

		builder.RegisterType<EmailSender>()
			.As<IEmailSender>()
			.AsImplementedInterfaces()
			.InstancePerDependency();
	}
}
