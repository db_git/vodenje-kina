//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Text.Json;
using System.Text.Json.Serialization;
using API.DTO.Converters;
using NodaTime.Serialization.SystemTextJson;

namespace API.Extensions;

internal static class JsonOptions
{
	private static JsonSerializerOptions? _options;

	public static JsonSerializerOptions GetOptions()
	{
		return _options ??= new JsonSerializerOptions
		{
			PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
			NumberHandling = JsonNumberHandling.Strict,
			AllowTrailingCommas = false,
			PropertyNameCaseInsensitive = true,
			ReferenceHandler = ReferenceHandler.IgnoreCycles,
			Converters =
			{
				NodaConverters.InstantConverter,
				NodaConverters.LocalDateConverter,
				new JsonConverterLocalDateTime()
			}
		};
	}
}
