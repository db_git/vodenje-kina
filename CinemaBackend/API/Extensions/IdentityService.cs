//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Domain.Context;
using Domain.Entities.Identity;
using Microsoft.AspNetCore.Identity;

namespace API.Extensions;

/// <remarks>
///     <c>IdentityService</c> class code was based on official documentation.
///     <see href="https://docs.microsoft.com/en-us/aspnet/core/security/authentication/identity">
///         Introduction to Identity on ASP.NET Core
///     </see>
/// </remarks>
internal static class IdentityService
{
	public static IServiceCollection ConfigureIdentity(this IServiceCollection services)
	{
		return services
			.AddIdentity<User, Role>(options =>
			{
				options.Password.RequireDigit = true;
				options.Password.RequireLowercase = false;
				options.Password.RequireNonAlphanumeric = false;
				options.Password.RequireUppercase = false;
				options.Password.RequiredLength = 12;

				options.SignIn.RequireConfirmedEmail = true;
				options.SignIn.RequireConfirmedPhoneNumber = false;

				options.User.AllowedUserNameCharacters =
					"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
				options.User.RequireUniqueEmail = true;
			})
			.AddEntityFrameworkStores<DatabaseContext>()
			.AddRoles<Role>()
			.AddDefaultTokenProviders().Services
			.Configure<PasswordHasherOptions>(options =>
			{
				options.IterationCount = 16384;
			})
			.Configure<DataProtectionTokenProviderOptions>(options =>
			{
				options.TokenLifespan = TimeSpan.FromHours(24);
			});
	}
}
