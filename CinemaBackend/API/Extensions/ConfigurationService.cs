//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace API.Extensions;

public static class ConfigurationService
{
	public static IServiceCollection AssertValidConfiguration(
		this IServiceCollection services,
		IConfiguration configuration
	)
	{
		var corsConfig = configuration.GetSection("Cors");
		var connection = configuration.GetConnectionString("POSTGRESQL");
		var jwtConfig = configuration.GetSection("Jwt");
		var imagekitConfig = configuration.GetSection("ImageKit");
		var sendGridConfig = configuration.GetSection("SendGrid");

		if (corsConfig.Exists() is false || corsConfig.GetSection("Origin").Value is null)
			throw new ArgumentException("Missing cross origin.");

		if (connection is null)
			throw new ArgumentException("Missing connection string 'POSTGRESQL'.");

		if (
			jwtConfig.Exists() is false ||
			jwtConfig.GetSection("Issuer").Value is null ||
			jwtConfig.GetSection("Key").Value is null ||
			jwtConfig.GetSection("Lifetime").Value is null
		) throw new ArgumentException("Missing some or all JWT options.");

		if (
			imagekitConfig.Exists() is false ||
			imagekitConfig.GetSection("PublicKey").Value is null ||
			imagekitConfig.GetSection("PrivateKey").Value is null ||
			imagekitConfig.GetSection("Endpoint").Value is null
		) throw new ArgumentException("Missing some or all ImageKit options.");

		if (
			sendGridConfig.Exists() is false ||
			sendGridConfig.GetSection("ApiKey").Value is null ||
			sendGridConfig.GetSection("EmailVerificationLink").Value is null ||
			sendGridConfig.GetSection("PasswordResetLink").Value is null ||
			sendGridConfig.GetSection("SenderEmail").Value is null ||
			sendGridConfig.GetSection("SenderName").Value is null ||
			sendGridConfig.GetSection("EmailVerificationTemplateId").Value is null ||
			sendGridConfig.GetSection("PasswordResetTemplateId").Value is null
		) throw new ArgumentException("Missing some or all SendGrid options.");

		return services;
	}
}
