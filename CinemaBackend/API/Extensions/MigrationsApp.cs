//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Autofac.Extensions.DependencyInjection;
using Domain.Context;
using Microsoft.EntityFrameworkCore;

namespace API.Extensions;

internal static class MigrationsApp
{
	public static IApplicationBuilder ApplyPendingMigrations(this WebApplication app)
	{
		using var scope = app.Services.CreateScope();
		var services = scope.ServiceProvider;
		var container = services.GetAutofacRoot();

		using var autofacScope = container.BeginLifetimeScope();
		var context = autofacScope.Resolve<DatabaseContext>();
		if (context.Database.GetPendingMigrations().Any()) context.Database.Migrate();

		return app;
	}
}
