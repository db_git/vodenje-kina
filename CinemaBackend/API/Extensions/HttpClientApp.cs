//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace API.Extensions;

internal static class HttpClientApp
{
	public static HttpClient UseHttpClient(this WebApplicationBuilder builder)
	{
		HttpClient client;

		if (builder.Environment.IsDevelopment())
		{
			var handler = new HttpClientHandler();
			handler.ServerCertificateCustomValidationCallback =
				HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;
			client = new HttpClient(handler);
		}
		else
		{
			client = new HttpClient();
		}

		return client;
	}
}
