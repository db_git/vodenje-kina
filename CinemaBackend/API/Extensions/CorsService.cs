//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace API.Extensions;

internal static class CorsService
{
	public static IServiceCollection ConfigureDefaultCors(this IServiceCollection services, IConfiguration configuration)
	{
		services.AddCors(c => c
			.AddPolicy("CorsPolicy", b => b
				.WithOrigins(configuration.GetSection("Cors").GetSection("Origin").Value!)
				.WithMethods("GET", "PATCH", "POST", "PUT", "DELETE")
				.AllowAnyHeader()
			)
		);

		return services;
	}
}
