//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Options;

namespace API.Extensions;

internal static class ControllerService
{
	public static IServiceCollection ConfigureControllers(this IServiceCollection services)
	{
		var builder = new ServiceCollection()
			.AddLogging()
			.AddMvc()
			.AddNewtonsoftJson()
			.Services
			.BuildServiceProvider();

		var jsonPatchFormater = builder
			.GetRequiredService<IOptions<MvcOptions>>()
			.Value
			.InputFormatters
			.OfType<NewtonsoftJsonPatchInputFormatter>()
			.First();

		services
			.AddControllers(options =>
			{
				options.Filters.Add(new ValidationModelAttribute());
				options.InputFormatters.Insert(0, jsonPatchFormater);
			})
			.AddJsonOptions(options =>
			{
				var jsonOptions = JsonOptions.GetOptions();

				options.JsonSerializerOptions.PropertyNamingPolicy = jsonOptions.PropertyNamingPolicy;
				options.JsonSerializerOptions.NumberHandling = jsonOptions.NumberHandling;
				options.JsonSerializerOptions.AllowTrailingCommas = jsonOptions.AllowTrailingCommas;
				options.JsonSerializerOptions.ReferenceHandler = jsonOptions.ReferenceHandler;

				options.JsonSerializerOptions.PropertyNameCaseInsensitive =
					jsonOptions.PropertyNameCaseInsensitive;

				foreach (var converter in jsonOptions.Converters)
					options.JsonSerializerOptions.Converters.Add(converter);
			});

		return services;
	}
}
