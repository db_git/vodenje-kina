//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Reflection;
using System.Text.Json;
using Imagekit.Helper;
using ImageMagick;
using Microsoft.AspNetCore.JsonPatch.Exceptions;
using Service.Contracts.Exceptions;
using JsonOptions = API.Extensions.JsonOptions;

namespace API.Middleware;

internal sealed class ExceptionMiddleware
{
	private readonly RequestDelegate _next;
	private readonly ILogger<ExceptionMiddleware> _logger;

	private static readonly Type[] _contextParameter = { typeof(HttpContext) };
	private static readonly Type[] _exceptionContextParameters = { typeof(HttpContext), typeof(Exception) };

	public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger)
	{
		_next = next;
		_logger = logger;
	}

	public async Task Invoke(HttpContext context)
	{
		try
		{
			await _next(context);
		}
		catch (Exception exception)
		{
			context.Response.ContentType = MediaTypeNames.Application.Json;
			await HandleException(context, exception);
		}
	}

	private async Task HandleException(HttpContext context, Exception exception)
	{
		var type = exception switch
		{
			// JSON exceptions
			JsonException => typeof(MalformedJsonNotification),
			JsonPatchException => typeof(MalformedJsonNotification),

			// Entity exceptions
			IDuplicateEntityException => typeof(DuplicateEntityNotification),
			IEntityNotFoundException => typeof(EntityNotFoundNotification),

			// Entity specific exceptions
			IProjectionOverlapException => typeof(ProjectionOverlapNotification),
			ISeatTakenException => typeof(SeatTakenNotification),
			IOutOfStockException => typeof(OutOfStockNotification),
			IProjectionTimeException => typeof(ProjectionTimeNotification),

			// Identity exceptions
			IInvalidCredentialsException => typeof(InvalidCredentialsNotification),
			IEmailAlreadyInUseException => typeof(EmailAlreadyInUseNotification),
			IEmailNotConfirmedException => typeof(EmailNotConfirmedNotification),
			IRegistrationException => typeof(RegistrationNotification),
			ICodeExpiredException => typeof(CodeExpiredNotification),

			// Email service exceptions
			IEmailSenderException => typeof(EmailSenderNotification),

			// Storage service exceptions
			MagickException => typeof(MagickNotification),
			ImagekitException => typeof(ImageStorageNotification),

			// Other exceptions
			_ => typeof(UnknownErrorNotification)
		};

		_logger.LogWarning(exception, "Exception '{Exception}' was thrown", exception.GetType());

		var notification = CreateResponse(type, context, exception);
		var response = JsonSerializer.Serialize(notification, JsonOptions.GetOptions());
		await context.Response.WriteAsync(response);

		_logger.LogInformation("Exception '{Type}' handled", exception.GetType());
	}

	private static BaseNotification CreateResponse(Type type, HttpContext context, Exception exception)
	{
		var contextOnlyCtor = type.GetConstructor(_contextParameter);
		if (contextOnlyCtor != null)
		{
			return (BaseNotification) contextOnlyCtor.Invoke(
				BindingFlags.Public,
				null,
				new object[] { context },
				null
			);
		}

		var exceptionContextCtor = type.GetConstructor(_exceptionContextParameters);
		return (BaseNotification) exceptionContextCtor!.Invoke(
			BindingFlags.Public,
			null,
			new object[] { context, exception },
			null
		);
	}
}
