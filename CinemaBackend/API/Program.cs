//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using API.Extensions;
using API.Extensions.Autofac;

var builder = WebApplication.CreateBuilder(args);

builder.Configuration
	.SetBasePath(AppContext.BaseDirectory)
	.AddJsonFile(builder.Environment.IsDevelopment() ? "appsettings.Development.json" : "appsettings.json")
	.AddEnvironmentVariables();

builder.Services
	.AssertValidConfiguration(builder.Configuration)
	.ConfigureControllers()
	.ConfigureDefaultCors(builder.Configuration)
	.ConfigureIdentity()
	.ConfigureJwt(builder.Configuration)
	.ConfigureRateLimiter()
	.AddEndpointsApiExplorer()
	.AddSwaggerOpenApi();

builder.Host
	.ConfigureLogging()
	.ConfigureAutofac(builder);

var app = builder.Build();

app
	.UseDefaultCors()
	.UseAuthentication()
	.UseRouting()
	.UseAuthorization();

app.MapControllers().RequireRateLimiting("fixed");
app.ApplyRateLimiter();
app.UseSwaggerOpenApi();
app.UseGlobalExceptionMiddleware();
app.ApplyPendingMigrations();

app.Run();
