//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Security.Claims;
using API.DTO.Request.Reservation;
using API.DTO.Response.Reservation;

namespace API.Controllers;

[Route("api/reservations")]
[Produces(MediaTypeNames.Application.Json)]
public sealed class ReservationController : ControllerBase
{
	private readonly IReservationQueryService _queryService;
	private readonly IReservationCommandService _commandService;
	private readonly IMapper _mapper;

	public ReservationController(
		IReservationQueryService queryService,
		IReservationCommandService commandService,
		IMapper mapper
	)
	{
		_queryService = queryService;
		_commandService = commandService;
		_mapper = mapper;
	}

	/// <summary>
	///     Gets users reservations.
	/// </summary>
	/// <param name="filter">
	///     Filter containing pageable information.
	/// </param>
	/// <returns>
	///     Users reservations.
	/// </returns>
	/// <response code="200">
	///     Reservations found.
	/// </response>
	/// <response code="204">
	///     Reservations are not found.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[Authorize]
	[Route("account")]
	public async Task<IActionResult> GetAllAccountAsync([FromQuery] PageableFilter filter)
	{
		var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
		if (userId is null) return StatusCode(StatusCodes.Status401Unauthorized);

		var reservationFilter = new ReservationFilter
		{
			Page = filter.Page,
			Size = filter.Size,
			User = Guid.Parse(userId)
		};

		var reservationsCount = await _queryService.CountAsync(reservationFilter);
		if (reservationsCount is 0) return StatusCode(StatusCodes.Status204NoContent);

		var reservations = await _queryService.GetAllAsync<ReservationDto>(reservationFilter);
		var pageable = new Page<ReservationDto>(reservationFilter, reservationsCount, reservations);

		return StatusCode(StatusCodes.Status200OK, pageable);
	}

	/// <summary>
	///     Creates reservations.
	/// </summary>
	/// <param name="dtos">
	///     DTOs required for creation.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="201">
	///     Reservations successfully created.
	/// </response>
	/// <response code="400">
	///     Food or souvenir is out of stock.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="404">
	///     Seat not found. Food not found. Souvenir not found.
	/// </response>
	/// <response code="406">
	///     Reservation is not made 30 minutes before projection.
	/// </response>
	/// <response code="409">
	///     Seats are taken.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPost]
	[Authorize]
	[ProducesResponseType(StatusCodes.Status201Created)]
	[ProducesResponseType(typeof(OutOfStockNotification), StatusCodes.Status400BadRequest)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(ProjectionTimeNotification), StatusCodes.Status406NotAcceptable)]
	[ProducesResponseType(typeof(SeatTakenNotification), StatusCodes.Status409Conflict)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> CreateReservationRangeAsync([FromBody] IEnumerable<CreateReservationDto> dtos)
	{
		var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
		if (userId is null) return StatusCode(StatusCodes.Status401Unauthorized);

		var reservations = _mapper.Map<IEnumerable<Reservation>>(dtos).ToList();
		foreach (var reservation in reservations) reservation.UserId = Guid.Parse(userId);

		await _commandService.AddRangeAsync(reservations);
		return StatusCode(StatusCodes.Status201Created);
	}
}
