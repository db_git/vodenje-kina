//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Security.Claims;
using API.DTO.Request.User;
using API.DTO.Response.User;
using Domain.Entities.Identity;
using Microsoft.AspNetCore.JsonPatch;

namespace API.Controllers;

[Route("api/users")]
[Produces(MediaTypeNames.Application.Json)]
public sealed class UserController : ControllerBase
{
	private readonly IUserCommandService _commandService;
	private readonly IUserQueryService _queryService;
	private readonly IMapper _mapper;
	private readonly Jwt _jwt;
	private readonly IImageFacade _image;

	public UserController(
		IUserQueryService queryService,
		IUserCommandService commandService,
		IMapper mapper,
		Jwt jwt,
		IImageFacade image
	)
	{
		_queryService = queryService;
		_commandService = commandService;
		_mapper = mapper;
		_jwt = jwt;
		_image = image;
	}

	/// <summary>
	///     Gets all users.
	/// </summary>
	/// <param name="queryFilter">
	///     Additional query parameters used for sorting, filtering and paging.
	/// </param>
	/// <returns>
	///     <see cref="Page{T}"/> containing page, item and record information.
	/// </returns>
	/// <response code="200">
	///     Users that match the filter are found.
	/// </response>
	/// <response code="204">
	///     Users that match the filter are not found.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[Authorize(Roles = nameof(Roles.Admin))]
	[ProducesResponseType(typeof(Page<UserDto>), StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAllAsync([FromQuery] QueryFilter queryFilter)
	{
		var usersCount = await _queryService.CountAsync(queryFilter);
		if (usersCount is 0) return StatusCode(StatusCodes.Status204NoContent);

		var users = await _queryService.GetAllAsync<UserDto>(queryFilter);
		var pageable = new Page<UserDto>(queryFilter, usersCount, users);

		return StatusCode(StatusCodes.Status200OK, pageable);
	}

	/// <summary>
	///     Gets user information.
	/// </summary>
	/// <returns>
	///     Account information on success.
	/// </returns>
	/// <response code="200">
	///     User is found.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="404">
	///     User doesn't exist.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[Authorize]
	[Route("account")]
	[ProducesResponseType(typeof(UserDto), StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAsync()
	{
		var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
		if (userId is null) return StatusCode(StatusCodes.Status401Unauthorized);

		var user = await _queryService.GetAsync<UserDto>(user => user.Id == Guid.Parse(userId));
		return StatusCode(StatusCodes.Status200OK, user);
	}

	/// <summary>
	///     Apply partial update to existing user.
	/// </summary>
	/// <param name="patch">
	///     DTO required for partial updating.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     User successfully updated.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     User not found.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPatch]
	[Authorize]
	[Consumes("application/json-patch+json")]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> PatchAsync([FromBody] JsonPatchDocument<PatchUserDto> patch)
	{
		var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
		if (userId is null) return StatusCode(StatusCodes.Status401Unauthorized);
		var user = await _queryService.GetAsync(user => user.Id == Guid.Parse(userId), true);

		var dto = new PatchUserDto();
		patch.ApplyTo(dto, ModelState);
		if (!TryValidateModel(dto))
		{
			return StatusCode(
				StatusCodes.Status422UnprocessableEntity,
				new ValidationResultModel(ModelState)
			);
		}

		if (dto.Name is not null) user.Name = dto.Name;
		if (dto.Image is not null)
		{
			var imageResult = await _image.UploadImageAsync(new ImageRequest
			{
				Name = "avatar.webp",
				Width = 200,
				Height = 200,
				Image = dto.Image,
				Path = $"cinema/users/{user.Id}"
			});

			if (user.ImageId is not null)
			{
				await _image.TryDeleteImageAsync(user.ImageId);
			}

			user.ImageId = imageResult.fileId;
			user.ImageUrl = imageResult.url;
		}

		await _commandService.UpdateUserAsync(user);

		return StatusCode(StatusCodes.Status200OK);
	}

	/// <summary>
	///     Verify user email
	/// </summary>
	/// <param name="verifyEmailDto">
	///     DTO required for verification.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     User successfully verified.
	/// </response>
	/// <response code="400">
	///     User not found. Code expired.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	/// <response code="502">
	///     Error with email service.
	/// </response>
	[HttpPost]
	[AllowAnonymous]
	[Route("verify-email")]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(typeof(InvalidCredentialsNotification), StatusCodes.Status400BadRequest)]
	[ProducesResponseType(typeof(CodeExpiredNotification), StatusCodes.Status400BadRequest)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	[ProducesResponseType(typeof(EmailSenderNotification), StatusCodes.Status502BadGateway)]
	public async Task<IActionResult> VerifyEmailAsync([FromBody] VerifyEmailDto verifyEmailDto)
	{
		await _commandService.VerifyEmailAsync(verifyEmailDto.UserId, verifyEmailDto.VerificationCode);
		return StatusCode(StatusCodes.Status200OK);
	}

	/// <summary>
	///     Send password recovery email.
	/// </summary>
	/// <param name="forgotPasswordDto">
	///     A DTO containing the email to send the link to.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Password recovery email successfully sent.
	/// </response>
	/// <response code="400">
	///     User not found.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	/// <response code="502">
	///     Error with email service.
	/// </response>
	[HttpPost]
	[AllowAnonymous]
	[Route("forgot-password")]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(typeof(InvalidCredentialsNotification), StatusCodes.Status400BadRequest)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	[ProducesResponseType(typeof(EmailSenderNotification), StatusCodes.Status502BadGateway)]
	public async Task<IActionResult> ForgotPasswordAsync([FromBody] ForgotPasswordDto forgotPasswordDto)
	{
		await _commandService.ForgotPasswordAsync(forgotPasswordDto.Email);
		return StatusCode(StatusCodes.Status200OK);
	}

	/// <summary>
	///     Reset user password.
	/// </summary>
	/// <param name="resetPasswordDto">
	///     DTO containing email, reset code and new password.
	/// </param>
	/// <returns>
	///     JWT on success.
	/// </returns>
	/// <response code="200">
	///     Password successfully changed.
	/// </response>
	/// <response code="400">
	///     User not found. Code expired.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPost]
	[AllowAnonymous]
	[Route("reset-password")]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(typeof(InvalidCredentialsNotification), StatusCodes.Status400BadRequest)]
	[ProducesResponseType(typeof(CodeExpiredNotification), StatusCodes.Status400BadRequest)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> ResetPasswordAsync([FromBody] ResetPasswordDto resetPasswordDto)
	{
		await _commandService.ResetPasswordAsync(
			resetPasswordDto.Email,
			resetPasswordDto.PasswordResetCode,
			resetPasswordDto.Password
		);

		var user = await _queryService.GetAsync(u => u.Email == resetPasswordDto.Email);
		var response = new UserTokenDto { Token = _jwt.CreateToken(user) };

		return StatusCode(StatusCodes.Status200OK, response);
	}

	/// <summary>
	///     Change user password.
	/// </summary>
	/// <param name="changePasswordDto">
	///     DTO containing both old and new password.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Password successfully changed.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     User not found.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPost]
	[Authorize]
	[Route("change-password")]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> ChangePasswordAsync([FromBody] ChangePasswordDto changePasswordDto)
	{
		var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
		if (userId is null) return StatusCode(StatusCodes.Status401Unauthorized);

		await _commandService.UpdatePasswordAsync(
			Guid.Parse(userId),
			changePasswordDto.OldPassword,
			changePasswordDto.NewPassword
		);

		return StatusCode(StatusCodes.Status200OK);
	}

	/// <summary>
	///     Log in user.
	/// </summary>
	/// <param name="userLoginDto">
	///     DTO required for login.
	/// </param>
	/// <returns>
	///     JWT on success.
	/// </returns>
	/// <response code="200">
	///     User successfully logged in.
	/// </response>
	/// <response code="400">
	///     Invalid credentials provided.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPost]
	[Route("login")]
	[AllowAnonymous]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(typeof(UserTokenDto), StatusCodes.Status200OK)]
	[ProducesResponseType(typeof(InvalidCredentialsNotification), StatusCodes.Status400BadRequest)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> LoginAsync([FromBody] UserLoginDto userLoginDto)
	{
		await _commandService.LoginAsync(userLoginDto.Email, userLoginDto.Password);
		var user = await _queryService.GetAsync(u => u.Email == userLoginDto.Email);

		var response = new UserTokenDto { Token = _jwt.CreateToken(user) };
		return StatusCode(StatusCodes.Status200OK, response);
	}

	/// <summary>
	///     Register new user.
	/// </summary>
	/// <param name="userRegisterDto">
	///     DTO required for creation.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="201">
	///     User successfully registered.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPost]
	[Route("register")]
	[AllowAnonymous]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(typeof(UserTokenDto), StatusCodes.Status201Created)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> RegisterAsync([FromBody] UserRegisterDto userRegisterDto)
	{
		var user = _mapper.Map<User>(userRegisterDto);
		await _commandService.RegisterAsync(user, userRegisterDto.Password);

		return StatusCode(StatusCodes.Status201Created);
	}
}
