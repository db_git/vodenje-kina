//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using API.DTO.Response.Dashboard;
using API.DTO.Response.Movie;
using API.DTO.Response.UserRating;

namespace API.Controllers;

[Route("api/dashboard")]
[Produces(MediaTypeNames.Application.Json)]
public sealed class AdminController : ControllerBase
{
	private readonly IFoodQueryService _foodQueryService;
	private readonly IMovieQueryService _movieQueryService;
	private readonly IReservationQueryService _reservationQueryService;
	private readonly ISouvenirQueryService _souvenirQueryService;
	private readonly IUserQueryService _userQueryService;

	public AdminController(
		IFoodQueryService foodQueryService,
		IMovieQueryService movieQueryService,
		IReservationQueryService reservationQueryService,
		ISouvenirQueryService souvenirQueryService,
		IUserQueryService userQueryService
	)
	{
		_foodQueryService = foodQueryService;
		_movieQueryService = movieQueryService;
		_reservationQueryService = reservationQueryService;
		_souvenirQueryService = souvenirQueryService;
		_userQueryService = userQueryService;
	}

	/// <summary>
	///     Gets revenue statistics.
	/// </summary>
	/// <returns>
	///     Revenue statistics.
	/// </returns>
	/// <response code="200">
	///     Revenue statistics.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[Route("revenue")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[ProducesResponseType(typeof(DashboardRevenueDto), StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetRevenueStatsAsync()
	{
		var previousMonthFoods =
			await _foodQueryService.GetPreviousMonthFoodsAsync<RevenueDto>();

		var currentMonthFoods =
			await _foodQueryService.GetCurrentMonthFoodsAsync<RevenueDto>();

		var previousMonthSouvenirs =
			await _souvenirQueryService.GetPreviousMonthSouvenirsAsync<RevenueDto>();

		var currentMonthSouvenirs =
			await _souvenirQueryService.GetCurrentMonthSouvenirsAsync<RevenueDto>();

		var previousMonthReservations =
			await _reservationQueryService.GetPreviousMonthReservationsAsync<RevenueDto>();

		var currentMonthReservations =
			await _reservationQueryService.GetCurrentMonthReservationsAsync<RevenueDto>();

		var foods = new MonthRevenueDto<FoodRevenueDto>(
			new FoodRevenueDto(previousMonthFoods),
			new FoodRevenueDto(currentMonthFoods)
		);

		var souvenirs = new MonthRevenueDto<SouvenirRevenueDto>(
			new SouvenirRevenueDto(previousMonthSouvenirs),
			new SouvenirRevenueDto(currentMonthSouvenirs)
		);

		var reservations = new MonthRevenueDto<ReservationRevenueDto>(
			new ReservationRevenueDto(previousMonthReservations),
			new ReservationRevenueDto(currentMonthReservations)
		);

		var response = new DashboardRevenueDto(
			foods,
			souvenirs,
			reservations
		);

		return StatusCode(StatusCodes.Status200OK, response);
	}

	/// <summary>
	///     Gets movie statistics.
	/// </summary>
	/// <returns>
	///     Movie statistics.
	/// </returns>
	/// <response code="200">
	///     Movie statistics.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[Route("movies")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[ProducesResponseType(typeof(DashboardMoviesDto), StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetMovieStatsAsync()
	{
		var totalMovies = await _movieQueryService.CountAsync();

		var bestRatedMovies = await _movieQueryService
			.GetBestRatedMoviesAsync<SimpleMovieDto>();

		var worstRatedMovies = await _movieQueryService
			.GetWorstRatedMoviesAsync<SimpleMovieDto>();

		var ratingStars = new List<RatingStarDto>(6)
		{
			new()
			{
				Star = 0,
				Count = await _movieQueryService
					.CountAsync(m => !m.Ratings.Any())
			}
		};
		foreach (var index in Enumerable.Range(1, 5))
		{
			ratingStars.Add(new()
			{
				Star = index,
				Count = await _movieQueryService
					.CountAsync(m => m.Ratings.Any(r => r.Rating == index))
			});
		}

		var response = new DashboardMoviesDto(
			totalMovies,
			bestRatedMovies,
			worstRatedMovies,
			ratingStars
		);

		return StatusCode(StatusCodes.Status200OK, response);
	}

	/// <summary>
	///     Gets user statistics.
	/// </summary>
	/// <returns>
	///     User statistics.
	/// </returns>
	/// <response code="200">
	///     User statistics.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[Route("users")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[ProducesResponseType(typeof(DashboardUsersDto), StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetUserStatsAsync()
	{
		var totalUsers = await _userQueryService.CountAsync();
		var confirmedEmailUsers = await _userQueryService.CountAsync(u => u.EmailConfirmed);
		var unconfirmedEmailUsers = totalUsers - confirmedEmailUsers;
		var usersWithReservations = await _userQueryService.CountAsync(u => u.Reservations.Any());
		var usersWithoutReservations = totalUsers - usersWithReservations;

		var response = new DashboardUsersDto(
			totalUsers,
			confirmedEmailUsers,
			unconfirmedEmailUsers,
			usersWithReservations,
			usersWithoutReservations
		);

		return StatusCode(StatusCodes.Status200OK, response);
	}
}
