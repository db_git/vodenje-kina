//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using API.DTO.Request.Movie;
using API.DTO.Response.Movie;
using Microsoft.AspNetCore.JsonPatch;

namespace API.Controllers;

[Route("api/movies")]
[Produces(MediaTypeNames.Application.Json)]
public sealed class MovieController : ControllerBase
{
	private readonly IMovieCommandService _commandService;
	private readonly IMovieQueryService _queryService;
	private readonly IMapper _mapper;
	private readonly IImageFacade _image;

	public MovieController(
		IMovieQueryService queryService,
		IMovieCommandService commandService,
		IMapper mapper,
		IImageFacade image
	)
	{
		_queryService = queryService;
		_commandService = commandService;
		_mapper = mapper;
		_image = image;
	}

	/// <summary>
	///     Gets all movies.
	/// </summary>
	/// <param name="movieFilter">
	///     Additional query parameters used for sorting, filtering and paging.
	/// </param>
	/// <returns>
	///     <see cref="Page{T}"/> containing page, item and record information.
	/// </returns>
	/// <response code="200">
	///     Movies that match the filter are found.
	/// </response>
	/// <response code="204">
	///     Movies that match the filter are not found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[AllowAnonymous]
	[ProducesResponseType(typeof(Page<MovieDto>), StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAllAsync([FromQuery] MovieFilter movieFilter)
	{
		var moviesCount = await _queryService.CountAsync(movieFilter);
		if (moviesCount is 0) return StatusCode(StatusCodes.Status204NoContent);

		IEnumerable<object> movies;

		if (User.IsInRole(nameof(Roles.Admin)))
		{
			movies = await _queryService.GetAllAsync<AdminMovieDto>(movieFilter);
		}
		else
		{
			movies = await _queryService.GetAllAsync<MovieDto>(movieFilter);
		}

		var pageable = new Page<object>(movieFilter, moviesCount, movies.ToList());

		return StatusCode(StatusCodes.Status200OK, pageable);
	}

	/// <summary>
	///     Gets a specific movie.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for movie.
	/// </param>
	/// <returns>
	///     Requested movie.
	/// </returns>
	/// <response code="200">
	///     Movie is found.
	/// </response>
	/// <response code="404">
	///     Movie doesn't exist.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[Route("{id}")]
	[AllowAnonymous]
	[ProducesResponseType(typeof(IndividualMovieDto), StatusCodes.Status200OK)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAsync(string id)
	{
		var movie = await _queryService.GetAsync<IndividualMovieDto>(movie => movie.Id == id);
		return StatusCode(StatusCodes.Status200OK, movie);
	}

	/// <summary>
	///     Gets featured, new, running and recommended movies to show on home page.
	/// </summary>
	/// <param name="filter">
	///     Additional query parameters used for filtering.
	/// </param>
	/// <returns>
	///     Selected movies.
	/// </returns>
	/// <response code="200">
	///     Returns selected movies.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[Route("home-page")]
	[AllowAnonymous]
	[ProducesResponseType(typeof(HomePageMoviesDto), StatusCodes.Status200OK)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetHomePageMoviesAsync([FromQuery] HomePageMoviesFilter filter)
	{
		var featuredMovies =
			await _queryService.GetFeaturedMoviesAsync<MovieDto>(filter.FeaturedMoviesLimit);
		var newMovies =
			await _queryService.GetNewMoviesAsync<MovieDto>(filter.NewMoviesLimit);
		var runningMovies =
			await _queryService.GetRunningMoviesAsync<MovieDto>(
				filter.RunningMoviesDay,
				filter.RunningMoviesMonth,
				filter.RunningMoviesLimit
			);
		var recommendedMovies =
			await _queryService.GetRecommendedMoviesAsync<MovieDto>(filter.RecommendedMoviesLimit);

		var homePageMovies = new HomePageMoviesDto
		{
			FeaturedMovies = featuredMovies,
			NewMovies = newMovies,
			RunningMovies = runningMovies,
			RecommendedMovies = recommendedMovies
		};

		return StatusCode(StatusCodes.Status200OK, homePageMovies);
	}

	/// <summary>
	///     Recommends similar movies.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for movie.
	/// </param>
	/// <returns>
	///     Similar movies on success.
	/// </returns>
	/// <response code="200">
	///     Recommendations are found.
	/// </response>
	/// <response code="204">
	///     No recommendations are found.
	/// </response>
	/// <response code="404">
	///     Movie doesn't exist.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[Route("{id}/recommendations")]
	[AllowAnonymous]
	[ProducesResponseType(typeof(IEnumerable<MovieDto>), StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetRecommendationsAsync(string id)
	{
		var recommendations = await _queryService.GetRecommendationsAsync<MovieDto>(id);
		if (recommendations.Count() is 0) return StatusCode(StatusCodes.Status204NoContent);

		return StatusCode(StatusCodes.Status200OK, recommendations);
	}

	/// <summary>
	///     Creates a movie.
	/// </summary>
	/// <param name="createMovieDto">
	///     DTO required for creation.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="201">
	///     Movie successfully created.
	/// </response>
	/// <response code="400">
	///     Image conversion failed.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	/// <response code="502">
	///     Error with remote storage.
	/// </response>
	[HttpPost]
	[Authorize(Roles = nameof(Roles.Admin))]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status201Created)]
	[ProducesResponseType(typeof(MagickNotification), StatusCodes.Status400BadRequest)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> AddAsync([FromBody] CreateMovieDto createMovieDto)
	{
		var movie = _mapper.Map<Movie>(createMovieDto);
		movie.Id = await _commandService.GenerateIdFromTitle(movie.Title);

		var posterResult = await _image.UploadImageAsync(new ImageRequest
		{
			Name = "poster.webp",
			Width = 600,
			Height = 900,
			Image = createMovieDto.PosterUrl,
			Path = $"cinema/movies/{movie.Id}"
		});

		var backdropResult = await _image.UploadImageAsync(new ImageRequest
		{
			Name = "backdrop.webp",
			Width = 1920,
			Height = 1080,
			Image = createMovieDto.BackdropUrl,
			Path = $"cinema/movies/{movie.Id}",
			Quality = 95
		});

		movie.PosterUrl = posterResult.url;
		movie.PosterId = posterResult.fileId;
		movie.BackdropUrl = backdropResult.url;
		movie.BackdropId = backdropResult.fileId;

		await _commandService.AddAsync(movie);

		return StatusCode(StatusCodes.Status201Created);
	}

	/// <summary>
	///     Updates existing movie.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for movie.
	/// </param>
	/// <param name="updateMovieDto">
	///     DTO required for updating.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Movie successfully updated.
	/// </response>
	/// <response code="400">
	///     Image conversion failed.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Movie not found.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	/// <response code="502">
	///     Error with remote storage.
	/// </response>
	[HttpPut]
	[Route("{id}")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(typeof(MagickNotification), StatusCodes.Status400BadRequest)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> UpdateAsync(string id, [FromBody] UpdateMovieDto updateMovieDto)
	{
		var movie = await _queryService.GetAsync(movie => movie.Id == id, true);
		var oldPosterId = movie.PosterId;
		var oldBackdropId = movie.BackdropId;

		var posterResult = await _image.UploadImageAsync(new ImageRequest
		{
			Name = "poster.webp",
			Width = 600,
			Height = 900,
			Image = updateMovieDto.PosterUrl,
			Path = $"cinema/movies/{movie.Id}"
		});

		var backdropResult = await _image.UploadImageAsync(new ImageRequest
		{
			Name = "backdrop.webp",
			Width = 1920,
			Height = 1080,
			Image = updateMovieDto.BackdropUrl,
			Path = $"cinema/movies/{movie.Id}",
			Quality = 95
		});

		movie = _mapper.Map(updateMovieDto, movie);
		movie.PosterUrl = posterResult.url;
		movie.PosterId = posterResult.fileId;
		movie.BackdropUrl = backdropResult.url;
		movie.BackdropId = backdropResult.fileId;

		await _commandService.UpdateAsync(movie);
		await _image.TryDeleteImageAsync(oldPosterId);
		await _image.TryDeleteImageAsync(oldBackdropId);

		return StatusCode(StatusCodes.Status200OK);
	}

	/// <summary>
	///     Apply partial update to existing movie.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for movie.
	/// </param>
	/// <param name="patch">
	///     DTO required for partial updating.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Movie successfully updated.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Movie not found.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPatch]
	[Route("{id}")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[Consumes("application/json-patch+json")]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> PatchAsync(string id, [FromBody] JsonPatchDocument<PatchMovieDto> patch)
	{
		var movie = await _queryService.GetAsync(movie => movie.Id == id);
		var patchMovieDto = _mapper.Map<PatchMovieDto>(movie);

		patch.ApplyTo(patchMovieDto);
		movie = _mapper.Map(patchMovieDto, movie);

		await _commandService.UpdateAsync(movie);

		return StatusCode(StatusCodes.Status200OK);
	}

	/// <summary>
	///     Deletes a movie.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for movie.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Movie successfully deleted.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Movie not found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	/// <response code="502">
	///     Error with remote storage.
	/// </response>
	[HttpDelete]
	[Route("{id}")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	[ProducesResponseType(typeof(ImageStorageNotification), StatusCodes.Status502BadGateway)]
	public async Task<IActionResult> DeleteAsync(string id)
	{
		var movie = await _queryService.GetAsync(movie => movie.Id == id);

		await _image.DeleteDirectoryAsync($"cinema/movies/{movie.Id}");
		await _commandService.DeleteAsync(movie);

		return StatusCode(StatusCodes.Status200OK);
	}
}
