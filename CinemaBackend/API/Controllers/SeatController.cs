//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using API.DTO.Request.Seat;
using API.DTO.Response.Seat;

namespace API.Controllers;

[Route("api/seats")]
[Produces(MediaTypeNames.Application.Json)]
public sealed class SeatController : ControllerBase
{
	private readonly ISeatCommandService _commandService;
	private readonly ISeatQueryService _queryService;
	private readonly IMapper _mapper;

	public SeatController(ISeatQueryService queryService, ISeatCommandService commandService, IMapper mapper)
	{
		_queryService = queryService;
		_commandService = commandService;
		_mapper = mapper;
	}

	/// <summary>
	///     Gets all seats.
	/// </summary>
	/// <param name="seatFilter">
	///     Additional query parameters used for sorting, filtering and paging.
	/// </param>
	/// <returns>
	///     <see cref="Page{T}"/> containing page, item and record information.
	/// </returns>
	/// <response code="200">
	///     Seats that match the filter are found.
	/// </response>
	/// <response code="204">
	///     Seats that match the filter are not found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[AllowAnonymous]
	[ProducesResponseType(typeof(Page<SeatDto>), StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAllAsync([FromQuery] SeatFilter seatFilter)
	{
		var seatsCount = await _queryService.CountAsync(seatFilter);
		if (seatsCount is 0) return StatusCode(StatusCodes.Status204NoContent);

		var seats = await _queryService.GetAllAsync<SeatDto>(seatFilter);
		var pageable = new Page<SeatDto>(seatFilter, seatsCount, seats);

		return StatusCode(StatusCodes.Status200OK, pageable);
	}

	/// <summary>
	///     Gets a specific seat.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for seat.
	/// </param>
	/// <returns>
	///     Requested seat.
	/// </returns>
	/// <response code="200">
	///     Seat is found.
	/// </response>
	/// <response code="404">
	///     Seat doesn't exist.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[Route("{id:Guid}")]
	[AllowAnonymous]
	[ProducesResponseType(typeof(SeatDto), StatusCodes.Status200OK)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAsync(Guid id)
	{
		var seat = await _queryService.GetAsync<IndividualSeatDto>(seat => seat.Id == id);
		return StatusCode(StatusCodes.Status200OK, seat);
	}

	/// <summary>
	///     Creates a seat.
	/// </summary>
	/// <param name="createSeatDto">
	///     DTO required for creation.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="201">
	///     Seat successfully created.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="409">
	///     Violates unique constraint.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPost]
	[Authorize(Roles = nameof(Roles.Admin))]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status201Created)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(DuplicateEntityNotification), StatusCodes.Status409Conflict)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> AddAsync([FromBody] CreateSeatDto createSeatDto)
	{
		var seat = _mapper.Map<Seat>(createSeatDto);
		await _commandService.AddAsync(seat);

		return StatusCode(StatusCodes.Status201Created);
	}

	/// <summary>
	///     Updates existing seat.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for seat.
	/// </param>
	/// <param name="updateSeatDto">
	///     DTO required for updating.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Seat successfully updated.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Seat not found.
	/// </response>
	/// <response code="409">
	///     Violates unique constraint.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPut]
	[Route("{id:Guid}")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(DuplicateEntityNotification), StatusCodes.Status409Conflict)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> UpdateAsync(Guid id, [FromBody] UpdateSeatDto updateSeatDto)
	{
		var seat = await _queryService.GetAsync(seat => seat.Id == id, true);
		await _commandService.UpdateAsync(_mapper.Map(updateSeatDto, seat));

		return StatusCode(StatusCodes.Status200OK);
	}

	/// <summary>
	///     Deletes a seat.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for seat.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Seat successfully deleted.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Seat not found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpDelete]
	[Route("{id:Guid}")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> DeleteAsync(Guid id)
	{
		var seat = await _queryService.GetAsync(seat => seat.Id == id);
		await _commandService.DeleteAsync(seat);

		return StatusCode(StatusCodes.Status200OK);
	}
}
