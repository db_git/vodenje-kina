//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Security.Claims;
using API.DTO.Request.UserRating;
using API.DTO.Response.UserRating;
using Microsoft.AspNetCore.JsonPatch;
using Repository.Utils;

namespace API.Controllers;

[Route("api/ratings")]
[Produces(MediaTypeNames.Application.Json)]
public sealed class UserRatingController : ControllerBase
{
	private readonly IUserRatingCommandService _commandService;
	private readonly IUserRatingQueryService _queryService;
	private readonly IMovieQueryService _movieQueryService;
	private readonly IMapper _mapper;

	public UserRatingController(
		IUserRatingCommandService commandService,
		IUserRatingQueryService queryService,
		IMovieQueryService movieQueryService,
		IMapper mapper
	)
	{
		_commandService = commandService;
		_queryService = queryService;
		_movieQueryService = movieQueryService;
		_mapper = mapper;
	}

	/// <summary>
	///     Gets all ratings.
	/// </summary>
	/// <param name="ratingFilter">
	///     Additional query parameters used for sorting, filtering and paging.
	/// </param>
	/// <returns>
	///     <see cref="Page{T}"/> containing page, item and record information.
	/// </returns>
	/// <response code="200">
	///     Ratings that match the filter are found.
	/// </response>
	/// <response code="204">
	///     Ratings that match the filter are not found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[Authorize(Roles = nameof(Roles.Admin))]
	[ProducesResponseType(typeof(Page<UserRatingDto>), StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAllAsync([FromQuery] UserRatingFilter ratingFilter)
	{
		var ratingsCount = await _queryService.CountAsync(ratingFilter);
		if (ratingsCount is 0) return StatusCode(StatusCodes.Status204NoContent);

		var ratings = await _queryService.GetAllAsync<UserRatingDto>(ratingFilter);
		var pageable = new Page<UserRatingDto>(ratingFilter, ratingsCount, ratings);

		return StatusCode(StatusCodes.Status200OK, pageable);
	}

	/// <summary>
	///     Gets ratings for movie.
	/// </summary>
	/// <param name="movieId">
	///     The movie to which the rating belongs.
	/// </param>
	/// <returns>
	///     <see cref="MovieRatingDto"/> containing rating information.
	/// </returns>
	/// <response code="200">
	///     Information about ratings.
	/// </response>
	/// <response code="204">
	///     No ratings are found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[AllowAnonymous]
	[Route("{movieId}")]
	[ProducesResponseType(typeof(MovieRatingDto), StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAsync(string movieId)
	{
		var moviesCount = await _movieQueryService.CountAsync(m => m.Id == movieId);
		if (moviesCount is 0) return StatusCode(StatusCodes.Status204NoContent);

		Expression<Func<UserRating, bool>> movieFunc = r => r.MovieId == movieId;
		var ratingsCount = await _queryService.CountAsync(movieFunc);
		if (ratingsCount is 0) return StatusCode(StatusCodes.Status204NoContent);

		var ratingSum = await _queryService.GetRatingSumAsync(movieId);

		var ratingStars = new List<RatingStarDto>(5);
		foreach (var index in Enumerable.Range(1, 5))
		{
			ratingStars.Add(new()
			{
				Star = index,
				Count = await _queryService
					.CountAsync(movieFunc.And(r => r.Rating == index))
			});
		}

		SimpleUserRatingDto? userRating = null;
		var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
		if (userId is not null)
		{
			userRating = await _queryService.TryGetAsync<SimpleUserRatingDto>(r =>
				r.UserId == Guid.Parse(userId) && r.MovieId == movieId);
		}

		var response = new MovieRatingDto
		{
			MyRating = userRating?.Rating,
			RatingsCount = ratingsCount,
			TotalRating = ratingSum / ratingsCount,
			RatingStars = ratingStars.OrderByDescending(r => r.Star)
		};

		return StatusCode(StatusCodes.Status200OK, response);
	}

	/// <summary>
	///     Creates a rating.
	/// </summary>
	/// <param name="createUserRatingDto">
	///     DTO required for creation.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="201">
	///     Rating successfully created.
	/// </response>
	/// <response code="400">
	///     Already rated.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Movie doesn't exist.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPost]
	[Authorize]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status201Created)]
	[ProducesResponseType(StatusCodes.Status400BadRequest)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> AddAsync([FromBody] CreateUserRatingDto createUserRatingDto)
	{
		var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
		if (userId is null) return StatusCode(StatusCodes.Status401Unauthorized);

		var moviesCount = await _movieQueryService
			.CountAsync(movie => movie.Id == createUserRatingDto.Movie);
		if (moviesCount is 0) return StatusCode(StatusCodes.Status404NotFound);

		var ratingsCount = await _queryService.CountAsync(r =>
			r.MovieId == createUserRatingDto.Movie && r.UserId == Guid.Parse(userId));
		if (ratingsCount is not 0) return StatusCode(StatusCodes.Status400BadRequest);

		var rating = _mapper.Map<UserRating>(createUserRatingDto);
		rating.UserId = Guid.Parse(userId);
		await _commandService.AddAsync(rating);

		return StatusCode(StatusCodes.Status201Created);
	}

	/// <summary>
	///     Updates a rating.
	/// </summary>
	/// <param name="movieId">
	///     The movie to which the rating belongs.
	/// </param>
	/// <param name="patch">
	///     DTO required for updating.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Rating successfully updated.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="404">
	///     Rating doesn't exist.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPatch]
	[Route("{movieId}")]
	[Authorize]
	[Consumes("application/json-patch+json")]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> PatchAsync(
		[Required(AllowEmptyStrings = false)] string movieId,
		[FromBody] JsonPatchDocument<PatchUserRatingDto> patch
	)
	{
		var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
		if (userId is null) return StatusCode(StatusCodes.Status401Unauthorized);

		var dto = new PatchUserRatingDto();
		patch.ApplyTo(dto, ModelState);
		if (!TryValidateModel(dto))
		{
			return StatusCode(
				StatusCodes.Status422UnprocessableEntity,
				new ValidationResultModel(ModelState)
			);
		}

		var rating = await _queryService.GetAsync(r =>
			r.MovieId == movieId && r.UserId == Guid.Parse(userId), true);

		rating.Rating = dto.Rating;
		await _commandService.UpdateAsync(rating);

		return StatusCode(StatusCodes.Status200OK);
	}

	/// <summary>
	///     Deletes a rating.
	/// </summary>
	/// <param name="deleteUserRatingDto">
	///     DTO required for deleting.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Rating successfully deleted.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Rating doesn't exist.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpDelete]
	[Authorize]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> DeleteAsync([FromQuery] DeleteUserRatingDto deleteUserRatingDto)
	{
		var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
		if (userId is null) return StatusCode(StatusCodes.Status401Unauthorized);

		var rating = await _queryService.GetAsync(r =>
			r.UserId == Guid.Parse(deleteUserRatingDto.User) && r.MovieId == deleteUserRatingDto.Movie);

		if (User.IsInRole(nameof(Roles.Admin)))
			await _commandService.DeleteAsync(rating);
		else if (User.IsInRole(nameof(Roles.User)) && userId.Equals(deleteUserRatingDto.User))
			await _commandService.DeleteAsync(rating);
		else
			return StatusCode(StatusCodes.Status403Forbidden);

		return StatusCode(StatusCodes.Status200OK);
	}
}
