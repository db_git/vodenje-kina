//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using API.DTO.Request.UserComment;
using API.DTO.Response.UserComment;
using Microsoft.AspNetCore.JsonPatch;

namespace API.Controllers;

[Route("api/comments")]
[Produces(MediaTypeNames.Application.Json)]
public sealed class UserCommentController : ControllerBase
{
	private readonly IUserCommentCommandService _commandService;
	private readonly IUserCommentQueryService _queryService;
	private readonly IMovieQueryService _movieQueryService;
	private readonly IMapper _mapper;

	public UserCommentController(
		IUserCommentCommandService commandService,
		IUserCommentQueryService queryService,
		IMovieQueryService movieQueryService,
		IMapper mapper
	)
	{
		_commandService = commandService;
		_queryService = queryService;
		_movieQueryService = movieQueryService;
		_mapper = mapper;
	}

	/// <summary>
	///     Gets whether a user can comment on a movie.
	/// </summary>
	/// <param name="movie">
	///     Movie to check.
	/// </param>
	/// <response code="204">
	///     User can comment.
	/// </response>
	/// <response code="400">
	///     User can't comment.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="404">
	///     Movie doesn't exist.
	/// </response>
	/// <response code="422">
	///     Missing movie query parameter.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet("can-comment")]
	[Authorize]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(StatusCodes.Status400BadRequest)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> CanComment([FromQuery] [Required(AllowEmptyStrings = false)] string movie)
	{
		var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
		if (userId is null) return StatusCode(StatusCodes.Status401Unauthorized);

		var moviesCount = await _movieQueryService
			.CountAsync(m => m.Id == movie);
		if (moviesCount is 0) return StatusCode(StatusCodes.Status404NotFound);

		var commentsCount = await _queryService.CountAsync(c
			=> c.UserId == Guid.Parse(userId) && c.MovieId == movie);

		if (commentsCount is not 0) return StatusCode(StatusCodes.Status400BadRequest);
		return StatusCode(StatusCodes.Status204NoContent);
	}

	/// <summary>
	///     Gets all comments.
	/// </summary>
	/// <param name="commentFilter">
	///     Additional query parameters used for sorting, filtering and paging.
	/// </param>
	/// <returns>
	///     <see cref="Page{T}"/> containing page, item and record information.
	/// </returns>
	/// <response code="200">
	///     Comments that match the filter are found.
	/// </response>
	/// <response code="204">
	///     Comments that match the filter are not found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[AllowAnonymous]
	[ProducesResponseType(typeof(Page<UserCommentDto>), StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAllAsync([FromQuery] UserCommentFilter commentFilter)
	{
		var commentsCount = await _queryService.CountAsync(commentFilter);
		if (commentsCount is 0) return StatusCode(StatusCodes.Status204NoContent);

		var comments = await _queryService.GetAllAsync<UserCommentDto>(commentFilter);
		var pageable = new Page<UserCommentDto>(commentFilter, commentsCount, comments);

		return StatusCode(StatusCodes.Status200OK, pageable);
	}

	/// <summary>
	///     Creates a comment.
	/// </summary>
	/// <param name="createUserCommentDto">
	///     DTO required for creation.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="201">
	///     Comment successfully created.
	/// </response>
	/// <response code="400">
	///     Already commented.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Movie doesn't exist.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPost]
	[Authorize]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status201Created)]
	[ProducesResponseType(StatusCodes.Status400BadRequest)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> AddAsync([FromBody] CreateUserCommentDto createUserCommentDto)
	{
		var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
		if (userId is null) return StatusCode(StatusCodes.Status401Unauthorized);

		var moviesCount = await _movieQueryService
			.CountAsync(movie => movie.Id == createUserCommentDto.Movie);
		if (moviesCount is 0) return StatusCode(StatusCodes.Status404NotFound);

		var commentsCount = await _queryService.CountAsync(c =>
			c.MovieId == createUserCommentDto.Movie && c.UserId == Guid.Parse(userId));
		if (commentsCount is not 0) return StatusCode(StatusCodes.Status400BadRequest);

		var comment = _mapper.Map<UserComment>(createUserCommentDto);
		comment.UserId = Guid.Parse(userId);
		await _commandService.AddAsync(comment);

		return StatusCode(StatusCodes.Status201Created);
	}

	/// <summary>
	///     Updates a comment.
	/// </summary>
	/// <param name="movieId">
	///     The movie to which the comment belongs.
	/// </param>
	/// <param name="patch">
	///     DTO required for updating.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Comment successfully updated.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="404">
	///     Comment doesn't exist.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPatch]
	[Route("{movieId}")]
	[Authorize]
	[Consumes("application/json-patch+json")]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> PatchAsync(
		[Required(AllowEmptyStrings = false)] string movieId,
		[FromBody] JsonPatchDocument<PatchUserCommentDto> patch
	)
	{
		var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
		if (userId is null) return StatusCode(StatusCodes.Status401Unauthorized);

		var dto = new PatchUserCommentDto();
		patch.ApplyTo(dto, ModelState);
		if (!TryValidateModel(dto))
		{
			return StatusCode(
				StatusCodes.Status422UnprocessableEntity,
				new ValidationResultModel(ModelState)
			);
		}

		var comment = await _queryService.GetAsync(c =>
			c.MovieId == movieId && c.UserId == Guid.Parse(userId), true);

		comment.Comment = dto.Comment;
		await _commandService.UpdateAsync(comment);

		return StatusCode(StatusCodes.Status200OK);
	}

	/// <summary>
	///     Deletes a comment.
	/// </summary>
	/// <param name="deleteUserCommentDto">
	///     DTO required for deleting.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Comment successfully deleted.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Comment doesn't exist.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpDelete]
	[Authorize]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> DeleteAsync([FromQuery] DeleteUserCommentDto deleteUserCommentDto)
	{
		var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
		if (userId is null) return StatusCode(StatusCodes.Status401Unauthorized);

		var comment = await _queryService.GetAsync(c =>
			c.UserId == Guid.Parse(deleteUserCommentDto.User) && c.MovieId == deleteUserCommentDto.Movie);

		if (User.IsInRole(nameof(Roles.Admin)))
			await _commandService.DeleteAsync(comment);
		else if (User.IsInRole(nameof(Roles.User)) && userId.Equals(deleteUserCommentDto.User))
			await _commandService.DeleteAsync(comment);
		else
			return StatusCode(StatusCodes.Status403Forbidden);

		return StatusCode(StatusCodes.Status200OK);
	}
}
