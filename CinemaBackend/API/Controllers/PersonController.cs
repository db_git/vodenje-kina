//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using API.DTO.Request.Person;
using API.DTO.Response.Person;

namespace API.Controllers;

[Route("api/people")]
[Produces(MediaTypeNames.Application.Json)]
public sealed class PersonController : ControllerBase
{
	private readonly IPersonCommandService _commandService;
	private readonly IPersonQueryService _queryService;
	private readonly IMapper _mapper;
	private readonly IImageFacade _image;

	public PersonController(
		IPersonQueryService queryService,
		IPersonCommandService commandService,
		IMapper mapper,
		IImageFacade image
	)
	{
		_queryService = queryService;
		_commandService = commandService;
		_mapper = mapper;
		_image = image;
	}

	/// <summary>
	///     Gets all people.
	/// </summary>
	/// <param name="queryFilter">
	///     Additional query parameters used for sorting, filtering and paging.
	/// </param>
	/// <returns>
	///     <see cref="Page{T}"/> containing page, item and record information.
	/// </returns>
	/// <response code="200">
	///     People that match the filter are found.
	/// </response>
	/// <response code="204">
	///     People that match the filter are not found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[AllowAnonymous]
	[ProducesResponseType(typeof(Page<PersonDto>), StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAllAsync([FromQuery] PersonFilter queryFilter)
	{
		var peopleCount = await _queryService.CountAsync(queryFilter);
		if (peopleCount is 0) return StatusCode(StatusCodes.Status204NoContent);

		var people = await _queryService.GetAllAsync<PersonDto>(queryFilter);
		var pageable = new Page<PersonDto>(queryFilter, peopleCount, people);

		return StatusCode(StatusCodes.Status200OK, pageable);
	}

	/// <summary>
	///     Gets a specific person.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for person.
	/// </param>
	/// <returns>
	///     Requested person.
	/// </returns>
	/// <response code="200">
	///     Person is found.
	/// </response>
	/// <response code="404">
	///     Person doesn't exist.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[Route("{id}")]
	[AllowAnonymous]
	[ProducesResponseType(typeof(PersonDto), StatusCodes.Status200OK)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAsync(string id)
	{
		var person = await _queryService.GetAsync<PersonDto>(person => person.Id == id);
		return StatusCode(StatusCodes.Status200OK, person);
	}

	/// <summary>
	///     Creates a person.
	/// </summary>
	/// <param name="createPersonDto">
	///     DTO required for creation.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="201">
	///     Person successfully created.
	/// </response>
	/// <response code="400">
	///     Image conversion failed.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	/// <response code="502">
	///     Error with remote storage.
	/// </response>
	[HttpPost]
	[Authorize(Roles = nameof(Roles.Admin))]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status201Created)]
	[ProducesResponseType(typeof(MagickNotification), StatusCodes.Status400BadRequest)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	[ProducesResponseType(typeof(ImageStorageNotification), StatusCodes.Status502BadGateway)]
	public async Task<IActionResult> AddAsync([FromBody] CreatePersonDto createPersonDto)
	{
		var person = _mapper.Map<Person>(createPersonDto);
		person.Id = await _commandService.GenerateIdFromName(person.Name);

		var result = await _image.UploadImageAsync(new ImageRequest
		{
			Name = $"{person.Id}.webp",
			Width = 420,
			Height = 630,
			Image = createPersonDto.ImageUrl,
			Path = "cinema/people"
		});

		person.ImageUrl = result.url;
		person.ImageId = result.fileId;

		await _commandService.AddAsync(person);

		return StatusCode(StatusCodes.Status201Created);
	}

	/// <summary>
	///     Updates existing person.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for person.
	/// </param>
	/// <param name="updatePersonDto">
	///     DTO required for updating.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Person successfully updated.
	/// </response>
	/// <response code="400">
	///     Image conversion failed.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Person not found.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	/// <response code="502">
	///     Error with remote storage.
	/// </response>
	[HttpPut]
	[Route("{id}")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(typeof(MagickNotification), StatusCodes.Status400BadRequest)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	[ProducesResponseType(typeof(ImageStorageNotification), StatusCodes.Status502BadGateway)]
	public async Task<IActionResult> UpdateAsync(string id, [FromBody] UpdatePersonDto updatePersonDto)
	{
		var person = await _queryService.GetAsync(person => person.Id == id, true);
		var oldImageId = person.ImageId;

		var result = await _image.UploadImageAsync(new ImageRequest
		{
			Name = $"{person.Id}.webp",
			Width = 420,
			Height = 630,
			Image = updatePersonDto.ImageUrl,
			Path = "cinema/people"
		});

		person = _mapper.Map(updatePersonDto, person);
		person.ImageUrl = result.url;
		person.ImageId = result.fileId;

		await _commandService.UpdateAsync(person);
		await _image.TryDeleteImageAsync(oldImageId);

		return StatusCode(StatusCodes.Status200OK);
	}

	/// <summary>
	///     Deletes a person.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for person.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Person successfully deleted.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Person not found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	/// <response code="502">
	///     Error with remote storage.
	/// </response>
	[HttpDelete]
	[Route("{id}")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	[ProducesResponseType(typeof(ImageStorageNotification), StatusCodes.Status502BadGateway)]
	public async Task<IActionResult> DeleteAsync(string id)
	{
		var person = await _queryService.GetAsync(person => person.Id == id);
		await _image.DeleteImageAsync(person.ImageId);
		await _commandService.DeleteAsync(person);

		return StatusCode(StatusCodes.Status200OK);
	}
}
