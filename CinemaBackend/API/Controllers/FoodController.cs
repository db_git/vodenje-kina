//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using API.DTO.Request.Food;
using API.DTO.Response.Food;

namespace API.Controllers;

[Route("api/foods")]
[Produces(MediaTypeNames.Application.Json)]
public sealed class FoodController : ControllerBase
{
	private readonly IFoodCommandService _commandSservice;
	private readonly IFoodQueryService _queryService;
	private readonly IMapper _mapper;
	private readonly IImageFacade _image;

	public FoodController(
		IFoodQueryService queryService,
		IFoodCommandService commandService,
		IMapper mapper,
		IImageFacade image
	)
	{
		_queryService = queryService;
		_mapper = mapper;
		_commandSservice = commandService;
		_image = image;
	}

	/// <summary>
	///     Gets all foods.
	/// </summary>
	/// <param name="foodFilter">
	///     Additional query parameters used for sorting, filtering and paging.
	/// </param>
	/// <returns>
	///     <see cref="Page{T}"/> containing page, item and record information.
	/// </returns>
	/// <response code="200">
	///     Foods that match the filter are found.
	/// </response>
	/// <response code="204">
	///     Foods that match the filter are not found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[AllowAnonymous]
	[ProducesResponseType(typeof(Page<FoodDto>), StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAllAsync([FromQuery] FoodFilter foodFilter)
	{
		var foodsCount = await _queryService.CountAsync(foodFilter);
		if (foodsCount is 0) return StatusCode(StatusCodes.Status204NoContent);

		var foods = await _queryService.GetAllAsync<FoodDto>(foodFilter);
		var pageable = new Page<FoodDto>(foodFilter, foodsCount, foods);

		return StatusCode(StatusCodes.Status200OK, pageable);
	}

	/// <summary>
	///     Gets a specific food.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for food.
	/// </param>
	/// <returns>
	///     Requested food.
	/// </returns>
	/// <response code="200">
	///     Food is found.
	/// </response>
	/// <response code="404">
	///     Food doesn't exist.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[Route("{id:Guid}")]
	[AllowAnonymous]
	[ProducesResponseType(typeof(FoodDto), StatusCodes.Status200OK)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAsync(Guid id)
	{
		var food = await _queryService.GetAsync<FoodDto>(food => food.Id == id);
		return StatusCode(StatusCodes.Status200OK, food);
	}

	/// <summary>
	///     Creates a food.
	/// </summary>
	/// <param name="createFoodDto">
	///     DTO required for creation.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="201">
	///     Food successfully created.
	/// </response>
	/// <response code="400">
	///     Image conversion failed.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPost]
	[Authorize(Roles = nameof(Roles.Admin))]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status201Created)]
	[ProducesResponseType(typeof(MagickNotification), StatusCodes.Status400BadRequest)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> AddAsync([FromBody] CreateFoodDto createFoodDto)
	{
		var food = _mapper.Map<Food>(createFoodDto);
		food.Id = Guid.NewGuid();

		var result = await _image.UploadImageAsync(new ImageRequest
		{
			Name = $"{food.Id}.webp",
			Width = 500,
			Height = 500,
			Image = createFoodDto.ImageUrl,
			Path = "cinema/foods"
		});

		food.ImageUrl = result.url;
		food.ImageId = result.fileId;

		await _commandSservice.AddAsync(food);

		return StatusCode(StatusCodes.Status201Created);
	}

	/// <summary>
	///     Updates existing food.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for food.
	/// </param>
	/// <param name="updateFoodDto">
	///     DTO required for updating.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Food successfully updated.
	/// </response>
	/// <response code="400">
	///     Image conversion failed.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Food not found.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	/// <response code="502">
	///     Error with remote storage.
	/// </response>
	[HttpPut]
	[Route("{id:Guid}")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(typeof(MagickNotification), StatusCodes.Status400BadRequest)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	[ProducesResponseType(typeof(ImageStorageNotification), StatusCodes.Status502BadGateway)]
	public async Task<IActionResult> UpdateAsync(Guid id, [FromBody] UpdateFoodDto updateFoodDto)
	{
		var food = await _queryService.GetAsync(food => food.Id == id, true);
		var oldImageId = food.ImageId;

		var result = await _image.UploadImageAsync(new ImageRequest
		{
			Name = $"{food.Id}.webp",
			Width = 500,
			Height = 500,
			Image = updateFoodDto.ImageUrl,
			Path = "cinema/foods"
		});

		food = _mapper.Map(updateFoodDto, food);
		food.ImageUrl = result.url;
		food.ImageId = result.fileId;

		await _commandSservice.UpdateAsync(food);
		await _image.TryDeleteImageAsync(oldImageId);

		return StatusCode(StatusCodes.Status200OK);
	}

	/// <summary>
	///     Deletes a food.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for food.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Food successfully deleted.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Food not found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	/// <response code="502">
	///     Error with remote storage.
	/// </response>
	[HttpDelete]
	[Route("{id:Guid}")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	[ProducesResponseType(typeof(ImageStorageNotification), StatusCodes.Status502BadGateway)]
	public async Task<IActionResult> DeleteAsync(Guid id)
	{
		var food = await _queryService.GetAsync(food => food.Id == id);
		await _image.DeleteImageAsync(food.ImageId);
		await _commandSservice.DeleteAsync(food);

		return StatusCode(StatusCodes.Status200OK);
	}
}
