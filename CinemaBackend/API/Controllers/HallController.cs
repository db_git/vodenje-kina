//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using API.DTO.Request.Hall;
using API.DTO.Response.Hall;

namespace API.Controllers;

[Route("api/halls")]
[Produces(MediaTypeNames.Application.Json)]
public sealed class HallController : ControllerBase
{
	private readonly IHallCommandService _commandService;
	private readonly IHallQueryService _queryService;
	private readonly IMapper _mapper;

	public HallController(IHallQueryService queryService, IHallCommandService commandService, IMapper mapper)
	{
		_queryService = queryService;
		_commandService = commandService;
		_mapper = mapper;
	}

	/// <summary>
	///     Gets all halls.
	/// </summary>
	/// <param name="queryFilter">
	///     Additional query parameters used for sorting, filtering and paging.
	/// </param>
	/// <returns>
	///     <see cref="Page{T}"/> containing page, item and record information.
	/// </returns>
	/// <response code="200">
	///     Halls that match the filter are found.
	/// </response>
	/// <response code="204">
	///     Halls that match the filter are not found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[ProducesResponseType(typeof(Page<HallDto>), StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAllAsync([FromQuery] QueryFilter queryFilter)
	{
		var hallsCount = await _queryService.CountAsync(queryFilter);
		if (hallsCount is 0) return StatusCode(StatusCodes.Status204NoContent);

		var halls = await _queryService.GetAllAsync<HallDto>(queryFilter);
		var pageable = new Page<HallDto>(queryFilter, hallsCount, halls);

		return StatusCode(StatusCodes.Status200OK, pageable);
	}

	/// <summary>
	///     Gets a specific hall.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for hall.
	/// </param>
	/// <returns>
	///     Requested hall.
	/// </returns>
	/// <response code="200">
	///     Hall is found.
	/// </response>
	/// <response code="404">
	///     Hall doesn't exist.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[Route("{id:Guid}")]
	[ProducesResponseType(typeof(HallDto), StatusCodes.Status200OK)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAsync(Guid id)
	{
		var hall = await _queryService.GetAsync<HallDto>(hall => hall.Id == id);
		return StatusCode(StatusCodes.Status200OK, hall);
	}

	/// <summary>
	///     Creates a hall.
	/// </summary>
	/// <param name="createHallDto">
	///     DTO required for creation.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="201">
	///     Hall successfully created.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPost]
	[Authorize(Roles = nameof(Roles.Admin))]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status201Created)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> AddAsync([FromBody] CreateHallDto createHallDto)
	{
		var hall = _mapper.Map<Hall>(createHallDto);
		await _commandService.AddAsync(hall);

		return StatusCode(StatusCodes.Status201Created);
	}

	/// <summary>
	///     Updates existing hall.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for hall.
	/// </param>
	/// <param name="updateHallDto">
	///     DTO required for updating.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Hall successfully updated.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Hall not found.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPut]
	[Route("{id:Guid}")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> UpdateAsync(Guid id, [FromBody] UpdateHallDto updateHallDto)
	{
		var hall = await _queryService.GetAsync(hall => hall.Id == id, true);
		await _commandService.UpdateAsync(_mapper.Map(updateHallDto, hall));

		return StatusCode(StatusCodes.Status200OK);
	}

	/// <summary>
	///     Deletes a hall.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for hall.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Hall successfully deleted.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Hall not found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpDelete]
	[Route("{id:Guid}")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> DeleteAsync(Guid id)
	{
		var hall = await _queryService.GetAsync(hall => hall.Id == id);
		await _commandService.DeleteAsync(hall);

		return StatusCode(StatusCodes.Status200OK);
	}
}
