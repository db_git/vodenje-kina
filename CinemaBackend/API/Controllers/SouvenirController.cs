//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using API.DTO.Request.Souvenir;
using API.DTO.Response.Souvenir;

namespace API.Controllers;

[Route("api/souvenirs")]
[Produces(MediaTypeNames.Application.Json)]
public sealed class SouvenirController : ControllerBase
{
	private readonly ISouvenirCommandService _commandService;
	private readonly ISouvenirQueryService _queryService;
	private readonly IMapper _mapper;
	private readonly IImageFacade _image;

	public SouvenirController(
		ISouvenirQueryService queryService,
		ISouvenirCommandService commandService,
		IMapper mapper,
		IImageFacade image
	)
	{
		_queryService = queryService;
		_commandService = commandService;
		_mapper = mapper;
		_image = image;
	}

	/// <summary>
	///     Gets all souvenirs.
	/// </summary>
	/// <param name="souvenirFilter">
	///     Additional query parameters used for sorting, filtering and paging.
	/// </param>
	/// <returns>
	///     <see cref="Page{T}"/> containing page, item and record information.
	/// </returns>
	/// <response code="200">
	///     Souvenirs that match the filter are found.
	/// </response>
	/// <response code="204">
	///     Souvenirs that match the filter are not found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[AllowAnonymous]
	[ProducesResponseType(typeof(Page<SouvenirDto>), StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAllAsync([FromQuery] SouvenirFilter souvenirFilter)
	{
		var souvenirsCount = await _queryService.CountAsync(souvenirFilter);
		if (souvenirsCount is 0) return StatusCode(StatusCodes.Status204NoContent);

		var souvenirs = await _queryService.GetAllAsync<SouvenirDto>(souvenirFilter);
		var pageable = new Page<SouvenirDto>(souvenirFilter, souvenirsCount, souvenirs);

		return StatusCode(StatusCodes.Status200OK, pageable);
	}

	/// <summary>
	///     Gets a specific souvenir.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for souvenir.
	/// </param>
	/// <returns>
	///     Requested souvenir.
	/// </returns>
	/// <response code="200">
	///     Souvenir is found.
	/// </response>
	/// <response code="404">
	///     Souvenir doesn't exist.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[Route("{id:Guid}")]
	[AllowAnonymous]
	[ProducesResponseType(typeof(SouvenirDto), StatusCodes.Status200OK)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAsync(Guid id)
	{
		var souvenir = await _queryService.GetAsync<SouvenirDto>(souvenir => souvenir.Id == id);
		return StatusCode(StatusCodes.Status200OK, souvenir);
	}

	/// <summary>
	///     Creates a souvenir.
	/// </summary>
	/// <param name="createSouvenirDto">
	///     DTO required for creation.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="201">
	///     Souvenir successfully created.
	/// </response>
	/// <response code="400">
	///     Image conversion failed.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="409">
	///     Violates unique constraint.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	/// <response code="502">
	///     Error with remote storage.
	/// </response>
	[HttpPost]
	[Authorize(Roles = nameof(Roles.Admin))]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status201Created)]
	[ProducesResponseType(typeof(MagickNotification), StatusCodes.Status400BadRequest)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(DuplicateEntityNotification), StatusCodes.Status409Conflict)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	[ProducesResponseType(typeof(ImageStorageNotification), StatusCodes.Status502BadGateway)]
	public async Task<IActionResult> AddAsync([FromBody] CreateSouvenirDto createSouvenirDto)
	{
		var souvenir = _mapper.Map<Souvenir>(createSouvenirDto);
		souvenir.Id = Guid.NewGuid();

		var result = await _image.UploadImageAsync(new ImageRequest
		{
			Name = $"{souvenir.Id}.webp",
			Width = 550,
			Height = 700,
			Image = createSouvenirDto.ImageUrl,
			Path = "cinema/souvenirs"
		});

		souvenir.ImageUrl = result.url;
		souvenir.ImageId = result.fileId;

		await _commandService.AddAsync(souvenir);

		return StatusCode(StatusCodes.Status201Created);
	}

	/// <summary>
	///     Updates existing souvenir.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for souvenir.
	/// </param>
	/// <param name="updateSouvenirDto">
	///     DTO required for updating.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Souvenir successfully updated.
	/// </response>
	/// <response code="400">
	///     Image conversion failed.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Souvenir not found.
	/// </response>
	/// <response code="409">
	///     Violates unique constraint.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	/// <response code="502">
	///     Error with remote storage.
	/// </response>
	[HttpPut]
	[Route("{id:Guid}")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(typeof(MagickNotification), StatusCodes.Status400BadRequest)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(DuplicateEntityNotification), StatusCodes.Status409Conflict)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	[ProducesResponseType(typeof(ImageStorageNotification), StatusCodes.Status502BadGateway)]
	public async Task<IActionResult> UpdateAsync(Guid id, [FromBody] UpdateSouvenirDto updateSouvenirDto)
	{
		var souvenir = await _queryService.GetAsync(souvenir => souvenir.Id == id, true);
		var oldImageId = souvenir.ImageId;

		var result = await _image.UploadImageAsync(new ImageRequest
		{
			Name = $"{souvenir.Id}.webp",
			Width = 550,
			Height = 700,
			Image = updateSouvenirDto.ImageUrl,
			Path = "cinema/souvenirs"
		});

		souvenir = _mapper.Map(updateSouvenirDto, souvenir);
		souvenir.ImageUrl = result.url;
		souvenir.ImageId = result.fileId;

		try { await _commandService.UpdateAsync(souvenir); }
		catch { await _image.TryDeleteImageAsync(result.fileId); throw; }

		await _image.TryDeleteImageAsync(oldImageId);

		return StatusCode(StatusCodes.Status200OK);
	}

	/// <summary>
	///     Deletes a souvenir.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for souvenir.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Souvenir successfully deleted.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Souvenir not found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	/// <response code="502">
	///     Error with remote storage.
	/// </response>
	[HttpDelete]
	[Route("{id:Guid}")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	[ProducesResponseType(typeof(ImageStorageNotification), StatusCodes.Status502BadGateway)]
	public async Task<IActionResult> DeleteAsync(Guid id)
	{
		var souvenir = await _queryService.GetAsync(souvenir => souvenir.Id == id);
		await _image.DeleteImageAsync(souvenir.ImageId);
		await _commandService.DeleteAsync(souvenir);

		return StatusCode(StatusCodes.Status200OK);
	}
}
