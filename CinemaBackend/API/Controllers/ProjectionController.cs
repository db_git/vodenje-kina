//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using API.DTO.Request.Projection;
using API.DTO.Response.Projection;
using API.DTO.Response.Seat;

namespace API.Controllers;

[Route("api/projections")]
[Produces(MediaTypeNames.Application.Json)]
public sealed class ProjectionController : ControllerBase
{
	private readonly IProjectionCommandService _commandService;
	private readonly IProjectionQueryService _queryService;
	private readonly ISeatQueryService _seatQueryService;
	private readonly IMapper _mapper;

	public ProjectionController(
		IProjectionQueryService queryService,
		IProjectionCommandService commandService,
		ISeatQueryService seatQueryService,
		IMapper mapper
	)
	{
		_queryService = queryService;
		_commandService = commandService;
		_seatQueryService = seatQueryService;
		_mapper = mapper;
	}

	/// <summary>
	///     Gets all projections.
	/// </summary>
	/// <param name="projectionFilter">
	///     Additional query parameters used for sorting, filtering and paging.
	/// </param>
	/// <returns>
	///     <see cref="Page{T}"/> containing page, item and record information.
	/// </returns>
	/// <response code="200">
	///     Projections that match the filter are found.
	/// </response>
	/// <response code="204">
	///     Projections that match the filter are not found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[AllowAnonymous]
	[ProducesResponseType(typeof(Page<ProjectionDto>), StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAllAsync([FromQuery] ProjectionFilter projectionFilter)
	{
		var projectionsCount = await _queryService.CountAsync(projectionFilter);
		if (projectionsCount is 0) return StatusCode(StatusCodes.Status204NoContent);

		var projections = await _queryService.GetAllAsync<ProjectionDto>(projectionFilter);
		var pageable = new Page<ProjectionDto>(projectionFilter, projectionsCount, projections);

		return StatusCode(StatusCodes.Status200OK, pageable);
	}

	/// <summary>
	///     Gets a specific projection.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for projection.
	/// </param>
	/// <returns>
	///     Requested projection.
	/// </returns>
	/// <response code="200">
	///     Projection is found.
	/// </response>
	/// <response code="404">
	///     Projection doesn't exist.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[Route("{id:Guid}")]
	[AllowAnonymous]
	[ProducesResponseType(typeof(ProjectionDto), StatusCodes.Status200OK)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAsync(Guid id)
	{
		var projection = await _queryService.GetAsync<ProjectionDto>(projection => projection.Id == id);
		return StatusCode(StatusCodes.Status200OK, projection);
	}

	/// <summary>
	///     Gets seats which are taken.
	/// </summary>
	/// <param name="id">
	///     Projection to search.
	/// </param>
	/// <returns>
	///     Collection of taken seats.
	/// </returns>
	/// <response code="200">
	///     Taken seats.
	/// </response>
	/// <response code="204">
	///     No seats are taken.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[Route("{id:Guid}/taken-seats")]
	[AllowAnonymous]
	[ProducesResponseType(typeof(IEnumerable<SeatDto>), StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAvailableSeatsAsync(Guid id)
	{
		var seats = await _seatQueryService
			.GetTakenSeatsAsync<SeatDto>(id);

		if (!seats.Any()) return StatusCode(StatusCodes.Status204NoContent);
		return StatusCode(StatusCodes.Status200OK, seats);
	}

	/// <summary>
	///     Creates a projection.
	/// </summary>
	/// <param name="createProjectionDto">
	///     DTO required for creation.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="201">
	///     Projection successfully created.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="409">
	///     Projection within the time of another projection.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPost]
	[Authorize(Roles = nameof(Roles.Admin))]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status201Created)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(ProjectionOverlapNotification), StatusCodes.Status409Conflict)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> AddAsync([FromBody] CreateProjectionDto createProjectionDto)
	{
		var projection = _mapper.Map<Projection>(createProjectionDto);
		await _commandService.AddAsync(projection);

		return StatusCode(StatusCodes.Status201Created);
	}

	/// <summary>
	///     Updates existing projection.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for projection.
	/// </param>
	/// <param name="updateProjectionDto">
	///     DTO required for updating.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Projection successfully updated.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Projection not found.
	/// </response>
	/// <response code="409">
	///     Projection within the time of another projection.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPut]
	[Route("{id:Guid}")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(ProjectionOverlapNotification), StatusCodes.Status409Conflict)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> UpdateAsync(Guid id, [FromBody] UpdateProjectionDto updateProjectionDto)
	{
		var projection = await _queryService.GetAsync(projection => projection.Id == id, true);
		await _commandService.UpdateAsync(_mapper.Map(updateProjectionDto, projection));

		return StatusCode(StatusCodes.Status200OK);
	}

	/// <summary>
	///     Deletes a projection.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for projection.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Projection successfully deleted.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Projection not found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpDelete]
	[Route("{id:Guid}")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> DeleteAsync(Guid id)
	{
		var projection = await _queryService.GetAsync(projection => projection.Id == id);
		await _commandService.DeleteAsync(projection);

		return StatusCode(StatusCodes.Status200OK);
	}
}
