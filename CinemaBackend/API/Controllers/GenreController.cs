//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using API.DTO.Request.Genre;
using API.DTO.Response.Genre;

namespace API.Controllers;

[Route("api/genres")]
[Produces(MediaTypeNames.Application.Json)]
public sealed class GenreController : ControllerBase
{
	private readonly IGenreCommandService _commandService;
	private readonly IGenreQueryService _queryService;
	private readonly IMapper _mapper;

	public GenreController(IGenreQueryService queryService, IGenreCommandService commandService, IMapper mapper)
	{
		_queryService = queryService;
		_commandService = commandService;
		_mapper = mapper;
	}

	/// <summary>
	///     Gets all genres.
	/// </summary>
	/// <param name="queryFilter">
	///     Additional query parameters used for sorting, filtering and paging.
	/// </param>
	/// <returns>
	///     <see cref="Page{T}"/> containing page, item and record information.
	/// </returns>
	/// <response code="200">
	///     Genres that match the filter are found.
	/// </response>
	/// <response code="204">
	///     Genres that match the filter are not found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[AllowAnonymous]
	[ProducesResponseType(typeof(Page<GenreDto>), StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAllAsync([FromQuery] QueryFilter queryFilter)
	{
		var genresCount = await _queryService.CountAsync(queryFilter);
		if (genresCount is 0) return StatusCode(StatusCodes.Status204NoContent);

		var genres = await _queryService.GetAllAsync<GenreDto>(queryFilter);
		var pageable = new Page<GenreDto>(queryFilter, genresCount, genres);

		return StatusCode(StatusCodes.Status200OK, pageable);
	}

	/// <summary>
	///     Gets a specific genre.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for genre.
	/// </param>
	/// <returns>
	///     Requested genre.
	/// </returns>
	/// <response code="200">
	///     Genre is found.
	/// </response>
	/// <response code="404">
	///     Genre doesn't exist.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[Route("{id:Guid}")]
	[AllowAnonymous]
	[ProducesResponseType(typeof(GenreDto), StatusCodes.Status200OK)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAsync(Guid id)
	{
		var genre = await _queryService.GetAsync<GenreDto>(genre => genre.Id == id);
		return StatusCode(StatusCodes.Status200OK, genre);
	}

	/// <summary>
	///     Creates a genre.
	/// </summary>
	/// <param name="createGenreDto">
	///     DTO required for creation.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="201">
	///     Genre successfully created.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="409">
	///     Violates unique constraint.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPost]
	[Authorize(Roles = nameof(Roles.Admin))]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status201Created)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(DuplicateEntityNotification), StatusCodes.Status409Conflict)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> AddAsync([FromBody] CreateGenreDto createGenreDto)
	{
		var genre = _mapper.Map<Genre>(createGenreDto);
		await _commandService.AddAsync(genre);

		return StatusCode(StatusCodes.Status201Created);
	}

	/// <summary>
	///     Updates existing genre.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for genre.
	/// </param>
	/// <param name="updateGenreDto">
	///     DTO required for updating.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Genre successfully updated.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Genre not found.
	/// </response>
	/// <response code="409">
	///     Violates unique constraint.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPut]
	[Route("{id:Guid}")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(DuplicateEntityNotification), StatusCodes.Status409Conflict)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> UpdateAsync(Guid id, [FromBody] UpdateGenreDto updateGenreDto)
	{
		var genre = await _queryService.GetAsync(genre => genre.Id == id, true);
		await _commandService.UpdateAsync(_mapper.Map(updateGenreDto, genre));

		return StatusCode(StatusCodes.Status200OK);
	}

	/// <summary>
	///     Deletes a genre.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for genre.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Genre successfully deleted.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Genre not found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpDelete]
	[Route("{id:Guid}")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> DeleteAsync(Guid id)
	{
		var genre = await _queryService.GetAsync(genre => genre.Id == id);
		await _commandService.DeleteAsync(genre);

		return StatusCode(StatusCodes.Status200OK);
	}
}
