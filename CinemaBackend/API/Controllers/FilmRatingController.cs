//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using API.DTO.Request.FilmRating;
using API.DTO.Response.FilmRating;

namespace API.Controllers;

[Route("api/film-ratings")]
[Produces(MediaTypeNames.Application.Json)]
public sealed class FilmRatingController : ControllerBase
{
	private readonly IFilmRatingCommandService _commandService;
	private readonly IFilmRatingQueryService _queryService;
	private readonly IMapper _mapper;

	public FilmRatingController(
		IFilmRatingQueryService queryService,
		IFilmRatingCommandService commandService,
		IMapper mapper
	)
	{
		_queryService = queryService;
		_commandService = commandService;
		_mapper = mapper;
	}

	/// <summary>
	///     Gets all film ratings.
	/// </summary>
	/// <param name="queryFilter">
	///     Additional query parameters used for sorting, filtering and paging.
	/// </param>
	/// <returns>
	///     <see cref="Page{T}"/> containing page, item and record information.
	/// </returns>
	/// <response code="200">
	///     Film ratings that match the filter are found.
	/// </response>
	/// <response code="204">
	///     Film ratings that match the filter are not found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[AllowAnonymous]
	[ProducesResponseType(typeof(Page<FilmRatingDto>), StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAllAsync([FromQuery] QueryFilter queryFilter)
	{
		var filmRatingsCount = await _queryService.CountAsync(queryFilter);
		if (filmRatingsCount is 0) return StatusCode(StatusCodes.Status204NoContent);

		var filmRatings = await _queryService.GetAllAsync<FilmRatingDto>(queryFilter);
		var pageable = new Page<FilmRatingDto>(queryFilter, filmRatingsCount, filmRatings);

		return StatusCode(StatusCodes.Status200OK, pageable);
	}

	/// <summary>
	///     Gets a specific film rating.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for film rating.
	/// </param>
	/// <returns>
	///     Requested film rating.
	/// </returns>
	/// <response code="200">
	///     Film rating is found.
	/// </response>
	/// <response code="404">
	///     Film rating doesn't exist.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[Route("{id:Guid}")]
	[AllowAnonymous]
	[ProducesResponseType(typeof(FilmRatingDto), StatusCodes.Status200OK)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAsync(Guid id)
	{
		var filmRating = await _queryService.GetAsync<FilmRatingDto>(filmRating => filmRating.Id == id);
		return StatusCode(StatusCodes.Status200OK, filmRating);
	}

	/// <summary>
	///     Creates a film rating.
	/// </summary>
	/// <param name="createFilmRatingDto">
	///     DTO required for creation.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="201">
	///     Film rating successfully created.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="409">
	///     Violates unique constraint.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPost]
	[Authorize(Roles = nameof(Roles.Admin))]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status201Created)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(DuplicateEntityNotification), StatusCodes.Status409Conflict)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> AddAsync([FromBody] CreateFilmRatingDto createFilmRatingDto)
	{
		var filmRating = _mapper.Map<FilmRating>(createFilmRatingDto);
		await _commandService.AddAsync(filmRating);

		return StatusCode(StatusCodes.Status201Created);
	}

	/// <summary>
	///     Updates existing film rating.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for film rating.
	/// </param>
	/// <param name="updateFilmRatingDto">
	///     DTO required for updating.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Film rating successfully updated.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Film rating not found.
	/// </response>
	/// <response code="409">
	///     Violates unique constraint.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPut]
	[Route("{id:Guid}")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(DuplicateEntityNotification), StatusCodes.Status409Conflict)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> UpdateAsync(Guid id, [FromBody] UpdateFilmRatingDto updateFilmRatingDto)
	{
		var filmRating = await _queryService.GetAsync(filmRating => filmRating.Id == id, true);
		await _commandService.UpdateAsync(_mapper.Map(updateFilmRatingDto, filmRating));

		return StatusCode(StatusCodes.Status200OK);
	}

	/// <summary>
	///     Deletes a film rating.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for film rating.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Film rating successfully deleted.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Film rating not found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpDelete]
	[Route("{id:Guid}")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> DeleteAsync(Guid id)
	{
		var filmRating = await _queryService.GetAsync(filmRating => filmRating.Id == id);
		await _commandService.DeleteAsync(filmRating);

		return StatusCode(StatusCodes.Status200OK);
	}
}
