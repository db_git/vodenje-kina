//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using API.DTO.Request.Cinema;
using API.DTO.Response.Cinema;

namespace API.Controllers;

[Route("api/cinemas")]
[Produces(MediaTypeNames.Application.Json)]
public sealed class CinemaController : ControllerBase
{
	private readonly ICinemaCommandService _commandService;
	private readonly ICinemaQueryService _queryService;
	private readonly IMapper _mapper;

	public CinemaController(ICinemaQueryService queryService, ICinemaCommandService commandService, IMapper mapper)
	{
		_queryService = queryService;
		_commandService = commandService;
		_mapper = mapper;
	}

	/// <summary>
	///     Gets all cinemas.
	/// </summary>
	/// <param name="queryFilter">
	///     Additional query parameters used for sorting, filtering and paging.
	/// </param>
	/// <returns>
	///     <see cref="Page{T}"/> containing page, item and record information.
	/// </returns>
	/// <response code="200">
	///     Cinemas that match the filter are found.
	/// </response>
	/// <response code="204">
	///     Cinemas that match the filter are not found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[AllowAnonymous]
	[ProducesResponseType(typeof(Page<CinemaDto>), StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAllAsync([FromQuery] QueryFilter queryFilter)
	{
		var cinemasCount = await _queryService.CountAsync(queryFilter);
		if (cinemasCount is 0) return StatusCode(StatusCodes.Status204NoContent);

		var cinemas = await _queryService.GetAllAsync<CinemaDto>(queryFilter);
		var pageable = new Page<CinemaDto>(queryFilter, cinemasCount, cinemas);

		return StatusCode(StatusCodes.Status200OK, pageable);
	}

	/// <summary>
	///     Gets a specific cinema.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for cinema.
	/// </param>
	/// <returns>
	///     Requested cinema.
	/// </returns>
	/// <response code="200">
	///     Cinema is found.
	/// </response>
	/// <response code="404">
	///     Cinema doesn't exist.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpGet]
	[Route("{id:Guid}")]
	[AllowAnonymous]
	[ProducesResponseType(typeof(CinemaDto), StatusCodes.Status200OK)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> GetAsync(Guid id)
	{
		var cinema = await _queryService.GetAsync<CinemaDto>(cinema => cinema.Id == id);
		return StatusCode(StatusCodes.Status200OK, cinema);
	}

	/// <summary>
	///     Creates a cinema.
	/// </summary>
	/// <param name="createCinemaDto">
	///     DTO required for creation.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="201">
	///     Cinema successfully created.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPost]
	[Authorize(Roles = nameof(Roles.Admin))]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status201Created)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> AddAsync([FromBody] CreateCinemaDto createCinemaDto)
	{
		var cinema = _mapper.Map<Cinema>(createCinemaDto);
		await _commandService.AddAsync(cinema);

		return StatusCode(StatusCodes.Status201Created);
	}

	/// <summary>
	///     Updates existing cinema.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for cinema.
	/// </param>
	/// <param name="updateCinemaDto">
	///     DTO required for updating.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Cinema successfully updated.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Cinema not found.
	/// </response>
	/// <response code="422">
	///     Validation failed.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpPut]
	[Route("{id:Guid}")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[Consumes(MediaTypeNames.Application.Json)]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(ValidationResultModel), StatusCodes.Status422UnprocessableEntity)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> UpdateAsync(Guid id, [FromBody] UpdateCinemaDto updateCinemaDto)
	{
		var cinema = await _queryService.GetAsync(cinema => cinema.Id == id, true);
		await _commandService.UpdateAsync(_mapper.Map(updateCinemaDto, cinema));

		return StatusCode(StatusCodes.Status200OK);
	}

	/// <summary>
	///     Deletes a cinema.
	/// </summary>
	/// <param name="id">
	///     Unique identifier that will be used to search for cinema.
	/// </param>
	/// <returns>
	///     Empty body on success.
	/// </returns>
	/// <response code="200">
	///     Cinema successfully deleted.
	/// </response>
	/// <response code="401">
	///     Missing or invalid JWT token.
	/// </response>
	/// <response code="403">
	///     User doesn't have permission to perform action.
	/// </response>
	/// <response code="404">
	///     Cinema not found.
	/// </response>
	/// <response code="500">
	///     An unknown error has occurred.
	/// </response>
	[HttpDelete]
	[Route("{id:Guid}")]
	[Authorize(Roles = nameof(Roles.Admin))]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status403Forbidden)]
	[ProducesResponseType(typeof(EntityNotFoundNotification), StatusCodes.Status404NotFound)]
	[ProducesResponseType(typeof(UnknownErrorNotification), StatusCodes.Status500InternalServerError)]
	public async Task<IActionResult> DeleteAsync(Guid id)
	{
		var cinema = await _queryService.GetAsync(cinema => cinema.Id == id);
		await _commandService.DeleteAsync(cinema);

		return StatusCode(StatusCodes.Status200OK);
	}
}
