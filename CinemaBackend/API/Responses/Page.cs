//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace API.Responses;

#if DEBUG
[UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
#endif
internal sealed record Page<T> where T : class
{
	/// <summary>
	///     Requested page.
	/// </summary>
	public int PageNumber { get; }

	/// <summary>
	///     Requested items per page.
	/// </summary>
	public int PageSize { get; }

	/// <summary>
	///     Amount of items available on requested page.
	///     Less than or equal to <see cref="PageSize"/>.
	/// </summary>
	public int Elements { get; }

	/// <summary>
	///     Total amount of available items. Additional filters taken into account if present.
	/// </summary>
	public int TotalElements { get; }

	/// <summary>
	///     Total amount of available pages.
	/// </summary>
	public int TotalPages { get; }

	public bool HasPreviousPage { get; }

	public bool HasNextPage { get; }

	/// <summary>
	///     Response data.
	/// </summary>
	public ICollection<T> Data { get; } = null!;

	public Page(PageableFilter filter, int totalElements, ICollection<T> data)
	{
		Data = data;
		Elements = data.Count;
		TotalElements = totalElements;

		PageSize = filter.Size;
		TotalPages = (int) Math.Ceiling((decimal) TotalElements / PageSize);

		PageNumber = filter.Page;
		if (PageNumber > TotalPages) PageNumber = TotalPages;

		HasPreviousPage = PageNumber > 1;
		HasNextPage = PageNumber < TotalPages;
	}
}
