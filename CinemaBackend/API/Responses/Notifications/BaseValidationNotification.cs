//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace API.Responses.Notifications;

#if DEBUG
[UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
#endif
internal abstract record BaseValidationNotification(
	string Title,
	IDictionary<string, IEnumerable<string>> Errors,
	string? Message = null
) : BaseNotification(Title, NotificationSeverity.Warning, Message)
{
	/// <summary>
	///     Dictionary containing key-value pairs of failed validation, where key
	///     is property name and value is a collection of validation messages.
	/// </summary>
	public IDictionary<string, IEnumerable<string>> Errors { get; set; } = Errors;
}
