//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Service.Contracts.Exceptions;

namespace API.Responses.Notifications;

internal sealed record RegistrationNotification : BaseValidationNotification
{
	public RegistrationNotification(HttpContext context, Exception exception)
		: base(
			Title: "Bad request",
			Message: "One or more validation errors occurred.",
			Errors: ((IRegistrationException) exception).Errors
		)
	{
		context.Response.StatusCode = StatusCodes.Status422UnprocessableEntity;
	}
}
