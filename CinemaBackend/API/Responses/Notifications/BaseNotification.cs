//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Text.Json.Serialization;

namespace API.Responses.Notifications;

#if DEBUG
[UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
#endif
internal abstract record BaseNotification(
	string Title,
	NotificationSeverity Severity,
	string? Message = null
)
{
	/// <summary>
	///     Title of the notification.
	/// </summary>
	[JsonPropertyOrder(-99)]
	public string Title { get; } = Title;

	/// <summary>
	///     Severity indicating importance of notification.
	/// </summary>
	[JsonPropertyOrder(-100)]
	public NotificationSeverity Severity { get; } = Severity;

	/// <summary>
	///     Optional message describing notification in more detail.
	/// </summary>
	[JsonPropertyOrder(-98)]
	[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
	public string? Message { get; set; } = Message;
}
