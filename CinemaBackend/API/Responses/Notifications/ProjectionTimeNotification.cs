//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace API.Responses.Notifications;

internal sealed record ProjectionTimeNotification : BaseNotification
{
	public ProjectionTimeNotification(HttpContext context)
		: base(
			Title: "Not acceptable",
			Message: "Reservations can be made 30 minutes before projection time.",
			Severity: NotificationSeverity.Warning
		)
	{
		context.Response.StatusCode = StatusCodes.Status406NotAcceptable;
	}
}
