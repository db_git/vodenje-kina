//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace API.Responses.Notifications;

internal sealed record ProjectionOverlapNotification : BaseNotification
{
	public ProjectionOverlapNotification(HttpContext context)
		: base(
			"Time overlap",
			NotificationSeverity.Warning,
			"Cannot add projection at the time of another projection."
		)
	{
		context.Response.StatusCode = StatusCodes.Status409Conflict;
	}
}
