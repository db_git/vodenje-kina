//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Text.Json;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace API.Responses.Validation;

internal sealed record ValidationResultModel : BaseValidationNotification
{
	public ValidationResultModel(ModelStateDictionary modelState)
		: base("Bad request", null!, "One or more validation errors occurred.")
	{
		if (modelState.Any(kvp => kvp.Key.StartsWith("$") || string.IsNullOrEmpty(kvp.Key)))
			throw new JsonException();

		Errors = modelState
			.Where(kvp => kvp.Value is not null && kvp.Value.Errors.Any())
			.OrderBy(kvp => kvp.Key)
			.ToDictionary(
				kvp => kvp.Key,
				kvp => kvp.Value!.Errors.Select(e => e.ErrorMessage)
			);
	}
}
