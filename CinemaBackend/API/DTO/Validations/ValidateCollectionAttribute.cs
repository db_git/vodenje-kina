//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

#if DEBUG
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("API.Tests")]
#endif
namespace API.DTO.Validations;

/// <summary>
///     Validate each value in the collection with specified type.
/// </summary>
/// <typeparam name="T">
///     Type of <see cref="ValidationAttribute"/>
/// </typeparam>
[AttributeUsage(AttributeTargets.Property)]
internal sealed class ValidateCollectionAttribute<T> : ValidationAttribute where T : ValidationAttribute, new()
{
	private Type Attribute { get; }

	public ValidateCollectionAttribute(string? errorMessage = null)
	{
		Attribute = typeof(T);
		if (errorMessage is not null) ErrorMessage = errorMessage;
	}

	protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
	{
		if (value is string || value is not IEnumerable collection)
			return new ValidationResult("Not a collection.");

		var validator = (ValidationAttribute) Attribute
			.GetConstructor(Array.Empty<Type>())!
			.Invoke(BindingFlags.Public, null, Array.Empty<object>(), null);

		var errorMessage = ErrorMessage ?? validator.ErrorMessage ?? "Validation failed.";
		var memberNames = new List<string>();

		var index = 0;
		foreach (var obj in collection)
		{
			if (!validator.IsValid(obj)) memberNames.Add($"[{index}]");
			++index;
		}

		return memberNames.Any()
			? new ValidationResult(errorMessage, memberNames)
			: ValidationResult.Success;
	}
}
