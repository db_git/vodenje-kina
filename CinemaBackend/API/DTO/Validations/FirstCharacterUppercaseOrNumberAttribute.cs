//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.ComponentModel.DataAnnotations;

#if DEBUG
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("API.Tests")]
#endif
namespace API.DTO.Validations;

[AttributeUsage(AttributeTargets.Property)]
internal sealed class FirstCharacterUppercaseOrNumberAttribute : ValidationAttribute
{
	public FirstCharacterUppercaseOrNumberAttribute()
	{
		ErrorMessage = "First character must be an uppercase or a number.";
	}

	protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
	{
		if (value == null || string.IsNullOrEmpty(value.ToString()))
			return ValidationResult.Success;

		var firstLetter = value.ToString()![0];

		return firstLetter == char.ToUpper(firstLetter) || char.IsDigit(firstLetter)
			? ValidationResult.Success
			: new ValidationResult(ErrorMessage);
	}
}
