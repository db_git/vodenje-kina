//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.ComponentModel.DataAnnotations;
using API.DTO.Validations;

namespace API.DTO.Request.Seat;

#if DEBUG
[UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
#endif
public record CreateSeatDto
{
	[Range(1, 500, ErrorMessage = "Value must be between {1} and {2}.")]
	[Required(ErrorMessage = "This field is required.")]
	public int Number { get; init; }

	[Required(ErrorMessage = "Select at least one hall.")]
	[MinLength(1, ErrorMessage = "Select at least one hall.")]
	[ValidateCollection<GuidAttribute>(ErrorMessage = "Select at least one hall.")]
	public IEnumerable<Guid> Halls { get; init; } = null!;
}
