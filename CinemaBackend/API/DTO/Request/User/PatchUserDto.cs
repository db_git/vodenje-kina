//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.ComponentModel.DataAnnotations;

namespace API.DTO.Request.User;

#if DEBUG
[UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
#endif
public sealed record PatchUserDto
{
	[MinLength(1, ErrorMessage = "Minimum length is {1} characters.")]
	[MaxLength(256, ErrorMessage = "Maximum length must not exceed {1} characters.")]
	public string? Name { get; init; }

	[MinLength(8, ErrorMessage = "Minimum length is {1} characters.")]
	public string? Image { get; init; }
}
