//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.ComponentModel.DataAnnotations;
using API.DTO.Validations;

namespace API.DTO.Request.Cinema;

#if DEBUG
[UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
#endif
public record CreateCinemaDto
{
	[FirstLetterUppercase]
	[MinLength(2, ErrorMessage = "Minimum length is {1} characters.")]
	[MaxLength(64, ErrorMessage = "Maximum length must not exceed {1} characters.")]
	[Required(ErrorMessage = "This field is required.")]
	public string Name { get; init; } = null!;

	[ForbidNumbers]
	[FirstLetterUppercase]
	[MinLength(2, ErrorMessage = "Minimum length is {1} characters.")]
	[MaxLength(64, ErrorMessage = "Maximum length must not exceed {1} characters.")]
	[Required(ErrorMessage = "This field is required.")]
	public string City { get; init; } = null!;

	[FirstLetterUppercase]
	[MinLength(2, ErrorMessage = "Minimum length is {1} characters.")]
	[MaxLength(128, ErrorMessage = "Maximum length must not exceed {1} characters.")]
	[Required(ErrorMessage = "This field is required.")]
	public string Street { get; init; } = null!;

	[Required(ErrorMessage = "This field is required.")]
	[Range(1, 10E9, ErrorMessage = "Value must be between {1} and {2}.")]
	public decimal TicketPrice { get; init; }
}
