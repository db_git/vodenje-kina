//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.ComponentModel.DataAnnotations;
using API.DTO.Validations;

namespace API.DTO.Request.Movie;

#if DEBUG
[UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
#endif
public record CreateMovieDto
{
	[FirstCharacterUppercaseOrNumber]
	[MaxLength(256, ErrorMessage = "Maximum length must not exceed {1} characters.")]
	[Required(ErrorMessage = "This field is required.")]
	public string Title { get; init; } = null!;

	[Range(1900, 2199, ErrorMessage = "Value must be between {1} and {2}.")]
	[Required(ErrorMessage = "This field is required.")]
	public uint Year { get; init; }

	[Range(30, 360, ErrorMessage = "Value must be between {1} and {2}.")]
	[Required(ErrorMessage = "This field is required.")]
	public uint Duration { get; init; }

	[FirstCharacterUppercaseOrNumber]
	[MinLength(8, ErrorMessage = "Minimum length is {1} characters.")]
	[MaxLength(256, ErrorMessage = "Maximum length must not exceed {1} characters.")]
	[Required(ErrorMessage = "This field is required.")]
	public string Tagline { get; init; } = null!;

	[FirstCharacterUppercaseOrNumber]
	[MinLength(32, ErrorMessage = "Minimum length is {1} characters.")]
	[MaxLength(2048, ErrorMessage = "Maximum length must not exceed {1} characters.")]
	[Required(ErrorMessage = "This field is required.")]
	public string Summary { get; init; } = null!;

	public bool InCinemas { get; init; }

	[Required(ErrorMessage = "This field is required.")]
	[MinLength(8, ErrorMessage = "Minimum length is {1} characters.")]
	public string PosterUrl { get; init; } = null!;

	[Required(ErrorMessage = "This field is required.")]
	[MinLength(8, ErrorMessage = "Minimum length is {1} characters.")]
	public string BackdropUrl { get; init; } = null!;

	[Url(ErrorMessage = "Value is not a valid url.")]
	[MinLength(8, ErrorMessage = "Minimum length is {1} characters.")]
	[MaxLength(256, ErrorMessage = "Maximum length must not exceed {1} characters.")]
	[Required(ErrorMessage = "This field is required.")]
	public string TrailerUrl { get; init; } = null!;

	[Guid(ErrorMessage = "Select a rating.")]
	public Guid? FilmRating { get; init; }

	[Required(ErrorMessage = "Select at least one genre.")]
	[MinLength(1, ErrorMessage = "Select at least one genre.")]
	[ValidateCollection<GuidAttribute>]
	public IEnumerable<Guid> Genres { get; init; } = null!;

	[Required(ErrorMessage = "Select at least one actor.")]
	[MinLength(1, ErrorMessage = "Select at least one actor.")]
	public IEnumerable<CreateMovieActorDto> Actors { get; init; } = null!;

	[Required(ErrorMessage = "Select at least one director.")]
	[MinLength(1, ErrorMessage = "Select at least one director.")]
	[ValidateCollection<StringNotNullOrEmptyAttribute>]
	public IEnumerable<string> Directors { get; init; } = null!;
}
