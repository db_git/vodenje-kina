//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.ComponentModel.DataAnnotations;
using API.DTO.Validations;

namespace API.DTO.Request.Food;

#if DEBUG
[UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
#endif
public record CreateFoodDto
{
	[FirstLetterUppercase]
	[MinLength(2, ErrorMessage = "Minimum length is {1} characters.")]
	[MaxLength(64, ErrorMessage = "Maximum length must not exceed {1} characters.")]
	[Required(ErrorMessage = "This field is required.")]
	public string Name { get; init; } = null!;

	[Range(1, 1000, ErrorMessage = "Value must be between {1} and {2}.")]
	[Required(ErrorMessage = "This field is required.")]
	public decimal Price { get; init; }

	[Range(0, 10E9, ErrorMessage = "Value must be between {1} and {2}.")]
	[Required(ErrorMessage = "This field is required.")]
	public ulong AvailableQuantity { get; init; }

	[SnackOrDrink]
	[Required(ErrorMessage = "This field is required.")]
	public string Type { get; init; } = null!;

	[Range(0.001, 25, ErrorMessage = "Value must be between {1} and {2}.")]
	[Required(ErrorMessage = "This field is required.")]
	public decimal Size { get; init; }

	[Required(ErrorMessage = "This field is required.")]
	[MinLength(8, ErrorMessage = "Minimum length is {1} characters.")]
	public string ImageUrl { get; init; } = null!;
}
