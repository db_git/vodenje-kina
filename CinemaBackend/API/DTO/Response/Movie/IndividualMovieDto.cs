//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using API.DTO.Response.FilmRating;
using API.DTO.Response.Genre;

namespace API.DTO.Response.Movie;

public sealed record IndividualMovieDto
{
	public string Id { get; init; } = null!;
	public string Title { get; init; } = null!;
	public uint Year { get; init; }
	public uint Duration { get; init; }
	public string Tagline { get; init; } = null!;
	public string Summary { get; init; } = null!;
	public bool InCinemas { get; init; }
	public string PosterUrl { get; init; } = null!;
	public string BackdropUrl { get; init; } = null!;
	public string TrailerUrl { get; init; } = null!;
	public FilmRatingDto FilmRating { get; init; } = null!;
	public IEnumerable<GenreDto> Genres { get; init; } = null!;
	public IEnumerable<MovieActorDto> Actors { get; init; } = null!;
	public IEnumerable<MovieDirectorDto> Directors { get; init; } = null!;
}
