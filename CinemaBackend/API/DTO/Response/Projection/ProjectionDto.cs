//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using API.DTO.Response.Hall;
using API.DTO.Response.Movie;

namespace API.DTO.Response.Projection;

public sealed record ProjectionDto
{
	public Guid Id { get; init; }
	public MovieDto Movie { get; init; } = null!;
	public HallDto Hall { get; init; } = null!;
	public LocalDateTime Time { get; init; }
}
