//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace API.DTO.Response.Food;

public sealed record FoodDto
{
	public Guid Id { get; init; }
	public string Name { get; init; } = null!;
	public decimal Price { get; init; }
	public ulong AvailableQuantity { get; init; }
	public string Type { get; init; } = null!;
	public decimal Size { get; init; }
	public string ImageUrl { get; init; } = null!;
}
