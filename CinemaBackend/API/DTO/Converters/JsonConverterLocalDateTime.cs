//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Globalization;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;

namespace API.DTO.Converters;

internal sealed class JsonConverterLocalDateTime : JsonConverter<LocalDateTime>
{
	public override LocalDateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
	{
		try
		{
			var parsedDate = Regex
				.Split(reader.GetString()!, @"^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})Z$")
				.Where(str => !string.IsNullOrEmpty(str))
				.ToArray();

			return new LocalDateTime(
				int.Parse(parsedDate[0]),
				int.Parse(parsedDate[1]),
				int.Parse(parsedDate[2]),
				int.Parse(parsedDate[3]),
				int.Parse(parsedDate[4]),
				int.Parse(parsedDate[5]),
				CalendarSystem.Iso
			);
		}
		catch (Exception exception)
		{
			throw new JsonException(
				"Date and time must be in ISO-8601 UTC format (YYYY-MM-DDThh:mm:ssZ).",
				exception
			);
		}
	}

	public override void Write(Utf8JsonWriter writer, LocalDateTime value, JsonSerializerOptions options)
	{
		writer.WriteStringValue(value.ToString("s", CultureInfo.InvariantCulture) + "Z");
	}
}
