//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Collections.Immutable;
using API.DTO.Request.Reservation;
using API.DTO.Response.Dashboard;
using API.DTO.Response.Reservation;
using API.DTO.Response.Seat;

#if DEBUG
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("API.Tests")]
[assembly: InternalsVisibleTo("Tests.Common")]
#endif
namespace API.DTO.Mappings;

internal sealed class ReservationMapper : Profile
{
	public ReservationMapper()
	{
		CreateMap<Reservation, ReservationDto>()
			.ForMember(reservation => reservation.Seats, options
				=> options.MapFrom(reservation => reservation.ReservationSeats
					.Select(reservationSeat => new SeatDto
					{
						Id = reservationSeat.Seat.Id,
						Number = reservationSeat.Seat.Number
					})
				));

		CreateMap<Reservation, RevenueDto>()
			.ForMember(dto => dto.Price, options
				=> options.MapFrom(reservation => reservation.Projection.Hall.Cinema.TicketPrice))
			.ForMember(dto => dto.Orders, options
				=> options.MapFrom(reservation => reservation.ReservationSeats.Count));

		CreateMap<CreateReservationDto, Reservation>()
			.ForMember(reservation => reservation.Id, options
				=> options.Ignore())
			.ForMember(reservation => reservation.CreatedAt, options
				=> options.Ignore())
			.ForMember(reservation => reservation.ModifiedAt, options
				=> options.Ignore())
			.ForMember(reservation => reservation.User, options
				=> options.Ignore())
			.ForMember(reservation => reservation.Projection, options
				=> options.Ignore())
			.ForMember(reservation => reservation.UserId, options
				=> options.Ignore())
			.ForMember(reservation => reservation.ProjectionId, options
				=> options.MapFrom(reservationDto => reservationDto.Projection))
			.ForMember(reservation => reservation.ReservationSeats, options
				=> options.MapFrom(MapSeatsFromDto));
	}

	private static IEnumerable<ReservationSeat> MapSeatsFromDto(CreateReservationDto reservationDto, Reservation _)
	{
		return reservationDto.Seats
			.Select(seatId => new ReservationSeat
			{
				CreatedAt = SystemClock.Instance.GetCurrentInstant(),
				ModifiedAt = SystemClock.Instance.GetCurrentInstant(),
				SeatId = seatId
			})
			.ToImmutableList();
	}
}
