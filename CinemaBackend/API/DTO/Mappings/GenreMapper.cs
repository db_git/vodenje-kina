//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using API.DTO.Request.Genre;
using API.DTO.Response.Genre;

#if DEBUG
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("API.Tests")]
[assembly: InternalsVisibleTo("Tests.Common")]
#endif
namespace API.DTO.Mappings;

internal sealed class GenreMapper : Profile
{
	public GenreMapper()
	{
		CreateMap<Genre, GenreDto>();

		CreateMap<CreateGenreDto, Genre>()
			.ForMember(genre => genre.Id, options
				=> options.Ignore())
			.ForMember(genre => genre.CreatedAt, options
				=> options.Ignore())
			.ForMember(genre => genre.ModifiedAt, options
				=> options.Ignore())
			.ForMember(genre => genre.MovieGenres, options
				=> options.Ignore());
	}
}
