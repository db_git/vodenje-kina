//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using API.DTO.Request.UserRating;
using API.DTO.Response.UserRating;

#if DEBUG
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("API.Tests")]
[assembly: InternalsVisibleTo("Tests.Common")]
#endif
namespace API.DTO.Mappings;

internal sealed class UserRatingMapper : Profile
{
	public UserRatingMapper()
	{
		CreateMap<UserRating, UserRatingDto>();
		CreateMap<UserRating, SimpleUserRatingDto>();

		CreateMap<CreateUserRatingDto, UserRating>()
			.ForMember(rating => rating.User, options
				=> options.Ignore())
			.ForMember(rating => rating.UserId, options
				=> options.Ignore())
			.ForMember(rating => rating.Movie, options
				=> options.Ignore())
			.ForMember(rating => rating.CreatedAt, options
				=> options.Ignore())
			.ForMember(rating => rating.ModifiedAt, options
				=> options.Ignore())
			.ForMember(rating => rating.MovieId, options
				=> options.MapFrom(dto => dto.Movie))
			.ForMember(rating => rating.Rating, options
				=> options.MapFrom(dto => dto.Rating));
	}
}
