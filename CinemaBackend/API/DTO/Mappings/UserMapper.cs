//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using API.DTO.Request.User;
using API.DTO.Response.User;
using API.DTO.Response.UserComment;
using API.DTO.Response.UserRating;
using Domain.Entities.Identity;

#if DEBUG
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("API.Tests")]
[assembly: InternalsVisibleTo("Tests.Common")]
#endif
namespace API.DTO.Mappings;

internal sealed class UserMapper : Profile
{
	public UserMapper()
	{
		CreateMap<User, UserDto>()
			.ForMember(userDto => userDto.Roles, options
				=> options.MapFrom(user => user.UserRoles
					.Select(role => role.Role.Name!)
				));

		CreateMap<User, UserCommentUserDto>();
		CreateMap<User, UserRatingUserDto>();

		CreateMap<UserRegisterDto, User>()
			.ForMember(user => user.Id, options
				=> options.Ignore())
			.ForMember(user => user.CreatedAt, options
				=> options.Ignore())
			.ForMember(user => user.ModifiedAt, options
				=> options.Ignore())
			.ForMember(user => user.Reservations, options
				=> options.Ignore())
			.ForMember(user => user.ImageUrl, options
				=> options.Ignore())
			.ForMember(user => user.ImageId, options
				=> options.Ignore())
			.ForMember(user => user.Claims, options
				=> options.Ignore())
			.ForMember(user => user.Logins, options
				=> options.Ignore())
			.ForMember(user => user.Tokens, options
				=> options.Ignore())
			.ForMember(user => user.UserRoles, options
				=> options.Ignore())
			.ForMember(user => user.UserName, options
				=> options.Ignore())
			.ForMember(user => user.NormalizedUserName, options
				=> options.Ignore())
			.ForMember(user => user.NormalizedEmail, options
				=> options.Ignore())
			.ForMember(user => user.EmailConfirmed, options
				=> options.Ignore())
			.ForMember(user => user.PasswordHash, options
				=> options.Ignore())
			.ForMember(user => user.SecurityStamp, options
				=> options.Ignore())
			.ForMember(user => user.ConcurrencyStamp, options
				=> options.Ignore())
			.ForMember(user => user.PhoneNumber, options
				=> options.Ignore())
			.ForMember(user => user.PhoneNumberConfirmed, options
				=> options.Ignore())
			.ForMember(user => user.TwoFactorEnabled, options
				=> options.Ignore())
			.ForMember(user => user.LockoutEnd, options
				=> options.Ignore())
			.ForMember(user => user.LockoutEnabled, options
				=> options.Ignore())
			.ForMember(user => user.AccessFailedCount, options
				=> options.Ignore())
			.AfterMap((_, user) =>
			{
				user.UserName = user.Email;
				user.NormalizedUserName = user.NormalizedEmail;
				user.EmailConfirmed = false;
			});
	}
}
