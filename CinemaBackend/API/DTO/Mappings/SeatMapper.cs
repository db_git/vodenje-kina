//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Collections.Immutable;
using API.DTO.Request.Seat;
using API.DTO.Response.Seat;

#if DEBUG
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("API.Tests")]
[assembly: InternalsVisibleTo("Tests.Common")]
#endif
namespace API.DTO.Mappings;

internal sealed class SeatMapper : Profile
{
	public SeatMapper()
	{
		CreateMap<Seat, SeatDto>();

		CreateMap<Seat, IndividualSeatDto>()
			.ForMember(seatDto => seatDto.Halls, options
				=> options.MapFrom(seat => seat.HallSeats
					.Where(hs => hs.SeatId == seat.Id)
					.Select(hs => new SeatHallDto { Id = hs.Hall.Id, Name = hs.Hall.Name })
				));

		CreateMap<CreateSeatDto, Seat>()
			.ForMember(seat => seat.Id, options
				=> options.Ignore())
			.ForMember(seat => seat.CreatedAt, options
				=> options.Ignore())
			.ForMember(seat => seat.ModifiedAt, options
				=> options.Ignore())
			.ForMember(seat => seat.ReservationSeats, options
				=> options.Ignore())
			.ForMember(seat => seat.HallSeats, options
				=> options.MapFrom(MapHallsFromDto));
	}

	private static IEnumerable<HallSeat> MapHallsFromDto(CreateSeatDto seatDto, Seat seat)
	{
		return seatDto.Halls
			.Select(hallId => new HallSeat { HallId = hallId, SeatId = seat.Id })
			.ToImmutableList();
	}
}
