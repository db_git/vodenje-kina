//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using API.DTO.Request.Reservation;
using API.DTO.Request.Souvenir;
using API.DTO.Response.Dashboard;
using API.DTO.Response.Souvenir;

#if DEBUG
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("API.Tests")]
[assembly: InternalsVisibleTo("Tests.Common")]
#endif
namespace API.DTO.Mappings;

internal sealed class SouvenirMapper : Profile
{
	public SouvenirMapper()
	{
		CreateMap<Souvenir, SouvenirDto>();

		CreateMap<Souvenir, RevenueDto>()
			.ForMember(dto => dto.Orders, options
				=> options.MapFrom(souvenir => souvenir.SouvenirOrders.Count));

		CreateMap<SouvenirOrder, SouvenirOrderDto>()
			.ForMember(dto => dto.Id, options
				=> options.MapFrom(order => order.Souvenir.Id))
			.ForMember(dto => dto.Name, options
				=> options.MapFrom(order => order.Souvenir.Name))
			.ForMember(dto => dto.ImageUrl, options
				=> options.MapFrom(order => order.Souvenir.ImageUrl))
			.ForMember(dto => dto.OrderedQuantity, options
				=> options.MapFrom(order => order.OrderedQuantity));

		CreateMap<CreateSouvenirDto, Souvenir>()
			.ForMember(souvenir => souvenir.Id, options
				=> options.Ignore())
			.ForMember(souvenir => souvenir.CreatedAt, options
				=> options.Ignore())
			.ForMember(souvenir => souvenir.ModifiedAt, options
				=> options.Ignore())
			.ForMember(souvenir => souvenir.ImageUrl, options
				=> options.Ignore())
			.ForMember(souvenir => souvenir.ImageId, options
				=> options.Ignore())
			.ForMember(souvenir => souvenir.SouvenirOrders, options
				=> options.Ignore());

		CreateMap<CreateOrderDto, SouvenirOrder>()
			.ForMember(souvenirOrder => souvenirOrder.Id, options
				=> options.Ignore())
			.ForMember(souvenirOrder => souvenirOrder.CreatedAt, options
				=> options.MapFrom(_ => SystemClock.Instance.GetCurrentInstant()))
			.ForMember(souvenirOrder => souvenirOrder.ModifiedAt, options
				=> options.Ignore())
			.ForMember(souvenirOrder => souvenirOrder.Souvenir, options
				=> options.Ignore())
			.ForMember(souvenirOrder => souvenirOrder.SouvenirId, options
				=> options.MapFrom(orderDto => orderDto.Id))
			.ForMember(souvenirOrder => souvenirOrder.OrderedQuantity, options
				=> options.MapFrom(orderDto => orderDto.Quantity));
	}
}
