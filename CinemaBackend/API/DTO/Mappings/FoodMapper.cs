//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using API.DTO.Request.Food;
using API.DTO.Request.Reservation;
using API.DTO.Response.Dashboard;
using API.DTO.Response.Food;

#if DEBUG
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("API.Tests")]
[assembly: InternalsVisibleTo("Tests.Common")]
#endif
namespace API.DTO.Mappings;

internal sealed class FoodMapper : Profile
{
	public FoodMapper()
	{
		CreateMap<Food, FoodDto>();

		CreateMap<Food, RevenueDto>()
			.ForMember(dto => dto.Orders, options
				=> options.MapFrom(food => food.FoodOrders.Count));

		CreateMap<FoodOrder, FoodOrderDto>()
			.ForMember(dto => dto.Id, options
				=> options.MapFrom(order => order.Food.Id))
			.ForMember(dto => dto.Name, options
				=> options.MapFrom(order => order.Food.Name))
			.ForMember(dto => dto.Type, options
				=> options.MapFrom(order => order.Food.Type))
			.ForMember(dto => dto.Size, options
				=> options.MapFrom(order => order.Food.Size))
			.ForMember(dto => dto.ImageUrl, options
				=> options.MapFrom(order => order.Food.ImageUrl))
			.ForMember(dto => dto.OrderedQuantity, options
				=> options.MapFrom(order => order.OrderedQuantity));

		CreateMap<CreateFoodDto, Food>()
			.ForMember(food => food.Id, options
				=> options.Ignore())
			.ForMember(food => food.CreatedAt, options
				=> options.Ignore())
			.ForMember(food => food.ModifiedAt, options
				=> options.Ignore())
			.ForMember(food => food.ImageUrl, options
				=> options.Ignore())
			.ForMember(food => food.ImageId, options
				=> options.Ignore())
			.ForMember(food => food.FoodOrders, options
				=> options.Ignore());

		CreateMap<CreateOrderDto, FoodOrder>()
			.ForMember(foodOrder => foodOrder.Id, options
				=> options.Ignore())
			.ForMember(foodOrder => foodOrder.CreatedAt, options
				=> options.MapFrom(_ => SystemClock.Instance.GetCurrentInstant()))
			.ForMember(foodOrder => foodOrder.ModifiedAt, options
				=> options.Ignore())
			.ForMember(foodOrder => foodOrder.Food, options
				=> options.Ignore())
			.ForMember(foodOrder => foodOrder.FoodId, options
				=> options.MapFrom(orderDto => orderDto.Id))
			.ForMember(foodOrder => foodOrder.OrderedQuantity, options
				=> options.MapFrom(orderDto => orderDto.Quantity));
	}
}
