//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using API.DTO.Request.Person;
using API.DTO.Response.Person;

#if DEBUG
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("API.Tests")]
[assembly: InternalsVisibleTo("Tests.Common")]
#endif
namespace API.DTO.Mappings;

internal sealed class PersonMapper : Profile
{
	public PersonMapper()
	{
		CreateMap<Person, PersonDto>()
			.ForMember(personDto => personDto.Movies, options
				=> options.MapFrom(person => person.MoviePeople
					.Where(moviePerson => moviePerson.PersonId == person.Id)
					.OrderBy(moviePerson => moviePerson.Movie.Year)
					.Select(moviePerson => new PersonMovieDto
					{
						Id = moviePerson.Movie.Id,
						Title = moviePerson.Movie.Title,
						Year = moviePerson.Movie.Year,
						Character = moviePerson.Character,
						Type = moviePerson.Type,
						PosterUrl = moviePerson.Movie.PosterUrl,
						BackdropUrl = moviePerson.Movie.BackdropUrl
					})
				));

		CreateMap<CreatePersonDto, Person>()
			.ForMember(person => person.Id, options
				=> options.Ignore())
			.ForMember(person => person.CreatedAt, options
				=> options.Ignore())
			.ForMember(person => person.ModifiedAt, options
				=> options.Ignore())
			.ForMember(person => person.ImageUrl, options
				=> options.Ignore())
			.ForMember(person => person.ImageId, options
				=> options.Ignore())
			.ForMember(person => person.MoviePeople, options
				=> options.Ignore());
	}
}
