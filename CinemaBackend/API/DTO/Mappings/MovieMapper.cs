//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Collections.Immutable;
using API.DTO.Request.Movie;
using API.DTO.Response.Genre;
using API.DTO.Response.Movie;
using API.DTO.Response.UserComment;
using API.DTO.Response.UserRating;

#if DEBUG
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("API.Tests")]
[assembly: InternalsVisibleTo("Tests.Common")]
#endif
namespace API.DTO.Mappings;

internal sealed class MovieMapper : Profile
{
	public MovieMapper()
	{
		CreateMap<Movie, MovieDto>()
			.ForMember(dto => dto.Rating, options
				=> options.MapFrom(movie
					=> movie.Ratings.Count == 0
						? 0M
						: (decimal) movie.Ratings.Sum(r => r.Rating) / movie.Ratings.Count));

		CreateMap<Movie, AdminMovieDto>()
			.ForMember(dto => dto.Rating, options
				=> options.MapFrom(movie
					=> movie.Ratings.Count == 0
						? 0M
						: (decimal) movie.Ratings.Sum(r => r.Rating) / movie.Ratings.Count));

		CreateMap<Movie, SimpleMovieDto>()
			.ForMember(dto => dto.Rating, options
				=> options.MapFrom(movie
					=> movie.Ratings.Count == 0
						? 0M
						: (decimal) movie.Ratings.Sum(r => r.Rating) / movie.Ratings.Count));

		CreateMap<Movie, UserCommentMovieDto>();
		CreateMap<Movie, UserRatingMovieDto>();

		CreateMap<Movie, IndividualMovieDto>()
			.ForMember(movieDto => movieDto.Genres, options
				=> options.MapFrom(movie => movie.MovieGenres
					.Select(genre => new GenreDto { Id = genre.GenreId, Name = genre.Genre.Name })
				))
			.ForMember(movieDto => movieDto.Actors, options
				=> options.MapFrom(movie => movie.MoviePeople
					.Where(moviePerson => moviePerson.Type == "Actor")
					.Select(moviePerson => new MovieActorDto
					{
						Id = moviePerson.PersonId,
						Name = moviePerson.Person.Name,
						Character = moviePerson.Character,
						ImageUrl = moviePerson.Person.ImageUrl,
						Order = moviePerson.Order
					})
					.OrderBy(actor => actor.Order)
				))
			.ForMember(movieDto => movieDto.Directors, options
				=> options.MapFrom(movie => movie.MoviePeople
					.Where(moviePerson => moviePerson.Type == "Director")
					.Select(moviePerson => new MovieDirectorDto
					{
						Id = moviePerson.PersonId,
						Name = moviePerson.Person.Name,
						ImageUrl = moviePerson.Person.ImageUrl
					})
				))
			.ForMember(movieDto => movieDto.PosterUrl, options
				=> options.MapFrom(movie => movie.PosterUrl))
			.ForMember(movieDto => movieDto.BackdropUrl, options
				=> options.MapFrom(movie => movie.BackdropUrl));

		CreateMap<CreateMovieDto, Movie>()
			.ForMember(movie => movie.Id, options
				=> options.Ignore())
			.ForMember(movie => movie.CreatedAt, options
				=> options.Ignore())
			.ForMember(movie => movie.ModifiedAt, options
				=> options.Ignore())
			.ForMember(movie => movie.IsFeatured, options
				=> options.Ignore())
			.ForMember(movie => movie.IsRecommended, options
				=> options.Ignore())
			.ForMember(movie => movie.Projections, options
				=> options.Ignore())
			.ForMember(movie => movie.FilmRating, options
				=> options.Ignore())
			.ForMember(movie => movie.FilmRatingId, options
				=> options.MapFrom(movieDto => movieDto.FilmRating))
			.ForMember(movie => movie.PosterUrl, options
				=> options.Ignore())
			.ForMember(movie => movie.PosterId, options
				=> options.Ignore())
			.ForMember(movie => movie.BackdropUrl, options
				=> options.Ignore())
			.ForMember(movie => movie.BackdropId, options
				=> options.Ignore())
			.ForMember(movie => movie.Ratings, options
				=> options.Ignore())
			.ForMember(movie => movie.MovieGenres, options
				=> options.MapFrom(MapGenresFromDto))
			.ForMember(movie => movie.MoviePeople, options
				=> options.MapFrom(MapActorsAndDirectorsFromDto));

		CreateMap<PatchMovieDto, Movie>()
			.ForMember(movie => movie.Id, options
				=> options.Ignore())
			.ForMember(movie => movie.CreatedAt, options
				=> options.Ignore())
			.ForMember(movie => movie.ModifiedAt, options
				=> options.Ignore())
			.ForMember(movie => movie.Title, options
				=> options.Ignore())
			.ForMember(movie => movie.Year, options
				=> options.Ignore())
			.ForMember(movie => movie.Duration, options
				=> options.Ignore())
			.ForMember(movie => movie.Tagline, options
				=> options.Ignore())
			.ForMember(movie => movie.Summary, options
				=> options.Ignore())
			.ForMember(movie => movie.PosterUrl, options
				=> options.Ignore())
			.ForMember(movie => movie.PosterId, options
				=> options.Ignore())
			.ForMember(movie => movie.BackdropUrl, options
				=> options.Ignore())
			.ForMember(movie => movie.BackdropId, options
				=> options.Ignore())
			.ForMember(movie => movie.TrailerUrl, options
				=> options.Ignore())
			.ForMember(movie => movie.FilmRating, options
				=> options.Ignore())
			.ForMember(movie => movie.FilmRatingId, options
				=> options.Ignore())
			.ForMember(movie => movie.MovieGenres, options
				=> options.Ignore())
			.ForMember(movie => movie.MoviePeople, options
				=> options.Ignore())
			.ForMember(movie => movie.Projections, options
				=> options.Ignore())
			.ForMember(movie => movie.Ratings, options
				=> options.Ignore())
			.ForMember(movie => movie.InCinemas, options
				=> options.MapFrom(dto => dto.InCinemas))
			.ForMember(movie => movie.IsFeatured, options
				=> options.MapFrom(dto => dto.IsFeatured))
			.ForMember(movie => movie.IsRecommended, options
				=> options.MapFrom(dto => dto.IsRecommended))
			.ReverseMap();
	}

	private static IEnumerable<MovieGenre> MapGenresFromDto(CreateMovieDto movieDto, Movie movie)
	{
		return movieDto.Genres
			.Select(genreId => new MovieGenre
			{
				CreatedAt = SystemClock.Instance.GetCurrentInstant(),
				GenreId = genreId,
				MovieId = movie.Id
			})
			.ToImmutableList();
	}

	private static IEnumerable<MoviePerson> MapActorsAndDirectorsFromDto(
		CreateMovieDto movieDto, Movie movie)
	{
		var moviePeople = new List<MoviePerson>();

		moviePeople.AddRange(movieDto.Actors
			.Select((actorDto, index) => new MoviePerson
			{
				CreatedAt = SystemClock.Instance.GetCurrentInstant(),
				PersonId = actorDto.Id,
				MovieId = movie.Id,
				Type = "Actor",
				Character = actorDto.Character,
				Order = (uint) index + 1
			})
		);
		moviePeople.AddRange(movieDto.Directors
			.Select(personId => new MoviePerson
			{
				CreatedAt = SystemClock.Instance.GetCurrentInstant(),
				PersonId = personId,
				MovieId = movie.Id,
				Type = "Director",
				Character = "Director",
				Order = 0
			})
		);

		return moviePeople.ToImmutableList();
	}
}
