//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using API.DTO.Request.Projection;
using API.DTO.Response.Projection;

#if DEBUG
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("API.Tests")]
[assembly: InternalsVisibleTo("Tests.Common")]
#endif
namespace API.DTO.Mappings;

internal sealed class ProjectionMapper : Profile
{
	public ProjectionMapper()
	{
		CreateMap<Projection, ProjectionDto>();

		CreateMap<Projection, SimpleProjectionDto>()
			.ForMember(dto => dto.Id, options
				=> options.MapFrom(projection => projection.Id))
			.ForMember(dto => dto.Time, options
				=> options.MapFrom(projection => projection.Time));

		CreateMap<CreateProjectionDto, Projection>()
			.ForMember(projection => projection.Id, options
				=> options.Ignore())
			.ForMember(projection => projection.CreatedAt, options
				=> options.Ignore())
			.ForMember(projection => projection.ModifiedAt, options
				=> options.Ignore())
			.ForMember(projection => projection.Reservations, options
				=> options.Ignore())
			.ForMember(projection => projection.Hall, options
				=> options.Ignore())
			.ForMember(projection => projection.Movie, options
				=> options.Ignore())
			.ForMember(projection => projection.Time, options
				=> options.MapFrom(projectionDto => projectionDto.Time))
			.ForMember(projection => projection.HallId, options
				=> options.MapFrom(projectionDto => projectionDto.Hall))
			.ForMember(projection => projection.MovieId, options
				=> options.MapFrom(projectionDto => projectionDto.Movie));
	}
}
