//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Domain.Entities.Identity;

/// <remarks>
///     <c>User</c> class code was based on official documentation.
///     <see href="https://docs.microsoft.com/aspnet/core/security/authentication/customize-identity-model">
///         Identity model customization in ASP.NET Core
///     </see>
/// </remarks>
public sealed class User : IdentityUser<Guid>, IEntity
{
	public Instant CreatedAt { get; set; }
	public Instant? ModifiedAt { get; set; }

	/// <remarks>
	///     Maximum length is 256 characters.
	/// </remarks>
	public string Name { get; set; } = null!;

	/// <remarks>
	///     Maximum length is 2048 characters.
	/// </remarks>
	public string? ImageUrl { get; set; }

	public string? ImageId { get; set; }

	/// <summary>
	///     Collection navigation property to associated reservations.
	/// </summary>
	public ICollection<Reservation> Reservations { get; set; } = null!;

	/// <summary>
	///     Collection navigation property to associated claims.
	/// </summary>
	public ICollection<UserClaim> Claims { get; set; } = null!;

	/// <summary>
	///     Collection navigation property to associated logins.
	/// </summary>
	public ICollection<UserLogin> Logins { get; set; } = null!;

	/// <summary>
	///     Collection navigation property to associated tokens.
	/// </summary>
	public ICollection<UserToken> Tokens { get; set; } = null!;

	/// <summary>
	///     Collection navigation property to associated roles.
	/// </summary>
	public ICollection<UserRole> UserRoles { get; set; } = null!;
}
