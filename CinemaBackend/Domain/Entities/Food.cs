//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Domain.Entities;

public sealed class Food : IEntity
{
	public Guid Id { get; set; }
	public Instant CreatedAt { get; set; }
	public Instant? ModifiedAt { get; set; }

	/// <remarks>
	///     Maximum length is 64 characters.
	/// </remarks>
	public string Name { get; set; } = null!;

	public decimal Price { get; set; }
	public ulong AvailableQuantity { get; set; }

	/// <summary>
	///     A value indicating food type. Can be <c>"Snack"</c> or <c>"Drink"</c>.
	/// </summary>
	public string Type { get; set; } = null!;

	/// <summary>
	///     A value indicating food size. Unit is liter (or kilogram).
	/// </summary>
	public decimal Size { get; set; }

	/// <remarks>
	///     Maximum length is 2048 characters.
	/// </remarks>
	public string ImageUrl { get; set; } = null!;

	public string ImageId { get; set; } = null!;

	/// <summary>
	///     Collection navigation property to associated food orders.
	/// </summary>
	public ICollection<FoodOrder> FoodOrders { get; set; } = null!;
}
