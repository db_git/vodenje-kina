//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Domain.Entities;

public sealed class MoviePerson : IEntity
{
	public Guid Id { get; set; }
	public Instant CreatedAt { get; set; }
	public Instant? ModifiedAt { get; set; }

	/// <summary>
	///     A value indicating person type.
	///     Can be <c>"Actor"</c> or <c>"Director"</c>.
	/// </summary>
	public string Type { get; set; } = null!;

	/// <summary>
	///     A value that shows which character the actor is playing.
	///     Must be <c>"Director"</c> for directors.
	/// </summary>
	/// <remarks>
	///     Maximum length is 256 characters.
	/// </remarks>
	public string Character { get; set; } = null!;

	/// <summary>
	///     A value that indicates the order in which the actors should be shown.
	///     Must be <c>0</c> for directors.
	/// </summary>
	public uint Order { get; set; }

	public string MovieId { get; set; } = null!;

	/// <summary>
	///     Reference navigation property to associated movie.
	/// </summary>
	public Movie Movie { get; set; } = null!;

	public string PersonId { get; set; } = null!;

	/// <summary>
	///     Reference navigation property to associated person.
	/// </summary>
	public Person Person { get; set; } = null!;
}
