//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Domain.Entities;

public sealed class Person : IEntity
{
	/// <summary>
	///     Entity primary key. It is visible to the user and should be derived from name.
	/// </summary>
	public string Id { get; set; } = null!;

	public Instant CreatedAt { get; set; }
	public Instant? ModifiedAt { get; set; }

	/// <remarks>
	///     Maximum length is 256 characters.
	/// </remarks>
	public string Name { get; set; } = null!;

	public LocalDate DateOfBirth { get; set; }
	public LocalDate? DateOfDeath { get; set; }

	/// <remarks>
	///     Maximum length is 2048 characters.
	/// </remarks>
	public string ImageUrl { get; set; } = null!;

	public string ImageId { get; set; } = null!;

	/// <remarks>
	///     Maximum length is 4096 characters.
	/// </remarks>
	public string Biography { get; set; } = null!;

	/// <summary>
	///     Collection navigation property to associated movies.
	/// </summary>
	public ICollection<MoviePerson> MoviePeople { get; set; } = null!;
}
