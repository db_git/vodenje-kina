//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Domain.Entities.Identity;

namespace Domain.Entities;

public sealed class Reservation : IEntity
{
	public Guid Id { get; set; }
	public Instant CreatedAt { get; set; }
	public Instant? ModifiedAt { get; set; }

	public Guid UserId { get; set; }

	/// <summary>
	///     Reference navigation property to associated user.
	/// </summary>
	public User User { get; set; } = null!;

	public Guid ProjectionId { get; set; }

	/// <summary>
	///     Reference navigation property to associated projection.
	/// </summary>
	public Projection Projection { get; set; } = null!;

	/// <summary>
	///     Collection navigation property to associated seats.
	/// </summary>
	public ICollection<ReservationSeat> ReservationSeats { get; set; } = null!;

	/// <summary>
	///     Collection navigation property to associated food orders.
	/// </summary>
	public ICollection<FoodOrder> FoodOrders { get; set; } = null!;

	/// <summary>
	///     Collection navigation property to associated souvenir orders.
	/// </summary>
	public ICollection<SouvenirOrder> SouvenirOrders { get; set; } = null!;
}
