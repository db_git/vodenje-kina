//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Domain.Entities.Identity;

namespace Domain.Entities;

public sealed class UserComment : IEntity
{
	public Instant CreatedAt { get; set; }
	public Instant? ModifiedAt { get; set; }

	/// <remarks>
	///     Composite and foreign key.
	/// </remarks>
	public string MovieId { get; set; } = null!;

	/// <summary>
	///     Reference navigation property to associated movie.
	/// </summary>
	public Movie Movie { get; set; } = null!;

	/// <remarks>
	///     Composite and foreign key.
	/// </remarks>
	public Guid UserId { get; set; }

	/// <summary>
	///     Reference navigation property to associated user.
	/// </summary>
	public User User { get; set; } = null!;

	/// <remarks>
	///     Maximum length is 4096 characters.
	/// </remarks>
	public string Comment { get; set; } = null!;
}
