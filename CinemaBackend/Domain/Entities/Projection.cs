//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Domain.Entities;

public sealed class Projection : IEntity
{
	public Guid Id { get; set; }
	public Instant CreatedAt { get; set; }
	public Instant? ModifiedAt { get; set; }

	public string MovieId { get; set; } = null!;

	/// <summary>
	///     Reference navigation property to associated movie.
	/// </summary>
	public Movie Movie { get; set; } = null!;

	public Guid HallId { get; set; }

	/// <summary>
	///     Reference navigation property to associated hall.
	/// </summary>
	public Hall Hall { get; set; } = null!;

	public LocalDateTime Time { get; set; }

	/// <summary>
	///     Collection navigation property to associated reservations.
	/// </summary>
	public ICollection<Reservation> Reservations { get; set; } = null!;
}
