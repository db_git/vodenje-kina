//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Domain.Entities;

public sealed class Movie : IEntity
{
	/// <summary>
	///     Entity primary key. It is visible to the user and should be derived from name.
	/// </summary>
	public string Id { get; set; } = null!;

	public Instant CreatedAt { get; set; }
	public Instant? ModifiedAt { get; set; }

	/// <remarks>
	///     Maximum length is 256 characters.
	/// </remarks>
	public string Title { get; set; } = null!;

	public uint Year { get; set; }
	public uint Duration { get; set; }

	/// <remarks>
	///     Maximum length is 256 characters.
	/// </remarks>
	public string Tagline { get; set; } = null!;

	/// <remarks>
	///     Maximum length is 2048 characters.
	/// </remarks>
	public string Summary { get; set; } = null!;

	public bool InCinemas { get; set; }
	public bool IsFeatured { get; set; }
	public bool IsRecommended { get; set; }

	/// <remarks>
	///     Maximum length is 2048 characters.
	/// </remarks>
	public string PosterUrl { get; set; } = null!;

	public string PosterId { get; set; } = null!;

	/// <remarks>
	///     Maximum length is 2048 characters.
	/// </remarks>
	public string BackdropUrl { get; set; } = null!;

	public string BackdropId { get; set; } = null!;

	/// <remarks>
	///     Maximum length is 256 characters.
	/// </remarks>
	public string TrailerUrl { get; set; } = null!;

	public Guid FilmRatingId { get; set; }

	/// <summary>
	///     Reference navigation property to associated film rating.
	/// </summary>
	public FilmRating FilmRating { get; set; } = null!;

	/// <summary>
	///     Collection navigation property to associated genres.
	/// </summary>
	public ICollection<MovieGenre> MovieGenres { get; set; } = null!;

	/// <summary>
	///     Collection navigation property to associated people.
	/// </summary>
	public ICollection<MoviePerson> MoviePeople { get; set; } = null!;

	/// <summary>
	///     Collection navigation property to associated ratings.
	/// </summary>
	public ICollection<UserRating> Ratings { get; set; } = null!;

	/// <summary>
	///     Collection navigation property to associated projections.
	/// </summary>
	public ICollection<Projection> Projections { get; set; } = null!;
}
