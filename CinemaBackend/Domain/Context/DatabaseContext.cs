//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Domain.Context.EntityMaps;
using Domain.Context.EntityMaps.Identity;
using Domain.Entities;
using Domain.Entities.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Domain.Context;

/// <summary>
///     A context instance that represents a session with the database and
///     can be used to query instances of entities.
/// </summary>
/// <remarks>
///     <c>DatabaseContext</c> class and entity map code was based on official documentation.
///     <see href="https://docs.microsoft.com/ef/core/modeling/relationships">
///         Relationships
///     </see>
///     <see href="https://docs.microsoft.com/ef/core/modeling/keys">
///         Keys
///     </see>
///     <seealso cref="IdentityDbContext"/>
/// </remarks>
public class DatabaseContext : IdentityDbContext<
	User, Role, Guid,
	UserClaim, UserRole, UserLogin,
	RoleClaim, UserToken>
{
	public DatabaseContext(DbContextOptions options) : base(options) {}

	protected override void OnModelCreating(ModelBuilder builder)
	{
		base.OnModelCreating(builder);
		builder.ApplyConfiguration(new CinemaMap())
			.ApplyConfiguration(new FilmRatingMap())
			.ApplyConfiguration(new FoodMap())
			.ApplyConfiguration(new FoodOrderMap())
			.ApplyConfiguration(new GenreMap())
			.ApplyConfiguration(new HallMap())
			.ApplyConfiguration(new HallSeatMap())
			.ApplyConfiguration(new MovieMap())
			.ApplyConfiguration(new MovieGenreMap())
			.ApplyConfiguration(new MoviePersonMap())
			.ApplyConfiguration(new PersonMap())
			.ApplyConfiguration(new ProjectionMap())
			.ApplyConfiguration(new ReservationMap())
			.ApplyConfiguration(new ReservationSeatMap())
			.ApplyConfiguration(new RoleMap())
			.ApplyConfiguration(new SeatMap())
			.ApplyConfiguration(new SouvenirMap())
			.ApplyConfiguration(new SouvenirOrderMap())
			.ApplyConfiguration(new UserMap())
			.ApplyConfiguration(new UserCommentMap())
			.ApplyConfiguration(new UserRatingMap());
	}

	public DbSet<Cinema> Cinemas { get; set; } = null!;
	public DbSet<FilmRating> FilmRatings { get; set; } = null!;
	public DbSet<Food> Foods { get; set; } = null!;
	public DbSet<FoodOrder> FoodOrders { get; set; } = null!;
	public DbSet<Genre> Genres { get; set; } = null!;
	public DbSet<Hall> Halls { get; set; } = null!;
	public DbSet<HallSeat> HallSeats { get; set; } = null!;
	public DbSet<Movie> Movies { get; set; } = null!;
	public DbSet<MovieGenre> MovieGenres { get; set; } = null!;
	public DbSet<MoviePerson> MoviePeople { get; set; } = null!;
	public DbSet<Person> People { get; set; } = null!;
	public DbSet<Projection> Projections { get; set; } = null!;
	public DbSet<Reservation> Reservations { get; set; } = null!;
	public DbSet<ReservationSeat> ReservationSeats { get; set; } = null!;
	public DbSet<Seat> Seats { get; set; } = null!;
	public DbSet<Souvenir> Souvenirs { get; set; } = null!;
	public DbSet<SouvenirOrder> SouvenirOrders { get; set; } = null!;
	public DbSet<UserComment> UserComments { get; set; } = null!;
	public DbSet<UserRating> UserRatings { get; set; } = null!;
}
