//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Domain.Entities;

namespace Domain.Context.EntityMaps;

internal sealed class MovieGenreMap : IEntityTypeConfiguration<MovieGenre>
{
	public void Configure(EntityTypeBuilder<MovieGenre> builder)
	{
		builder.HasKey(movieGenre => new { movieGenre.MovieId, movieGenre.GenreId });

		builder
			.HasOne(movieGenre => movieGenre.Movie)
			.WithMany(movie => movie.MovieGenres)
			.HasForeignKey(movieGenre => movieGenre.MovieId);

		builder
			.HasOne(movieGenre => movieGenre.Genre)
			.WithMany(genre => genre.MovieGenres)
			.HasForeignKey(movieGenre => movieGenre.GenreId);
	}
}
