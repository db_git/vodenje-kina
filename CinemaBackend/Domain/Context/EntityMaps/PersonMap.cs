//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Domain.Entities;

namespace Domain.Context.EntityMaps;

internal sealed class PersonMap : IEntityTypeConfiguration<Person>
{
	public void Configure(EntityTypeBuilder<Person> builder)
	{
		builder.Property(person => person.Id).ValueGeneratedNever();
		builder.Property(person => person.Name).HasMaxLength(256);
		builder.Property(person => person.ImageUrl).HasMaxLength(2048);
		builder.Property(person => person.Biography).HasMaxLength(4096);
		builder.Property(person => person.ImageId).HasDefaultValue("");
	}
}
