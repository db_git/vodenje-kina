//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Domain.Entities.Identity;
using Domain.Enums;

namespace Domain.Context.EntityMaps.Identity;

internal sealed class RoleMap : IEntityTypeConfiguration<Role>
{
	public void Configure(EntityTypeBuilder<Role> builder)
	{
		builder
			.HasMany(e => e.UserRoles)
			.WithOne(e => e.Role)
			.HasForeignKey(ur => ur.RoleId)
			.IsRequired();

		builder
			.HasMany(e => e.RoleClaims)
			.WithOne(e => e.Role)
			.HasForeignKey(rc => rc.RoleId)
			.IsRequired();

		builder.HasData(
			new Role
			{
				Id = Guid.Parse("7ff8dce8-c4c8-4899-9cdc-21d0cd4d319c"),
				Name = Roles.User.ToString(),
				NormalizedName = Roles.User.ToString().Normalize().ToUpperInvariant()
			},
			new Role
			{
				Id = Guid.Parse("ba6ac7be-594f-440a-87be-49334f4789db"),
				Name = Roles.Admin.ToString(),
				NormalizedName = Roles.Admin.ToString().Normalize().ToUpperInvariant()
			}
		);
	}
}
