//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Domain.Entities;

namespace Domain.Context.EntityMaps;

internal sealed class MovieMap : IEntityTypeConfiguration<Movie>
{
	public void Configure(EntityTypeBuilder<Movie> builder)
	{
		builder.Property(movie => movie.Id).ValueGeneratedNever();
		builder.Property(movie => movie.Title).HasMaxLength(256);
		builder.Property(movie => movie.Tagline).HasMaxLength(256);
		builder.Property(movie => movie.Summary).HasMaxLength(2048);
		builder.Property(movie => movie.PosterUrl).HasMaxLength(2048);
		builder.Property(movie => movie.PosterId).HasDefaultValue("");
		builder.Property(movie => movie.BackdropUrl).HasMaxLength(2048);
		builder.Property(movie => movie.BackdropId).HasDefaultValue("");
		builder.Property(movie => movie.TrailerUrl).HasMaxLength(256);
		builder.Property(movie => movie.IsFeatured).HasDefaultValue(false);
		builder.Property(movie => movie.IsRecommended).HasDefaultValue(false);

		builder.HasOne(movie => movie.FilmRating)
			.WithMany(filmRating => filmRating.Movies)
			.HasForeignKey(movie => movie.FilmRatingId);

		builder.HasMany(movie => movie.Ratings)
			.WithOne(rating => rating.Movie)
			.HasForeignKey(rating => rating.MovieId);
	}
}
