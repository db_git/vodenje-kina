//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Domain.Entities;

namespace Domain.Context.EntityMaps;

internal sealed class ReservationSeatMap : IEntityTypeConfiguration<ReservationSeat>
{
	public void Configure(EntityTypeBuilder<ReservationSeat> builder)
	{
		builder.HasKey(reservationSeat => new { reservationSeat.ReservationId, reservationSeat.SeatId });

		builder
			.HasOne(reservationSeat => reservationSeat.Reservation)
			.WithMany(reservation => reservation.ReservationSeats)
			.HasForeignKey(reservationSeat => reservationSeat.ReservationId);

		builder
			.HasOne(reservationSeat => reservationSeat.Seat)
			.WithMany(seat => seat.ReservationSeats)
			.HasForeignKey(reservationSeat => reservationSeat.SeatId);
	}
}
