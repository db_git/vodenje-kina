//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Repository;

/// <inheritdoc />
public sealed class UnitOfWork : IUnitOfWork
{
	private readonly DatabaseContext _context;
	private readonly IMapper _mapper;

	private ICinemaRepository? _cinemaRepository;
	private IFilmRatingRepository? _filmRatingRepository;
	private IFoodRepository? _foodRepository;
	private IGenreRepository? _genreRepository;
	private IHallRepository? _hallRepository;
	private IMovieRepository? _movieRepository;
	private IPersonRepository? _personRepository;
	private IProjectionRepository? _projectionRepository;
	private IReservationRepository? _reservationRepository;
	private ISeatRepository? _seatRepository;
	private ISouvenirRepository? _souvenirRepository;
	private IUserRepository? _userRepository;
	private IUserCommentRepository? _userCommentRepository;
	private IUserRatingRepository? _userRatingRepository;

	public ICinemaRepository CinemaRepository =>
		_cinemaRepository ??= new CinemaRepository(_context, _mapper);

	public IFilmRatingRepository FilmRatingRepository =>
		_filmRatingRepository ??= new FilmRatingRepository(_context, _mapper);

	public IFoodRepository FoodRepository =>
		_foodRepository ??= new FoodRepository(_context, _mapper);

	public IGenreRepository GenreRepository =>
		_genreRepository ??= new GenreRepository(_context, _mapper);

	public IHallRepository HallRepository =>
		_hallRepository ??= new HallRepository(_context, _mapper);

	public IMovieRepository MovieRepository =>
		_movieRepository ??= new MovieRepository(_context, _mapper);

	public IPersonRepository PersonRepository =>
		_personRepository ??= new PersonRepository(_context, _mapper);

	public IProjectionRepository ProjectionRepository =>
		_projectionRepository ??= new ProjectionRepository(_context, _mapper);

	public IReservationRepository ReservationRepository =>
		_reservationRepository ??= new ReservationRepository(_context, _mapper);

	public ISeatRepository SeatRepository =>
		_seatRepository ??= new SeatRepository(_context, _mapper);

	public ISouvenirRepository SouvenirRepository =>
		_souvenirRepository ??= new SouvenirRepository(_context, _mapper);

	public IUserRepository UserRepository =>
		_userRepository ??= new UserRepository(_context, _mapper);

	public IUserCommentRepository UserCommentRepository =>
		_userCommentRepository ??= new UserCommentRepository(_context, _mapper);

	public IUserRatingRepository UserRatingRepository =>
		_userRatingRepository ??= new UserRatingRepository(_context, _mapper);

	public UnitOfWork(DatabaseContext context, IMapper mapper)
	{
		_context = context;
		_mapper = mapper;
	}

	public void Dispose()
	{
		_context.Dispose();
		GC.SuppressFinalize(this);
	}

	public async Task SaveAsync()
	{
		await _context.SaveChangesAsync();
	}

	~UnitOfWork()
	{
		Dispose();
	}
}
