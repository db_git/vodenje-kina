//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Repository;

/// <inheritdoc cref="IUserRepository" />
public class UserRepository : RepositoryBase<User>, IUserRepository
{
	public UserRepository(DatabaseContext context, IMapper mapper) : base(context, mapper) {}

#pragma warning disable 0809
	[Obsolete("Use UserManager.CreateAsync() method.", true)]
	public override void Add(User user) { throw new NotImplementedException(); }

	[Obsolete("Not available.", true)]
	public override void AddRange(IEnumerable<User> users) { throw new NotImplementedException(); }

	[Obsolete("Use UserManager.UpdateAsync() method.", true)]
	public override void Update(User user) { throw new NotImplementedException(); }
#pragma warning restore 0809
}
