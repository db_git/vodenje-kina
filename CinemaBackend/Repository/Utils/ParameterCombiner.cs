//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Repository.Utils;

/// <summary>
///     Combine multiple lambda expressions.
/// </summary>
/// <remarks>
///     <c>ParameterCombiner</c> class code was based on official documentation.
///     <see href="https://docs.microsoft.com/archive/blogs/meek/linq-to-entities-combining-predicates">
///         LINQ to Entities: Combining Predicates
///     </see>
/// </remarks>
public static class ParameterCombiner
{
	private static Expression<T> Combine<T>(
		this Expression<T>? left,
		Expression<T> right,
		Func<Expression, Expression, Expression> merge
	)
	{
		if (left is null) return right;

		var parameterMap = left
			.Parameters
			.Select((first, iterator) => new
				{
					first, second = right.Parameters[iterator]
				})
			.ToDictionary(
				parameter => parameter.second,
				parameter => parameter.first
			);

		var secondBody = ParameterReplacer.ReplaceParameters(parameterMap, right.Body);
		return Expression.Lambda<T>(merge(left.Body, secondBody), left.Parameters);
	}

	/// <summary>
	///     Combine predicates with logical AND.
	/// </summary>
	/// <param name="left">
	///     First predicate.
	/// </param>
	/// <param name="right">
	///     Second predicate.
	/// </param>
	/// <typeparam name="T">
	///     The type being operated on.
	/// </typeparam>
	/// <returns>
	///     Expression that has the <paramref name="left"/> and
	///     <paramref name="right"/> expressions combined with logical AND.
	/// </returns>
	public static Expression<Func<T, bool>> And<T>(
		this Expression<Func<T, bool>>? left,
		Expression<Func<T, bool>> right
	)
	{
		return left.Combine(right, Expression.AndAlso);
	}

	/// <summary>
	///     Combine predicates with logical OR.
	/// </summary>
	/// <param name="left">
	///     First predicate.
	/// </param>
	/// <param name="right">
	///     Second predicate.
	/// </param>
	/// <typeparam name="T">
	///     The type being operated on.
	/// </typeparam>
	/// <returns>
	///     Expression that has the <paramref name="left"/> and
	///     <paramref name="right"/> expressions combined with logical OR.
	/// </returns>
	public static Expression<Func<T, bool>> Or<T>(
		this Expression<Func<T, bool>>? left,
		Expression<Func<T, bool>> right
	)
	{
		return left.Combine(right, Expression.OrElse);
	}
}
