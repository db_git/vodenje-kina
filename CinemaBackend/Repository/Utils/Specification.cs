//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Microsoft.EntityFrameworkCore.Query;

namespace Repository.Utils;

/// <inheritdoc />
public sealed class Specification<TEntity> : ISpecification<TEntity> where TEntity : class
{
	public Expression<Func<TEntity, bool>>? Filter { get; private set; }
	public Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? Include { get; private set; }
	public Expression<Func<TEntity, object>>? OrderBy { get; private set; }
	public Expression<Func<TEntity, object>>? ThenOrderBy { get; private set; }
	public Expression<Func<TEntity, object>>? OrderByDescending { get; private set; }
	public Expression<Func<TEntity, object>>? ThenOrderByDescending { get; private set; }

	public int Skip { get; private set; }
	public int Take { get; private set; }
	public bool PaginationEnabled { get; private set; }
	public bool Tracking { get; private set; }
	public bool Distinct { get; private set; }

	public void AddFilter(Expression<Func<TEntity, bool>> filterExpression)
	{
		Filter = filterExpression;
	}

	public void AddInclude(Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? includeExpression)
	{
		Include = includeExpression;
	}

	public void AddOrderBy(Expression<Func<TEntity, object>> orderByExpression)
	{
		OrderBy = orderByExpression;
	}

	public void AddThenOrderBy(Expression<Func<TEntity, object>> thenOrderByExpression)
	{
		ThenOrderBy = thenOrderByExpression;
	}

	public void AddOrderByDescending(Expression<Func<TEntity, object>> orderByDescendingExpression)
	{
		OrderByDescending = orderByDescendingExpression;
	}

	public void AddThenOrderByDescending(Expression<Func<TEntity, object>> thenOrderByDescendingExpression)
	{
		ThenOrderByDescending = thenOrderByDescendingExpression;
	}

	public void AddPagination(int pageNumber, int pageSize)
	{
		Skip = (pageNumber - 1) * pageSize;
		Take = pageSize;
		PaginationEnabled = true;
	}

	public void WithTracking(bool withTracking)
	{
		Tracking = withTracking;
	}

	public void WithDistinct(bool withDistinct)
	{
		Distinct = withDistinct;
	}
}
