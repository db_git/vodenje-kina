//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Repository.Utils;

/// <remarks>
///     <c>ParameterReplacer</c> class code was based on official documentation.
///     <see href="https://docs.microsoft.com/archive/blogs/meek/linq-to-entities-combining-predicates">
///         LINQ to Entities: Combining Predicates
///     </see>
/// </remarks>
internal sealed class ParameterReplacer : ExpressionVisitor
{
	private readonly Dictionary<ParameterExpression, ParameterExpression> _replacements;

	private ParameterReplacer(Dictionary<ParameterExpression, ParameterExpression> replacements)
	{
		_replacements = replacements;
	}

	public static Expression ReplaceParameters(
		Dictionary<ParameterExpression, ParameterExpression> replacements,
		Expression expression
	)
	{
		return new ParameterReplacer(replacements).Visit(expression);
	}

	protected override Expression VisitParameter(ParameterExpression node)
	{
		if (_replacements.TryGetValue(node, out var replacement))
			node = replacement;
		return base.VisitParameter(node);
	}
}
