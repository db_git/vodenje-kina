//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Repository.Tests")]
#endif
namespace Repository.Utils;

/// <summary>
///     Allow evaluating specifications.
/// </summary>
/// <typeparam name="TEntity">The type of entity being operated on.</typeparam>
internal static class SpecificationEvaluator<TEntity> where TEntity : class
{
	/// <summary>
	///     Perform query evaluation.
	/// </summary>
	/// <param name="entityQuery">
	///     <see cref="IQueryable"/> on which evaluation can be performed.
	/// </param>
	/// <param name="specification">
	///     Specification on which the evaluation is based.
	/// </param>
	/// <returns>
	///     Evaluated <see cref="IQueryable"/>.
	/// </returns>
	public static IQueryable<TEntity> GetQuery(
		IQueryable<TEntity> entityQuery,
		ISpecification<TEntity> specification
	)
	{
		var query = entityQuery;

		if (specification.Filter is not null) query = query.Where(specification.Filter);
		if (specification.Include is not null) query = specification.Include(query);

		if (specification.OrderBy is not null)
		{
			query = specification.ThenOrderBy is not null
				? query.OrderBy(specification.OrderBy).ThenBy(specification.ThenOrderBy)
				: query.OrderBy(specification.OrderBy);
		}
		else if (specification.OrderByDescending is not null)
		{
			query = specification.ThenOrderByDescending is not null
				? query.OrderByDescending(specification.OrderByDescending)
					.ThenByDescending(specification.ThenOrderByDescending)
				: query.OrderByDescending(specification.OrderByDescending);
		}

		if (specification.PaginationEnabled)
			query = query.Skip(specification.Skip).Take(specification.Take);

		query = specification.Tracking
			? query.AsTracking()
			: query.AsNoTrackingWithIdentityResolution();

		if (specification.Distinct)
			query = query.Distinct();

		return query;
	}
}
