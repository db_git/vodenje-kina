//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using AutoMapper.QueryableExtensions;

namespace Repository;

/// <inheritdoc />
public abstract class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class, IEntity
{
	private readonly DatabaseContext _context;
	protected readonly DbSet<TEntity> DbSet;
	private readonly IMapper _mapper;

	protected RepositoryBase(DatabaseContext context, IMapper mapper)
	{
		_context = context;
		DbSet = _context.Set<TEntity>();
		_mapper = mapper;
	}

	public virtual Task<int> CountAsync()
	{
		return DbSet.CountAsync();
	}

	public virtual Task<int> CountAsync(Expression<Func<TEntity, bool>>? expression)
	{
		IQueryable<TEntity> query = DbSet;
		if (expression != null)
			query = query.Where(expression);

		return query.CountAsync();
	}

	public virtual Task<int> CountAsync(ISpecification<TEntity> specification)
	{
		IQueryable<TEntity> query = DbSet;
		query = SpecificationEvaluator<TEntity>.GetQuery(query, specification);

		return query.CountAsync();
	}

	public virtual async Task<ICollection<TEntity>> GetAllAsync(ISpecification<TEntity> specification)
	{
		IQueryable<TEntity> query = DbSet;
		query = SpecificationEvaluator<TEntity>.GetQuery(query, specification);

		return await query.AsNoTrackingWithIdentityResolution().ToListAsync();
	}

	public virtual async Task<ICollection<TDto>> GetAllAsync<TDto>(ISpecification<TEntity> specification)
		where TDto : class, new()
	{
		IQueryable<TEntity> query = DbSet;
		query = SpecificationEvaluator<TEntity>.GetQuery(query, specification);

		return await query
			.AsNoTrackingWithIdentityResolution()
			.ProjectTo<TDto>(_mapper.ConfigurationProvider)
			.ToListAsync();
	}

	public virtual Task<TEntity?> GetAsync(ISpecification<TEntity> specification)
	{
		IQueryable<TEntity> query = DbSet;
		query = SpecificationEvaluator<TEntity>.GetQuery(query, specification);

		return query.FirstOrDefaultAsync();
	}

	public Task<TDto?> GetAsync<TDto>(ISpecification<TEntity> specification) where TDto : class, new()
	{
		IQueryable<TEntity> query = DbSet;
		query = SpecificationEvaluator<TEntity>.GetQuery(query, specification);

		return query
			.AsNoTrackingWithIdentityResolution()
			.ProjectTo<TDto>(_mapper.ConfigurationProvider)
			.FirstOrDefaultAsync();
	}

	public virtual void Add(TEntity entity)
	{
		DbSet.Add(entity);
	}

	public virtual void AddRange(IEnumerable<TEntity> entities)
	{
		DbSet.AddRange(entities);
	}

	public virtual void Update(TEntity entity)
	{
		DbSet.Attach(entity);
		_context.Entry(entity).State = EntityState.Modified;
	}

	public virtual void UpdateRange(IEnumerable<TEntity> entities)
	{
		var entitiesArray = entities.ToArray();
		foreach (var entity in entitiesArray)
		{
			DbSet.Attach(entity);
			_context.Entry(entity).State = EntityState.Modified;
		}
	}

	public virtual void Delete(TEntity entity)
	{
		DbSet.Attach(entity);
		_context.Entry(entity).State = EntityState.Deleted;
		DbSet.Remove(entity);
	}

	public virtual void DeleteRange(IEnumerable<TEntity> entities)
	{
		var entitiesArray = entities.ToArray();
		foreach (var entity in entitiesArray)
		{
			DbSet.Attach(entity);
			_context.Entry(entity).State = EntityState.Deleted;
		}

		DbSet.RemoveRange(entitiesArray);
	}
}
