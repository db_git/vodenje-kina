//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Repository;

/// <inheritdoc cref="IUserRatingRepository" />
public class UserRatingRepository : RepositoryBase<UserRating>, IUserRatingRepository
{
	public UserRatingRepository(DatabaseContext context, IMapper mapper) : base(context, mapper) {}

	public async Task<decimal> GetRatingSumAsync(string movieId)
	{
		return await DbSet
			.Where(r => r.MovieId == movieId)
			.Select(r => r.Rating)
			.SumAsync(r => r);
	}
}
