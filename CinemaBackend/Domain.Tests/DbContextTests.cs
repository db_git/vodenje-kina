//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using Tests.Common.Contexts;

namespace Domain.Tests;

[TestFixture]
public sealed class DbContextTests
{
	private readonly IntegrationSqliteContext _dbContext =
		new (new DbContextOptionsBuilder<IntegrationSqliteContext>().Options);

	[SetUp]
	public async Task SetUp()
	{
		await _dbContext.Database.OpenConnectionAsync();
		await _dbContext.Database.EnsureDeletedAsync();
		await _dbContext.Database.EnsureCreatedAsync();
	}

	[TearDown]
	public async Task TearDown()
	{
		await _dbContext.Database.EnsureDeletedAsync();
		await _dbContext.Database.CloseConnectionAsync();
	}

	[Test]
	public async Task Assert_MigrationsApply()
	{
		await _dbContext.Database.MigrateAsync();
		Assert.Pass();
	}

	[Test]
	public void Given_Context_When_InitialData_Then_ReturnCinema()
	{
		Assert.That(_dbContext.Cinemas.FirstOrDefault(), Is.Not.Null);
	}

	[Test]
	public void Given_Context_When_InitialData_Then_ReturnFilmRating()
	{
		Assert.That(_dbContext.FilmRatings.FirstOrDefault(), Is.Not.Null);
	}

	[Test]
	public void Given_Context_When_InitialData_Then_ReturnFood()
	{
		Assert.That(_dbContext.Foods.FirstOrDefault(), Is.Not.Null);
	}

	[Test]
	public void Given_Context_When_InitialData_Then_ReturnGenre()
	{
		Assert.That(_dbContext.Genres.FirstOrDefault(), Is.Not.Null);
	}

	[Test]
	public void Given_Context_When_InitialData_Then_ReturnHall()
	{
		Assert.That(_dbContext.Halls.FirstOrDefault(), Is.Not.Null);
	}

	[Test]
	public void Given_Context_When_InitialData_Then_ReturnHallSeat()
	{
		Assert.That(_dbContext.HallSeats.FirstOrDefault(), Is.Not.Null);
	}

	[Test]
	public void Given_Context_When_InitialData_Then_ReturnMovie()
	{
		Assert.That(_dbContext.Movies.FirstOrDefault(), Is.Not.Null);
	}

	[Test]
	public void Given_Context_When_InitialData_Then_ReturnMovieGenre()
	{
		Assert.That(_dbContext.MovieGenres.FirstOrDefault(), Is.Not.Null);
	}

	[Test]
	public void Given_Context_When_InitialData_Then_ReturnMoviePerson()
	{
		Assert.That(_dbContext.MoviePeople.FirstOrDefault(), Is.Not.Null);
	}

	[Test]
	public void Given_Context_When_InitialData_Then_ReturnPerson()
	{
		Assert.That(_dbContext.People.FirstOrDefault(), Is.Not.Null);
	}

	[Test]
	public void Given_Context_When_InitialData_Then_ReturnProjection()
	{
		Assert.That(_dbContext.Projections.FirstOrDefault(), Is.Not.Null);
	}

	[Test]
	public void Given_Context_When_InitialData_Then_ReturnSeat()
	{
		Assert.That(_dbContext.Seats.FirstOrDefault(), Is.Not.Null);
	}

	[Test]
	public void Given_Context_When_InitialData_Then_ReturnSouvenir()
	{
		Assert.That(_dbContext.Souvenirs.FirstOrDefault(), Is.Not.Null);
	}
}
#endif
