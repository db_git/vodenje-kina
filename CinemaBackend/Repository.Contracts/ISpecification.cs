//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.Query;

namespace Repository.Contracts;

/// <summary>
///     Specification allows for encapsulating a part of domain knowledge
///     into a single unit and reusing it in different parts of the code base.
/// </summary>
/// <typeparam name="TEntity">
///     The type of entity being operated on.
/// </typeparam>
public interface ISpecification<TEntity> where TEntity : class
{
	Expression<Func<TEntity, bool>>? Filter { get; }
	Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? Include { get; }
	Expression<Func<TEntity, object>>? OrderBy { get; }
	Expression<Func<TEntity, object>>? ThenOrderBy { get; }
	Expression<Func<TEntity, object>>? OrderByDescending { get; }
	Expression<Func<TEntity, object>>? ThenOrderByDescending { get; }

	int Skip { get; }
	int Take { get; }
	bool PaginationEnabled { get; }
	bool Tracking { get; }
	bool Distinct { get; }

	void AddFilter(Expression<Func<TEntity, bool>> filterExpression);
	void AddInclude(Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? includeExpression);
	void AddOrderBy(Expression<Func<TEntity, object>> orderByExpression);
	void AddThenOrderBy(Expression<Func<TEntity, object>> thenOrderByExpression);
	void AddOrderByDescending(Expression<Func<TEntity, object>> orderByDescendingExpression);
	void AddThenOrderByDescending(Expression<Func<TEntity, object>> thenOrderByDescendingExpression);
	void AddPagination(int pageNumber, int pageSize);
	void WithTracking(bool withTracking);
	void WithDistinct(bool withDistinct);
}
