//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using API.DTO.Response.Souvenir;

namespace Service.Tests;

[TestFixture]
public sealed class SouvenirServiceTests : ServiceTestBase<
	Souvenir, SouvenirQueryService, SouvenirCommandService,
	SouvenirInMemoryContext, SouvenirFilter>
{
	[TestCaseSource(typeof(SouvenirUtils), nameof(SouvenirUtils.RandomSouvenirNames))]
	public void Given_DuplicateName_When_AddAsync_Then_ThrowDuplicateEntityException(string name)
	{
		var repositoryMock = new Mock<ISouvenirRepository>();
		var unitOfWorkMock = new Mock<IUnitOfWork>();

		repositoryMock
			.Setup(s => s.CountAsync(
				It.IsAny<Expression<Func<Souvenir, bool>>>())
			)
			.ReturnsAsync(1);
		unitOfWorkMock.Setup(u => u.SouvenirRepository).Returns(repositoryMock.Object);
		var service = new SouvenirCommandService(Clock, unitOfWorkMock.Object);

		Assert.ThrowsAsync<DuplicateEntityException>(async () =>
		{
			await service.AddAsync(new Souvenir { Name = name });
		});
	}

	[TestCaseSource(typeof(SouvenirUtils), nameof(SouvenirUtils.RandomSouvenirNames))]
	public async Task Given_Souvenir_When_AddAsync_Then_SaveSouvenir(string name)
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var souvenir = new Souvenir
		{
			Name = name,
			Price = Faker.Finance.Amount(),
			AvailableQuantity = Faker.Random.ULong(),
			ImageUrl = Faker.Internet.Url(),
			ImageId = Faker.Random.String(10)
		};

		await CommandService.AddAsync(souvenir);

		var newCount = await QueryService.CountAsync(Filter);

		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(initialCount + 1, Is.EqualTo(newCount));
	}

	[Test]
	public async Task Given_Souvenirs_When_AddRangeAsync_Then_SaveSouvenirs()
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var souvenirs = new List<Souvenir>(SouvenirUtils.RandomSouvenirNames.Length);
		souvenirs.AddRange(SouvenirUtils.RandomSouvenirNames
			.Select(name => new Souvenir
			{
				Name = name,
				Price = Faker.Finance.Amount(),
				AvailableQuantity = Faker.Random.ULong(),
				ImageUrl = Faker.Internet.Url(),
				ImageId = Faker.Random.String(10)
			})
		);

		await CommandService.AddRangeAsync(souvenirs);

		var newCount = await QueryService.CountAsync(Filter);
		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(initialCount + SouvenirUtils.RandomSouvenirNames.Length, Is.EqualTo(newCount));
	}

	[TestCaseSource(typeof(SouvenirUtils), nameof(SouvenirUtils.SpecificSouvenirNames))]
	public async Task Given_SearchFilter_When_GetAllAsync_Then_ReturnSouvenir(string name)
	{
		var souvenirs = await QueryService.GetAllAsync(new SouvenirFilter
		{
			Page = 1,
			Size = 10,
			Search = name
		});

		Assert.That(souvenirs, Has.Count.EqualTo(1));
		Assert.That(souvenirs.First().Name, Is.EqualTo(name));
	}

	[TestCase(1)]
	[TestCase(2)]
	[TestCase(3)]
	[TestCase(4)]
	public async Task Given_PaginationFilter_When_GetAllAsync_Then_ReturnSouvenirs(int size)
	{
		var souvenirs = await QueryService.GetAllAsync(new SouvenirFilter
		{
			Page = 1,
			Size = size
		});

		Assert.That(souvenirs, Has.Count.EqualTo(size));
	}

	[Test]
	public void Given_OrderByFilter_When_GetAllAsync_Then_ReturnOrderedSouvenirs()
	{
		AssertOrdered(
			QueryServiceInstance().GetAllAsync,
			QueryServiceInstance().GetAllAsync,
			SouvenirOrderBy.NameAscending,
			SouvenirOrderBy.NameDescending,
			s => s.Name
		);
		AssertOrdered(
			QueryServiceInstance().GetAllAsync,
			QueryServiceInstance().GetAllAsync,
			SouvenirOrderBy.PriceAscending,
			SouvenirOrderBy.PriceDescending,
			s => s.Price
		);
		AssertOrdered(
			QueryServiceInstance().GetAllAsync,
			QueryServiceInstance().GetAllAsync,
			SouvenirOrderBy.AvailableQuantityAscending,
			SouvenirOrderBy.AvailableQuantityDescending,
			s => s.AvailableQuantity
		);
	}

	[Test]
	public void Given_InvalidId_When_GetAsync_Then_ThrowEntityNotFoundException()
	{
		Assert.ThrowsAsync<EntityNotFoundException>(() =>
		{
			return QueryService.GetAsync(s => s.Id == Guid.Empty);
		});
		Assert.ThrowsAsync<EntityNotFoundException>(() =>
		{
			return QueryService.GetAsync<SouvenirDto>(s => s.Id == Guid.Empty);
		});
	}

	[TestCaseSource(typeof(SouvenirFakeData), nameof(SouvenirFakeData.SouvenirIds))]
	public async Task Given_SouvenirId_When_GetAsyncProjectTo_Then_ReturnProjected(Guid id)
	{
		var souvenir = await QueryService.GetAsync(f => f.Id == id);
		var dto = await QueryService.GetAsync<SouvenirDto>(f => f.Id == id);

		Assert.That(dto.Id, Is.EqualTo(souvenir.Id));
		Assert.That(dto.Name, Is.EqualTo(souvenir.Name));
		Assert.That(dto.Price, Is.EqualTo(souvenir.Price));
		Assert.That(dto.AvailableQuantity, Is.EqualTo(souvenir.AvailableQuantity));
		Assert.That(dto.ImageUrl, Is.EqualTo(souvenir.ImageUrl));
	}

	[TestCaseSource(typeof(SouvenirFakeData), nameof(SouvenirFakeData.SouvenirIds))]
	public async Task Given_PaginationFilter_When_GetAllAsyncProjectTo_Then_ReturnProjectedList(Guid id)
	{
		var filter = new SouvenirFilter { Page = 1, Size = 1 };

		QueryService = QueryServiceInstance();
		var souvenirs = await QueryService.GetAllAsync(filter);
		QueryService = QueryServiceInstance();
		var dtos = await QueryService.GetAllAsync<SouvenirDto>(filter);

		Assert.That(dtos.First().Id, Is.EqualTo(souvenirs.First().Id));
		Assert.That(dtos.First().Name, Is.EqualTo(souvenirs.First().Name));
		Assert.That(dtos.First().Price, Is.EqualTo(souvenirs.First().Price));
		Assert.That(dtos.First().AvailableQuantity, Is.EqualTo(souvenirs.First().AvailableQuantity));
		Assert.That(dtos.First().ImageUrl, Is.EqualTo(souvenirs.First().ImageUrl));
	}

	[TestCaseSource(typeof(SouvenirFakeData), nameof(SouvenirFakeData.SouvenirIds))]
	public async Task Given_SouvenirId_When_DeleteAsync_Then_DeleteSouvenir(Guid id)
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var souvenir = await QueryService.GetAsync(s => s.Id == id);
		Assert.That(souvenir, Is.Not.Null);
		await CommandService.DeleteAsync(souvenir);

		var newCount = await QueryService.CountAsync(Filter);
		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(initialCount - 1, Is.EqualTo(newCount));
	}

	[Test]
	public async Task Given_SouvenirIds_When_DeleteRangeAsync_Then_DeleteSouvenirs()
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var souvenirs = new List<Souvenir>(SouvenirFakeData.SouvenirIds.Count);
		foreach (Guid id in SouvenirFakeData.SouvenirIds)
		{
			var souvenir = await QueryService.GetAsync(s => s.Id == id);
			Assert.That(souvenir, Is.Not.Null);
			souvenirs.Add(souvenir);
		}

		await CommandService.DeleteRangeAsync(souvenirs);

		var newCount = await QueryService.CountAsync(Filter);
		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(initialCount - SouvenirFakeData.SouvenirIds.Count, Is.EqualTo(newCount));
	}
}
#endif
