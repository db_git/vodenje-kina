//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using API.DTO.Response.Hall;

namespace Service.Tests;

[TestFixture]
public sealed class HallServiceTests : ServiceTestBase<
	Hall, HallQueryService, HallCommandService,
	HallInMemoryContext, QueryFilter>
{
	protected override HallCommandService CommandServiceInstance()
	{
		return new HallCommandService(
			Clock,
			UnitOfWork,
			new CinemaQueryService(UnitOfWork, new Specification<Cinema>())
		);
	}

	[TestCaseSource(typeof(HallUtils), nameof(HallUtils.RandomHallNames))]
	public async Task Given_Hall_When_AddAsync_Then_SaveHall(string name)
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var hall = new Hall
		{
			Name = name,
			CinemaId = CinemaFakeData.CinemaIds.Cast<Guid>().First()
		};

		await CommandService.AddAsync(hall);

		var newCount = await QueryService.CountAsync(Filter);

		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(initialCount + 1, Is.EqualTo(newCount));
	}

	[Test]
	public async Task Given_Halls_When_AddRangeAsync_Then_SaveHalls()
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var halls = new List<Hall>(HallUtils.RandomHallNames.Length);
		halls.AddRange(HallUtils.RandomHallNames
			.Select(name => new Hall
			{
				Name = name,
				CinemaId = CinemaFakeData.CinemaIds.Cast<Guid>().First()
			})
		);

		await CommandService.AddRangeAsync(halls);

		var newCount = await QueryService.CountAsync(Filter);
		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(initialCount + HallUtils.RandomHallNames.Length, Is.EqualTo(newCount));
	}

	[TestCaseSource(typeof(HallUtils), nameof(HallUtils.SpecificHallNames))]
	public async Task Given_SearchFilter_When_GetAllAsync_Then_ReturnHall(string name)
	{
		var halls = await QueryService.GetAllAsync(new QueryFilter
		{
			Page = 1,
			Size = 10,
			Search = name
		});

		Assert.That(halls, Has.Count.EqualTo(1));
		Assert.That(halls.First().Name, Is.EqualTo(name));
	}

	[TestCase(1)]
	[TestCase(2)]
	[TestCase(3)]
	[TestCase(4)]
	public async Task Given_PaginationFilter_When_GetAllAsync_Then_ReturnHalls(int size)
	{
		var halls = await QueryService.GetAllAsync(new QueryFilter { Page = 1, Size = size });
		Assert.That(halls, Has.Count.EqualTo(size));
	}

	[Test]
	public void Given_OrderByFilter_When_GetAllAsync_Then_ReturnOrderedHalls()
	{
		AssertOrdered(
			QueryServiceInstance().GetAllAsync,
			QueryServiceInstance().GetAllAsync,
			HallOrderBy.NameAscending,
			HallOrderBy.NameDescending,
			h => h.Name
		);
	}

	[Test]
	public void Given_InvalidId_When_GetAsync_Then_ThrowEntityNotFoundException()
	{
		Assert.ThrowsAsync<EntityNotFoundException>(() =>
		{
			return QueryService.GetAsync(h => h.Id == Guid.Empty);
		});
		Assert.ThrowsAsync<EntityNotFoundException>(() =>
		{
			return QueryService.GetAsync<HallDto>(h => h.Id == Guid.Empty);
		});
	}

	[TestCaseSource(typeof(HallFakeData), nameof(HallFakeData.HallIds))]
	public async Task Given_HallId_When_GetAsyncProjectTo_Then_ReturnProjected(Guid id)
	{
		var hall = await QueryService.GetAsync(h => h.Id == id);
		var dto = await QueryService.GetAsync<HallDto>(h => h.Id == id);

		Assert.That(dto.Id, Is.EqualTo(hall.Id));
		Assert.That(dto.Name, Is.EqualTo(hall.Name));
		Assert.That(dto.Cinema.Id, Is.EqualTo(hall.CinemaId));
		Assert.That(dto.Cinema.Id, Is.EqualTo(hall.Cinema.Id));
		Assert.That(dto.Cinema.Name, Is.EqualTo(hall.Cinema.Name));
	}

	[TestCaseSource(typeof(HallFakeData), nameof(HallFakeData.HallIds))]
	public async Task Given_PaginationFilter_When_GetAllAsyncProjectTo_Then_ReturnProjectedList(Guid id)
	{
		var filter = new QueryFilter { Page = 1, Size = 1 };

		QueryService = QueryServiceInstance();
		var halls = await QueryService.GetAllAsync(filter);
		QueryService = QueryServiceInstance();
		var dtos = await QueryService.GetAllAsync<HallDto>(filter);

		Assert.That(dtos.First().Id, Is.EqualTo(halls.First().Id));
		Assert.That(dtos.First().Name, Is.EqualTo(halls.First().Name));
		Assert.That(dtos.First().Cinema.Id, Is.EqualTo(halls.First().CinemaId));
		Assert.That(dtos.First().Cinema.Id, Is.EqualTo(halls.First().CinemaId));
		Assert.That(dtos.First().Cinema.Name, Is.EqualTo(halls.First().Cinema.Name));
	}

	[TestCaseSource(typeof(HallFakeData), nameof(HallFakeData.HallIds))]
	public async Task Given_HallId_When_DeleteAsync_Then_DeleteHall(Guid id)
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var hall = await QueryService.GetAsync(f => f.Id == id);
		Assert.That(hall, Is.Not.Null);
		await CommandService.DeleteAsync(hall);

		var newCount = await QueryService.CountAsync(Filter);
		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(initialCount - 1, Is.EqualTo(newCount));
	}

	[Test]
	public async Task Given_HallId_When_DeleteRangeAsync_Then_DeleteHall()
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var halls = new List<Hall>(HallFakeData.HallIds.Count);
		foreach (Guid id in HallFakeData.HallIds)
		{
			var hall = await QueryService.GetAsync(f => f.Id == id);
			Assert.That(hall, Is.Not.Null);
			halls.Add(hall);
		}

		await CommandService.DeleteRangeAsync(halls);
		var newCount = await QueryService.CountAsync(Filter);

		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(initialCount - HallFakeData.HallIds.Count, Is.EqualTo(newCount));
	}
}
#endif
