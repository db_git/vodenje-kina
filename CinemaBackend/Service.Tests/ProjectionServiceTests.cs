//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using API.DTO.Response.Projection;

namespace Service.Tests;

[TestFixture]
public sealed class ProjectionServiceTests : ServiceTestBase<
	Projection, ProjectionQueryService, ProjectionCommandService,
	ProjectionInMemoryContext, ProjectionFilter>
{
	protected override ProjectionCommandService CommandServiceInstance()
	{
		return new ProjectionCommandService(
			Clock,
			UnitOfWork,
			new MovieQueryService(UnitOfWork, new Specification<Movie>()),
			QueryServiceInstance()
		);
	}

	[TestCase(1)]
	[TestCase(2)]
	[TestCase(3)]
	[TestCase(4)]
	public async Task Given_PaginationFilter_When_GetAllAsync_Then_ReturnProjections(int size)
	{
		var projections = await QueryService.GetAllAsync(new ProjectionFilter
		{
			Page = 1, Size = size
		});
		Assert.That(projections, Has.Count.EqualTo(size));
	}

	[Test]
	public void Given_OrderByFilter_When_GetAllAsync_Then_ReturnOrderedProjections()
	{
		AssertOrdered(
			QueryServiceInstance().GetAllAsync,
			QueryServiceInstance().GetAllAsync,
			ProjectionOrderBy.TimeAscending,
			ProjectionOrderBy.TimeDescending,
			p => p.Time
		);
	}

	[Test]
	public void Given_InvalidId_When_GetAsync_Then_ThrowEntityNotFoundException()
	{
		Assert.ThrowsAsync<EntityNotFoundException>(() =>
		{
			return QueryService.GetAsync(p => p.Id == Guid.Empty);
		});
		Assert.ThrowsAsync<EntityNotFoundException>(() =>
		{
			return QueryService.GetAsync<ProjectionDto>(p => p.Id == Guid.Empty);
		});
	}

	[TestCaseSource(typeof(ProjectionFakeData), nameof(ProjectionFakeData.ProjectionIds))]
	public async Task Given_ProjectionId_When_GetAsyncProjectTo_Then_ReturnProjected(Guid id)
	{
		var projection = await QueryService.GetAsync(p => p.Id == id);
		var dto = await QueryService.GetAsync<ProjectionDto>(p => p.Id == id);

		Assert.That(dto.Id, Is.EqualTo(projection.Id));
		Assert.That(dto.Time, Is.EqualTo(projection.Time));
		Assert.That(dto.Movie.Id, Is.EqualTo(projection.MovieId));
		Assert.That(dto.Movie.Id, Is.EqualTo(projection.Movie.Id));
		Assert.That(dto.Movie.Title, Is.EqualTo(projection.Movie.Title));
		Assert.That(dto.Movie.Year, Is.EqualTo(projection.Movie.Year));
		Assert.That(dto.Movie.Duration, Is.EqualTo(projection.Movie.Duration));
		Assert.That(dto.Movie.Tagline, Is.EqualTo(projection.Movie.Tagline));
		Assert.That(dto.Movie.Summary, Is.EqualTo(projection.Movie.Summary));
		Assert.That(dto.Movie.InCinemas, Is.EqualTo(projection.Movie.InCinemas));
		Assert.That(dto.Movie.PosterUrl, Is.EqualTo(projection.Movie.PosterUrl));
		Assert.That(dto.Movie.BackdropUrl, Is.EqualTo(projection.Movie.BackdropUrl));
		Assert.That(dto.Movie.TrailerUrl, Is.EqualTo(projection.Movie.TrailerUrl));
		Assert.That(dto.Hall.Id, Is.EqualTo(projection.HallId));
		Assert.That(dto.Hall.Id, Is.EqualTo(projection.Hall.Id));
		Assert.That(dto.Hall.Name, Is.EqualTo(projection.Hall.Name));
		Assert.That(dto.Hall.Cinema.Id, Is.EqualTo(projection.Hall.CinemaId));
		Assert.That(dto.Hall.Cinema.Id, Is.EqualTo(projection.Hall.Cinema.Id));
		Assert.That(dto.Hall.Cinema.Name, Is.EqualTo(projection.Hall.Cinema.Name));
		Assert.That(dto.Hall.Cinema.City, Is.EqualTo(projection.Hall.Cinema.City));
		Assert.That(dto.Hall.Cinema.Street, Is.EqualTo(projection.Hall.Cinema.Street));
		Assert.That(dto.Hall.Cinema.TicketPrice, Is.EqualTo(projection.Hall.Cinema.TicketPrice));
	}

	[TestCaseSource(typeof(ProjectionFakeData), nameof(ProjectionFakeData.ProjectionIds))]
	public async Task Given_PaginationFilter_When_GetAllAsyncProjectTo_Then_ReturnProjectedList(Guid id)
	{
		var filter = new ProjectionFilter { Page = 1, Size = 1 };

		QueryService = QueryServiceInstance();
		var projections = await QueryService.GetAllAsync(filter);
		QueryService = QueryServiceInstance();
		var dtos = await QueryService.GetAllAsync<ProjectionDto>(filter);

		Assert.That(dtos.First().Id, Is.EqualTo(projections.First().Id));
		Assert.That(dtos.First().Time, Is.EqualTo(projections.First().Time));
		Assert.That(dtos.First().Movie.Id, Is.EqualTo(projections.First().MovieId));
		Assert.That(dtos.First().Movie.Id, Is.EqualTo(projections.First().Movie.Id));
		Assert.That(dtos.First().Movie.Title, Is.EqualTo(projections.First().Movie.Title));
		Assert.That(dtos.First().Movie.Year, Is.EqualTo(projections.First().Movie.Year));
		Assert.That(dtos.First().Movie.Duration, Is.EqualTo(projections.First().Movie.Duration));
		Assert.That(dtos.First().Movie.Tagline, Is.EqualTo(projections.First().Movie.Tagline));
		Assert.That(dtos.First().Movie.Summary, Is.EqualTo(projections.First().Movie.Summary));
		Assert.That(dtos.First().Movie.InCinemas, Is.EqualTo(projections.First().Movie.InCinemas));
		Assert.That(dtos.First().Movie.PosterUrl, Is.EqualTo(projections.First().Movie.PosterUrl));
		Assert.That(dtos.First().Movie.BackdropUrl, Is.EqualTo(projections.First().Movie.BackdropUrl));
		Assert.That(dtos.First().Movie.TrailerUrl, Is.EqualTo(projections.First().Movie.TrailerUrl));
		Assert.That(dtos.First().Hall.Id, Is.EqualTo(projections.First().HallId));
		Assert.That(dtos.First().Hall.Id, Is.EqualTo(projections.First().Hall.Id));
		Assert.That(dtos.First().Hall.Name, Is.EqualTo(projections.First().Hall.Name));
		Assert.That(dtos.First().Hall.Cinema.Id, Is.EqualTo(projections.First().Hall.CinemaId));
		Assert.That(dtos.First().Hall.Cinema.Id, Is.EqualTo(projections.First().Hall.Cinema.Id));
		Assert.That(dtos.First().Hall.Cinema.Name, Is.EqualTo(projections.First().Hall.Cinema.Name));
		Assert.That(dtos.First().Hall.Cinema.City, Is.EqualTo(projections.First().Hall.Cinema.City));
		Assert.That(dtos.First().Hall.Cinema.Street, Is.EqualTo(projections.First().Hall.Cinema.Street));
		Assert.That(dtos.First().Hall.Cinema.TicketPrice, Is.EqualTo(projections.First().Hall.Cinema.TicketPrice));
	}

	[TestCaseSource(typeof(ProjectionFakeData), nameof(ProjectionFakeData.ProjectionIds))]
	public async Task Given_ProjectionId_When_DeleteAsync_Then_DeleteProjection(Guid id)
	{
		var initialCount = await QueryService.CountAsync(new ProjectionFilter
		{
			Page = 1, Size = 100
		});

		var projection = await QueryService.GetAsync(p => p.Id == id);
		Assert.That(projection, Is.Not.Null);
		await CommandService.DeleteAsync(projection);

		var newCount = await QueryService.CountAsync(new ProjectionFilter());
		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(initialCount - 1, Is.EqualTo(newCount));
	}

	[Test]
	public async Task Given_ProjectionIds_When_DeleteRangeAsync_Then_DeleteProjections()
	{
		var initialCount = await QueryService.CountAsync(new ProjectionFilter());

		var projections = new List<Projection>(ProjectionFakeData.ProjectionIds.Count);
		foreach (Guid id in ProjectionFakeData.ProjectionIds)
		{
			var projection = await QueryService.GetAsync(p => p.Id == id);
			Assert.That(projection, Is.Not.Null);
			projections.Add(projection);
		}

		await CommandService.DeleteRangeAsync(projections);
		var newCount = await QueryService.CountAsync(new ProjectionFilter());

		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(initialCount - ProjectionFakeData.ProjectionIds.Count, Is.EqualTo(newCount));
	}
}
#endif
