//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using API.DTO.Response.Cinema;

namespace Service.Tests;

[TestFixture]
public sealed class CinemaServiceTests : ServiceTestBase<
	Cinema, CinemaQueryService, CinemaCommandService,
	CinemaInMemoryContext, QueryFilter>
{
	[TestCaseSource(typeof(CinemaUtils), nameof(CinemaUtils.RandomCinemaNames))]
	public async Task Given_Cinema_When_AddAsync_Then_SaveCinema(string name)
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var cinema = new Cinema
		{
			Name = name,
			City = Faker.Address.City(),
			Street = Faker.Address.StreetAddress(),
			TicketPrice = 10
		};

		await CommandService.AddAsync(cinema);

		var newCount = await QueryService.CountAsync(Filter);

		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + 1));
	}

	[Test]
	public async Task Given_Cinemas_When_AddRangeAsync_Then_SaveCinemas()
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var cinemas = new List<Cinema>(CinemaUtils.RandomCinemaNames.Length);
		cinemas.AddRange(CinemaUtils.RandomCinemaNames
			.Select(name => new Cinema
			{
				Name = name,
				City = Faker.Address.City(),
				Street = Faker.Address.StreetAddress(),
				TicketPrice = 10
			})
		);

		await CommandService.AddRangeAsync(cinemas);

		var newCount = await QueryService.CountAsync(Filter);

		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + CinemaUtils.RandomCinemaNames.Length));
	}

	[TestCaseSource(typeof(CinemaUtils), nameof(CinemaUtils.SpecificCinemaNames))]
	public async Task Given_SearchFilter_When_GetAllAsync_Then_ReturnCinema(string name)
	{
		var cinemas = await QueryService.GetAllAsync(new QueryFilter
		{
			Page = 1,
			Size = 10,
			Search = name
		});

		Assert.That(cinemas, Has.Count.EqualTo(1));
		Assert.That(cinemas.First().Name.ToLower(), Does.Contain(name.ToLower()));
	}

	[TestCase(1)]
	[TestCase(2)]
	[TestCase(3)]
	[TestCase(4)]
	public async Task Given_PaginationFilter_When_GetAllAsync_Then_ReturnCinemas(int size)
	{
		var cinemas = await QueryService.GetAllAsync(new QueryFilter { Page = 1, Size = size });
		Assert.That(cinemas, Has.Count.EqualTo(size));
	}

	[Test]
	public void Given_OrderByFilter_When_GetAllAsync_Then_ReturnOrderedCinemas()
	{
		AssertOrdered(
			QueryServiceInstance().GetAllAsync,
			QueryServiceInstance().GetAllAsync,
			CinemaOrderBy.NameAscending,
			CinemaOrderBy.NameDescending,
			c => c.Name
		);
		AssertOrdered(
			QueryServiceInstance().GetAllAsync,
			QueryServiceInstance().GetAllAsync,
			CinemaOrderBy.CityAscending,
			CinemaOrderBy.CityDescending,
			c => c.City
		);
		AssertOrdered(
			QueryServiceInstance().GetAllAsync,
			QueryServiceInstance().GetAllAsync,
			CinemaOrderBy.TicketPriceAscending,
			CinemaOrderBy.TicketPriceDescending,
			c => c.TicketPrice
		);
	}

	[Test]
	public void Given_InvalidId_When_GetAsync_Then_ThrowEntityNotFoundException()
	{
		Assert.ThrowsAsync<EntityNotFoundException>(() =>
		{
			return QueryService.GetAsync(c => c.Id == Guid.Empty);
		});
		Assert.ThrowsAsync<EntityNotFoundException>(() =>
		{
			return QueryService.GetAsync<CinemaDto>(c => c.Id == Guid.Empty);
		});
	}

	[TestCaseSource(typeof(CinemaFakeData), nameof(CinemaFakeData.CinemaIds))]
	public async Task Given_CinemaId_When_GetAsyncProjectTo_Then_ReturnProjected(Guid id)
	{
		var cinema = await QueryService.GetAsync(c => c.Id == id);
		var dto = await QueryService.GetAsync<CinemaDto>(c => c.Id == id);

		Assert.That(dto.Id, Is.EqualTo(cinema.Id));
		Assert.That(dto.Name, Is.EqualTo(cinema.Name));
		Assert.That(dto.City, Is.EqualTo(cinema.City));
		Assert.That(dto.Street, Is.EqualTo(cinema.Street));
		Assert.That(dto.TicketPrice, Is.EqualTo(cinema.TicketPrice));

		Assert.That(dto.Halls.Count(), Is.EqualTo(cinema.Halls.Count));
		Assert.That(
			dto.Halls.OrderBy(d => d.Name),
			Is.EqualTo(cinema.Halls.OrderBy(c => c.Name))
		);
	}

	[TestCaseSource(typeof(CinemaFakeData), nameof(CinemaFakeData.CinemaIds))]
	public async Task Given_PaginationFilter_When_GetAllAsyncProjectTo_Then_ReturnProjectedList(Guid id)
	{
		var filter = new QueryFilter { Page = 1, Size = 1 };
		QueryService = QueryServiceInstance();
		var cinemas = await QueryService.GetAllAsync(filter);
		QueryService = QueryServiceInstance();
		var dtos = await QueryService.GetAllAsync<CinemaDto>(filter);

		Assert.That(dtos.First().Id, Is.EqualTo(cinemas.First().Id));
		Assert.That(dtos.First().Name, Is.EqualTo(cinemas.First().Name));
		Assert.That(dtos.First().City, Is.EqualTo(cinemas.First().City));
		Assert.That(dtos.First().Street, Is.EqualTo(cinemas.First().Street));
		Assert.That(dtos.First().TicketPrice, Is.EqualTo(cinemas.First().TicketPrice));

		Assert.That(dtos.First().Halls.Count(), Is.EqualTo(cinemas.First().Halls.Count));
		Assert.That(
			dtos.First().Halls.OrderBy(d => d.Name),
			Is.EqualTo(cinemas.First().Halls.OrderBy(c => c.Name))
		);
	}

	[TestCaseSource(typeof(CinemaFakeData), nameof(CinemaFakeData.CinemaIds))]
	public async Task Given_CinemaId_When_DeleteAsync_Then_DeleteCinema(Guid id)
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var cinema = await QueryService.GetAsync(c => c.Id == id);
		Assert.That(cinema, Is.Not.Null);
		await CommandService.DeleteAsync(cinema);

		var newCount = await QueryService.CountAsync(Filter);
		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(initialCount - 1, Is.EqualTo(newCount));
	}

	[Test]
	public async Task Given_CinemaIds_When_DeleteRangeAsync_Then_DeleteCinemas()
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var cinemas = new List<Cinema>(CinemaFakeData.CinemaIds.Count);
		foreach (Guid id in CinemaFakeData.CinemaIds)
		{
			var cinema = await QueryService.GetAsync(c => c.Id == id);
			Assert.That(cinema, Is.Not.Null);
			cinemas.Add(cinema);
		}

		await CommandService.DeleteRangeAsync(cinemas);

		var newCount = await QueryService.CountAsync(Filter);
		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(initialCount - CinemaFakeData.CinemaIds.Count, Is.EqualTo(newCount));
	}
}
#endif
