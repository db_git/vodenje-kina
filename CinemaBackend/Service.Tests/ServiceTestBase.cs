//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Service.Tests;

public abstract class ServiceTestBase<TEntity, TQueryService, TCommandService, TContext, TFilter>
	where TEntity : class
	where TQueryService : class
	where TCommandService : class
	where TContext : DatabaseContext
	where TFilter : QueryFilter, new()
{
	private DatabaseContext _dbContext = null!;
	private readonly IMapper _mapper = new Mapper(MapperConfig.GetConfiguration());

	protected IUnitOfWork UnitOfWork = null!;
	protected TQueryService QueryService = null!;
	protected TCommandService CommandService = null!;
	protected readonly IClock Clock = SystemClock.Instance;
	protected readonly Faker Faker = FakerUtils.GetInstance();
	protected readonly TFilter Filter = new();

	[SetUp]
	public async Task SetUp()
	{
		_dbContext = (TContext) Activator.CreateInstance(
			typeof(TContext),
			new DbContextOptionsBuilder<TContext>().Options
		)!;

		await _dbContext.Database.EnsureCreatedAsync();

		UnitOfWork = new UnitOfWork(_dbContext, _mapper);
		QueryService = QueryServiceInstance();
		CommandService = CommandServiceInstance();
	}

	[TearDown]
	public async Task TearDown()
	{
		await _dbContext.Database.EnsureDeletedAsync();
	}

	protected virtual TCommandService CommandServiceInstance()
	{
		return (TCommandService) Activator.CreateInstance(
			typeof(TCommandService),
			Clock,
			UnitOfWork
		)!;
	}

	protected virtual TQueryService QueryServiceInstance()
	{
		return (TQueryService) Activator.CreateInstance(
			typeof(TQueryService),
			UnitOfWork,
			new Specification<TEntity>()
		)!;
	}

	protected static void AssertOrdered<TEnum>(
		in Func<TFilter, Task<ICollection<TEntity>>> firstGetter,
		in Func<TFilter, Task<ICollection<TEntity>>> secondGetter,
		in TEnum firstOrderer,
		in TEnum secondOrderer,
		in Func<TEntity, object> accessor
	)
		where TEnum : struct, Enum
	{
		var firstFilter = new TFilter { Page = 1, Size = 100, OrderBy = (int) (object) firstOrderer };
		var secondFilter = new TFilter { Page = 1, Size = 100, OrderBy = (int) (object) secondOrderer };

		var firstOrderedCollection = firstGetter(firstFilter).Result;
		var secondOrderedCollection = secondGetter(secondFilter).Result;

		var firstCount = firstOrderedCollection.Count;
		var secondCount = secondOrderedCollection.Count;

		Assert.That(firstCount, Is.EqualTo(secondCount));

		for (var i = 0; i < firstCount; ++i)
		{
			var first = firstOrderedCollection.Skip(i).First();
			var second = secondOrderedCollection.Skip(secondCount - 1 - i).First();
			Assert.That(accessor(first), Is.EqualTo(accessor(second)));
		}
	}
}
#endif
