//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using API.DTO.Response.FilmRating;

namespace Service.Tests;

[TestFixture]
public sealed class FilmRatingServiceTests : ServiceTestBase<
	FilmRating, FilmRatingQueryService, FilmRatingCommandService,
	FilmRatingInMemoryContext, QueryFilter>
{
	[TestCaseSource(typeof(FilmRatingUtils), nameof(FilmRatingUtils.RandomFilmRatingTypes))]
	public void Given_DuplicateName_When_AddAsync_Then_ThrowDuplicateEntityException(string type)
	{
		var repositoryMock = new Mock<IFilmRatingRepository>();
		var unitOfWorkMock = new Mock<IUnitOfWork>();

		repositoryMock
			.Setup(g => g.CountAsync(
				It.IsAny<Expression<Func<FilmRating, bool>>>())
			)
			.ReturnsAsync(1);
		unitOfWorkMock.Setup(u => u.FilmRatingRepository).Returns(repositoryMock.Object);
		var service = new FilmRatingCommandService(Clock, unitOfWorkMock.Object);

		Assert.ThrowsAsync<DuplicateEntityException>(async () =>
		{
			await service.AddAsync(new FilmRating
			{
				Type = type, Description = Faker.Lorem.Sentence(2)
			});
		});
	}

	[TestCaseSource(typeof(FilmRatingUtils), nameof(FilmRatingUtils.RandomFilmRatingTypes))]
	public async Task Given_FilmRating_When_AddAsync_Then_SaveFilmRating(string type)
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var filmRating = new FilmRating { Type = type, Description = Faker.Lorem.Sentence(2) };

		await CommandService.AddAsync(filmRating);

		var newCount = await QueryService.CountAsync(Filter);

		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + 1));
	}

	[Test]
	public async Task Given_FilmRatings_When_AddRangeAsync_Then_SaveFilmRatings()
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var filmRatings = new List<FilmRating>(FilmRatingUtils.RandomFilmRatingTypes.Length);
		filmRatings.AddRange(FilmRatingUtils.RandomFilmRatingTypes
			.Select(type => new FilmRating
			{
				Type = type, Description = Faker.Lorem.Sentence(2)
			})
		);

		await CommandService.AddRangeAsync(filmRatings);

		var newCount = await QueryService.CountAsync(Filter);

		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + FilmRatingUtils.RandomFilmRatingTypes.Length));
	}

	[TestCaseSource(typeof(FilmRatingUtils), nameof(FilmRatingUtils.SpecificFilmRatingTypes))]
	public async Task Given_SearchFilter_When_GetAllAsync_Then_ReturnFilmRatings(string type)
	{
		var filmRatings = await QueryService.GetAllAsync(new QueryFilter
		{
			Page = 1,
			Size = 10,
			Search = type
		});

		Assert.That(filmRatings, Has.Count.Positive);
	}

	[TestCase(1)]
	[TestCase(2)]
	[TestCase(3)]
	[TestCase(4)]
	public async Task Given_PaginationFilter_When_GetAllAsync_Then_ReturnFilmRatings(int size)
	{
		var filmRatings = await QueryService.GetAllAsync(new QueryFilter { Page = 1, Size = size });
		Assert.That(size, Is.EqualTo(filmRatings.Count));
	}

	[Test]
	public void Given_OrderByFilter_When_GetAllAsync_Then_ReturnOrderedFilmRatings()
	{
		AssertOrdered(
			QueryServiceInstance().GetAllAsync,
			QueryServiceInstance().GetAllAsync,
			FilmRatingOrderBy.TypeAscending,
			FilmRatingOrderBy.TypeDescending,
			fr => fr.Type
		);
	}

	[Test]
	public void Given_InvalidId_When_GetAsync_Then_ThrowEntityNotFoundException()
	{
		Assert.ThrowsAsync<EntityNotFoundException>(() =>
		{
			return QueryService.GetAsync(fr => fr.Id == Guid.Empty);
		});
		Assert.ThrowsAsync<EntityNotFoundException>(() =>
		{
			return QueryService.GetAsync<FilmRatingDto>(fr => fr.Id == Guid.Empty);
		});
	}

	[TestCaseSource(typeof(FilmRatingFakeData), nameof(FilmRatingFakeData.FilmRatingIds))]
	public async Task Given_FilmRatingId_When_GetAsyncProjectTo_Then_ReturnProjected(Guid id)
	{
		var filmRating = await QueryService.GetAsync(fr => fr.Id == id);
		var dto = await QueryService.GetAsync<FilmRatingDto>(fr => fr.Id == id);

		Assert.That(dto.Id, Is.EqualTo(filmRating.Id));
		Assert.That(dto.Type, Is.EqualTo(filmRating.Type));
		Assert.That(dto.Description, Is.EqualTo(filmRating.Description));
	}

	[TestCaseSource(typeof(FilmRatingFakeData), nameof(FilmRatingFakeData.FilmRatingIds))]
	public async Task Given_PaginationFilter_When_GetAllAsyncProjectTo_Then_ReturnProjectedList(Guid id)
	{
		var filter = new QueryFilter { Page = 1, Size = 1 };

		QueryService = QueryServiceInstance();
		var filmRatings = await QueryService.GetAllAsync(filter);
		QueryService = QueryServiceInstance();
		var dtos = await QueryService.GetAllAsync<FilmRatingDto>(filter);

		Assert.That(dtos.First().Id, Is.EqualTo(filmRatings.First().Id));
		Assert.That(dtos.First().Type, Is.EqualTo(filmRatings.First().Type));
		Assert.That(dtos.First().Description, Is.EqualTo(filmRatings.First().Description));
	}

	[TestCaseSource(typeof(FilmRatingFakeData), nameof(FilmRatingFakeData.FilmRatingIds))]
	public async Task Given_FilmRatingId_When_DeleteAsync_Then_DeleteFilmRating(Guid id)
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var filmRating = await QueryService.GetAsync(fr => fr.Id == id);
		Assert.That(filmRating, Is.Not.Null);
		await CommandService.DeleteAsync(filmRating);

		var newCount = await QueryService.CountAsync(Filter);
		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - 1));
	}

	[Test]
	public async Task Given_FilmRatingIds_When_DeleteAsync_Then_DeleteFilmRatings()
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var filmRatings = new List<FilmRating>(FilmRatingFakeData.FilmRatingIds.Count);
		foreach (Guid id in FilmRatingFakeData.FilmRatingIds)
		{
			var filmRating = await QueryService.GetAsync(fr => fr.Id == id);
			Assert.That(filmRating, Is.Not.Null);
			filmRatings.Add(filmRating);
		}

		await CommandService.DeleteRangeAsync(filmRatings);
		var newCount = await QueryService.CountAsync(Filter);

		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - FilmRatingFakeData.FilmRatingIds.Count));
	}
}
#endif
