//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using API.DTO.Response.Genre;

namespace Service.Tests;

[TestFixture]
public sealed class GenreServiceTests : ServiceTestBase<
	Genre, GenreQueryService, GenreCommandService,
	GenreInMemoryContext, QueryFilter>
{
	[TestCaseSource(typeof(GenreUtils), nameof(GenreUtils.RandomGenreNames))]
	public void Given_DuplicateName_When_AddAsync_Then_ThrowDuplicateEntityException(string name)
	{
		var repositoryMock = new Mock<IGenreRepository>();
		var unitOfWorkMock = new Mock<IUnitOfWork>();

		repositoryMock
			.Setup(g => g.CountAsync(It.IsAny<Expression<Func<Genre, bool>>>()))
			.ReturnsAsync(1);
		unitOfWorkMock.Setup(u => u.GenreRepository).Returns(repositoryMock.Object);
		var service = new GenreCommandService(Clock, unitOfWorkMock.Object);

		Assert.ThrowsAsync<DuplicateEntityException>(async () =>
		{
			await service.AddAsync(new Genre { Name = name });
		});
	}

	[TestCaseSource(typeof(GenreUtils), nameof(GenreUtils.RandomGenreNames))]
	public async Task Given_Genre_When_AddAsync_Then_SaveGenre(string name)
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var genre = new Genre { Name = name };
		await CommandService.AddAsync(genre);

		var newCount = await QueryService.CountAsync(Filter);

		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(initialCount + 1, Is.EqualTo(newCount));
	}

	[Test]
	public async Task Given_Genre_When_AddRangeAsync_Then_SaveGenre()
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var genres = new List<Genre>(GenreUtils.RandomGenreNames.Length);
		genres.AddRange(GenreUtils.RandomGenreNames
			.Select(name => new Genre { Name = name })
		);

		await CommandService.AddRangeAsync(genres);

		var newCount = await QueryService.CountAsync(Filter);
		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(initialCount + GenreUtils.RandomGenreNames.Length, Is.EqualTo(newCount));
	}

	[TestCaseSource(typeof(GenreUtils), nameof(GenreUtils.SpecificGenreNames))]
	public async Task Given_SearchFilter_When_GetAllAsync_Then_ReturnGenre(string name)
	{
		var genres = await QueryService.GetAllAsync(new QueryFilter
		{
			Page = 1,
			Size = 10,
			Search = name
		});

		Assert.That(genres, Has.Count.EqualTo(1));
		Assert.That(genres.First().Name, Is.EqualTo(name));
	}

	[TestCase(1)]
	[TestCase(2)]
	[TestCase(3)]
	[TestCase(4)]
	public async Task Given_PaginationFilter_When_GetAllAsync_Then_ReturnGenres(int size)
	{
		var genres = await QueryService.GetAllAsync(new QueryFilter { Page = 1, Size = size });
		Assert.That(genres, Has.Count.EqualTo(size));
	}

	[Test]
	public void Given_OrderByFilter_When_GetAllAsync_Then_ReturnOrderedGenres()
	{
		AssertOrdered(
			QueryServiceInstance().GetAllAsync,
			QueryServiceInstance().GetAllAsync,
			GenreOrderBy.NameAscending,
			GenreOrderBy.NameDescending,
			g => g.Name
		);
	}

	[Test]
	public void Given_InvalidId_When_GetAsync_Then_ThrowEntityNotFoundException()
	{
		Assert.ThrowsAsync<EntityNotFoundException>(() =>
		{
			return QueryService.GetAsync(g => g.Id == Guid.Empty);
		});
		Assert.ThrowsAsync<EntityNotFoundException>(() =>
		{
			return QueryService.GetAsync<GenreDto>(g => g.Id == Guid.Empty);
		});
	}

	[TestCaseSource(typeof(GenreFakeData), nameof(GenreFakeData.GenreIds))]
	public async Task Given_GenreId_When_GetAsyncProjectTo_Then_ReturnProjected(Guid id)
	{
		var genre = await QueryService.GetAsync(g => g.Id == id);
		var dto = await QueryService.GetAsync<GenreDto>(g => g.Id == id);

		Assert.That(dto.Id, Is.EqualTo(genre.Id));
		Assert.That(dto.Name, Is.EqualTo(genre.Name));
	}

	[TestCaseSource(typeof(GenreFakeData), nameof(GenreFakeData.GenreIds))]
	public async Task Given_PaginationFilter_When_GetAllAsyncProjectTo_Then_ReturnProjectedList(Guid id)
	{
		var filter = new QueryFilter { Page = 1, Size = 1 };

		QueryService = QueryServiceInstance();
		var genres = await QueryService.GetAllAsync(filter);
		QueryService = QueryServiceInstance();
		var dtos = await QueryService.GetAllAsync<GenreDto>(filter);

		Assert.That(dtos.First().Id, Is.EqualTo(genres.First().Id));
		Assert.That(dtos.First().Name, Is.EqualTo(genres.First().Name));
	}

	[TestCaseSource(typeof(GenreFakeData), nameof(GenreFakeData.GenreIds))]
	public async Task Given_GenreId_When_DeleteAsync_Then_DeleteGenre(Guid id)
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var genre = await QueryService.GetAsync(g => g.Id == id);
		Assert.That(genre, Is.Not.Null);
		await CommandService.DeleteAsync(genre);

		var newCount = await QueryService.CountAsync(Filter);
		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(initialCount - 1, Is.EqualTo(newCount));
	}

	[Test]
	public async Task Given_GenreIds_When_DeleteRangeAsync_Then_DeleteGenres()
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var genres = new List<Genre>(GenreFakeData.GenreIds.Count);
		foreach (Guid id in GenreFakeData.GenreIds)
		{
			var genre = await QueryService.GetAsync(g => g.Id == id);
			Assert.That(genre, Is.Not.Null);
			genres.Add(genre);
		}

		await CommandService.DeleteRangeAsync(genres);
		var newCount = await QueryService.CountAsync(Filter);

		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(initialCount - GenreFakeData.GenreIds.Count, Is.EqualTo(newCount));
	}
}
#endif
