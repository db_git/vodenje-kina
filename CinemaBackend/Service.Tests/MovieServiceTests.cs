//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using API.DTO.Response.Movie;

namespace Service.Tests;

[TestFixture]
public sealed class MovieServiceTests : ServiceTestBase<
	Movie, MovieQueryService, MovieCommandService,
	MovieInMemoryContext, MovieFilter>
{
	[TestCaseSource(typeof(MovieUtils), nameof(MovieUtils.RandomMovieIds))]
	public async Task Given_Movie_When_AddAsync_Then_SaveMovie(string id)
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var movie = new Movie
		{
			Id = id,
			Title = Faker.Lorem.Word(),
			Year = Faker.Random.UInt(),
			Duration = Faker.Random.UInt(),
			Tagline = Faker.Lorem.Sentence(),
			Summary = Faker.Lorem.Paragraphs(2),
			InCinemas = Faker.Random.Bool(),
			PosterUrl = Faker.Internet.Url(),
			PosterId = Faker.Random.String(10),
			BackdropUrl = Faker.Internet.Url(),
			BackdropId = Faker.Random.String(10),
			TrailerUrl = Faker.Internet.Url()
		};

		await CommandService.AddAsync(movie);

		var newCount = await QueryService.CountAsync(Filter);

		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(initialCount + 1, Is.EqualTo(newCount));
	}

	[Test]
	public async Task Given_Movies_When_AddRangeAsync_Then_SaveMovies()
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var movies = new List<Movie>(MovieUtils.RandomMovieIds.Length);
		movies.AddRange(MovieUtils.RandomMovieIds
			.Select(id => new Movie
			{
				Id = id,
				Title = Faker.Lorem.Word(),
				Year = Faker.Random.UInt(),
				Duration = Faker.Random.UInt(),
				Tagline = Faker.Lorem.Sentence(),
				Summary = Faker.Lorem.Paragraphs(2),
				InCinemas = Faker.Random.Bool(),
				PosterUrl = Faker.Internet.Url(),
				PosterId = Faker.Random.String(10),
				BackdropUrl = Faker.Internet.Url(),
				BackdropId = Faker.Random.String(10),
				TrailerUrl = Faker.Internet.Url()
			})
		);

		await CommandService.AddRangeAsync(movies);

		var newCount = await QueryService.CountAsync(Filter);
		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(initialCount + MovieUtils.RandomMovieIds.Length, Is.EqualTo(newCount));
	}

	[TestCaseSource(typeof(MovieUtils), nameof(MovieUtils.SpecificMovieTitles))]
	public async Task Given_SearchFilter_When_GetAllAsync_Then_ReturnMovie(string title)
	{
		var movies = await QueryService.GetAllAsync(new MovieFilter
		{
			Page = 1,
			Size = 10,
			Search = title
		});

		Assert.That(movies, Has.Count.EqualTo(1));
		Assert.That(movies.First().Title, Is.EqualTo(title));
	}

	[TestCase(1)]
	[TestCase(2)]
	[TestCase(3)]
	[TestCase(4)]
	public async Task Given_PaginationFilter_When_GetAllAsync_Then_ReturnMovies(int size)
	{
		var movies = await QueryService.GetAllAsync(new MovieFilter { Page = 1, Size = size });
		Assert.That(movies, Has.Count.EqualTo(size));
	}

	[Test]
	public void Given_OrderByFilter_When_GetAllAsync_Then_ReturnOrderedMovies()
	{
		AssertOrdered(
			QueryServiceInstance().GetAllAsync,
			QueryServiceInstance().GetAllAsync,
			MovieOrderBy.TitleAscending,
			MovieOrderBy.TitleDescending,
			m => m.Title
		);
		AssertOrdered(
			QueryServiceInstance().GetAllAsync,
			QueryServiceInstance().GetAllAsync,
			MovieOrderBy.YearAscending,
			MovieOrderBy.YearDescending,
			m => m.Year
		);
		AssertOrdered(
			QueryServiceInstance().GetAllAsync,
			QueryServiceInstance().GetAllAsync,
			MovieOrderBy.CreatedAtAscending,
			MovieOrderBy.CreatedAtDescending,
			m => m.CreatedAt
		);
	}

	[TestCase(0U)]
	[TestCase(9999U)]
	[TestCase(11111U)]
	public async Task Given_InvalidYearFilter_When_GetAllAsync_Then_ReturnNoMovies(uint year)
	{
		var movies = await QueryService.GetAllAsync(new MovieFilter { Page = 1, Size = 100, Year = year });
		Assert.That(movies, Is.Empty);
	}

	[Test]
	public async Task Given_YearRangeFilter_When_GetAllAsync_Then_ReturnMovies()
	{
		var movies = await QueryService.GetAllAsync(new MovieFilter
		{
			Page = 1, Size = 100, YearStart = 1U, YearEnd = 5000U
		});

		Assert.That(movies, Is.Not.Empty);
	}

	[TestCase(5750U, 99999U)]
	[TestCase(100000U, uint.MaxValue)]
	public async Task Given_InvalidYearRangeFilter_When_GetAllAsync_Then_ReturnNoMovies(uint start, uint end)
	{
		var movies = await QueryService.GetAllAsync(new MovieFilter
		{
			Page = 1, Size = 100, YearStart = start, YearEnd = end
		});

		Assert.That(movies, Is.Empty);
	}

	[Test]
	public async Task Given_DurationFilter_When_GetAllAsync_Then_ReturnMovies()
	{
		var movies = await QueryService.GetAllAsync(new MovieFilter
		{
			Page = 1, Size = 100, DurationUnder = 360U
		});

		Assert.That(movies, Is.Not.Empty);
	}

	[Test]
	public async Task Given_InvalidDurationFilter_When_GetAllAsync_Then_ReturnNoMovies()
	{
		var movies = await QueryService.GetAllAsync(new MovieFilter
		{
			Page = 1, Size = 100, DurationOver = 60000U
		});

		Assert.That(movies, Is.Empty);
	}

	[Test]
	public async Task Given_GenresFilter_When_GetAllAsync_Then_ReturnMovies()
	{
		var movies = await QueryService.GetAllAsync(new MovieFilter
		{
			Page = 1, Size = 100, Genres = new List<string> { "War" }
		});

		Assert.That(movies, Is.Not.Empty);
	}

	[Test]
	public async Task Given_UpcomingFilter_When_GetAllAsync_Then_ReturnMoviesInCinemas()
	{
		var movies = await QueryService.GetAllAsync(new MovieFilter
		{
			Page = 1, Size = 100, ShowUpcoming = 1U
		});

		Assert.That(movies, Is.Not.Empty);
		foreach (var movie in movies)
		{
			Assert.That(movie.InCinemas, Is.True);
		}
	}

	[Test]
	public void Given_InvalidId_When_GetAsync_Then_ThrowEntityNotFoundException()
	{
		var id = Faker.Lorem.Sentence(10).Trim();

		Assert.ThrowsAsync<EntityNotFoundException>(() =>
		{
			return QueryService.GetAsync(m => m.Id == id);
		});
		Assert.ThrowsAsync<EntityNotFoundException>(() =>
		{
			return QueryService.GetAsync<MovieDto>(m => m.Id == id);
		});
		Assert.ThrowsAsync<EntityNotFoundException>(() =>
		{
			return QueryService.GetAsync<IndividualMovieDto>(m => m.Id == id);
		});
	}

	[TestCaseSource(typeof(MovieFakeData), nameof(MovieFakeData.MovieIds))]
	public async Task Given_MovieId_When_GetAsyncProjectTo_Then_ReturnProjected(string id)
	{
		var movie = await QueryService.GetAsync(m => m.Id == id);
		var dto1 = await QueryService.GetAsync<MovieDto>(m => m.Id == id);
		var dto2 = await QueryService.GetAsync<IndividualMovieDto>(m => m.Id == id);

		Assert.That(dto1.Id, Is.EqualTo(movie.Id));
		Assert.That(dto1.Title, Is.EqualTo(movie.Title));
		Assert.That(dto1.Tagline, Is.EqualTo(movie.Tagline));
		Assert.That(dto1.Summary, Is.EqualTo(movie.Summary));
		Assert.That(dto1.Year, Is.EqualTo(movie.Year));
		Assert.That(dto1.Duration, Is.EqualTo(movie.Duration));
		Assert.That(dto1.PosterUrl, Is.EqualTo(movie.PosterUrl));
		Assert.That(dto1.BackdropUrl, Is.EqualTo(movie.BackdropUrl));
		Assert.That(dto1.TrailerUrl, Is.EqualTo(movie.TrailerUrl));
		Assert.That(dto1.InCinemas, Is.EqualTo(movie.InCinemas));
		Assert.That(dto2.Id, Is.EqualTo(movie.Id));
		Assert.That(dto2.Title, Is.EqualTo(movie.Title));
		Assert.That(dto2.Tagline, Is.EqualTo(movie.Tagline));
		Assert.That(dto2.Summary, Is.EqualTo(movie.Summary));
		Assert.That(dto2.Year, Is.EqualTo(movie.Year));
		Assert.That(dto2.Duration, Is.EqualTo(movie.Duration));
		Assert.That(dto2.PosterUrl, Is.EqualTo(movie.PosterUrl));
		Assert.That(dto2.BackdropUrl, Is.EqualTo(movie.BackdropUrl));
		Assert.That(dto2.TrailerUrl, Is.EqualTo(movie.TrailerUrl));
		Assert.That(dto2.InCinemas, Is.EqualTo(movie.InCinemas));
		Assert.That(dto2.FilmRating.Id, Is.EqualTo(movie.FilmRatingId));
		Assert.That(dto2.FilmRating.Id, Is.EqualTo(movie.FilmRating.Id));
		Assert.That(dto2.FilmRating.Type, Is.EqualTo(movie.FilmRating.Type));
		Assert.That(dto2.FilmRating.Description, Is.EqualTo(movie.FilmRating.Description));
		Assert.That(
			movie.MovieGenres
				.Select(mg => new { mg.Genre.Id, mg.Genre.Name })
				.OrderBy(g => g.Name)
				.ToList(),
			Is.EqualTo(dto2.Genres
				.Select(g => new { g.Id, g.Name })
				.OrderBy(g => g.Name)
				.ToList()
			)
		);
		Assert.That(
			movie.MoviePeople
				.Where(mp => mp.Type == "Actor")
				.Select(mp => new
				{
					mp.Person.Id,
					mp.Person.Name,
					mp.Character,
					mp.Order,
					mp.Person.ImageUrl
				})
				.OrderBy(p => p.Name),
			Is.EqualTo(dto2.Actors
				.Select(p => new { p.Id, p.Name, p.Character, p.Order, p.ImageUrl })
				.OrderBy(p => p.Name)
			)
		);
		Assert.That(
			movie.MoviePeople
				.Where(mp => mp.Type == "Director")
				.Select(mp => new { mp.Person.Id, mp.Person.Name, mp.Person.ImageUrl })
				.OrderBy(p => p.Name),
			Is.EqualTo(dto2.Directors
				.Select(p => new { p.Id, p.Name, p.ImageUrl })
				.OrderBy(p => p.Name)
			)
		);
	}

	[TestCaseSource(typeof(MovieFakeData), nameof(MovieFakeData.MovieIds))]
	public async Task Given_PaginationFilter_When_GetAllAsyncProjectTo_Then_ReturnProjectedList(string id)
	{
		var filter = new MovieFilter { Page = 1, Size = 1 };

		QueryService = QueryServiceInstance();
		var movies = await QueryService.GetAllAsync(filter);
		QueryService = QueryServiceInstance();
		var dtos = await QueryService.GetAllAsync<MovieDto>(filter);

		Assert.That(dtos.First().Id, Is.EqualTo(movies.First().Id));
		Assert.That(dtos.First().Title, Is.EqualTo(movies.First().Title));
		Assert.That(dtos.First().Tagline, Is.EqualTo(movies.First().Tagline));
		Assert.That(dtos.First().Summary, Is.EqualTo(movies.First().Summary));
		Assert.That(dtos.First().Year, Is.EqualTo(movies.First().Year));
		Assert.That(dtos.First().Duration, Is.EqualTo(movies.First().Duration));
		Assert.That(dtos.First().PosterUrl, Is.EqualTo(movies.First().PosterUrl));
		Assert.That(dtos.First().BackdropUrl, Is.EqualTo(movies.First().BackdropUrl));
		Assert.That(dtos.First().TrailerUrl, Is.EqualTo(movies.First().TrailerUrl));
		Assert.That(dtos.First().InCinemas, Is.EqualTo(movies.First().InCinemas));
	}

	[TestCaseSource(typeof(MovieFakeData), nameof(MovieFakeData.MovieIds))]
	public async Task Given_MovieId_When_DeleteRangeAsync_Then_DeleteMovie(string id)
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var movie = await QueryService.GetAsync(m => m.Id == id);
		Assert.That(movie, Is.Not.Null);
		await CommandService.DeleteAsync(movie);

		var newCount = await QueryService.CountAsync(Filter);
		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(initialCount - 1, Is.EqualTo(newCount));
	}
}
#endif
