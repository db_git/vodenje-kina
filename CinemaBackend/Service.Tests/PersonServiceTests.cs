//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using API.DTO.Response.Person;
using Person = Domain.Entities.Person;

namespace Service.Tests;

[TestFixture]
public sealed class PersonServiceTests : ServiceTestBase<
	Person, PersonQueryService, PersonCommandService,
	PersonInMemoryContext, PersonFilter>
{
	[TestCaseSource(typeof(PersonUtils), nameof(PersonUtils.RandomPersonNames))]
	public async Task Given_Person_When_AddAsync_Then_SavePerson(string name)
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var person = new Person
		{
			Id = name + Faker.Lorem.Sentence(2),
			Name = name,
			DateOfBirth = new LocalDate(),
			ImageUrl = Faker.Internet.Url(),
			ImageId = Faker.Random.String(10),
			Biography = Faker.Lorem.Paragraphs()
		};

		await CommandService.AddAsync(person);

		var newCount = await QueryService.CountAsync(Filter);

		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(initialCount + 1, Is.EqualTo(newCount));
	}

	[Test]
	public async Task Given_People_When_AddRangeAsync_Then_SavePeople()
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var persons = new List<Person>(PersonUtils.RandomPersonNames.Length);
		persons.AddRange(PersonUtils.RandomPersonNames
			.Select(name => new Person
			{
				Id = name + Faker.Lorem.Sentence(2),
				Name = name,
				DateOfBirth = new LocalDate(),
				ImageUrl = Faker.Internet.Url(),
				ImageId = Faker.Random.String(10),
				Biography = Faker.Lorem.Paragraphs()
			})
		);

		await CommandService.AddRangeAsync(persons);

		var newCount = await QueryService.CountAsync(Filter);
		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(initialCount + PersonUtils.RandomPersonNames.Length, Is.EqualTo(newCount));
	}

	[TestCaseSource(typeof(PersonUtils), nameof(PersonUtils.SpecificPersonNames))]
	public async Task Given_SearchFilter_When_GetAllAsync_Then_ReturnPerson(string name)
	{
		var people = await QueryService.GetAllAsync(new PersonFilter
		{
			Page = 1,
			Size = 10,
			Search = name
		});
		Assert.That(people, Is.Not.Empty);
	}

	[TestCase(1)]
	[TestCase(2)]
	[TestCase(3)]
	[TestCase(4)]
	public async Task Given_PaginationFilter_When_GetAllAsync_Then_ReturnPeople(int size)
	{
		var people = await QueryService.GetAllAsync(new PersonFilter { Page = 1, Size = size });
		Assert.That(people, Has.Count.EqualTo(size));
	}

	[Test]
	public void Given_OrderByFilter_When_GetAllAsync_Then_ReturnOrderedPeople()
	{
		AssertOrdered(
			QueryServiceInstance().GetAllAsync,
			QueryServiceInstance().GetAllAsync,
			PersonOrderBy.NameAscending,
			PersonOrderBy.NameDescending,
			p => p.Name
		);
	}

	[Test]
	public async Task Given_DayMonthFilter_When_GetAllAsync_Then_ReturnSpecificPeople()
	{
		var people = await QueryService.GetAllAsync(new PersonFilter
		{
			Page = 1, Size = 100, Day = 31, Month = 12
		});

		Assert.That(people, Is.Not.Empty);
		foreach (var person in people)
		{
			Assert.That(person.DateOfBirth.Day, Is.EqualTo(31));
			Assert.That(person.DateOfBirth.Month, Is.EqualTo(12));
		}
	}

	[Test]
	public void Given_InvalidId_When_GetAsync_Then_ThrowEntityNotFoundException()
	{
		var id = Faker.Lorem.Sentence(10).Trim();

		Assert.ThrowsAsync<EntityNotFoundException>(() =>
		{
			return QueryService.GetAsync(p => p.Id == id);
		});
		Assert.ThrowsAsync<EntityNotFoundException>(() =>
		{
			return QueryService.GetAsync<PersonDto>(p => p.Id == id);
		});
	}

	[TestCaseSource(typeof(PersonFakeData), nameof(PersonFakeData.PeopleIds))]
	public async Task Given_PersonId_When_GetAsyncProjectTo_Then_ReturnProjected(string id)
	{
		var person = await QueryService.GetAsync(p => p.Id == id);
		var dto = await QueryService.GetAsync<PersonDto>(p => p.Id == id);

		Assert.That(dto.Id, Is.EqualTo(person.Id));
		Assert.That(dto.Name, Is.EqualTo(person.Name));
		Assert.That(dto.Biography, Is.EqualTo(person.Biography));
		Assert.That(dto.DateOfBirth, Is.EqualTo(person.DateOfBirth));
		Assert.That(dto.DateOfDeath, Is.EqualTo(person.DateOfDeath));
		Assert.That(dto.ImageUrl, Is.EqualTo(person.ImageUrl));
		Assert.That(
			dto.Movies
				.Select(m => new
				{
					m.Id,
					m.Title,
					m.Year,
					m.PosterUrl,
					m.BackdropUrl,
					m.Type,
					m.Character
				}),
			Is.EqualTo(person.MoviePeople
				.Select(mp => new
				{
					mp.Movie.Id,
					mp.Movie.Title,
					mp.Movie.Year,
					mp.Movie.PosterUrl,
					mp.Movie.BackdropUrl,
					mp.Type,
					mp.Character
				})
			)
		);
	}

	[TestCaseSource(typeof(PersonFakeData), nameof(PersonFakeData.PeopleIds))]
	public async Task Given_PaginationFilter_When_GetAllAsyncProjectTo_Then_ReturnProjectedList(string id)
	{
		var filter = new PersonFilter { Page = 1, Size = 1 };

		QueryService = QueryServiceInstance();
		var people = await QueryService.GetAllAsync(filter);
		QueryService = QueryServiceInstance();
		var dtos = await QueryService.GetAllAsync<PersonDto>(filter);

		Assert.That(dtos.First().Id, Is.EqualTo(people.First().Id));
		Assert.That(dtos.First().Name, Is.EqualTo(people.First().Name));
		Assert.That(dtos.First().Biography, Is.EqualTo(people.First().Biography));
		Assert.That(dtos.First().DateOfBirth, Is.EqualTo(people.First().DateOfBirth));
		Assert.That(dtos.First().DateOfDeath, Is.EqualTo(people.First().DateOfDeath));
		Assert.That(dtos.First().ImageUrl, Is.EqualTo(people.First().ImageUrl));
		Assert.That(
			dtos.First().Movies
				.Select(m => new
				{
					m.Id,
					m.Title,
					m.Year,
					m.PosterUrl,
					m.BackdropUrl,
					m.Type,
					m.Character
				}),
			Is.EqualTo(people.First().MoviePeople
				.Select(mp => new
				{
					mp.Movie.Id,
					mp.Movie.Title,
					mp.Movie.Year,
					mp.Movie.PosterUrl,
					mp.Movie.BackdropUrl,
					mp.Type,
					mp.Character
				})
			)
		);
	}

	[TestCaseSource(typeof(PersonFakeData), nameof(PersonFakeData.PeopleIds))]
	public async Task Given_PersonId_When_DeleteAsync_Then_DeletePerson(string id)
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var person = await QueryService.GetAsync(p => p.Id == id);
		Assert.That(person, Is.Not.Null);
		await CommandService.DeleteAsync(person);

		var newCount = await QueryService.CountAsync(Filter);
		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(initialCount - 1, Is.EqualTo(newCount));
	}

	[Test]
	public async Task Given_PeopleIds_When_DeleteAsync_Then_DeletePeople()
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var people = new List<Person>(PersonFakeData.PeopleIds.Count);
		foreach (string id in PersonFakeData.PeopleIds)
		{
			var person = await QueryService.GetAsync(p => p.Id == id);
			Assert.That(person, Is.Not.Null);
			people.Add(person);
		}

		await CommandService.DeleteRangeAsync(people);

		var newCount = await QueryService.CountAsync(Filter);
		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(initialCount - PersonFakeData.PeopleIds.Count, Is.EqualTo(newCount));
	}
}
#endif
