//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using API.DTO.Response.Food;

namespace Service.Tests;

[TestFixture]
public sealed class FoodServiceTests : ServiceTestBase<
	Food, FoodQueryService, FoodCommandService,
	FoodInMemoryContext, FoodFilter>
{
	[TestCaseSource(typeof(FoodUtils), nameof(FoodUtils.SpecificFoodTypes))]
	public async Task Given_Food_When_AddAsync_Then_SaveFood(string type)
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var food = new Food
		{
			Name = Faker.Lorem.Word(),
			Price = Faker.Finance.Amount(),
			AvailableQuantity = Faker.Random.ULong(),
			Type = type,
			Size = Faker.Finance.Amount(),
			ImageUrl = Faker.Internet.Url(),
			ImageId = Faker.Random.String(10)
		};

		await CommandService.AddAsync(food);

		var newCount = await QueryService.CountAsync(Filter);

		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + 1));
	}

	[Test]
	public async Task Given_Foods_When_AddRangeAsync_Then_SaveFoods()
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var foods = new List<Food>(FoodUtils.SpecificFoodTypes.Length);
		foods.AddRange(FoodUtils.SpecificFoodTypes
			.Select(type => new Food
			{
				Name = Faker.Lorem.Word(),
				Price = Faker.Finance.Amount(),
				AvailableQuantity = Faker.Random.ULong(),
				Type = type,
				Size = Faker.Finance.Amount(),
				ImageUrl = Faker.Internet.Url(),
				ImageId = Faker.Random.String(10)
			})
		);

		await CommandService.AddRangeAsync(foods);

		var newCount = await QueryService.CountAsync(Filter);

		Assert.That(newCount, Is.GreaterThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount + FoodUtils.SpecificFoodTypes.Length));
	}

	[TestCaseSource(typeof(FoodUtils), nameof(FoodUtils.SpecificFoodNames))]
	public async Task Given_SearchFilter_When_GetAllAsync_Then_ReturnFood(string name)
	{
		var foods = await QueryService.GetAllAsync(new FoodFilter
		{
			Page = 1,
			Size = 10,
			Search = name
		});

		Assert.That(foods, Has.Count.EqualTo(1));
		Assert.That(foods.First().Name, Is.EqualTo(name));
	}

	[TestCase(1)]
	[TestCase(2)]
	[TestCase(3)]
	[TestCase(4)]
	public async Task Given_PaginationFilter_When_GetAllAsync_Then_ReturnFoods(int size)
	{
		var foods = await QueryService.GetAllAsync(new FoodFilter { Page = 1, Size = size });
		Assert.That(foods, Has.Count.EqualTo(size));
	}

	[Test]
	public void Given_OrderByFilter_When_GetAllAsync_Then_ReturnOrderedFoods()
	{
		AssertOrdered(
			QueryServiceInstance().GetAllAsync,
			QueryServiceInstance().GetAllAsync,
			FoodOrderBy.NameAscending,
			FoodOrderBy.NameDescending,
			f => f.Name
		);
		AssertOrdered(
			QueryServiceInstance().GetAllAsync,
			QueryServiceInstance().GetAllAsync,
			FoodOrderBy.PriceAscending,
			FoodOrderBy.PriceDescending,
			f => f.Price
		);
		AssertOrdered(
			QueryServiceInstance().GetAllAsync,
			QueryServiceInstance().GetAllAsync,
			FoodOrderBy.SizeAscending,
			FoodOrderBy.SizeDescending,
			f => f.Size
		);
		AssertOrdered(
			QueryServiceInstance().GetAllAsync,
			QueryServiceInstance().GetAllAsync,
			FoodOrderBy.TypeAscending,
			FoodOrderBy.TypeDescending,
			f => f.Type
		);
		AssertOrdered(
			QueryServiceInstance().GetAllAsync,
			QueryServiceInstance().GetAllAsync,
			FoodOrderBy.AvailableQuantityAscending,
			FoodOrderBy.AvailableQuantityDescending,
			f => f.AvailableQuantity
		);
	}

	[Test]
	public void Given_InvalidId_When_GetAsync_Then_ThrowEntityNotFoundException()
	{
		Assert.ThrowsAsync<EntityNotFoundException>(() =>
		{
			return QueryService.GetAsync(f => f.Id == Guid.Empty);
		});
		Assert.ThrowsAsync<EntityNotFoundException>(() =>
		{
			return QueryService.GetAsync<FoodDto>(f => f.Id == Guid.Empty);
		});
	}

	[TestCaseSource(typeof(FoodFakeData), nameof(FoodFakeData.FoodIds))]
	public async Task Given_FoodId_When_GetAsyncProjectTo_Then_ReturnProjected(Guid id)
	{
		var food = await QueryService.GetAsync(f => f.Id == id);
		var dto = await QueryService.GetAsync<FoodDto>(f => f.Id == id);

		Assert.That(dto.Id, Is.EqualTo(food.Id));
		Assert.That(dto.Name, Is.EqualTo(food.Name));
		Assert.That(dto.Price, Is.EqualTo(food.Price));
		Assert.That(dto.Size, Is.EqualTo(food.Size));
		Assert.That(dto.Type, Is.EqualTo(food.Type));
		Assert.That(dto.AvailableQuantity, Is.EqualTo(food.AvailableQuantity));
		Assert.That(dto.ImageUrl, Is.EqualTo(food.ImageUrl));
	}

	[TestCaseSource(typeof(FoodFakeData), nameof(FoodFakeData.FoodIds))]
	public async Task Given_PaginationFilter_When_GetAllAsyncProjectTo_Then_ReturnProjectedList(Guid id)
	{
		var filter = new FoodFilter { Page = 1, Size = 1 };

		QueryService = QueryServiceInstance();
		var foods = await QueryService.GetAllAsync(filter);
		QueryService = QueryServiceInstance();
		var dtos = await QueryService.GetAllAsync<FoodDto>(filter);

		Assert.That(dtos.First().Id, Is.EqualTo(foods.First().Id));
		Assert.That(dtos.First().Name, Is.EqualTo(foods.First().Name));
		Assert.That(dtos.First().Price, Is.EqualTo(foods.First().Price));
		Assert.That(dtos.First().Size, Is.EqualTo(foods.First().Size));
		Assert.That(dtos.First().Type, Is.EqualTo(foods.First().Type));
		Assert.That(dtos.First().AvailableQuantity, Is.EqualTo(foods.First().AvailableQuantity));
		Assert.That(dtos.First().ImageUrl, Is.EqualTo(foods.First().ImageUrl));
	}

	[TestCaseSource(typeof(FoodFakeData), nameof(FoodFakeData.FoodIds))]
	public async Task Given_FoodId_When_DeleteAsync_Then_DeleteFood(Guid id)
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var food = await QueryService.GetAsync(f => f.Id == id);
		Assert.That(food, Is.Not.Null);
		await CommandService.DeleteAsync(food);

		var newCount = await QueryService.CountAsync(Filter);
		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - 1));
	}

	[Test]
	public async Task Given_FoodIds_When_DeleteRangeAsync_Then_DeleteFoods()
	{
		var initialCount = await QueryService.CountAsync(Filter);

		var foods = new List<Food>(FoodFakeData.FoodIds.Count);
		foreach (Guid id in FoodFakeData.FoodIds)
		{
			var food = await QueryService.GetAsync(f => f.Id == id);
			Assert.That(food, Is.Not.Null);
			foods.Add(food);
		}

		await CommandService.DeleteRangeAsync(foods);
		var newCount = await QueryService.CountAsync(Filter);

		Assert.That(newCount, Is.LessThan(initialCount));
		Assert.That(newCount, Is.EqualTo(initialCount - FoodFakeData.FoodIds.Count));
	}
}
#endif
