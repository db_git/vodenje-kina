//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using API.DTO.Validations;
using NUnit.Framework;

namespace API.Tests;

[TestFixture]
public sealed class ValidationAttributeTests
{
	[TestCase("f11faf4b-d714-4a07-83a9-2bf4a5c2b51b", ExpectedResult = true)]
	[TestCase("9b48e240-05f8-4a4a-b6c6-142c0cda2111", ExpectedResult = true)]
	[TestCase("c95e3a7e-5872-4586-90da-2f51b9db9d64", ExpectedResult = true)]
	public bool Given_ValidGuid_When_GuidAttribute_Then_ReturnTrue(string? value)
	{
		return new GuidAttribute().IsValid(value);
	}

	[TestCase(null, ExpectedResult = false)]
	[TestCase("1234 wasd test", ExpectedResult = false)]
	[TestCase("00000000-0000-0000-0000-000000000000", ExpectedResult = false)]
	[TestCase("test string", ExpectedResult = false)]
	[TestCase("Test string", ExpectedResult = false)]
	[TestCase("", ExpectedResult = false)]
	[TestCase("TESt sTRING", ExpectedResult = false)]
	[TestCase("1TESt2sTRING3", ExpectedResult = false)]
	[TestCase("TESTSTRING", ExpectedResult = false)]
	[TestCase("TEST STRING", ExpectedResult = false)]
	[TestCase("123456890", ExpectedResult = false)]
	[TestCase("1 2 3 4 5 6 8 9 0 W A S D", ExpectedResult = false)]
	public bool Given_InvalidGuid_When_GuidAttribute_Then_ReturnFalse(string? value)
	{
		return new GuidAttribute().IsValid(value);
	}

	[TestCase(null, ExpectedResult = true)]
	[TestCase("WASD wasd test TEST", ExpectedResult = true)]
	[TestCase("Test string", ExpectedResult = true)]
	[TestCase("TESt sTRING", ExpectedResult = true)]
	public bool Given_NoNumbers_When_ForbidNumbersAttribute_Then_ReturnTrue(string? value)
	{
		return new ForbidNumbersAttribute().IsValid(value);
	}

	[TestCase("1234 wasd test", ExpectedResult = false)]
	[TestCase("Test string 1", ExpectedResult = false)]
	[TestCase("1TESt2sTRING3", ExpectedResult = false)]
	[TestCase("123456890", ExpectedResult = false)]
	[TestCase("1 2 3 4 5 6 8 9 0 W A S D", ExpectedResult = false)]
	[TestCase(123456890, ExpectedResult = false)]
	public bool Given_Numbers_When_ForbidNumbersAttribute_Then_ReturnFalse(object? value)
	{
		return new ForbidNumbersAttribute().IsValid(value);
	}

	[TestCase(null, ExpectedResult = true)]
	[TestCase("1234 wasd test", ExpectedResult = true)]
	[TestCase("WASD wasd test TEST", ExpectedResult = true)]
	[TestCase("Test string", ExpectedResult = true)]
	[TestCase("TESt sTRING", ExpectedResult = true)]
	[TestCase("1TESt2sTRING3", ExpectedResult = true)]
	[TestCase("TEST STRING", ExpectedResult = true)]
	[TestCase("123456890", ExpectedResult = true)]
	[TestCase("1 2 3 4 5 6 8 9 0 W A S D", ExpectedResult = true)]
	[TestCase(123456890, ExpectedResult = true)]
	public bool Given_UppercaseValueOrNumber_When_FirstCharacterUppercaseOrNumberAttribute_Then_ReturnTrue(object? value)
	{
		return new FirstCharacterUppercaseOrNumberAttribute().IsValid(value);
	}

	[TestCase("f11faf4b", ExpectedResult = false)]
	[TestCase("test string", ExpectedResult = false)]
	[TestCase("tEST", ExpectedResult = false)]
	public bool Given_LowercaseValue_When_FirstCharacterUppercaseOrNumberAttribute_Then_ReturnFalse(string? value)
	{
		return new FirstCharacterUppercaseOrNumberAttribute().IsValid(value);
	}

	[TestCase(null, ExpectedResult = true)]
	[TestCase("1234 wasd test", ExpectedResult = true)]
	[TestCase("WASD wasd test TEST", ExpectedResult = true)]
	[TestCase("Test string", ExpectedResult = true)]
	[TestCase("TESt sTRING", ExpectedResult = true)]
	[TestCase("1TESt2sTRING3", ExpectedResult = true)]
	[TestCase("TEST STRING", ExpectedResult = true)]
	[TestCase("123456890", ExpectedResult = true)]
	[TestCase("1 2 3 4 5 6 8 9 0 W A S D", ExpectedResult = true)]
	[TestCase(123456890, ExpectedResult = true)]
	public bool Given_UppercaseValue_When_FirstLetterUppercaseAttribute_Then_ReturnTrue(object? value)
	{
		return new FirstLetterUppercaseAttribute().IsValid(value);
	}

	[TestCase("f11faf4b", ExpectedResult = false)]
	[TestCase("test string", ExpectedResult = false)]
	[TestCase("tEST", ExpectedResult = false)]
	public bool Given_LowercaseValue_When_FirstLetterUppercaseAttribute_Then_ReturnFalse(string? value)
	{
		return new FirstLetterUppercaseAttribute().IsValid(value);
	}

	[TestCase("Snack", ExpectedResult = true)]
	[TestCase("Drink", ExpectedResult = true)]
	public bool Given_SnackOrDrink_When_SnackOrDrinkAttribute_Then_ReturnTrue(string? value)
	{
		return new SnackOrDrinkAttribute().IsValid(value);
	}

	[TestCase("test string", ExpectedResult = false)]
	[TestCase("snack", ExpectedResult = false)]
	[TestCase("drink", ExpectedResult = false)]
	[TestCase(null, ExpectedResult = false)]
	[TestCase(123, ExpectedResult = false)]
	public bool Given_NotSnackAndNotDrink_When_SnackOrDrinkAttribute_Then_ReturnFalse(object? value)
	{
		return new SnackOrDrinkAttribute().IsValid(value);
	}

	[TestCase("1234 wasd test", ExpectedResult = true)]
	[TestCase("00000000-0000-0000-0000-000000000000", ExpectedResult = true)]
	[TestCase("Test string 1", ExpectedResult = true)]
	[TestCase("TESTSTRING", ExpectedResult = true)]
	[TestCase("TEST STRING", ExpectedResult = true)]
	[TestCase("1TESt2sTRING3", ExpectedResult = true)]
	[TestCase(123456890, ExpectedResult = true)]
	[TestCase("1 2 3 4 5 6 8 9 0 W A S D", ExpectedResult = true)]
	[TestCase("\n", ExpectedResult = true)]
	public bool Given_NotNullAndNotEmptyValue_When_StringNotNullOrEmptyAttribute_Then_ReturnTrue(object? value)
	{
		return new StringNotNullOrEmptyAttribute().IsValid(value);
	}

	[TestCase("", ExpectedResult = false)]
	[TestCase(null, ExpectedResult = false)]
	public bool Given_NullOrEmptyValue_When_StringNotNullOrEmptyAttribute_Then_ReturnFalse(string? value)
	{
		return new StringNotNullOrEmptyAttribute().IsValid(value);
	}

	[TestCase("", ExpectedResult = false)]
	[TestCase(null, ExpectedResult = false)]
	[TestCase(123, ExpectedResult = false)]
	public bool Given_NotCollectionWhen_ValidateCollectionAttribute_Then_ReturnFalse(object? value)
	{
		return new ValidateCollectionAttribute<GuidAttribute>().IsValid(value);
	}

	[TestCase(null, ExpectedResult = false)]
	[TestCase("", ExpectedResult = false)]
	[TestCase("00000000-0000-0000-0000-000000000000", ExpectedResult = false)]
	[TestCase("1234 wasd test", ExpectedResult = false)]
	[TestCase(123, ExpectedResult = false)]
	public bool Given_InvalidGuidCollectionWhen_ValidateCollectionAttribute_Then_ReturnFalse(object? value)
	{
		return new ValidateCollectionAttribute<GuidAttribute>().IsValid(new List<object?> { value });
	}

	[TestCase("f11faf4b-d714-4a07-83a9-2bf4a5c2b51b", ExpectedResult = true)]
	[TestCase("9b48e240-05f8-4a4a-b6c6-142c0cda2111", ExpectedResult = true)]
	[TestCase("c95e3a7e-5872-4586-90da-2f51b9db9d64", ExpectedResult = true)]
	public bool Given_ValidGuidCollectionWhen_ValidateCollectionAttribute_Then_ReturnTrue(object? value)
	{
		return new ValidateCollectionAttribute<GuidAttribute>().IsValid(new List<object?> { value });
	}
}
#endif
