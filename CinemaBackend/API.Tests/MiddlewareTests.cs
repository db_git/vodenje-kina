//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using System.Text.Json;
using API.Middleware;
using Imagekit.Helper;
using ImageMagick;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using Service.Exceptions;

namespace API.Tests;

[TestFixture]
public sealed class MiddlewareTests
{
	private readonly ILogger<ExceptionMiddleware> _logger = new LoggerFactory().CreateLogger<ExceptionMiddleware>();
	private ExceptionMiddleware _middleware = null!;
	private static HttpContext _context = null!;

	[SetUp]
	public void SetUp()
	{
		_context = new DefaultHttpContext();
		_context.Response.Body = new MemoryStream();
	}

	[Test]
	public async Task When_JsonException_Should_ReturnStatusUnsupportedMediaType()
	{
		Task Request(HttpContext _) => Task.FromException(new JsonException());
		_middleware = new(Request, _logger);

		await _middleware.Invoke(_context);

		var body = await ReadContextStreamToLower(_context);

		Assert.That(_context.Response.StatusCode, Is.EqualTo(StatusCodes.Status415UnsupportedMediaType));
		Assert.That(body, Does.StartWith("{"));
		Assert.That(body, Does.EndWith("}"));
		Assert.That(body, Does.Contain("warning"));
		Assert.That(body, Does.Contain("title"));
		Assert.That(body, Does.Contain("severity"));
		Assert.That(body, Does.Contain("message"));
		Assert.That(body, Does.Contain("bad request"));
		Assert.That(body, Does.Contain("malformed json"));
	}

	[Test]
	public async Task When_IDuplicateEntityException_Should_ReturnStatusConflict()
	{
		Task Request(HttpContext _) => Task.FromException(new DuplicateEntityException("wasd123"));
		_middleware = new(Request, _logger);

		await _middleware.Invoke(_context);

		var body = await ReadContextStreamToLower(_context);

		Assert.That(_context.Response.StatusCode, Is.EqualTo(StatusCodes.Status409Conflict));
		Assert.That(body, Does.StartWith("{"));
		Assert.That(body, Does.EndWith("}"));
		Assert.That(body, Does.Contain("title"));
		Assert.That(body, Does.Contain("severity"));
		Assert.That(body, Does.Contain("message"));
		Assert.That(body, Does.Contain("warning"));
		Assert.That(body, Does.Contain("conflict"));
		Assert.That(body, Does.Contain("wasd123"));
	}

	[Test]
	public async Task When_IEntityNotFoundException_Should_ReturnStatusNotFound()
	{
		Task Request(HttpContext _) => Task.FromException(new EntityNotFoundException("wasd123"));
		_middleware = new(Request, _logger);

		await _middleware.Invoke(_context);

		var body = await ReadContextStreamToLower(_context);

		Assert.That(_context.Response.StatusCode, Is.EqualTo(StatusCodes.Status404NotFound));
		Assert.That(body, Does.StartWith("{"));
		Assert.That(body, Does.EndWith("}"));
		Assert.That(body, Does.Contain("title"));
		Assert.That(body, Does.Contain("severity"));
		Assert.That(body, Does.Contain("warning"));
		Assert.That(body, Does.Contain("wasd123"));
	}

	[Test]
	public async Task When_IProjectionOverlapException_Should_ReturnStatusConflict()
	{
		Task Request(HttpContext _) => Task.FromException(new ProjectionOverlapException());
		_middleware = new(Request, _logger);

		await _middleware.Invoke(_context);

		var body = await ReadContextStreamToLower(_context);

		Assert.That(_context.Response.StatusCode, Is.EqualTo(StatusCodes.Status409Conflict));
		Assert.That(body, Does.StartWith("{"));
		Assert.That(body, Does.EndWith("}"));
		Assert.That(body, Does.Contain("title"));
		Assert.That(body, Does.Contain("severity"));
		Assert.That(body, Does.Contain("message"));
		Assert.That(body, Does.Contain("warning"));
		Assert.That(body, Does.Contain("time overlap"));
		Assert.That(body, Does.Contain("projection"));
	}

	[Test]
	public async Task When_IInvalidCredentialsException_Should_ReturnStatusBadRequest()
	{
		Task Request(HttpContext _) => Task.FromException(new InvalidCredentialsException());
		_middleware = new(Request, _logger);

		await _middleware.Invoke(_context);

		var body = await ReadContextStreamToLower(_context);

		Assert.That(_context.Response.StatusCode, Is.EqualTo(StatusCodes.Status400BadRequest));
		Assert.That(body, Does.StartWith("{"));
		Assert.That(body, Does.EndWith("}"));
		Assert.That(body, Does.Contain("title"));
		Assert.That(body, Does.Contain("severity"));
		Assert.That(body, Does.Contain("message"));
		Assert.That(body, Does.Contain("warning"));
		Assert.That(body, Does.Contain("invalid"));
		Assert.That(body, Does.Contain("credentials"));
		Assert.That(body, Does.Contain("email"));
		Assert.That(body, Does.Contain("password"));
	}

	[Test]
	public async Task When_IEmailAlreadyInUseException_Should_ReturnStatusBadRequest()
	{
		Task Request(HttpContext _) => Task.FromException(new EmailAlreadyInUseException());
		_middleware = new(Request, _logger);

		await _middleware.Invoke(_context);

		var body = await ReadContextStreamToLower(_context);

		Assert.That(_context.Response.StatusCode, Is.EqualTo(StatusCodes.Status400BadRequest));
		Assert.That(body, Does.StartWith("{"));
		Assert.That(body, Does.EndWith("}"));
		Assert.That(body, Does.Contain("title"));
		Assert.That(body, Does.Contain("severity"));
		Assert.That(body, Does.Contain("warning"));
		Assert.That(body, Does.Contain("email"));
		Assert.That(body, Does.Contain("taken"));
	}

	[Test]
	public async Task When_IRegistrationException_Should_ReturnStatusUnprocessableEntity()
	{
		Task Request(HttpContext _) =>
			Task.FromException(new RegistrationException(Mock.Of<IDictionary<string,IEnumerable<string>>>()));

		_middleware = new(Request, _logger);
		await _middleware.Invoke(_context);

		var body = await ReadContextStreamToLower(_context);

		Assert.That(_context.Response.StatusCode, Is.EqualTo(StatusCodes.Status422UnprocessableEntity));
		Assert.That(body, Does.StartWith("{"));
		Assert.That(body, Does.EndWith("}"));
		Assert.That(body, Does.Contain("title"));
		Assert.That(body, Does.Contain("severity"));
		Assert.That(body, Does.Contain("message"));
		Assert.That(body, Does.Contain("errors"));
		Assert.That(body, Does.Contain("warning"));
		Assert.That(body, Does.Contain("validation"));
	}

	[Test]
	public async Task When_MagickException_Should_ReturnStatusBadRequest()
	{
		Task Request(HttpContext _) => Task.FromException(new MagickErrorException(""));
		_middleware = new(Request, _logger);

		await _middleware.Invoke(_context);

		var body = await ReadContextStreamToLower(_context);

		Assert.That(_context.Response.StatusCode, Is.EqualTo(StatusCodes.Status400BadRequest));
		Assert.That(body, Does.StartWith("{"));
		Assert.That(body, Does.EndWith("}"));
		Assert.That(body, Does.Contain("title"));
		Assert.That(body, Does.Contain("severity"));
		Assert.That(body, Does.Contain("message"));
		Assert.That(body, Does.Contain("warning"));
		Assert.That(body, Does.Contain("failed"));
		Assert.That(body, Does.Contain("image"));
	}

	[Test]
	public async Task When_ImagekitException_Should_ReturnStatusBadGateway()
	{
		Task Request(HttpContext _) => Task.FromException(new ImagekitException());
		_middleware = new(Request, _logger);

		await _middleware.Invoke(_context);

		var body = await ReadContextStreamToLower(_context);

		Assert.That(_context.Response.StatusCode, Is.EqualTo(StatusCodes.Status502BadGateway));
		Assert.That(body, Does.StartWith("{"));
		Assert.That(body, Does.EndWith("}"));
		Assert.That(body, Does.Contain("title"));
		Assert.That(body, Does.Contain("severity"));
		Assert.That(body, Does.Contain("message"));
		Assert.That(body, Does.Contain("error"));
		Assert.That(body, Does.Contain("remote"));
		Assert.That(body, Does.Contain("storage"));
	}

	[Test]
	public async Task When_Exception_Should_ReturnStatusInternalServerError()
	{
		Task Request(HttpContext _) => Task.FromException(new Exception());
		_middleware = new(Request, _logger);

		await _middleware.Invoke(_context);

		var body = await ReadContextStreamToLower(_context);

		Assert.That(_context.Response.StatusCode, Is.EqualTo(StatusCodes.Status500InternalServerError));
		Assert.That(body, Does.StartWith("{"));
		Assert.That(body, Does.EndWith("}"));
		Assert.That(body, Does.Contain("title"));
		Assert.That(body, Does.Contain("severity"));
		Assert.That(body, Does.Contain("message"));
		Assert.That(body, Does.Contain("error"));
		Assert.That(body, Does.Contain("unknown"));
	}

	[Test]
	public async Task When_NoException_Should_Pass()
	{
		Task Request(HttpContext _) => Task.CompletedTask;
		_middleware = new(Request, _logger);

		await _middleware.Invoke(_context);

		Assert.Pass();
	}

	private static async Task<string> ReadContextStreamToLower(HttpContext context)
	{
		context.Response.Body.Position = 0;
		using var sr = new StreamReader(context.Response.Body);
		return (await sr.ReadToEndAsync()).ToLower();
	}
}
#endif
