//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using API.DTO.Request.FilmRating;
using API.DTO.Response.FilmRating;
using Domain.Entities;
using NUnit.Framework;
using Tests.Common.Fakers;

namespace API.Tests.MapperTests;

[TestFixture]
public sealed class FilmRatingMapperTests : MapperTestBase
{
	[SetUp]
	public void SetUp()
	{
		FilmRatingFakeData.Initialize();
	}

	[Test]
	public void Given_FilmRating_When_Map_Then_ReturnFilmRatingDto()
	{
		var filmRatings = FilmRatingFakeData.FilmRatings.ToList();

		var filmRatingDtos = Mapper.Map<IList<FilmRatingDto>>(filmRatings);

		Assert.That(filmRatings, Has.Count.EqualTo(filmRatingDtos.Count));
		foreach (var (filmRating, dto) in filmRatings.Zip(filmRatingDtos))
		{
			Assert.That(dto.Id, Is.EqualTo(filmRating.Id));
			Assert.That(dto.Type, Is.EqualTo(filmRating.Type));
			Assert.That(dto.Description, Is.EqualTo(filmRating.Description));
		}
	}

	[Test]
	public void Given_CreateFilmRatingDto_When_Map_Then_ReturnFilmRating()
	{
		var dto = new CreateFilmRatingDto
		{
			Type = "A",
			Description = Faker.Lorem.Sentence()
		};

		var filmRating = Mapper.Map<FilmRating>(dto);

		Assert.That(Guid.Empty, Is.EqualTo(filmRating.Id));
		Assert.That(dto.Type, Is.EqualTo(filmRating.Type));
		Assert.That(dto.Description, Is.EqualTo(filmRating.Description));
	}
}
#endif
