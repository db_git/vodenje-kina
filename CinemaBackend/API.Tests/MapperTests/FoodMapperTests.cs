//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using API.DTO.Request.Food;
using API.DTO.Response.Food;
using Domain.Entities;
using NUnit.Framework;
using Tests.Common.Fakers;

namespace API.Tests.MapperTests;

[TestFixture]
public sealed class FoodMapperTests : MapperTestBase
{
	[SetUp]
	public void SetUp()
	{
		FoodFakeData.Initialize();
	}

	[Test]
	public void Given_Food_When_Map_Then_ReturnFoodDto()
	{
		var foods = FoodFakeData.Foods.ToList();

		var foodDtos = Mapper.Map<IList<FoodDto>>(foods);

		Assert.That(foods, Has.Count.EqualTo(foodDtos.Count));
		foreach (var (food, dto) in foods.Zip(foodDtos))
		{
			Assert.That(dto.Id, Is.EqualTo(food.Id));
			Assert.That(dto.Name, Is.EqualTo(food.Name));
			Assert.That(dto.Price, Is.EqualTo(food.Price));
			Assert.That(dto.AvailableQuantity, Is.EqualTo(food.AvailableQuantity));
			Assert.That(dto.Type, Is.EqualTo(food.Type));
			Assert.That(dto.Size, Is.EqualTo(food.Size));
			Assert.That(dto.ImageUrl, Is.EqualTo(food.ImageUrl));
		}
	}

	[Test]
	public void Given_CreateFoodDto_When_Map_Then_ReturnFood()
	{
		var dto = new CreateFoodDto
		{
			Name = Faker.Lorem.Word(),
			Price = Faker.Finance.Amount(),
			AvailableQuantity = Faker.Random.ULong(),
			Type = "Snack",
			Size = Faker.Random.ULong(),
			ImageUrl = Faker.Internet.Url()
		};

		var food = Mapper.Map<Food>(dto);

		Assert.That(Guid.Empty, Is.EqualTo(food.Id));
		Assert.That(dto.Name, Is.EqualTo(food.Name));
		Assert.That(dto.Price, Is.EqualTo(food.Price));
		Assert.That(dto.AvailableQuantity, Is.EqualTo(food.AvailableQuantity));
		Assert.That(dto.Type, Is.EqualTo(food.Type));
		Assert.That(dto.Size, Is.EqualTo(food.Size));
		Assert.That(food.ImageUrl, Is.Null);
	}
}
#endif
