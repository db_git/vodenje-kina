//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using API.DTO.Request.Cinema;
using API.DTO.Response.Cinema;
using Domain.Entities;
using NUnit.Framework;
using Tests.Common.Fakers;

namespace API.Tests.MapperTests;

[TestFixture]
public sealed class CinemaMapperTests : MapperTestBase
{
	[SetUp]
	public void SetUp()
	{
		CinemaFakeData.Initialize();
		HallFakeData.Initialize();
	}

	[Test]
	public void Given_Cinema_When_Map_Then_ReturnCinemaDto()
	{
		var cinemas = CinemaFakeData.Cinemas.ToList();

		var cinemaDtos = Mapper.Map<IList<CinemaDto>>(cinemas);

		Assert.That(cinemas, Has.Count.EqualTo(cinemaDtos.Count));
		foreach (var (cinema, dto) in cinemas.Zip(cinemaDtos))
		{
			Assert.That(dto.Id, Is.EqualTo(cinema.Id));
			Assert.That(dto.Name, Is.EqualTo(cinema.Name));
			Assert.That(dto.City, Is.EqualTo(cinema.City));
			Assert.That(dto.Street, Is.EqualTo(cinema.Street));
			Assert.That(dto.TicketPrice, Is.EqualTo(cinema.TicketPrice));
		}
	}

	[Test]
	public void Given_Hall_When_Map_Then_ReturnCinemaHallDto()
	{
		var halls = HallFakeData.Halls.ToList();

		var cinemaHallDtos = Mapper.Map<IList<CinemaHallDto>>(halls);

		Assert.That(halls, Has.Count.EqualTo(cinemaHallDtos.Count));
		foreach (var (hall, dto) in halls.Zip(cinemaHallDtos))
		{
			Assert.That(dto.Id, Is.EqualTo(hall.Id));
			Assert.That(dto.Name, Is.EqualTo(hall.Name));
		}
	}

	[Test]
	public void Given_CreateCinemaDto_When_Map_Then_ReturnCinema()
	{
		var dto = new CreateCinemaDto
		{
			Name = Faker.Lorem.Word(),
			City = Faker.Address.City(),
			Street = Faker.Address.StreetAddress(),
			TicketPrice = Faker.Finance.Amount()
		};

		var cinema = Mapper.Map<Cinema>(dto);

		Assert.That(Guid.Empty, Is.EqualTo(cinema.Id));
		Assert.That(dto.Name, Is.EqualTo(cinema.Name));
		Assert.That(dto.City, Is.EqualTo(cinema.City));
		Assert.That(dto.Street, Is.EqualTo(cinema.Street));
		Assert.That(dto.TicketPrice, Is.EqualTo(cinema.TicketPrice));
	}
}
#endif
