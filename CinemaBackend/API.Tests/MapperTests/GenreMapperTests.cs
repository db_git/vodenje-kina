//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using API.DTO.Request.Genre;
using API.DTO.Response.Genre;
using Domain.Entities;
using NUnit.Framework;
using Tests.Common.Fakers;

namespace API.Tests.MapperTests;

[TestFixture]
public sealed class GenreMapperTests : MapperTestBase
{
	[SetUp]
	public void SetUp()
	{
		GenreFakeData.Initialize();
	}

	[Test]
	public void Given_Genre_When_Map_Then_ReturnGenreDto()
	{
		var genres = GenreFakeData.Genres.ToList();

		var genreDtos = Mapper.Map<IList<GenreDto>>(genres);

		Assert.That(genres, Has.Count.EqualTo(genreDtos.Count));
		foreach (var (genre, dto) in genres.Zip(genreDtos))
		{
			Assert.That(dto.Id, Is.EqualTo(genre.Id));
			Assert.That(dto.Name, Is.EqualTo(genre.Name));
		}
	}

	[Test]
	public void Given_CreateGenreDto_When_Map_Then_ReturnGenre()
	{
		var dto = new CreateGenreDto
		{
			Name = Faker.Lorem.Word(),
		};

		var genre = Mapper.Map<Genre>(dto);

		Assert.That(Guid.Empty, Is.EqualTo(genre.Id));
		Assert.That(dto.Name, Is.EqualTo(genre.Name));
	}
}
#endif
