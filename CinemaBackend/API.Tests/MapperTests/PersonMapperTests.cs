//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using API.DTO.Response.Person;
using NUnit.Framework;
using Tests.Common.Fakers;

namespace API.Tests.MapperTests;

[TestFixture]
public sealed class PersonMapperTests : MapperTestBase
{
	[SetUp]
	public void SetUp()
	{
		PersonFakeData.Initialize();
	}

	[Test]
	public void Given_Person_When_Map_Then_ReturnPersonDto()
	{
		var persons = PersonFakeData.People.ToList();

		var personDtos = Mapper.Map<IList<PersonDto>>(persons);

		Assert.That(persons, Has.Count.EqualTo(personDtos.Count));
		foreach (var (person, dto) in persons.Zip(personDtos))
		{
			Assert.That(dto.Id, Is.EqualTo(person.Id));
			Assert.That(dto.Name, Is.EqualTo(person.Name));
			Assert.That(dto.ImageUrl, Is.EqualTo(person.ImageUrl));
			Assert.That(dto.DateOfBirth, Is.EqualTo(person.DateOfBirth));
			Assert.That(dto.DateOfDeath, Is.EqualTo(person.DateOfDeath));
			Assert.That(dto.Biography, Is.EqualTo(person.Biography));
		}
	}
}
#endif
