//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using API.DTO.Request.Hall;
using API.DTO.Response.Hall;
using Domain.Entities;
using NUnit.Framework;
using Tests.Common.Fakers;

namespace API.Tests.MapperTests;

[TestFixture]
public sealed class HallMapperTests : MapperTestBase
{
	[SetUp]
	public void SetUp()
	{
		HallFakeData.Initialize();
		CinemaFakeData.Initialize();
	}

	[Test]
	public void Given_Hall_When_Map_Then_ReturnHallDto()
	{
		var halls = HallFakeData.Halls.ToList();

		var hallDtos = Mapper.Map<IEnumerable<HallDto>>(halls).ToList();

		Assert.That(halls, Has.Count.EqualTo(hallDtos.Count));
		foreach (var (hall, dto) in halls.Zip(hallDtos))
		{
			Assert.That(dto.Id, Is.EqualTo(hall.Id));
			Assert.That(dto.Name, Is.EqualTo(hall.Name));
		}
	}

	[Test]
	public void Given_Cinema_When_Map_Then_ReturnHallCinemaDto()
	{
		var cinemas = CinemaFakeData.Cinemas.ToList();

		var hallCinemaDtos = Mapper.Map<IList<HallCinemaDto>>(cinemas);

		Assert.That(cinemas, Has.Count.EqualTo(hallCinemaDtos.Count));
		foreach (var (cinema, dto) in cinemas.Zip(hallCinemaDtos))
		{
			Assert.That(dto.Id, Is.EqualTo(cinema.Id));
			Assert.That(dto.Name, Is.EqualTo(cinema.Name));
			Assert.That(dto.City, Is.EqualTo(cinema.City));
			Assert.That(dto.Street, Is.EqualTo(cinema.Street));
			Assert.That(dto.TicketPrice, Is.EqualTo(cinema.TicketPrice));
		}
	}

	[Test]
	public void Given_CreateHallDto_When_Map_Then_ReturnHall()
	{
		var dto = new CreateHallDto
		{
			Name = Faker.Lorem.Word(),
			Cinema = Guid.NewGuid()
		};

		var hall = Mapper.Map<Hall>(dto);

		Assert.That(Guid.Empty, Is.EqualTo(hall.Id));
		Assert.That(dto.Name, Is.EqualTo(hall.Name));
		Assert.That(hall.Cinema, Is.Null);
		Assert.That(dto.Cinema, Is.EqualTo(hall.CinemaId));
		Assert.That(hall.HallSeats, Is.Empty.Or.Null);
		Assert.That(hall.Projections, Is.Empty.Or.Null);
	}
}
#endif
