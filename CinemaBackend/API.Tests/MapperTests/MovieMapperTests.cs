//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using API.DTO.Request.Movie;
using API.DTO.Response.Movie;
using Domain.Entities;
using NodaTime;
using NUnit.Framework;
using Tests.Common.Fakers;

namespace API.Tests.MapperTests;

[TestFixture]
public sealed class MovieMapperTests : MapperTestBase
{
	[SetUp]
	public void SetUp()
	{
		MovieFakeData.Initialize();
		MovieGenreFakeData.Initialize();
		MoviePersonFakeData.Initialize();
	}

	[Test]
	public void Given_Movie_When_Map_Then_ReturnMovieDto()
	{
		var movies = MovieFakeData.Movies.ToList();

		var movieDtos = Mapper.Map<IList<MovieDto>>(movies);

		Assert.That(movies, Has.Count.EqualTo(movieDtos.Count));
		foreach (var (movie, dto) in movies.Zip(movieDtos))
		{
			Assert.That(dto.Id, Is.EqualTo(movie.Id));
			Assert.That(dto.Title, Is.EqualTo(movie.Title));
			Assert.That(dto.Year, Is.EqualTo(movie.Year));
			Assert.That(dto.Duration, Is.EqualTo(movie.Duration));
			Assert.That(dto.Tagline, Is.EqualTo(movie.Tagline));
			Assert.That(dto.Summary, Is.EqualTo(movie.Summary));
			Assert.That(dto.InCinemas, Is.EqualTo(movie.InCinemas));
			Assert.That(dto.PosterUrl, Is.EqualTo(movie.PosterUrl));
			Assert.That(dto.BackdropUrl, Is.EqualTo(movie.BackdropUrl));
			Assert.That(dto.TrailerUrl, Is.EqualTo(movie.TrailerUrl));
		}
	}

	[Test]
	public void Given_Movie_When_Map_Then_ReturnIndividualMovieDto()
	{
		var individualMovie = MovieFakeData.Movies.First();
		var filmRatingId = Guid.NewGuid();

		individualMovie.FilmRatingId = filmRatingId;
		individualMovie.FilmRating = new FilmRating
		{
			Id = filmRatingId,
			Type = "A",
			Description = Faker.Lorem.Sentence()
		};

		individualMovie.MovieGenres = MovieGenreFakeData.MovieGenres.Select(mg =>
		{
			mg.Genre = new Genre
			{
				Id = Guid.NewGuid(),
				Name = Faker.Lorem.Word()
			};

			return mg;
		}).ToList();

		individualMovie.MoviePeople = MoviePersonFakeData.MoviePeople.Select(mp =>
		{
			var id = Faker.Name.FullName();

			mp.PersonId = id;
			mp.Person = new Person
			{
				Id = id,
				Name = Faker.Person.FullName,
				DateOfBirth = LocalDate.FromDateTime(Faker.Date.Past().ToUniversalTime()),
				DateOfDeath = LocalDate.FromDateTime(Faker.Date.Soon().ToUniversalTime()),
				ImageUrl = Faker.Internet.Url(),
				ImageId = Faker.Random.String(10),
				Biography = Faker.Lorem.Paragraphs()
			};

			return mp;
		}).ToList();

		var individualMovieDto = Mapper.Map<IndividualMovieDto>(individualMovie);

		Assert.That(individualMovieDto.Id, Is.EqualTo(individualMovie.Id));
			Assert.That(individualMovieDto.Title, Is.EqualTo(individualMovie.Title));
			Assert.That(individualMovieDto.Year, Is.EqualTo(individualMovie.Year));
			Assert.That(individualMovieDto.Duration, Is.EqualTo(individualMovie.Duration));
			Assert.That(individualMovieDto.Tagline, Is.EqualTo(individualMovie.Tagline));
			Assert.That(individualMovieDto.Summary, Is.EqualTo(individualMovie.Summary));
			Assert.That(individualMovieDto.InCinemas, Is.EqualTo(individualMovie.InCinemas));
			Assert.That(individualMovieDto.PosterUrl, Is.EqualTo(individualMovie.PosterUrl));
			Assert.That(individualMovieDto.BackdropUrl, Is.EqualTo(individualMovie.BackdropUrl));
			Assert.That(individualMovieDto.TrailerUrl, Is.EqualTo(individualMovie.TrailerUrl));
			Assert.That(individualMovieDto.FilmRating.Id, Is.EqualTo(individualMovie.FilmRatingId));
			Assert.That(individualMovieDto.FilmRating.Id, Is.EqualTo(individualMovie.FilmRating.Id));
			Assert.That(individualMovieDto.FilmRating.Type, Is.EqualTo(individualMovie.FilmRating.Type));
			Assert.That(individualMovieDto.FilmRating.Description, Is.EqualTo(individualMovie.FilmRating.Description));

			var movieActors = individualMovie
				.MoviePeople
				.Where(mp => mp.Type == "Actor")
				.ToList();
			var movieDirectors = individualMovie
				.MoviePeople
				.Where(mp => mp.Type == "Director")
				.ToList();

			Assert.That(individualMovieDto.Actors.ToList(), Has.Count.EqualTo(movieActors.Count));
			foreach (var (moviePerson, movieActorDto) in movieActors.Zip(individualMovieDto.Actors))
			{
				Assert.That(movieActorDto.Id, Is.EqualTo(moviePerson.Person.Id));
				Assert.That(movieActorDto.Name, Is.EqualTo(moviePerson.Person.Name));
				Assert.That(movieActorDto.ImageUrl, Is.EqualTo(moviePerson.Person.ImageUrl));
				Assert.That(movieActorDto.Character, Is.EqualTo(moviePerson.Character));
			}

			Assert.That(individualMovieDto.Directors.ToList(), Has.Count.EqualTo(movieDirectors.Count));
			foreach (var (moviePerson, movieDirectorDto) in movieActors.Zip(individualMovieDto.Actors))
			{
				Assert.That(movieDirectorDto.Id, Is.EqualTo(moviePerson.Person.Id));
				Assert.That(movieDirectorDto.Name, Is.EqualTo(moviePerson.Person.Name));
				Assert.That(movieDirectorDto.ImageUrl, Is.EqualTo(moviePerson.Person.ImageUrl));
			}
	}

	[Test]
	public void Given_CreateMovieDto_When_Map_Then_ReturnMovie()
	{
		var dto = new CreateMovieDto
		{
			Title = Faker.Lorem.Word(),
			Year = Faker.Random.UInt(1U, 5000U),
			Duration = Faker.Random.UInt(30U, 360U),
			Tagline = Faker.Lorem.Sentence(),
			Summary = Faker.Lorem.Paragraphs(2),
			InCinemas = Faker.Random.Bool(),
			PosterUrl = Faker.Internet.Url(),
			BackdropUrl = Faker.Internet.Url(),
			TrailerUrl = Faker.Internet.Url(),
			FilmRating = Guid.NewGuid(),
			Genres = new List<Guid> { Guid.NewGuid() },
			Actors = new List<CreateMovieActorDto>
			{
				new()
				{
					Id = Faker.Name.FullName(), Character = Faker.Name.FirstName()
				}
			},
			Directors = new List<string> { Faker.Name.FullName() }
		};

		var movie = Mapper.Map<Movie>(dto);

		Assert.That(movie.Id, Is.Null.Or.Empty);
		Assert.That(dto.Title, Is.EqualTo(movie.Title));
		Assert.That(dto.Year, Is.EqualTo(movie.Year));
		Assert.That(dto.Duration, Is.EqualTo(movie.Duration));
		Assert.That(dto.Tagline, Is.EqualTo(movie.Tagline));
		Assert.That(dto.Summary, Is.EqualTo(movie.Summary));
		Assert.That(dto.InCinemas, Is.EqualTo(movie.InCinemas));
		Assert.That(movie.PosterUrl, Is.Null);
		Assert.That(movie.BackdropUrl, Is.Null);
		Assert.That(dto.TrailerUrl, Is.EqualTo(movie.TrailerUrl));
		Assert.That(dto.FilmRating, Is.EqualTo(movie.FilmRatingId));

		Assert.That(
			dto.Actors.Count(),
			Is.EqualTo(movie.MoviePeople.Count(mp => mp.Type == "Actor"))
		);
		Assert.That(
			dto.Directors.Count(),
			Is.EqualTo(movie.MoviePeople.Count(mp => mp.Type == "Director"))
		);
	}
}
#endif
