//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using Tests.Common.Fakers;
using Tests.Common.Utils;

namespace Tests.Common.Contexts;

public sealed class CinemaInMemoryContext : DatabaseContext
{
	private readonly Faker _faker = FakerUtils.GetInstance();

	public CinemaInMemoryContext(DbContextOptions options) : base(options) {}

	protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
	{
		base.OnConfiguring(optionsBuilder);
		optionsBuilder
			.UseInMemoryDatabase("CinemaInMemoryTestingDatabase")
			.EnableSensitiveDataLogging()
			.EnableDetailedErrors()
			.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
	}

	protected override void OnModelCreating(ModelBuilder builder)
	{
		base.OnModelCreating(builder);
		builder.ApplyConfiguration(new CinemaFaker());

		builder.Entity<Cinema>().HasData(CinemaUtils.SpecificCinemaNames
			.Select(name => new Cinema
			{
				Id = Guid.NewGuid(),
				Name = name,
				City = _faker.Address.City(),
				Street = _faker.Address.StreetAddress(),
				TicketPrice = _faker.Finance.Amount(1M, 500M)
			})
			.ToArray()
		);
	}
}
#endif
