//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using Tests.Common.Fakers;
using Tests.Common.Utils;

namespace Tests.Common.Contexts;

public sealed class FoodInMemoryContext : DatabaseContext
{
	private readonly Faker _faker = FakerUtils.GetInstance();

	public FoodInMemoryContext(DbContextOptions options) : base(options) {}

	protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
	{
		base.OnConfiguring(optionsBuilder);
		optionsBuilder
			.UseInMemoryDatabase("FoodInMemoryTestingDatabase")
			.EnableSensitiveDataLogging()
			.EnableDetailedErrors()
			.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
	}

	protected override void OnModelCreating(ModelBuilder builder)
	{
		base.OnModelCreating(builder);
		builder.ApplyConfiguration(new FoodFaker());

		builder.Entity<Food>().HasData(FoodUtils.SpecificFoodNames
			.Select(name => new Food
			{
				Id = Guid.NewGuid(),
				Name = name,
				Price = _faker.Finance.Amount(),
				AvailableQuantity = _faker.Random.ULong(),
				Type = _faker.Random.ListItem(new List<string> { "Snack", "Drink" }),
				Size = _faker.Finance.Amount(),
				ImageUrl = _faker.Internet.Url(),
				ImageId = _faker.Random.String(10)
			})
			.ToArray()
		);
	}
}
#endif
