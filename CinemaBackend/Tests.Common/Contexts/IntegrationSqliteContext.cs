//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using Microsoft.Data.Sqlite;
using Tests.Common.Fakers;

namespace Tests.Common.Contexts;

public sealed class IntegrationSqliteContext : DatabaseContext
{
	public IntegrationSqliteContext(DbContextOptions options) : base(options) {}

	protected override void OnModelCreating(ModelBuilder builder)
	{
		base.OnModelCreating(builder);
		builder.ApplyConfiguration(new CinemaFaker());
		builder.ApplyConfiguration(new FilmRatingFaker());
		builder.ApplyConfiguration(new FoodFaker());
		builder.ApplyConfiguration(new GenreFaker());
		builder.ApplyConfiguration(new HallFaker());
		builder.ApplyConfiguration(new HallSeatFaker());
		builder.ApplyConfiguration(new MovieFaker());
		builder.ApplyConfiguration(new MovieGenreFaker());
		builder.ApplyConfiguration(new MoviePersonFaker());
		builder.ApplyConfiguration(new PersonFaker());
		builder.ApplyConfiguration(new ProjectionFaker());
		builder.ApplyConfiguration(new SeatFaker());
		builder.ApplyConfiguration(new SouvenirFaker());
	}

	protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
	{
		base.OnConfiguring(optionsBuilder);

		var connectionStringBuilder = new SqliteConnectionStringBuilder
		{
			DataSource = "file::memory:?cache=shared"
		};

		var connectionString = connectionStringBuilder.ToString();
		var connection = new SqliteConnection(connectionString);

		optionsBuilder.UseSqlite(connection, x => x.UseNodaTime());
	}
}
#endif
