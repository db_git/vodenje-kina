//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using Tests.Common.Fakers;
using Tests.Common.Utils;

namespace Tests.Common.Contexts;

public sealed class ProjectionInMemoryContext : DatabaseContext
{
	public ProjectionInMemoryContext(DbContextOptions options) : base(options) {}

	protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
	{
		base.OnConfiguring(optionsBuilder);
		optionsBuilder
			.UseInMemoryDatabase("ProjectionInMemoryTestingDatabase")
			.EnableSensitiveDataLogging()
			.EnableDetailedErrors()
			.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
	}

	protected override void OnModelCreating(ModelBuilder builder)
	{
		base.OnModelCreating(builder);
		builder.ApplyConfiguration(new CinemaFaker());
		builder.ApplyConfiguration(new HallFaker());
		builder.ApplyConfiguration(new HallSeatFaker());
		builder.ApplyConfiguration(new MovieFaker());
		builder.ApplyConfiguration(new ProjectionFaker());
		builder.ApplyConfiguration(new SeatFaker());

		var projections = new List<Projection>(2);
		for (var i = 0; i < 2; i++)
		{
			projections.Add(new Projection
			{
				Id = Guid.NewGuid(),
				HallId = HallFakeData.HallIds.Cast<Guid>().ToArray()[i],
				MovieId = MovieFakeData.MovieIds.Cast<string>().ToArray()[i],
				Time= ProjectionUtils.SpecificProjectionTimes.ToArray()[i]
			});
		}

		builder.Entity<Projection>().HasData(projections);
	}
}
#endif
