//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using Tests.Common.Fakers;
using Tests.Common.Utils;

namespace Tests.Common.Contexts;

public sealed class MovieInMemoryContext : DatabaseContext
{
	private readonly Faker _faker = FakerUtils.GetInstance();

	public MovieInMemoryContext(DbContextOptions options) : base(options) {}

	protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
	{
		base.OnConfiguring(optionsBuilder);
		optionsBuilder
			.UseInMemoryDatabase("MovieInMemoryTestingDatabase")
			.EnableSensitiveDataLogging()
			.EnableDetailedErrors()
			.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
	}

	protected override void OnModelCreating(ModelBuilder builder)
	{
		base.OnModelCreating(builder);
		builder.ApplyConfiguration(new CinemaFaker());
		builder.ApplyConfiguration(new FilmRatingFaker());
		builder.ApplyConfiguration(new GenreFaker());
		builder.ApplyConfiguration(new HallFaker());
		builder.ApplyConfiguration(new SeatFaker());
		builder.ApplyConfiguration(new HallSeatFaker());
		builder.ApplyConfiguration(new MovieFaker());
		builder.ApplyConfiguration(new MovieGenreFaker());
		builder.ApplyConfiguration(new MoviePersonFaker());
		builder.ApplyConfiguration(new PersonFaker());
		builder.ApplyConfiguration(new ProjectionFaker());

		builder.Entity<Movie>().HasData(MovieUtils.SpecificMovieTitles
			.Select(title => new Movie
			{
				Id = title + _faker.Lorem.Word(),
				Title = title,
				Year = _faker.Random.UInt(1U, 5000U),
				Duration = _faker.Random.UInt(30U, 360U),
				Tagline = _faker.Lorem.Sentence(),
				Summary = _faker.Lorem.Paragraphs(2),
				InCinemas = _faker.Random.Bool(),
				PosterUrl = _faker.Internet.Url(),
				PosterId = _faker.Random.String(10),
				BackdropUrl = _faker.Internet.Url(),
				BackdropId = _faker.Random.String(10),
				TrailerUrl = _faker.Internet.Url()
			})
			.ToArray()
		);

		var genreId = Guid.NewGuid();
		var movieId = MovieUtils.SpecificMovieTitles[0] + _faker.Lorem.Sentence(5);
		builder.Entity<Genre>().HasData(new Genre { Id = genreId, Name = "War" });
		builder.Entity<MovieGenre>().HasData(new MovieGenre { GenreId = genreId, MovieId = movieId });
		builder.Entity<Movie>().HasData(new Movie
		{
			Id = movieId,
			Title = _faker.Lorem.Sentence(),
			Year = _faker.Random.UInt(1U, 5000U),
			Duration = _faker.Random.UInt(30U, 360U),
			Tagline = _faker.Lorem.Sentence(),
			Summary = _faker.Lorem.Paragraphs(2),
			InCinemas = _faker.Random.Bool(),
			PosterUrl = _faker.Internet.Url(),
			PosterId = _faker.Random.String(10),
			BackdropUrl = _faker.Internet.Url(),
			BackdropId = _faker.Random.String(10),
			TrailerUrl = _faker.Internet.Url(),
			FilmRatingId = FilmRatingFakeData.FilmRatingIds.Cast<Guid>().First()
		});
	}
}
#endif
