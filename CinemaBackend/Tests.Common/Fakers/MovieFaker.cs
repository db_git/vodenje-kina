//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Tests.Common.Fakers;

public sealed class MovieFaker : IEntityTypeConfiguration<Movie>
{
	public void Configure(EntityTypeBuilder<Movie> builder)
	{
		MovieFakeData.Initialize();
		builder.HasData(MovieFakeData.Movies);
	}
}

public static class MovieFakeData
{
	public static IEnumerable<Movie> Movies { get; private set; } = null!;
	public static readonly ArrayList MovieIds = new() { "random-movie-12", "yet-another-movie-movie-3456" };
	private static readonly ArrayList _filmRatingIds = FilmRatingFakeData.FilmRatingIds;

	public static void Initialize()
	{
		var movieIdIndex = 0;
		var movieFaker = new Faker<Movie>()
			.RuleFor(movie => movie.Id, _ => MovieIds[movieIdIndex++])
			.RuleFor(movie => movie.Title, faker => faker.Lorem.Word())
			.RuleFor(movie => movie.Year, faker => faker.Random.UInt(1U, 5000U))
			.RuleFor(movie => movie.Duration, faker => faker.Random.UInt(30U, 360U))
			.RuleFor(movie => movie.Tagline, faker => faker.Lorem.Sentence())
			.RuleFor(movie => movie.Summary, faker => faker.Lorem.Paragraphs(2))
			.RuleFor(movie => movie.InCinemas, faker => faker.Random.Bool())
			.RuleFor(movie => movie.PosterUrl, faker => faker.Internet.Url())
			.RuleFor(movie => movie.PosterId, faker => faker.Random.String(10))
			.RuleFor(movie => movie.BackdropUrl, faker => faker.Internet.Url())
			.RuleFor(movie => movie.BackdropId, faker => faker.Random.String(10))
			.RuleFor(movie => movie.TrailerUrl, faker => faker.Internet.Url())
			.RuleFor(movie => movie.FilmRatingId,
				faker => faker.Random.ListItem(_filmRatingIds.Cast<Guid>().ToArray())
			);

		Movies = movieFaker.Generate(MovieIds.Count);
	}
}
#endif
