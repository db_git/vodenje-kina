//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Tests.Common.Fakers;

public sealed class FoodFaker : IEntityTypeConfiguration<Food>
{
	public void Configure(EntityTypeBuilder<Food> builder)
	{
		FoodFakeData.Initialize();
		builder.HasData(FoodFakeData.Foods);
	}
}

public static class FoodFakeData
{
	public static IEnumerable<Food> Foods { get; private set; } = null!;

	public static readonly ArrayList FoodIds = new()
	{
		Guid.Parse("5d025c26-e099-47d7-be88-26439659fb27"),
		Guid.Parse("53709ac8-1a82-4913-9441-1623b9eff426")
	};

	public static void Initialize()
	{
		var foodIdIndex = 0;
		var foodFaker = new Faker<Food>()
			.RuleFor(food => food.Id, _ => FoodIds[foodIdIndex++])
			.RuleFor(food => food.Name, faker => faker.Lorem.Word())
			.RuleFor(food => food.Price, faker => faker.Finance.Amount())
			.RuleFor(food => food.AvailableQuantity, faker => faker.Random.ULong())
			.RuleFor(food => food.Type,
				faker => faker.Random.ListItem(new List<string> { "Snack", "Drink" }))
			.RuleFor(food => food.Size, faker => faker.Finance.Amount())
			.RuleFor(food => food.ImageUrl, faker => faker.Internet.Url())
			.RuleFor(food => food.ImageId, faker => faker.Random.String(10));

		Foods = foodFaker.Generate(FoodIds.Count);
	}
}
#endif
