//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Tests.Common.Fakers;

public sealed class HallFaker : IEntityTypeConfiguration<Hall>
{
	public void Configure(EntityTypeBuilder<Hall> builder)
	{
		HallFakeData.Initialize();
		builder.HasData(HallFakeData.Halls);
	}
}

public static class HallFakeData
{
	public static IEnumerable<Hall> Halls { get; private set; } = null!;
	private static readonly ArrayList _cinemaIds = CinemaFakeData.CinemaIds;

	public static readonly ArrayList HallIds = new()
	{
		Guid.Parse("ac9b47a8-17d7-40ec-9072-60162cf54404"),
		Guid.Parse("db896936-967b-4484-809d-846a9f22ae81")
	};

	public static void Initialize()
	{
		var hallIdIndex = 0;
		var hallFaker = new Faker<Hall>()
			.RuleFor(hall => hall.Id, _ => HallIds[hallIdIndex])
			.RuleFor(hall => hall.Name, faker => faker.Lorem.Word())
			.RuleFor(hall => hall.CinemaId, _ => _cinemaIds[hallIdIndex++]);

		Halls = hallFaker.Generate(HallIds.Count);
	}
}
#endif
