//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Tests.Common.Fakers;

public sealed class SouvenirFaker : IEntityTypeConfiguration<Souvenir>
{
	public void Configure(EntityTypeBuilder<Souvenir> builder)
	{
		SouvenirFakeData.Initialize();
		builder.HasData(SouvenirFakeData.Souvenirs);
	}
}

public static class SouvenirFakeData
{
	public static IEnumerable<Souvenir> Souvenirs { get; private set; } = null!;
	public static readonly ArrayList SouvenirIds = new()
	{
		Guid.Parse("fb54a8f4-e07a-49fd-890f-3f4fc50bc7d2"),
		Guid.Parse("e94ea6d2-c20e-4e9d-a59b-6a82b8b3e895")
	};

	public static void Initialize()
	{
		var souvenirIdIndex = 0;
		var souvenirFaker = new Faker<Souvenir>()
			.RuleFor(souvenir => souvenir.Id, _ => SouvenirIds[souvenirIdIndex++])
			.RuleFor(souvenir => souvenir.Name, faker => faker.Lorem.Word())
			.RuleFor(souvenir => souvenir.Price, faker => faker.Finance.Amount(1M, 500M))
			.RuleFor(souvenir => souvenir.AvailableQuantity, faker => faker.Random.ULong())
			.RuleFor(souvenir => souvenir.ImageUrl, faker => faker.Internet.Url())
			.RuleFor(souvenir => souvenir.ImageId, faker => faker.Random.String(10));

		Souvenirs = souvenirFaker.Generate(SouvenirIds.Count);
	}
}
#endif
