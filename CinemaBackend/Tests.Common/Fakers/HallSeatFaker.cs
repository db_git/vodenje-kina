//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Tests.Common.Fakers;

public sealed class HallSeatFaker : IEntityTypeConfiguration<HallSeat>
{
	public void Configure(EntityTypeBuilder<HallSeat> builder)
	{
		HallSeatFakeData.Initialize();
		builder.HasData(HallSeatFakeData.HallSeats);
	}
}

public static class HallSeatFakeData
{
	public static IEnumerable<HallSeat> HallSeats { get; private set; } = null!;
	private static readonly ArrayList _hallIds = HallFakeData.HallIds;
	private static readonly ArrayList _seatIds = SeatFakeData.SeatIds;

	public static void Initialize()
	{
		var hallISeatdIndex = 0;
		var hallFaker = new Faker<HallSeat>()
			.RuleFor(hallSeat => hallSeat.HallId, _ => _hallIds[hallISeatdIndex])
			.RuleFor(hallSeat => hallSeat.SeatId, _ => _seatIds[hallISeatdIndex++]);

		HallSeats = hallFaker.Generate(_hallIds.Count);
	}
}
#endif
