//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Tests.Common.Fakers;

public sealed class MoviePersonFaker : IEntityTypeConfiguration<MoviePerson>
{
	public void Configure(EntityTypeBuilder<MoviePerson> builder)
	{
		MoviePersonFakeData.Initialize();
		builder.HasData(MoviePersonFakeData.MoviePeople);
	}
}

public static class MoviePersonFakeData
{
	public static IEnumerable<MoviePerson> MoviePeople { get; private set; } = null!;
	private static readonly ArrayList _movieIds = MovieFakeData.MovieIds;
	private static readonly ArrayList _peopleIds = PersonFakeData.PeopleIds;

	public static void Initialize()
	{
		var moviePersonIdIndex = 0;
		var moviePersonFaker = new Faker<MoviePerson>()
			.RuleFor(moviePerson => moviePerson.Id, _ => Guid.NewGuid())
			.RuleFor(moviePerson => moviePerson.MovieId, _ => _movieIds[moviePersonIdIndex])
			.RuleFor(moviePerson => moviePerson.PersonId, _ => _peopleIds[moviePersonIdIndex++])
			.RuleFor(moviePerson => moviePerson.Character, faker => faker.Person.FirstName)
			.RuleFor(moviePerson => moviePerson.Order, faker => faker.Random.UInt())
			.RuleFor(moviePerson => moviePerson.Type,
				faker => faker.Random.ListItem(new List<string> { "Actor", "Director" }));

		MoviePeople = moviePersonFaker.Generate(_movieIds.Count);
	}
}
#endif
