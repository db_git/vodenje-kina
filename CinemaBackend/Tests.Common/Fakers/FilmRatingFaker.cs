//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Tests.Common.Fakers;

public sealed class FilmRatingFaker : IEntityTypeConfiguration<FilmRating>
{
	public void Configure(EntityTypeBuilder<FilmRating> builder)
	{
		FilmRatingFakeData.Initialize();
		builder.HasData(FilmRatingFakeData.FilmRatings);
	}
}

public static class FilmRatingFakeData
{
	public static IEnumerable<FilmRating> FilmRatings { get; private set; } = null!;

	public static readonly ArrayList FilmRatingIds = new()
	{
		Guid.Parse("747d7052-4738-4c80-b77b-2ebdce14a435"),
		Guid.Parse("ba992849-8ce2-4b41-a598-15c5359516e7")
	};

	public static void Initialize()
	{
		var filmRatingIdIndex = 0;
		var filmRatingFaker = new Faker<FilmRating>()
			.RuleFor(filmRating => filmRating.Id, _ => FilmRatingIds[filmRatingIdIndex++])
			.RuleFor(filmRating => filmRating.Type, faker => faker.Lorem.Word())
			.RuleFor(filmRating => filmRating.Description, faker => faker.Lorem.Sentence(3));

		FilmRatings = filmRatingFaker.Generate(FilmRatingIds.Count);
	}
}
#endif
