//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Tests.Common.Fakers;

public sealed class GenreFaker : IEntityTypeConfiguration<Genre>
{
	public void Configure(EntityTypeBuilder<Genre> builder)
	{
		GenreFakeData.Initialize();
		builder.HasData(GenreFakeData.Genres);
	}
}

public static class GenreFakeData
{
	public static IEnumerable<Genre> Genres { get; private set; } = null!;

	public static readonly ArrayList GenreIds = new()
	{
		Guid.Parse("13b8f998-3fb2-4982-a7a0-4c120cb6cd08"),
		Guid.Parse("fcec8348-faea-4c02-bc87-dc77b8b6d870")
	};

	public static void Initialize()
	{
		var genreIdIndex = 0;
		var genreFaker = new Faker<Genre>()
			.RuleFor(genre => genre.Id, _ => GenreIds[genreIdIndex++])
			.RuleFor(genre => genre.Name, faker => faker.Lorem.Word());

		Genres = genreFaker.Generate(GenreIds.Count);
	}
}
#endif
