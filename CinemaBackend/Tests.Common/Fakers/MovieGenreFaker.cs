//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Tests.Common.Fakers;

public sealed class MovieGenreFaker : IEntityTypeConfiguration<MovieGenre>
{
	public void Configure(EntityTypeBuilder<MovieGenre> builder)
	{
		MovieGenreFakeData.Initialize();
		builder.HasData(MovieGenreFakeData.MovieGenres);
	}
}

public static class MovieGenreFakeData
{
	public static IEnumerable<MovieGenre> MovieGenres { get; private set; } = null!;
	private static readonly ArrayList _movieIds = MovieFakeData.MovieIds;
	private static readonly ArrayList _genreIds = GenreFakeData.GenreIds;

	public static void Initialize()
	{
		var movieGenreIdIndex = 0;
		var movieGenreFaker = new Faker<MovieGenre>()
			.RuleFor(movieGenre => movieGenre.MovieId, _ => _movieIds[movieGenreIdIndex])
			.RuleFor(movieGenre => movieGenre.GenreId, _ => _genreIds[movieGenreIdIndex++]);

		MovieGenres = movieGenreFaker.Generate(_movieIds.Count);
	}
}
#endif
