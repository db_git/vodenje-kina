//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using NodaTime;

namespace Tests.Common.Fakers;

public sealed class ProjectionFaker : IEntityTypeConfiguration<Projection>
{
	public void Configure(EntityTypeBuilder<Projection> builder)
	{
		ProjectionFakeData.Initialize();
		builder.HasData(ProjectionFakeData.Projections);
	}
}

public static class ProjectionFakeData
{
	public static IEnumerable<Projection> Projections { get; private set; } = null!;
	private static readonly ArrayList _hallIds = HallFakeData.HallIds;
	private static readonly ArrayList _movieIds = MovieFakeData.MovieIds;

	public static readonly ArrayList ProjectionIds = new()
	{
		Guid.Parse("244bd12e-7666-4093-bdf7-cb84742eeb9b"),
		Guid.Parse("16c2c1d7-2e14-402f-8654-edf493b6af36")
	};

	public static void Initialize()
	{
		var projectionIdIndex = 0;
		var projectionFaker = new Faker<Projection>()
			.RuleFor(projection => projection.Id, _ => ProjectionIds[projectionIdIndex])
			.RuleFor(projection => projection.HallId,
				_ => _hallIds.Cast<Guid>().ToArray()[projectionIdIndex])
			.RuleFor(projection => projection.MovieId,
				_ => _movieIds.Cast<string>().ToArray()[projectionIdIndex++])
			.RuleFor(projection => projection.Time,
				faker => LocalDateTime.FromDateTime(faker.Date.Soon()));

		Projections = projectionFaker.Generate(ProjectionIds.Count);
	}
}
#endif
