//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Tests.Common.Fakers;

public sealed class CinemaFaker : IEntityTypeConfiguration<Cinema>
{
	public void Configure(EntityTypeBuilder<Cinema> builder)
	{
		CinemaFakeData.Initialize();
		builder.HasData(CinemaFakeData.Cinemas);
	}
}

public static class CinemaFakeData
{
	public static IEnumerable<Cinema> Cinemas { get; private set; } = null!;

	public static readonly ArrayList CinemaIds = new()
	{
		Guid.Parse("841a06bf-7f8d-4112-b032-60dab0a9f9b4"),
		Guid.Parse("79ee60a7-2d69-4e86-b467-73b1459b8a24")
	};

	public static void Initialize()
	{
		var cinemaIdIndex = 0;
		var cinemaFaker = new Faker<Cinema>()
			.RuleFor(cinema => cinema.Id, _ => CinemaIds[cinemaIdIndex++])
			.RuleFor(cinema => cinema.Name, faker => faker.Lorem.Word())
			.RuleFor(cinema => cinema.City, faker => faker.Address.City())
			.RuleFor(cinema => cinema.Street, faker => faker.Address.StreetAddress())
			.RuleFor(cinema => cinema.TicketPrice, faker => faker.Finance.Amount(1M, 500M));

		Cinemas = cinemaFaker.Generate(CinemaIds.Count);
	}
}
#endif
