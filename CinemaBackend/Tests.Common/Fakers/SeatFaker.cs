//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Tests.Common.Fakers;

public sealed class SeatFaker : IEntityTypeConfiguration<Seat>
{
	public void Configure(EntityTypeBuilder<Seat> builder)
	{
		SeatFakeData.Initialize();
		builder.HasData(SeatFakeData.Seats);
	}
}

public static class SeatFakeData
{
	public static IEnumerable<Seat> Seats { get; private set; } = null!;
	public static readonly ArrayList SeatIds = new()
	{
		Guid.Parse("e3b9f390-2413-430f-8997-d6f4bc1f57d0"),
		Guid.Parse("0cd4eeb3-2a15-4317-87d5-6ad5ca8f90a1")
	};

	public static void Initialize()
	{
		var seatIdIndex = 0;
		var seatFaker = new Faker<Seat>()
			.RuleFor(seat => seat.Id, _ => SeatIds[seatIdIndex++])
			.RuleFor(seat => seat.Number, faker => faker.Random.UInt(1U, 100U));

		Seats = seatFaker.Generate(SeatIds.Count);
	}
}
#endif
