//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using NodaTime;
using Person = Domain.Entities.Person;

namespace Tests.Common.Fakers;

public sealed class PersonFaker : IEntityTypeConfiguration<Person>
{
	public void Configure(EntityTypeBuilder<Person> builder)
	{
		PersonFakeData.Initialize();
		builder.HasData(PersonFakeData.People);
	}
}

public static class PersonFakeData
{
	public static IEnumerable<Person> People { get; private set; } = null!;
	public static readonly ArrayList PeopleIds = new() { "john-doe-123", "jane-doe-456" };

	public static void Initialize()
	{
		var personIdIndex = 0;
		var personFaker = new Faker<Person>()
			.RuleFor(person => person.Id, _ => PeopleIds[personIdIndex++])
			.RuleFor(person => person.Name, faker => faker.Person.FullName)
			.RuleFor(person => person.DateOfBirth,
				faker => LocalDate.FromDateTime(faker.Date.Past().ToUniversalTime()))
			.RuleFor(person => person.DateOfDeath,
				faker => LocalDate.FromDateTime(faker.Date.Soon().ToUniversalTime()))
			.RuleFor(person => person.ImageUrl, faker => faker.Internet.Url())
			.RuleFor(person => person.ImageId, faker => faker.Random.String(10))
			.RuleFor(person => person.Biography, faker => faker.Lorem.Paragraphs());

		var people = personFaker.Generate(PeopleIds.Count);

		people.Add(new Person
		{
			Id = "unknown-999",
			Name = "Name",
			DateOfBirth = new LocalDate(2022, 12, 31),
			ImageUrl = "https://",
			ImageId = "wasd123",
			Biography = "Biography."
		});

		People = people;
	}
}
#endif
