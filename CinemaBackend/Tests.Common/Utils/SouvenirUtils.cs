//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Tests.Common.Utils;

public static class SouvenirUtils
{
	private static readonly Faker _faker = FakerUtils.GetInstance();

	public static readonly string[] RandomSouvenirNames =
	{
		_faker.Lorem.Sentence(1),
		_faker.Lorem.Sentence(1),
		_faker.Lorem.Sentence(1),
		_faker.Lorem.Sentence(1)
	};

	public static readonly string[] SpecificSouvenirNames =
	{
		"Toy123",
		"MovieToy3456"
	};
}
#endif
