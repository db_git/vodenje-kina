//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Tests.Common.Utils;

public static class MapperConfig
{
	public static MapperConfiguration GetConfiguration()
	{
		return new MapperConfiguration(cfg =>
		{
			cfg.AddProfile<CinemaMapper>();
			cfg.AddProfile<FilmRatingMapper>();
			cfg.AddProfile<FoodMapper>();
			cfg.AddProfile<GenreMapper>();
			cfg.AddProfile<HallMapper>();
			cfg.AddProfile<MovieMapper>();
			cfg.AddProfile<PersonMapper>();
			cfg.AddProfile<ProjectionMapper>();
			cfg.AddProfile<ReservationMapper>();
			cfg.AddProfile<SeatMapper>();
			cfg.AddProfile<SouvenirMapper>();
			cfg.AddProfile<UserMapper>();
			cfg.AddProfile<UserCommentMapper>();
			cfg.AddProfile<UserRatingMapper>();
		});
	}
}
#endif
