//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
using NodaTime;

namespace Tests.Common.Utils;

public static class ProjectionUtils
{
	public static readonly LocalDateTime[] RandomProjectionTimes =
	{
		new(2222, 9, 9, 9, 9),
		new(3333, 3, 3, 3, 3),
	};

	public static readonly LocalDateTime[] SpecificProjectionTimes =
	{
		new(2022, 12, 31, 1, 15),
		new(2022, 11, 30, 2, 30)
	};
}
#endif
