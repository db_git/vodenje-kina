//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#if DEBUG
namespace Tests.Common.Utils;

public static class GuidUtils
{
	public static readonly ArrayList NonexistentGuids = new()
	{
		Guid.Parse("26a3cce7-04d4-4019-9423-57e36fea07ab"),
		Guid.Parse("123e4567-e89b-12d3-a456-426614174000"),
		Guid.Parse("123d5678-b12b-45c6-0000-123456789012"),
		Guid.Parse("93945629-734b-475e-99ce-6aa7afa43259"),
		Guid.Empty
	};
}
#endif
