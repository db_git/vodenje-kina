//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

/** @type {import("tailwindcss").Config} */

const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
	content: ["./index.html", "./src/**/*.{ts,tsx}"],
	theme: {
		screens: {
			xs: "475px",
			...defaultTheme.screens
		},
		extend: {
			height: {
				"screen/2": "calc(100vh / 2)",
				"screen/3": "calc(100vh / 3)",
				"screen/4": "calc(100vh / 4)",
				"screen/5": "calc(100vh / 5)"
			},
			keyframes: {
				"hide-navbar-keyframe": {
					"0%": { transform: "translateY(0)" },
					"100%": { transform: "translateY(-65px)" }
				},
				"show-navbar-keyframe": {
					"0%": { transform: "translateY(-65px)" },
					"100%": { transform: "translateY(0)" }
				}
			},
			animation: {
				"hide-navbar": "hide-navbar-keyframe 0.85s forwards",
				"show-navbar": "show-navbar-keyframe 0.25s forwards"
			},
			fontFamily: {
				sans: ["Work Sans", ...defaultTheme.fontFamily.sans]
			}
		}
	},
	plugins: []
};
