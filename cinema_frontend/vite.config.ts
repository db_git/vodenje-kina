//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { resolve } from "path";
import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import svgrPlugin from "vite-plugin-svgr";
import svg from "vite-plugin-svgo";
import checker from "vite-plugin-checker";
import ViteMinifyPlugin from "vite-plugin-minify";

export default defineConfig({
	build: {
		minify: "esbuild",
		target: "esnext",
		outDir: "build",
		rollupOptions: {
			input: {
				index: resolve(__dirname, "index.html")
			},
			output: {
				manualChunks(id) {
					if (id.includes("node_modules")) {
						return id.toString().split("node_modules/")[1].split("/")[0].toString();
					}
				}
			}
		}
	},
	server: {
		open: false,
		port: 3000,
		proxy: {
			"/api/": {
				target: "http://localhost:5000",
				changeOrigin: true,
				rewrite: (path) => path.replace("^/api/", ""),
				secure: false
			}
		}
	},
	plugins: [
		react(),
		checker({ typescript: true, eslint: { lintCommand: "eslint . --ext .ts,.tsx" } }),
		svg({
			multipass: true,
			plugins: [
				{
					name: "preset-default"
				}
			]
		}),
		svgrPlugin({
			svgrOptions: {
				icon: true
			}
		}),
		ViteMinifyPlugin({})
	]
});
