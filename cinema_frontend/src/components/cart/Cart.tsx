//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Indicator, useMantineColorScheme } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { MdOutlineShoppingCart } from "react-icons/md";
import { Link } from "react-router-dom";

import useCartStore from "../../hooks/useCartStore";
import useNavbarStore from "../../hooks/useNavbarStore";
import { Routes } from "../../routes/Routes";

const indicatorStyle = { indicator: "select-none" };

const Cart = () => {
	const cartStore = useCartStore();
	const navbarStore = useNavbarStore();
	const { colorScheme } = useMantineColorScheme();

	const buttonColor = colorScheme === "dark" ? "blue" : "cyan";
	const buttonVariant = navbarStore.active.includes(Routes.Checkout) ? "filled" : "light";

	return (
		<Indicator
			classNames={indicatorStyle}
			color="indigo"
			inline
			label={cartStore.itemsCount}
			mr={7}
			mt={3}
			size={16}
		>
			<Button
				color={buttonColor}
				compact
				component={Link}
				px="1rem"
				radius={10}
				size="md"
				to={Routes.Checkout}
				variant={buttonVariant}
			>
				<MdOutlineShoppingCart size={20} />
			</Button>
		</Indicator>
	);
};

export default observer(Cart);
