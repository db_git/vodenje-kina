//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { useForm, zodResolver } from "@mantine/form";
import { observer } from "mobx-react-lite";

import { type FormProps } from "./forms";

const Form = <T extends Record<string, unknown>>({
	children,
	initialValues,
	onSubmit,
	onReset,
	schema,
	validateInputOnBlur,
	validateInputOnChange,
	...props
}: FormProps<T>) => {
	const form = useForm({
		validate: zodResolver(schema),
		validateInputOnBlur,
		validateInputOnChange,
		initialValues
	});

	return (
		<form
			onReset={onReset}
			onSubmit={form.onSubmit((values) => {
				onSubmit(values);
			})}
			{...props}
		>
			{children(form)}
		</form>
	);
};

Form.defaultProps = {
	noValidate: true,
	validateInputOnBlur: true,
	validateInputOnChange: true
};

const ObserverForm = observer(Form);

export default Form;
export { ObserverForm };
