//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Group, Input, Tabs, Text, TextInput, ThemeIcon, useMantineTheme } from "@mantine/core";
import { Dropzone, IMAGE_MIME_TYPE } from "@mantine/dropzone";
import { memo } from "react";
import { MdHttp, MdImage } from "react-icons/md";

import ImagePreview from "./ImagePreview";
import { type ImageInputProps } from "./forms";

const fileTab = "file";
const urlTab = "url";

const ImageInput = ({
	label,
	placeholder,
	required,
	uploadedMessage,
	uploadMessage,
	disabled,
	image,
	removeImage,
	onDrop,
	isTouched,
	...props
}: ImageInputProps) => {
	const theme = useMantineTheme();

	const errorColor = theme.colorScheme === "dark" ? theme.colors.red[8] : theme.colors.red[6];
	const borderColor = theme.colorScheme === "dark" ? theme.colors.gray[7] : theme.colors.gray[4];
	const textColor = props.error ? errorColor : "";

	const dropzoneStyle = {
		borderStyle: "dashed",
		borderWidth: "1.5px",
		borderColor: props.error ? errorColor : borderColor
	};

	const fileDisabled = isTouched && !props.error && typeof image === "string";
	const urlDisabled = isTouched && !props.error && image instanceof Blob;

	return (
		<>
			{image && !props.error && (
				<ImagePreview
					disabled={disabled}
					image={image}
					removeImage={removeImage}
					uploadedMessage={uploadedMessage}
				/>
			)}
			<Tabs color="teal" defaultValue={urlTab} mt="2rem">
				<Tabs.List grow position="center">
					<Tabs.Tab
						disabled={fileDisabled}
						icon={
							<ThemeIcon color="teal" radius="xl" size="lg" variant="light">
								<MdImage size={16} />
							</ThemeIcon>
						}
						value="file"
					>
						<Text size="sm">Upload image</Text>
					</Tabs.Tab>
					<Tabs.Tab
						disabled={urlDisabled}
						icon={
							<ThemeIcon color="teal" radius="xl" size="lg" variant="light">
								<MdHttp size={16} />
							</ThemeIcon>
						}
						value="url"
					>
						<Text size="sm">Enter URL</Text>
					</Tabs.Tab>
				</Tabs.List>

				<Tabs.Panel value={fileTab}>
					<Input.Wrapper {...props} label={label} required={required}>
						<Dropzone
							accept={IMAGE_MIME_TYPE}
							className={disabled ? "cursor-not-allowed" : ""}
							disabled={disabled}
							mb={6}
							multiple={false}
							onDrop={onDrop}
							sx={dropzoneStyle}
						>
							<Group position="center" spacing="xl">
								<Text color={textColor} size="md" weight={500}>
									{uploadMessage}
								</Text>
							</Group>
						</Dropzone>
					</Input.Wrapper>
				</Tabs.Panel>
				<Tabs.Panel value={urlTab}>
					<TextInput
						icon={<MdHttp />}
						mt="1rem"
						required
						{...props}
						disabled={disabled}
						label={`${label} URL`}
						placeholder={placeholder}
					/>
				</Tabs.Panel>
			</Tabs>
		</>
	);
};

export default memo(ImageInput);
