//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Group, Image, Modal, Stack, Title } from "@mantine/core";
import { memo, useCallback, useState } from "react";

import { type ImagePreviewProps } from "./forms";

const mantineCursorPointer = { root: "cursor-pointer" };
const mantineImageStyle = { image: "max-h-screen w-full" };

const ImagePreview = ({ image, uploadedMessage, removeImage, disabled }: ImagePreviewProps) => {
	const [opened, setOpened] = useState(false);

	const toggleModal = useCallback((): void => {
		setOpened(!opened);
	}, [opened]);

	if (!image) return null;

	const previewImage: string = image instanceof Blob ? URL.createObjectURL(image) : image;

	return (
		<>
			<Modal centered onClose={toggleModal} opened={opened} size="auto">
				<Image classNames={mantineImageStyle} fit="contain" src={previewImage} />
			</Modal>

			<Stack align="center" mt={10}>
				<Title order={5}>{uploadedMessage}</Title>
				<Group position="center" spacing="xs">
					<Button
						classNames={mantineCursorPointer}
						color="green"
						compact
						disabled={disabled}
						onClick={toggleModal}
						variant="light"
					>
						View
					</Button>
					{removeImage && (
						<Button
							classNames={mantineCursorPointer}
							color="red"
							compact
							disabled={disabled}
							onClick={removeImage}
							variant="light"
						>
							Remove
						</Button>
					)}
				</Group>
			</Stack>
		</>
	);
};

export default memo(ImagePreview);
