//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Group, Image, Skeleton, Text } from "@mantine/core";
import { forwardRef, memo } from "react";

import { type SelectItemProps } from "./forms";

const SelectItem = forwardRef<HTMLDivElement, SelectItemProps>(
	({ label, pictureUrl, ...others }: SelectItemProps, ref) => {
		return (
			<div ref={ref} {...others}>
				<Group noWrap>
					<Image
						height="4rem"
						placeholder={<Skeleton height="4rem" radius={20} width="4rem" />}
						radius={20}
						src={pictureUrl}
						width="4rem"
						withPlaceholder
					/>

					<div>
						<Text size="sm">{label}</Text>
					</div>
				</Group>
			</div>
		);
	}
);

export default memo(SelectItem);
