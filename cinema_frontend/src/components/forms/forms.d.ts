//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type InputWrapperProps } from "@mantine/core";
import { type FileWithPath } from "@mantine/dropzone";
import { type UseFormReturnType } from "@mantine/form";
import { type ReactNode, type ComponentPropsWithoutRef } from "react";
import { type ZodTypeAny } from "zod";

export type ImagePreviewProps = {
	readonly disabled?: boolean;
	readonly uploadedMessage: string;
	readonly image: Blob | string | null;
	readonly removeImage?: () => void;
};

export type ImageInputProps = ImagePreviewProps &
	Omit<InputWrapperProps, "children" | "label" | "onDrop" | "required"> & {
		readonly uploadMessage: string;
		readonly label: string;
		readonly placeholder?: string;
		readonly required?: boolean;
		readonly isTouched: boolean;
		readonly onDrop: (files: FileWithPath[]) => void;
	};

export type SelectItemProps = ComponentPropsWithoutRef<"div"> & {
	readonly label: string;
	readonly pictureUrl: string;
};

export type FormProps<T extends Record<string, unknown>> = Omit<
	ComponentPropsWithoutRef<"form">,
	"children" | "onReset" | "onSubmit"
> & {
	readonly onSubmit: (values: T) => void;
	readonly schema: ZodTypeAny;
	readonly children: (form: UseFormReturnType<T, (values: T) => T>) => ReactNode;
	readonly initialValues: T;
	readonly validateInputOnBlur?: boolean;
	readonly validateInputOnChange?: boolean;
	readonly onReset?: () => void;
};

export type UseFormProps<T> = {
	readonly form: UseFormReturnType<T, (values: T) => T>;
};
