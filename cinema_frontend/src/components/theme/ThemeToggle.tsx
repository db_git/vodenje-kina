//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, useMantineColorScheme } from "@mantine/core";
import React from "react";
import { BiMoon, BiSun } from "react-icons/bi";

import { type ThemeToggleProps } from "./theme";

const ThemeToggle = ({
	toggleDarkIcon,
	toggleLightIcon,
	margin,
	marginTop,
	marginBottom,
	marginRight,
	marginLeft,
	radius,
	variant
}: ThemeToggleProps) => {
	const { colorScheme, toggleColorScheme } = useMantineColorScheme();
	const dark = colorScheme === "dark";

	return (
		<Button
			color={dark ? "orange" : "dark"}
			compact
			m={margin}
			mb={marginBottom}
			ml={marginLeft}
			mr={marginRight}
			mt={marginTop}
			onClick={() => {
				toggleColorScheme();
			}}
			px="1rem"
			radius={radius}
			size="md"
			title="Toggle color scheme"
			variant={variant}
		>
			{dark ? toggleLightIcon : toggleDarkIcon}
		</Button>
	);
};

ThemeToggle.defaultProps = {
	radius: 10,
	toggleDarkIcon: <BiMoon />,
	toggleLightIcon: <BiSun />,
	variant: "light"
};

export default ThemeToggle;
