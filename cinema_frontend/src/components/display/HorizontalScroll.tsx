//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Carousel } from "@mantine/carousel";
import { memo } from "react";

import { type HorizontalScrollItemProps, type HorizontalScrollProps } from "./display";

const carouselStyle = {
	control: {
		height: "1.75rem",
		width: "1.75rem",
		"&[data-inactive]": {
			opacity: 0,
			cursor: "default"
		}
	},
	indicator: {
		height: "1rem",
		width: "1rem"
	}
};

const HorizontalScrollItem = memo(({ children }: HorizontalScrollItemProps) => {
	return <div className="mr-4 flex items-stretch pb-4">{children}</div>;
});

const HorizontalScrollSlide = memo(({ children }: HorizontalScrollItemProps) => {
	return <Carousel.Slide>{children}</Carousel.Slide>;
});

const HorizontalScroll = ({
	id,
	loop,
	slideSize,
	slidesToScroll,
	withControls,
	children,
	margin,
	marginTop,
	marginRight,
	marginBottom,
	marginLeft
}: HorizontalScrollProps) => {
	return (
		<Carousel
			align="start"
			draggable
			id={id}
			initialSlide={0}
			loop={loop}
			m={margin}
			mb={marginBottom}
			ml={marginLeft}
			mr={marginRight}
			mt={marginTop}
			skipSnaps
			slideGap="md"
			slideSize={slideSize ?? "100%"}
			slidesToScroll={slidesToScroll ?? "auto"}
			styles={carouselStyle}
			withControls={withControls}
			withIndicators={false}
		>
			{children}
		</Carousel>
	);
};

HorizontalScroll.defaultProps = {
	withControls: false
};

HorizontalScroll.Item = HorizontalScrollItem;
HorizontalScroll.Slide = HorizontalScrollSlide;

export default HorizontalScroll;
