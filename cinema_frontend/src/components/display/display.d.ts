//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import {
	type OptionalMarginProps,
	type OptionalClassNameProps,
	type ChildrenProps,
	type OptionalIdProps
} from "../../types/shared";

export type HorizontalScrollProps = ChildrenProps &
	OptionalIdProps &
	OptionalMarginProps & {
		readonly loop?: boolean;
		readonly slideSize?: number | string;
		readonly slidesToScroll?: number;
		readonly withControls?: boolean;
	};

export type HorizontalScrollItemProps = ChildrenProps;

export type HeadingProps = ChildrenProps &
	OptionalClassNameProps & {
		readonly anchor?: string;
		readonly icon?: boolean;
		readonly size?: 0 | 1 | 2 | 3 | 4 | 5 | 6;
	};
