//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Text, Title } from "@mantine/core";
import classNames from "classnames";
import { memo } from "react";

import { type HeadingProps } from "./display";

const Heading = ({ children, anchor, icon, size, className }: HeadingProps) => {
	const linkStyle = classNames("inline-block text-inherit no-underline", className);
	const titleStyle = classNames({
		"text-4xl md:text-5xl": size === 0,
		"text-3xl md:text-4xl": size === 1,
		"text-2xl md:text-3xl": size === 2,
		"text-xl md:text-2xl": size === 3,
		"text-lg md:text-xl": size === 4,
		"text-base md:text-lg": size === 5,
		"text-sm md:text-base": size === 6
	});

	return (
		<a className={linkStyle} href={anchor}>
			<Title className={titleStyle}>
				{icon && (
					<Text color="orange" inherit span>
						|&nbsp;
					</Text>
				)}
				{children}
			</Title>
		</a>
	);
};

Heading.defaultProps = {
	icon: true,
	size: 0
};

export default memo(Heading);
