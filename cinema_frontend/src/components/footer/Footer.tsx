//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import {
	ActionIcon,
	Anchor,
	AspectRatio,
	Container,
	createStyles,
	Divider,
	Flex,
	Group,
	Image,
	Title
} from "@mantine/core";
import { FaGitlab } from "react-icons/fa";

import FeritIcon from "../../assets/images/ferit-icon.png";
import FeritLogo from "../../assets/images/ferit-logo.png";

// https://ui.mantine.dev/category/footers

const useStyles = createStyles((theme) => {
	return {
		outer: {
			position: "relative",
			left: 0,
			bottom: 0,
			width: "100%",
			backgroundColor: theme.colorScheme === "dark" ? theme.colors.dark[8] : theme.colors.dark[4],

			[theme.fn.smallerThan("sm")]: {
				paddingBottom: "1rem"
			}
		},
		inner: {
			display: "flex",
			justifyContent: "space-between",
			alignItems: "center",
			paddingTop: theme.spacing.xl,
			paddingBottom: theme.spacing.xl,

			[theme.fn.smallerThan("sm")]: {
				flexDirection: "column",
				gap: "1.5rem",
				marginTop: 0,
				paddingTop: 0,
				paddingBottom: 0
			}
		},
		actionIcon: {
			width: "40px",
			minWidth: "40px",
			maxWidth: "40px",
			height: "40px",
			minHeight: "40px",
			maxHeight: "40px",

			[theme.fn.smallerThan("sm")]: {
				width: "36px",
				minWidth: "36px",
				maxWidth: "36px",
				height: "36px",
				minHeight: "36px",
				maxHeight: "36px"
			}
		},
		icon: {
			width: "20px",
			minWidth: "20px",
			maxWidth: "20px",
			height: "20px",
			minHeight: "20px",
			maxHeight: "20px",

			[theme.fn.smallerThan("sm")]: {
				width: "16px",
				minWidth: "16px",
				maxWidth: "16px",
				height: "16px",
				minHeight: "16px",
				maxHeight: "16px"
			}
		},
		title: {
			color: theme.colorScheme === "dark" ? theme.colors.gray[5] : theme.colors.gray[4]
		}
	};
});

const Footer = () => {
	const { classes, cx } = useStyles();

	return (
		<footer className={classes.outer}>
			<Divider />

			<Container className={classes.inner} size="lg">
				<AspectRatio className="w-56" ratio={16 / 9}>
					<Image src={FeritLogo} />
				</AspectRatio>

				<Flex align="center" direction="column" gap="sm" wrap="wrap">
					<Title className={classes.title} fw={500} order={5}>
						Vođenje kina - Završni rad
					</Title>
					<Title className={classes.title} fw={600} order={5}>
						&copy; 2022 - Denis Bošnjaković
					</Title>
				</Flex>

				<Group mt={10} mx="2rem" noWrap spacing="sm">
					<Anchor href="https://www.gitlab.com/db_git/vodenje-kina" target="_blank">
						<ActionIcon className={cx(classes.title, classes.actionIcon)} variant="outline">
							<FaGitlab className={classes.icon} />
						</ActionIcon>
					</Anchor>
					<Anchor href="https://www.ferit.unios.hr" target="_blank">
						<ActionIcon className={cx(classes.title, classes.actionIcon)} variant="outline">
							<Image className={classes.icon} src={FeritIcon} />
						</ActionIcon>
					</Anchor>
				</Group>
			</Container>
		</footer>
	);
};

export default Footer;
