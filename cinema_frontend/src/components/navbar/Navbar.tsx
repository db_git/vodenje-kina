//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Burger, Group, Header, useMantineColorScheme } from "@mantine/core";
import debounce from "lodash.debounce";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback, useEffect, useState } from "react";

import useApiStore from "../../hooks/useApiStore";
import useNavbarStore from "../../hooks/useNavbarStore";
import Cart from "../cart/Cart";
import ThemeToggle from "../theme/ThemeToggle";

import NavbarHeader from "./NavbarHeader";
import { AdminSublinks, Links } from "./NavbarLinks";
import NavbarMobile from "./NavbarMobile";

const burgerStyle = "inline-block md:hidden z-10";
const navbarVisibleAnimation = { root: "animate-show-navbar z-10" };
const navbarHiddenAnimation = { root: "animate-hide-navbar z-10" };

const Navbar = () => {
	const { authStore } = useApiStore();
	const navbarStore = useNavbarStore();
	const { colorScheme } = useMantineColorScheme();
	const [modalOpen, setModalOpen] = useState(false);
	const [previousScrollPosition, setPreviousScrollPosition] = useState(0);

	useEffect(() => {
		const toggleNavbar = debounce(() => {
			if (window.scrollY > 115 && window.scrollY > previousScrollPosition) {
				runInAction(() => {
					navbarStore.setVisible(false);
				});
			} else {
				runInAction(() => {
					navbarStore.setVisible(true);
				});
			}

			setPreviousScrollPosition(window.scrollY);
		}, 150);

		window.addEventListener("scroll", toggleNavbar);
		return () => {
			window.removeEventListener("scroll", toggleNavbar);
		};
	}, [navbarStore, previousScrollPosition]);

	const toggleModal = useCallback((): void => {
		setModalOpen(!modalOpen);
	}, [modalOpen]);

	const burgerIconColor = colorScheme === "dark" ? "orange" : "";
	const mantineHeader = navbarStore.isVisible ? navbarVisibleAnimation : navbarHiddenAnimation;

	return (
		<div className="z-10 mb-16">
			<Header classNames={mantineHeader} fixed height="3rem">
				<NavbarHeader adminSublinks={AdminSublinks} links={Links} />

				<Group position="right">
					<span className={burgerStyle}>
						<Burger color={burgerIconColor} mr={5} mt={1} onClick={toggleModal} opened={modalOpen} />
					</span>
					{authStore.isLoggedIn && (
						<span className={burgerStyle}>
							<Cart />
						</span>
					)}
					<span className={burgerStyle}>
						<ThemeToggle marginRight={10} />
					</span>
				</Group>
				<NavbarMobile
					adminSublinks={AdminSublinks}
					isOpened={modalOpen}
					links={Links}
					toggleMobile={toggleModal}
				/>
			</Header>
		</div>
	);
};

export default observer(Navbar);
