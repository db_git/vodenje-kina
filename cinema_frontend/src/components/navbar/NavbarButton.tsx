//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, useMantineColorScheme } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { useCallback } from "react";
import { NavLink as RouterLink } from "react-router-dom";

import useNavbarStore from "../../hooks/useNavbarStore";

import { type NavbarButtonProps } from "./navbar";

const NavbarButton = ({
	activeVariant,
	inactiveVariant,
	link,
	toggle,
	isParent,
	whiteColor,
	darkColor,
	radius
}: NavbarButtonProps) => {
	const navbarStore = useNavbarStore();
	const { colorScheme } = useMantineColorScheme();

	let isActive: boolean;
	if (isParent) {
		isActive = navbarStore.active === link.location;
	} else {
		isActive = navbarStore.active.includes(link.location);
	}

	const buttonColor = colorScheme === "dark" ? darkColor : whiteColor;
	const buttonVariant = isActive ? activeVariant : inactiveVariant;

	const handleOnClick = useCallback((): void => {
		if (toggle) toggle(false);
		navbarStore.setActive(link.location);
	}, [link.location, navbarStore, toggle]);

	return (
		<Button
			color={buttonColor}
			component={RouterLink}
			fullWidth
			onClick={handleOnClick}
			radius={radius}
			to={link.location}
			variant={buttonVariant}
		>
			{link.label}
		</Button>
	);
};

NavbarButton.defaultProps = {
	activeVariant: "filled",
	darkColor: "orange",
	inactiveVariant: "subtle",
	isParent: false,
	radius: 8,
	whiteColor: "dark"
};

export default observer(NavbarButton);
