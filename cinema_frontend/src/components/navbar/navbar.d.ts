//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type ButtonVariant, type DefaultMantineColor } from "@mantine/core";

import { type RouteVisibility } from "../../types/enums/RouteVisibility";
import { type ChildrenProps } from "../../types/shared";

export type NavbarLink = {
	location: string;
	label: string;
	isParent: boolean;
	visibility: RouteVisibility;
};

export type NavbarButtonProps = {
	readonly link: NavbarLink;
	readonly toggle?: (state: boolean) => void;
	readonly isParent?: boolean;
	readonly whiteColor?: DefaultMantineColor;
	readonly darkColor?: DefaultMantineColor;
	readonly radius?: number;
	readonly activeVariant?: ButtonVariant;
	readonly inactiveVariant?: ButtonVariant;
};

export type LogOutButtonProps = Omit<NavbarButtonProps, "link">;

export type NavbarMobileProps = {
	readonly toggleMobile: () => void;
	readonly isOpened: boolean;
	readonly links: NavbarLink[];
	readonly adminSublinks: NavbarLink[];
};

export type NavbarHeaderProps = {
	readonly links: NavbarLink[];
	readonly adminSublinks: NavbarLink[];
};

export type NavbarModalProps = ChildrenProps & {
	readonly toggleModal: () => void;
	readonly isOpen: boolean;
};
