//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Modal, useMantineTheme } from "@mantine/core";
import { memo, useCallback } from "react";

import { type NavbarModalProps } from "./navbar";

const mantineModal = {
	root: "opacity-95",
	modal: "shadow-none bg-transparent"
};

const NavbarModal = ({ children, toggleModal, isOpen }: NavbarModalProps) => {
	const theme = useMantineTheme();
	const overlayColor = theme.colorScheme === "dark" ? theme.colors.dark[9] : theme.colors.gray[2];

	const handleOnClose = useCallback((): void => {
		toggleModal();
	}, [toggleModal]);

	return (
		<Modal
			classNames={mantineModal}
			onClose={handleOnClose}
			opened={isOpen}
			overlayColor={overlayColor}
			overlayOpacity={1}
			size="sm"
			transition="fade"
			transitionDuration={350}
		>
			{children}
		</Modal>
	);
};

export default memo(NavbarModal);
