//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { observer } from "mobx-react-lite";

import useApiStore from "../../hooks/useApiStore";
import useCartStore from "../../hooks/useCartStore";
import useNavbarStore from "../../hooks/useNavbarStore";
import { Routes } from "../../routes/Routes";
import { RouteVisibility } from "../../types/enums/RouteVisibility";

import NavbarButton from "./NavbarButton";
import { type LogOutButtonProps, type NavbarLink } from "./navbar";

const logOutLink: NavbarLink = { isParent: true, visibility: RouteVisibility.LoggedIn, label: "Log out", location: "" };

const LogOutButton = ({
	toggle,
	isParent,
	whiteColor,
	darkColor,
	inactiveVariant,
	activeVariant,
	radius
}: LogOutButtonProps) => {
	const { authStore } = useApiStore();
	const navbarStore = useNavbarStore();
	const cartStore = useCartStore();

	const handleLogout = (): void => {
		navbarStore.setActive(Routes.Home);
		authStore.logout();
		cartStore.clearOrders();
	};

	if (!authStore.isLoggedIn) return null;

	return (
		<span onClick={handleLogout}>
			<NavbarButton
				activeVariant={activeVariant}
				darkColor={darkColor}
				inactiveVariant={inactiveVariant}
				isParent={isParent}
				link={logOutLink}
				radius={radius}
				toggle={toggle}
				whiteColor={whiteColor}
			/>
		</span>
	);
};

export default observer(LogOutButton);
