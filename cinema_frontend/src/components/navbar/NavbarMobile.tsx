//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Grid } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { useCallback, useState } from "react";

import useApiStore from "../../hooks/useApiStore";
import useNavbarStore from "../../hooks/useNavbarStore";
import canShowRoute from "../../routes/canShowRoute";

import LogOutButton from "./LogOutButton";
import NavbarButton from "./NavbarButton";
import NavbarModal from "./NavbarModal";
import { type NavbarMobileProps } from "./navbar";

const NavbarMobile = ({ links, adminSublinks, toggleMobile, isOpened }: NavbarMobileProps) => {
	const [adminModalOpen, setAdminModalOpen] = useState(false);
	const navbarStore = useNavbarStore();
	const { authStore } = useApiStore();

	const toggleModal = useCallback((): void => {
		setAdminModalOpen(!adminModalOpen);
	}, [adminModalOpen]);

	const toggleMobileAndAdmin = useCallback((): void => {
		toggleMobile();
		setAdminModalOpen(!adminModalOpen);
	}, [adminModalOpen, toggleMobile]);

	const buttonVariant = navbarStore.active.includes("admin") ? "filled" : "subtle";

	return (
		<NavbarModal isOpen={isOpened} toggleModal={toggleMobile}>
			<Grid gutter="xs">
				{authStore.isAdmin && (
					<Button color="red" fullWidth onClick={toggleModal} radius={8} variant={buttonVariant}>
						Admin
					</Button>
				)}

				<NavbarModal isOpen={adminModalOpen} toggleModal={toggleModal}>
					{adminSublinks.map((link) => {
						if (canShowRoute(authStore, link.visibility)) {
							return (
								<NavbarButton
									activeVariant="filled"
									darkColor="red"
									isParent={link.isParent}
									key={link.location}
									link={link}
									toggle={toggleMobileAndAdmin}
									whiteColor="red"
								/>
							);
						}

						return null;
					})}
				</NavbarModal>

				{links.map((link) => {
					if (canShowRoute(authStore, link.visibility)) {
						return (
							<Grid.Col key={link.location}>
								<NavbarButton isParent={link.isParent} link={link} toggle={toggleMobile} />
							</Grid.Col>
						);
					}

					return null;
				})}

				<Grid.Col>
					<LogOutButton darkColor="red" isParent toggle={toggleMobile} whiteColor="red" />
				</Grid.Col>
			</Grid>
		</NavbarModal>
	);
};

export default observer(NavbarMobile);
