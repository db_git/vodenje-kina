//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Group } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { useCallback, useState } from "react";

import useApiStore from "../../hooks/useApiStore";
import useNavbarStore from "../../hooks/useNavbarStore";
import canShowRoute from "../../routes/canShowRoute";
import Cart from "../cart/Cart";
import ThemeToggle from "../theme/ThemeToggle";

import LogOutButton from "./LogOutButton";
import NavbarButton from "./NavbarButton";
import NavbarModal from "./NavbarModal";
import { type NavbarHeaderProps } from "./navbar";

const navbarLinks = "md:block hidden";

const NavbarHeader = ({ links, adminSublinks }: NavbarHeaderProps) => {
	const [modalOpen, setModalOpen] = useState(false);
	const navbarStore = useNavbarStore();
	const { authStore } = useApiStore();

	const toggleModal = useCallback((): void => {
		setModalOpen(!modalOpen);
	}, [modalOpen]);

	const closeModal = useCallback((): void => {
		setModalOpen(false);
	}, []);

	const buttonVariant = navbarStore.active.includes("admin") ? "filled" : "subtle";

	return (
		<Group ml={10} mt={7} position="right" spacing="xs">
			{authStore.isAdmin && (
				<span className={navbarLinks}>
					<Button color="red" onClick={toggleModal} radius={8} variant={buttonVariant}>
						Admin
					</Button>
				</span>
			)}

			<NavbarModal isOpen={modalOpen} toggleModal={toggleModal}>
				{adminSublinks.map((link) => {
					if (canShowRoute(authStore, link.visibility)) {
						return (
							<NavbarButton
								darkColor="red"
								isParent={link.isParent}
								key={link.location}
								link={link}
								toggle={toggleModal}
								whiteColor="red"
							/>
						);
					}

					return null;
				})}
			</NavbarModal>

			{links.map((link) => {
				if (canShowRoute(authStore, link.visibility)) {
					return (
						<span className={navbarLinks} key={link.location}>
							<NavbarButton isParent={link.isParent} link={link} />
						</span>
					);
				}

				return null;
			})}

			<span className={navbarLinks}>
				<LogOutButton darkColor="red" isParent toggle={closeModal} whiteColor="red" />
			</span>
			{authStore.isLoggedIn && (
				<span className={navbarLinks}>
					<Cart />
				</span>
			)}
			<span className={navbarLinks}>
				<ThemeToggle marginRight={10} />
			</span>
		</Group>
	);
};

export default observer(NavbarHeader);
