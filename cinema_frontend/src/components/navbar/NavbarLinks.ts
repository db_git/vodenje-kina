//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Routes } from "../../routes/Routes";
import { RouteVisibility } from "../../types/enums/RouteVisibility";

import { type NavbarLink } from "./navbar";

const Links: NavbarLink[] = [
	{ location: Routes.Home, label: "Home", isParent: true, visibility: RouteVisibility.Everyone },
	{ location: Routes.Movies, label: "Movies", isParent: false, visibility: RouteVisibility.Everyone },
	{ location: Routes.Account, label: "Account", isParent: true, visibility: RouteVisibility.LoggedIn },
	{ location: Routes.Registration, label: "Registration", isParent: true, visibility: RouteVisibility.LoggedOut },
	{ location: Routes.About, label: "About", isParent: true, visibility: RouteVisibility.Everyone }
];

const AdminSublinks: NavbarLink[] = [
	{ location: Routes.AdminDashboard, label: "Dashboard", isParent: true, visibility: RouteVisibility.Admin },
	{ location: Routes.AdminCinema, label: "Cinema", isParent: false, visibility: RouteVisibility.Admin },
	{ location: Routes.AdminFilmRating, label: "Film rating", isParent: false, visibility: RouteVisibility.Admin },
	{ location: Routes.AdminFood, label: "Food", isParent: false, visibility: RouteVisibility.Admin },
	{ location: Routes.AdminGenre, label: "Genre", isParent: false, visibility: RouteVisibility.Admin },
	{ location: Routes.AdminHall, label: "Hall", isParent: false, visibility: RouteVisibility.Admin },
	{ location: Routes.AdminMovie, label: "Movie", isParent: false, visibility: RouteVisibility.Admin },
	{ location: Routes.AdminPerson, label: "Person", isParent: false, visibility: RouteVisibility.Admin },
	{ location: Routes.AdminProjection, label: "Projection", isParent: false, visibility: RouteVisibility.Admin },
	{ location: Routes.AdminSeat, label: "Seat", isParent: false, visibility: RouteVisibility.Admin },
	{ location: Routes.AdminSouvenir, label: "Souvenir", isParent: false, visibility: RouteVisibility.Admin },
	{ location: Routes.AdminUser, label: "User", isParent: false, visibility: RouteVisibility.Admin },
	{ location: Routes.AdminUserComment, label: "User comment", isParent: false, visibility: RouteVisibility.Admin },
	{ location: Routes.AdminUserRating, label: "User rating", isParent: false, visibility: RouteVisibility.Admin }
];

export { Links, AdminSublinks };
