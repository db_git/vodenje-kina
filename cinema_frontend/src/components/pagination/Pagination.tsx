//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { useMantineColorScheme } from "@mantine/core";
import { type MantineThemeColors } from "@mantine/styles/lib/theme/types/MantineColor";
import { type MantineNumberSize } from "@mantine/styles/lib/theme/types/MantineSize";
import { observer } from "mobx-react-lite";

import { type QueryParameters } from "../../types/query";

import DesktopPagination from "./DesktopPagination";
import MobilePagination from "./MobilePagination";
import { type PaginationProps } from "./pagination";

const buttonRadius: MantineNumberSize = "md";
const darkButtonColor: keyof MantineThemeColors = "orange";
const lightButtonColor: keyof MantineThemeColors = "dark";

const Pagination = <T, U extends QueryParameters>({ store, modifyUrl }: PaginationProps<T, U>) => {
	const { colorScheme } = useMantineColorScheme();
	const paginationButtonColor = colorScheme === "dark" ? darkButtonColor : lightButtonColor;

	if (store.responses && !store.responses.hasNextPage && !store.responses.hasPreviousPage) return null;

	return (
		<>
			<DesktopPagination
				buttonColor={paginationButtonColor}
				buttonRadius={buttonRadius}
				modifyUrl={modifyUrl}
				store={store}
			/>
			<MobilePagination
				buttonColor={paginationButtonColor}
				buttonRadius={buttonRadius}
				modifyUrl={modifyUrl}
				store={store}
			/>
		</>
	);
};

export default observer(Pagination);
