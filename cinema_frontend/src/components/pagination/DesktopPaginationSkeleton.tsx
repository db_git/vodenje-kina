//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Pagination, Stack } from "@mantine/core";

import { type DesktopPaginationSkeletonProps } from "./pagination";

const DesktopPaginationSkeleton = ({ buttonRadius, buttonColor }: DesktopPaginationSkeletonProps) => {
	return (
		<div className="hidden md:block">
			<Stack align="center">
				<Pagination
					boundaries={2}
					color={buttonColor}
					disabled
					mt="1rem"
					page={0}
					radius={buttonRadius}
					total={5}
					withEdges
				/>
			</Stack>
		</div>
	);
};

export default DesktopPaginationSkeleton;
