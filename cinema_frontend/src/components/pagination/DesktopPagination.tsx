//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Pagination, Stack } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback } from "react";

import { QueryParameterOptions } from "../../types/enums/Stores";
import { type QueryParameters } from "../../types/query";

import DesktopPaginationSkeleton from "./DesktopPaginationSkeleton";
import PaginationInfo from "./PaginationInfo";
import { type DesktopPaginationProps } from "./pagination";

const DesktopPagination = <T, U extends QueryParameters>({
	store,
	modifyUrl,
	buttonRadius,
	buttonColor
}: DesktopPaginationProps<T, U>) => {
	const onChange = useCallback(
		(page: number) => {
			runInAction(() => {
				if (store.responses?.pageNumber !== page) {
					store.setQueryParameters(QueryParameterOptions.Page, page);
					modifyUrl();
				}
			});
		},
		[modifyUrl, store]
	);

	if (!store.responses) return null;

	if (store.loading.getAll)
		return <DesktopPaginationSkeleton buttonColor={buttonColor} buttonRadius={buttonRadius} />;

	return (
		<div className="hidden md:block">
			<Stack align="center">
				<Pagination
					boundaries={2}
					color={buttonColor}
					mt="1rem"
					onChange={onChange}
					page={store.responses.pageNumber}
					radius={buttonRadius}
					total={store.responses.totalPages}
					withEdges
				/>
				<PaginationInfo store={store} />
			</Stack>
		</div>
	);
};

export default observer(DesktopPagination);
