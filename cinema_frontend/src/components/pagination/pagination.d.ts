//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type DefaultMantineColor } from "@mantine/core";
import { type MantineNumberSize } from "@mantine/styles/lib/theme/types/MantineSize";

import { type IApiStore } from "../../stores/stores";
import { type QueryParameters } from "../../types/query";

export type PaginationInfoProps<T, U extends QueryParameters> = {
	readonly store: IApiStore<T, U>;
};

export type PaginationProps<T, U extends QueryParameters> = PaginationInfoProps<T, U> & {
	readonly modifyUrl: () => void;
};

export type CommonPaginationProps = {
	readonly buttonRadius: MantineNumberSize;
	readonly buttonColor: DefaultMantineColor | undefined;
};

export type MobilePaginationProps<T, U extends QueryParameters> = CommonPaginationProps & PaginationProps<T, U>;
export type DesktopPaginationProps<T, U extends QueryParameters> = CommonPaginationProps & PaginationProps<T, U>;

export type DesktopPaginationSkeletonProps = CommonPaginationProps;
export type MobilePaginationSkeletonProps = CommonPaginationProps;
