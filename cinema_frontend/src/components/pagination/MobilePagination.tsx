//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Center, Group } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback } from "react";

import { QueryParameterOptions } from "../../types/enums/Stores";
import { type QueryParameters } from "../../types/query";

import MobilePaginationSkeleton from "./MobilePaginationSkeleton";
import PaginationInfo from "./PaginationInfo";
import { type MobilePaginationProps } from "./pagination";

const MobilePagination = <T, U extends QueryParameters>({
	store,
	modifyUrl,
	buttonRadius,
	buttonColor
}: MobilePaginationProps<T, U>) => {
	const previousPage = useCallback(() => {
		runInAction(() => {
			store.setQueryParameters(QueryParameterOptions.Page, store.queryParameters.page - 1);
		});
		modifyUrl();
	}, [modifyUrl, store]);

	const nextPage = useCallback(() => {
		runInAction(() => {
			store.setQueryParameters(QueryParameterOptions.Page, store.queryParameters.page + 1);
		});
		modifyUrl();
	}, [modifyUrl, store]);

	if (store.loading.getAll) return <MobilePaginationSkeleton buttonColor={buttonColor} buttonRadius={buttonRadius} />;

	return (
		<div className="block md:hidden">
			<Group mt="1rem" position="apart">
				<Button
					color={buttonColor}
					disabled={!store.responses?.hasPreviousPage}
					onClick={previousPage}
					radius={buttonRadius}
					size="xs"
					variant="filled"
				>
					Previous
				</Button>
				<Button
					color={buttonColor}
					disabled={!store.responses?.hasNextPage}
					onClick={nextPage}
					radius={buttonRadius}
					size="xs"
					variant="filled"
				>
					Next
				</Button>
			</Group>
			<Center mt="1rem">
				<PaginationInfo store={store} />
			</Center>
		</div>
	);
};

export default observer(MobilePagination);
