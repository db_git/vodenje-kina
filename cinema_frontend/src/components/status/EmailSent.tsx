//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Stack, Text } from "@mantine/core";
import classNames from "classnames";
import { memo } from "react";

import { ReactComponent as EmailSentSvg } from "../../assets/illustrations/EmailSent.svg";

import { type EmailSentProps } from "./status";

import "./styles/EmailSent.scss";

const EmailSent = ({ message, className }: EmailSentProps) => {
	const iconStyle = classNames("animated h-full w-full max-w-sm", className);

	return (
		<Stack align="center">
			<EmailSentSvg className={iconStyle} />

			{Boolean(message) && (
				<Text align="center" color="dimmed" size="lg">
					{message}
				</Text>
			)}
		</Stack>
	);
};

export default memo(EmailSent);
