//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Stack, Text } from "@mantine/core";
import { memo, useEffect } from "react";

import { ReactComponent as InternalServerError } from "../../assets/illustrations/ServerError.svg";

import "./styles/ServerError.scss";

const reload = (): void => {
	window.location.reload();
};

const ServerError = () => {
	useEffect(() => {
		document.title = "Server error";
	}, []);

	return (
		<Stack align="center">
			<p className="m-0 p-0 text-center text-3xl font-semibold md:text-4xl lg:text-5xl">
				Something bad just happened...
			</p>

			<InternalServerError className="animated m-0 h-full w-full max-w-lg p-0" />

			<Text align="center" color="dimmed" size="lg">
				Our servers could not handle your request. Please refresh the page or try again later.
			</Text>

			<Button color="red" mt="1rem" onClick={reload} radius="xl" size="md">
				<p className="whitespace-pre-line">Refresh page</p>
			</Button>
		</Stack>
	);
};

export default memo(ServerError);
