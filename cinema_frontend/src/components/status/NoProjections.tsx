//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Stack, Text } from "@mantine/core";
import classNames from "classnames";
import { memo } from "react";

import { ReactComponent as NoProjectionsSvg } from "../../assets/illustrations/NoProjections.svg";

import { type NoProjectionsProps } from "./status";

const NoProjections = ({ message, className }: NoProjectionsProps) => {
	const iconStyle = classNames("h-fit w-full", className);

	return (
		<Stack align="center" className="bg-transparent">
			<NoProjectionsSvg className={iconStyle} />

			<Text align="center" color="dimmed" mt="-1.5rem" size="lg">
				{message}
			</Text>
		</Stack>
	);
};

NoProjections.defaultProps = {
	className: "max-w-sm",
	message: "No projections found..."
};

export default memo(NoProjections);
