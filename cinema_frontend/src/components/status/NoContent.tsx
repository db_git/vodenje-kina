//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Stack, Text } from "@mantine/core";
import classNames from "classnames";
import { memo } from "react";

import { ReactComponent as NoContentSvg } from "../../assets/illustrations/NoContent.svg";

import { type NoContentProps } from "./status";

import "./styles/NoContent.scss";

const NoContent = ({ message, className }: NoContentProps) => {
	const iconStyle = classNames("animated h-full w-full", className);

	return (
		<Stack align="center">
			<NoContentSvg className={iconStyle} />

			<Text align="center" color="dimmed" size="lg">
				{message}
			</Text>
		</Stack>
	);
};

NoContent.defaultProps = {
	className: "max-w-sm",
	message: "No data available..."
};

export default memo(NoContent);
