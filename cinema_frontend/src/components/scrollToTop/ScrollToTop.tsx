//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Affix, Button, Transition } from "@mantine/core";
import { useWindowScroll } from "@mantine/hooks";
import React, { memo, useCallback } from "react";
import { BsArrowUp } from "react-icons/bs";

const ScrollToTop = () => {
	const [scroll, scrollTo] = useWindowScroll();

	const handleOnClick = useCallback(() => {
		scrollTo({ y: 0 });
	}, [scrollTo]);

	return (
		<Affix position={{ bottom: 10, right: 10 }}>
			<Transition mounted={scroll.y > document.documentElement.clientHeight / 2} transition="slide-up">
				{(transitionStyles) => {
					return (
						<Button
							color="cyan"
							leftIcon={<BsArrowUp size={14} />}
							onClick={handleOnClick}
							radius="xl"
							size="xs"
							style={transitionStyles}
						>
							Scroll to top
						</Button>
					);
				}}
			</Transition>
		</Affix>
	);
};

export default memo(ScrollToTop);
