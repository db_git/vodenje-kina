//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

export type Cinema = {
	id: string;
	name: string;
	city: string;
	street: string;
	ticketPrice: number;
	halls?: Hall[];
};

export type FilmRating = {
	id: string;
	type: string;
	description: string;
};

export type Food = {
	id: string;
	name: string;
	price: number;
	availableQuantity: number;
	type: FoodType;
	size: number;
	imageUrl: string;
};

export type FoodType = "Drink" | "Snack";

export type Genre = {
	id: string;
	name: string;
};

export type Hall = {
	id: string;
	name: string;
	cinema: HallCinema;
};

export type HallCinema = Omit<Cinema, "halls">;

export type HomePageMovies = {
	featuredMovies: Movie[];
	newMovies: Movie[];
	runningMovies: Movie[];
	recommendedMovies: Movie[];
};

export type SimpleMovie = {
	id: string;
	title: string;
	posterUrl: string;
	rating: number;
};

export type Movie = {
	id: string;
	title: string;
	year: number;
	duration: number;
	tagline: string;
	summary: string;
	inCinemas: boolean;
	isFeatured?: boolean;
	isRecommended?: boolean;
	posterUrl: string;
	backdropUrl: string;
	trailerUrl: string;
	rating: number;
};

export type IndividualMovie = {
	id: string;
	title: string;
	year: number;
	duration: number;
	tagline: string;
	summary: string;
	inCinemas: boolean;
	isFeatured?: boolean;
	isRecommended?: boolean;
	posterUrl: string;
	backdropUrl: string;
	trailerUrl: string;

	filmRating: FilmRating;
	genres: Genre[];
	actors: MovieActor[];
	directors: MovieDirector[];
};

export type MovieActor = {
	id: string;
	name: string;
	character: string;
	imageUrl: string;
	order: number;
};

export type MovieDirector = {
	id: string;
	name: string;
	imageUrl: string;
};

export type Person = {
	id: string;
	name: string;
	dateOfBirth: string;
	dateOfDeath?: string;
	imageUrl: string;
	biography: string;
	movies: PersonMovie[];
};

export type PersonMovie = {
	id: string;
	title: string;
	year: number;
	character: string;
	type: string;
	posterUrl: string;
	backdropUrl: string;
};

export type Projection = {
	id: string;
	movie: Movie;
	hall: Hall;
	time: string;
};

export type FoodOrder = Omit<Food, "availableQuantity"> & {
	orderedQuantity: number;
};

export type SouvenirOrder = Omit<Souvenir, "availableQuantity"> & {
	orderedQuantity: number;
};

export type Reservation = {
	id: string;
	projection: Projection;
	seats: Seat[];
	foodOrders?: FoodOrder[];
	souvenirOrders?: SouvenirOrder[];
};

export type Seat = {
	id: string;
	number: number;
	halls: Hall[];
};

export type IndividualSeat = Omit<Seat, "halls">;

export type Souvenir = {
	id: string;
	name: string;
	price: number;
	availableQuantity: number;
	imageUrl: string;
};

export type User = {
	id: string;
	name: string;
	imageUrl: string | null;
	email: string;
	roles: string[];
};

export type UserToken = {
	token: string;
};

export type UserCommentMovie = {
	id: string;
	title: string;
};

export type UserCommentUser = {
	id: string;
	name: string;
	imageUrl: string | null;
};

export type UserComment = {
	movie: UserCommentMovie;
	user: UserCommentUser;
	comment: string;
	date: string;
};

export type RatingStar = {
	star: number;
	count: number;
};

export type MovieRating = {
	myRating: number | null;
	totalRating: number;
	ratingsCount: number;
	ratingStars: RatingStar[];
};

export type UserRatingMovie = {
	id: string;
	title: string;
};

export type UserRatingUser = {
	id: string;
	name: string;
	imageUrl: string | null;
};

export type UserRating = {
	movie: UserRatingMovie;
	user: UserRatingUser;
	rating: number;
};
