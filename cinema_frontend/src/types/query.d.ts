//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

export type QueryParameters = {
	page: number;
	size: number;
	orderBy: number;
	search?: string;
};

export type FoodQueryParameters = QueryParameters & {
	availableOnly?: number;
};

export type HomePageMoviesQueryParameters = {
	featuredMoviesLimit: number;
	newMoviesLimit?: number;
	runningMoviesDay: number;
	runningMoviesMonth: number;
	runningMoviesLimit?: number;
	recommendedMoviesLimit: number;
};

export type MovieQueryParameters = QueryParameters & {
	year?: number;
	yearStart?: number;
	yearEnd?: number;
	durationUnder?: number;
	durationOver?: number;
	showUpcoming?: number;
	filmRating?: string;
	hasProjections?: boolean;
	genres?: string[];
};

export type PersonQueryParameters = QueryParameters & {
	day?: number;
	month?: number;
};

export type ProjectionQueryParameters = QueryParameters & {
	movie?: string;
	hall?: string;
	startDate?: string;
	endDate?: string;
};

export type SeatQueryParameters = QueryParameters & {
	hall?: string;
};

export type SouvenirQueryParameters = QueryParameters & {
	availableOnly?: number;
};
