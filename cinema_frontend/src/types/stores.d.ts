//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

export type ApiStoreStatus = {
	delete: number;
	get: number;
	getAll: number;
	post: number;
	put: number;
	patch: number;
};

export type ApiStoreLoading = {
	delete: boolean;
	get: boolean;
	getAll: boolean;
	post: boolean;
	put: boolean;
	patch: boolean;
};

export type Page<T> = {
	pageNumber: number;
	pageSize: number;

	elements: number;
	totalElements: number;
	totalPages: number;

	hasPreviousPage: boolean;
	hasNextPage: boolean;

	data: T;
};
