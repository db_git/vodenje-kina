//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

export enum QueryParameterOptions {
	Page = 1,
	Size = 2,
	OrderBy = 3,
	Search = 4
}

export enum FoodQueryParameterOptions {
	Page = 1,
	Size = 2,
	OrderBy = 3,
	Search = 4,
	AvailableOnly = 5
}

export enum MovieQueryParameterOptions {
	Page = 1,
	Size = 2,
	OrderBy = 3,
	Search = 4,
	Year = 5,
	YearStart = 6,
	YearEnd = 7,
	DurationUnder = 9,
	DurationOver = 8,
	ShowUpcoming = 10,
	FilmRating = 11,
	HasProjections = 12,
	Genres = 13
}

export enum PersonQueryParameterOptions {
	Page = 1,
	Size = 2,
	OrderBy = 3,
	Search = 4,
	Day = 5,
	Month = 6
}

export enum ProjectionQueryParameterOptions {
	Page = 1,
	Size = 2,
	OrderBy = 3,
	Search = 4,
	Movie = 5,
	Hall = 6,
	StartDate = 7,
	EndDate = 8
}

export enum SeatQueryParameterOptions {
	Page = 1,
	Size = 2,
	OrderBy = 3,
	Search = 4,
	Hall = 5
}

export enum SouvenirQueryParameterOptions {
	Page = 1,
	Size = 2,
	OrderBy = 3,
	Search = 4,
	AvailableOnly = 5
}
