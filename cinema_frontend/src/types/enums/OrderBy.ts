//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

export enum CinemaOrderBy {
	NameAscending = 1,
	NameDescending = 2,
	CityAscending = 3,
	CityDescending = 4,
	TicketPriceAscending = 5,
	TicketPriceDescending = 6
}

export enum FilmRatingOrderBy {
	TypeAscending = 1,
	TypeDescending = 2
}

export enum FoodOrderBy {
	NameAscending = 1,
	NameDescending = 2,
	PriceAscending = 3,
	PriceDescending = 4,
	AvailableQuantityAscending = 5,
	AvailableQuantityDescending = 6,
	SizeAscending = 7,
	SizeDescending = 8,
	TypeAscending = 9,
	TypeDescending = 10
}

export enum GenreOrderBy {
	NameAscending = 1,
	NameDescending = 2
}

export enum HallOrderBy {
	NameAscending = 1,
	NameDescending = 2
}

export enum MovieOrderBy {
	TitleAscending = 1,
	TitleDescending = 2,
	YearAscending = 3,
	YearDescending = 4,
	CreatedAtAscending = 5,
	CreatedAtDescending = 6
}

export enum PersonOrderBy {
	NameAscending = 1,
	NameDescending = 2
}

export enum ProjectionOrderBy {
	MovieTitleAscending = 1,
	MovieTitleDescending = 2,
	HallNameAscending = 3,
	HallNameDescending = 4,
	TimeAscending = 5,
	TimeDescending = 6
}

export enum ReservationOrderBy {
	UserNameAscending = 1,
	UserNameDescending = 2,
	MovieTitleAscending = 3,
	MovieTitleDescending = 4,
	DateAscending = 5,
	DateDescending = 6
}

export enum SeatOrderBy {
	NumberAscending = 1,
	NumberDescending = 2
}

export enum SouvenirOrderBy {
	NameAscending = 1,
	NameDescending = 2,
	PriceAscending = 3,
	PriceDescending = 4,
	AvailableQuantityAscending = 5,
	AvailableQuantityDescending = 6
}
