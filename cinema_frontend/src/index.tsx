//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { NotificationsProvider } from "@mantine/notifications";
import React from "react";
import { createRoot } from "react-dom/client";

import App from "./App";
import StoreProvider from "./providers/store/StoreProvider";
import ThemeProvider from "./providers/theme/ThemeProvider";

import "./styles/index.scss";

const root = createRoot(document.querySelector("#root") as Element);
root.render(
	<React.StrictMode>
		<ThemeProvider>
			<StoreProvider>
				<NotificationsProvider autoClose={3_500} limit={10} notificationMaxHeight={380} position="top-right">
					<App />
				</NotificationsProvider>
			</StoreProvider>
		</ThemeProvider>
	</React.StrictMode>
);
