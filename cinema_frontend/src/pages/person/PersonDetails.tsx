//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Container, Grid, Stack } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { useParams } from "react-router-dom";

import Heading from "../../components/display/Heading";
import NoContent from "../../components/status/NoContent";
import ServerError from "../../components/status/ServerError";
import useGetStore from "../../hooks/useGetStore";
import { is5xxStatus, isNotFoundOrNoContent } from "../../utils/isStatus";

import PersonBiography from "./components/PersonBiography";
import PersonCreditedMovies from "./components/PersonCreditedMovies";
import PersonDate from "./components/PersonDate";
import PersonDetailsSkeleton from "./components/PersonDetailsSkeleton";
import PersonFilmography from "./components/PersonFilmography";
import PersonImage from "./components/PersonImage";

const PersonDetails = () => {
	const { id } = useParams();
	const { personGetStore } = useGetStore();

	const person = personGetStore.response;

	useEffect(() => {
		runInAction(() => {
			document.title = person ? person.name : "Loading...";
		});
	}, [person]);

	useEffect(() => {
		if (id && person === undefined) void personGetStore.get(id);
	}, [id, person, personGetStore]);

	useEffect(() => {
		return () => {
			personGetStore.setResponse(undefined);
		};
	}, [personGetStore]);

	if (isNotFoundOrNoContent(personGetStore.status.get)) return <NoContent message="Person not found." />;
	if (is5xxStatus(personGetStore.status.get)) return <ServerError />;
	if (personGetStore.loading.get) return <PersonDetailsSkeleton />;
	if (!person) return null;

	return (
		<Container px="sm" size="lg">
			<Grid grow gutter="xl" justify="center">
				<Grid.Col sm={6} xs={12}>
					<PersonImage person={person} />
				</Grid.Col>

				<Grid.Col mt="2rem" sm={6} xs={12}>
					<Stack align="flex-start" justify="space-around" spacing="xl">
						<Heading size={1}>{person.name}</Heading>
						<PersonDate person={person} />
						<PersonCreditedMovies person={person} />
					</Stack>
				</Grid.Col>

				<Grid.Col xs={12}>
					<PersonBiography person={person} />
				</Grid.Col>

				<Grid.Col xs={12}>
					<PersonFilmography person={person} />
				</Grid.Col>
			</Grid>
		</Container>
	);
};

export default observer(PersonDetails);
