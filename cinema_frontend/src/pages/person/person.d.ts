//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type Person, type PersonMovie } from "../../types/entities";

export type PersonProps = {
	readonly person: Person;
};

export type PersonBiographyProps = PersonProps;
export type PersonCreditedMoviesProps = PersonProps;
export type PersonDateProps = PersonProps;
export type PersonFilmographyProps = PersonProps;
export type PersonImageProps = PersonProps;

export type PersonMovieRowProps = {
	readonly movie: PersonMovie;
	readonly isLast: boolean;
};
