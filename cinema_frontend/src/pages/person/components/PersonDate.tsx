//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Text, Title, Tooltip } from "@mantine/core";
import dayjs from "dayjs";
import { observer } from "mobx-react-lite";

import { type PersonDateProps } from "../person";
import getAge from "../utils/getAge";

const PersonDate = ({ person }: PersonDateProps) => {
	const age = `(${getAge(person.dateOfBirth)} years old)`;

	return (
		<>
			<div>
				<Title order={4}>Birthday</Title>

				<Text>
					<Tooltip label={person.dateOfBirth} transition="fade" transitionDuration={425} withArrow>
						<span>{dayjs(person.dateOfBirth).format("LL")} &nbsp;</span>
					</Tooltip>
					{!person.dateOfDeath && age}
				</Text>
			</div>

			{person.dateOfDeath && (
				<div>
					<Title order={4}>Death</Title>
					<Text>
						<Tooltip label={person.dateOfDeath} transition="fade" transitionDuration={425} withArrow>
							<Text>{dayjs(person.dateOfDeath).format("LL")} &nbsp;</Text>
						</Tooltip>
						{age}
					</Text>
				</div>
			)}
		</>
	);
};

export default observer(PersonDate);
