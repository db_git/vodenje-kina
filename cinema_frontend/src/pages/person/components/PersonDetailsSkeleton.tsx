//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Container, Grid, Skeleton, Stack } from "@mantine/core";

import PersonFilmographySkeleton from "./PersonFilmographySkeleton";
import PersonImageSkeleton from "./PersonImageSkeleton";

const PersonDetailsSkeleton = () => {
	return (
		<Container px="sm" size="lg">
			<Grid grow gutter="xl" justify="center">
				<Grid.Col sm={6} xs={12}>
					<PersonImageSkeleton />
				</Grid.Col>

				<Grid.Col mt="2rem" sm={6} xs={12}>
					<Stack align="flex-start" justify="space-around" spacing="xl">
						<Skeleton h="3rem" w="20rem" />

						<div>
							<Skeleton h="1rem" w="6rem" />
							<Skeleton h="1rem" mt="0.5rem" w="9rem" />

							<Skeleton h="1rem" mt="2rem" w="4rem" />
							<Skeleton h="1rem" mt="0.5rem" w="9rem" />
							<Skeleton h="1rem" mt="0.5rem" w="8rem" />
						</div>

						<div>
							<Skeleton h="1rem" w="12rem" />
							<Skeleton h="1rem" mt="0.5rem" w="2rem" />
						</div>
					</Stack>
				</Grid.Col>

				<Grid.Col xs={12}>
					<Stack align="flex-start" mt="1.5rem">
						<Skeleton h="1.5rem" w="10rem" />

						<Skeleton h="0.8rem" mt="1rem" w="100%" />
						<Skeleton h="0.8rem" w="100%" />
						<Skeleton h="0.8rem" w="100%" />
						<Skeleton h="0.8rem" w="100%" />
						<Skeleton h="0.8rem" w="100%" />
						<Skeleton h="0.8rem" w="100%" />

						<Skeleton h="0.5rem" w="10%" />
					</Stack>
				</Grid.Col>

				<Grid.Col xs={12}>
					<PersonFilmographySkeleton />
				</Grid.Col>
			</Grid>
		</Container>
	);
};

export default PersonDetailsSkeleton;
