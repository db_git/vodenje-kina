//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Center, Image, Skeleton, Text, useMantineTheme } from "@mantine/core";
import { observer } from "mobx-react-lite";
import React from "react";
import { NavLink } from "react-router-dom";

import { Routes } from "../../../routes/Routes";
import MovieStyle from "../../../styles/Movies.module.scss";
import { type PersonMovieRowProps } from "../person";

const directorCharacter = "Director";
const textHover = MovieStyle[`movie-card-title`];

const PersonMovieRow = ({ movie, isLast }: PersonMovieRowProps) => {
	const theme = useMantineTheme();
	const rowStyle = {
		borderBottom: `1px solid ${theme.colorScheme === "dark" ? theme.colors.dark[4] : theme.colors.gray[3]}`
	};

	return (
		<>
			<tr>
				<td className="hidden md:block">
					<Center>
						<NavLink to={`${Routes.MovieDetails}/${movie.id}`}>
							<Image
								height="7.5rem"
								placeholder={<Skeleton height="7.5rem" radius="lg" width="5rem" />}
								radius="lg"
								src={movie.posterUrl}
								width="5rem"
								withPlaceholder
							/>
						</NavLink>
					</Center>
				</td>

				<td>
					<Text inline span weight={500}>
						{movie.year}
					</Text>
				</td>

				<td>
					<Text inline span>
						<Text component={NavLink} inline span to={`${Routes.MovieDetails}/${movie.id}`} weight={700}>
							<span className={textHover}>{movie.title}</span>
						</Text>
						{movie.character !== directorCharacter && (
							<>
								<Text color="dimmed" span>
									&nbsp;as&nbsp;
								</Text>
								<Text inline span weight={400}>
									{movie.character}
								</Text>
							</>
						)}
					</Text>
				</td>
			</tr>
			{isLast && <tr style={rowStyle} />}
		</>
	);
};

export default observer(PersonMovieRow);
