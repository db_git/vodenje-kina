//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Paper, Stack, Table } from "@mantine/core";
import { observer } from "mobx-react-lite";
import React from "react";

import Heading from "../../../components/display/Heading";
import { type PersonFilmographyProps } from "../person";

import PersonMovieRow from "./PersonMovieRow";

const PersonFilmography = ({ person }: PersonFilmographyProps) => {
	if (person.movies.length)
		return (
			<Stack align="flex-start" id="filmography">
				<Heading anchor="#filmography" className="mb-4 mt-4" size={2}>
					Filmography
				</Heading>

				<Paper p="sm" radius="xl" shadow="xl" style={{ width: "100%" }} withBorder>
					<Table
						fontSize="md"
						horizontalSpacing="xl"
						px="5rem"
						sx={{
							"& tbody tr td": {
								borderBottom: "none"
							}
						}}
						verticalSpacing="sm"
					>
						<tbody>
							{person.movies.map((movie, index) => {
								return (
									<PersonMovieRow
										isLast={person.movies.length !== index + 1}
										key={movie.id}
										movie={movie}
									/>
								);
							})}
						</tbody>
					</Table>
				</Paper>
			</Stack>
		);

	return null;
};

export default observer(PersonFilmography);
