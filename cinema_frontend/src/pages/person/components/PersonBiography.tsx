//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Spoiler, Stack, Text } from "@mantine/core";
import { observer } from "mobx-react-lite";

import Heading from "../../../components/display/Heading";
import { type PersonBiographyProps } from "../person";

const PersonBiography = ({ person }: PersonBiographyProps) => {
	return (
		<Stack align="flex-start" id="biography">
			<Heading anchor="#biography" className="mb-4 mt-4" size={2}>
				Biography
			</Heading>
			<Spoiler hideLabel="Hide" maxHeight={250} showLabel="Show more">
				<Text>{person.biography}</Text>
			</Spoiler>
		</Stack>
	);
};

export default observer(PersonBiography);
