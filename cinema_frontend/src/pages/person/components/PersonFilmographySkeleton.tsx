//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Center, Paper, Skeleton, Stack, Table, useMantineTheme } from "@mantine/core";
import React, { Fragment } from "react";

const array = [...Array.from({ length: 5 }).keys()];

const PersonFilmographySkeleton = () => {
	const theme = useMantineTheme();
	const rowStyle = {
		borderBottom: `1px solid ${theme.colorScheme === "dark" ? theme.colors.dark[4] : theme.colors.gray[3]}`
	};

	return (
		<Stack align="flex-start">
			<Skeleton h="1.5rem" mb="0.5rem" mt="1rem" w="12rem" />

			<Paper p="sm" radius="xl" shadow="xl" style={{ width: "100%" }} withBorder>
				<Table
					fontSize="md"
					horizontalSpacing="xl"
					px="5rem"
					sx={{
						"& tbody tr td": {
							borderBottom: "none"
						}
					}}
					verticalSpacing="sm"
				>
					<tbody>
						{array.map((number) => {
							return (
								<Fragment key={number}>
									<tr>
										<td className="hidden md:block">
											<Center>
												<Skeleton height="7.5rem" radius="lg" width="5rem" />
											</Center>
										</td>

										<td>
											<Skeleton h="0.8rem" w="4rem" />
										</td>

										<td>
											<Skeleton h="0.8rem" w="14rem" />
										</td>
									</tr>
									{array.length !== number + 1 && <tr style={rowStyle} />}
								</Fragment>
							);
						})}
					</tbody>
				</Table>
			</Paper>
		</Stack>
	);
};

export default PersonFilmographySkeleton;
