//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Card, Image, Skeleton } from "@mantine/core";
import classNames from "classnames";
import { observer } from "mobx-react-lite";

import { type PersonImageProps } from "../person";

export const CardImageStyle = classNames(
	"h-80 w-60 min-h-full min-w-[12rem]",
	"md:h-[20rem] md:w-[15rem]",
	"lg:w-[17.5rem] lg:h-[24rem]",
	"bg-transparent rounded-3xl"
);

const PersonImage = ({ person }: PersonImageProps) => {
	return (
		<div className="flex items-center justify-center">
			<Card className={CardImageStyle} m={0} p={0}>
				<Card.Section>
					<Image
						placeholder={<Skeleton height="100vh" width="100vw" />}
						src={person.imageUrl}
						withPlaceholder
					/>
				</Card.Section>
			</Card>
		</div>
	);
};

export default observer(PersonImage);
