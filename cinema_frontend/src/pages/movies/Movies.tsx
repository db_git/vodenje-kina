//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Container, Grid } from "@mantine/core";
import { memo, useEffect } from "react";

import useNavbarStore from "../../hooks/useNavbarStore";
import { Routes } from "../../routes/Routes";

import MovieAlbum from "./components/album/MovieAlbum";
import MovieFilter from "./components/filter/MovieFilter";

const Movies = () => {
	const navbarStore = useNavbarStore();

	useEffect(() => {
		navbarStore.setActive(Routes.Movies);
		document.title = "Movies";
	});

	return (
		<Container size="xl">
			<Grid>
				<Grid.Col md={3} xs={12}>
					<MovieFilter />
				</Grid.Col>
				<Grid.Col md={9} xs={12}>
					<MovieAlbum />
				</Grid.Col>
			</Grid>
		</Container>
	);
};

export default memo(Movies);
