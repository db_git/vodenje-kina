//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type SimpleMovieProps } from "../../types/movies";
import { type OptionalMarginProps } from "../../types/shared";

export type MovieFilmRatingFilterProps = OptionalMarginProps;
export type MovieGenresFilterProps = OptionalMarginProps;
export type MovieKeywordsFilterProps = OptionalMarginProps;
export type MoviesPerPageFilterProps = OptionalMarginProps;
export type MovieSortFilterProps = OptionalMarginProps;
export type MovieUpcomingFilterProps = OptionalMarginProps;
export type MovieYearFilterProps = OptionalMarginProps;
export type AlternateMovieCardProps = SimpleMovieProps & {
	readonly withRating?: boolean;
};
