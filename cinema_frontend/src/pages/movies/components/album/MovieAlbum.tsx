//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { SimpleGrid, type SimpleGridBreakpoint } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback, useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";

import Pagination from "../../../../components/pagination/Pagination";
import NoContent from "../../../../components/status/NoContent";
import ServerError from "../../../../components/status/ServerError";
import useApiStore from "../../../../hooks/useApiStore";
import { Routes } from "../../../../routes/Routes";
import { is5xxStatus, isNotFoundOrNoContent } from "../../../../utils/isStatus";

import MovieCard from "./MovieCard";
import MovieCardSkeleton from "./MovieCardSkeleton";

const breakpoints: SimpleGridBreakpoint[] = [
	{ minWidth: "xs", cols: 3 },
	{ minWidth: "sm", cols: 4 },
	{ minWidth: "md", cols: 4 },
	{ minWidth: "lg", cols: 5 },
	{ minWidth: "xl", cols: 5 }
];

const MovieAlbum = () => {
	const url = useNavigate();
	const { movieStore } = useApiStore();
	const locationSearch = useLocation().search;
	const movies = movieStore.responses?.data;

	useEffect(() => {
		runInAction(() => {
			movieStore.setUrlSearchString(new URLSearchParams(locationSearch));
			movieStore.modifyUrl(Routes.Movies, url);
		});
	}, [url, movieStore, locationSearch]);

	useEffect(() => {
		runInAction(() => {
			if (movieStore.responses) return;
			void movieStore.getAll();
		});
	}, [movieStore]);

	const modifyUrl = useCallback(() => {
		movieStore.modifyUrl(Routes.Movies, url);
		void movieStore.getAll();
	}, [url, movieStore]);

	if (is5xxStatus(movieStore.status.getAll)) return <ServerError />;
	if (isNotFoundOrNoContent(movieStore.status.getAll))
		return <NoContent message="There are no movies available..." />;

	return (
		<>
			{movieStore.loading.getAll && (
				<SimpleGrid breakpoints={breakpoints} cols={2} spacing="sm" verticalSpacing="xl">
					{[...Array.from({ length: movieStore.queryParameters.size }).keys()].map((number) => {
						return <MovieCardSkeleton key={number} />;
					})}
				</SimpleGrid>
			)}

			{!movieStore.loading.getAll && movies && (
				<SimpleGrid breakpoints={breakpoints} cols={2} spacing="sm" verticalSpacing="xl">
					{movies.map((movie) => {
						return <MovieCard key={movie.id} movie={movie} />;
					})}
				</SimpleGrid>
			)}
			<Pagination modifyUrl={modifyUrl} store={movieStore} />
		</>
	);
};

export default observer(MovieAlbum);
