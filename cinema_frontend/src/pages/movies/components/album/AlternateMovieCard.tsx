//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { AspectRatio, Card, Image, Skeleton, Text } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { useCallback } from "react";
import { Link } from "react-router-dom";

import useNavbarStore from "../../../../hooks/useNavbarStore";
import { Routes } from "../../../../routes/Routes";
import MovieStyle from "../../../../styles/Movies.module.scss";
import { type AlternateMovieCardProps } from "../../movies";

import MovieRatingProgress from "./MovieRatingProgress";

const movieCardTitleStyle = MovieStyle[`movie-card-title`];

const AlternateMovieCard = ({ movie, withRating }: AlternateMovieCardProps) => {
	const navbarStore = useNavbarStore();

	const setMoviesActive = useCallback(() => {
		navbarStore.setActive(Routes.Movies);
	}, [navbarStore]);

	return (
		<Card className="w-44 select-none rounded-2xl md:w-48 xl:w-52" shadow="md" withBorder>
			<Card.Section>
				<AspectRatio ratio={9 / 12}>
					<Link onClick={setMoviesActive} to={`${Routes.MovieDetails}/${movie.id}`}>
						<Image
							fit="contain"
							placeholder={<Skeleton height="50vh" m={0} width="50vw" />}
							src={movie.posterUrl}
							withPlaceholder
						/>
					</Link>
				</AspectRatio>
				{withRating && <MovieRatingProgress movie={movie} />}
			</Card.Section>

			<Text align="center" className="break-words text-base lg:text-lg" mt="1.5rem" size="lg" weight={500}>
				<Text
					className={movieCardTitleStyle}
					component={Link}
					inherit
					onClick={setMoviesActive}
					to={`${Routes.MovieDetails}/${movie.id}`}
				>
					{movie.title}
				</Text>
			</Text>
		</Card>
	);
};

export default observer(AlternateMovieCard);
