//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { RingProgress, Text } from "@mantine/core";
import { observer } from "mobx-react-lite";

import { type SimpleMovieProps } from "../../../../types/movies";
import { useNumberFormatter } from "../../../../utils/converters";

const getColourFromRating = (rating: number): string => {
	if (rating < 1) {
		return "dark";
	} else if (rating >= 1 && rating < 1.5) {
		return "red";
	} else if (rating >= 1.5 && rating < 2) {
		return "orange.7";
	} else if (rating >= 2 && rating < 2.5) {
		return "orange.5";
	} else if (rating >= 2.5 && rating < 3) {
		return "yellow.7";
	} else if (rating >= 3 && rating < 3.5) {
		return "lime.5";
	} else if (rating >= 3.5 && rating < 4) {
		return "lime.6";
	} else if (rating >= 4 && rating <= 4.5) {
		return "green.5";
	} else {
		return "green.7";
	}
};

const MovieRatingProgress = ({ movie }: SimpleMovieProps) => {
	const numberFormatter = useNumberFormatter(1);

	const rating = movie.rating * 20;
	const colour = getColourFromRating(movie.rating);

	return (
		<div className="absolute top-0 right-0">
			<RingProgress
				className="rounded-full bg-[#081c22]"
				label={
					<Text align="center" className="text-sm" color="white" inherit>
						{numberFormatter.format(movie.rating)}/5
					</Text>
				}
				roundCaps
				sections={[
					{
						value: rating,
						color: colour
					}
				]}
				size={60}
				thickness={4}
			/>
		</div>
	);
};

export default observer(MovieRatingProgress);
