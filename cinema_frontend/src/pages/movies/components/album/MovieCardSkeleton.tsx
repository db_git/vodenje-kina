//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Card, Skeleton } from "@mantine/core";

import MovieStyle from "../../../../styles/Movies.module.scss";

const movieCardStyle = MovieStyle[`movie-card`];

const MovieCardSkeleton = () => {
	return (
		<Card className={movieCardStyle} p="lg" shadow="sm" withBorder>
			<Card.Section>
				<Skeleton
					className="min-h-[14rem] sm:min-h-[15.5rem] md:min-h-[17rem] lg:min-h-[18rem] xl:min-h-[15.5rem]"
					miw="100%"
				/>
			</Card.Section>

			<Skeleton height="1rem" m={0} mb="5rem" mt="2.75rem" width="100%" />
		</Card>
	);
};

export default MovieCardSkeleton;
