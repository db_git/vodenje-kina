//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Badge, Center, Group, Loader, Text } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";

import useApiStore from "../../../../hooks/useApiStore";
import useGetStore from "../../../../hooks/useGetStore";
import { MovieQueryParameterOptions, QueryParameterOptions } from "../../../../types/enums/Stores";
import { is2xxStatus } from "../../../../utils/isStatus";
import { type MovieGenresFilterProps } from "../../movies";

const gradient = { from: "indigo", to: "cyan" };
const mantineBadge = { root: "cursor-pointer select-none" };

const GenresFilter = ({ margin, marginTop, marginRight, marginBottom, marginLeft }: MovieGenresFilterProps) => {
	const { movieStore } = useApiStore();
	const { genreGetStore } = useGetStore();
	const genreResponse = genreGetStore.responses?.data;

	useEffect(() => {
		runInAction(() => {
			if (genreResponse) return;

			genreGetStore.setQueryParameters(QueryParameterOptions.Size, 100);
			void genreGetStore.getAll();
		});
	}, [genreResponse, genreGetStore]);

	const changeGenres = (genreName: string) => {
		if (Array.isArray(movieStore.queryParameters.genres)) {
			if (movieStore.queryParameters.genres.includes(genreName)) {
				movieStore.setMovieQueryParameters(
					MovieQueryParameterOptions.Genres,
					movieStore.queryParameters.genres.filter((value: string) => {
						return value !== genreName;
					})
				);
			} else {
				movieStore.setMovieQueryParameters(MovieQueryParameterOptions.Genres, [
					...movieStore.queryParameters.genres,
					genreName
				]);
			}
		} else {
			movieStore.setMovieQueryParameters(MovieQueryParameterOptions.Genres, [genreName]);
		}
	};

	let genreOptions: JSX.Element;
	if (genreGetStore.loading.getAll)
		genreOptions = (
			<Center>
				<Loader size="sm" />
			</Center>
		);
	else if (is2xxStatus(genreGetStore.status.getAll) && genreResponse)
		genreOptions = (
			<Group spacing="xs">
				{genreResponse.map((genre) => {
					return (
						<Badge
							classNames={mantineBadge}
							color="indigo"
							gradient={gradient}
							key={genre.id}
							onClick={() => {
								runInAction(() => {
									changeGenres(genre.name);
								});
							}}
							size="lg"
							variant={movieStore.queryParameters.genres?.includes(genre.name) ? "gradient" : "outline"}
						>
							{genre.name}
						</Badge>
					);
				})}
			</Group>
		);
	else genreOptions = <Text color="red">Failed to retrieve genres.</Text>;

	return (
		<>
			<Text m={margin} mb={marginBottom} ml={marginLeft} mr={marginRight} mt={marginTop}>
				Genres
			</Text>
			{genreOptions}
		</>
	);
};

export default observer(GenresFilter);
