//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Select } from "@mantine/core";
import { observer } from "mobx-react-lite";

import useApiStore from "../../../../hooks/useApiStore";
import { QueryParameterOptions } from "../../../../types/enums/Stores";
import { type MoviesPerPageFilterProps } from "../../movies";

const moviesPerPage = ["5", "10", "15", "20", "25", "30", "50"];

const MoviesPerPageFilter = ({
	margin,
	marginTop,
	marginRight,
	marginBottom,
	marginLeft
}: MoviesPerPageFilterProps) => {
	const { movieStore } = useApiStore();

	const changeMoviesPerPage = (value: string) => {
		movieStore.setMovieQueryParameters(QueryParameterOptions.Size, Number.parseInt(value, 10));
	};

	return (
		<Select
			data={moviesPerPage}
			label="Movies per page"
			m={margin}
			mb={marginBottom}
			ml={marginLeft}
			mr={marginRight}
			mt={marginTop}
			onChange={changeMoviesPerPage}
			value={movieStore.queryParameters.size?.toString()}
		/>
	);
};

export default observer(MoviesPerPageFilter);
