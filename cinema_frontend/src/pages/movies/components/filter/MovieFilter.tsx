//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import {
	type AccordionStylesNames,
	type AccordionStylesParams,
	type Styles,
	Accordion,
	Button,
	Stack
} from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback, useEffect, useState } from "react";
import { BiCameraMovie } from "react-icons/bi";
import { BsPlus } from "react-icons/bs";
import { useLocation, useNavigate } from "react-router-dom";

import useApiStore from "../../../../hooks/useApiStore";
import { Routes } from "../../../../routes/Routes";
import { MovieQueryParameterOptions } from "../../../../types/enums/Stores";

import FilmRatingFilter from "./FilmRatingFilter";
import GenresFilter from "./GenresFilter";
import KeywordsFilter from "./KeywordsFilter";
import MoviesPerPageFilter from "./MoviesPerPageFilter";
import SortFilter from "./SortFilter";
import UpcomingFilter from "./UpcomingFilter";
import YearFilter from "./YearFilter";

const marginTop = "0.5rem";
const accordionStyle: Styles<AccordionStylesNames, AccordionStylesParams> = {
	chevron: {
		"&[data-rotate]": {
			transform: "rotate(45deg)"
		}
	}
};

const MovieFilter = () => {
	const url = useNavigate();
	const locationSearch = useLocation().search;
	const { movieStore } = useApiStore();

	const [opened, setOpened] = useState(false);

	useEffect(() => {
		runInAction(() => {
			movieStore.setUrlSearchString(new URLSearchParams(locationSearch));
			movieStore.modifyUrl(Routes.Movies, url);
		});
	}, [url, movieStore, locationSearch]);

	const handleOnChange = useCallback(() => {
		if (!opened) setOpened(true);
	}, [opened]);

	const applyFilters = useCallback(() => {
		runInAction(() => {
			movieStore.setMovieQueryParameters(MovieQueryParameterOptions.Page, 1);
			movieStore.modifyUrl(Routes.Movies, url);
		});
		void movieStore.getAll();
	}, [movieStore, url]);

	return (
		<Accordion chevron={<BsPlus />} radius="lg" styles={accordionStyle} variant="contained">
			<Accordion.Item onClick={handleOnChange} value="movie-filter">
				<Accordion.Control icon={<BiCameraMovie size={20} />}>Sort and filter</Accordion.Control>
				<Accordion.Panel>
					<Stack>
						<Button
							color="cyan"
							loading={movieStore.loading.getAll}
							mt={marginTop}
							onClick={applyFilters}
							radius={6}
							size="md"
						>
							Search
						</Button>

						<KeywordsFilter marginTop={marginTop} />
						<SortFilter marginTop={marginTop} />
						<MoviesPerPageFilter marginTop={marginTop} />
						<UpcomingFilter marginTop={marginTop} />
						<YearFilter marginTop={marginTop} />
						{opened && (
							<>
								<GenresFilter marginTop={marginTop} />
								<FilmRatingFilter marginTop={marginTop} />
							</>
						)}
					</Stack>
				</Accordion.Panel>
			</Accordion.Item>
		</Accordion>
	);
};

export default observer(MovieFilter);
