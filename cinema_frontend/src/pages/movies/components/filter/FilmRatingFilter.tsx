//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Badge, Center, Group, Loader, Text } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";

import useApiStore from "../../../../hooks/useApiStore";
import useGetStore from "../../../../hooks/useGetStore";
import { MovieQueryParameterOptions, QueryParameterOptions } from "../../../../types/enums/Stores";
import { is2xxStatus } from "../../../../utils/isStatus";
import { type MovieFilmRatingFilterProps } from "../../movies";

const gradient = { from: "teal", to: "lime", deg: 105 };
const mantineBadge = { root: "cursor-pointer select-none" };

const FilmRatingFilter = ({ margin, marginTop, marginRight, marginBottom, marginLeft }: MovieFilmRatingFilterProps) => {
	const { movieStore } = useApiStore();
	const { filmRatingGetStore } = useGetStore();
	const filmRatingResponse = filmRatingGetStore.responses?.data;

	useEffect(() => {
		runInAction(() => {
			if (filmRatingResponse) return;

			filmRatingGetStore.setQueryParameters(QueryParameterOptions.Size, 100);
			void filmRatingGetStore.getAll();
		});
	}, [filmRatingResponse, filmRatingGetStore]);

	const changeFilmRating = (filmRatingType: string) => {
		if (movieStore.queryParameters.filmRating === filmRatingType) {
			movieStore.setMovieQueryParameters(MovieQueryParameterOptions.FilmRating, undefined);
		} else {
			movieStore.setMovieQueryParameters(MovieQueryParameterOptions.FilmRating, filmRatingType);
		}
	};

	let filmRatingOptions: JSX.Element;
	if (filmRatingGetStore.loading.getAll)
		filmRatingOptions = (
			<Center>
				<Loader size="sm" />
			</Center>
		);
	else if (is2xxStatus(filmRatingGetStore.status.getAll) && filmRatingResponse)
		filmRatingOptions = (
			<Group position="apart">
				{filmRatingResponse.map((fr) => {
					return (
						<Badge
							classNames={mantineBadge}
							color="violet"
							gradient={gradient}
							key={fr.id}
							onClick={() => {
								runInAction(() => {
									changeFilmRating(fr.type);
								});
							}}
							size="lg"
							variant={movieStore.queryParameters.filmRating === fr.type ? "gradient" : "outline"}
						>
							{fr.type}
						</Badge>
					);
				})}
			</Group>
		);
	else filmRatingOptions = <Text color="red">Failed to retrieve film ratings.</Text>;

	return (
		<>
			<Text m={margin} mb={marginBottom} ml={marginLeft} mr={marginRight} mt={marginTop}>
				Film rating
			</Text>
			{filmRatingOptions}
		</>
	);
};

export default observer(FilmRatingFilter);
