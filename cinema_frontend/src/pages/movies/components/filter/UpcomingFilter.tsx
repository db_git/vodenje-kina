//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Switch, useMantineTheme } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { type ChangeEvent, useEffect, useState } from "react";
import { TbCheck, TbX } from "react-icons/tb";

import useApiStore from "../../../../hooks/useApiStore";
import { MovieQueryParameterOptions } from "../../../../types/enums/Stores";
import { type MovieUpcomingFilterProps } from "../../movies";

const UpcomingFilter = ({ margin, marginTop, marginRight, marginBottom, marginLeft }: MovieUpcomingFilterProps) => {
	const theme = useMantineTheme();
	const { movieStore } = useApiStore();
	const [checked, setChecked] = useState<boolean>(false);

	useEffect(() => {
		runInAction(() => {
			if (movieStore.queryParameters.showUpcoming) {
				setChecked(true);
			}
		});
	}, [movieStore.queryParameters.showUpcoming]);

	const toggle = (event: ChangeEvent<HTMLInputElement>) => {
		if (event.currentTarget.checked) {
			movieStore.setMovieQueryParameters(MovieQueryParameterOptions.ShowUpcoming, 1);
			setChecked(true);
		} else {
			movieStore.setMovieQueryParameters(MovieQueryParameterOptions.ShowUpcoming, 0);
			setChecked(false);
		}
	};

	const thumbIcon = checked ? (
		<TbCheck color={theme.colors.teal[theme.fn.primaryShade()]} size={16} />
	) : (
		<TbX color={theme.colors.red[theme.fn.primaryShade()]} size={16} />
	);

	return (
		<Switch
			checked={checked}
			color="teal"
			label="Show upcoming only?"
			m={margin}
			mb={marginBottom}
			ml={marginLeft}
			mr={marginRight}
			mt={marginTop}
			onChange={toggle}
			size="md"
			thumbIcon={thumbIcon}
		/>
	);
};

export default observer(UpcomingFilter);
