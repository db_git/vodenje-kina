//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Switch, NumberInput, RangeSlider, useMantineTheme } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { type ChangeEvent, type ReactNode, useEffect, useState } from "react";
import { TbCheck, TbX } from "react-icons/tb";

import useApiStore from "../../../../hooks/useApiStore";
import { MovieQueryParameterOptions } from "../../../../types/enums/Stores";
import { type MovieYearFilterProps } from "../../movies";

const currentYear = new Date().getFullYear();
const max = currentYear - (currentYear % 10) + 15;
const min = 1_980;
const marks: Array<{ value: number; label?: ReactNode }> = [
	{ value: min, label: min },
	{ value: max, label: max }
];

const YearFilter = ({ margin, marginTop, marginRight, marginBottom, marginLeft }: MovieYearFilterProps) => {
	const theme = useMantineTheme();
	const { movieStore } = useApiStore();
	const [checked, setChecked] = useState<boolean>(true);

	useEffect(() => {
		runInAction(() => {
			if (checked && movieStore.queryParameters.yearStart && movieStore.queryParameters.yearEnd) {
				setChecked(false);
			}
		});
	}, [checked, movieStore.queryParameters.yearEnd, movieStore.queryParameters.yearStart]);

	const rangeValue: [number, number] =
		movieStore.queryParameters.yearStart && movieStore.queryParameters.yearEnd
			? [movieStore.queryParameters.yearStart, movieStore.queryParameters.yearEnd]
			: [currentYear - 10, currentYear];

	const changeYear = (value: number | undefined) => {
		movieStore.setMovieQueryParameters(MovieQueryParameterOptions.Year, value);
	};

	const changeYearRange = (value: number[]) => {
		if (value.length > 0) {
			movieStore.setMovieQueryParameters(MovieQueryParameterOptions.YearStart, value[0]);
			movieStore.setMovieQueryParameters(MovieQueryParameterOptions.YearEnd, value[1]);
		} else {
			movieStore.setMovieQueryParameters(MovieQueryParameterOptions.YearStart, undefined);
			movieStore.setMovieQueryParameters(MovieQueryParameterOptions.YearEnd, undefined);
		}
	};

	const toggle = (event: ChangeEvent<HTMLInputElement>) => {
		if (event.currentTarget.checked) {
			movieStore.setMovieQueryParameters(MovieQueryParameterOptions.YearStart, undefined);
			movieStore.setMovieQueryParameters(MovieQueryParameterOptions.YearEnd, undefined);
		} else {
			movieStore.setMovieQueryParameters(MovieQueryParameterOptions.Year, undefined);
			movieStore.setMovieQueryParameters(MovieQueryParameterOptions.YearStart, currentYear - 10);
			movieStore.setMovieQueryParameters(MovieQueryParameterOptions.YearEnd, currentYear);
		}

		setChecked(event.currentTarget.checked);
	};

	const thumbIcon = checked ? (
		<TbCheck color={theme.colors.teal[theme.fn.primaryShade()]} size={16} />
	) : (
		<TbX color={theme.colors.red[theme.fn.primaryShade()]} size={16} />
	);

	return (
		<>
			<Switch
				checked={checked}
				color="green"
				label="Exact year?"
				m={margin}
				mb={marginBottom}
				ml={marginLeft}
				mr={marginRight}
				mt={marginTop}
				onChange={toggle}
				size="md"
				thumbIcon={thumbIcon}
			/>

			{checked ? (
				<NumberInput label="Year" onChange={changeYear} value={movieStore.queryParameters.year} />
			) : (
				<RangeSlider
					marks={marks}
					max={max}
					min={min}
					my="0.75rem"
					onChange={changeYearRange}
					value={rangeValue}
				/>
			)}
		</>
	);
};

export default observer(YearFilter);
