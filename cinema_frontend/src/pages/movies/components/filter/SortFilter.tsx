//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Select } from "@mantine/core";
import { observer } from "mobx-react-lite";

import useApiStore from "../../../../hooks/useApiStore";
import { MovieOrderBy } from "../../../../types/enums/OrderBy";
import { QueryParameterOptions } from "../../../../types/enums/Stores";
import { type MovieSortFilterProps } from "../../movies";

const movieSort = [
	{ value: MovieOrderBy.TitleAscending.toString(), label: "Title, ascending" },
	{ value: MovieOrderBy.TitleDescending.toString(), label: "Title, descending" },
	{ value: MovieOrderBy.YearAscending.toString(), label: "Year, ascending" },
	{ value: MovieOrderBy.YearDescending.toString(), label: "Year, descending" }
];

const SortFilter = ({ margin, marginTop, marginRight, marginBottom, marginLeft }: MovieSortFilterProps) => {
	const { movieStore } = useApiStore();

	const changeSort = (value: string) => {
		movieStore.setMovieQueryParameters(QueryParameterOptions.OrderBy, Number.parseInt(value, 10));
	};

	return (
		<Select
			data={movieSort}
			label="Sort"
			m={margin}
			mb={marginBottom}
			ml={marginLeft}
			mr={marginRight}
			mt={marginTop}
			onChange={changeSort}
			value={movieStore.queryParameters.orderBy?.toString(10)}
		/>
	);
};

export default observer(SortFilter);
