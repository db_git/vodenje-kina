//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Center } from "@mantine/core";
import { observer } from "mobx-react-lite";

import Loading from "../../../components/status/Loading";
import useGetStore from "../../../hooks/useGetStore";
import { type MoviesProps } from "../home";

import MovieCarousel from "./MovieCarousel";
import TopMovie from "./TopMovie";

const TopMovies = ({ movies }: MoviesProps) => {
	const { movieGetStore } = useGetStore();

	const loading = movieGetStore.homePageMoviesLoading;

	if (!loading && (!movies || !movies.length)) return null;

	return (
		<div className="mb-4">
			{loading && (
				<Center>
					<Loading noHeight />
				</Center>
			)}

			{!loading && movies && (
				<MovieCarousel autoplay={false} loop={false} withControls withIndicators={false}>
					{movies?.map((movie) => {
						return (
							<MovieCarousel.Slide key={movie.id}>
								<TopMovie movie={movie} />
							</MovieCarousel.Slide>
						);
					})}
				</MovieCarousel>
			)}
		</div>
	);
};

export default observer(TopMovies);
