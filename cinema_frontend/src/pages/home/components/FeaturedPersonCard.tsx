//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { AspectRatio, Image, Skeleton, Stack, Text } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { useCallback } from "react";
import { Link } from "react-router-dom";

import useNavbarStore from "../../../hooks/useNavbarStore";
import { Routes } from "../../../routes/Routes";
import MovieStyle from "../../../styles/Movies.module.scss";
import { type PersonProps } from "../../person/person";
import getAge from "../../person/utils/getAge";

const mantineFeaturedPersonImage = { root: "rounded-full" };
const textHover = MovieStyle[`movie-card-title`];

const FeaturedPersonCard = ({ person }: PersonProps) => {
	const navbarStore = useNavbarStore();
	const age = person.dateOfDeath
		? `${new Date(person.dateOfBirth).getUTCFullYear()} - ${new Date(person.dateOfDeath).getUTCFullYear()}`
		: getAge(person.dateOfBirth);

	const removeActiveLink = useCallback(() => {
		navbarStore.setActive("*");
	}, [navbarStore]);

	return (
		<Stack>
			<Link
				className="mb-2 ml-3.5 w-40 bg-transparent"
				onClick={removeActiveLink}
				to={`${Routes.PersonDetails}/${person.id}`}
			>
				<AspectRatio ratio={15 / 16}>
					<Image
						classNames={mantineFeaturedPersonImage}
						placeholder={<Skeleton className="absolute h-96 w-96" />}
						radius={192}
						src={person.imageUrl}
						withPlaceholder
					/>
				</AspectRatio>
			</Link>

			<Text
				align="center"
				component={Link}
				onClick={removeActiveLink}
				size="md"
				to={`${Routes.PersonDetails}/${person.id}`}
				weight={800}
			>
				<span className={textHover}>
					<Text>{person.name}</Text>
				</span>
			</Text>

			<Text align="center" mt="0.25rem" size="md">
				{age}
			</Text>
		</Stack>
	);
};

export default observer(FeaturedPersonCard);
