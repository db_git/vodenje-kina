//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Carousel as EmblaCarousel } from "@mantine/carousel";
import { useMantineTheme } from "@mantine/core";
import classNames from "classnames";
import Autoplay from "embla-carousel-autoplay";
import { useCallback, useRef } from "react";

import { type MovieCarouselProps } from "../home";

const carouselStyle = {
	control: {
		height: "1.75rem",
		width: "1.75rem",
		"&[data-inactive]": {
			opacity: 0,
			cursor: "default"
		}
	},
	indicator: {
		height: "1rem",
		width: "1rem"
	}
};

const MovieCarousel = ({ autoplay, loop, withControls, withIndicators, children }: MovieCarouselProps) => {
	const theme = useMantineTheme();
	const handleAutoplay = useRef(Autoplay({ delay: 10_000 }));

	const onMouseLeave = useCallback(() => {
		handleAutoplay.current.play();
	}, []);

	const carouselClassNames = {
		indicator: classNames(
			{ "bg-white": theme.colorScheme === "dark" },
			{ "bg-black": theme.colorScheme === "light" }
		),
		control: classNames(
			{ "bg-slate-900": theme.colorScheme === "light" },
			{ "border-slate-900": theme.colorScheme === "light" },
			{ "stroke-white": theme.colorScheme === "light" },
			{ "stroke-black": theme.colorScheme === "dark" },
			{ "stroke-1": true }
		)
	};

	return (
		<EmblaCarousel
			classNames={carouselClassNames}
			controlsOffset="xs"
			initialSlide={0}
			loop={loop}
			onMouseEnter={autoplay ? handleAutoplay.current.stop : undefined}
			onMouseLeave={autoplay ? onMouseLeave : undefined}
			plugins={autoplay ? [handleAutoplay.current] : undefined}
			slideGap="xl"
			slideSize="100%"
			styles={carouselStyle}
			withControls={withControls}
			withIndicators={withIndicators}
		>
			{children}
		</EmblaCarousel>
	);
};

MovieCarousel.Slide = EmblaCarousel.Slide;

export default MovieCarousel;
