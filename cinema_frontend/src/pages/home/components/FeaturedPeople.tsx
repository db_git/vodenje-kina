//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Text } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";

import Heading from "../../../components/display/Heading";
import HorizontalScroll from "../../../components/display/HorizontalScroll";
import NoPeople from "../../../components/status/NoPeople";
import useGetStore from "../../../hooks/useGetStore";
import useLanguage from "../../../hooks/useLanguage";
import { PersonQueryParameterOptions } from "../../../types/enums/Stores";
import { isNotFoundOrNoContent } from "../../../utils/isStatus";

import FeaturedPersonCard from "./FeaturedPersonCard";
import FeaturedPersonCardSkeleton from "./FeaturedPersonCardSkeleton";

const FeaturedPeople = () => {
	const { personGetStore } = useGetStore();
	const lang = useLanguage();

	const loading = personGetStore.loading.getAll;

	useEffect(() => {
		const today = new Date();

		personGetStore.setPersonQueryParameters(PersonQueryParameterOptions.Day, today.getDate());
		personGetStore.setPersonQueryParameters(PersonQueryParameterOptions.Month, today.getMonth() + 1);
		personGetStore.setPersonQueryParameters(PersonQueryParameterOptions.Page, 1);
		personGetStore.setPersonQueryParameters(PersonQueryParameterOptions.Size, 25);
		void personGetStore.getAll();

		return () => {
			personGetStore.setPersonQueryParameters(PersonQueryParameterOptions.Day, undefined);
			personGetStore.setPersonQueryParameters(PersonQueryParameterOptions.Month, undefined);
			personGetStore.setPersonQueryParameters(PersonQueryParameterOptions.Page, 1);
			personGetStore.setPersonQueryParameters(PersonQueryParameterOptions.Size, 10);
		};
	}, [personGetStore]);

	return (
		<div id="people">
			<Heading anchor="#people" className="mt-8" size={2}>
				Born today
			</Heading>
			<Text color="dimmed" mb="2rem" size="lg">
				People born on {new Date().toLocaleDateString(lang, { month: "long", day: "numeric" })}
			</Text>

			{loading && (
				<HorizontalScroll slidesToScroll={2} withControls>
					{[...Array.from({ length: 8 }).keys()].map((number) => {
						return (
							<HorizontalScroll.Item key={number}>
								<FeaturedPersonCardSkeleton />
							</HorizontalScroll.Item>
						);
					})}
				</HorizontalScroll>
			)}

			{!loading && isNotFoundOrNoContent(personGetStore.status.getAll) && (
				<NoPeople message="There are no people born on this day..." />
			)}

			{!loading && personGetStore.responses?.data && (
				<HorizontalScroll slidesToScroll={2} withControls>
					{personGetStore.responses.data.map((actor) => {
						return (
							<HorizontalScroll.Item key={actor.id}>
								<FeaturedPersonCard person={actor} />
							</HorizontalScroll.Item>
						);
					})}
				</HorizontalScroll>
			)}
		</div>
	);
};

export default observer(FeaturedPeople);
