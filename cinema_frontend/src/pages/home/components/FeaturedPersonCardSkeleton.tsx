//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Skeleton, Stack } from "@mantine/core";

const FeaturedPersonCardSkeleton = () => {
	return (
		<Stack align="center">
			<Skeleton className="h-44 w-44 rounded-full" />
			<Skeleton height="0.9rem" mt="0.5rem" width="65%" />
			<Skeleton height="0.75rem" mt="0.8rem" width="25%" />
		</Stack>
	);
};

export default FeaturedPersonCardSkeleton;
