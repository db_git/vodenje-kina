//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Center } from "@mantine/core";
import { observer } from "mobx-react-lite";

import Heading from "../../../components/display/Heading";
import Loading from "../../../components/status/Loading";
import useGetStore from "../../../hooks/useGetStore";
import { type NewMoviesProps } from "../home";

import MovieCarousel from "./MovieCarousel";
import MovieCarouselSlide from "./MovieCarouselSlide";

const divStyle = { marginBottom: "2rem" };

const MoviesCarousel = ({ title, anchor, movies, autoplay, loop, withControls, withIndicators }: NewMoviesProps) => {
	const { movieGetStore } = useGetStore();

	const loading = movieGetStore.homePageMoviesLoading;

	if (!loading && (!movies || !movies.length)) return null;

	return (
		<div id={anchor.slice(1)} style={divStyle}>
			<Heading anchor={anchor} className="ml-4 mt-2 mb-2" size={2}>
				{title}
			</Heading>

			{loading && (
				<Center my="10rem">
					<Loading noHeight />
				</Center>
			)}

			{!loading && movies && (
				<MovieCarousel
					autoplay={autoplay}
					loop={loop}
					withControls={withControls}
					withIndicators={withIndicators}
				>
					{movies.map((movie) => {
						return (
							<MovieCarousel.Slide key={movie.id}>
								<MovieCarouselSlide movie={movie} />
							</MovieCarousel.Slide>
						);
					})}
				</MovieCarousel>
			)}
		</div>
	);
};

export default observer(MoviesCarousel);
