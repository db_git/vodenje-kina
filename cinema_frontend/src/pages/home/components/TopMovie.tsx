//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Center, Container, Stack, Text } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { Link } from "react-router-dom";

import Loading from "../../../components/status/Loading";
import useGetStore from "../../../hooks/useGetStore";
import { Routes } from "../../../routes/Routes";
import MovieStyle from "../../../styles/Movies.module.scss";
import { type MovieProps } from "../../../types/movies";
import MovieBackgroundImage from "../../movieDetails/components/MovieBackgroundImage";
import MoviePaper from "../../movieDetails/components/MoviePaper";

const textHover = MovieStyle[`movie-card-title`];

const TopMovie = ({ movie }: MovieProps) => {
	const { movieGetStore } = useGetStore();

	const loading = movieGetStore.homePageMoviesLoading;

	if (loading)
		return (
			<div className="mt-4 py-12">
				<Center>
					<Loading noHeight />
				</Center>
			</div>
		);

	return (
		<div className="-mt-4">
			<MovieBackgroundImage movie={movie}>
				<MoviePaper paddingBottom="24rem" paddingTop="18rem">
					<Stack align="center" justify="center" spacing="xl">
						<Text
							align="center"
							className="m-0 select-none text-2xl font-bold uppercase underline sm:text-3xl lg:text-4xl"
							component={Link}
							inherit
							inline
							span
							to={`${Routes.MovieDetails}/${movie.id}`}
						>
							<span className={textHover}>{movie.title}</span>
						</Text>

						<Container size="lg">
							<p className="text-md m-0 my-3 select-none text-center italic sm:text-lg lg:text-xl">
								{movie.tagline}
							</p>
						</Container>
					</Stack>
				</MoviePaper>
			</MovieBackgroundImage>
		</div>
	);
};

export default observer(TopMovie);
