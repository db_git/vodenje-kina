//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { observer } from "mobx-react-lite";

import Heading from "../../../components/display/Heading";
import HorizontalScroll from "../../../components/display/HorizontalScroll";
import useGetStore from "../../../hooks/useGetStore";
import AlternateMovieCard from "../../movies/components/album/AlternateMovieCard";
import AlternateMovieCardSkeleton from "../../movies/components/album/AlternateMovieCardSkeleton";

const RunningMovies = () => {
	const { movieGetStore } = useGetStore();

	const loading = movieGetStore.homePageMoviesLoading;
	const movies = movieGetStore.homePageMovies?.runningMovies;

	if (!loading && (!movies || !movies.length)) return null;

	return (
		<div id="running">
			<Heading anchor="#running" className="ml-4 mb-8" size={2}>
				Running
			</Heading>

			{loading && (
				<HorizontalScroll slidesToScroll={2} withControls>
					{[...Array.from({ length: 6 }).keys()].map((number) => {
						return (
							<HorizontalScroll.Item key={number}>
								<AlternateMovieCardSkeleton />
							</HorizontalScroll.Item>
						);
					})}
				</HorizontalScroll>
			)}

			{!loading && movies && (
				<HorizontalScroll slidesToScroll={2} withControls>
					{movies.map((movie) => {
						return (
							<HorizontalScroll.Item key={movie.id}>
								<AlternateMovieCard movie={movie} />
							</HorizontalScroll.Item>
						);
					})}
				</HorizontalScroll>
			)}
		</div>
	);
};

export default observer(RunningMovies);
