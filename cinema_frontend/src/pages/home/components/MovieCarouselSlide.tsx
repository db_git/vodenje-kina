//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Space, Stack, Text } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { Link } from "react-router-dom";

import { Routes } from "../../../routes/Routes";
import MovieStyle from "../../../styles/Movies.module.scss";
import { type MovieProps } from "../../../types/movies";
import MovieBackgroundImage from "../../movieDetails/components/MovieBackgroundImage";

const textHover = MovieStyle[`movie-card-title`];

const MovieCarouselSlide = ({ movie }: MovieProps) => {
	return (
		<div className="my-4 mb-12">
			<MovieBackgroundImage movie={movie} radius="xl">
				<Stack
					align="center"
					className="rounded-b-3xl bg-gradient-to-t from-black to-transparent"
					justify="flex-end"
				>
					<Space className="h-[25rem] md:h-[30rem] xl:h-[35rem]" />
					<Text
						className="text-md m-0 mb-2 max-w-[70%] truncate font-bold text-white md:text-2xl"
						component={Link}
						inherit
						inline
						span
						to={`${Routes.MovieDetails}/${movie.id}`}
					>
						<span className={textHover}>{movie.title}</span>
					</Text>
				</Stack>
			</MovieBackgroundImage>
		</div>
	);
};

export default observer(MovieCarouselSlide);
