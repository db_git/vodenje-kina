//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type Movie } from "../../types/entities";
import { type ChildrenProps } from "../../types/shared";

export type NewMoviesProps = CommonCarouselProps & {
	readonly movies?: Movie[];
	readonly title: string;
	readonly anchor: string;
};

export type MoviesProps = {
	readonly movies: Movie[];
};

export type MovieCarouselProps = ChildrenProps & CommonCarouselProps;

export type CommonCarouselProps = {
	readonly autoplay: boolean;
	readonly loop: boolean;
	readonly withIndicators: boolean;
	readonly withControls: boolean;
};
