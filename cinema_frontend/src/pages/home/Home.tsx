//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Container } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";

import ServerError from "../../components/status/ServerError";
import useGetStore from "../../hooks/useGetStore";
import useNavbarStore from "../../hooks/useNavbarStore";
import { Routes } from "../../routes/Routes";
import { is5xxStatus } from "../../utils/isStatus";

import FeaturedPeople from "./components/FeaturedPeople";
import MoviesCarousel from "./components/MoviesCarousel";
import RunningMovies from "./components/RunningMovies";
import TopMovie from "./components/TopMovie";
import TopMovies from "./components/TopMovies";

const Home = () => {
	const navbarStore = useNavbarStore();
	const { movieGetStore, personGetStore } = useGetStore();

	useEffect(() => {
		const today = new Date();

		runInAction(() => {
			if (!movieGetStore.homePageMovies) {
				void movieGetStore.getHomePageMovies({
					newMoviesLimit: 7,
					featuredMoviesLimit: 10,
					recommendedMoviesLimit: 7,
					runningMoviesLimit: 5,
					runningMoviesMonth: today.getMonth() + 1,
					runningMoviesDay: today.getDate()
				});
			}
		});
	}, [movieGetStore]);

	useEffect(() => {
		navbarStore.setActive(Routes.Home);
		document.title = "Home";
	});

	if (is5xxStatus(movieGetStore.homePageMoviesStatus) || is5xxStatus(personGetStore.status.getAll))
		return <ServerError />;

	return (
		<>
			{movieGetStore.homePageMovies && movieGetStore.homePageMovies.featuredMovies.length > 0 && (
				<>
					<TopMovie movie={movieGetStore.homePageMovies.featuredMovies[0]} />
					{movieGetStore.homePageMovies.featuredMovies.length > 1 && (
						<TopMovies movies={movieGetStore.homePageMovies.featuredMovies.slice(1)} />
					)}
				</>
			)}

			<Container fluid my={0}>
				<MoviesCarousel
					anchor="#new"
					autoplay
					loop
					movies={movieGetStore.homePageMovies?.newMovies}
					title="New"
					withControls={false}
					withIndicators
				/>
				<MoviesCarousel
					anchor="#recommended"
					autoplay
					loop
					movies={movieGetStore.homePageMovies?.recommendedMovies}
					title="Recommended"
					withControls={false}
					withIndicators
				/>
			</Container>

			<Container size="xl">
				<RunningMovies />
				<FeaturedPeople />
			</Container>
		</>
	);
};

export default observer(Home);
