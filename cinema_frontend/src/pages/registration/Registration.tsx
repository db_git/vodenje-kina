//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Center, Container } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { useEffect, useState } from "react";
import { Navigate } from "react-router-dom";

import { ReactComponent as ForgotPasswordSvg } from "../../assets/illustrations/ForgotPassword.svg";
import { ReactComponent as LoginSvg } from "../../assets/illustrations/Login.svg";
import { ReactComponent as RegisterSvg } from "../../assets/illustrations/Register.svg";
import EmailSent from "../../components/status/EmailSent";
import useApiStore from "../../hooks/useApiStore";
import useNavbarStore from "../../hooks/useNavbarStore";
import { Routes } from "../../routes/Routes";

import ForgotPasswordForm from "./components/ForgotPasswordForm";
import LoginForm from "./components/LoginForm";
import RegisterForm from "./components/RegisterForm";

const illustrationStyle = "h-full w-full max-w-sm pb-5";

// eslint-disable-next-line complexity
const Registration = () => {
	const { authStore } = useApiStore();
	const navbarStore = useNavbarStore();
	const [showLoginForm, setShowLoginForm] = useState(true);
	const [forgotPassword, setForgotPassword] = useState(false);

	useEffect(() => {
		navbarStore.setActive(Routes.Registration);
		document.title = showLoginForm ? "Login" : "Register";
	});

	if (authStore.isLoggedIn) {
		return <Navigate to={Routes.Home} />;
	}

	return (
		<Container size="sm">
			<Center>
				{!authStore.verificationEmailSent &&
					!authStore.forgotPasswordEmailSent &&
					showLoginForm &&
					forgotPassword && <ForgotPasswordSvg className={illustrationStyle} />}
				{!authStore.verificationEmailSent && showLoginForm && !forgotPassword && (
					<LoginSvg className={illustrationStyle} />
				)}
				{!authStore.verificationEmailSent && !showLoginForm && <RegisterSvg className={illustrationStyle} />}
			</Center>

			{authStore.verificationEmailSent && (
				<EmailSent
					className={illustrationStyle}
					message="Verification e-mail has been sent. Please check your inbox and spam folders."
				/>
			)}
			{authStore.forgotPasswordEmailSent && (
				<EmailSent
					className={illustrationStyle}
					message="Password reset e-mail has been sent. Please check your inbox and spam folders."
				/>
			)}

			{!authStore.verificationEmailSent &&
				!authStore.forgotPasswordEmailSent &&
				showLoginForm &&
				forgotPassword && <ForgotPasswordForm setForgotPassword={setForgotPassword} />}
			{!authStore.verificationEmailSent && showLoginForm && !forgotPassword && (
				<LoginForm setForgotPassword={setForgotPassword} setShowLoginForm={setShowLoginForm} />
			)}
			{!authStore.verificationEmailSent && !showLoginForm && <RegisterForm setShowLoginForm={setShowLoginForm} />}
		</Container>
	);
};

export default observer(Registration);
