//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Center, PasswordInput, Stack, Text, TextInput } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { useCallback } from "react";
import { MdOutlineMail } from "react-icons/md";
import { SlLock } from "react-icons/sl";

import { ObserverForm } from "../../../components/forms/Form";
import useApiStore from "../../../hooks/useApiStore";
import { loginFormValidationSchema } from "../../../schemas/registrationSchemas";
import { type LoginFormProps } from "../registration";

const LoginForm = ({ setShowLoginForm, setForgotPassword }: LoginFormProps) => {
	const { authStore } = useApiStore();

	const showLogin = useCallback(() => {
		setShowLoginForm(false);
	}, [setShowLoginForm]);

	const showForgotPassword = useCallback(() => {
		setForgotPassword(true);
	}, [setForgotPassword]);

	return (
		<ObserverForm
			initialValues={{
				email: "",
				password: ""
			}}
			onSubmit={authStore.login}
			schema={loginFormValidationSchema}
		>
			{(form) => {
				return (
					<Stack>
						<TextInput
							disabled={authStore.loading}
							icon={<MdOutlineMail />}
							label="E-Mail"
							placeholder="Enter your e-mail"
							required
							{...form.getInputProps("email")}
						/>
						<PasswordInput
							disabled={authStore.loading}
							icon={<SlLock />}
							label="Password"
							placeholder="Enter your password"
							required
							{...form.getInputProps("password")}
						/>
						<Button loading={authStore.loading} type="submit">
							Login
						</Button>
						<Center>
							<Text color="dimmed" mt="1rem" onClick={showLogin} size="sm">
								<span className="cursor-pointer hover:underline">
									Don&apos;t have an account? Sign up!
								</span>
							</Text>
						</Center>

						<Center>
							<Text color="dimmed" onClick={showForgotPassword} size="sm">
								<span className="cursor-pointer hover:underline">Forgot your password?</span>
							</Text>
						</Center>
					</Stack>
				);
			}}
		</ObserverForm>
	);
};

export default observer(LoginForm);
