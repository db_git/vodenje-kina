//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Center, PasswordInput, Stack, Text, TextInput } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { useCallback } from "react";
import { MdOutlineMail, MdTitle } from "react-icons/md";
import { SlLock } from "react-icons/sl";

import { ObserverForm } from "../../../components/forms/Form";
import useApiStore from "../../../hooks/useApiStore";
import { registerFormValidationSchema } from "../../../schemas/registrationSchemas";
import { type RegisterFormProps } from "../registration";

const RegisterForm = ({ setShowLoginForm }: RegisterFormProps) => {
	const { authStore } = useApiStore();

	const showLogin = useCallback(() => {
		setShowLoginForm(true);
	}, [setShowLoginForm]);

	return (
		<ObserverForm
			initialValues={{
				name: "",
				email: "",
				password: ""
			}}
			onSubmit={authStore.register}
			schema={registerFormValidationSchema}
		>
			{(form) => {
				return (
					<Stack>
						<TextInput
							icon={<MdTitle />}
							label="Name"
							placeholder="Enter your full name"
							required
							{...form.getInputProps("name")}
						/>
						<TextInput
							icon={<MdOutlineMail />}
							label="E-Mail"
							placeholder="Enter your e-mail"
							required
							{...form.getInputProps("email")}
						/>
						<PasswordInput
							icon={<SlLock />}
							label="Password"
							placeholder="Enter your password"
							required
							{...form.getInputProps("password")}
						/>
						<Button loading={authStore.loading} type="submit">
							Register
						</Button>
						<Center>
							<Text color="dimmed" mt="1rem" onClick={showLogin} size="sm">
								<span className="cursor-pointer hover:underline">
									Already have an account? Login instead!
								</span>
							</Text>
						</Center>
					</Stack>
				);
			}}
		</ObserverForm>
	);
};

export default observer(RegisterForm);
