//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Center, Stack, Text, TextInput } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { useCallback } from "react";
import { MdOutlineMail } from "react-icons/md";

import { ObserverForm } from "../../../components/forms/Form";
import useApiStore from "../../../hooks/useApiStore";
import { forgotPasswordFormValidationSchema } from "../../../schemas/registrationSchemas";
import { type ForgotPasswordFormProps } from "../registration";

const ForgotPasswordForm = ({ setForgotPassword }: ForgotPasswordFormProps) => {
	const { authStore } = useApiStore();

	const showForgotPassword = useCallback(() => {
		setForgotPassword(false);
	}, [setForgotPassword]);

	const handleSubmit = useCallback(
		(values: { email: string }) => {
			void authStore.forgotPassword(values.email);
		},
		[authStore]
	);

	return (
		<ObserverForm
			initialValues={{
				email: ""
			}}
			onSubmit={handleSubmit}
			schema={forgotPasswordFormValidationSchema}
		>
			{(form) => {
				return (
					<Stack>
						<TextInput
							disabled={authStore.loading}
							icon={<MdOutlineMail />}
							label="E-Mail"
							placeholder="Enter your e-mail"
							required
							{...form.getInputProps("email")}
						/>
						<Button loading={authStore.loading} type="submit">
							Send reset link
						</Button>

						<Center>
							<Text color="dimmed" onClick={showForgotPassword} size="sm">
								<span className="cursor-pointer hover:underline">Remembered your password? Login!</span>
							</Text>
						</Center>
					</Stack>
				);
			}}
		</ObserverForm>
	);
};

export default observer(ForgotPasswordForm);
