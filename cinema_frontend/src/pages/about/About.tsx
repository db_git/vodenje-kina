//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Container, Stack, Text } from "@mantine/core";
import React, { useEffect } from "react";

import Heading from "../../components/display/Heading";
import useNavbarStore from "../../hooks/useNavbarStore";
import { Routes } from "../../routes/Routes";

import Attributions from "./components/Attributions";
import Services from "./components/Services";
import Technologies from "./components/Technologies";

const About = () => {
	const navbarStore = useNavbarStore();

	useEffect(() => {
		navbarStore.setActive(Routes.About);
		document.title = "About";
	});

	return (
		<Container size="lg">
			<Stack align="flex-start" spacing="xl">
				<div id="about">
					<Heading anchor="#about" size={2}>
						About
					</Heading>
					<Text align="justify" mt="1.5rem">
						The goal of this final thesis is to enable unregistered and registered users to view information
						about the movies and to buy a ticket for the selected movie. The information includes the
						release year of the movie, duration of the movie, genres, actors and directors. Also, the user
						has an overview of the halls in which the film is shown and an overview of occupied seats.
						Registered users can book seats, buy drinks, snacks, souvenirs and leave a comment on the movie
						they watched. Administrators have the ability to remove, edit and add genres, movies, actors,
						seats, halls, drinks and snacks.
					</Text>
				</div>

				<Technologies />
				<Services />
				<Attributions />
			</Stack>
		</Container>
	);
};

export default About;
