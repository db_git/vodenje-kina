//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { List } from "@mantine/core";
import React from "react";

import Heading from "../../../components/display/Heading";

import ListItem from "./ListItem";

const Technologies = () => {
	return (
		<div id="technologies">
			<Heading anchor="#technologies" className="mt-4" size={2}>
				Technologies
			</Heading>
			<List center mt="1.5rem" spacing="xl" withPadding>
				<List.Item>
					<ListItem link="https://dotnet.microsoft.com" message="for backend" name=".NET Core" />
				</List.Item>

				<List.Item>
					<ListItem
						link="https://learn.microsoft.com/ef/core"
						message="as ORM"
						name="Entity Framework Core"
					/>
				</List.Item>

				<List.Item>
					<ListItem link="https://www.postgresql.org" message="for database" name="PostgreSQL" />
				</List.Item>

				<List.Item>
					<ListItem link="https://nodatime.org" message="as better date and time API" name="Noda Time" />
				</List.Item>

				<List.Item>
					<ListItem link="https://autofac.org" message="as IoC and DI container" name="Autofac" />
				</List.Item>

				<List.Item>
					<ListItem link="https://automapper.org" message="as entity to DTO mapper" name="Automapper" />
				</List.Item>

				<List.Item>
					<ListItem
						link="https://www.docker.com"
						message="for deploying images using containerization"
						name="Docker"
					/>
				</List.Item>

				<List.Item>
					<ListItem link="https://reactjs.org" message="for frontend" name="React" />
				</List.Item>

				<List.Item>
					<ListItem
						link="https://www.typescriptlang.org"
						message="as prefered frontend language"
						name="TypeScript"
					/>
				</List.Item>

				<List.Item>
					<ListItem link="https://mobx.js.org/README.html" message="for state management" name="MobX" />
				</List.Item>

				<List.Item>
					<ListItem link="https://axios-http.com/docs/intro" message="for fetching data" name="Axios" />
				</List.Item>

				<List.Item>
					<ListItem link="https://reactrouter.com" message="for routing" name="React Router" />
				</List.Item>

				<List.Item>
					<ListItem link="https://day.js.org" message="for parsing dates" name="Day.js" />
				</List.Item>

				<List.Item>
					<ListItem link="https://mantine.dev" message="as component library" name="Mantine UI" />
				</List.Item>

				<List.Item>
					<ListItem link="https://tailwindcss.com" message="for utility classes" name="Tailwind CSS" />
				</List.Item>

				<List.Item>
					<ListItem link="https://sass-lang.com" message="for advanced styling" name="SCSS" />
				</List.Item>

				<List.Item>
					<ListItem link="https://eslint.org" message="for code linting" name="ESLint" />
				</List.Item>

				<List.Item>
					<ListItem link="https://prettier.io" message="for code formatting" name="Prettier" />
				</List.Item>

				<List.Item>
					<ListItem
						link="https://vitejs.dev"
						message="for faster and better development experience"
						name="Vite"
					/>
				</List.Item>
			</List>
		</div>
	);
};

export default Technologies;
