//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Anchor, List } from "@mantine/core";
import React from "react";

import Heading from "../../../components/display/Heading";

const Attributions = () => {
	return (
		<div id="attributions">
			<Heading anchor="#attributions" className="mt-4" size={2}>
				Attributions
			</Heading>
			<List center mt="1.5rem" spacing="xl" withPadding>
				<List.Item>
					<Anchor
						className="block"
						href="https://github.com/react-icons/react-icons"
						rel="noopener noreferrer"
						target="_blank"
					>
						React Icons
					</Anchor>
				</List.Item>

				<List.Item>
					<Anchor
						className="block"
						href="https://github.com/weiweihuanghuang/Work-Sans"
						rel="noopener noreferrer"
						target="_blank"
					>
						Work Sans font
					</Anchor>
				</List.Item>

				<List.Item>
					<Anchor className="block" href="https://storyset.com" rel="noopener noreferrer" target="_blank">
						Illustrations by Storyset
					</Anchor>
				</List.Item>

				<List.Item>
					<Anchor className="block" href="https://icons8.com" rel="noopener noreferrer" target="_blank">
						Favicon by Icons8
					</Anchor>
				</List.Item>
			</List>
		</div>
	);
};

export default Attributions;
