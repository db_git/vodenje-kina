//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Anchor, Text } from "@mantine/core";
import React from "react";

import { type ListItemProps } from "../about";

const ListItem = ({ name, link, message }: ListItemProps) => {
	return (
		<Text>
			<Text span weight={500}>
				<Anchor href={link} rel="noopener noreferrer" target="_blank">
					{name}
				</Anchor>
			</Text>
			&nbsp;{message}
		</Text>
	);
};

export default ListItem;
