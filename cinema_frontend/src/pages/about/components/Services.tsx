//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { List } from "@mantine/core";
import React from "react";

import Heading from "../../../components/display/Heading";

import ListItem from "./ListItem";

const Services = () => {
	return (
		<div id="services">
			<Heading anchor="#services" className="mt-4" size={2}>
				Services
			</Heading>
			<List center mt="1.5rem" spacing="xl" withPadding>
				<List.Item>
					<ListItem link="https://www.render.com/" message="for hosting" name="Render" />
				</List.Item>

				<List.Item>
					<ListItem link="https://www.imagekit.io/" message="as image CDN" name="ImageKit" />
				</List.Item>

				<List.Item>
					<ListItem link="https://www.sendgrid.com/" message="for email delivery" name="SendGrid" />
				</List.Item>
			</List>
		</div>
	);
};

export default Services;
