//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type BackgroundImageProps } from "@mantine/core";
import { type MantineNumberSize } from "@mantine/styles/lib/theme/types/MantineSize";
import { type Dayjs } from "dayjs";
import { type Dispatch, type SetStateAction } from "react";

import {
	type Souvenir,
	type Seat,
	type Food,
	type Movie,
	type UserComment,
	type Projection
} from "../../types/entities";
import { type IndividualMovieProps } from "../../types/movies";
import {
	type OptionalClassNameProps,
	type ChildrenProps,
	type OptionalMarginProps,
	type OptionalPaddingProps
} from "../../types/shared";

export type MovieProjectionsProps = IndividualMovieProps;

export type ProjectionCardProps = {
	readonly projection: Projection;
};

export type ProjectionCalendarProps = {
	readonly dateFormat: string;
	readonly projectionDates: string[];
	readonly startDate: Dayjs;
	readonly endDate: Dayjs;
	readonly selectedCalendarDate: Date | null;
	readonly setSelectedCalendarDate: Dispatch<SetStateAction<Date | null>>;
};

export type ProjectionScrollProps = {
	readonly selectedCalendarDate: Date | null;
	readonly dateFormat: string;
	readonly projections: Projection[];
};

export type IconSizeProps = {
	readonly iconSize: number;
};

export type FontWeightProps = {
	readonly fontWeight: number;
};

export type MovieActorsProps = IndividualMovieProps;
export type MovieDirectorsProps = FontWeightProps & IndividualMovieProps;
export type MovieGenresProps = IndividualMovieProps;

export type MovieBackgroundImageProps = ChildrenProps &
	Omit<BackgroundImageProps, "children" | "src"> & {
		readonly movie: Pick<Movie, "backdropUrl" | "id">;
	};

export type MovieDurationProps = FontWeightProps & IconSizeProps & IndividualMovieProps;
export type MovieFilmRatingProps = FontWeightProps & IconSizeProps & IndividualMovieProps;
export type MovieImageProps = IndividualMovieProps;
export type MovieInformationProps = IndividualMovieProps;

export type MoviePaperProps = ChildrenProps &
	OptionalMarginProps &
	OptionalPaddingProps & {
		readonly alphaDark?: number;
		readonly alphaLight?: number;
		readonly radius?: MantineNumberSize;
		readonly inline?: boolean;

		readonly mih?: number | string;
		readonly miw?: number | string;
		readonly mah?: number | string;
		readonly maw?: number | string;
		readonly h?: number | string;
		readonly w?: number | string;
	};

export type MovieRecommendationsProps = IndividualMovieProps;
export type MovieTrailerProps = FontWeightProps & IndividualMovieProps;
export type MovieYearProps = FontWeightProps & IconSizeProps & IndividualMovieProps;
export type MovieReviewsProps = IndividualMovieProps;

export type ReviewCardProps = {
	readonly review: UserComment;
	readonly withMenu: boolean;
};

export type ReviewSpoilerProps = {
	readonly maxHeight: number;
	readonly review: UserComment;
};

export type ReviewAuthorProps = {
	readonly review: UserComment;
	readonly showAvatar: boolean;
	readonly withMenu: boolean;
};

export type ReviewPaperProps = ChildrenProps &
	OptionalClassNameProps &
	OptionalMarginProps & {
		readonly radius?: MantineNumberSize;
		readonly withBorder?: boolean;
	};

export type UserCommentFormSubmitValues = {
	readonly movie: string;
	readonly comment: string;
};

export type UserRatingFormSubmitValues = {
	readonly movie: string;
	readonly rating: number;
};

export type WriteReviewProps =
	| {
			readonly movieId: string;
			readonly edit: true;
			readonly comment: string;
	  }
	| {
			readonly movieId: string;
			readonly edit?: false;
			readonly comment?: string;
	  };

export type MovieRatingsProps = IndividualMovieProps;
export type EmptyRatingsProps = IndividualMovieProps;

export type UserRatingProps = {
	readonly rating: number | null;
	readonly movieId: string | null;
};

export type IProjectionOrderStore = {
	get orderStep(): number;
	readonly setOrderStep: (step: number) => void;

	get selectedSeats(): Seat[];
	readonly setSelectedSeats: (selectedSeats: Seat[]) => void;

	get selectedProjection(): Projection | undefined;
	readonly setSelectedProjection: (selectedProjection: Projection | undefined) => void;

	get selectedFoods(): SelectedFood[];
	readonly setSelectedFoods: (selectedFoods: SelectedFood[]) => void;

	get selectedSouvenirs(): SelectedSouvenir[];
	readonly setSelectedSouvenirs: (selectedSouvenirs: SelectedSouvenir[]) => void;
};

export type SelectedFood = {
	food: Food;
	quantity;
};

export type SelectedSouvenir = {
	souvenir: Souvenir;
	quantity: number;
};

export type FoodCardProps = {
	readonly food: Food;
};

export type SelectedFoodCardProps = {
	readonly food: Food;
};

export type SouvenirCardProps = {
	readonly souvenir: Souvenir;
};

export type SelectedSouvenirCardProps = {
	readonly souvenir: Souvenir;
};

export type QuantityInputProps = {
	readonly min?: number;
	readonly max?: number;
	readonly value?: number;
	readonly setValue: (value: number | undefined) => void;
};

export type OrderedGoodCardProps = {
	readonly imageUrl: string;
	readonly name: string;
	readonly quantity: number;
};
