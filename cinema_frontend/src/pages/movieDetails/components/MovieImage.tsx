//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Card, Image, Skeleton } from "@mantine/core";
import classNames from "classnames";
import { observer } from "mobx-react-lite";

import { type MovieImageProps } from "../movieDetails";

const cardStyle = classNames(
	"h-72 w-48 min-h-full min-w-[12rem]",
	"sm:h-96 sm:w-64",
	"md:h-[27rem] md:w-72",
	"lg:w-screen lg:h-screen lg:max-w-[22.25rem] lg:max-h-[32rem]",
	"bg-transparent rounded-3xl"
);

const MovieImage = ({ movie }: MovieImageProps) => {
	return (
		<Card className={cardStyle} m={0} p={0}>
			<Card.Section>
				<Image placeholder={<Skeleton height="100vh" width="100vw" />} src={movie.posterUrl} withPlaceholder />
			</Card.Section>
		</Card>
	);
};

export default observer(MovieImage);
