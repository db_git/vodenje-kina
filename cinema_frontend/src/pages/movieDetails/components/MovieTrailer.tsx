//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { AspectRatio, Group, Modal, Text } from "@mantine/core";
import { useMediaQuery } from "@mantine/hooks";
import { observer } from "mobx-react-lite";
import { useState } from "react";
import { BsFillPlayFill } from "react-icons/bs";

import { type MovieTrailerProps } from "../movieDetails";

const MovieTrailer = ({ movie, fontWeight }: MovieTrailerProps) => {
	const [open, setOpen] = useState(false);
	const isMobile = useMediaQuery("(max-width: 1024px)");

	const closeModal = () => {
		setOpen(false);
	};

	const openModal = () => {
		setOpen(true);
	};

	return (
		<>
			<Group>
				<span className="cursor-pointer">
					<Group onClick={openModal}>
						<BsFillPlayFill size={28} />
						<Text fw={fontWeight} underline>
							Watch trailer
						</Text>
					</Group>
				</span>
			</Group>
			<Modal
				centered
				onClose={closeModal}
				opened={open}
				size={isMobile ? "100%" : "65%"}
				title={movie.title + " trailer"}
			>
				<AspectRatio ratio={16 / 9}>
					<iframe
						allowFullScreen
						sandbox="allow-same-origin allow-scripts"
						src={movie.trailerUrl}
						width="100%"
					/>
				</AspectRatio>
			</Modal>
		</>
	);
};

export default observer(MovieTrailer);
