//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Stack, Text, Tooltip } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { BiStar } from "react-icons/bi";

import { type MovieFilmRatingProps } from "../movieDetails";

const MovieFilmRating = ({ iconSize, movie, fontWeight }: MovieFilmRatingProps) => {
	if (!movie.filmRating) return null;

	return (
		<Tooltip
			label={movie.filmRating.description}
			multiline
			transition="slide-right"
			transitionDuration={425}
			width={200}
			withArrow
		>
			<Stack align="center" justify="center">
				<BiStar size={iconSize} />
				<Text align="center" fw={fontWeight} size="md">
					{movie.filmRating.type}
				</Text>
			</Stack>
		</Tooltip>
	);
};

export default observer(MovieFilmRating);
