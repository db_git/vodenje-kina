//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Badge, useMantineColorScheme } from "@mantine/core";
import { observer } from "mobx-react-lite";

import { type GenreProps } from "../../../../types/movies";

const badgeStyle = { root: "select-none" };

const GenreBadge = ({ genre }: GenreProps) => {
	const { colorScheme } = useMantineColorScheme();
	const isDark = colorScheme === "dark";

	return (
		<Badge
			classNames={badgeStyle}
			color={isDark ? "orange" : "dark"}
			m={2}
			size="lg"
			variant={isDark ? "light" : "filled"}
		>
			{genre.name}
		</Badge>
	);
};

export default observer(GenreBadge);
