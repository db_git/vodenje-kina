//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Grid, Text } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { BsInfoCircle } from "react-icons/bs";

import Heading from "../../../components/display/Heading";
import { type MovieInformationProps } from "../movieDetails";

import MovieBackgroundImage from "./MovieBackgroundImage";
import MovieDuration from "./MovieDuration";
import MovieFilmRating from "./MovieFilmRating";
import MovieImage from "./MovieImage";
import MoviePaper from "./MoviePaper";
import MovieTrailer from "./MovieTrailer";
import MovieYear from "./MovieYear";
import MovieDirectors from "./directors/MovieDirectors";
import MovieGenres from "./genres/MovieGenres";

const iconSize = 28;
const fontWeight = 500;

const MovieInformation = ({ movie }: MovieInformationProps) => {
	return (
		<div className="-mt-4">
			<MovieBackgroundImage movie={movie}>
				<MoviePaper paddingBottom="4rem" paddingLeft="2.5rem" paddingRight="2.5rem" paddingTop="4rem">
					<Grid align="center" justify="center">
						<Grid.Col span={12}>
							<Heading
								className="mb-8 flex items-center justify-center text-center"
								icon={false}
								size={1}
							>
								{movie.title}
							</Heading>
						</Grid.Col>

						<Grid.Col className="flex items-center justify-center" lg={5} my="1rem" sm={6} xs={12}>
							<MovieImage movie={movie} />
						</Grid.Col>

						<Grid.Col sm={6} xs={12}>
							<Grid align="flex-start" justify="flex-start">
								<Grid.Col my="1rem" span={12}>
									<MovieGenres movie={movie} />
								</Grid.Col>

								<Grid.Col my="1rem" span={12}>
									<Grid align="flex-start" grow justify="space-between">
										<Grid.Col span={4}>
											<MovieYear fontWeight={fontWeight} iconSize={iconSize} movie={movie} />
										</Grid.Col>

										<Grid.Col span={4}>
											<MovieDuration fontWeight={fontWeight} iconSize={iconSize} movie={movie} />
										</Grid.Col>

										<Grid.Col span={4}>
											<MovieFilmRating
												fontWeight={fontWeight}
												iconSize={iconSize}
												movie={movie}
											/>
										</Grid.Col>
									</Grid>
								</Grid.Col>

								<Grid.Col span={12}>
									<MovieTrailer fontWeight={fontWeight} movie={movie} />
								</Grid.Col>

								<Grid.Col my="1rem" span={12}>
									<Text fw={fontWeight} italic size="lg">
										{movie.tagline}
									</Text>
								</Grid.Col>

								<Grid.Col span={12}>
									<Text align="justify" fw={fontWeight} size="xl">
										<BsInfoCircle size={18} />
										&nbsp;{movie.summary}
									</Text>
								</Grid.Col>

								<Grid.Col my="1rem" span={12}>
									<MovieDirectors fontWeight={fontWeight} movie={movie} />
								</Grid.Col>
							</Grid>
						</Grid.Col>
					</Grid>
				</MoviePaper>
			</MovieBackgroundImage>
		</div>
	);
};

export default observer(MovieInformation);
