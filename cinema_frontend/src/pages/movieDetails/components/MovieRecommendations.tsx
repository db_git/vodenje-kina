//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";

import Heading from "../../../components/display/Heading";
import HorizontalScroll from "../../../components/display/HorizontalScroll";
import useGetStore from "../../../hooks/useGetStore";
import AlternateMovieCard from "../../movies/components/album/AlternateMovieCard";
import AlternateMovieCardSkeleton from "../../movies/components/album/AlternateMovieCardSkeleton";
import { type MovieRecommendationsProps } from "../movieDetails";

const MovieRecommendations = ({ movie }: MovieRecommendationsProps) => {
	const { movieGetStore } = useGetStore();

	const loading = movieGetStore.recommendationsLoading;
	const recommendations = movieGetStore.recommendations;

	useEffect(() => {
		runInAction(() => {
			void movieGetStore.getRecommendations(movie.id);
		});
	}, [movie.id, movieGetStore]);

	if (!loading && (!recommendations || !recommendations.length)) return null;

	return (
		<div id="recommendations">
			<Heading anchor="#recommendations" className="mb-4 mt-14" size={1}>
				Recommendations
			</Heading>

			{loading && (
				<HorizontalScroll slidesToScroll={2} withControls>
					{[...Array.from({ length: 6 }).keys()].map((number) => {
						return (
							<HorizontalScroll.Item key={number}>
								<AlternateMovieCardSkeleton />
							</HorizontalScroll.Item>
						);
					})}
				</HorizontalScroll>
			)}

			{!loading && recommendations && (
				<HorizontalScroll slidesToScroll={2} withControls>
					{recommendations.map((recommendation) => {
						return (
							<HorizontalScroll.Item key={recommendation.id}>
								<AlternateMovieCard movie={recommendation} />
							</HorizontalScroll.Item>
						);
					})}
				</HorizontalScroll>
			)}
		</div>
	);
};

export default observer(MovieRecommendations);
