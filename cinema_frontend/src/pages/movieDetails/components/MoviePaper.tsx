//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type Sx, Paper } from "@mantine/core";

import { type MoviePaperProps } from "../movieDetails";

const MoviePaper = ({
	children,
	inline,
	alphaDark,
	alphaLight,
	radius,
	margin,
	marginTop,
	marginRight,
	marginBottom,
	marginLeft,
	mih,
	miw,
	mah,
	maw,
	// eslint-disable-next-line id-length
	h,
	// eslint-disable-next-line id-length
	w,
	padding,
	paddingTop,
	paddingRight,
	paddingBottom,
	paddingLeft
}: MoviePaperProps) => {
	const paperStyle: Array<Sx | undefined> | Sx | undefined = (theme) => {
		return {
			minHeight: mih,
			maxHeight: mah,
			height: h,
			minWidth: miw,
			maxWidth: maw,
			width: w,
			display: inline ? "inline-block" : "block",
			backgroundColor:
				theme.colorScheme === "dark" ? `hsla(0, 0%, 0%, ${alphaDark})` : `hsla(0, 0%, 100%, ${alphaLight})`
		};
	};

	return (
		<Paper
			m={margin}
			mb={marginBottom}
			ml={marginLeft}
			mr={marginRight}
			mt={marginTop}
			p={padding}
			pb={paddingBottom}
			pl={paddingLeft}
			pr={paddingRight}
			pt={paddingTop}
			radius={radius}
			sx={paperStyle}
		>
			{children}
		</Paper>
	);
};

MoviePaper.defaultProps = {
	alphaDark: 0.85,
	alphaLight: 0.55,
	inline: false,
	paddingLeft: "1rem",
	paddingRight: "1rem",
	radius: 0
};

export default MoviePaper;
