//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Grid, Group, Progress, Rating, Stack, Text } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { Fragment } from "react";

import { type EmptyRatingsProps } from "../../movieDetails";

import UserRating from "./UserRating";

const stars = [5, 4, 3, 2, 1];

const EmptyRatings = ({ movie }: EmptyRatingsProps) => {
	return (
		<Stack>
			<UserRating movieId={movie.id} rating={null} />
			<Group spacing="xs">
				<Rating color="yellow.5" defaultValue={0} fractions={1} readOnly size="lg" />
				<Text>0 out of 5</Text>
			</Group>

			<Text c="dimmed" mt="0.5rem" size="sm">
				0 global ratings
			</Text>

			<Grid align="center" columns={32} grow gutter="xs">
				{stars.map((star) => {
					return (
						<Fragment key={star}>
							<Grid.Col span={6}>
								<Text color="blue" fw={600} size="sm">
									{star} star
								</Text>
							</Grid.Col>
							<Grid.Col span={22}>
								<Progress className="py-3" color="yellow.5" radius="md" size="xl" value={0} />
							</Grid.Col>
							<Grid.Col span={2}>
								<Text color="blue" fw={600} size="sm">
									0%
								</Text>
							</Grid.Col>
						</Fragment>
					);
				})}
			</Grid>
		</Stack>
	);
};

export default observer(EmptyRatings);
