//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { ActionIcon, Group, Rating, Text } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback } from "react";
import { IoMdTrash } from "react-icons/io";

import useApiStore from "../../../../hooks/useApiStore";
import useGetStore from "../../../../hooks/useGetStore";
import { type UserRating as UserRatingType } from "../../../../types/entities";
import { JsonPatchOperation } from "../../../../types/enums/Http";
import { type JsonPatch } from "../../../../types/shared";
import { type UserRatingProps } from "../../movieDetails";

const UserRating = ({ rating, movieId }: UserRatingProps) => {
	const { authStore, movieStore, userRatingStore } = useApiStore();
	const { userRatingGetStore } = useGetStore();

	const deleteRating = useCallback(async () => {
		await runInAction(async () => {
			if (movieId !== null && authStore.getUserId !== null) {
				await userRatingStore.deleteRating(movieId, authStore.getUserId);
				await userRatingGetStore.get(movieId);
				await movieStore.getAll();
			}
		});
	}, [movieStore, authStore.getUserId, movieId, userRatingGetStore, userRatingStore]);

	const changeRating = useCallback(
		async (value: number) => {
			if (movieId === null) return;

			if (rating === null) {
				await userRatingStore.post({
					movie: movieId,
					rating: value
				});
			} else {
				const patch: Array<JsonPatch<UserRatingType>> = [
					{
						op: JsonPatchOperation.Replace,
						value,
						path: "/rating"
					}
				];

				await userRatingStore.updateRating(movieId, patch);
			}

			await userRatingGetStore.get(movieId);
			await movieStore.getAll();
		},
		[movieId, rating, userRatingGetStore, movieStore, userRatingStore]
	);

	if (!authStore.isLoggedIn) return null;

	return (
		<Group>
			<Text>Your rating:</Text>
			<Rating defaultValue={rating ?? 0} onChange={changeRating} size="md" />
			{rating && (
				<ActionIcon color="red">
					<IoMdTrash onClick={deleteRating} />
				</ActionIcon>
			)}
		</Group>
	);
};

export default observer(UserRating);
