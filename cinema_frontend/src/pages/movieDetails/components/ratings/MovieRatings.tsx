//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Grid, Group, Progress, Rating, Stack, Text } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { Fragment, useEffect } from "react";

import useApiStore from "../../../../hooks/useApiStore";
import useGetStore from "../../../../hooks/useGetStore";
import { HttpMethod, HttpStatusCode } from "../../../../types/enums/Http";
import { useNumberFormatter } from "../../../../utils/converters";
import { type MovieRatingsProps } from "../../movieDetails";

import EmptyRatings from "./EmptyRatings";
import RatingsSkeleton from "./RatingsSkeleton";
import UserRating from "./UserRating";

const MovieRatings = ({ movie }: MovieRatingsProps) => {
	const { userRatingStore } = useApiStore();
	const { userRatingGetStore } = useGetStore();
	const numberFormatter = useNumberFormatter(2);

	const loading =
		userRatingGetStore.loading.get ||
		userRatingStore.loading.get ||
		userRatingStore.loading.post ||
		userRatingStore.loading.patch ||
		userRatingStore.loading.delete;
	const rating = userRatingGetStore.response;

	useEffect(() => {
		return () => {
			userRatingStore.setResponse(undefined);
			userRatingStore.setStatus(HttpMethod.Get, 0);
			userRatingStore.setResponses(undefined);
			userRatingStore.setStatus(HttpMethod.GetAll, 0);
			userRatingGetStore.setResponse(undefined);
			userRatingGetStore.setStatus(HttpMethod.Get, 0);
			userRatingGetStore.setResponses(undefined);
			userRatingGetStore.setStatus(HttpMethod.GetAll, 0);
		};
	}, [userRatingGetStore, userRatingStore]);

	useEffect(() => {
		runInAction(() => {
			void userRatingGetStore.get(movie.id);
		});
	}, [movie.id, userRatingGetStore, userRatingStore]);

	if (loading) return <RatingsSkeleton />;
	if (!loading && (rating === undefined || userRatingGetStore.status.get === HttpStatusCode.NoContent))
		return <EmptyRatings movie={movie} />;

	if (!rating) return null;

	return (
		<Stack>
			<UserRating movieId={movie.id} rating={rating.myRating} />
			<Group spacing="xs">
				<Rating color="yellow.5" defaultValue={rating.totalRating} fractions={4} readOnly size="lg" />
				<Text>{numberFormatter.format(rating.totalRating)} out of 5</Text>
			</Group>

			<Text c="dimmed" mt="0.5rem" size="sm">
				{rating.ratingsCount} global ratings
			</Text>

			<Grid align="center" columns={32} grow gutter="xs">
				{rating.ratingStars.map((ratingStar) => {
					return (
						<Fragment key={`${ratingStar.count}_${ratingStar.star}`}>
							<Grid.Col span={6}>
								<Text color="blue" fw={600} size="sm">
									{ratingStar.star} star
								</Text>
							</Grid.Col>
							<Grid.Col span={20}>
								<Progress
									className="py-3"
									color="yellow.5"
									radius="md"
									size="xl"
									value={(ratingStar.count / rating?.ratingsCount) * 100}
								/>
							</Grid.Col>
							<Grid.Col span={4}>
								<Text color="blue" fw={600} size="sm">
									{numberFormatter.format((ratingStar.count / rating?.ratingsCount) * 100)}%
								</Text>
							</Grid.Col>
						</Fragment>
					);
				})}
			</Grid>
		</Stack>
	);
};

export default observer(MovieRatings);
