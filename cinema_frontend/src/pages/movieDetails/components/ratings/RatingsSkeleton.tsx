//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Grid, Group, Rating, Skeleton, Stack, Text } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { Fragment } from "react";

import useApiStore from "../../../../hooks/useApiStore";

const stars = [5, 4, 3, 2, 1];

const RatingsSkeleton = () => {
	const { authStore } = useApiStore();

	return (
		<Stack>
			{authStore.isLoggedIn && (
				<Group>
					<Text>Your rating:</Text>
					<Rating defaultValue={0} fractions={1} readOnly size="md" />
				</Group>
			)}

			<Group spacing="xs">
				<Rating color="yellow.5" defaultValue={0} fractions={4} readOnly size="lg" />
				<Skeleton animate h="1.25rem" w="8rem" />
			</Group>

			<Skeleton animate h="1.25rem" w="7rem" />

			<Grid align="center" columns={32} grow gutter="xs">
				{stars.map((star) => {
					return (
						<Fragment key={star}>
							<Grid.Col span={6}>
								<Text color="blue" fw={600} size="sm">
									{star} star
								</Text>
							</Grid.Col>
							<Grid.Col span={26}>
								<Skeleton animate h="1.5rem" radius="md" w="100%" />
							</Grid.Col>
						</Fragment>
					);
				})}
			</Grid>
		</Stack>
	);
};

export default observer(RatingsSkeleton);
