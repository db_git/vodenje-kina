//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { AspectRatio, Card, Image, Skeleton, Text } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { Link } from "react-router-dom";

import useNavbarStore from "../../../../hooks/useNavbarStore";
import { Routes } from "../../../../routes/Routes";
import MovieStyle from "../../../../styles/Movies.module.scss";
import { type MovieActorProps } from "../../../../types/movies";

const movieActorNameStyle = MovieStyle[`movie-actor-name`];

const ActorCard = ({ actor }: MovieActorProps) => {
	const navbarStore = useNavbarStore();

	const removeActiveLink = () => {
		navbarStore.setActive("*");
	};

	return (
		<Card className="w-36 select-none rounded-2xl md:w-40 xl:w-44" shadow="md" withBorder>
			<Card.Section>
				<AspectRatio ratio={9 / 12}>
					<Link onClick={removeActiveLink} to={`${Routes.PersonDetails}/${actor.id}`}>
						<Image
							fit="contain"
							placeholder={<Skeleton height="50vh" m={0} width="50vw" />}
							src={actor.imageUrl}
							withPlaceholder
						/>
					</Link>
				</AspectRatio>
			</Card.Section>

			<Text inherit mt="1rem">
				<Text
					className={movieActorNameStyle}
					component={Link}
					onClick={removeActiveLink}
					to={`${Routes.PersonDetails}/${actor.id}`}
					weight={700}
				>
					{actor.name}
				</Text>
			</Text>
			<Text mt="1rem">{actor.character}</Text>
		</Card>
	);
};

export default observer(ActorCard);
