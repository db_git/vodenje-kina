//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { observer } from "mobx-react-lite";

import Heading from "../../../../components/display/Heading";
import HorizontalScroll from "../../../../components/display/HorizontalScroll";
import { type MovieActor } from "../../../../types/entities";
import { type MovieActorsProps } from "../../movieDetails";

import ActorCard from "./ActorCard";

const MovieActors = ({ movie }: MovieActorsProps) => {
	return (
		<div id="actors">
			<Heading anchor="#actors" className="mb-4 mt-8" size={1}>
				Actors
			</Heading>
			<HorizontalScroll slidesToScroll={2} withControls>
				{movie.actors.map((actor: MovieActor) => {
					return (
						<HorizontalScroll.Item key={actor.id}>
							<ActorCard actor={actor} />
						</HorizontalScroll.Item>
					);
				})}
			</HorizontalScroll>
		</div>
	);
};

export default observer(MovieActors);
