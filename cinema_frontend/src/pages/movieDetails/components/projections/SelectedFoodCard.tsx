//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Card, Divider, Group, Image, Text } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback } from "react";

import { useCurrencyFormatter } from "../../../../utils/converters";
import useProjectionOrderStore from "../../contexts/useProjectionOrderStore";
import { type SelectedFoodCardProps } from "../../movieDetails";

import QuantityInput from "./QuantityInput";

const SelectedFoodCard = ({ food }: SelectedFoodCardProps) => {
	const projectionOrder = useProjectionOrderStore();
	const currencyFormatter = useCurrencyFormatter();

	const addedIndex = projectionOrder.selectedFoods
		.map((selectedFood) => {
			return selectedFood.food.id;
		})
		.indexOf(food.id);

	const setValue = useCallback(
		(value: number | undefined) => {
			runInAction(() => {
				const selected = projectionOrder.selectedFoods.at(addedIndex);
				if (value === undefined || selected === undefined) return;

				const newValue = {
					food: selected.food,
					quantity: value
				};

				const foods = JSON.parse(JSON.stringify(projectionOrder.selectedFoods));
				// eslint-disable-next-line security/detect-object-injection
				foods[addedIndex] = newValue;

				projectionOrder.setSelectedFoods(foods);
			});
		},
		[addedIndex, projectionOrder]
	);

	return (
		<Card p={0} radius="md" withBorder>
			<Group noWrap position="apart" pr={10} spacing={0}>
				<Image height={100} src={food.imageUrl} width={100} />

				<div className="px-4">
					<Text className="whitespace-normal" fw={600} mb="md" mt="xs" size="sm">
						{food.name}
					</Text>

					<Text color="dimmed" mr={-8} size="md">
						{currencyFormatter.format(
							food.price * (projectionOrder.selectedFoods.at(addedIndex)?.quantity ?? 1)
						)}
					</Text>
				</div>
			</Group>

			<>
				<Divider />
				<QuantityInput
					max={food.availableQuantity < 5 ? food.availableQuantity : 5}
					min={1}
					setValue={setValue}
					value={addedIndex === -1 ? undefined : projectionOrder.selectedFoods.at(addedIndex)?.quantity}
				/>
			</>
		</Card>
	);
};

export default observer(SelectedFoodCard);
