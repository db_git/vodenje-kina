//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Card, Group, Image, Text } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback } from "react";
import { MdOutlineProductionQuantityLimits } from "react-icons/md";

import { useCurrencyFormatter } from "../../../../utils/converters";
import useProjectionOrderStore from "../../contexts/useProjectionOrderStore";
import { type SouvenirCardProps } from "../../movieDetails";
import { deselectedCardStyle, selectedCardStyle } from "../../styles/selectedCardStyle";

const SouvenirCard = ({ souvenir }: SouvenirCardProps) => {
	const projectionOrder = useProjectionOrderStore();
	const currencyFormatter = useCurrencyFormatter();

	const addedIndex = projectionOrder.selectedSouvenirs
		.map((selectedSouvenir) => {
			return selectedSouvenir.souvenir.id;
		})
		.indexOf(souvenir.id);

	const cardStyle = addedIndex > -1 ? selectedCardStyle : deselectedCardStyle;

	const onClick = useCallback(() => {
		runInAction(() => {
			if (addedIndex === -1) {
				projectionOrder.setSelectedSouvenirs([
					...projectionOrder.selectedSouvenirs,
					{
						souvenir,
						quantity: 1
					}
				]);
			} else {
				projectionOrder.setSelectedSouvenirs(
					projectionOrder.selectedSouvenirs.filter((selectedSouvenir) => {
						return selectedSouvenir.souvenir.id !== souvenir.id;
					})
				);
			}
		});
	}, [souvenir, addedIndex, projectionOrder]);

	return (
		<Card onClick={onClick} p={0} radius="md" sx={cardStyle} withBorder>
			<Group noWrap position="apart" pr={10} spacing={0}>
				<Image height={100} src={souvenir.imageUrl} width={100} />

				<div className="w-full px-4">
					<Group noWrap position="apart" spacing="xs">
						<Text className="whitespace-normal" fw={600} mb="md" mt="xs" size="sm">
							{souvenir.name}
						</Text>
						<Group ml={8} noWrap position="left" spacing="xs">
							<Text color="dimmed" mt={-5.5} size="md" span>
								{souvenir.availableQuantity}
							</Text>
							<Text color="dimmed" size="md">
								<MdOutlineProductionQuantityLimits />
							</Text>
						</Group>
					</Group>

					<Text color="dimmed" mr={-8} size="md">
						{currencyFormatter.format(souvenir.price)}
					</Text>
				</div>
			</Group>
		</Card>
	);
};

export default observer(SouvenirCard);
