//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Card, Group, Image, Text } from "@mantine/core";

import { type OrderedGoodCardProps } from "../../movieDetails";

const OrderedGoodCard = ({ imageUrl, name, quantity }: OrderedGoodCardProps) => {
	return (
		<Card p={0} radius="md" withBorder>
			<Group noWrap position="apart" pr={10} spacing={0}>
				<Image height={100} src={imageUrl} width={100} />

				<div className="px-4">
					<Text className="whitespace-normal" fw={600} mb="md" mt="xs" size="sm">
						{name}
					</Text>
					<Text color="dimmed" mt={5.5}>
						x {quantity}
					</Text>
				</div>
			</Group>
		</Card>
	);
};

export default OrderedGoodCard;
