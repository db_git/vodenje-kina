//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type NumberInputHandlers, createStyles, NumberInput as MantineNumberInput, ActionIcon } from "@mantine/core";
import { useRef } from "react";
import { AiOutlineMinus, AiOutlinePlus } from "react-icons/ai";

import { type QuantityInputProps } from "../../movieDetails";

// https://ui.mantine.dev/category/inputs

const useStyles = createStyles((theme) => {
	return {
		wrapper: {
			display: "flex",
			alignItems: "center",
			justifyContent: "space-between",
			padding: `6px ${theme.spacing.xs}px`,
			border: `1px solid ${theme.colorScheme === "dark" ? "transparent" : theme.colors.gray[3]}`,
			backgroundColor: theme.colorScheme === "dark" ? theme.colors.dark[5] : theme.white,

			"&:focus-within": {
				borderColor: theme.colors[theme.primaryColor][6]
			}
		},

		control: {
			backgroundColor: theme.colorScheme === "dark" ? theme.colors.dark[7] : theme.colors.gray[1],
			border: `1px solid ${theme.colorScheme === "dark" ? "transparent" : theme.colors.gray[3]}`,

			"&:disabled": {
				borderColor: theme.colorScheme === "dark" ? "transparent" : theme.colors.gray[3],
				opacity: 0.8,
				backgroundColor: "transparent"
			}
		},

		input: {
			textAlign: "center",
			paddingRight: `${theme.spacing.sm}px !important`,
			paddingLeft: `${theme.spacing.sm}px !important`,
			height: 28,
			flex: 1
		}
	};
});

const QuantityInput = ({ min = 0, max = 5, value, setValue }: QuantityInputProps) => {
	const { classes } = useStyles();
	const handlers = useRef<NumberInputHandlers>(null);

	return (
		<div className={classes.wrapper}>
			<ActionIcon<"button">
				className={classes.control}
				disabled={value === min}
				onClick={() => {
					return handlers.current?.decrement();
				}}
				onMouseDown={(event) => {
					event.preventDefault();
				}}
				size={28}
				variant="transparent"
			>
				<AiOutlineMinus size={16} />
			</ActionIcon>

			<MantineNumberInput
				classNames={{ input: classes.input }}
				handlersRef={handlers}
				max={max}
				min={min}
				onChange={setValue}
				value={value}
				variant="unstyled"
			/>

			<ActionIcon<"button">
				className={classes.control}
				disabled={value === max}
				onClick={() => {
					return handlers.current?.increment();
				}}
				onMouseDown={(event) => {
					event.preventDefault();
				}}
				size={28}
				variant="transparent"
			>
				<AiOutlinePlus size={16} />
			</ActionIcon>
		</div>
	);
};

export default QuantityInput;
