//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import dayjs from "dayjs";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useEffect, useState } from "react";

import Heading from "../../../../components/display/Heading";
import Loading from "../../../../components/status/Loading";
import NoProjections from "../../../../components/status/NoProjections";
import useApiStore from "../../../../hooks/useApiStore";
import { HttpMethod } from "../../../../types/enums/Http";
import { ProjectionQueryParameterOptions } from "../../../../types/enums/Stores";
import { is2xxStatus, isNotFoundOrNoContent } from "../../../../utils/isStatus";
import MovieProjectionProvider from "../../contexts/ProjectionOrderProvider";
import useProjectionOrderStore from "../../contexts/useProjectionOrderStore";
import { type MovieProjectionsProps } from "../../movieDetails";

import ProjectionCalendar from "./ProjectionCalendar";
import ProjectionOrder from "./ProjectionOrder";
import ProjectionScroll from "./ProjectionScroll";

const DATE_FORMAT = "YYYY-MM-DD";
const today = new Date();
const startDate = dayjs(today);
const endDate = dayjs(today).add(1, "month");

const MovieProjectionsInternal = observer(({ movie }: MovieProjectionsProps) => {
	const { projectionStore } = useApiStore();
	const projectionOrder = useProjectionOrderStore();
	const [projectionDates, setProjectionDates] = useState<string[]>([]);
	const [selectedCalendarDate, setSelectedCalendarDate] = useState<Date | null>(null);

	const loading = projectionStore.loading.getAll;
	const status = projectionStore.status.getAll;

	const projections = projectionStore.responses
		? projectionStore.responses.data
				.map((projection) => {
					return projection;
				})
				.sort((projection1, projection2) => {
					return dayjs(projection1.time).isAfter(projection2.time) ? 1 : 0;
				})
		: undefined;

	useEffect(() => {
		runInAction(() => {
			projectionStore.setProjectionQueryParameters(ProjectionQueryParameterOptions.Movie, movie.id);

			projectionStore.setProjectionQueryParameters(
				ProjectionQueryParameterOptions.StartDate,
				startDate.format(DATE_FORMAT)
			);

			projectionStore.setProjectionQueryParameters(
				ProjectionQueryParameterOptions.EndDate,
				endDate.format(DATE_FORMAT)
			);

			void projectionStore.getAll();
		});
	}, [movie, projectionStore]);

	useEffect(() => {
		runInAction(() => {
			if (!projections || projectionDates.length) return;

			const projectionItemDates: string[] = [];

			for (const projection of projections) {
				projectionItemDates.push(dayjs(projection.time).format(DATE_FORMAT));
			}

			setProjectionDates(projectionItemDates);
		});
	}, [projectionDates, projections]);

	useEffect(() => {
		return () => {
			runInAction(() => {
				projectionStore.setResponses(undefined);
				projectionStore.setStatus(HttpMethod.GetAll, 0);
				projectionStore.setProjectionQueryParameters(ProjectionQueryParameterOptions.Movie, undefined);
				projectionStore.setProjectionQueryParameters(ProjectionQueryParameterOptions.StartDate, undefined);
				projectionStore.setProjectionQueryParameters(ProjectionQueryParameterOptions.EndDate, undefined);

				projectionOrder.setSelectedProjection(undefined);
				projectionOrder.setOrderStep(0);
				projectionOrder.setSelectedSeats([]);
				projectionOrder.setSelectedSouvenirs([]);
			});
		};
	}, [projectionOrder, projectionStore]);

	return (
		<div id="projections">
			<Heading anchor="#projections" className="mb-4 mt-12" size={1}>
				Projections
			</Heading>

			{loading && <Loading noHeight />}

			{!loading && (isNotFoundOrNoContent(status) || !projections || !is2xxStatus(status)) && (
				<NoProjections className="max-w-[16rem] md:max-w-xs xl:max-w-sm" />
			)}

			{!loading && !isNotFoundOrNoContent(status) && projections && (
				<>
					<ProjectionCalendar
						dateFormat={DATE_FORMAT}
						endDate={endDate}
						projectionDates={projectionDates}
						selectedCalendarDate={selectedCalendarDate}
						setSelectedCalendarDate={setSelectedCalendarDate}
						startDate={startDate}
					/>
					<ProjectionScroll
						dateFormat={DATE_FORMAT}
						projections={projections}
						selectedCalendarDate={selectedCalendarDate}
					/>
				</>
			)}

			<ProjectionOrder />
		</div>
	);
});

const MovieProjections = (props: MovieProjectionsProps) => {
	return (
		<MovieProjectionProvider>
			<MovieProjectionsInternal {...props} />
		</MovieProjectionProvider>
	);
};

export default MovieProjections;
