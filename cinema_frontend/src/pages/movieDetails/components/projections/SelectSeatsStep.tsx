//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Center, Container, Stack, Text } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { Fragment, useEffect } from "react";

import Loading from "../../../../components/status/Loading";
import useApiStore from "../../../../hooks/useApiStore";
import useCartStore from "../../../../hooks/useCartStore";
import { type Seat } from "../../../../types/entities";
import { SeatQueryParameterOptions } from "../../../../types/enums/Stores";
import { is2xxStatus } from "../../../../utils/isStatus";
import useProjectionOrderStore from "../../contexts/useProjectionOrderStore";

const SelectSeatsStep = () => {
	const cartStore = useCartStore();
	const projectionOrder = useProjectionOrderStore();
	const { seatStore, projectionStore } = useApiStore();

	const loading = seatStore.loading.getAll || projectionStore.loading.get;

	useEffect(() => {
		runInAction(() => {
			if (!projectionOrder.selectedProjection) return;

			seatStore.setSeatQueryParameters(
				SeatQueryParameterOptions.Hall,
				projectionOrder.selectedProjection.hall.id
			);
			seatStore.setSeatQueryParameters(SeatQueryParameterOptions.Page, 1);
			seatStore.setSeatQueryParameters(SeatQueryParameterOptions.Size, 500);

			void seatStore.getAll();
			void projectionStore.getTakenSeats(projectionOrder.selectedProjection.id);
		});
	}, [projectionOrder.selectedProjection, projectionStore, seatStore]);

	useEffect(() => {
		return () => {
			seatStore.setSeatQueryParameters(SeatQueryParameterOptions.Hall, undefined);
			seatStore.setSeatQueryParameters(SeatQueryParameterOptions.Page, 1);
			seatStore.setSeatQueryParameters(SeatQueryParameterOptions.Size, 10);
		};
	}, [seatStore]);

	const onSeatClick = (seat: Seat): void => {
		const index = projectionOrder.selectedSeats
			.map((selectedSeat) => {
				return selectedSeat.id;
			})
			.indexOf(seat.id);

		if (index === -1) {
			projectionOrder.setSelectedSeats([...projectionOrder.selectedSeats, seat]);
		} else {
			projectionOrder.setSelectedSeats(
				projectionOrder.selectedSeats.filter((selectedSeat) => {
					return selectedSeat.id !== seat.id;
				})
			);
		}
	};

	if (!loading && !is2xxStatus(seatStore.status.getAll)) {
		return (
			<Center>
				<Text>Failed to retrieve seats. Please try again later.</Text>
			</Center>
		);
	}

	return (
		<Container>
			<Stack>
				{loading && <Loading />}
				{!loading && (
					<div className="grid grid-cols-3 gap-y-2 gap-x-3 px-0 xs:grid-cols-5 md:grid-cols-10 md:gap-y-0">
						{seatStore.responses?.data.map((seat, index) => {
							return (
								<Fragment key={seat.id}>
									{index % 10 === 0 && <p className="col-span-full" />}
									<Button
										color="blue"
										disabled={
											cartStore.orders
												.filter((order) => {
													return (
														order.projection.id === projectionOrder.selectedProjection?.id
													);
												})
												.some((order) => {
													return order.seats
														.map((takenSeat) => {
															return takenSeat.id;
														})
														.includes(seat.id);
												}) ||
											projectionStore.takenSeats
												?.map((takenSeat) => {
													return takenSeat.id;
												})
												.includes(seat.id)
										}
										mx={0}
										onClick={() => {
											runInAction(() => {
												onSeatClick(seat);
											});
										}}
										radius="xl"
										variant={
											projectionOrder.selectedSeats
												.map((selectedSeat) => {
													return selectedSeat.id;
												})
												.includes(seat.id)
												? "filled"
												: "light"
										}
									>
										{seat.number}
									</Button>
								</Fragment>
							);
						})}
					</div>
				)}
			</Stack>
		</Container>
	);
};

export default observer(SelectSeatsStep);
