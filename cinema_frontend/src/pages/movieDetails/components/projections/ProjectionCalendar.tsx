//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type DayModifiers, Calendar } from "@mantine/dates";
import { type MantineTheme } from "@mantine/styles";
import dayjs from "dayjs";
import { type CSSProperties, useCallback } from "react";

import useLanguage from "../../../../hooks/useLanguage";
import useProjectionOrderStore from "../../contexts/useProjectionOrderStore";
import { type ProjectionCalendarProps } from "../../movieDetails";

// https://mantine.dev/dates/calendar

const dayStyle = (date: Date, modifiers: DayModifiers): CSSProperties => {
	if (modifiers.disabled)
		return {
			textDecoration: "line-through"
		};

	return {};
};

const calendarStyle = (theme: MantineTheme) => {
	return {
		cell: {
			border: `1px solid ${theme.colorScheme === "dark" ? theme.colors.dark[4] : theme.colors.gray[2]}`
		},
		day: { borderRadius: 0, height: 70, fontSize: theme.fontSizes.lg },
		weekday: { fontSize: theme.fontSizes.lg },
		weekdayCell: {
			fontSize: theme.fontSizes.xl,
			backgroundColor: theme.colorScheme === "dark" ? theme.colors.dark[5] : theme.colors.gray[0],
			border: `1px solid ${theme.colorScheme === "dark" ? theme.colors.dark[4] : theme.colors.gray[2]}`,
			height: 70
		}
	};
};

const ProjectionCalendar = ({
	startDate,
	endDate,
	dateFormat,
	projectionDates,
	selectedCalendarDate,
	setSelectedCalendarDate
}: ProjectionCalendarProps) => {
	const projectionOrder = useProjectionOrderStore();
	const locale = useLanguage();

	const shouldExcludeDate = useCallback(
		(excludeDate: Date): boolean => {
			return !projectionDates.includes(dayjs(excludeDate).format(dateFormat));
		},
		[dateFormat, projectionDates]
	);

	const handleOnChange = useCallback(
		(selectedDate: Date | null): void => {
			setSelectedCalendarDate(selectedDate?.getTime() === selectedCalendarDate?.getTime() ? null : selectedDate);
			projectionOrder.setSelectedProjection(undefined);
		},
		[selectedCalendarDate, projectionOrder, setSelectedCalendarDate]
	);

	return (
		<Calendar
			allowLevelChange={false}
			dayStyle={dayStyle}
			excludeDate={shouldExcludeDate}
			fullWidth
			locale={locale}
			maxDate={endDate.toDate()}
			minDate={startDate.toDate()}
			onChange={handleOnChange}
			size="xl"
			styles={calendarStyle}
			value={selectedCalendarDate}
		/>
	);
};

export default ProjectionCalendar;
