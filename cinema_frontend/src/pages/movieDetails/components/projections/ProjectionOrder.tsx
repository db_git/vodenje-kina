//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Center, Stepper } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback } from "react";
import { MdNavigateNext } from "react-icons/md";
import { Link } from "react-router-dom";

import useApiStore from "../../../../hooks/useApiStore";
import useCartStore from "../../../../hooks/useCartStore";
import { Routes } from "../../../../routes/Routes";
import useProjectionOrderStore from "../../contexts/useProjectionOrderStore";

import SelectGoodiesStep from "./SelectGoodiesStep";
import SelectOrderStep from "./SelectOrderStep";
import SelectSeatsStep from "./SelectSeatsStep";

const ProjectionOrder = () => {
	const projectionOrder = useProjectionOrderStore();
	const cartStore = useCartStore();
	const { seatStore, projectionStore } = useApiStore();

	const loading = seatStore.loading.getAll || projectionStore.loading.get;

	const nextStep = useCallback(() => {
		runInAction(() => {
			projectionOrder.setOrderStep(projectionOrder.orderStep + 1);
		});
	}, [projectionOrder]);

	const handleCancel = useCallback(() => {
		projectionOrder.setOrderStep(0);
		projectionOrder.setSelectedProjection(undefined);
		projectionOrder.setSelectedSeats([]);
		projectionOrder.setSelectedFoods([]);
		projectionOrder.setSelectedSouvenirs([]);
	}, [projectionOrder]);

	const resetStepper = useCallback(() => {
		runInAction(() => {
			projectionOrder.setSelectedProjection(undefined);
			projectionOrder.setOrderStep(0);
		});
	}, [projectionOrder]);

	const addToCart = useCallback(() => {
		runInAction(() => {
			if (projectionOrder.selectedProjection === undefined) return;

			cartStore.addOrder({
				seats: projectionOrder.selectedSeats,
				projection: projectionOrder.selectedProjection,
				foods: projectionOrder.selectedFoods,
				souvenirs: projectionOrder.selectedSouvenirs
			});

			nextStep();
		});
	}, [
		cartStore,
		nextStep,
		projectionOrder.selectedFoods,
		projectionOrder.selectedProjection,
		projectionOrder.selectedSeats,
		projectionOrder.selectedSouvenirs
	]);

	if (projectionOrder.selectedProjection === undefined) return null;

	return (
		<div className="my-8">
			<Stepper active={projectionOrder.orderStep} breakpoint="xs">
				<Stepper.Step label="Select seats">
					<SelectSeatsStep />
				</Stepper.Step>
				<Stepper.Step label="Select goodies">
					<SelectGoodiesStep />
				</Stepper.Step>
				<Stepper.Step label="Add to cart">
					<SelectOrderStep />
				</Stepper.Step>
				<Stepper.Completed>
					<Center>
						<p>Added to cart!</p>
					</Center>
				</Stepper.Completed>
			</Stepper>

			<div className="mt-20 flex flex-col items-center">
				<Button.Group>
					{projectionOrder.orderStep === 0 && (
						<Button
							disabled={loading || !projectionOrder.selectedSeats.length}
							onClick={nextStep}
							rightIcon={<MdNavigateNext />}
						>
							Continue
						</Button>
					)}
					{projectionOrder.orderStep === 1 && (
						<Button disabled={loading} onClick={nextStep} rightIcon={<MdNavigateNext />}>
							Continue to cart
						</Button>
					)}
					{projectionOrder.orderStep === 2 && (
						<Button onClick={addToCart} rightIcon={<MdNavigateNext />}>
							Add to cart
						</Button>
					)}

					{projectionOrder.orderStep !== 3 && (
						<Button color="red" onClick={handleCancel} variant="light">
							Cancel
						</Button>
					)}
					{projectionOrder.orderStep === 3 && (
						<>
							<Button component={Link} onClick={resetStepper} to={Routes.Checkout}>
								Proceed to checkout
							</Button>
							<Button onClick={resetStepper} variant="light">
								Finish
							</Button>
						</>
					)}
				</Button.Group>
			</div>
		</div>
	);
};

export default observer(ProjectionOrder);
