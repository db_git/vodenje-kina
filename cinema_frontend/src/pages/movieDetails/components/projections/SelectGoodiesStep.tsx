//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Center, Container, Space, Stack, Text } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback, useEffect } from "react";

import Heading from "../../../../components/display/Heading";
import HorizontalScroll from "../../../../components/display/HorizontalScroll";
import Pagination from "../../../../components/pagination/Pagination";
import Loading from "../../../../components/status/Loading";
import useApiStore from "../../../../hooks/useApiStore";
import {
	FoodQueryParameterOptions,
	QueryParameterOptions,
	SouvenirQueryParameterOptions
} from "../../../../types/enums/Stores";
import { is2xxStatus, isNotFoundOrNoContent } from "../../../../utils/isStatus";
import Search from "../../../admin/Search";
import useProjectionOrderStore from "../../contexts/useProjectionOrderStore";

import FoodCard from "./FoodCard";
import SelectedFoodCard from "./SelectedFoodCard";
import SelectedSouvenirCard from "./SelectedSouvenirCard";
import SouvenirCard from "./SouvenirCard";

// eslint-disable-next-line complexity
const SelectGoodiesStep = () => {
	const projectionOrder = useProjectionOrderStore();
	const { foodStore, souvenirStore } = useApiStore();

	const loading = foodStore.loading.getAll || souvenirStore.loading.getAll;

	useEffect(() => {
		runInAction(() => {
			if (!projectionOrder.selectedProjection) return;

			foodStore.setQueryParameters(QueryParameterOptions.Page, 1);
			foodStore.setQueryParameters(QueryParameterOptions.Size, 4);
			foodStore.setFoodQueryParameters(FoodQueryParameterOptions.AvailableOnly, 1);
			souvenirStore.setQueryParameters(QueryParameterOptions.Page, 1);
			souvenirStore.setQueryParameters(QueryParameterOptions.Size, 4);
			souvenirStore.setSouvenirQueryParameters(SouvenirQueryParameterOptions.AvailableOnly, 1);

			void foodStore.getAll();
			void souvenirStore.getAll();
		});
	}, [souvenirStore, foodStore, projectionOrder.selectedProjection]);

	useEffect(() => {
		return () => {
			foodStore.setQueryParameters(QueryParameterOptions.Page, 1);
			foodStore.setQueryParameters(QueryParameterOptions.Size, 10);
			foodStore.setFoodQueryParameters(FoodQueryParameterOptions.AvailableOnly, undefined);
			souvenirStore.setQueryParameters(QueryParameterOptions.Page, 1);
			souvenirStore.setQueryParameters(QueryParameterOptions.Size, 10);
			souvenirStore.setSouvenirQueryParameters(SouvenirQueryParameterOptions.AvailableOnly, undefined);
		};
	}, [souvenirStore, foodStore]);

	const changeFoodPage = useCallback(() => {
		void foodStore.getAll();
	}, [foodStore]);

	const changeSouvenirPage = useCallback(() => {
		void souvenirStore.getAll();
	}, [souvenirStore]);

	if (!loading && (!is2xxStatus(foodStore.status.getAll) || !is2xxStatus(souvenirStore.status.getAll))) {
		return (
			<Center>
				<Text>Failed to retrieve foods and souvenirs. Please try again later.</Text>
			</Center>
		);
	}

	return (
		<Container>
			<Stack>
				{loading && <Loading />}

				{!loading && (foodStore.responses || foodStore.queryParameters.search !== undefined) && (
					<>
						<Heading size={3}>Add foods</Heading>
						<Search placeholder="Search foods..." store={foodStore} />
						{foodStore.responses !== undefined && !isNotFoundOrNoContent(foodStore.status.getAll) && (
							<div className="grid w-full grid-cols-1 gap-3 lg:grid-cols-2">
								{foodStore.responses.data.map((food) => {
									return <FoodCard food={food} key={food.id} />;
								})}
							</div>
						)}
						<Pagination modifyUrl={changeFoodPage} store={foodStore} />
					</>
				)}
				{!loading && (souvenirStore.responses || souvenirStore.queryParameters.search !== undefined) && (
					<>
						<Heading size={3}>Add souvenirs</Heading>
						<Search placeholder="Search souvenirs..." store={souvenirStore} />
						{souvenirStore.responses !== undefined &&
							!isNotFoundOrNoContent(souvenirStore.status.getAll) && (
								<div className="grid w-full grid-cols-1 gap-3 lg:grid-cols-2">
									{souvenirStore.responses.data.map((souvenir) => {
										return <SouvenirCard key={souvenir.id} souvenir={souvenir} />;
									})}
								</div>
							)}
						<Pagination modifyUrl={changeSouvenirPage} store={souvenirStore} />
					</>
				)}

				<Space my="xl" />

				{!loading && foodStore.responses && projectionOrder.selectedFoods.length > 0 && (
					<HorizontalScroll slidesToScroll={1} withControls>
						{projectionOrder.selectedFoods.map((selectedFood) => {
							return (
								<HorizontalScroll.Item key={selectedFood.food.id}>
									<SelectedFoodCard food={selectedFood.food} />
								</HorizontalScroll.Item>
							);
						})}
					</HorizontalScroll>
				)}
				{!loading && souvenirStore.responses && projectionOrder.selectedSouvenirs.length > 0 && (
					<HorizontalScroll slidesToScroll={1} withControls>
						{projectionOrder.selectedSouvenirs.map((selectedSouvenir) => {
							return (
								<HorizontalScroll.Item key={selectedSouvenir.souvenir.id}>
									<SelectedSouvenirCard souvenir={selectedSouvenir.souvenir} />
								</HorizontalScroll.Item>
							);
						})}
					</HorizontalScroll>
				)}
			</Stack>
		</Container>
	);
};

export default observer(SelectGoodiesStep);
