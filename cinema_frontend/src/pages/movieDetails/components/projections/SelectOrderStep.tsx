//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Container, Flex, Stack, Text } from "@mantine/core";
import dayjs from "dayjs";
import { observer } from "mobx-react-lite";
import { Fragment } from "react";
import { BiMovie } from "react-icons/all";
import { BsClock } from "react-icons/bs";
import { GiFilmProjector, GiTheater } from "react-icons/gi";
import { MdOutlineLocationOn } from "react-icons/md";

import Heading from "../../../../components/display/Heading";
import HorizontalScroll from "../../../../components/display/HorizontalScroll";
import useApiStore from "../../../../hooks/useApiStore";
import { useCurrencyFormatter } from "../../../../utils/converters";
import useProjectionOrderStore from "../../contexts/useProjectionOrderStore";

import OrderedGoodCard from "./OrderedGoodCard";

const SelectOrderStep = () => {
	const projectionOrder = useProjectionOrderStore();
	const { seatStore } = useApiStore();
	const currencyFormatter = useCurrencyFormatter();

	if (projectionOrder.selectedProjection === undefined) return null;

	const calculatePrice = (): number => {
		if (projectionOrder.selectedProjection === undefined) return 0;

		let price = projectionOrder.selectedProjection.hall.cinema.ticketPrice;

		price *= projectionOrder.selectedSeats.length;

		for (const selectedFood of projectionOrder.selectedFoods) {
			price += selectedFood.food.price * selectedFood.quantity;
		}

		for (const selectedSouvenir of projectionOrder.selectedSouvenirs) {
			price += selectedSouvenir.souvenir.price * selectedSouvenir.quantity;
		}

		return price;
	};

	return (
		<Container size="sm">
			<Heading size={3}>Review your order</Heading>
			<Stack align="center" my="sm">
				<Stack my="sm" spacing={0}>
					<Flex align="center" direction="row" gap="md" justify="start" mb="md" wrap="nowrap">
						<BiMovie size={21} />
						<Text className="whitespace-normal text-xs sm:text-base" fw={500} size="md">
							{projectionOrder.selectedProjection.movie.title}
						</Text>
					</Flex>

					<Flex align="center" direction="row" gap="md" justify="start" mb="md" wrap="nowrap">
						<BsClock size={21} />
						<Text className="text-xs sm:text-base">
							{dayjs(projectionOrder.selectedProjection.time).format("LLL")}
						</Text>
					</Flex>
					<Flex align="center" direction="row" gap="md" justify="start" mb="md" wrap="nowrap">
						<GiTheater size={21} />
						<Text className="text-xs sm:text-base">{projectionOrder.selectedProjection.hall.name}</Text>
					</Flex>
					<Flex align="center" direction="row" gap="md" justify="start" mb="md" wrap="nowrap">
						<GiFilmProjector size={21} />
						<Text className="text-xs sm:text-base">
							{projectionOrder.selectedProjection.hall.cinema.name}
						</Text>
					</Flex>
					<Flex align="center" direction="row" gap="md" justify="start" mb="md" wrap="nowrap">
						<MdOutlineLocationOn size={21} />
						<Text className="text-xs sm:text-base">
							{projectionOrder.selectedProjection.hall.cinema.city},&nbsp;
							{projectionOrder.selectedProjection.hall.cinema.street}
						</Text>
					</Flex>
				</Stack>
			</Stack>

			<Heading size={3}>Seats</Heading>
			<Stack align="center" my="sm">
				<div className="grid grid-cols-3 gap-y-2 gap-x-3 px-0 xs:grid-cols-5 md:grid-cols-10 md:gap-y-0">
					{seatStore.responses?.data.map((seat, index) => {
						return (
							<Fragment key={seat.id}>
								{index % 10 === 0 && <p className="col-span-full" />}
								<Button
									color="blue"
									disabled={
										!projectionOrder.selectedSeats
											?.map((takenSeat) => {
												return takenSeat.id;
											})
											.includes(seat.id)
									}
									mx={0}
									radius="xl"
									variant={
										projectionOrder.selectedSeats
											.map((selectedSeat) => {
												return selectedSeat.id;
											})
											.includes(seat.id)
											? "filled"
											: "light"
									}
								>
									{seat.number}
								</Button>
							</Fragment>
						);
					})}
				</div>
			</Stack>

			{projectionOrder.selectedFoods.length > 0 && (
				<>
					<Heading className="mt-10 mb-6" size={3}>
						Foods
					</Heading>
					<HorizontalScroll>
						{projectionOrder.selectedFoods.map((selectedFood) => {
							return (
								<HorizontalScroll.Item key={selectedFood.food.id}>
									<OrderedGoodCard
										imageUrl={selectedFood.food.imageUrl}
										name={selectedFood.food.name}
										quantity={selectedFood.quantity}
									/>
								</HorizontalScroll.Item>
							);
						})}
					</HorizontalScroll>
				</>
			)}

			{projectionOrder.selectedSouvenirs.length > 0 && (
				<>
					<Heading className="mt-10 mb-6" size={3}>
						Souvenirs
					</Heading>
					<HorizontalScroll>
						{projectionOrder.selectedSouvenirs.map((selectedSouvenir) => {
							return (
								<HorizontalScroll.Item key={selectedSouvenir.souvenir.id}>
									<OrderedGoodCard
										imageUrl={selectedSouvenir.souvenir.imageUrl}
										name={selectedSouvenir.souvenir.name}
										quantity={selectedSouvenir.quantity}
									/>
								</HorizontalScroll.Item>
							);
						})}
					</HorizontalScroll>
				</>
			)}

			<Heading className="mt-5" size={3}>
				Total
			</Heading>
			<Text fw={600} inline mt="lg" size="xl">
				{currencyFormatter.format(calculatePrice())}
			</Text>
		</Container>
	);
};

export default observer(SelectOrderStep);
