//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import dayjs from "dayjs";
import { runInAction } from "mobx";
import { memo } from "react";

import HorizontalScroll from "../../../../components/display/HorizontalScroll";
import { type ProjectionScrollProps } from "../../movieDetails";

import ProjectionCard from "./ProjectionCard";

const ProjectionScroll = ({ selectedCalendarDate, dateFormat, projections }: ProjectionScrollProps) => {
	if (!selectedCalendarDate) return null;

	return (
		<HorizontalScroll marginTop="2rem" slideSize="25%" slidesToScroll={1} withControls>
			{runInAction(() => {
				return projections
					.filter((projection) => {
						return (
							dayjs(projection.time).format(dateFormat) === dayjs(selectedCalendarDate).format(dateFormat)
						);
					})
					.map((projection) => {
						return (
							<HorizontalScroll.Slide key={projection.id}>
								<ProjectionCard projection={projection} />
							</HorizontalScroll.Slide>
						);
					});
			})}
		</HorizontalScroll>
	);
};

export default memo(ProjectionScroll);
