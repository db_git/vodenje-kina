//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Card, Center, Flex, Space, Text, Title } from "@mantine/core";
import dayjs from "dayjs";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback } from "react";
import { BsClock } from "react-icons/bs";
import { GiFilmProjector, GiTheater } from "react-icons/gi";
import { MdOutlineLocationOn } from "react-icons/md";

import useApiStore from "../../../../hooks/useApiStore";
import useProjectionOrderStore from "../../contexts/useProjectionOrderStore";
import { type ProjectionCardProps } from "../../movieDetails";
import { deselectedCardStyle, selectedCardStyle } from "../../styles/selectedCardStyle";

const timeNow = new Date();

const ProjectionCard = ({ projection }: ProjectionCardProps) => {
	const { authStore } = useApiStore();
	const projectionOrder = useProjectionOrderStore();

	const isSelected = projectionOrder.selectedProjection?.id === projection.id;

	const cardStyle = isSelected ? selectedCardStyle : deselectedCardStyle;

	const handleOnClick = useCallback(() => {
		runInAction(() => {
			if (isSelected) {
				projectionOrder.setSelectedProjection(undefined);
			} else {
				projectionOrder.setSelectedProjection(projection);
			}

			projectionOrder.setOrderStep(0);
			projectionOrder.setSelectedSeats([]);
			projectionOrder.setSelectedFoods([]);
			projectionOrder.setSelectedSouvenirs([]);
		});
	}, [isSelected, projection, projectionOrder]);

	return (
		<Card my="3rem" px="2rem" radius="lg" shadow="xl" sx={cardStyle}>
			<Flex align="center" direction="column" gap="md" justify="center" wrap="nowrap">
				<Flex align="center" direction="row" gap="md" justify="center" wrap="nowrap">
					<BsClock size={21} />
					<Title order={3}>{dayjs(projection.time).format("LT")}</Title>
				</Flex>
				<Flex align="center" direction="row" gap="md" justify="center" wrap="nowrap">
					<GiTheater size={21} />
					<Title order={4}>{projection.hall.name}</Title>
				</Flex>
			</Flex>

			<Space py={20} />

			<Flex align="center" direction="column" gap="xs" justify="center" wrap="nowrap">
				<Flex align="center" direction="row" gap="md" justify="center" wrap="nowrap">
					<GiFilmProjector size={21} />
					<Text size="md">{projection.hall.cinema.name}</Text>
				</Flex>
				<Flex align="center" direction="row" gap="md" justify="center" wrap="nowrap">
					<MdOutlineLocationOn size={21} />
					<Text size="md">{projection.hall.cinema.street}</Text>
				</Flex>
			</Flex>

			{authStore.isLoggedIn && (
				<Center mt={20}>
					<Button
						disabled={dayjs(timeNow).isAfter(dayjs(projection.time).add(30, "minutes"))}
						onClick={handleOnClick}
						radius="xl"
						variant={isSelected ? "filled" : "light"}
					>
						{isSelected ? "Deselect" : "Select"}
					</Button>
				</Center>
			)}
		</Card>
	);
};

export default observer(ProjectionCard);
