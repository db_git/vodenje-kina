//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Group, Text } from "@mantine/core";
import { observer } from "mobx-react-lite";

import { type MovieDirectorsProps } from "../../movieDetails";

import DirectorBadge from "./DirectorBadge";

const MovieDirectors = ({ movie, fontWeight }: MovieDirectorsProps) => {
	return movie.directors.length > 1 ? (
		<>
			<Text fw={fontWeight} size="lg">
				Directors:{" "}
			</Text>
			<Group mt="1rem" position="left">
				{movie.directors.map((director) => {
					return <DirectorBadge director={director} key={director.id} />;
				})}
			</Group>
		</>
	) : (
		<Group>
			<Text fw={fontWeight} size="lg">
				Director:
			</Text>
			<DirectorBadge director={movie.directors[0]} />
		</Group>
	);
};

export default observer(MovieDirectors);
