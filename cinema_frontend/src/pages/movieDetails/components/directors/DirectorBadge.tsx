//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Avatar, Badge, useMantineColorScheme } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { Link } from "react-router-dom";

import useNavbarStore from "../../../../hooks/useNavbarStore";
import { Routes } from "../../../../routes/Routes";
import { type MovieDirectorProps } from "../../../../types/movies";

const mantineBadgeStyle = { root: "cursor-pointer pl-0 select-none" };

const DirectorBadge = ({ director }: MovieDirectorProps) => {
	const { colorScheme } = useMantineColorScheme();
	const navbarStore = useNavbarStore();
	const isDark = colorScheme === "dark";

	const removeActiveLink = () => {
		navbarStore.setActive("*");
	};

	return (
		<Badge
			classNames={mantineBadgeStyle}
			color={isDark ? "orange" : "dark"}
			component={Link}
			leftSection={<Avatar mr={10} src={director.imageUrl} />}
			onClick={removeActiveLink}
			radius="xl"
			size="lg"
			to={`${Routes.PersonDetails}/${director.id}`}
			variant={isDark ? "light" : "filled"}
		>
			{director.name}
		</Badge>
	);
};

export default observer(DirectorBadge);
