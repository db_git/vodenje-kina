//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Container, Group, Stack, Textarea } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { useCallback, useState } from "react";

import { ObserverForm } from "../../../../components/forms/Form";
import useApiStore from "../../../../hooks/useApiStore";
import useGetStore from "../../../../hooks/useGetStore";
import { reviewSchema } from "../../../../schemas/reviewSchema";
import { type UserComment } from "../../../../types/entities";
import { JsonPatchOperation } from "../../../../types/enums/Http";
import { QueryParameterOptions } from "../../../../types/enums/Stores";
import { type JsonPatch } from "../../../../types/shared";
import { type UserCommentFormSubmitValues, type WriteReviewProps } from "../../movieDetails";

import ReviewPaper from "./ReviewPaper";

const errorProps = { className: "hidden" };
const textareaStyle = {
	input: "rounded-t-xl"
};

const WriteReview = ({ movieId, comment, edit }: WriteReviewProps) => {
	const { authStore, userCommentStore } = useApiStore();
	const { userCommentGetStore } = useGetStore();
	const [initialComment, setInitialComment] = useState<string>(comment ?? "");

	const onCancel = useCallback(() => {
		userCommentStore.setEdit(undefined);
	}, [userCommentStore]);

	const onSubmit = useCallback(
		async (values: UserCommentFormSubmitValues) => {
			setInitialComment(values.comment);

			if (edit) {
				const patch: Array<JsonPatch<UserComment>> = [
					{
						op: JsonPatchOperation.Replace,
						path: "/comment",
						value: values.comment
					}
				];

				await userCommentStore.patch(movieId, patch);
				userCommentStore.setEdit(undefined);
			} else {
				await userCommentStore.post({
					movie: movieId,
					comment: values.comment
				});
			}

			userCommentGetStore.setQueryParameters(QueryParameterOptions.Page, 1);
			await userCommentGetStore.getAll(
				new URLSearchParams({
					movie: movieId,
					page: "1",
					size: "2"
				})
			);

			await userCommentStore.getCanComment(movieId);
		},
		[edit, movieId, userCommentGetStore, userCommentStore]
	);

	if (!authStore.isLoggedIn) return null;

	return (
		<Container mb="2rem" size="sm">
			<Stack spacing={0}>
				<ObserverForm
					initialValues={{ movie: movieId, comment: initialComment }}
					onSubmit={onSubmit}
					schema={reviewSchema}
				>
					{(form) => {
						return (
							<>
								<Textarea
									autosize
									classNames={textareaStyle}
									maxRows={15}
									minRows={7}
									placeholder="Write a comment..."
									radius={0}
									required
									{...form.getInputProps("comment")}
									errorProps={errorProps}
								/>

								<ReviewPaper className="rounded-b-xl" radius={0} withBorder>
									<Group>
										<Button
											disabled={userCommentStore.loading.post || userCommentStore.loading.patch}
											loading={userCommentStore.loading.post || userCommentStore.loading.patch}
											radius="lg"
											type="submit"
										>
											{edit ? "Edit comment" : "Post comment"}
										</Button>
										{edit && (
											<Button
												color="red"
												disabled={userCommentStore.loading.patch}
												onClick={onCancel}
												radius="lg"
												type="button"
											>
												Cancel
											</Button>
										)}
									</Group>
								</ReviewPaper>
							</>
						);
					}}
				</ObserverForm>
			</Stack>
		</Container>
	);
};

export default observer(WriteReview);
