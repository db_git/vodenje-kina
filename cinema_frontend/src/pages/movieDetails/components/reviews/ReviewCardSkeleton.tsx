//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Divider, Group, Skeleton } from "@mantine/core";

import ReviewPaper from "./ReviewPaper";

const ReviewCardSkeleton = () => {
	return (
		<ReviewPaper marginBottom="1.25rem" marginTop="0.75rem">
			<Group>
				<Skeleton animate className="h-9 w-9 rounded-full md:h-14 md:w-14" radius="xl" />
				<Skeleton animate h="0.6rem" w="35%" />
			</Group>

			<Divider my="1.25rem" size="xs" />

			<Skeleton animate h="1rem" my="0.25rem" w="100%" />
			<Skeleton animate h="1rem" my="0.25rem" w="100%" />
			<Skeleton animate h="1rem" my="0.25rem" w="100%" />
		</ReviewPaper>
	);
};

export default ReviewCardSkeleton;
