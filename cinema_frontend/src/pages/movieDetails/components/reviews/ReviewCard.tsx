//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { observer } from "mobx-react-lite";

import useApiStore from "../../../../hooks/useApiStore";
import { type ReviewCardProps } from "../../movieDetails";

import ReviewAuthor from "./ReviewAuthor";
import ReviewPaper from "./ReviewPaper";
import ReviewSpoiler from "./ReviewSpoiler";
import WriteReview from "./WriteReview";

const ReviewCard = ({ review, withMenu }: ReviewCardProps) => {
	const { authStore, userCommentStore } = useApiStore();

	if (
		userCommentStore.edit &&
		userCommentStore.edit.movie.id === review.movie.id &&
		userCommentStore.edit.user.id === review.user.id
	) {
		return <WriteReview comment={review.comment} edit movieId={review.movie.id} />;
	}

	return (
		<ReviewPaper marginBottom="1.25rem" marginTop="0.75rem">
			<ReviewAuthor review={review} showAvatar withMenu={withMenu && authStore.isLoggedIn} />
			<ReviewSpoiler maxHeight={195} review={review} />
		</ReviewPaper>
	);
};

ReviewCard.defaultProps = {
	withMenu: true
};

export default observer(ReviewCard);
