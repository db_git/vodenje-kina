//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { ActionIcon, Avatar, Divider, Grid, Group, Menu, Text } from "@mantine/core";
import dayjs from "dayjs";
import { observer } from "mobx-react-lite";
import { useCallback } from "react";
import { BiDotsHorizontalRounded } from "react-icons/bi";
import { IoMdTrash } from "react-icons/io";
import { VscEdit } from "react-icons/vsc";

import useApiStore from "../../../../hooks/useApiStore";
import useGetStore from "../../../../hooks/useGetStore";
import { QueryParameterOptions } from "../../../../types/enums/Stores";
import { type ReviewAuthorProps } from "../../movieDetails";

const ReviewAuthor = ({ review, showAvatar, withMenu }: ReviewAuthorProps) => {
	const { authStore, userCommentStore } = useApiStore();
	const { userCommentGetStore } = useGetStore();

	const isAdmin = authStore.isAdmin;
	const isAuthor = authStore.getUserId === review.user.id;
	const showMenu = withMenu && (isAdmin || isAuthor);

	const movieId = review.movie.id;
	const userId = review.user.id;

	const onEdit = useCallback(() => {
		userCommentStore.setEdit(review);
	}, [review, userCommentStore]);

	const onDelete = useCallback(async () => {
		await userCommentStore.deleteComment(movieId, userId);

		userCommentGetStore.setQueryParameters(QueryParameterOptions.Page, 1);
		await userCommentGetStore.getAll(
			new URLSearchParams({
				movie: movieId,
				page: "1",
				size: "2"
			})
		);

		await userCommentStore.getCanComment(movieId);
	}, [movieId, userId, userCommentGetStore, userCommentStore]);

	return (
		<Grid>
			<Grid.Col span={10}>
				<Group>
					{showAvatar && (
						<Avatar
							className="h-9 w-9 rounded-full md:h-14 md:w-14"
							radius="xl"
							src={review.user.imageUrl}
						/>
					)}

					<Text className="text-sm md:text-base" fw={500}>
						{review.user.name}
					</Text>

					<Text c="dimmed" className="text-sm md:text-base">
						{dayjs(review.date).format("ll")}
					</Text>
				</Group>
			</Grid.Col>

			{showMenu && (
				<Grid.Col span={2}>
					<Group position="right" spacing="xs">
						<Menu>
							<Menu.Target>
								<ActionIcon>
									<BiDotsHorizontalRounded size={28} />
								</ActionIcon>
							</Menu.Target>

							<Menu.Dropdown>
								{isAuthor && (
									<Menu.Item icon={<VscEdit color="orange" size={18} />} onClick={onEdit}>
										<Text>Edit</Text>
									</Menu.Item>
								)}
								{(isAuthor || isAdmin) && (
									<Menu.Item icon={<IoMdTrash color="#e03131" size={18} />} onClick={onDelete}>
										<Text>Delete</Text>
									</Menu.Item>
								)}
							</Menu.Dropdown>
						</Menu>
					</Group>
				</Grid.Col>
			)}

			<Grid.Col span={12}>
				<Divider size="xs" />
			</Grid.Col>
		</Grid>
	);
};

export default observer(ReviewAuthor);
