//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback, useEffect } from "react";

import Pagination from "../../../../components/pagination/Pagination";
import NoContent from "../../../../components/status/NoContent";
import useApiStore from "../../../../hooks/useApiStore";
import useGetStore from "../../../../hooks/useGetStore";
import { HttpMethod } from "../../../../types/enums/Http";
import { type MovieReviewsProps } from "../../movieDetails";

import ReviewCard from "./ReviewCard";
import ReviewCardSkeleton from "./ReviewCardSkeleton";
import WriteReview from "./WriteReview";

const MovieReviews = ({ movie }: MovieReviewsProps) => {
	const { authStore, userCommentStore } = useApiStore();
	const { userCommentGetStore } = useGetStore();

	const loading =
		userCommentGetStore.loading.getAll || userCommentStore.loading.get || userCommentStore.loading.delete;

	const modifyUrl = useCallback(() => {
		void userCommentGetStore.getAll(
			new URLSearchParams({
				movie: movie.id,
				page: userCommentGetStore.queryParameters.page.toString(10),
				size: "2"
			})
		);
	}, [movie.id, userCommentGetStore]);

	useEffect(() => {
		return () => {
			userCommentGetStore.setResponses(undefined);
			userCommentGetStore.setResponse(undefined);
			userCommentGetStore.setStatus(HttpMethod.Get, 0);
		};
	}, [userCommentGetStore]);

	useEffect(() => {
		runInAction(() => {
			if (authStore.isLoggedIn) void userCommentStore.getCanComment(movie.id);
		});
	}, [authStore.isLoggedIn, movie.id, userCommentStore]);

	useEffect(() => {
		runInAction(() => {
			modifyUrl();
		});
	}, [modifyUrl]);

	return (
		<>
			{loading && <ReviewCardSkeleton />}

			{!loading && authStore.isLoggedIn && userCommentStore.canComment && <WriteReview movieId={movie.id} />}

			{!loading && (!userCommentGetStore.responses || !userCommentGetStore.responses?.data.length) && (
				<NoContent className="max-w-[16rem] md:max-w-xs" message="No reviews found." />
			)}

			{!loading && userCommentGetStore.responses?.data && Boolean(userCommentGetStore.responses?.data.length) && (
				<>
					{userCommentGetStore.responses.data.map((review) => {
						return <ReviewCard key={`${review.user.id}${review.user.name}`} review={review} />;
					})}
					<Pagination modifyUrl={modifyUrl} store={userCommentGetStore} />
				</>
			)}
		</>
	);
};

export default observer(MovieReviews);
