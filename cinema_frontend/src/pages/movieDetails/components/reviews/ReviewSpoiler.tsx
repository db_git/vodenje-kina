//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Anchor, Box, Modal, Text } from "@mantine/core";
import { useElementSize } from "@mantine/hooks";
import { observer } from "mobx-react-lite";
import { useCallback, useEffect, useState } from "react";

import { type ReviewSpoilerProps } from "../../movieDetails";

import ReviewAuthor from "./ReviewAuthor";

const modalStyle = {
	title: "w-full"
};

// https://github.com/mantinedev/mantine/blob/master/src/mantine-core/src/Spoiler/Spoiler.tsx
const ReviewSpoiler = ({ maxHeight, review }: ReviewSpoilerProps) => {
	const [spoiler, setSpoiler] = useState(false);
	const [opened, setOpened] = useState(false);
	const { ref: contentRef, height } = useElementSize();

	useEffect(() => {
		setSpoiler(height > maxHeight);
	}, [height, maxHeight, review.comment]);

	const toggleModal = useCallback(() => {
		setOpened(!opened);
	}, [opened]);

	return (
		<>
			<Modal
				centered
				classNames={modalStyle}
				onClose={toggleModal}
				opened={opened}
				size="xl"
				title={<ReviewAuthor review={review} showAvatar={false} withMenu={false} />}
			>
				{review.comment}
			</Modal>
			<Box className="relative">
				<div className="flex flex-col overflow-hidden" style={{ maxHeight }}>
					<div ref={contentRef}>
						<Text mt="1rem">{review.comment}</Text>
					</div>
				</div>

				{spoiler && (
					<Anchor component="button" mb={0} onClick={toggleModal}>
						Read the rest
					</Anchor>
				)}
			</Box>
		</>
	);
};

export default observer(ReviewSpoiler);
