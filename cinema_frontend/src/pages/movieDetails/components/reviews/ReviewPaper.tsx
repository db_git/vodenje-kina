//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type Sx, Paper } from "@mantine/core";
import { memo } from "react";

import { type ReviewPaperProps } from "../../movieDetails";

const paperStyle: Array<Sx | undefined> | Sx | undefined = (theme) => {
	return {
		backgroundColor: theme.colorScheme === "dark" ? theme.colors.dark[8] : theme.colors.gray[1]
	};
};

const ReviewPaper = ({
	children,
	className,
	radius,
	withBorder,
	margin,
	marginTop,
	marginRight,
	marginBottom,
	marginLeft
}: ReviewPaperProps) => {
	return (
		<Paper
			className={className}
			m={margin}
			mb={marginBottom}
			ml={marginLeft}
			mr={marginRight}
			mt={marginTop}
			p="xl"
			radius={radius ?? "lg"}
			sx={paperStyle}
			withBorder={withBorder}
		>
			{children}
		</Paper>
	);
};

export default memo(ReviewPaper);
