//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Container, Grid } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { useParams } from "react-router-dom";

import Heading from "../../components/display/Heading";
import Loading from "../../components/status/Loading";
import NoContent from "../../components/status/NoContent";
import ServerError from "../../components/status/ServerError";
import useApiStore from "../../hooks/useApiStore";
import useGetStore from "../../hooks/useGetStore";
import useNavbarStore from "../../hooks/useNavbarStore";
import { Routes } from "../../routes/Routes";
import { HttpMethod } from "../../types/enums/Http";
import { is5xxStatus, isNotFoundOrNoContent } from "../../utils/isStatus";

import MovieInformation from "./components/MovieInformation";
import MovieRecommendations from "./components/MovieRecommendations";
import MovieActors from "./components/actors/MovieActors";
import MovieProjections from "./components/projections/MovieProjections";
import MovieRatings from "./components/ratings/MovieRatings";
import MovieReviews from "./components/reviews/MovieReviews";

const MovieDetails = () => {
	const { id } = useParams();
	const { movieStore, userCommentStore, userRatingStore } = useApiStore();
	const { userRatingGetStore, userCommentGetStore } = useGetStore();
	const navbarStore = useNavbarStore();

	const movie = movieStore.response;

	useEffect(() => {
		navbarStore.setActive(Routes.Movies);
	}, [navbarStore]);

	useEffect(() => {
		runInAction(() => {
			document.title = movie ? movie.title : "Loading...";
		});
	}, [movie]);

	useEffect(() => {
		runInAction(() => {
			if ((id && movie === undefined) || (movie?.id !== id && id)) void movieStore.get(id);
		});
	}, [id, movie, movieStore]);

	useEffect(() => {
		return () => {
			runInAction(() => {
				movieStore.setResponse(undefined);
				movieStore.setStatus(HttpMethod.Get, 0);

				userCommentStore.setResponse(undefined);
				userCommentStore.setStatus(HttpMethod.Get, 0);
				userCommentStore.setResponses(undefined);
				userCommentStore.setStatus(HttpMethod.GetAll, 0);
				userCommentGetStore.setResponse(undefined);
				userCommentGetStore.setStatus(HttpMethod.Get, 0);
				userCommentGetStore.setResponses(undefined);
				userCommentGetStore.setStatus(HttpMethod.GetAll, 0);

				userRatingStore.setResponse(undefined);
				userRatingStore.setStatus(HttpMethod.Get, 0);
				userRatingStore.setResponses(undefined);
				userRatingStore.setStatus(HttpMethod.GetAll, 0);
				userRatingGetStore.setResponse(undefined);
				userRatingGetStore.setStatus(HttpMethod.Get, 0);
				userRatingGetStore.setResponses(undefined);
				userRatingGetStore.setStatus(HttpMethod.GetAll, 0);
			});
		};
	}, [movieStore, userCommentGetStore, userCommentStore, userRatingGetStore, userRatingStore]);

	if (isNotFoundOrNoContent(movieStore.status.get)) return <NoContent message="Movie not found." />;
	if (is5xxStatus(movieStore.status.get)) return <ServerError />;
	if (movieStore.loading.get) return <Loading />;
	if (!movie) return null;

	return (
		<Container fluid mb="3.5rem" mx={0} px={0}>
			<MovieInformation movie={movie} />

			<Container size="xl">
				<MovieActors movie={movie} />
				{movie.inCinemas && <MovieProjections movie={movie} />}
				<MovieRecommendations movie={movie} />

				<div id="reviews">
					<Grid>
						<Grid.Col span={12}>
							<Heading anchor="#reviews" className="mb-4 mt-8" size={1}>
								Reviews
							</Heading>
						</Grid.Col>

						<Grid.Col md={6} my="0.25rem" order={2} orderMd={1} xs={12}>
							<MovieReviews movie={movie} />
						</Grid.Col>

						<Grid.Col md={6} my="0.25rem" order={1} orderMd={2} xs={12}>
							<MovieRatings movie={movie} />
						</Grid.Col>
					</Grid>
				</div>
			</Container>
		</Container>
	);
};

export default observer(MovieDetails);
