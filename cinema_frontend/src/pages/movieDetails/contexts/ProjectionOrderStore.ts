//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { action, computed, makeObservable, observable } from "mobx";

import { type Seat, type Projection } from "../../../types/entities";
import { type SelectedSouvenir, type SelectedFood, type IProjectionOrderStore } from "../movieDetails";

class ProjectionOrderStore implements IProjectionOrderStore {
	private static _instance: IProjectionOrderStore | undefined;

	private _orderStep = 0;
	private _selectedSeats: Seat[] = [];
	private _selectedProjection: Projection | undefined;
	private _selectedFoods: SelectedFood[] = [];
	private _selectedSouvenirs: SelectedSouvenir[] = [];

	private constructor() {
		this._selectedProjection = undefined;

		makeObservable<
			IProjectionOrderStore,
			| "_instance"
			| "_orderStep"
			| "_selectedFoods"
			| "_selectedProjection"
			| "_selectedSeats"
			| "_selectedSouvenirs"
		>(this, {
			_instance: false,

			_selectedSouvenirs: observable,
			selectedSouvenirs: computed,
			setSelectedSouvenirs: action,

			_selectedFoods: observable,
			selectedFoods: computed,
			setSelectedFoods: action,

			_orderStep: observable,
			orderStep: computed,
			setOrderStep: action,

			_selectedSeats: observable,
			selectedSeats: computed,
			setSelectedSeats: action,

			_selectedProjection: observable,
			selectedProjection: computed,
			setSelectedProjection: action
		});
	}

	public get selectedSouvenirs(): SelectedSouvenir[] {
		return this._selectedSouvenirs;
	}

	public readonly setSelectedSouvenirs = (selectedSouvenirs: SelectedSouvenir[]) => {
		this._selectedSouvenirs = selectedSouvenirs;
	};

	public get selectedFoods(): SelectedFood[] {
		return this._selectedFoods;
	}

	public readonly setSelectedFoods = (selectedFoods: SelectedFood[]) => {
		this._selectedFoods = selectedFoods;
	};

	public get orderStep(): number {
		return this._orderStep;
	}

	public readonly setOrderStep = (step: number): void => {
		this._orderStep = step;
	};

	public get selectedSeats(): Seat[] {
		return this._selectedSeats;
	}

	public readonly setSelectedSeats = (seats: Seat[]) => {
		this._selectedSeats = seats;
	};

	public get selectedProjection(): Projection | undefined {
		return this._selectedProjection;
	}

	public readonly setSelectedProjection = (selectedProjection: Projection | undefined): void => {
		this._selectedProjection = selectedProjection;
	};

	public static readonly GetInstance = (): IProjectionOrderStore => {
		if (this._instance === undefined) this._instance = new ProjectionOrderStore();
		return this._instance;
	};
}

export default ProjectionOrderStore;
