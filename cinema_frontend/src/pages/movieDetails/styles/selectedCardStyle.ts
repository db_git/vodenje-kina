//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type Sx } from "@mantine/core";

const selectedCardStyle: Array<Sx | undefined> | Sx | undefined = (theme) => {
	const selectedBorder = `3px solid ${theme.colors.blue[5]}`;

	return {
		paddingTop: "1rem",
		paddingBottom: "1rem",
		minWidth: "15rem",
		border: selectedBorder,
		userSelect: "none"
	};
};

const deselectedCardStyle: Array<Sx | undefined> | Sx | undefined = (theme) => {
	const isDark = theme.colorScheme === "dark";
	const themeBorder = isDark ? `1px solid ${theme.colors.dark[3]}` : `1px solid ${theme.colors.gray[4]}`;

	return {
		paddingTop: "1rem",
		paddingBottom: "1rem",
		minWidth: "15rem",
		border: themeBorder,
		userSelect: "none"
	};
};

export { selectedCardStyle, deselectedCardStyle };
