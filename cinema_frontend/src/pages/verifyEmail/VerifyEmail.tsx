//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { Navigate, useLocation } from "react-router-dom";

import Loading from "../../components/status/Loading";
import NotFound from "../../components/status/NotFound";
import ServerError from "../../components/status/ServerError";
import useApiStore from "../../hooks/useApiStore";
import useNavbarStore from "../../hooks/useNavbarStore";
import { Routes } from "../../routes/Routes";
import { is2xxStatus, is4xxStatus, is5xxStatus } from "../../utils/isStatus";

const VerifyEmail = () => {
	const { authStore } = useApiStore();
	const navbarStore = useNavbarStore();

	const query = useLocation().search;
	const url = new URLSearchParams(query);

	const userId = url.get("userId");
	const verificationCode = url.get("verificationCode");
	const showNotFound = authStore.isLoggedIn || !userId || !verificationCode;

	useEffect(() => {
		if (!showNotFound) document.title = "Verify email";
		runInAction(() => {
			navbarStore.setActive("*");
		});
	}, [navbarStore, showNotFound]);

	useEffect(() => {
		runInAction(() => {
			if (userId && verificationCode) void authStore.verifyEmail(userId, verificationCode);
		});
	}, [authStore, userId, verificationCode]);

	if (showNotFound) return <NotFound />;
	if (is5xxStatus(authStore.status)) return <ServerError />;
	if (authStore.emailVerified || is2xxStatus(authStore.status) || is4xxStatus(authStore.status)) {
		return <Navigate to={Routes.Registration} />;
	}

	return <Loading />;
};

export default observer(VerifyEmail);
