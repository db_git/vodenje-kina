//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Center, Container, Stack } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";

import Heading from "../../components/display/Heading";
import Loading from "../../components/status/Loading";
import NoContent from "../../components/status/NoContent";
import NotFound from "../../components/status/NotFound";
import ServerError from "../../components/status/ServerError";
import useApiStore from "../../hooks/useApiStore";
import useNavbarStore from "../../hooks/useNavbarStore";
import { Routes } from "../../routes/Routes";
import { ReservationOrderBy } from "../../types/enums/OrderBy";
import { QueryParameterOptions } from "../../types/enums/Stores";
import { is5xxStatus, isNotFoundOrNoContent } from "../../utils/isStatus";

import AccountAvatar from "./components/AccountAvatar";
import AccountEmail from "./components/AccountEmail";
import AccountName from "./components/AccountName";
import AccountPassword from "./components/AccountPassword";
import Reservations from "./components/Reservations";

const Account = () => {
	const { authStore, reservationStore } = useApiStore();
	const navbarStore = useNavbarStore();

	useEffect(() => {
		navbarStore.setActive(Routes.Account);
		document.title = "Account";
	});

	useEffect(() => {
		void authStore.accountInformation();
	}, [authStore]);

	useEffect(() => {
		runInAction(() => {
			if (authStore.account !== undefined && reservationStore.responses === undefined) {
				reservationStore.setQueryParameters(QueryParameterOptions.Page, 1);
				reservationStore.setQueryParameters(QueryParameterOptions.Size, 4);
				reservationStore.setQueryParameters(QueryParameterOptions.OrderBy, ReservationOrderBy.DateAscending);

				void reservationStore.getAllAccount();
			}
		});
	}, [authStore.account, reservationStore]);

	useEffect(() => {
		runInAction(() => {
			reservationStore.setQueryParameters(QueryParameterOptions.Page, 1);
			reservationStore.setQueryParameters(QueryParameterOptions.Size, 10);
			reservationStore.setQueryParameters(QueryParameterOptions.OrderBy, ReservationOrderBy.DateAscending);
			reservationStore.setResponses(undefined);
		});
	}, [reservationStore]);

	if (is5xxStatus(authStore.status) || is5xxStatus(reservationStore.status.getAll)) return <ServerError />;
	if (!authStore.isLoggedIn) return <NotFound />;
	if (authStore.loading) return <Loading />;
	if (!authStore.account) return null;

	return (
		<>
			<Container size="sm">
				<Stack align="stretch" justify="space-between" spacing="xl">
					<AccountAvatar />
					<AccountName />
					<AccountEmail />
					<AccountPassword />

					<div id="reservations">
						<Heading anchor="#reservations" className="mt-8" size={2}>
							Reservations
						</Heading>
					</div>
				</Stack>
			</Container>

			<Container mb="5rem" mt="2rem" size="xl">
				{reservationStore.loading.getAll && <Loading noHeight />}

				{!reservationStore.loading.getAll && isNotFoundOrNoContent(reservationStore.status.getAll) && (
					<Center>
						<NoContent message="You haven't made any reservations." />
					</Center>
				)}
				{!reservationStore.loading.getAll &&
					!isNotFoundOrNoContent(reservationStore.status.getAll) &&
					Boolean(reservationStore.responses?.data.length) &&
					reservationStore.responses && <Reservations reservations={reservationStore.responses.data} />}
			</Container>
		</>
	);
};

export default observer(Account);
