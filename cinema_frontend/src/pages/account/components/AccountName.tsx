//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Grid, TextInput } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { type ChangeEvent, useCallback, useState } from "react";
import { MdTitle } from "react-icons/md";

import Heading from "../../../components/display/Heading";
import useApiStore from "../../../hooks/useApiStore";
import { nameSchema } from "../../../schemas/accountSchemas";

import CancelIcon from "./CancelIcon";
import ConfirmIcon from "./ConfirmIcon";
import EditIcon from "./EditIcon";

const AccountName = () => {
	const { authStore } = useApiStore();
	const [isEditing, setIsEditing] = useState(false);
	const [name, setName] = useState(authStore.account?.name);
	const [error, setError] = useState<string | undefined>();

	const handleOnChange = useCallback((event: ChangeEvent<HTMLInputElement>) => {
		const validation = nameSchema.safeParse(event.currentTarget.value);
		setName(event.currentTarget.value);

		if (validation.success) setError(undefined);
		else setError(validation.error.errors.at(0)?.message);
	}, []);

	const onEditClick = useCallback(() => {
		setIsEditing(true);
	}, []);

	const onCancelClick = useCallback(() => {
		setIsEditing(false);
		setError(undefined);

		runInAction(() => {
			setName(authStore.account?.name);
		});
	}, [authStore.account?.name]);

	const onConfirmClick = useCallback(() => {
		if (error || !name) return;

		void authStore
			.updateName(name)
			// eslint-disable-next-line promise/prefer-await-to-then
			.then(() => {
				runInAction(() => {
					setIsEditing(false);
				});
			});
	}, [authStore, error, name]);

	if (!authStore.account) return null;

	return (
		<div id="name">
			<Heading anchor="#name" className="mt-8" size={2}>
				Name
			</Heading>
			<Grid mt="1.5rem" px={2}>
				<Grid.Col span={isEditing ? 9 : 10}>
					<TextInput
						className="break-all"
						disabled={!isEditing}
						error={error}
						icon={isEditing ? <MdTitle /> : undefined}
						onChange={handleOnChange}
						value={name ? name : ""}
					/>
				</Grid.Col>
				<Grid.Col span={1}>
					{!isEditing && <EditIcon onClick={onEditClick} radius="md" size="lg" />}
					{isEditing && (
						<Button.Group>
							<ConfirmIcon
								disabled={authStore.nameUpdateLoading}
								loading={authStore.nameUpdateLoading}
								onClick={onConfirmClick}
								size="lg"
							/>
							<CancelIcon disabled={authStore.nameUpdateLoading} onClick={onCancelClick} size="lg" />
						</Button.Group>
					)}
				</Grid.Col>
			</Grid>
		</div>
	);
};

export default observer(AccountName);
