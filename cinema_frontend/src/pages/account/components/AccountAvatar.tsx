//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Avatar, Button, Center, Indicator } from "@mantine/core";
import { type FileWithPath } from "@mantine/dropzone";
import { useForm, zodResolver } from "@mantine/form";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback, useState } from "react";

import Heading from "../../../components/display/Heading";
import ImageInput from "../../../components/forms/ImageInput";
import useApiStore from "../../../hooks/useApiStore";
import { imageSchema } from "../../../schemas/accountSchemas";
import { blobToBase64 } from "../../../utils/converters";

import CancelIcon from "./CancelIcon";
import ConfirmIcon from "./ConfirmIcon";
import EditIcon from "./EditIcon";

const AccountAvatar = () => {
	const { authStore } = useApiStore();
	const [isEditing, setIsEditing] = useState(false);

	const form = useForm<{
		imageUrl: Blob | string | null;
	}>({
		initialValues: {
			imageUrl: authStore.account?.imageUrl ?? ""
		},
		validateInputOnBlur: true,
		validateInputOnChange: true,
		validate: zodResolver(imageSchema)
	});

	const handleOnDrop = useCallback(
		(files: FileWithPath[]) => {
			form.setFieldValue("imageUrl", new Blob([files[0]]));
		},
		[form]
	);

	const handleRemoveImage = useCallback(() => {
		form.setFieldValue("imageUrl", "");
	}, [form]);

	const onEditClick = useCallback(() => {
		setIsEditing(true);
	}, []);

	const onCancelClick = useCallback(() => {
		setIsEditing(false);
		handleRemoveImage();
	}, [handleRemoveImage]);

	const onConfirmClick = useCallback(async () => {
		let image: string | null | undefined;
		if (form.values.imageUrl instanceof Blob) {
			image = await blobToBase64(form.values.imageUrl);
		} else {
			image = form.values.imageUrl;
		}

		if (form.errors.imageUrl || !image) return;

		void authStore
			.updateImage(image)
			// eslint-disable-next-line promise/prefer-await-to-then
			.then(() => {
				runInAction(() => {
					setIsEditing(false);
				});
			});
	}, [authStore, form.errors.imageUrl, form.values.imageUrl]);

	if (!authStore.account) return null;

	return (
		<div id="avatar">
			<Heading anchor="#avatar" size={2}>
				Avatar
			</Heading>
			{!isEditing && (
				<Center mt="1.75rem">
					<Indicator
						label={<EditIcon onClick={onEditClick} radius="xl" size="xl" />}
						offset={32}
						position="top-end"
						size={0}
						zIndex={5}
					>
						<Avatar
							className="h-48 w-48 sm:h-52 sm:w-52 lg:h-56 lg:w-56"
							radius={360}
							src={authStore.account.imageUrl}
						/>
					</Indicator>
				</Center>
			)}
			{isEditing && (
				<>
					<ImageInput
						image={form.values.imageUrl}
						isTouched={form.isTouched("imageUrl")}
						label="Image"
						onDrop={handleOnDrop}
						removeImage={handleRemoveImage}
						required
						uploadMessage="Drag you image here"
						uploadedMessage="Image uploaded"
						{...form.getInputProps("imageUrl")}
					/>
					<Center mt="1rem">
						<Button.Group>
							<ConfirmIcon
								disabled={authStore.imageUpdateLoading}
								loading={authStore.imageUpdateLoading}
								onClick={onConfirmClick}
								size="lg"
							/>
							<CancelIcon disabled={authStore.imageUpdateLoading} onClick={onCancelClick} size="lg" />
						</Button.Group>
					</Center>
				</>
			)}
		</div>
	);
};

export default observer(AccountAvatar);
