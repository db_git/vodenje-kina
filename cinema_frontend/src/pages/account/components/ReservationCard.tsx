//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { ActionIcon, Button, Card, Flex, Group, Stack, Text, Title } from "@mantine/core";
import dayjs from "dayjs";
import { observer } from "mobx-react-lite";
import { useCallback, useState } from "react";
import { BiChevronDown, BiChevronUp, BiTime } from "react-icons/bi";
import { GiFilmProjector, GiTheater } from "react-icons/gi";
import { MdOutlineLocationOn } from "react-icons/md";

import Heading from "../../../components/display/Heading";
import HorizontalScroll from "../../../components/display/HorizontalScroll";
import MovieBackgroundImage from "../../movieDetails/components/MovieBackgroundImage";
import MoviePaper from "../../movieDetails/components/MoviePaper";
import OrderedGoodCard from "../../movieDetails/components/projections/OrderedGoodCard";
import { type ReservationCardProps } from "../account";

const ReservationCard = ({ reservation }: ReservationCardProps) => {
	const [showDetails, setShowDetails] = useState(false);

	const onDetailsClick = useCallback(() => {
		setShowDetails((previous) => {
			return !previous;
		});
	}, []);

	return (
		<Card p={0} radius="md" withBorder>
			<Card.Section>
				<MovieBackgroundImage movie={reservation.projection.movie}>
					<MoviePaper mih="10rem" radius={0}>
						<Stack align="center" spacing="xl">
							<Title mt="3rem" order={2}>
								{reservation.projection.movie.title}
							</Title>

							<Stack spacing={0}>
								<Flex align="center" direction="row" gap="md" justify="start" wrap="nowrap">
									<BiTime size={21} />
									<Text className="text-base sm:text-lg" fw={500} my="1rem">
										{dayjs(reservation.projection.time).format("LLLL")}
									</Text>
								</Flex>
								<Flex align="center" direction="row" gap="md" justify="start" mb="md" wrap="nowrap">
									<GiTheater size={21} />
									<Text className="text-base sm:text-lg">{reservation.projection.hall.name}</Text>
								</Flex>
								<Flex align="center" direction="row" gap="md" justify="start" mb="md" wrap="nowrap">
									<GiFilmProjector size={21} />
									<Text className="text-base sm:text-lg">
										{reservation.projection.hall.cinema.name}
									</Text>
								</Flex>
								<Flex align="center" direction="row" gap="md" justify="start" mb="md" wrap="nowrap">
									<MdOutlineLocationOn size={21} />
									<Text className="text-base sm:text-lg">
										{reservation.projection.hall.cinema.city},&nbsp;
										{reservation.projection.hall.cinema.street}
									</Text>
								</Flex>
							</Stack>
						</Stack>

						<Group align="flex-end" position="right">
							<div className="mb-3 pt-10">
								<ActionIcon onClick={onDetailsClick} variant="light">
									{showDetails ? <BiChevronUp /> : <BiChevronDown />}
								</ActionIcon>
							</div>
						</Group>
					</MoviePaper>
				</MovieBackgroundImage>
			</Card.Section>

			{showDetails && (
				<Stack mb="1rem" px={10}>
					{reservation.foodOrders && reservation.foodOrders.length > 0 && (
						<>
							<Heading className="mt-4" size={3}>
								Foods
							</Heading>
							<HorizontalScroll slidesToScroll={1} withControls>
								{reservation.foodOrders.map((food) => {
									return (
										<HorizontalScroll.Item key={food.id}>
											<OrderedGoodCard
												imageUrl={food.imageUrl}
												name={food.name}
												quantity={food.orderedQuantity}
											/>
										</HorizontalScroll.Item>
									);
								})}
							</HorizontalScroll>
						</>
					)}
					{reservation.souvenirOrders && reservation.souvenirOrders.length > 0 && (
						<>
							<Heading className="mt-4" size={3}>
								Foods
							</Heading>
							<HorizontalScroll slidesToScroll={1} withControls>
								{reservation.souvenirOrders.map((souvenir) => {
									return (
										<HorizontalScroll.Item key={souvenir.id}>
											<OrderedGoodCard
												imageUrl={souvenir.imageUrl}
												name={souvenir.name}
												quantity={souvenir.orderedQuantity}
											/>
										</HorizontalScroll.Item>
									);
								})}
							</HorizontalScroll>
						</>
					)}

					<Heading className="mt-4" size={3}>
						Seats
					</Heading>
					<Flex align="center" direction="row" gap="xs" justify="flex-start" wrap="wrap">
						{reservation.seats
							.map((seat) => {
								return seat;
							})
							.sort((s1, s2) => {
								return s1.number - s2.number;
							})
							.map((seat) => {
								return (
									<Button color="blue" compact key={seat.id} radius="xl" variant="filled">
										{seat.number}
									</Button>
								);
							})}
					</Flex>
				</Stack>
			)}
		</Card>
	);
};

export default observer(ReservationCard);
