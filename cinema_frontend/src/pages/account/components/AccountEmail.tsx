//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Text } from "@mantine/core";
import { observer } from "mobx-react-lite";

import Heading from "../../../components/display/Heading";
import useApiStore from "../../../hooks/useApiStore";

const AccountEmail = () => {
	const { authStore } = useApiStore();

	if (!authStore.account) return null;

	return (
		<div id="email">
			<Heading anchor="#email" className="mt-8" size={2}>
				Email
			</Heading>
			<Text className="break-all" mt="1.5rem" td="underline">
				{authStore.account.email}
			</Text>
		</div>
	);
};

export default observer(AccountEmail);
