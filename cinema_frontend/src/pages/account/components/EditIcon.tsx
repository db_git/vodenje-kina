//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { ActionIcon } from "@mantine/core";
import { VscEdit } from "react-icons/vsc";

import { type EditIconProps } from "../account";
import { getIconSize } from "../utils/getIconSize";

const EditIcon = ({ radius, size, onClick }: EditIconProps) => {
	const iconSize = getIconSize(size);

	return (
		<ActionIcon color="yellow" onClick={onClick} radius={radius} size={size} variant="filled">
			<VscEdit size={iconSize} />
		</ActionIcon>
	);
};

export default EditIcon;
