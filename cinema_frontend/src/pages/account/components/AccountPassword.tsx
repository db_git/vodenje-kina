//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Center, PasswordInput, Stack } from "@mantine/core";
import { useForm, zodResolver } from "@mantine/form";
import { observer } from "mobx-react-lite";
import { useCallback, useState } from "react";
import { SlLock } from "react-icons/sl";
import { VscEdit } from "react-icons/vsc";

import Heading from "../../../components/display/Heading";
import useApiStore from "../../../hooks/useApiStore";
import { passwordChangeSchema } from "../../../schemas/accountSchemas";

const AccountPassword = () => {
	const { authStore } = useApiStore();
	const [isEditing, setIsEditing] = useState(false);

	const form = useForm<{
		oldPassword: string;
		newPassword: string;
	}>({
		initialValues: {
			oldPassword: "",
			newPassword: ""
		},
		validateInputOnBlur: true,
		validateInputOnChange: true,
		validate: zodResolver(passwordChangeSchema)
	});

	const showPasswordForm = useCallback(() => {
		setIsEditing(true);
	}, []);

	const hidePasswordForm = useCallback(() => {
		setIsEditing(false);
	}, []);

	const handlePasswordChange = useCallback(() => {
		if (form.isValid("oldPassword") && form.isValid("newPassword")) {
			void authStore.updatePassword(form.values.oldPassword, form.values.newPassword);
		}
	}, [authStore, form]);

	return (
		<div className="mt-4" id="password">
			<Heading anchor="#password" size={2}>
				Password
			</Heading>

			{!isEditing && (
				<Center mt="1rem">
					<Button color="yellow" onClick={showPasswordForm} radius="md" rightIcon={<VscEdit />}>
						Change password
					</Button>
				</Center>
			)}

			{isEditing && (
				<Stack mt="1rem">
					<PasswordInput
						disabled={authStore.passwordChangeLoading}
						icon={<SlLock />}
						label="Old password"
						required
						{...form.getInputProps("oldPassword")}
					/>
					<PasswordInput
						disabled={authStore.passwordChangeLoading}
						icon={<SlLock />}
						label="New password"
						required
						{...form.getInputProps("newPassword")}
					/>

					<Center>
						<Button.Group>
							<Button
								color="green"
								disabled={authStore.passwordChangeLoading}
								loading={authStore.passwordChangeLoading}
								onClick={handlePasswordChange}
							>
								Change password
							</Button>
							<Button
								color="red"
								disabled={authStore.passwordChangeLoading}
								onClick={hidePasswordForm}
								variant="light"
							>
								Cancel
							</Button>
						</Button.Group>
					</Center>
				</Stack>
			)}
		</div>
	);
};

export default observer(AccountPassword);
