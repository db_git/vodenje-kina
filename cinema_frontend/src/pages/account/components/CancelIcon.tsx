//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { ActionIcon } from "@mantine/core";
import { AiOutlineClose } from "react-icons/ai";

import { type CancelIconProps } from "../account";
import { getIconSize } from "../utils/getIconSize";

const actionIconStyle = () => {
	return {
		borderRadius: "0 6px 6px 0"
	};
};

const CancelIcon = ({ disabled, size, onClick }: CancelIconProps) => {
	const iconSize = getIconSize(size);

	return (
		<ActionIcon
			color="red"
			disabled={disabled}
			onClick={onClick}
			radius={0}
			size={size}
			sx={actionIconStyle}
			variant="light"
		>
			<AiOutlineClose size={iconSize} />
		</ActionIcon>
	);
};

export default CancelIcon;
