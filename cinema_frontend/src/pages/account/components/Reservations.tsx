//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Grid } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { useCallback } from "react";

import Pagination from "../../../components/pagination/Pagination";
import useApiStore from "../../../hooks/useApiStore";
import { type ReservationsProps } from "../account";

import ReservationCard from "./ReservationCard";

const Reservations = ({ reservations }: ReservationsProps) => {
	const { reservationStore } = useApiStore();

	const modifyUrl = useCallback(async () => {
		await reservationStore.getAllAccount();
	}, [reservationStore]);

	return (
		<>
			<Grid align="center">
				{reservations.map((reservation) => {
					return (
						<Grid.Col key={reservation.id} md={6} span={12}>
							<ReservationCard reservation={reservation} />
						</Grid.Col>
					);
				})}
			</Grid>

			<Pagination modifyUrl={modifyUrl} store={reservationStore} />
		</>
	);
};

export default observer(Reservations);
