//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { ActionIcon } from "@mantine/core";
import { AiOutlineCheck } from "react-icons/ai";

import { type ConfirmIconProps } from "../account";
import { getIconSize } from "../utils/getIconSize";

const actionIconStyle = () => {
	return {
		borderRadius: "6px 0 0 6px"
	};
};

const ConfirmIcon = ({ loading, disabled, size, onClick }: ConfirmIconProps) => {
	const iconSize = getIconSize(size);

	return (
		<ActionIcon
			color="green"
			disabled={disabled}
			loading={loading}
			onClick={onClick}
			radius={0}
			size={size}
			sx={actionIconStyle}
			variant="filled"
		>
			<AiOutlineCheck size={iconSize} />
		</ActionIcon>
	);
};

export default ConfirmIcon;
