//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type Reservation } from "../../types/entities";

export type IconSize = "lg" | "md" | "sm" | "xl" | "xs";

export type IconProps = {
	readonly size: IconSize;
	readonly radius?: IconSize;
	readonly onClick?: () => void;
};

export type EditIconProps = IconProps;

export type CancelIconProps = Omit<IconProps, "radius"> & {
	readonly disabled: boolean;
};

export type ConfirmIconProps = Omit<IconProps, "radius"> & {
	readonly loading: boolean;
	readonly disabled: boolean;
};

export type ReservationsProps = {
	readonly reservations: Reservation[];
};

export type ReservationCardProps = {
	readonly reservation: Reservation;
};
