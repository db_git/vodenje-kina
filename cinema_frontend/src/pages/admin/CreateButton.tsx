//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Center } from "@mantine/core";
import { TbPlus } from "react-icons/tb";
import { NavLink } from "react-router-dom";

import { type CreateButtonProps } from "./admin";

const CreateButton = ({ className, mt, center, location, fullWidth, size }: CreateButtonProps) => {
	const button = (
		<Button
			className={className}
			component={NavLink}
			fullWidth={fullWidth}
			mt={mt}
			radius="md"
			rightIcon={<TbPlus size={15} />}
			size={size}
			to={location}
			variant="default"
		>
			Create new
		</Button>
	);

	return center ? <Center>{button}</Center> : button;
};

export default CreateButton;
