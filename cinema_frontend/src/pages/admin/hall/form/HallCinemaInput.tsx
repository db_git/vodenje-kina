//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Select } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useEffect, useRef, useState } from "react";
import { GiFilmProjector } from "react-icons/gi";

import useApiStore from "../../../../hooks/useApiStore";
import useGetStore from "../../../../hooks/useGetStore";
import { QueryParameterOptions } from "../../../../types/enums/Stores";
import { type InputSelectOptions } from "../../../../types/forms";
import { type HallCinemaInputProps } from "../hall";
import { changeHallCinemaSearch } from "../utils/changeHallSearch";

const HallCinemaInput = ({ form }: HallCinemaInputProps) => {
	const { hallStore } = useApiStore();
	const { cinemaGetStore } = useGetStore();

	const changeCinemaSearch = useRef(changeHallCinemaSearch).current;
	const [selectOptions, setSelectOptions] = useState<InputSelectOptions[]>(
		hallStore.response?.cinema
			? [
					{
						value: hallStore.response.cinema.id,
						label: hallStore.response.cinema.name
					}
			  ]
			: []
	);

	const loading =
		hallStore.loading.get ||
		hallStore.loading.post ||
		hallStore.loading.put ||
		cinemaGetStore.loading.get ||
		cinemaGetStore.loading.getAll;

	useEffect(() => {
		runInAction(() => {
			if (cinemaGetStore.responses === undefined) {
				(async () => {
					await cinemaGetStore.getAll();
				})();
			}
		});

		return () => {
			cinemaGetStore.setQueryParameters(QueryParameterOptions.Search, "");
			cinemaGetStore.setQueryParameters(QueryParameterOptions.Size, 10);
			(async () => {
				await cinemaGetStore.getAll();
			})();
		};
	}, [cinemaGetStore]);

	useEffect(() => {
		changeCinemaSearch(cinemaGetStore, setSelectOptions, "");
	}, [changeCinemaSearch, cinemaGetStore, setSelectOptions]);

	useEffect(() => {
		return () => {
			changeCinemaSearch.cancel();
		};
	}, [changeCinemaSearch]);

	const handleOnSearch = (query: string): void => {
		changeCinemaSearch(cinemaGetStore, setSelectOptions, query);
	};

	return (
		<Select
			data={selectOptions}
			disabled={loading}
			icon={<GiFilmProjector />}
			label="Cinema"
			mt="1rem"
			nothingFound="No cinemas found"
			onSearchChange={handleOnSearch}
			placeholder="Select cinema"
			required
			searchable
			{...form.getInputProps("cinema")}
		/>
	);
};

export default observer(HallCinemaInput);
