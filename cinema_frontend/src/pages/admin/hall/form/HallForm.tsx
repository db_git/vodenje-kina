//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { observer } from "mobx-react-lite";

import Form from "../../../../components/forms/Form";
import Loading from "../../../../components/status/Loading";
import useApiStore from "../../../../hooks/useApiStore";
import { hallFormSchema } from "../../../../schemas/adminSchemas";
import { type AdminFormProps } from "../../admin";
import { type HallFormInputValues, type HallFormSubmitValues } from "../hall";

import HallCinemaInput from "./HallCinemaInput";
import HallNameInput from "./HallNameInput";

const HallForm = ({ id }: AdminFormProps) => {
	const { hallStore } = useApiStore();

	const handleSubmit = (values: HallFormSubmitValues): void => {
		if (id) {
			void hallStore.put(id, values);
		} else {
			void hallStore.post(values);
		}
	};

	if (hallStore.loading.get || (id && !hallStore.response)) return <Loading />;

	const initialValues: HallFormInputValues = {
		name: hallStore.response?.name ?? "",
		cinema: hallStore.response?.cinema.id ?? ""
	};

	return (
		<Form id="hallForm" initialValues={initialValues} onSubmit={handleSubmit} schema={hallFormSchema}>
			{(form) => {
				return (
					<>
						<HallNameInput form={form} />
						<HallCinemaInput form={form} />
					</>
				);
			}}
		</Form>
	);
};

export default observer(HallForm);
