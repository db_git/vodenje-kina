//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import debounce from "lodash.debounce";
import { runInAction } from "mobx";
import { type Dispatch, type SetStateAction } from "react";

import { type ICinemaGetStore } from "../../../../stores/stores";
import { QueryParameterOptions } from "../../../../types/enums/Stores";
import { type InputSelectOptions } from "../../../../types/forms";

const changeHallCinemaSearch = debounce(
	(
		cinemaGetStore: ICinemaGetStore,
		setSelectOptions: Dispatch<SetStateAction<InputSelectOptions[]>>,
		search: string
	) => {
		runInAction(() => {
			cinemaGetStore.setQueryParameters(QueryParameterOptions.Search, search);
			cinemaGetStore.setQueryParameters(QueryParameterOptions.Page, 1);
			cinemaGetStore.setQueryParameters(QueryParameterOptions.Size, 25);
			cinemaGetStore.setQueryParameters(QueryParameterOptions.OrderBy, 1);

			void cinemaGetStore.getAll();

			let cinemaOptions: InputSelectOptions[] = [];
			if (cinemaGetStore.responses) {
				cinemaOptions = cinemaGetStore.responses.data.map((cinema) => {
					return { value: cinema.id, label: cinema.name };
				});
			}

			setSelectOptions(cinemaOptions);
		});
	},
	500
);

export { changeHallCinemaSearch };
