//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { useParams } from "react-router-dom";

import useApiStore from "../../../hooks/useApiStore";
import EditAdminEntityBase from "../EditAdminEntityBase";

import HallForm from "./form/HallForm";

const EditHall = () => {
	const { id } = useParams();
	const { hallStore } = useApiStore();

	useEffect(() => {
		runInAction(() => {
			document.title = hallStore.response ? `Edit ${hallStore.response.name} | Admin` : "Loading...";
		});
	}, [hallStore.response]);

	useEffect(() => {
		runInAction(() => {
			if (hallStore.response === undefined && id) void hallStore.get(id);
		});
	}, [id, hallStore]);

	return (
		<EditAdminEntityBase formId="hallForm" noContentMessage="Hall does not exist." store={hallStore}>
			<HallForm id={id} />
		</EditAdminEntityBase>
	);
};

export default observer(EditHall);
