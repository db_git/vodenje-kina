//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button } from "@mantine/core";
import { forwardRef, memo } from "react";
import { IoMdArrowDropdown } from "react-icons/io";

const DisplayColumns = forwardRef<HTMLDivElement>((props, ref) => {
	return (
		<div ref={ref}>
			<Button radius="md" rightIcon={<IoMdArrowDropdown size={16} />} variant="default" {...props}>
				Display columns
			</Button>
		</div>
	);
});

export default memo(DisplayColumns);
