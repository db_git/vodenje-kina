//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import debounce from "lodash.debounce";
import { runInAction } from "mobx";
import { type Dispatch, type SetStateAction } from "react";

import { type IHallGetStore } from "../../../../stores/stores";
import { QueryParameterOptions } from "../../../../types/enums/Stores";
import { type InputSelectOptions } from "../../../../types/forms";

const changeSeatHallSearch = debounce(
	(hallGetStore: IHallGetStore, setSelectOptions: Dispatch<SetStateAction<InputSelectOptions[]>>, search: string) => {
		runInAction(() => {
			hallGetStore.setQueryParameters(QueryParameterOptions.Search, search);
			hallGetStore.setQueryParameters(QueryParameterOptions.Page, 1);
			hallGetStore.setQueryParameters(QueryParameterOptions.Size, 20);
			hallGetStore.setQueryParameters(QueryParameterOptions.OrderBy, 1);

			void hallGetStore.getAll();

			let hallOptions: InputSelectOptions[] = [];
			if (hallGetStore.responses) {
				hallOptions = hallGetStore.responses.data.map((hall) => {
					return { value: hall.id, label: hall.name };
				});
			}

			setSelectOptions(hallOptions);
		});
	},
	500
);

export { changeSeatHallSearch };
