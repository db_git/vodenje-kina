//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { runInAction } from "mobx";
import { type NavigateFunction } from "react-router-dom";

import { Routes } from "../../../../routes/Routes";
import { type ISeatApiStore } from "../../../../stores/stores";
import { SeatOrderBy } from "../../../../types/enums/OrderBy";
import { QueryParameterOptions } from "../../../../types/enums/Stores";

const changeSeatNumberSort = (seatStore: ISeatApiStore, url: NavigateFunction) => {
	runInAction(() => {
		seatStore.setQueryParameters(
			QueryParameterOptions.OrderBy,
			(seatStore.queryParameters.orderBy % SeatOrderBy.NumberDescending) + SeatOrderBy.NumberAscending
		);
		seatStore.modifyUrl(Routes.AdminSeat, url);
		void seatStore.getAll();
	});
};

export { changeSeatNumberSort };
