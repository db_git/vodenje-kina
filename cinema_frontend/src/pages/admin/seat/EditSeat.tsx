//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { useParams } from "react-router-dom";

import useApiStore from "../../../hooks/useApiStore";
import EditAdminEntityBase from "../EditAdminEntityBase";

import SeatForm from "./form/SeatForm";

const EditSeat = () => {
	const { id } = useParams();
	const { seatStore } = useApiStore();

	useEffect(() => {
		runInAction(() => {
			document.title = seatStore.response ? `Edit ${seatStore.response.number} | Admin` : "Loading...";
		});
	}, [seatStore.response]);

	useEffect(() => {
		runInAction(() => {
			if (seatStore.response === undefined && id) void seatStore.get(id);
		});
	}, [id, seatStore]);

	return (
		<EditAdminEntityBase formId="seatForm" noContentMessage="Seat does not exist." store={seatStore}>
			<SeatForm id={id} />
		</EditAdminEntityBase>
	);
};

export default observer(EditSeat);
