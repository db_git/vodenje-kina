//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { NumberInput } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { MdOutlineChair } from "react-icons/md";

import useApiStore from "../../../../hooks/useApiStore";
import { type SeatNumberInputProps } from "../seat";

const SeatNumberInput = ({ form }: SeatNumberInputProps) => {
	const { seatStore } = useApiStore();

	return (
		<NumberInput
			disabled={seatStore.loading.get || seatStore.loading.post || seatStore.loading.put}
			icon={<MdOutlineChair />}
			label="Number"
			max={500}
			min={1}
			placeholder="Enter seat number"
			required
			step={1}
			{...form.getInputProps("number")}
		/>
	);
};

export default observer(SeatNumberInput);
