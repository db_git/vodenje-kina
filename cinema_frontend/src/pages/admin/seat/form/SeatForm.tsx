//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { observer } from "mobx-react-lite";

import Form from "../../../../components/forms/Form";
import Loading from "../../../../components/status/Loading";
import useApiStore from "../../../../hooks/useApiStore";
import { seatFormSchema } from "../../../../schemas/adminSchemas";
import { type AdminFormProps } from "../../admin";
import { type SeatFormInputValues, type SeatFormSubmitValues } from "../seat";

import SeatHallInput from "./SeatHallInput";
import SeatNumberInput from "./SeatNumberInput";

const SeatForm = ({ id }: AdminFormProps) => {
	const { seatStore } = useApiStore();

	const handleSubmit = (values: SeatFormSubmitValues): void => {
		if (id) {
			void seatStore.put(id, values);
		} else {
			void seatStore.post(values);
		}
	};

	if (seatStore.loading.get || (id && !seatStore.response)) return <Loading />;

	const initialValues: SeatFormInputValues = {
		number: seatStore.response?.number ?? 0,
		halls:
			seatStore.response?.halls.map((hall) => {
				return hall.id;
			}) ?? []
	};

	return (
		<Form id="seatForm" initialValues={initialValues} onSubmit={handleSubmit} schema={seatFormSchema}>
			{(form) => {
				return (
					<>
						<SeatNumberInput form={form} />
						<SeatHallInput form={form} />
					</>
				);
			}}
		</Form>
	);
};

export default observer(SeatForm);
