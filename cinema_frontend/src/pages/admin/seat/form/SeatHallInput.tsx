//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { MultiSelect } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useEffect, useRef, useState } from "react";
import { FaTheaterMasks } from "react-icons/fa";

import useApiStore from "../../../../hooks/useApiStore";
import useGetStore from "../../../../hooks/useGetStore";
import { QueryParameterOptions } from "../../../../types/enums/Stores";
import { type InputSelectOptions } from "../../../../types/forms";
import { type SeatHallInputProps } from "../seat";
import { changeSeatHallSearch } from "../utils/changeSeatSearch";

const SeatHallInput = ({ form }: SeatHallInputProps) => {
	const { hallGetStore } = useGetStore();
	const { seatStore } = useApiStore();

	const changeSearch = useRef(changeSeatHallSearch).current;
	const [selectOptions, setSelectOptions] = useState<InputSelectOptions[]>(
		seatStore.response?.halls.map((hall) => {
			return {
				value: hall.id,
				label: hall.name
			};
		}) ?? []
	);

	const loading =
		seatStore.loading.get ||
		seatStore.loading.post ||
		seatStore.loading.put ||
		hallGetStore.loading.get ||
		hallGetStore.loading.getAll;

	useEffect(() => {
		runInAction(() => {
			if (hallGetStore.responses === undefined) {
				(async () => {
					await hallGetStore.getAll();
				})();
			}
		});

		return () => {
			hallGetStore.setQueryParameters(QueryParameterOptions.Search, "");
			hallGetStore.setQueryParameters(QueryParameterOptions.Size, 10);
			(async () => {
				await hallGetStore.getAll();
			})();
		};
	}, [hallGetStore]);

	useEffect(() => {
		changeSearch(hallGetStore, setSelectOptions, "");
	}, [changeSearch, hallGetStore, setSelectOptions]);

	useEffect(() => {
		return () => {
			changeSearch.cancel();
		};
	}, [changeSearch]);

	const handleOnSearch = (query: string): void => {
		changeSearch(hallGetStore, setSelectOptions, query);
	};

	return (
		<MultiSelect
			data={selectOptions}
			disabled={loading}
			icon={<FaTheaterMasks />}
			label="Halls"
			nothingFound="No halls found"
			onSearchChange={handleOnSearch}
			required
			searchable
			{...form.getInputProps("halls")}
		/>
	);
};

export default observer(SeatHallInput);
