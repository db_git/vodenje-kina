//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Text } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { type ReactNode, useState } from "react";

import useApiStore from "../../../../hooks/useApiStore";
import { Routes } from "../../../../routes/Routes";
import DeleteModal from "../../DeleteModal";
import TableActionIcons from "../../utils/TableActionIcons";

const SeatTableBody = () => {
	const { seatStore } = useApiStore();
	const [id, setId] = useState("");
	const [modalMessage, setModalMessage] = useState("");
	const [notificationSuccessMessage, setNotificationSuccessMessage] = useState<ReactNode>();
	const [deleteModalOpen, setDeleteModalOpen] = useState(false);

	return (
		<>
			<DeleteModal
				id={id}
				isOpen={deleteModalOpen}
				message={modalMessage}
				notificationSuccessMessage={notificationSuccessMessage}
				store={seatStore}
				toggle={() => {
					setDeleteModalOpen(!deleteModalOpen);
				}}
			/>

			{seatStore.responses?.data.map((seat) => {
				return (
					<tr key={seat.id}>
						<td>
							<Text pr="5rem" weight={500}>
								{seat.number}
							</Text>
						</td>
						<TableActionIcons
							location={`${Routes.AdminSeatEdit}/${seat.id}`}
							onDeleteClick={() => {
								runInAction(() => {
									setId(seat.id);
									setModalMessage(`Are you sure you want to delete seat ${seat.number}?`);
									setNotificationSuccessMessage(`Seat '${seat.number}' deleted successfully.`);
								});
								setDeleteModalOpen(!deleteModalOpen);
							}}
						/>
					</tr>
				);
			})}
		</>
	);
};

export default observer(SeatTableBody);
