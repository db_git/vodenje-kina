//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Container, Grid } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { NavLink, useLocation } from "react-router-dom";

import Loading from "../../components/status/Loading";
import NoContent from "../../components/status/NoContent";
import ServerError from "../../components/status/ServerError";
import { HttpMethod } from "../../types/enums/Http";
import { is5xxStatus, isNotFoundOrNoContent } from "../../utils/isStatus";

import { type EditAdminEntityBaseProps } from "./admin";

const EditAdminEntityBase = <T,>({
	store,
	noContentMessage,
	formId,
	children,
	containerSize
}: EditAdminEntityBaseProps<T>) => {
	const route = useLocation().pathname;

	useEffect(() => {
		return () => {
			store.setResponse(undefined);
			store.setStatus(HttpMethod.Put, 0);
		};
	}, [store]);

	if (isNotFoundOrNoContent(store.status.get)) return <NoContent message={noContentMessage} />;
	if (is5xxStatus(store.status.get)) return <ServerError />;
	if (store.loading.get) return <Loading />;

	return (
		<Container size={containerSize}>
			<Grid grow justify="center">
				<Grid.Col span={12}>{children}</Grid.Col>

				<Grid.Col mt="2rem" span={12}>
					<Grid>
						<Grid.Col md={8} xs={12}>
							<Button
								color="yellow"
								disabled={store.loading.get}
								form={formId}
								fullWidth
								loading={store.loading.put}
								type="submit"
							>
								Update
							</Button>
						</Grid.Col>
						<Grid.Col md={4} xs={12}>
							<Button
								color="red"
								component={NavLink}
								disabled={store.loading.put || store.loading.get}
								fullWidth
								to={route.slice(0, route.indexOf("/edit"))}
								variant="light"
							>
								Go back
							</Button>
						</Grid.Col>
					</Grid>
				</Grid.Col>
			</Grid>
		</Container>
	);
};

EditAdminEntityBase.defaultProps = {
	containerSize: "md"
};

export default observer(EditAdminEntityBase);
