//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Container } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";

import useApiStore from "../../../hooks/useApiStore";
import { Routes } from "../../../routes/Routes";
import { type IApiStore } from "../../../stores/stores";
import { type MovieRating, type UserRating } from "../../../types/entities";
import { type QueryParameters } from "../../../types/query";
import { type UserRatingFormSubmitValues } from "../../movieDetails/movieDetails";
import AdminEntityBase from "../AdminEntityBase";

import UserTableBody from "./table/UserTableBody";
import UserTableHead from "./table/UserTableHead";
import UserRatingOptions from "./utils/UserRatingOptions";

const AdminUserRating = () => {
	const { userRatingStore } = useApiStore();

	useEffect(() => {
		document.title = "User rating | Admin";
	}, []);

	return (
		<Container size="xl">
			<AdminEntityBase
				location={Routes.AdminUserRating}
				store={
					userRatingStore as unknown as IApiStore<
						UserRating,
						QueryParameters,
						MovieRating,
						UserRatingFormSubmitValues,
						Record<string, unknown>
					>
				}
				tbody={<UserTableBody />}
				thead={<UserTableHead />}
				withCreateButton={false}
			>
				<UserRatingOptions />
			</AdminEntityBase>
		</Container>
	);
};

export default observer(AdminUserRating);
