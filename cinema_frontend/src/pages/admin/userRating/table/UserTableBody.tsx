//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Avatar, Rating, Text } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback, useState } from "react";

import useApiStore from "../../../../hooks/useApiStore";
import { type IApiStore } from "../../../../stores/stores";
import { type MovieRating, type UserRating } from "../../../../types/entities";
import { type QueryParameters } from "../../../../types/query";
import { type UserRatingFormSubmitValues } from "../../../movieDetails/movieDetails";
import DeleteModal from "../../DeleteModal";
import DeleteIcon from "../../utils/DeleteIcon";

const UserTableBody = () => {
	const { userRatingStore } = useApiStore();
	const [selectedRating, setSelectedRating] = useState<UserRating>();
	const [deleteModalOpen, setDeleteModalOpen] = useState(false);

	const deleteRating = useCallback(async () => {
		await runInAction(async () => {
			if (!selectedRating) return;

			await userRatingStore.deleteRating(selectedRating.movie.id, selectedRating.user.id, false);
			await userRatingStore.getAll();
		});
	}, [selectedRating, userRatingStore]);

	const toggleModal = useCallback(() => {
		setDeleteModalOpen(!deleteModalOpen);
	}, [deleteModalOpen]);

	return (
		<>
			<DeleteModal
				customDelete={deleteRating}
				id={selectedRating?.movie.id ?? ""}
				isOpen={deleteModalOpen}
				message="Are you sure you want to remove this rating?"
				notificationSuccessMessage="Rating deleted successfully."
				store={
					userRatingStore as unknown as IApiStore<
						UserRating,
						QueryParameters,
						MovieRating,
						UserRatingFormSubmitValues,
						Record<string, unknown>
					>
				}
				toggle={toggleModal}
			/>

			{userRatingStore.responses?.data.map((rating) => {
				return (
					<tr key={`${rating.user.id}${rating.user.name}${rating.movie.id}${rating.movie.title}`}>
						{userRatingStore.showImage && (
							<td>
								<Avatar radius="xl" src={rating.user.imageUrl} />
							</td>
						)}
						<td>
							<Text pr="1rem" weight={500}>
								{rating.user.name}
							</Text>
						</td>
						<td>
							<Text pr="1rem" weight={500}>
								{rating.movie.title}
							</Text>
						</td>
						<td>
							<Rating color="yellow.6" defaultValue={rating.rating} readOnly />
						</td>
						<td>
							<DeleteIcon
								onDeleteClick={() => {
									runInAction(() => {
										setSelectedRating(rating);
									});

									toggleModal();
								}}
							/>
						</td>
					</tr>
				);
			})}
		</>
	);
};

export default observer(UserTableBody);
