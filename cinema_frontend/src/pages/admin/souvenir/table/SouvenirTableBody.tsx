//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Text } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { type ReactNode, useState } from "react";

import useApiStore from "../../../../hooks/useApiStore";
import { Routes } from "../../../../routes/Routes";
import { useCurrencyFormatter } from "../../../../utils/converters";
import DeleteModal from "../../DeleteModal";
import TableActionIcons from "../../utils/TableActionIcons";
import TableImage from "../../utils/TableImage";

const SouvenirTableBody = () => {
	const { souvenirStore } = useApiStore();
	const [id, setId] = useState("");
	const [modalMessage, setModalMessage] = useState("");
	const [notificationSuccessMessage, setNotificationSuccessMessage] = useState<ReactNode>();
	const [deleteModalOpen, setDeleteModalOpen] = useState(false);
	const currencyFormatter = useCurrencyFormatter();

	return (
		<>
			<DeleteModal
				id={id}
				isOpen={deleteModalOpen}
				message={modalMessage}
				notificationSuccessMessage={notificationSuccessMessage}
				store={souvenirStore}
				toggle={() => {
					setDeleteModalOpen(!deleteModalOpen);
				}}
			/>

			{souvenirStore.responses?.data.map((souvenir) => {
				return (
					<tr key={souvenir.id}>
						{souvenirStore.showImage && (
							<td>
								<TableImage height="6rem" radius="xl" src={souvenir.imageUrl} width="6rem" withBorder />
							</td>
						)}
						<td>
							<Text pr="1rem" weight={500}>
								{souvenir.name}
							</Text>
						</td>
						<td>
							<Text pr="2rem">{currencyFormatter.format(souvenir.price)}</Text>
						</td>
						<td>
							<Text color={souvenir.availableQuantity < 10 ? "red" : ""} pr="5rem">
								{souvenir.availableQuantity}
							</Text>
						</td>
						<TableActionIcons
							location={`${Routes.AdminSouvenirEdit}/${souvenir.id}`}
							onDeleteClick={() => {
								runInAction(() => {
									setId(souvenir.id);
									setModalMessage(`Are you sure you want to delete souvenir ${souvenir.name}?`);
									setNotificationSuccessMessage(`Souvenir '${souvenir.name}' deleted successfully.`);
								});
								setDeleteModalOpen(!deleteModalOpen);
							}}
						/>
					</tr>
				);
			})}
		</>
	);
};

export default observer(SouvenirTableBody);
