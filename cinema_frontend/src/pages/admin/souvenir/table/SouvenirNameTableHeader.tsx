//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { observer } from "mobx-react-lite";
import { useNavigate } from "react-router-dom";

import useApiStore from "../../../../hooks/useApiStore";
import { SouvenirOrderBy } from "../../../../types/enums/OrderBy";
import TableHeaderSort from "../../utils/TableHeaderSort";
import { changeSouvenirNameSort } from "../utils/changeSouvenirSort";

const SouvenirNameTableHeader = () => {
	const url = useNavigate();
	const { souvenirStore } = useApiStore();

	const handleSouvenirNameSort = (): void => {
		changeSouvenirNameSort(souvenirStore, url);
	};

	return (
		<TableHeaderSort
			ascending={souvenirStore.queryParameters.orderBy === SouvenirOrderBy.NameAscending}
			descending={souvenirStore.queryParameters.orderBy === SouvenirOrderBy.NameDescending}
			key="name"
			onClick={handleSouvenirNameSort}
		>
			Name
		</TableHeaderSort>
	);
};

export default observer(SouvenirNameTableHeader);
