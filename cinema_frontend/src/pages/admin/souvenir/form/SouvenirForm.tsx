//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Grid } from "@mantine/core";
import { observer } from "mobx-react-lite";

import Form from "../../../../components/forms/Form";
import Loading from "../../../../components/status/Loading";
import useApiStore from "../../../../hooks/useApiStore";
import { souvenirFormSchema } from "../../../../schemas/adminSchemas";
import { blobToBase64 } from "../../../../utils/converters";
import { type AdminFormProps } from "../../admin";
import { type SouvenirFormInputValues, type SouvenirFormSubmitValues } from "../souvenir";

import SouvenirAvailableQuantityInput from "./SouvenirAvailableQuantityInput";
import SouvenirImageInput from "./SouvenirImageInput";
import SouvenirNameInput from "./SouvenirNameInput";
import SouvenirPriceInput from "./SouvenirPriceInput";

const SouvenirForm = ({ id }: AdminFormProps) => {
	const { souvenirStore } = useApiStore();

	const handleSubmit = async (values: SouvenirFormInputValues): Promise<void> => {
		const newValues: SouvenirFormSubmitValues = {
			...values,
			imageUrl: values.imageUrl instanceof Blob ? await blobToBase64(values.imageUrl) : values.imageUrl
		};

		if (id) {
			await souvenirStore.put(id, newValues);
		} else {
			await souvenirStore.post(newValues);
		}
	};

	if (souvenirStore.loading.get || (id && !souvenirStore.response)) return <Loading />;

	const initialValues: SouvenirFormInputValues = {
		name: souvenirStore.response?.name ?? "",
		price: souvenirStore.response?.price ?? 0,
		availableQuantity: souvenirStore.response?.availableQuantity ?? 0,
		imageUrl: souvenirStore.response?.imageUrl ?? ""
	};

	return (
		<Form id="souvenirForm" initialValues={initialValues} onSubmit={handleSubmit} schema={souvenirFormSchema}>
			{(form) => {
				return (
					<Grid>
						<Grid.Col span={12}>
							<SouvenirNameInput form={form} />
						</Grid.Col>

						<Grid.Col md={6} xs={12}>
							<SouvenirPriceInput form={form} />
						</Grid.Col>
						<Grid.Col md={6} xs={12}>
							<SouvenirAvailableQuantityInput form={form} />
						</Grid.Col>

						<Grid.Col span={12}>
							<SouvenirImageInput form={form} />
						</Grid.Col>
					</Grid>
				);
			}}
		</Form>
	);
};

export default observer(SouvenirForm);
