//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type FileWithPath } from "@mantine/dropzone";
import { observer } from "mobx-react-lite";
import { useCallback } from "react";

import ImageInput from "../../../../components/forms/ImageInput";
import useApiStore from "../../../../hooks/useApiStore";
import { type SouvenirImageInputProps } from "../souvenir";

const SouvenirImageInput = ({ form }: SouvenirImageInputProps) => {
	const { souvenirStore } = useApiStore();

	const handleOnDrop = useCallback(
		(files: FileWithPath[]) => {
			form.setFieldValue("imageUrl", new Blob([files[0]]));
		},
		[form]
	);

	const handleRemoveImage = useCallback(() => {
		form.setFieldValue("imageUrl", "");
	}, [form]);

	return (
		<ImageInput
			disabled={souvenirStore.loading.get || souvenirStore.loading.post || souvenirStore.loading.put}
			image={form.values.imageUrl}
			isTouched={form.isTouched("imageUrl")}
			label="Image"
			onDrop={handleOnDrop}
			removeImage={handleRemoveImage}
			required
			uploadMessage="Drag image here"
			uploadedMessage="Image uploaded."
			{...form.getInputProps("imageUrl")}
		/>
	);
};

export default observer(SouvenirImageInput);
