//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { useParams } from "react-router-dom";

import useApiStore from "../../../hooks/useApiStore";
import EditAdminEntityBase from "../EditAdminEntityBase";

import SouvenirForm from "./form/SouvenirForm";

const EditSouvenir = () => {
	const { id } = useParams();
	const { souvenirStore } = useApiStore();

	useEffect(() => {
		runInAction(() => {
			document.title = souvenirStore.response ? `Edit ${souvenirStore.response.name} | Admin` : "Loading...";
		});
	}, [souvenirStore.response]);

	useEffect(() => {
		runInAction(() => {
			if (souvenirStore.response === undefined && id) void souvenirStore.get(id);
		});
	}, [id, souvenirStore]);

	return (
		<EditAdminEntityBase formId="souvenirForm" noContentMessage="Souvenir does not exist." store={souvenirStore}>
			<SouvenirForm id={id} />
		</EditAdminEntityBase>
	);
};

export default observer(EditSouvenir);
