//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { runInAction } from "mobx";
import { type NavigateFunction } from "react-router-dom";

import { Routes } from "../../../../routes/Routes";
import { type ISouvenirApiStore } from "../../../../stores/stores";
import { SouvenirOrderBy } from "../../../../types/enums/OrderBy";
import { QueryParameterOptions } from "../../../../types/enums/Stores";

const changeSouvenirNameSort = (souvenirStore: ISouvenirApiStore, url: NavigateFunction): void => {
	runInAction(() => {
		if (
			souvenirStore.queryParameters.orderBy === SouvenirOrderBy.NameAscending ||
			souvenirStore.queryParameters.orderBy === SouvenirOrderBy.NameDescending
		) {
			souvenirStore.setQueryParameters(
				QueryParameterOptions.OrderBy,
				(souvenirStore.queryParameters.orderBy % SouvenirOrderBy.NameDescending) + SouvenirOrderBy.NameAscending
			);
		} else {
			souvenirStore.setQueryParameters(QueryParameterOptions.OrderBy, SouvenirOrderBy.NameAscending);
		}

		souvenirStore.modifyUrl(Routes.AdminSouvenir, url);
		void souvenirStore.getAll();
	});
};

const changeSouvenirPriceSort = (souvenirStore: ISouvenirApiStore, url: NavigateFunction): void => {
	runInAction(() => {
		if (
			souvenirStore.queryParameters.orderBy === SouvenirOrderBy.PriceAscending ||
			souvenirStore.queryParameters.orderBy === SouvenirOrderBy.PriceDescending
		) {
			souvenirStore.setQueryParameters(
				QueryParameterOptions.OrderBy,
				(souvenirStore.queryParameters.orderBy % (SouvenirOrderBy.PriceDescending / 2)) +
					SouvenirOrderBy.PriceAscending
			);
		} else {
			souvenirStore.setQueryParameters(QueryParameterOptions.OrderBy, SouvenirOrderBy.PriceAscending);
		}

		souvenirStore.modifyUrl(Routes.AdminSouvenir, url);
		void souvenirStore.getAll();
	});
};

const changeSouvenirAvailableQuantitySort = (souvenirStore: ISouvenirApiStore, url: NavigateFunction): void => {
	runInAction(() => {
		if (
			souvenirStore.queryParameters.orderBy === SouvenirOrderBy.AvailableQuantityAscending ||
			souvenirStore.queryParameters.orderBy === SouvenirOrderBy.AvailableQuantityDescending
		) {
			souvenirStore.setQueryParameters(
				QueryParameterOptions.OrderBy,
				(souvenirStore.queryParameters.orderBy % (SouvenirOrderBy.AvailableQuantityDescending / 3)) +
					SouvenirOrderBy.AvailableQuantityAscending
			);
		} else {
			souvenirStore.setQueryParameters(QueryParameterOptions.OrderBy, SouvenirOrderBy.AvailableQuantityAscending);
		}

		souvenirStore.modifyUrl(Routes.AdminSouvenir, url);
		void souvenirStore.getAll();
	});
};

export { changeSouvenirNameSort, changeSouvenirPriceSort, changeSouvenirAvailableQuantitySort };
