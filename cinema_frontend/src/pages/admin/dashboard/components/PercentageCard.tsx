//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Card, Grid, RingProgress, Stack, Text, Center, useMantineTheme } from "@mantine/core";

import { useNumberFormatter } from "../../../../utils/converters";
import { type PercentageCardProps } from "../dashboard";

const getPercentageColor = (percentage: number): "green" | "lime" | "orange" | "red" | "yellow" => {
	if (percentage < 20) {
		return "red";
	} else if (percentage >= 20 && percentage < 40) {
		return "orange";
	} else if (percentage >= 40 && percentage < 60) {
		return "yellow";
	} else if (percentage < 80) {
		return "lime";
	}

	return "green";
};

const PercentageCard = ({ percentage, children }: PercentageCardProps) => {
	const color = getPercentageColor(percentage);
	const theme = useMantineTheme();
	const numberFormatter = useNumberFormatter(2);

	const isDark = theme.colorScheme === "dark";
	const cardStyle = {
		border: `1px solid ${isDark ? theme.colors.dark[3] : theme.colors.gray[4]}`,
		backgroundColor: isDark ? theme.colors.dark[6] : theme.colors.gray[2]
	};

	return (
		<Card radius="lg" sx={cardStyle}>
			<Grid>
				<Grid.Col lg={5} span={12}>
					<Center>
						<RingProgress
							label={
								<Text align="center" color={`${color}.9`} size="xl" weight={700}>
									{numberFormatter.format(percentage)}%
								</Text>
							}
							rootColor={isDark ? theme.colors.gray[7] : theme.colors.gray[5]}
							roundCaps
							sections={[
								{
									value: percentage,
									color
								}
							]}
							size={220}
							thickness={16}
						/>
					</Center>
				</Grid.Col>

				<Grid.Col lg={7} span={12}>
					<Stack align="flex-start" className="h-full" justify="center" spacing="xs">
						{children}
					</Stack>
				</Grid.Col>
			</Grid>
		</Card>
	);
};

export default PercentageCard;
