//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Stack } from "@mantine/core";

import Heading from "../../../../components/display/Heading";
import MovieRatings from "../stats/MovieRatings";
import TopRatedMovies from "../stats/TopRatedMovies";
import WorstRatedMovies from "../stats/WorstRatedMovies";

const MovieStats = () => {
	return (
		<div id="movies">
			<Heading anchor="#movies" className="mb-4" size={2}>
				Movies
			</Heading>

			<Stack mt="0.5rem" spacing="xs">
				<MovieRatings />
				<TopRatedMovies />
				<WorstRatedMovies />
			</Stack>
		</div>
	);
};

export default MovieStats;
