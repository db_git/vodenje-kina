//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Card, Grid, Stack, Center, useMantineTheme, Skeleton } from "@mantine/core";

const PercentageCardSkeleton = () => {
	const theme = useMantineTheme();

	const isDark = theme.colorScheme === "dark";
	const cardStyle = {
		border: `1px solid ${isDark ? theme.colors.dark[3] : theme.colors.gray[4]}`,
		backgroundColor: isDark ? theme.colors.dark[6] : theme.colors.gray[1]
	};

	return (
		<Card radius="lg" sx={cardStyle}>
			<Grid>
				<Grid.Col lg={5} span={12}>
					<Center>
						<Skeleton animate className="rounded-full" h={200} w={200} />
					</Center>
				</Grid.Col>

				<Grid.Col lg={7} span={12}>
					<Stack align="flex-start" className="h-full" justify="center" spacing="xs">
						<Skeleton animate className="my-1" h="1rem" w="8rem" />
						<Skeleton animate className="my-1" h="1rem" w="12rem" />
						<Skeleton animate className="my-1" h="1rem" w="13rem" />
					</Stack>
				</Grid.Col>
			</Grid>
		</Card>
	);
};

export default PercentageCardSkeleton;
