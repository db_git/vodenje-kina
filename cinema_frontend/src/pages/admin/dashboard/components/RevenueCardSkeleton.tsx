//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Group, Paper, Text, Skeleton } from "@mantine/core";

const RevenueCardSkeleton = () => {
	return (
		<Paper p="xl" radius="md" withBorder>
			<Group position="apart">
				<div>
					<Skeleton animate h="0.75rem" w="8.5rem" />
					<Skeleton animate h="1.5rem" mt="0.5rem" w="7rem" />
				</div>
				<Skeleton animate h="2.5rem" radius="md" w="2.5rem" />
			</Group>
			<Text color="dimmed" mt="md" size="sm">
				<Skeleton animate h="0.75rem" mt="2rem" w="100%" />
			</Text>
		</Paper>
	);
};

export default RevenueCardSkeleton;
