//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Group, Paper, ThemeIcon, Text } from "@mantine/core";
import { BsArrowDownRight, BsArrowUpRight } from "react-icons/bs";

import { useCurrencyFormatter, useNumberFormatter } from "../../../../utils/converters";
import { type RevenueCardProps } from "../dashboard";

// https://ui.mantine.dev/category/stats

const RevenueCard = ({ title, revenue, percentage }: RevenueCardProps) => {
	const currencyFormatter = useCurrencyFormatter();
	const numberFormatter = useNumberFormatter(2);

	const DiffIcon = percentage > 0 ? BsArrowUpRight : BsArrowDownRight;

	return (
		<Paper p="xl" radius="md" withBorder>
			<Group position="apart">
				<div>
					<Text color="dimmed" size="xs" transform="uppercase" weight={700}>
						{title}
					</Text>
					<Text size="xl" weight={700}>
						{currencyFormatter.format(revenue)}
					</Text>
				</div>
				<ThemeIcon
					color="gray"
					radius="md"
					size={38}
					sx={(theme) => {
						return { color: percentage > 0 ? theme.colors.teal[6] : theme.colors.red[6] };
					}}
					variant="light"
				>
					<DiffIcon size={22} />
				</ThemeIcon>
			</Group>
			<Text color="dimmed" mt="md" size="sm">
				<Text color={percentage > 0 ? "teal" : "red"} component="span" weight={700}>
					{numberFormatter.format(percentage)}%&nbsp;
				</Text>
				{percentage > 0 ? "increase" : "decrease"} compared to last month
			</Text>
		</Paper>
	);
};

export default RevenueCard;
