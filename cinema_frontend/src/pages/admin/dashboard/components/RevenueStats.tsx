//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { SimpleGrid } from "@mantine/core";
import { observer } from "mobx-react-lite";

import Heading from "../../../../components/display/Heading";
import useApiStore from "../../../../hooks/useApiStore";

import RevenueCard from "./RevenueCard";
import RevenueCardSkeleton from "./RevenueCardSkeleton";

const calculateRevenuePercentage = (current: number, previous: number): number => {
	if (current === 0) return 0;
	if (previous === 0) return 100;
	return ((current - previous) / previous) * 100;
};

const RevenueStats = () => {
	const { dashboardStore } = useApiStore();

	const loading = dashboardStore.loading.revenue;

	return (
		<div id="revenue">
			<Heading anchor="#revenue" className="mb-4" size={2}>
				Revenue
			</Heading>

			{loading && (
				<SimpleGrid breakpoints={[{ maxWidth: "sm", cols: 1 }]} cols={3} mt="0.5rem">
					<RevenueCardSkeleton />
					<RevenueCardSkeleton />
					<RevenueCardSkeleton />
				</SimpleGrid>
			)}

			{!loading && dashboardStore.revenueStats && (
				<SimpleGrid breakpoints={[{ maxWidth: "sm", cols: 1 }]} cols={3} mt="0.5rem">
					<RevenueCard
						percentage={calculateRevenuePercentage(
							dashboardStore.revenueStats.food.currentMonth.total,
							dashboardStore.revenueStats.food.previousMonth.total
						)}
						revenue={dashboardStore.revenueStats.food.currentMonth.total}
						title="Food revenue"
					/>
					<RevenueCard
						percentage={calculateRevenuePercentage(
							dashboardStore.revenueStats.souvenir.currentMonth.total,
							dashboardStore.revenueStats.souvenir.previousMonth.total
						)}
						revenue={dashboardStore.revenueStats.souvenir.currentMonth.total}
						title="Souvenir revenue"
					/>
					<RevenueCard
						percentage={calculateRevenuePercentage(
							dashboardStore.revenueStats.reservation.currentMonth.total,
							dashboardStore.revenueStats.reservation.previousMonth.total
						)}
						revenue={dashboardStore.revenueStats.reservation.currentMonth.total}
						title="Reservation revenue"
					/>
				</SimpleGrid>
			)}
		</div>
	);
};

export default observer(RevenueStats);
