//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Grid } from "@mantine/core";
import { observer } from "mobx-react-lite";

import Heading from "../../../../components/display/Heading";
import useApiStore from "../../../../hooks/useApiStore";
import UserEmailsCard from "../stats/UserEmailsCard";
import UserReservationsCard from "../stats/UserReservationsCard";

import PercentageCardSkeleton from "./PercentageCardSkeleton";

const UserStats = () => {
	const { dashboardStore } = useApiStore();

	const loading = dashboardStore.loading.users;

	return (
		<div id="users">
			<Heading anchor="#users" className="mb-4" size={2}>
				Users
			</Heading>

			<Grid>
				<Grid.Col sm={6} span={12}>
					{loading && <PercentageCardSkeleton />}
					{!loading && dashboardStore.userStats && <UserEmailsCard userStats={dashboardStore.userStats} />}
				</Grid.Col>
				<Grid.Col sm={6} span={12}>
					{loading && <PercentageCardSkeleton />}
					{!loading && dashboardStore.userStats && (
						<UserReservationsCard userStats={dashboardStore.userStats} />
					)}
				</Grid.Col>
			</Grid>
		</div>
	);
};

export default observer(UserStats);
