//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Container, Stack } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";

import ServerError from "../../../components/status/ServerError";
import useApiStore from "../../../hooks/useApiStore";
import useNavbarStore from "../../../hooks/useNavbarStore";
import { Routes } from "../../../routes/Routes";
import { is5xxStatus } from "../../../utils/isStatus";

import MovieStats from "./components/MovieStats";
import RevenueStats from "./components/RevenueStats";
import UserStats from "./components/UserStats";

const Admin = () => {
	const { dashboardStore } = useApiStore();
	const navbarStore = useNavbarStore();

	useEffect(() => {
		document.title = "Dashboard | Admin";
		navbarStore.setActive(Routes.AdminDashboard);
	}, [navbarStore]);

	useEffect(() => {
		void dashboardStore.getUserStats();
		void dashboardStore.getMovieStats();
		void dashboardStore.getRevenueStats();
	}, [dashboardStore]);

	if (
		is5xxStatus(dashboardStore.status.users) ||
		is5xxStatus(dashboardStore.status.movies) ||
		is5xxStatus(dashboardStore.status.revenue)
	)
		return <ServerError />;

	return (
		<Container size="xl">
			<Stack spacing="xl">
				<MovieStats />
				<RevenueStats />
				<UserStats />
			</Stack>
		</Container>
	);
};

export default observer(Admin);
