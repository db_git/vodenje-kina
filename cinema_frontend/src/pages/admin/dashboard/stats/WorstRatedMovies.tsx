//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { observer } from "mobx-react-lite";

import Heading from "../../../../components/display/Heading";
import HorizontalScroll from "../../../../components/display/HorizontalScroll";
import useApiStore from "../../../../hooks/useApiStore";
import AlternateMovieCard from "../../../movies/components/album/AlternateMovieCard";
import AlternateMovieCardSkeleton from "../../../movies/components/album/AlternateMovieCardSkeleton";

const WorstRatedMovies = () => {
	const { dashboardStore } = useApiStore();
	const loading = dashboardStore.loading.movies;

	return (
		<>
			<Heading icon={false} size={3}>
				Worst rated
			</Heading>
			<HorizontalScroll slidesToScroll={2} withControls>
				{loading && (
					<>
						{[...Array.from({ length: 6 }).keys()].map((number) => {
							return (
								<HorizontalScroll.Item key={number}>
									<AlternateMovieCardSkeleton />
								</HorizontalScroll.Item>
							);
						})}
					</>
				)}
				{!loading && dashboardStore.movieStats && (
					<>
						{dashboardStore.movieStats.worstRatedMovies.map((movie) => {
							return (
								<HorizontalScroll.Item key={movie.id}>
									<AlternateMovieCard movie={movie} withRating />
								</HorizontalScroll.Item>
							);
						})}
					</>
				)}
			</HorizontalScroll>
		</>
	);
};

export default observer(WorstRatedMovies);
