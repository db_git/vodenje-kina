//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Text } from "@mantine/core";
import { observer } from "mobx-react-lite";

import PercentageCard from "../components/PercentageCard";
import { type UserStatsProps } from "../dashboard";

const UserEmailsCard = ({ userStats }: UserStatsProps) => {
	const confirmedEmailUsersPercentage = (userStats.confirmedEmailUsers / userStats.totalUsers) * 100;

	return (
		<PercentageCard percentage={confirmedEmailUsersPercentage}>
			<Text fw={500}>
				Total users:&nbsp;
				<Text fw={700} inline span>
					{userStats.totalUsers}
				</Text>
			</Text>
			<Text fw={500}>
				Users with confirmed e-mails:&nbsp;
				<Text fw={700} inline span>
					{userStats.confirmedEmailUsers}
				</Text>
			</Text>
			<Text fw={500}>
				Users with unconfirmed e-mails:&nbsp;
				<Text fw={700} inline span>
					{userStats.unconfirmedEmailUsers}
				</Text>
			</Text>
		</PercentageCard>
	);
};

export default observer(UserEmailsCard);
