//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { ColorSwatch, Grid, Group, Rating, RingProgress, Skeleton, Stack, Text, useMantineTheme } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback, useState } from "react";

import Heading from "../../../../components/display/Heading";
import useApiStore from "../../../../hooks/useApiStore";
import { useNumberFormatter } from "../../../../utils/converters";

const MovieRatings = () => {
	const { dashboardStore } = useApiStore();
	const [hovered, setHovered] = useState(-1);
	const theme = useMantineTheme();
	const numberFormatter = useNumberFormatter(2);

	const resetHovered = useCallback(() => {
		setHovered(-1);
	}, []);

	const totalRatings =
		dashboardStore.movieStats?.ratingStars
			.map((rating) => {
				return rating.count;
			})
			.reduce((count1, count2) => {
				return count1 + count2;
			}) ?? 1;

	const loading = dashboardStore.loading.movies;
	const ringColors = [
		theme.colors.yellow[5],
		theme.colors.red[8],
		theme.colors.green[7],
		theme.colors.orange[7],
		theme.colors.indigo[8],
		theme.colors.cyan[8]
	];

	const ringLabel = (
		<Text align="center" size="xl" weight={700}>
			{hovered !== -1 && numberFormatter.format((hovered / totalRatings) * 100) + "%"}
		</Text>
	);

	return (
		<>
			<Heading icon={false} size={3}>
				Individual ratings
			</Heading>

			{loading && (
				<Grid mb="1.5rem">
					<Grid.Col sm={5} span={12}>
						<Stack align="center" className="h-full" justify="center" spacing="xs">
							<Skeleton animate className="rounded-full" h={250} w={250} />
						</Stack>
					</Grid.Col>

					<Grid.Col sm={7} span={12}>
						<Stack align="flex-start" className="h-full" justify="center" spacing="xs">
							{[...Array.from({ length: 6 }).keys()].map((number) => {
								return (
									<Group key={number}>
										<Skeleton animate className="rounded-full" h={27} w={27} />
										<Skeleton animate className="rounded-full" h="1rem" w="4rem" />
										<Rating count={5} defaultValue={0} readOnly size="lg" />
										<Skeleton animate className="rounded-full" h="1rem" w="3rem" />
									</Group>
								);
							})}
						</Stack>
					</Grid.Col>
				</Grid>
			)}

			{!loading && dashboardStore.movieStats && (
				<Grid mb="1.5rem">
					<Grid.Col sm={5} span={12}>
						<Stack align="center" className="h-full" justify="center" spacing="xs">
							<RingProgress
								label={ringLabel}
								onMouseLeave={resetHovered}
								sections={dashboardStore.movieStats.ratingStars.map((rating) => {
									return {
										value: (rating.count / totalRatings) * 100,
										color: ringColors[rating.star],
										onMouseEnter: () => {
											runInAction(() => {
												setHovered(rating.count);
											});
										},
										onMouseLeave: resetHovered,
										tooltip: `${rating.star} star`
									};
								})}
								size={300}
								thickness={25}
							/>
						</Stack>
					</Grid.Col>

					<Grid.Col sm={7} span={12}>
						<Stack align="flex-start" className="h-full" justify="center" spacing="xs">
							{dashboardStore.movieStats.ratingStars.map((rating) => {
								return (
									<Group key={JSON.stringify(rating)}>
										<ColorSwatch color={ringColors[rating.star]} />
										<Text fw={500} size="lg">
											{rating.star} star
										</Text>
										<Rating count={5} defaultValue={rating.star} readOnly size="lg" />
										<Text fw={700} size="lg">
											{numberFormatter.format((rating.count / totalRatings) * 100)} %
										</Text>
									</Group>
								);
							})}
						</Stack>
					</Grid.Col>
				</Grid>
			)}
		</>
	);
};

export default observer(MovieRatings);
