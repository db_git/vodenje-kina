//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type UseFormProps } from "../../../components/forms/forms";

export type PersonFormInputValues = {
	name: string;
	dateOfBirth: Date | null;
	dateOfDeath: Date | null;
	imageUrl: Blob | string;
	biography: string;
};

export type PersonFormSubmitValues = Omit<PersonFormInputValues, "dateOfBirth" | "dateOfDeath" | "imageUrl"> & {
	dateOfBirth: string;
	dateOfDeath?: string;
	imageUrl: string;
};

export type PersonNameInputProps = UseFormProps<PersonFormInputValues>;
export type PersonBirthInputProps = UseFormProps<PersonFormInputValues>;
export type PersonDeathInputProps = UseFormProps<PersonFormInputValues>;
export type PersonBiographyInputProps = UseFormProps<PersonFormInputValues>;
export type PersonImageInputProps = UseFormProps<PersonFormInputValues>;
