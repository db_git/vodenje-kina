//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Textarea } from "@mantine/core";
import { observer } from "mobx-react-lite";

import useApiStore from "../../../../hooks/useApiStore";
import { type PersonBiographyInputProps } from "../person";

const PersonBiographyInput = ({ form }: PersonBiographyInputProps) => {
	const { personStore } = useApiStore();

	return (
		<Textarea
			autosize
			disabled={personStore.loading.get || personStore.loading.post || personStore.loading.put}
			label="Biography"
			maxRows={16}
			minRows={4}
			placeholder="Enter persons biography"
			required
			{...form.getInputProps("biography")}
		/>
	);
};

export default observer(PersonBiographyInput);
