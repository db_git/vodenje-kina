//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { DatePicker } from "@mantine/dates";
import { observer } from "mobx-react-lite";
import { BsCalendarDate } from "react-icons/bs";

import useApiStore from "../../../../hooks/useApiStore";
import { type PersonDeathInputProps } from "../person";

const PersonDeathInput = ({ form }: PersonDeathInputProps) => {
	const { personStore } = useApiStore();

	return (
		<DatePicker
			disabled={personStore.loading.get || personStore.loading.post || personStore.loading.put}
			dropdownType="modal"
			icon={<BsCalendarDate />}
			label="Death"
			placeholder="Enter persons date of death"
			{...form.getInputProps("dateOfDeath")}
		/>
	);
};

export default observer(PersonDeathInput);
