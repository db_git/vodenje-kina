//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Grid } from "@mantine/core";
import { observer } from "mobx-react-lite";

import Form from "../../../../components/forms/Form";
import Loading from "../../../../components/status/Loading";
import useApiStore from "../../../../hooks/useApiStore";
import { personFormSchema } from "../../../../schemas/adminSchemas";
import { blobToBase64 } from "../../../../utils/converters";
import { type AdminFormProps } from "../../admin";
import { type PersonFormInputValues, type PersonFormSubmitValues } from "../person";

import PersonBiographyInput from "./PersonBiographyInput";
import PersonBirthInput from "./PersonBirthInput";
import PersonDeathInput from "./PersonDeathInput";
import PersonImageInput from "./PersonImageInput";
import PersonNameInput from "./PersonNameInput";

const PersonForm = ({ id }: AdminFormProps) => {
	const { personStore } = useApiStore();

	const handleSubmit = async (values: PersonFormInputValues): Promise<void> => {
		const newValues: PersonFormSubmitValues = {
			name: values.name,
			imageUrl: values.imageUrl instanceof Blob ? await blobToBase64(values.imageUrl) : values.imageUrl,
			biography: values.biography,
			dateOfBirth: new Date(
				(values.dateOfBirth as Date).getTime() - (values.dateOfBirth as Date).getTimezoneOffset() * 60 * 1_000
			)
				.toISOString()
				.slice(0, -14),
			dateOfDeath: values.dateOfDeath
				? new Date(values.dateOfDeath.getTime() - values.dateOfDeath.getTimezoneOffset() * 60 * 1_000)
						.toISOString()
						.slice(0, -14)
				: undefined
		};

		if (id) {
			await personStore.put(id, newValues);
		} else {
			await personStore.post(newValues);
		}
	};

	if (personStore.loading.get || (id && !personStore.response)) return <Loading />;

	const initialValues: PersonFormInputValues = {
		name: personStore.response?.name ?? "",
		dateOfBirth: personStore.response?.dateOfBirth ? new Date(personStore.response?.dateOfBirth) : null,
		dateOfDeath: personStore.response?.dateOfDeath ? new Date(personStore.response?.dateOfDeath) : null,
		imageUrl: personStore.response?.imageUrl ?? "",
		biography: personStore.response?.biography ?? ""
	};

	return (
		<Form id="personForm" initialValues={initialValues} onSubmit={handleSubmit} schema={personFormSchema}>
			{(form) => {
				return (
					<Grid>
						<Grid.Col span={12}>
							<PersonNameInput form={form} />
						</Grid.Col>

						<Grid.Col md={6} xs={12}>
							<PersonBirthInput form={form} />
						</Grid.Col>
						<Grid.Col md={6} xs={12}>
							<PersonDeathInput form={form} />
						</Grid.Col>

						<Grid.Col span={12}>
							<PersonBiographyInput form={form} />
						</Grid.Col>

						<Grid.Col span={12}>
							<PersonImageInput form={form} />
						</Grid.Col>
					</Grid>
				);
			}}
		</Form>
	);
};

export default observer(PersonForm);
