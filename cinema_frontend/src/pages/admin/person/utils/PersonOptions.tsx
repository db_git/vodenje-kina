//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Menu, Text, ThemeIcon } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { GiDeathSkull } from "react-icons/all";
import { BiCameraMovie } from "react-icons/bi";
import { FaBirthdayCake } from "react-icons/fa";
import { MdImage, MdTitle } from "react-icons/md";

import useApiStore from "../../../../hooks/useApiStore";
import DisplayColumns from "../../DisplayColumns";

const PersonOptions = () => {
	const { personStore } = useApiStore();

	const pictureColor = personStore.showPicture ? "green" : "red";
	const biographyColor = personStore.showBiography ? "green" : "red";
	const birthColor = personStore.showDateOfBirth ? "green" : "red";
	const deathColor = personStore.showDateOfDeath ? "green" : "red";
	const moviesColor = personStore.showMoviesCount ? "green" : "red";

	return (
		<Menu closeOnItemClick={false} position="bottom" withArrow>
			<Menu.Target>
				<DisplayColumns />
			</Menu.Target>

			<Menu.Dropdown>
				<Menu.Item
					icon={
						<ThemeIcon color={pictureColor} variant="light">
							<MdImage />
						</ThemeIcon>
					}
					onClick={() => {
						personStore.toggleShowPicture();
					}}
				>
					<Text color={pictureColor}>Picture</Text>
				</Menu.Item>

				<Menu.Item
					icon={
						<ThemeIcon color={biographyColor} variant="light">
							<MdTitle />
						</ThemeIcon>
					}
					onClick={() => {
						personStore.toggleShowBiography();
					}}
				>
					<Text color={biographyColor}>Biography</Text>
				</Menu.Item>

				<Menu.Item
					icon={
						<ThemeIcon color={birthColor} variant="light">
							<FaBirthdayCake />
						</ThemeIcon>
					}
					onClick={() => {
						personStore.toggleShowDateOfBirth();
					}}
				>
					<Text color={birthColor}>Birth</Text>
				</Menu.Item>

				<Menu.Item
					icon={
						<ThemeIcon color={deathColor} variant="light">
							<GiDeathSkull />
						</ThemeIcon>
					}
					onClick={() => {
						personStore.toggleShowDateOfDeath();
					}}
				>
					<Text color={deathColor}>Death</Text>
				</Menu.Item>

				<Menu.Item
					icon={
						<ThemeIcon color={moviesColor} variant="light">
							<BiCameraMovie />
						</ThemeIcon>
					}
					onClick={() => {
						personStore.toggleShowMoviesCount();
					}}
				>
					<Text color={moviesColor}>Movies count</Text>
				</Menu.Item>
			</Menu.Dropdown>
		</Menu>
	);
};

export default observer(PersonOptions);
