//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Center, Spoiler, Text } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { type ReactNode, useState } from "react";

import useApiStore from "../../../../hooks/useApiStore";
import { Routes } from "../../../../routes/Routes";
import { type Person } from "../../../../types/entities";
import DeleteModal from "../../DeleteModal";
import TableActionIcons from "../../utils/TableActionIcons";
import TableImage from "../../utils/TableImage";

const PersonTableBody = () => {
	const { personStore } = useApiStore();
	const [selectedPerson, setSelectedPerson] = useState<Person>();
	const [notificationSuccessMessage, setNotificationSuccessMessage] = useState<ReactNode>();
	const [deleteModalOpen, setDeleteModalOpen] = useState(false);

	const modalMessage = (
		<>
			{`Are you sure you want to remove ${selectedPerson?.name}?`}
			<Center mt="1rem">
				<TableImage height="15rem" radius={64} src={selectedPerson?.imageUrl ?? ""} width="12rem" withBorder />
			</Center>
		</>
	);

	return (
		<>
			<DeleteModal
				id={selectedPerson?.id ?? ""}
				isOpen={deleteModalOpen}
				message={modalMessage}
				notificationSuccessMessage={notificationSuccessMessage}
				store={personStore}
				toggle={() => {
					setDeleteModalOpen(!deleteModalOpen);
				}}
			/>

			{personStore.responses?.data.map((person) => {
				return (
					<tr key={person.id}>
						{personStore.showPicture && (
							<td>
								<TableImage height="5rem" radius={64} src={person.imageUrl} width="5rem" withBorder />
							</td>
						)}
						<td>
							<Text pr="1rem" weight={500}>
								{person.name}
							</Text>
						</td>
						{personStore.showBiography && (
							<td className="max-w-xs">
								<Spoiler hideLabel="Hide" maxHeight={45} showLabel="Show more">
									<Text>{person.biography}</Text>
								</Spoiler>
							</td>
						)}
						{personStore.showDateOfBirth && (
							<td>
								<Text>{person.dateOfBirth}</Text>
							</td>
						)}
						{personStore.showDateOfDeath && (
							<td>
								<Text>{person.dateOfDeath}</Text>
							</td>
						)}
						{personStore.showMoviesCount && (
							<td>
								<Text>{person.movies.length}</Text>
							</td>
						)}
						<TableActionIcons
							location={`${Routes.AdminPersonEdit}/${person.id}`}
							onDeleteClick={() => {
								runInAction(() => {
									setSelectedPerson(person);
									setNotificationSuccessMessage(`'${person.name}' deleted successfully.`);
								});
								setDeleteModalOpen(!deleteModalOpen);
							}}
						/>
					</tr>
				);
			})}
		</>
	);
};

export default observer(PersonTableBody);
