//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Avatar, Center, Spoiler, Text } from "@mantine/core";
import dayjs from "dayjs";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback, useState } from "react";

import useApiStore from "../../../../hooks/useApiStore";
import { type IApiStore } from "../../../../stores/stores";
import { type UserComment } from "../../../../types/entities";
import { type QueryParameters } from "../../../../types/query";
import ReviewCard from "../../../movieDetails/components/reviews/ReviewCard";
import { type UserCommentFormSubmitValues } from "../../../movieDetails/movieDetails";
import DeleteModal from "../../DeleteModal";
import DeleteIcon from "../../utils/DeleteIcon";

const UserTableBody = () => {
	const { userCommentStore } = useApiStore();
	const [selectedReview, setSelectedReview] = useState<UserComment>();
	const [deleteModalOpen, setDeleteModalOpen] = useState(false);

	const deleteReview = useCallback(async () => {
		await runInAction(async () => {
			if (!selectedReview) return;

			await userCommentStore.deleteComment(selectedReview.movie.id, selectedReview.user.id, false);
			await userCommentStore.getAll();
		});
	}, [selectedReview, userCommentStore]);

	const toggleModal = useCallback(() => {
		setDeleteModalOpen(!deleteModalOpen);
	}, [deleteModalOpen]);

	const modalMessage = (
		<>
			Are you sure you want to remove this comment?
			{selectedReview && (
				<Center>
					<ReviewCard review={selectedReview} withMenu={false} />
				</Center>
			)}
		</>
	);

	return (
		<>
			<DeleteModal
				customDelete={deleteReview}
				fontWeight={400}
				id={selectedReview?.movie.id ?? ""}
				isOpen={deleteModalOpen}
				message={modalMessage}
				modalSize="75%"
				notificationSuccessMessage="Comment deleted successfully."
				store={
					userCommentStore as unknown as IApiStore<
						UserComment,
						QueryParameters,
						UserComment,
						UserCommentFormSubmitValues,
						Record<string, unknown>
					>
				}
				toggle={toggleModal}
			/>

			{userCommentStore.responses?.data.map((comment) => {
				return (
					<tr key={`${comment.user.id}${comment.user.name}${comment.movie.id}${comment.movie.title}`}>
						{userCommentStore.showImage && (
							<td>
								<Avatar radius="xl" src={comment.user.imageUrl} />
							</td>
						)}
						<td>
							<Text pr="1rem" weight={500}>
								{comment.user.name}
							</Text>
						</td>
						<td>
							<Text pr="1rem" weight={500}>
								{comment.movie.title}
							</Text>
						</td>
						<td>
							<Spoiler hideLabel="Hide" maxHeight={45} showLabel="Show more">
								<Text pr="2rem">{comment.comment}</Text>
							</Spoiler>
						</td>
						<td>
							<Text pr="1rem" weight={500}>
								{dayjs(comment.date).format("ll")}
							</Text>
						</td>
						<td>
							<DeleteIcon
								onDeleteClick={() => {
									runInAction(() => {
										setSelectedReview(comment);
									});

									toggleModal();
								}}
							/>
						</td>
					</tr>
				);
			})}
		</>
	);
};

export default observer(UserTableBody);
