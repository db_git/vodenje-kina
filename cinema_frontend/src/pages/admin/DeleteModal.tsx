//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Center, Modal, Stack, Text, useMantineTheme } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { useCallback } from "react";
import { MdErrorOutline } from "react-icons/md";

import { HttpMethod } from "../../types/enums/Http";
import { type QueryParameters } from "../../types/query";
import { showDeletedNotification } from "../../utils/notifications";

import AdminStyle from "./Admin.module.scss";
import { type DeleteModalProps } from "./admin";

const DeleteModal = <
	TDto,
	TQuery extends QueryParameters,
	TIndividualDto = TDto,
	TCreateDto = Record<string, unknown>,
	TUpdateDto = Record<string, unknown>
>({
	id,
	isOpen,
	toggle,
	message,
	notificationSuccessMessage,
	store,
	onSuccess,
	customDelete,
	fontWeight,
	modalSize
}: DeleteModalProps<TDto, TQuery, TIndividualDto, TCreateDto, TUpdateDto>) => {
	const theme = useMantineTheme();
	const deleteModalIcon = AdminStyle[`delete-modal-icon-${theme.colorScheme}`];

	const handleOnToggle = useCallback((): void => {
		store.setStatus(HttpMethod.Delete, 0);
		toggle();
	}, [store, toggle]);

	const handleDelete = useCallback(async () => {
		if (customDelete) await customDelete();
		else await store.delete(id);

		if (onSuccess) onSuccess();
		showDeletedNotification(notificationSuccessMessage);
		handleOnToggle();
	}, [customDelete, handleOnToggle, id, notificationSuccessMessage, onSuccess, store]);

	const onClick = useCallback(async () => {
		await handleDelete();
	}, [handleDelete]);

	return (
		<Modal
			centered
			onClose={handleOnToggle}
			opened={isOpen}
			overlayBlur={3}
			overlayOpacity={0.55}
			radius="lg"
			size={modalSize}
			withCloseButton={false}
		>
			<Stack p="1rem">
				<Center>
					<span className={deleteModalIcon}>
						<MdErrorOutline size={50} />
					</span>
				</Center>

				<Text align="center" component="div" mb="2rem" mt="0.5rem" size="lg" weight={fontWeight}>
					{message}
				</Text>

				<Button.Group>
					<Button color="red" fullWidth loading={store.loading.delete} onClick={onClick}>
						Delete
					</Button>
					<Button fullWidth onClick={handleOnToggle} variant="light">
						Cancel
					</Button>
				</Button.Group>
			</Stack>
		</Modal>
	);
};

DeleteModal.defaultProps = {
	fontWeight: 700,
	message: "Are you sure you want to delete the selected entry?",
	modalSize: "lg"
};

export default observer(DeleteModal);
