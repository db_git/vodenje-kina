//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Image, Skeleton, useMantineTheme } from "@mantine/core";
import { memo } from "react";

import { type TableImageProps } from "../admin";

const TableImage = ({ src, width, height, radius, withBorder }: TableImageProps) => {
	const theme = useMantineTheme();

	const borderColor = theme.colorScheme === "dark" ? theme.colors.dark[4] : theme.colors.gray[3];
	const imageStyle = {
		image: {
			border: withBorder ? `2px solid ${borderColor}` : undefined
		}
	};

	return (
		<Image
			height={height}
			placeholder={<Skeleton height={height} radius={radius} width={width} />}
			radius={radius}
			src={src}
			styles={imageStyle}
			width={width}
			withPlaceholder
		/>
	);
};

export default memo(TableImage);
