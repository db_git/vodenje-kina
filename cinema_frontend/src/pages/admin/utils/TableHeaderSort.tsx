//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { SimpleGrid, Text, useMantineColorScheme } from "@mantine/core";
import { memo } from "react";
import { BiSort, BiSortAZ, BiSortZA } from "react-icons/bi";

import AdminStyle from "../Admin.module.scss";
import { type TableHeaderSortProps } from "../admin";

const iconSize = 19;
const theadIconStyle = AdminStyle[`thead-icon`];

const TableHeaderSort = ({
	onClick,
	ascending,
	descending,
	ascendingIcon,
	descendingIcon,
	unsortedIcon,
	children
}: TableHeaderSortProps) => {
	const { colorScheme } = useMantineColorScheme();

	let icon = unsortedIcon;
	if (ascending) icon = ascendingIcon;
	else if (descending) icon = descendingIcon;

	const theadStyle = AdminStyle[`thead-${colorScheme}`];

	return (
		<th className={theadStyle} onClick={onClick}>
			<SimpleGrid cols={2}>
				<Text transform="uppercase">{children}</Text>
				<div className={theadIconStyle}>{icon}</div>
			</SimpleGrid>
		</th>
	);
};

TableHeaderSort.defaultProps = {
	ascendingIcon: <BiSortAZ size={iconSize} />,
	descendingIcon: <BiSortZA size={iconSize} />,
	unsortedIcon: <BiSort size={iconSize} />
};

export default memo(TableHeaderSort);
