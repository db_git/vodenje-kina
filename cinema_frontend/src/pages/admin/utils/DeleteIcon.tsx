//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { useMantineTheme } from "@mantine/core";
import { memo } from "react";
import { AiOutlineDelete } from "react-icons/ai";

import AdminStyle from "../Admin.module.scss";
import { type DeleteIconProps } from "../admin";

const DeleteIcon = ({ onDeleteClick }: DeleteIconProps) => {
	const theme = useMantineTheme();
	const deleteIconStyle = AdminStyle[`action-icon-delete-${theme.colorScheme}`];

	return <AiOutlineDelete className={deleteIconStyle} onClick={onDeleteClick} />;
};

export default memo(DeleteIcon);
