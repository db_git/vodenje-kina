//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type MantineSize } from "@mantine/core";
import { type MantineNumberSize } from "@mantine/styles/lib/theme/types/MantineSize";
import { type ReactNode } from "react";
import { type NavigateFunction } from "react-router-dom";

import { type IApiStore } from "../../stores/stores";
import { type QueryParameters } from "../../types/query";
import { type ChildrenProps, type OptionalChildrenProps, type OptionalIdProps } from "../../types/shared";

export type AdminEntityBaseProps<
	TDto,
	TQuery extends QueryParameters,
	TIndividualDto = TDto,
	TCreateDto = Record<string, unknown>,
	TUpdateDto = Record<string, unknown>
> = OptionalChildrenProps & {
	readonly store: IApiStore<TDto, TQuery, TIndividualDto, TCreateDto, TUpdateDto>;
	readonly location: string;
	readonly thead: JSX.Element;
	readonly tbody: JSX.Element;
	readonly withCreateButton: boolean;
};

export type CreateAdminEntityBaseProps<T> = ChildrenProps & {
	readonly store: IApiStore<T, QueryParameters>;
	readonly formId: string;
	readonly containerSize?: MantineNumberSize;
};

export type EditAdminEntityBaseProps<T> = CreateAdminEntityBaseProps<T> & {
	readonly noContentMessage: string;
};

export type AdminFormProps = OptionalIdProps;

export type CreateButtonProps = {
	readonly className?: string;
	readonly center?: boolean;
	readonly fullWidth?: boolean;
	readonly size?: MantineSize;
	readonly location: string;
	readonly mt?: MantineNumberSize | string;
};

export type TableHeaderSortProps = ChildrenProps & {
	readonly ascending: boolean;
	readonly descending: boolean;
	readonly ascendingIcon: ReactNode;
	readonly descendingIcon: ReactNode;
	readonly unsortedIcon: ReactNode;
	readonly onClick: () => void;
};

export type DeleteIconProps = {
	readonly onDeleteClick: () => void;
};

export type EditIconProps = {
	readonly location: string;
};

export type TableActionIconsProps = DeleteIconProps & EditIconProps;

export type TableImageProps = {
	readonly src: string;
	readonly radius: MantineNumberSize;
	readonly width: number | string | undefined;
	readonly height: number | string | undefined;
	readonly withBorder?: boolean;
};

export type DeleteModalProps<
	TDto,
	TQuery extends QueryParameters,
	TIndividualDto = TDto,
	TCreateDto = Record<string, unknown>,
	TUpdateDto = Record<string, unknown>
> = {
	readonly id: string;
	readonly isOpen: boolean;
	readonly toggle: () => void;
	readonly message: ReactNode;
	readonly notificationSuccessMessage: ReactNode;
	readonly store: IApiStore<TDto, TQuery, TIndividualDto, TCreateDto, TUpdateDto>;
	readonly onSuccess?: () => void;
	readonly customDelete?: () => Promise<void>;
	readonly fontWeight?: number;
	readonly modalSize?:
		| MantineNumberSize
		| "10%"
		| "20%"
		| "25%"
		| "30%"
		| "40%"
		| "50%"
		| "60%"
		| "70%"
		| "75%"
		| "80%"
		| "90%"
		| "100%"
		| "auto";
};

type SearchProps<
	TDto,
	TQuery extends QueryParameters,
	TIndividualDto = TDto,
	TCreateDto = Record<string, unknown>,
	TUpdateDto = Record<string, unknown>
> = {
	readonly store: IApiStore<TDto, TQuery, TIndividualDto, TCreateDto, TUpdateDto>;
	readonly url?: NavigateFunction;
	readonly location?: string;
	readonly center?: boolean;
	readonly placeholder?: string;
};
