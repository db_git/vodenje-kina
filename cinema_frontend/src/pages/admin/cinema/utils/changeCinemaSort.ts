//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { runInAction } from "mobx";
import { type NavigateFunction } from "react-router-dom";

import { Routes } from "../../../../routes/Routes";
import { type ICinemaApiStore } from "../../../../stores/stores";
import { CinemaOrderBy } from "../../../../types/enums/OrderBy";
import { QueryParameterOptions } from "../../../../types/enums/Stores";

const changeCinemaNameSort = (cinemaStore: ICinemaApiStore, url: NavigateFunction): void => {
	runInAction(() => {
		if (
			cinemaStore.queryParameters.orderBy === CinemaOrderBy.NameAscending ||
			cinemaStore.queryParameters.orderBy === CinemaOrderBy.NameDescending
		) {
			cinemaStore.setQueryParameters(
				QueryParameterOptions.OrderBy,
				(cinemaStore.queryParameters.orderBy % CinemaOrderBy.NameDescending) + CinemaOrderBy.NameAscending
			);
		} else {
			cinemaStore.setQueryParameters(QueryParameterOptions.OrderBy, CinemaOrderBy.NameAscending);
		}

		cinemaStore.modifyUrl(Routes.AdminCinema, url);
		void cinemaStore.getAll();
	});
};

const changeCinemaCitySort = (cinemaStore: ICinemaApiStore, url: NavigateFunction): void => {
	runInAction(() => {
		if (
			cinemaStore.queryParameters.orderBy === CinemaOrderBy.CityAscending ||
			cinemaStore.queryParameters.orderBy === CinemaOrderBy.CityDescending
		) {
			cinemaStore.setQueryParameters(
				QueryParameterOptions.OrderBy,
				(cinemaStore.queryParameters.orderBy % (CinemaOrderBy.CityDescending / 2)) + CinemaOrderBy.CityAscending
			);
		} else {
			cinemaStore.setQueryParameters(QueryParameterOptions.OrderBy, CinemaOrderBy.CityAscending);
		}

		cinemaStore.modifyUrl(Routes.AdminCinema, url);
		void cinemaStore.getAll();
	});
};

const changeCinemaTicketPriceSort = (cinemaStore: ICinemaApiStore, url: NavigateFunction): void => {
	runInAction(() => {
		if (
			cinemaStore.queryParameters.orderBy === CinemaOrderBy.TicketPriceAscending ||
			cinemaStore.queryParameters.orderBy === CinemaOrderBy.TicketPriceDescending
		) {
			cinemaStore.setQueryParameters(
				QueryParameterOptions.OrderBy,
				(cinemaStore.queryParameters.orderBy % (CinemaOrderBy.TicketPriceDescending / 3)) +
					CinemaOrderBy.TicketPriceAscending
			);
		} else {
			cinemaStore.setQueryParameters(QueryParameterOptions.OrderBy, CinemaOrderBy.TicketPriceAscending);
		}

		cinemaStore.modifyUrl(Routes.AdminCinema, url);
		void cinemaStore.getAll();
	});
};

export { changeCinemaNameSort, changeCinemaCitySort, changeCinemaTicketPriceSort };
