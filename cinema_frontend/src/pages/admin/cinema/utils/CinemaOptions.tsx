//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Menu, Text, ThemeIcon } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { FaTheaterMasks } from "react-icons/fa";
import { GiModernCity } from "react-icons/gi";

import useApiStore from "../../../../hooks/useApiStore";
import DisplayColumns from "../../DisplayColumns";

const CinemaOptions = () => {
	const { cinemaStore } = useApiStore();

	const hallsColor = cinemaStore.showHalls ? "green" : "red";
	const addressColor = cinemaStore.showAddress ? "green" : "red";

	return (
		<Menu closeOnItemClick={false} position="bottom" withArrow>
			<Menu.Target>
				<DisplayColumns />
			</Menu.Target>

			<Menu.Dropdown>
				<Menu.Item
					icon={
						<ThemeIcon color={addressColor} variant="light">
							<GiModernCity />
						</ThemeIcon>
					}
					onClick={cinemaStore.toggleShowAddress}
				>
					<Text color={addressColor}>Address</Text>
				</Menu.Item>
				<Menu.Item
					icon={
						<ThemeIcon color={hallsColor} variant="light">
							<FaTheaterMasks />
						</ThemeIcon>
					}
					onClick={cinemaStore.toggleShowHalls}
				>
					<Text color={hallsColor}>Halls</Text>
				</Menu.Item>
			</Menu.Dropdown>
		</Menu>
	);
};

export default observer(CinemaOptions);
