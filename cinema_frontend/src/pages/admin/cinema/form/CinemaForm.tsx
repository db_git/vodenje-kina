//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Grid } from "@mantine/core";
import { observer } from "mobx-react-lite";

import Form from "../../../../components/forms/Form";
import Loading from "../../../../components/status/Loading";
import useApiStore from "../../../../hooks/useApiStore";
import { cinemaFormSchema } from "../../../../schemas/adminSchemas";
import { type AdminFormProps } from "../../admin";
import { type CinemaFormSubmitValues, type CinemaFormInputValues } from "../cinema";

import CinemaCityInput from "./CinemaCityInput";
import CinemaNameInput from "./CinemaNameInput";
import CinemaStreetInput from "./CinemaStreetInput";
import CinemaTicketPriceInput from "./CinemaTicketPriceInput";

const CinemaForm = ({ id }: AdminFormProps) => {
	const { cinemaStore } = useApiStore();

	const handleSubmit = (valus: CinemaFormSubmitValues): void => {
		if (id) {
			void cinemaStore.put(id, valus);
		} else {
			void cinemaStore.post(valus);
		}
	};

	if (cinemaStore.loading.get || (id && !cinemaStore.response)) return <Loading />;

	const initialValues: CinemaFormInputValues = {
		name: cinemaStore.response?.name ?? "",
		city: cinemaStore.response?.city ?? "",
		street: cinemaStore.response?.street ?? "",
		ticketPrice: cinemaStore.response?.ticketPrice ?? 0
	};

	return (
		<Form id="cinemaForm" initialValues={initialValues} onSubmit={handleSubmit} schema={cinemaFormSchema}>
			{(form) => {
				return (
					<Grid>
						<Grid.Col span={12}>
							<CinemaNameInput form={form} />
						</Grid.Col>

						<Grid.Col md={6} xs={12}>
							<CinemaCityInput form={form} />
						</Grid.Col>
						<Grid.Col md={6} xs={12}>
							<CinemaStreetInput form={form} />
						</Grid.Col>

						<Grid.Col span={12}>
							<CinemaTicketPriceInput form={form} />
						</Grid.Col>
					</Grid>
				);
			}}
		</Form>
	);
};

export default observer(CinemaForm);
