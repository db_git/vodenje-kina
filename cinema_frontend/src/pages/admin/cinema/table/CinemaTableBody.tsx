//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Text } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { type ReactNode, useState } from "react";

import useApiStore from "../../../../hooks/useApiStore";
import { Routes } from "../../../../routes/Routes";
import { useCurrencyFormatter } from "../../../../utils/converters";
import DeleteModal from "../../DeleteModal";
import TableActionIcons from "../../utils/TableActionIcons";

const CinemaTableBody = () => {
	const { cinemaStore } = useApiStore();
	const [id, setId] = useState("");
	const [modalMessage, setModalMessage] = useState("");
	const [notificationSuccessMessage, setNotificationSuccessMessage] = useState<ReactNode>();
	const [deleteModalOpen, setDeleteModalOpen] = useState(false);
	const currencyFormatter = useCurrencyFormatter();

	if (!cinemaStore.responses) return null;

	return (
		<>
			<DeleteModal
				id={id}
				isOpen={deleteModalOpen}
				message={modalMessage}
				notificationSuccessMessage={notificationSuccessMessage}
				store={cinemaStore}
				toggle={() => {
					setDeleteModalOpen(!deleteModalOpen);
				}}
			/>

			{cinemaStore.responses?.data.map((cinema) => {
				return (
					<tr key={cinema.id}>
						<td>
							<Text pr="1rem" weight={500}>
								{cinema.name}
							</Text>
						</td>
						{cinemaStore.showAddress && (
							<>
								<td>
									<Text pr="1rem">{cinema.city}</Text>
								</td>
								<td>
									<Text>{cinema.street}</Text>
								</td>
							</>
						)}
						<td>
							<Text pr="2rem">{currencyFormatter.format(cinema.ticketPrice)}</Text>
						</td>
						{cinemaStore.showHalls && cinema.halls && (
							<td>
								<Text>
									{cinema.halls
										.map((hall) => {
											return hall.name;
										})
										.join(", ")}
								</Text>
							</td>
						)}
						<TableActionIcons
							location={`${Routes.AdminCinemaEdit}/${cinema.id}`}
							onDeleteClick={() => {
								runInAction(() => {
									setId(cinema.id);
									setModalMessage(`Are you sure you want to delete cinema ${cinema.name}?`);
									setNotificationSuccessMessage(`Cinema '${cinema.name}' deleted successfully.`);
								});
								setDeleteModalOpen(!deleteModalOpen);
							}}
						/>
					</tr>
				);
			})}
		</>
	);
};

export default observer(CinemaTableBody);
