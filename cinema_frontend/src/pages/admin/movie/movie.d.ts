//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type UseFormProps } from "../../../components/forms/forms";
import { type MovieActor, type MovieDirector } from "../../../types/entities";

export type MovieFormInputValues = {
	title: string;
	year: number;
	duration: number;
	tagline: string;
	summary: string;
	inCinemas: boolean;
	posterUrl: Blob | string;
	backdropUrl: Blob | string;
	trailerUrl: string;
	filmRating: string;
	genres: string[];
	actors: MovieActor[];
	directors: MovieDirector[];
};

export type MovieFormSubmitValues = Omit<MovieFormInputValues, "actors" | "backdropUrl" | "directors" | "posterUrl"> & {
	actors: Array<{ id: string; character: string }>;
	directors: string[];
	posterUrl: string;
	backdropUrl: string;
};

export type MovieActorsInputProps = UseFormProps<MovieFormInputValues>;
export type MovieBackdropInputProps = UseFormProps<MovieFormInputValues>;
export type MovieDirectorsInputProps = UseFormProps<MovieFormInputValues>;
export type MovieDurationInputProps = UseFormProps<MovieFormInputValues>;
export type MovieFilmRatingInputProps = UseFormProps<MovieFormInputValues>;
export type MovieGenresInputProps = UseFormProps<MovieFormInputValues>;
export type MovieInCinemasInputProps = UseFormProps<MovieFormInputValues>;
export type MoviePosterInputProps = UseFormProps<MovieFormInputValues>;
export type MovieSummaryInputProps = UseFormProps<MovieFormInputValues>;
export type MovieTaglineInputProps = UseFormProps<MovieFormInputValues>;
export type MovieTitleInputProps = UseFormProps<MovieFormInputValues>;
export type MovieTrailerInputProps = UseFormProps<MovieFormInputValues>;
export type MovieYearInputProps = UseFormProps<MovieFormInputValues>;
