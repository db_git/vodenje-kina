//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import debounce from "lodash.debounce";
import { runInAction } from "mobx";
import { type Dispatch, type SetStateAction } from "react";

import { type IGenreGetStore, type IPersonGetStore } from "../../../../stores/stores";
import { QueryParameterOptions } from "../../../../types/enums/Stores";
import { type InputSelectOptions, type InputSelectOptionsWithPicture } from "../../../../types/forms";

const changeMovieGenreSearch = debounce(
	(store: IGenreGetStore, setGenres: Dispatch<SetStateAction<InputSelectOptions[]>>, search: string): void => {
		runInAction(() => {
			if (store.queryParameters.search === search) return;

			store.setQueryParameters(QueryParameterOptions.Search, search);
			void store.getAll();

			let genreOptions: InputSelectOptions[] = [];
			if (store.responses) {
				genreOptions = store.responses.data.map((genreResponse) => {
					return { value: genreResponse.id, label: genreResponse.name };
				});
			}

			setGenres(genreOptions);
		});
	},
	500
);

const changeMoviePersonSearch = debounce(
	(
		store: IPersonGetStore,
		setDirectors: Dispatch<SetStateAction<InputSelectOptionsWithPicture[]>>,
		search: string
	): void => {
		runInAction(() => {
			if (store.queryParameters.search === search) return;

			store.setQueryParameters(QueryParameterOptions.Search, search);
			void store.getAll();

			let peopleOptions: InputSelectOptionsWithPicture[] = [];
			if (store.responses) {
				peopleOptions = store.responses.data.map((person): InputSelectOptionsWithPicture => {
					return { value: person.id, label: person.name, pictureUrl: person.imageUrl };
				});
			}

			setDirectors(peopleOptions);
		});
	},
	500
);

export { changeMovieGenreSearch, changeMoviePersonSearch };
