//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Menu, Text, ThemeIcon } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { BiCameraMovie, BiText } from "react-icons/bi";
import { BsCalendar2Date, BsClock } from "react-icons/bs";
import { MdImage, MdTitle } from "react-icons/md";

import useApiStore from "../../../../hooks/useApiStore";
import DisplayColumns from "../../DisplayColumns";

const MovieOptions = () => {
	const { movieStore } = useApiStore();

	const pictureColor = movieStore.showPoster ? "green" : "red";
	const yearColor = movieStore.showYear ? "green" : "red";
	const durationColor = movieStore.showDuration ? "green" : "red";
	const taglineColor = movieStore.showTagline ? "green" : "red";
	const summaryColor = movieStore.showSummary ? "green" : "red";
	const inCinemasColor = movieStore.showInCinemas ? "green" : "red";
	const isFeaturedColor = movieStore.showIsFeatured ? "green" : "red";
	const isRecommendedColor = movieStore.showIsRecommended ? "green" : "red";

	return (
		<Menu closeOnItemClick={false} position="bottom" withArrow>
			<Menu.Target>
				<DisplayColumns />
			</Menu.Target>

			<Menu.Dropdown>
				<Menu.Item
					icon={
						<ThemeIcon color={pictureColor} variant="light">
							<MdImage />
						</ThemeIcon>
					}
					onClick={() => {
						movieStore.toggleShowPoster();
					}}
				>
					<Text color={pictureColor}>Picture</Text>
				</Menu.Item>

				<Menu.Item
					icon={
						<ThemeIcon color={yearColor} variant="light">
							<BsCalendar2Date />
						</ThemeIcon>
					}
					onClick={() => {
						movieStore.toggleShowYear();
					}}
				>
					<Text color={yearColor}>Year</Text>
				</Menu.Item>

				<Menu.Item
					icon={
						<ThemeIcon color={durationColor} variant="light">
							<BsClock />
						</ThemeIcon>
					}
					onClick={() => {
						movieStore.toggleShowDuration();
					}}
				>
					<Text color={durationColor}>Duration</Text>
				</Menu.Item>

				<Menu.Item
					icon={
						<ThemeIcon color={taglineColor} variant="light">
							<BiText />
						</ThemeIcon>
					}
					onClick={() => {
						movieStore.toggleShowTagline();
					}}
				>
					<Text color={taglineColor}>Tagline</Text>
				</Menu.Item>

				<Menu.Item
					icon={
						<ThemeIcon color={summaryColor} variant="light">
							<MdTitle />
						</ThemeIcon>
					}
					onClick={() => {
						movieStore.toggleShowSummary();
					}}
				>
					<Text color={summaryColor}>Summary</Text>
				</Menu.Item>

				<Menu.Item
					icon={
						<ThemeIcon color={inCinemasColor} variant="light">
							<BiCameraMovie />
						</ThemeIcon>
					}
					onClick={() => {
						movieStore.toggleShowInCinemas();
					}}
				>
					<Text color={inCinemasColor}>In cinemas</Text>
				</Menu.Item>

				<Menu.Item
					icon={
						<ThemeIcon color={isFeaturedColor} variant="light">
							<BiCameraMovie />
						</ThemeIcon>
					}
					onClick={() => {
						movieStore.toggleShowIsFeatured();
					}}
				>
					<Text color={isFeaturedColor}>Is featured</Text>
				</Menu.Item>

				<Menu.Item
					icon={
						<ThemeIcon color={isRecommendedColor} variant="light">
							<BiCameraMovie />
						</ThemeIcon>
					}
					onClick={() => {
						movieStore.toggleShowIsRecommended();
					}}
				>
					<Text color={isRecommendedColor}>Is recommended</Text>
				</Menu.Item>
			</Menu.Dropdown>
		</Menu>
	);
};

export default observer(MovieOptions);
