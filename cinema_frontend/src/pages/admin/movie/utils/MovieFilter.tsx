//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Modal, Stack, Text, useMantineTheme } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback, useEffect, useState } from "react";
import { MdFilterList } from "react-icons/md";
import { useLocation, useNavigate } from "react-router-dom";

import useApiStore from "../../../../hooks/useApiStore";
import { Routes } from "../../../../routes/Routes";
import { MovieQueryParameterOptions } from "../../../../types/enums/Stores";
import FilmRatingFilter from "../../../movies/components/filter/FilmRatingFilter";
import GenresFilter from "../../../movies/components/filter/GenresFilter";
import UpcomingFilter from "../../../movies/components/filter/UpcomingFilter";
import YearFilter from "../../../movies/components/filter/YearFilter";

const marginTop = "0.5rem";

const MovieFilter = () => {
	const url = useNavigate();
	const locationSearch = useLocation().search;
	const { movieStore } = useApiStore();
	const theme = useMantineTheme();
	const [opened, setOpened] = useState(false);

	useEffect(() => {
		runInAction(() => {
			movieStore.setUrlSearchString(new URLSearchParams(locationSearch));
			movieStore.modifyUrl(Routes.AdminMovie, url);
		});
	}, [url, movieStore, locationSearch]);

	const toggleModal = useCallback(() => {
		setOpened(!opened);
	}, [opened]);

	const applyFilters = useCallback(() => {
		runInAction(() => {
			movieStore.setMovieQueryParameters(MovieQueryParameterOptions.Page, 1);
			movieStore.modifyUrl(Routes.AdminMovie, url);
		});
		void movieStore.getAll();
	}, [movieStore, url]);

	return (
		<>
			<Modal
				centered
				onClose={toggleModal}
				opened={opened}
				overlayBlur={3}
				overlayColor={theme.colorScheme === "dark" ? theme.colors.dark[9] : theme.colors.gray[2]}
				overlayOpacity={0.55}
				title="Movie filter"
			>
				<Stack>
					<UpcomingFilter marginTop={marginTop} />
					<YearFilter marginTop={marginTop} />
					{opened && (
						<>
							<GenresFilter marginTop={marginTop} />
							<FilmRatingFilter marginTop={marginTop} />
						</>
					)}

					<Button
						color="cyan"
						loading={movieStore.loading.getAll}
						mt={marginTop}
						onClick={applyFilters}
						radius={6}
						size="md"
					>
						Search
					</Button>
				</Stack>
			</Modal>

			<Button onClick={toggleModal} radius="md" rightIcon={<MdFilterList />} variant="default">
				<Text>Filter</Text>
			</Button>
		</>
	);
};

export default observer(MovieFilter);
