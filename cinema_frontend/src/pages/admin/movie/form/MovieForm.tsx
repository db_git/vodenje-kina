//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Grid } from "@mantine/core";
import { observer } from "mobx-react-lite";

import Form from "../../../../components/forms/Form";
import Loading from "../../../../components/status/Loading";
import useApiStore from "../../../../hooks/useApiStore";
import { movieFormSchema } from "../../../../schemas/adminSchemas";
import { blobToBase64 } from "../../../../utils/converters";
import { type AdminFormProps } from "../../admin";
import { type MovieFormInputValues, type MovieFormSubmitValues } from "../movie";

import MovieActorsInput from "./MovieActorsInput";
import MovieBackdropInput from "./MovieBackdropInput";
import MovieDirectorsInput from "./MovieDirectorsInput";
import MovieDurationInput from "./MovieDurationInput";
import MovieFilmRatingInput from "./MovieFilmRatingInput";
import MovieGenresInput from "./MovieGenresInput";
import MovieInCinemasInput from "./MovieInCinemasInput";
import MoviePosterInput from "./MoviePosterInput";
import MovieSummaryInput from "./MovieSummaryInput";
import MovieTaglineInput from "./MovieTaglineInput";
import MovieTitleInput from "./MovieTitleInput";
import MovieTrailerInput from "./MovieTrailerInput";
import MovieYearInput from "./MovieYearInput";

const MovieForm = ({ id }: AdminFormProps) => {
	const { movieStore } = useApiStore();

	const handleSubmit = async (values: MovieFormInputValues): Promise<void> => {
		const newValues: MovieFormSubmitValues = {
			...values,
			posterUrl: values.posterUrl instanceof Blob ? await blobToBase64(values.posterUrl) : values.posterUrl,
			backdropUrl:
				values.backdropUrl instanceof Blob ? await blobToBase64(values.backdropUrl) : values.backdropUrl,

			directors: values.directors.map((director) => {
				return director.id;
			}),
			actors: values.actors.map((actor) => {
				return { id: actor.id, character: actor.character };
			})
		};

		if (id) {
			await movieStore.put(id, newValues);
		} else {
			await movieStore.post(newValues);
		}
	};

	if (movieStore.loading.get || (id && !movieStore.response)) return <Loading />;

	const initialValues: MovieFormInputValues = {
		title: movieStore.response?.title ?? "",
		year: movieStore.response?.year ?? 0,
		duration: movieStore.response?.duration ?? 0,
		tagline: movieStore.response?.tagline ?? "",
		summary: movieStore.response?.summary ?? "",
		inCinemas: movieStore.response?.inCinemas ?? true,
		backdropUrl: movieStore.response?.backdropUrl ?? "",
		posterUrl: movieStore.response?.posterUrl ?? "",
		trailerUrl: movieStore.response?.trailerUrl ?? "",
		filmRating: movieStore.response?.filmRating?.id ?? "",
		genres:
			movieStore.response?.genres?.map((genre) => {
				return genre.id;
			}) ?? [],
		actors: movieStore.response?.actors ?? [{ id: "", character: "", order: 0, name: "", imageUrl: "" }],
		directors: movieStore.response?.directors ?? [{ id: "", name: "", imageUrl: "" }]
	};

	return (
		<Form id="movieForm" initialValues={initialValues} onSubmit={handleSubmit} schema={movieFormSchema}>
			{(form) => {
				return (
					<Grid>
						<Grid.Col md={6} xs={12}>
							<MovieTitleInput form={form} />
						</Grid.Col>
						<Grid.Col md={6} xs={12}>
							<MovieTaglineInput form={form} />
						</Grid.Col>

						<Grid.Col span={12}>
							<MovieInCinemasInput form={form} />
						</Grid.Col>

						<Grid.Col md={6} xs={12}>
							<MovieYearInput form={form} />
						</Grid.Col>
						<Grid.Col md={6} xs={12}>
							<MovieDurationInput form={form} />
						</Grid.Col>

						<Grid.Col span={12}>
							<MovieSummaryInput form={form} />
						</Grid.Col>

						<Grid.Col md={6} xs={12}>
							<MoviePosterInput form={form} />
						</Grid.Col>
						<Grid.Col md={6} xs={12}>
							<MovieBackdropInput form={form} />
						</Grid.Col>

						<Grid.Col span={12}>
							<MovieTrailerInput form={form} />
						</Grid.Col>

						<Grid.Col md={6} xs={12}>
							<MovieFilmRatingInput form={form} />
						</Grid.Col>
						<Grid.Col md={6} xs={12}>
							<MovieGenresInput form={form} />
						</Grid.Col>

						<Grid.Col mt="1rem" span={12}>
							<MovieDirectorsInput form={form} />
						</Grid.Col>
						<Grid.Col mt="1rem" span={12}>
							<MovieActorsInput form={form} />
						</Grid.Col>
					</Grid>
				);
			}}
		</Form>
	);
};

export default observer(MovieForm);
