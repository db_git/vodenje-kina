//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { MultiSelect } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useEffect, useRef, useState } from "react";
import { FaTheaterMasks } from "react-icons/fa";

import useApiStore from "../../../../hooks/useApiStore";
import useGetStore from "../../../../hooks/useGetStore";
import { QueryParameterOptions } from "../../../../types/enums/Stores";
import { type InputSelectOptions } from "../../../../types/forms";
import { type MovieGenresInputProps } from "../movie";
import { changeMovieGenreSearch } from "../utils/changeMovieSearch";

const MovieGenresInput = ({ form }: MovieGenresInputProps) => {
	const { movieStore } = useApiStore();
	const { genreGetStore } = useGetStore();

	const changeGenreSearch = useRef(changeMovieGenreSearch).current;
	const [genreSelectOptions, setGenreSelectOptions] = useState<InputSelectOptions[]>([]);

	useEffect(() => {
		runInAction(() => {
			genreGetStore.setQueryParameters(QueryParameterOptions.Page, 1);
			genreGetStore.setQueryParameters(QueryParameterOptions.Size, 25);
			genreGetStore.setQueryParameters(QueryParameterOptions.OrderBy, 1);

			if (genreGetStore.responses) return;
			void genreGetStore.getAll();
		});

		return () => {
			runInAction(() => {
				genreGetStore.setQueryParameters(QueryParameterOptions.Search, "");
			});
		};
	}, [genreGetStore]);

	useEffect(() => {
		let genreOptions: InputSelectOptions[] = [];

		runInAction(() => {
			if (genreGetStore.responses) {
				genreOptions = genreGetStore.responses.data.map((genreResponse) => {
					return { value: genreResponse.id, label: genreResponse.name };
				});
			}
		});

		setGenreSelectOptions(genreOptions);
	}, [genreGetStore.responses]);

	useEffect(() => {
		return () => {
			changeGenreSearch.cancel();
		};
	}, [changeGenreSearch]);

	const handleOnSearch = (search: string): void => {
		changeGenreSearch(genreGetStore, setGenreSelectOptions, search);
	};

	return (
		<MultiSelect
			data={genreSelectOptions}
			disabled={movieStore.loading.get || movieStore.loading.post || movieStore.loading.put}
			icon={<FaTheaterMasks />}
			label="Genres"
			nothingFound="No genres found"
			onSearchChange={handleOnSearch}
			placeholder="Select genres"
			required
			{...form.getInputProps("genres")}
		/>
	);
};

export default observer(MovieGenresInput);
