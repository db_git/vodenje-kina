//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type FileWithPath } from "@mantine/dropzone";
import { observer } from "mobx-react-lite";
import { useCallback } from "react";

import ImageInput from "../../../../components/forms/ImageInput";
import useApiStore from "../../../../hooks/useApiStore";
import { type MovieBackdropInputProps } from "../movie";

const MovieBackdropInput = ({ form }: MovieBackdropInputProps) => {
	const { movieStore } = useApiStore();

	const handleOnDrop = useCallback(
		(files: FileWithPath[]) => {
			form.setFieldValue("backdropUrl", new Blob([files[0]]));
		},
		[form]
	);

	const handleRemoveImage = useCallback(() => {
		form.setFieldValue("backdropUrl", "");
	}, [form]);

	return (
		<ImageInput
			disabled={movieStore.loading.get || movieStore.loading.post || movieStore.loading.put}
			image={form.values.backdropUrl}
			isTouched={form.isTouched("backdropUrl")}
			label="Backdrop"
			onDrop={handleOnDrop}
			placeholder="Enter backdrop URL"
			removeImage={handleRemoveImage}
			required
			uploadMessage="Drag backdrop here"
			uploadedMessage="Backdrop uploaded."
			{...form.getInputProps("backdropUrl")}
		/>
	);
};

export default observer(MovieBackdropInput);
