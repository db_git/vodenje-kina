//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { ActionIcon, Button, Grid, Group, Select, Text } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback, useEffect, useRef, useState } from "react";
import { BsPersonFill } from "react-icons/bs";
import { IoMdTrash } from "react-icons/io";
import { TbPlus } from "react-icons/tb";

import SelectItem from "../../../../components/forms/SelectItem";
import useApiStore from "../../../../hooks/useApiStore";
import useGetStore from "../../../../hooks/useGetStore";
import { QueryParameterOptions } from "../../../../types/enums/Stores";
import { type InputSelectOptionsWithPicture } from "../../../../types/forms";
import hashCode from "../../../../utils/hashCode";
import { type MovieDirectorsInputProps } from "../movie";
import { changeMoviePersonSearch } from "../utils/changeMovieSearch";

const MovieDirectorsInput = ({ form }: MovieDirectorsInputProps) => {
	const { directorGetStore } = useGetStore();
	const { movieStore } = useApiStore();

	const [directorsSelectOptions, setDirectorsSelectOptions] = useState<InputSelectOptionsWithPicture[]>(
		form.values.directors.map((director) => {
			return { value: director.id, label: director.name, pictureUrl: director.imageUrl };
		})
	);

	const changePersonSearch = useRef(changeMoviePersonSearch).current;

	useEffect(() => {
		runInAction(() => {
			directorGetStore.setQueryParameters(QueryParameterOptions.Page, 1);
			directorGetStore.setQueryParameters(QueryParameterOptions.Size, 25);
			directorGetStore.setQueryParameters(QueryParameterOptions.OrderBy, 1);
			directorGetStore.setQueryParameters(QueryParameterOptions.Search, "");

			if (directorGetStore.responses) return;
			void directorGetStore.getAll();
		});
	}, [directorGetStore]);

	useEffect(() => {
		const directorsOptions: InputSelectOptionsWithPicture[] = [];

		runInAction(() => {
			if (directorGetStore.responses) {
				for (const personResponse of directorGetStore.responses.data) {
					directorsOptions.push({
						value: personResponse.id,
						label: personResponse.name,
						pictureUrl: personResponse.imageUrl
					});
				}
			}
		});

		setDirectorsSelectOptions(Array.from(new Set(directorsOptions)));
	}, [directorGetStore.responses]);

	useEffect(() => {
		return () => {
			changePersonSearch.cancel();
		};
	}, [changePersonSearch]);

	const handleOnSearch = useCallback(
		(search: string): void => {
			changePersonSearch(directorGetStore, setDirectorsSelectOptions, search);
		},
		[changePersonSearch, directorGetStore]
	);

	const handleOnChange = useCallback(
		(value: string | null, index: number): void => {
			if (
				form.values.directors.findIndex((movieDirector) => {
					return movieDirector.id === value;
				}) < 0
			)
				form.getInputProps(`directors.${index}.id`).onChange(value);
		},
		[form]
	);

	return (
		<Grid>
			<Grid.Col span={12}>
				<span className="cursor-default">
					<Text size="sm" weight={500}>
						Directors&nbsp;
						<Text color="red" inline size="sm" span>
							*
						</Text>
					</Text>
				</span>
			</Grid.Col>
			{form.values.directors.map((director, index) => {
				return (
					<Grid.Col key={hashCode(`${director.id}-${index}`)} md={6} xs={12}>
						<Group>
							<Select
								className="flex-1"
								disabled={movieStore.loading.get || movieStore.loading.post || movieStore.loading.put}
								icon={<BsPersonFill />}
								itemComponent={SelectItem}
								maxDropdownHeight={576}
								nothingFound="No directors found"
								placeholder="Select director"
								searchable
								{...form.getInputProps(`directors.${index}.id`)}
								data={directorsSelectOptions}
								onChange={(value) => {
									handleOnChange(value, index);
								}}
								onSearchChange={handleOnSearch}
							/>
							<ActionIcon
								color="red"
								disabled={
									index === 0 ||
									movieStore.loading.get ||
									movieStore.loading.post ||
									movieStore.loading.put
								}
								onClick={() => {
									form.removeListItem("directors", index);
								}}
							>
								<IoMdTrash size={16} />
							</ActionIcon>
						</Group>
					</Grid.Col>
				);
			})}
			<Grid.Col span={12}>
				<Button
					color="cyan"
					compact
					disabled={movieStore.loading.get || movieStore.loading.post || movieStore.loading.put}
					leftIcon={<BsPersonFill size={16} />}
					onClick={() => {
						form.insertListItem("directors", { id: "", name: "", imageUrl: "" });
					}}
					rightIcon={<TbPlus size={16} />}
				>
					Add director
				</Button>
			</Grid.Col>
		</Grid>
	);
};

export default observer(MovieDirectorsInput);
