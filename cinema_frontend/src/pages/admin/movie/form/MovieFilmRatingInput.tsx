//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Select } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useEffect, useState } from "react";
import { BiStar } from "react-icons/bi";

import useApiStore from "../../../../hooks/useApiStore";
import useGetStore from "../../../../hooks/useGetStore";
import { QueryParameterOptions } from "../../../../types/enums/Stores";
import { type InputSelectOptions } from "../../../../types/forms";
import { type MovieFilmRatingInputProps } from "../movie";

const MovieFilmRatingInput = ({ form }: MovieFilmRatingInputProps) => {
	const { movieStore } = useApiStore();
	const { filmRatingGetStore } = useGetStore();
	const [filmRatingSelectOptions, setFilmRatingSelectOptions] = useState<InputSelectOptions[]>([]);

	useEffect(() => {
		runInAction(() => {
			filmRatingGetStore.setQueryParameters(QueryParameterOptions.Page, 1);
			filmRatingGetStore.setQueryParameters(QueryParameterOptions.Size, 50);
			filmRatingGetStore.setQueryParameters(QueryParameterOptions.OrderBy, 1);

			if (filmRatingGetStore.responses) return;
			void filmRatingGetStore.getAll();
		});

		return () => {
			runInAction(() => {
				filmRatingGetStore.setQueryParameters(QueryParameterOptions.Size, 10);
				filmRatingGetStore.setQueryParameters(QueryParameterOptions.Search, "");
			});
		};
	}, [filmRatingGetStore]);

	useEffect(() => {
		let filmRatingOptions: InputSelectOptions[] = [];

		runInAction(() => {
			if (filmRatingGetStore.responses) {
				filmRatingOptions = filmRatingGetStore.responses.data.map((filmRatingResponse) => {
					return { value: filmRatingResponse.id, label: filmRatingResponse.type };
				});
			}
		});

		setFilmRatingSelectOptions(filmRatingOptions);
	}, [filmRatingGetStore.responses]);

	return (
		<Select
			data={filmRatingSelectOptions}
			disabled={movieStore.loading.get || movieStore.loading.post || movieStore.loading.put}
			icon={<BiStar />}
			label="Film rating"
			placeholder="Select film rating"
			required
			{...form.getInputProps("filmRating")}
		/>
	);
};

export default observer(MovieFilmRatingInput);
