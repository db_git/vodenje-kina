//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Textarea } from "@mantine/core";
import { observer } from "mobx-react-lite";

import useApiStore from "../../../../hooks/useApiStore";
import { type MovieSummaryInputProps } from "../movie";

const MovieSummaryInput = ({ form }: MovieSummaryInputProps) => {
	const { movieStore } = useApiStore();

	return (
		<Textarea
			autosize
			disabled={movieStore.loading.get || movieStore.loading.post || movieStore.loading.put}
			label="Summary"
			maxRows={20}
			minRows={10}
			placeholder="Enter movie summary"
			required
			{...form.getInputProps("summary")}
		/>
	);
};

export default observer(MovieSummaryInput);
