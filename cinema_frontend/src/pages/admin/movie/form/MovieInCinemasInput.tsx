//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Switch, useMantineTheme } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { TbX, TbCheck } from "react-icons/tb";

import useApiStore from "../../../../hooks/useApiStore";
import { type MovieInCinemasInputProps } from "../movie";

const MovieInCinemasInput = ({ form }: MovieInCinemasInputProps) => {
	const theme = useMantineTheme();
	const { movieStore } = useApiStore();

	const thumbIcon = form.values.inCinemas ? (
		<TbCheck color={theme.colors.teal[theme.fn.primaryShade()]} size={16} />
	) : (
		<TbX color={theme.colors.red[theme.fn.primaryShade()]} size={16} />
	);

	return (
		<Switch
			color="teal"
			disabled={movieStore.loading.get || movieStore.loading.post || movieStore.loading.put}
			label="Upcoming release?"
			mt={15}
			size="md"
			thumbIcon={thumbIcon}
			{...form.getInputProps("inCinemas")}
			checked={form.values.inCinemas}
		/>
	);
};

export default observer(MovieInCinemasInput);
