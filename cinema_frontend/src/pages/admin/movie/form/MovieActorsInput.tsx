//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { ActionIcon, Button, Grid, Group, Select, Text, TextInput } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback, useEffect, useRef, useState } from "react";
import { BsPersonFill } from "react-icons/bs";
import { IoMdTrash } from "react-icons/io";
import { TbPlus } from "react-icons/tb";

import SelectItem from "../../../../components/forms/SelectItem";
import useApiStore from "../../../../hooks/useApiStore";
import useGetStore from "../../../../hooks/useGetStore";
import { QueryParameterOptions } from "../../../../types/enums/Stores";
import { type InputSelectOptionsWithPicture } from "../../../../types/forms";
import hashCode from "../../../../utils/hashCode";
import { type MovieActorsInputProps } from "../movie";
import { changeMoviePersonSearch } from "../utils/changeMovieSearch";

const MovieActorsInput = ({ form }: MovieActorsInputProps) => {
	const { actorGetStore } = useGetStore();
	const { movieStore } = useApiStore();

	const [actorsSelectOptions, setActorsSelectOptions] = useState<InputSelectOptionsWithPicture[]>(
		form.values.actors.map((actor) => {
			return { value: actor.id, label: actor.name, pictureUrl: actor.imageUrl };
		})
	);

	const changePersonSearch = useRef(changeMoviePersonSearch).current;

	useEffect(() => {
		runInAction(() => {
			actorGetStore.setQueryParameters(QueryParameterOptions.Search, "");
			actorGetStore.setQueryParameters(QueryParameterOptions.Page, 1);
			actorGetStore.setQueryParameters(QueryParameterOptions.Size, 25);
			actorGetStore.setQueryParameters(QueryParameterOptions.OrderBy, 1);

			if (actorGetStore.responses) return;
			void actorGetStore.getAll();
		});
	}, [actorGetStore]);

	useEffect(() => {
		const actorsOptions: InputSelectOptionsWithPicture[] = [];

		runInAction(() => {
			if (actorGetStore.responses) {
				for (const personResponse of actorGetStore.responses.data) {
					actorsOptions.push({
						value: personResponse.id,
						label: personResponse.name,
						pictureUrl: personResponse.imageUrl
					});
				}
			}
		});

		setActorsSelectOptions(Array.from(new Set(actorsOptions)));
	}, [actorGetStore.responses]);

	useEffect(() => {
		return () => {
			changePersonSearch.cancel();
		};
	}, [changePersonSearch]);

	const handleOnSearch = useCallback(
		(search: string): void => {
			changePersonSearch(actorGetStore, setActorsSelectOptions, search);
		},
		[actorGetStore, changePersonSearch]
	);

	return (
		<Grid>
			<Grid.Col span={12}>
				<span className="cursor-default">
					<Text size="sm" weight={500}>
						Actors&nbsp;
						<Text color="red" inline size="sm" span>
							*
						</Text>
					</Text>
				</span>
			</Grid.Col>
			{form.values.actors.map((actor, index) => {
				return (
					<Grid.Col key={hashCode(`${actor.id}-${index}`)} md={6} xs={12}>
						<Group>
							<Select
								className="flex-1"
								disabled={movieStore.loading.get || movieStore.loading.post || movieStore.loading.put}
								icon={<BsPersonFill />}
								itemComponent={SelectItem}
								label="Actor"
								maxDropdownHeight={576}
								nothingFound="No actors found"
								placeholder="Select actor"
								searchable
								{...form.getInputProps(`actors.${index}.id`)}
								data={actorsSelectOptions}
								onSearchChange={handleOnSearch}
							/>
							<TextInput
								className="flex-1"
								disabled={movieStore.loading.get || movieStore.loading.post || movieStore.loading.put}
								label="Character"
								required
								{...form.getInputProps(`actors.${index}.character`)}
							/>
							<ActionIcon
								color="red"
								disabled={
									index === 0 ||
									movieStore.loading.get ||
									movieStore.loading.post ||
									movieStore.loading.put
								}
								mt={24}
								onClick={() => {
									form.removeListItem("actors", index);
								}}
							>
								<IoMdTrash size={16} />
							</ActionIcon>
						</Group>
					</Grid.Col>
				);
			})}
			<Grid.Col span={12}>
				<Button
					color="cyan"
					compact
					disabled={movieStore.loading.get || movieStore.loading.post || movieStore.loading.put}
					leftIcon={<BsPersonFill size={16} />}
					onClick={() => {
						form.insertListItem("actors", { id: "", name: "", character: "", imageUrl: "", order: "" });
					}}
					rightIcon={<TbPlus size={16} />}
				>
					Add actor
				</Button>
			</Grid.Col>
		</Grid>
	);
};

export default observer(MovieActorsInput);
