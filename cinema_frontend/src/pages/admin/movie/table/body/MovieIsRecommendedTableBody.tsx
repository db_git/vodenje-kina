//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Text } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { MdOutlineCheckCircleOutline } from "react-icons/md";
import { VscError } from "react-icons/vsc";

import useApiStore from "../../../../../hooks/useApiStore";
import { JsonPatchOperation } from "../../../../../types/enums/Http";
import { type MovieProps } from "../../../../../types/movies";

const MovieIsRecommendedTableBody = ({ movie }: MovieProps) => {
	const { movieStore } = useApiStore();

	if (!movieStore.showIsRecommended) return null;

	const toggleIsRecommended = () => {
		runInAction(() => {
			void movieStore.patch(movie.id, [
				{
					op: JsonPatchOperation.Add,
					path: "/isRecommended",
					value: !movie.isRecommended
				}
			]);
		});
	};

	return (
		<td>
			<Text color={movie.isRecommended ? "green" : "red"} ta="center" weight={500}>
				{movie.isRecommended ? (
					<MdOutlineCheckCircleOutline className="cursor-pointer" onClick={toggleIsRecommended} size={24} />
				) : (
					<VscError className="cursor-pointer" onClick={toggleIsRecommended} size={24} />
				)}
			</Text>
		</td>
	);
};

export default observer(MovieIsRecommendedTableBody);
