//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { memo } from "react";

import ActionsTableHeader from "../../utils/ActionsTableHeader";

import MovieDurationTableHeader from "./header/MovieDurationTableHeader";
import MovieInCinemasTableHeader from "./header/MovieInCinemasTableHeader";
import MovieIsFeaturedTableHeader from "./header/MovieIsFeaturedTableHeader";
import MovieIsRecommendedTableHeader from "./header/MovieIsRecommendedTableHeader";
import MoviePosterTableHeader from "./header/MoviePosterTableHeader";
import MovieSummaryTableHeader from "./header/MovieSummaryTableHeader";
import MovieTaglineTableHeader from "./header/MovieTaglineTableHeader";
import MovieTitleTableHeader from "./header/MovieTitleTableHeader";
import MovieYearTableHeader from "./header/MovieYearTableHeader";

const MovieTableHead = () => {
	return (
		<tr>
			<MoviePosterTableHeader />
			<MovieTitleTableHeader />
			<MovieYearTableHeader />
			<MovieDurationTableHeader />
			<MovieTaglineTableHeader />
			<MovieSummaryTableHeader />
			<MovieInCinemasTableHeader />
			<MovieIsFeaturedTableHeader />
			<MovieIsRecommendedTableHeader />
			<ActionsTableHeader />
		</tr>
	);
};

export default memo(MovieTableHead);
