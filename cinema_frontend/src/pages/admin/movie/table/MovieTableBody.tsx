//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Center, Spoiler, Text } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { type ReactNode, useState } from "react";

import useApiStore from "../../../../hooks/useApiStore";
import { Routes } from "../../../../routes/Routes";
import { type Movie } from "../../../../types/entities";
import DeleteModal from "../../DeleteModal";
import TableActionIcons from "../../utils/TableActionIcons";
import TableImage from "../../utils/TableImage";

import MovieInCinemasTableBody from "./body/MovieInCinemasTableBody";
import MovieIsFeaturedTableBody from "./body/MovieIsFeaturedTableBody";
import MovieIsRecommendedTableBody from "./body/MovieIsRecommendedTableBody";

const MovieTableBody = () => {
	const { movieStore } = useApiStore();
	const [selectedMovie, setSelectedMovie] = useState<Movie>();
	const [notificationSuccessMessage, setNotificationSuccessMessage] = useState<ReactNode>();
	const [deleteModalOpen, setDeleteModalOpen] = useState(false);

	const modalMessage = (
		<>
			{`Are you sure you want to remove movie ${selectedMovie?.title}?`}
			<Center mt="1rem">
				<TableImage
					height="16rem"
					radius={24}
					src={selectedMovie?.posterUrl ?? ""}
					width="12.5rem"
					withBorder
				/>
			</Center>
		</>
	);

	return (
		<>
			<DeleteModal
				id={selectedMovie?.id ?? ""}
				isOpen={deleteModalOpen}
				message={modalMessage}
				notificationSuccessMessage={notificationSuccessMessage}
				store={movieStore}
				toggle={() => {
					setDeleteModalOpen(!deleteModalOpen);
				}}
			/>

			{movieStore.responses?.data.map((movie) => {
				return (
					<tr key={movie.id}>
						{movieStore.showPoster && (
							<td>
								<TableImage
									height="5.5rem"
									radius={16}
									src={movie.posterUrl}
									width="4.25rem"
									withBorder
								/>
							</td>
						)}
						<td>
							<Text pr="2rem" weight={500}>
								{movie.title}
							</Text>
						</td>
						{movieStore.showYear && (
							<td>
								<Text className="min-w-[4.125rem]" pr="1rem" weight={500}>
									{movie.year}
								</Text>
							</td>
						)}
						{movieStore.showDuration && (
							<td>
								<Text weight={500}>{`${Math.floor(movie.duration / 60)}h ${
									movie.duration % 60
								}m`}</Text>
							</td>
						)}
						{movieStore.showTagline && (
							<td>
								<Text weight={500}>{movie.tagline}</Text>
							</td>
						)}
						{movieStore.showSummary && (
							<td>
								<Spoiler hideLabel="Hide" maxHeight={45} showLabel="Show more">
									<Text weight={500}>{movie.summary}</Text>
								</Spoiler>
							</td>
						)}
						<MovieInCinemasTableBody movie={movie} />
						<MovieIsFeaturedTableBody movie={movie} />
						<MovieIsRecommendedTableBody movie={movie} />
						<TableActionIcons
							location={`${Routes.AdminMoviesEdit}/${movie.id}`}
							onDeleteClick={() => {
								runInAction(() => {
									setSelectedMovie(movie);
									setNotificationSuccessMessage(`Movie '${movie.title}' deleted successfully.`);
								});
								setDeleteModalOpen(!deleteModalOpen);
							}}
						/>
					</tr>
				);
			})}
		</>
	);
};

export default observer(MovieTableBody);
