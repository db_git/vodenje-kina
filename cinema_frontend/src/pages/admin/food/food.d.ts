//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type UseFormProps } from "../../../components/forms/forms";

export type FoodFormInputValues = {
	name: string;
	price: number;
	availableQuantity: number;
	type: "" | "Drink" | "Snack";
	size: number;
	imageUrl: Blob | string;
};

export type FoodFormSubmitValues = Omit<FoodFormInputValues, "imageUrl"> & {
	imageUrl: string;
};

export type FoodAvailableQuantityInputProps = UseFormProps<FoodFormInputValues>;
export type FoodImageInputProps = UseFormProps<FoodFormInputValues>;
export type FoodNameInputProps = UseFormProps<FoodFormInputValues>;
export type FoodPriceInputProps = UseFormProps<FoodFormInputValues>;
export type FoodSizeInputProps = UseFormProps<FoodFormInputValues>;
export type FoodTypeInputProps = UseFormProps<FoodFormInputValues>;
