//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Container } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";

import useApiStore from "../../../hooks/useApiStore";
import { Routes } from "../../../routes/Routes";
import AdminEntityBase from "../AdminEntityBase";

import FoodTableBody from "./table/FoodTableBody";
import FoodTableHead from "./table/FoodTableHead";
import FoodOptions from "./utils/FoodOptions";

const AdminFood = () => {
	const { foodStore } = useApiStore();

	useEffect(() => {
		document.title = "Food | Admin";
	}, []);

	return (
		<Container size="xl">
			<AdminEntityBase
				location={Routes.AdminFood}
				store={foodStore}
				tbody={<FoodTableBody />}
				thead={<FoodTableHead />}
				withCreateButton
			>
				<FoodOptions />
			</AdminEntityBase>
		</Container>
	);
};

export default observer(AdminFood);
