//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { NumberInput } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { GiWeight } from "react-icons/gi";

import useApiStore from "../../../../hooks/useApiStore";
import { type FoodSizeInputProps } from "../food";

const FoodSizeInput = ({ form }: FoodSizeInputProps) => {
	const { foodStore } = useApiStore();

	return (
		<NumberInput
			disabled={foodStore.loading.get || foodStore.loading.post || foodStore.loading.put}
			icon={<GiWeight />}
			label="Size (liters or kilograms)"
			max={25}
			min={0.001}
			placeholder="Enter food size"
			precision={5}
			required
			step={0.05}
			{...form.getInputProps("size")}
		/>
	);
};

export default observer(FoodSizeInput);
