//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { NumberInput } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { MdOutlineAttachMoney } from "react-icons/md";

import useApiStore from "../../../../hooks/useApiStore";
import { type FoodPriceInputProps } from "../food";

const FoodPriceInput = ({ form }: FoodPriceInputProps) => {
	const { foodStore } = useApiStore();

	return (
		<NumberInput
			disabled={foodStore.loading.get || foodStore.loading.post || foodStore.loading.put}
			icon={<MdOutlineAttachMoney />}
			label="Price (EUR)"
			max={1_000}
			min={1}
			placeholder="Enter food price"
			precision={2}
			required
			step={0.05}
			{...form.getInputProps("price")}
		/>
	);
};

export default observer(FoodPriceInput);
