//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Grid } from "@mantine/core";
import { observer } from "mobx-react-lite";

import Form from "../../../../components/forms/Form";
import Loading from "../../../../components/status/Loading";
import useApiStore from "../../../../hooks/useApiStore";
import { foodFormSchema } from "../../../../schemas/adminSchemas";
import { blobToBase64 } from "../../../../utils/converters";
import { type AdminFormProps } from "../../admin";
import { type FoodFormInputValues, type FoodFormSubmitValues } from "../food";

import FoodAvailableQuantityInput from "./FoodAvailableQuantityInput";
import FoodImageInput from "./FoodImageInput";
import FoodNameInput from "./FoodNameInput";
import FoodPriceInput from "./FoodPriceInput";
import FoodSizeInput from "./FoodSizeInput";
import FoodTypeInput from "./FoodTypeInput";

const FoodForm = ({ id }: AdminFormProps) => {
	const { foodStore } = useApiStore();

	const handleSubmit = async (values: FoodFormInputValues): Promise<void> => {
		const newValues: FoodFormSubmitValues = {
			...values,
			imageUrl: values.imageUrl instanceof Blob ? await blobToBase64(values.imageUrl) : values.imageUrl
		};

		if (id) {
			await foodStore.put(id, newValues);
		} else {
			await foodStore.post(newValues);
		}
	};

	if (foodStore.loading.get || (id && !foodStore.response)) return <Loading />;

	const initialValues: FoodFormInputValues = {
		name: foodStore.response?.name ?? "",
		price: foodStore.response?.price ?? 0,
		availableQuantity: foodStore.response?.availableQuantity ?? 0,
		type: foodStore.response?.type ?? "",
		size: foodStore.response?.size ?? 0,
		imageUrl: foodStore.response?.imageUrl ?? ""
	};

	return (
		<Form id="foodForm" initialValues={initialValues} onSubmit={handleSubmit} schema={foodFormSchema}>
			{(form) => {
				return (
					<Grid>
						<Grid.Col span={12}>
							<FoodNameInput form={form} />
						</Grid.Col>

						<Grid.Col md={6} xs={12}>
							<FoodPriceInput form={form} />
						</Grid.Col>
						<Grid.Col md={6} xs={12}>
							<FoodAvailableQuantityInput form={form} />
						</Grid.Col>

						<Grid.Col md={6} xs={12}>
							<FoodTypeInput form={form} />
						</Grid.Col>
						<Grid.Col md={6} xs={12}>
							<FoodSizeInput form={form} />
						</Grid.Col>

						<Grid.Col span={12}>
							<FoodImageInput form={form} />
						</Grid.Col>
					</Grid>
				);
			}}
		</Form>
	);
};

export default observer(FoodForm);
