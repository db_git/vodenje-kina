//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Select } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { MdFastfood } from "react-icons/md";

import useApiStore from "../../../../hooks/useApiStore";
import { type FoodTypeInputProps } from "../food";

const selectOptions = [
	{ value: "Drink", label: "Drink" },
	{ value: "Snack", label: "Snack" }
];

const FoodTypeInput = ({ form }: FoodTypeInputProps) => {
	const { foodStore } = useApiStore();

	return (
		<Select
			data={selectOptions}
			disabled={foodStore.loading.get || foodStore.loading.post || foodStore.loading.put}
			icon={<MdFastfood />}
			label="Type"
			placeholder="Select type"
			required
			{...form.getInputProps("type")}
		/>
	);
};

export default observer(FoodTypeInput);
