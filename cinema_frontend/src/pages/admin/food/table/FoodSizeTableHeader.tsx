//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { observer } from "mobx-react-lite";
import { useNavigate } from "react-router-dom";

import useApiStore from "../../../../hooks/useApiStore";
import { FoodOrderBy } from "../../../../types/enums/OrderBy";
import TableHeaderSort from "../../utils/TableHeaderSort";
import { changeFoodSizeSort } from "../utils/changeFoodSort";

const FoodSizeTableHeader = () => {
	const url = useNavigate();
	const { foodStore } = useApiStore();

	const handleFoodSizeSort = (): void => {
		changeFoodSizeSort(foodStore, url);
	};

	if (!foodStore.showSize) return null;

	return (
		<TableHeaderSort
			ascending={foodStore.queryParameters.orderBy === FoodOrderBy.SizeAscending}
			descending={foodStore.queryParameters.orderBy === FoodOrderBy.SizeDescending}
			key="size"
			onClick={handleFoodSizeSort}
		>
			Size
		</TableHeaderSort>
	);
};

export default observer(FoodSizeTableHeader);
