//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Text } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { type ReactNode, useState } from "react";

import useApiStore from "../../../../hooks/useApiStore";
import { Routes } from "../../../../routes/Routes";
import { useCurrencyFormatter } from "../../../../utils/converters";
import DeleteModal from "../../DeleteModal";
import TableActionIcons from "../../utils/TableActionIcons";
import TableImage from "../../utils/TableImage";

const FoodTableBody = () => {
	const { foodStore } = useApiStore();
	const [id, setId] = useState("");
	const [modalMessage, setModalMessage] = useState("");
	const [notificationSuccessMessage, setNotificationSuccessMessage] = useState<ReactNode>();
	const [deleteModalOpen, setDeleteModalOpen] = useState(false);
	const currencyFormatter = useCurrencyFormatter();

	return (
		<>
			<DeleteModal
				id={id}
				isOpen={deleteModalOpen}
				message={modalMessage}
				notificationSuccessMessage={notificationSuccessMessage}
				store={foodStore}
				toggle={() => {
					setDeleteModalOpen(!deleteModalOpen);
				}}
			/>

			{foodStore.responses?.data.map((food) => {
				return (
					<tr key={food.id}>
						{foodStore.showImage && (
							<td>
								<TableImage height="4rem" radius="xl" src={food.imageUrl} width="4rem" withBorder />
							</td>
						)}
						<td>
							<Text pr="1rem" weight={500}>
								{food.name}
							</Text>
						</td>
						<td>
							<Text pr="3rem">{currencyFormatter.format(food.price)}</Text>
						</td>
						<td>
							<Text color={food.availableQuantity < 250 ? "red" : ""} pr="4rem">
								{food.availableQuantity}
							</Text>
						</td>
						{foodStore.showSize && (
							<td>
								<Text pr="1rem">{food.size}</Text>
							</td>
						)}
						{foodStore.showType && (
							<td>
								<Text pr="1rem">{food.type}</Text>
							</td>
						)}
						<TableActionIcons
							location={`${Routes.AdminFoodEdit}/${food.id}`}
							onDeleteClick={() => {
								runInAction(() => {
									setId(food.id);
									setModalMessage(`Are you sure you want to remove ${food.name}?`);
									setNotificationSuccessMessage(`Food '${food.name}' deleted successfully.`);
								});
								setDeleteModalOpen(!deleteModalOpen);
							}}
						/>
					</tr>
				);
			})}
		</>
	);
};

export default observer(FoodTableBody);
