//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { runInAction } from "mobx";
import { type NavigateFunction } from "react-router-dom";

import { Routes } from "../../../../routes/Routes";
import { type IFoodApiStore } from "../../../../stores/stores";
import { FoodOrderBy } from "../../../../types/enums/OrderBy";
import { QueryParameterOptions } from "../../../../types/enums/Stores";

const changeFoodNameSort = (foodStore: IFoodApiStore, url: NavigateFunction): void => {
	runInAction(() => {
		if (
			foodStore.queryParameters.orderBy === FoodOrderBy.NameAscending ||
			foodStore.queryParameters.orderBy === FoodOrderBy.NameDescending
		) {
			foodStore.setQueryParameters(
				QueryParameterOptions.OrderBy,
				(foodStore.queryParameters.orderBy % FoodOrderBy.NameDescending) + FoodOrderBy.NameAscending
			);
		} else {
			foodStore.setQueryParameters(QueryParameterOptions.OrderBy, FoodOrderBy.NameAscending);
		}

		foodStore.modifyUrl(Routes.AdminFood, url);
		void foodStore.getAll();
	});
};

const changeFoodPriceSort = (foodStore: IFoodApiStore, url: NavigateFunction): void => {
	runInAction(() => {
		if (
			foodStore.queryParameters.orderBy === FoodOrderBy.PriceAscending ||
			foodStore.queryParameters.orderBy === FoodOrderBy.PriceDescending
		) {
			foodStore.setQueryParameters(
				QueryParameterOptions.OrderBy,
				(foodStore.queryParameters.orderBy % (FoodOrderBy.PriceDescending / 2)) + FoodOrderBy.PriceAscending
			);
		} else {
			foodStore.setQueryParameters(QueryParameterOptions.OrderBy, FoodOrderBy.PriceAscending);
		}

		foodStore.modifyUrl(Routes.AdminFood, url);
		void foodStore.getAll();
	});
};

const changeFoodAvailableQuantitySort = (foodStore: IFoodApiStore, url: NavigateFunction): void => {
	runInAction(() => {
		if (
			foodStore.queryParameters.orderBy === FoodOrderBy.AvailableQuantityAscending ||
			foodStore.queryParameters.orderBy === FoodOrderBy.AvailableQuantityDescending
		) {
			foodStore.setQueryParameters(
				QueryParameterOptions.OrderBy,
				(foodStore.queryParameters.orderBy % (FoodOrderBy.AvailableQuantityDescending / 3)) +
					FoodOrderBy.AvailableQuantityAscending
			);
		} else {
			foodStore.setQueryParameters(QueryParameterOptions.OrderBy, FoodOrderBy.AvailableQuantityAscending);
		}

		foodStore.modifyUrl(Routes.AdminFood, url);
		void foodStore.getAll();
	});
};

const changeFoodSizeSort = (foodStore: IFoodApiStore, url: NavigateFunction): void => {
	runInAction(() => {
		if (
			foodStore.queryParameters.orderBy === FoodOrderBy.SizeAscending ||
			foodStore.queryParameters.orderBy === FoodOrderBy.SizeDescending
		) {
			foodStore.setQueryParameters(
				QueryParameterOptions.OrderBy,
				(foodStore.queryParameters.orderBy % (FoodOrderBy.SizeDescending / 4)) + FoodOrderBy.SizeAscending
			);
		} else {
			foodStore.setQueryParameters(QueryParameterOptions.OrderBy, FoodOrderBy.SizeAscending);
		}

		foodStore.modifyUrl(Routes.AdminFood, url);
		void foodStore.getAll();
	});
};

const changeFoodTypeSort = (foodStore: IFoodApiStore, url: NavigateFunction): void => {
	runInAction(() => {
		if (
			foodStore.queryParameters.orderBy === FoodOrderBy.TypeAscending ||
			foodStore.queryParameters.orderBy === FoodOrderBy.TypeDescending
		) {
			foodStore.setQueryParameters(
				QueryParameterOptions.OrderBy,
				(foodStore.queryParameters.orderBy % (FoodOrderBy.TypeDescending / 5)) + FoodOrderBy.TypeAscending
			);
		} else {
			foodStore.setQueryParameters(QueryParameterOptions.OrderBy, FoodOrderBy.TypeAscending);
		}

		foodStore.modifyUrl(Routes.AdminFood, url);
		void foodStore.getAll();
	});
};

export {
	changeFoodNameSort,
	changeFoodPriceSort,
	changeFoodSizeSort,
	changeFoodAvailableQuantitySort,
	changeFoodTypeSort
};
