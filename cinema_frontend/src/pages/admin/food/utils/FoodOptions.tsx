//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Menu, Text, ThemeIcon } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { GiWeight } from "react-icons/gi";
import { MdImage, MdFastfood } from "react-icons/md";

import useApiStore from "../../../../hooks/useApiStore";
import DisplayColumns from "../../DisplayColumns";

const FoodOptions = () => {
	const { foodStore } = useApiStore();

	const imageColor = foodStore.showImage ? "green" : "red";
	const sizeColor = foodStore.showSize ? "green" : "red";
	const typeColor = foodStore.showType ? "green" : "red";

	return (
		<Menu closeOnItemClick={false} position="bottom" withArrow>
			<Menu.Target>
				<DisplayColumns />
			</Menu.Target>

			<Menu.Dropdown>
				<Menu.Item
					icon={
						<ThemeIcon color={imageColor} variant="light">
							<MdImage />
						</ThemeIcon>
					}
					onClick={() => {
						foodStore.toggleShowImage();
					}}
				>
					<Text color={imageColor}>Picture</Text>
				</Menu.Item>

				<Menu.Item
					icon={
						<ThemeIcon color={sizeColor} variant="light">
							<GiWeight />
						</ThemeIcon>
					}
					onClick={() => {
						foodStore.toggleShowSize();
					}}
				>
					<Text color={sizeColor}>Size</Text>
				</Menu.Item>

				<Menu.Item
					icon={
						<ThemeIcon color={typeColor} variant="light">
							<MdFastfood />
						</ThemeIcon>
					}
					onClick={() => {
						foodStore.toggleShowType();
					}}
				>
					<Text color={typeColor}>Type</Text>
				</Menu.Item>
			</Menu.Dropdown>
		</Menu>
	);
};

export default observer(FoodOptions);
