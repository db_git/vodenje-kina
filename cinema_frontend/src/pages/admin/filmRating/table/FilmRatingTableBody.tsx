//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Text } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { type ReactNode, useState } from "react";

import useApiStore from "../../../../hooks/useApiStore";
import { Routes } from "../../../../routes/Routes";
import DeleteModal from "../../DeleteModal";
import TableActionIcons from "../../utils/TableActionIcons";

const FilmRatingTableBody = () => {
	const { filmRatingStore } = useApiStore();
	const [id, setId] = useState("");
	const [modalMessage, setModalMessage] = useState("");
	const [notificationSuccessMessage, setNotificationSuccessMessage] = useState<ReactNode>();
	const [deleteModalOpen, setDeleteModalOpen] = useState(false);

	return (
		<>
			<DeleteModal
				id={id}
				isOpen={deleteModalOpen}
				message={modalMessage}
				notificationSuccessMessage={notificationSuccessMessage}
				store={filmRatingStore}
				toggle={() => {
					setDeleteModalOpen(!deleteModalOpen);
				}}
			/>

			{filmRatingStore.responses?.data.map((filmRating) => {
				return (
					<tr key={filmRating.id}>
						<td>
							<Text pr="2rem" weight={500}>
								{filmRating.type}
							</Text>
						</td>
						{filmRatingStore.showDescription && (
							<td>
								<Text>{filmRating.description}</Text>
							</td>
						)}
						<TableActionIcons
							location={`${Routes.AdminFilmRatingEdit}/${filmRating.id}`}
							onDeleteClick={() => {
								runInAction(() => {
									setId(filmRating.id);
									setModalMessage(`Are you sure you want to delete film rating ${filmRating.type}?`);
									setNotificationSuccessMessage(
										`Film rating '${filmRating.type}' deleted successfully.`
									);
								});
								setDeleteModalOpen(!deleteModalOpen);
							}}
						/>
					</tr>
				);
			})}
		</>
	);
};

export default observer(FilmRatingTableBody);
