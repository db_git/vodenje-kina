//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { observer } from "mobx-react-lite";
import { useNavigate } from "react-router-dom";

import useApiStore from "../../../../hooks/useApiStore";
import { FilmRatingOrderBy } from "../../../../types/enums/OrderBy";
import TableHeaderSort from "../../utils/TableHeaderSort";
import { changeFilmRatingTypeSort } from "../utils/changeFilmRatingSort";

const FilmRatingTypeTableHeader = () => {
	const url = useNavigate();
	const { filmRatingStore } = useApiStore();

	const handleFilmRatingTypeSort = (): void => {
		changeFilmRatingTypeSort(filmRatingStore, url);
	};

	return (
		<TableHeaderSort
			ascending={filmRatingStore.queryParameters.orderBy === FilmRatingOrderBy.TypeAscending}
			descending={filmRatingStore.queryParameters.orderBy === FilmRatingOrderBy.TypeDescending}
			key="type"
			onClick={handleFilmRatingTypeSort}
		>
			Type
		</TableHeaderSort>
	);
};

export default observer(FilmRatingTypeTableHeader);
