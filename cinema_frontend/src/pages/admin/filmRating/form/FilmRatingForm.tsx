//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Grid } from "@mantine/core";
import { observer } from "mobx-react-lite";

import Form from "../../../../components/forms/Form";
import Loading from "../../../../components/status/Loading";
import useApiStore from "../../../../hooks/useApiStore";
import { filmRatingFormSchema } from "../../../../schemas/adminSchemas";
import { type AdminFormProps } from "../../admin";
import { type FilmRatingFormInputValues, type FilmRatingFormSubmitValues } from "../filmRating";

import FilmRatingDescriptionInput from "./FilmRatingDescriptionInput";
import FilmRatingTypeInput from "./FilmRatingTypeInput";

const FilmRatingForm = ({ id }: AdminFormProps) => {
	const { filmRatingStore } = useApiStore();

	const handleSubmit = (values: FilmRatingFormSubmitValues): void => {
		if (id) {
			void filmRatingStore.put(id, values);
		} else {
			void filmRatingStore.post(values);
		}
	};

	if (filmRatingStore.loading.get || (id && !filmRatingStore.response)) return <Loading />;

	const initialValues: FilmRatingFormInputValues = {
		type: filmRatingStore.response?.type ?? "",
		description: filmRatingStore.response?.description ?? ""
	};

	return (
		<Form id="filmRatingForm" initialValues={initialValues} onSubmit={handleSubmit} schema={filmRatingFormSchema}>
			{(form) => {
				return (
					<Grid>
						<Grid.Col span={12}>
							<FilmRatingTypeInput form={form} />
						</Grid.Col>
						<Grid.Col span={12}>
							<FilmRatingDescriptionInput form={form} />
						</Grid.Col>
					</Grid>
				);
			}}
		</Form>
	);
};

export default observer(FilmRatingForm);
