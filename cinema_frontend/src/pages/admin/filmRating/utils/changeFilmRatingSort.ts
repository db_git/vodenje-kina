//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { runInAction } from "mobx";
import { type NavigateFunction } from "react-router-dom";

import { Routes } from "../../../../routes/Routes";
import { type IFilmRatingApiStore } from "../../../../stores/stores";
import { FilmRatingOrderBy } from "../../../../types/enums/OrderBy";
import { QueryParameterOptions } from "../../../../types/enums/Stores";

const changeFilmRatingTypeSort = (filmRatingStore: IFilmRatingApiStore, url: NavigateFunction): void => {
	runInAction(() => {
		filmRatingStore.setQueryParameters(
			QueryParameterOptions.OrderBy,
			(filmRatingStore.queryParameters.orderBy % FilmRatingOrderBy.TypeDescending) +
				FilmRatingOrderBy.TypeAscending
		);
		filmRatingStore.modifyUrl(Routes.AdminFilmRating, url);
		void filmRatingStore.getAll();
	});
};

export { changeFilmRatingTypeSort };
