//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Group, Paper, Select, Stack, Table, useMantineTheme } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback, useEffect } from "react";
import { AiOutlineNumber } from "react-icons/ai";
import { useLocation, useNavigate } from "react-router-dom";

import Pagination from "../../components/pagination/Pagination";
import Loading from "../../components/status/Loading";
import NoContent from "../../components/status/NoContent";
import ServerError from "../../components/status/ServerError";
import { QueryParameterOptions } from "../../types/enums/Stores";
import { type QueryParameters } from "../../types/query";
import { is5xxStatus, isNotFoundOrNoContent } from "../../utils/isStatus";

import AdminStyle from "./Admin.module.scss";
import CreateButton from "./CreateButton";
import Search from "./Search";
import { type AdminEntityBaseProps } from "./admin";

const pageSizes = ["5", "10", "15", "20", "30", "50"];

const AdminEntityBase = <
	TDto,
	TQuery extends QueryParameters,
	TIndividualDto,
	TCreateDto = Record<string, unknown>,
	TUpdateDto = Record<string, unknown>
>({
	store,
	location,
	thead,
	tbody,
	withCreateButton,
	children
}: AdminEntityBaseProps<TDto, TQuery, TIndividualDto, TCreateDto, TUpdateDto>) => {
	const url = useNavigate();
	const locationSearch = useLocation().search;
	const theme = useMantineTheme();

	const adminTableStyle = AdminStyle[`entity-table-${theme.colorScheme}`];
	const createButtonLocation = `${location}/create`;

	useEffect(() => {
		store.setUrlSearchString(new URLSearchParams(locationSearch));

		runInAction(() => {
			if (!pageSizes.includes(store.queryParameters.size.toString(10))) {
				store.setQueryParameters(QueryParameterOptions.Size, 10);
			}
		});

		store.modifyUrl(location, url);
	}, [location, url, store, locationSearch]);

	useEffect(() => {
		runInAction(() => {
			if (store.responses === undefined) {
				void store.getAll();
			}
		});
	}, [store]);

	const handleSelectOnChange = useCallback(
		(value: string | null): void => {
			runInAction(() => {
				if (value !== null && value !== store.queryParameters.size.toString(10)) {
					store.setQueryParameters(QueryParameterOptions.Page, 1);
					store.setQueryParameters(QueryParameterOptions.Size, Number.parseInt(value, 10));
					store.modifyUrl(location, url);
					void store.getAll();
				}
			});
		},
		[location, store, url]
	);

	const handlePagionationModifyUrl = useCallback((): void => {
		store.modifyUrl(location, url);
		void store.getAll();
	}, [location, store, url]);

	if (is5xxStatus(store.status.getAll)) return <ServerError />;

	return (
		<Stack>
			<Group position="center">
				<Select
					data={pageSizes}
					icon={<AiOutlineNumber />}
					onChange={handleSelectOnChange}
					radius="md"
					value={store.queryParameters.size.toString(10)}
				/>
				<Search location={location} store={store} url={url} />
				{children}
			</Group>

			<Paper className="w-full overflow-auto" radius="md" shadow="md" withBorder>
				<Table className={adminTableStyle} highlightOnHover horizontalSpacing="xl" verticalSpacing="md">
					<thead>{thead}</thead>
					{!store.loading.getAll && !isNotFoundOrNoContent(store.status.getAll) && <tbody>{tbody}</tbody>}
				</Table>
			</Paper>

			{store.loading.getAll && <Loading />}

			{withCreateButton && (
				<Group position="right">
					<CreateButton location={createButtonLocation} />
				</Group>
			)}

			{!store.loading.getAll && isNotFoundOrNoContent(store.status.getAll) && <NoContent />}

			<Pagination modifyUrl={handlePagionationModifyUrl} store={store} />
		</Stack>
	);
};

export default observer(AdminEntityBase);
