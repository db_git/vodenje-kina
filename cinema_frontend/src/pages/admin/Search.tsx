//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Center, TextInput } from "@mantine/core";
import debounce from "lodash.debounce";
import { runInAction } from "mobx";
import { type ChangeEvent, useCallback, useEffect, useRef, useState } from "react";
import { TbSearch } from "react-icons/tb";

import { QueryParameterOptions } from "../../types/enums/Stores";
import { type QueryParameters } from "../../types/query";

import { type SearchProps } from "./admin";

const Search = <
	TDto,
	TQuery extends QueryParameters,
	TIndividualDto = TDto,
	TCreateDto = Record<string, unknown>,
	TUpdateDto = Record<string, unknown>
>({
	store,
	url,
	location,
	center,
	placeholder
}: SearchProps<TDto, TQuery, TIndividualDto, TCreateDto, TUpdateDto>) => {
	const [search, setSearch] = useState("");

	const handleOnSearch = useRef(
		debounce((event: ChangeEvent<HTMLInputElement>) => {
			store.setQueryParameters(QueryParameterOptions.Search, event.target.value);

			if (url && location) store.modifyUrl(location, url);

			void store.getAll();
			event.target.focus();
		}, 500)
	).current;

	const onChange = useCallback(
		(event: ChangeEvent<HTMLInputElement>) => {
			setSearch(event.target.value);
			handleOnSearch.cancel();
			handleOnSearch(event);
		},
		[handleOnSearch]
	);

	useEffect(() => {
		runInAction(() => {
			setSearch(store.queryParameters.search ?? "");
		});
	}, [store]);

	const textInput = (
		<TextInput
			icon={<TbSearch />}
			onChange={onChange}
			placeholder={placeholder ?? "Enter keywords..."}
			radius="md"
			value={search}
		/>
	);

	return center ? <Center>{textInput}</Center> : textInput;
};

export default Search;
