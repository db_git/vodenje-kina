//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { useParams } from "react-router-dom";

import useApiStore from "../../../hooks/useApiStore";
import EditAdminEntityBase from "../EditAdminEntityBase";

import ProjectionForm from "./form/ProjectionForm";

const EditProjection = () => {
	const { id } = useParams();
	const { projectionStore } = useApiStore();

	useEffect(() => {
		runInAction(() => {
			document.title = projectionStore.response
				? `Edit ${projectionStore.response.movie.title} | Admin`
				: "Loading...";
		});
	}, [projectionStore.response]);

	useEffect(() => {
		runInAction(() => {
			if (projectionStore.response === undefined && id) void projectionStore.get(id);
		});
	}, [id, projectionStore]);

	return (
		<EditAdminEntityBase
			formId="projectionForm"
			noContentMessage="Projection does not exist."
			store={projectionStore}
		>
			<ProjectionForm id={id} />
		</EditAdminEntityBase>
	);
};

export default observer(EditProjection);
