//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { observer } from "mobx-react-lite";
import { useNavigate } from "react-router-dom";

import useApiStore from "../../../../hooks/useApiStore";
import { ProjectionOrderBy } from "../../../../types/enums/OrderBy";
import TableHeaderSort from "../../utils/TableHeaderSort";
import { changeProjectionTimeSort } from "../utils/changeProjectionSort";

const ProjectionTimeTableHeader = () => {
	const url = useNavigate();
	const { projectionStore } = useApiStore();

	const handleProjectionTimeSort = (): void => {
		changeProjectionTimeSort(projectionStore, url);
	};

	return (
		<TableHeaderSort
			ascending={projectionStore.queryParameters.orderBy === ProjectionOrderBy.TimeAscending}
			descending={projectionStore.queryParameters.orderBy === ProjectionOrderBy.TimeDescending}
			onClick={handleProjectionTimeSort}
		>
			Time
		</TableHeaderSort>
	);
};

export default observer(ProjectionTimeTableHeader);
