//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Text } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { type ReactNode, useState } from "react";

import useApiStore from "../../../../hooks/useApiStore";
import useLanguage from "../../../../hooks/useLanguage";
import { Routes } from "../../../../routes/Routes";
import DeleteModal from "../../DeleteModal";
import TableActionIcons from "../../utils/TableActionIcons";

const ProjectionTableBody = () => {
	const lang = useLanguage();
	const { projectionStore } = useApiStore();
	const [id, setId] = useState("");
	const [modalMessage, setModalMessage] = useState<JSX.Element>();
	const [notificationSuccessMessage, setNotificationSuccessMessage] = useState<ReactNode>();
	const [deleteModalOpen, setDeleteModalOpen] = useState(false);

	return (
		<>
			<DeleteModal
				id={id}
				isOpen={deleteModalOpen}
				message={modalMessage}
				notificationSuccessMessage={notificationSuccessMessage}
				store={projectionStore}
				toggle={() => {
					setDeleteModalOpen(!deleteModalOpen);
				}}
			/>

			{projectionStore.responses?.data.map((projection) => {
				const time = `${new Date(projection.time).toLocaleDateString(lang)} ${new Date(
					projection.time
				).toLocaleTimeString(lang)}`;
				const hall = `${projection.hall.name} (${projection.hall.cinema.name})`;

				const deleteMessage = (
					<>
						<Text span>Are you sure you want to delete projection for movie&nbsp;</Text>
						<Text span underline>
							{projection.movie.title}
						</Text>
						<Text span>, running in hall&nbsp;</Text>
						<Text span underline>
							{hall}
						</Text>
						<Text span>&nbsp;with scheduled time&nbsp;</Text>
						<Text span underline>
							{time}
						</Text>
						<Text span>?</Text>
					</>
				);

				return (
					<tr key={projection.id}>
						<td>
							<Text pr="2rem" weight={500}>
								{projection.movie.title}
							</Text>
						</td>
						<td>
							<Text pr="1rem">{hall}</Text>
						</td>
						<td>
							<Text pr="1rem" weight={500}>
								{time}
							</Text>
						</td>
						<TableActionIcons
							location={`${Routes.AdminProjectionEdit}/${projection.id}`}
							onDeleteClick={() => {
								runInAction(() => {
									setId(projection.id);
									setModalMessage(deleteMessage);
									setNotificationSuccessMessage(
										`Projection for movie '${projection.movie.title}' with scheduled time '${time}' deleted successfully.`
									);
								});
								setDeleteModalOpen(!deleteModalOpen);
							}}
						/>
					</tr>
				);
			})}
		</>
	);
};

export default observer(ProjectionTableBody);
