//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Grid } from "@mantine/core";
import { DatePicker, TimeInput } from "@mantine/dates";
import { observer } from "mobx-react-lite";
import { BiTime } from "react-icons/bi";
import { BsCalendarDay } from "react-icons/bs";

import useApiStore from "../../../../hooks/useApiStore";
import useLanguage from "../../../../hooks/useLanguage";
import { type ProjectionTimeInputProps } from "../projection";

const ProjectionTimeInput = ({ form }: ProjectionTimeInputProps) => {
	const { projectionStore } = useApiStore();
	const locale = useLanguage();

	const loading = projectionStore.loading.get || projectionStore.loading.post || projectionStore.loading.put;

	return (
		<Grid>
			<Grid.Col md={6} xs={12}>
				<DatePicker
					clearable
					disabled={loading}
					dropdownType="modal"
					icon={<BsCalendarDay />}
					label="Enter date"
					locale={locale}
					required
					{...form.getInputProps("date")}
				/>
			</Grid.Col>

			<Grid.Col md={6} xs={12}>
				<TimeInput
					clearable
					disabled={loading}
					icon={<BiTime />}
					label="Enter time in hours and minutes"
					required
					{...form.getInputProps("time")}
				/>
			</Grid.Col>
		</Grid>
	);
};

export default observer(ProjectionTimeInput);
