//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Select } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback, useEffect, useRef, useState } from "react";
import { BiCameraMovie } from "react-icons/bi";

import SelectItem from "../../../../components/forms/SelectItem";
import useApiStore from "../../../../hooks/useApiStore";
import useGetStore from "../../../../hooks/useGetStore";
import { QueryParameterOptions } from "../../../../types/enums/Stores";
import { type InputSelectOptionsWithPicture } from "../../../../types/forms";
import { type ProjectionMovieInputProps } from "../projection";
import { changeProjectionMovieInputSearch } from "../utils/changeProjectionSearch";

const ProjectionMovieInput = ({ form }: ProjectionMovieInputProps) => {
	const { movieGetStore } = useGetStore();
	const { projectionStore } = useApiStore();

	const changeMovieSearch = useRef(changeProjectionMovieInputSearch).current;
	const [movieSelectOptions, setMovieSelectOptions] = useState<InputSelectOptionsWithPicture[]>([]);

	useEffect(() => {
		runInAction(() => {
			movieGetStore.setQueryParameters(QueryParameterOptions.Search, "");
			movieGetStore.setQueryParameters(QueryParameterOptions.Page, 1);
			movieGetStore.setQueryParameters(QueryParameterOptions.Size, 25);
			movieGetStore.setQueryParameters(QueryParameterOptions.OrderBy, 1);

			if (movieGetStore.responses) return;
			void movieGetStore.getAll();
		});

		return () => {
			runInAction(() => {
				movieGetStore.setQueryParameters(QueryParameterOptions.Size, 10);
				movieGetStore.setQueryParameters(QueryParameterOptions.Search, undefined);
				movieGetStore.setResponses(undefined);
			});
		};
	}, [movieGetStore]);

	useEffect(() => {
		let options: InputSelectOptionsWithPicture[] = [];

		runInAction(() => {
			if (movieGetStore.responses) {
				options = movieGetStore.responses.data.map((movieResponse) => {
					return {
						value: movieResponse.id,
						label: movieResponse.title,
						pictureUrl: movieResponse.posterUrl
					};
				});
			}
		});

		setMovieSelectOptions(options);
	}, [movieGetStore.responses]);

	useEffect(() => {
		return () => {
			changeMovieSearch.cancel();
		};
	}, [changeMovieSearch]);

	const handleOnMovieSearch = useCallback(
		(search: string): void => {
			changeMovieSearch(movieGetStore, setMovieSelectOptions, search);
		},
		[changeMovieSearch, movieGetStore]
	);

	return (
		<Select
			data={movieSelectOptions}
			disabled={
				projectionStore.loading.get ||
				projectionStore.loading.post ||
				projectionStore.loading.put ||
				movieGetStore.loading.getAll ||
				movieGetStore.loading.get
			}
			dropdownPosition="bottom"
			icon={<BiCameraMovie />}
			itemComponent={SelectItem}
			label="Movie"
			maxDropdownHeight={576}
			onSearchChange={handleOnMovieSearch}
			placeholder="Select movie"
			required
			searchable
			{...form.getInputProps("movie")}
		/>
	);
};

export default observer(ProjectionMovieInput);
