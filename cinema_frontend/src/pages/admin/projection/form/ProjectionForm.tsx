//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Grid } from "@mantine/core";
import { observer } from "mobx-react-lite";

import Form from "../../../../components/forms/Form";
import Loading from "../../../../components/status/Loading";
import useApiStore from "../../../../hooks/useApiStore";
import { projectionFormSchema } from "../../../../schemas/adminSchemas";
import { type AdminFormProps } from "../../admin";
import { type ProjectionFormInputValues, type ProjectionFormSubmitValues } from "../projection";

import ProjectionHallInput from "./ProjectionHallInput";
import ProjectionMovieInput from "./ProjectionMovieInput";
import ProjectionTimeInput from "./ProjectionTimeInput";

const ProjectionForm = ({ id }: AdminFormProps) => {
	const { projectionStore } = useApiStore();

	const handleSubmit = (values: ProjectionFormInputValues): void => {
		if (!values.date || !values.time) return;

		const time = new Date(values.time);
		const date = new Date(values.date);
		date.setHours(time.getHours());
		date.setMinutes(time.getMinutes());

		const newValues: ProjectionFormSubmitValues = {
			movie: values.movie,
			hall: values.hall,
			time: `${date.toISOString().slice(0, -5)}Z`
		};

		if (id) {
			void projectionStore.put(id, newValues);
		} else {
			void projectionStore.post(newValues);
		}
	};

	if (projectionStore.loading.get || (id && !projectionStore.response)) return <Loading />;

	const initialValues: ProjectionFormInputValues = {
		movie: projectionStore.response?.movie.id ?? "",
		hall: projectionStore.response?.hall.id ?? "",
		date: projectionStore.response?.time ? new Date(projectionStore.response?.time) : undefined,
		time: projectionStore.response?.time ? new Date(projectionStore.response?.time) : undefined
	};

	return (
		<Form id="projectionForm" initialValues={initialValues} onSubmit={handleSubmit} schema={projectionFormSchema}>
			{(form) => {
				return (
					<Grid>
						<Grid.Col span={12}>
							<ProjectionMovieInput form={form} />
						</Grid.Col>

						<Grid.Col span={12}>
							<ProjectionHallInput form={form} />
						</Grid.Col>

						<Grid.Col span={12}>
							<ProjectionTimeInput form={form} />
						</Grid.Col>
					</Grid>
				);
			}}
		</Form>
	);
};

export default observer(ProjectionForm);
