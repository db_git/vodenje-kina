//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Modal, Stack, Text, useMantineTheme } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback, useEffect, useState } from "react";
import { MdFilterList } from "react-icons/md";
import { useLocation, useNavigate } from "react-router-dom";

import useApiStore from "../../../../hooks/useApiStore";
import { Routes } from "../../../../routes/Routes";
import { QueryParameterOptions } from "../../../../types/enums/Stores";

import ProjectionDateRangeFilter from "./ProjectionDateRangeFilter";
import ProjectionHallFilter from "./ProjectionHallFilter";
import ProjectionMovieFilter from "./ProjectionMovieFilter";

const ProjectionFilter = () => {
	const url = useNavigate();
	const locationSearch = useLocation().search;
	const { projectionStore } = useApiStore();
	const theme = useMantineTheme();
	const [opened, setOpened] = useState(false);

	useEffect(() => {
		runInAction(() => {
			projectionStore.setUrlSearchString(new URLSearchParams(locationSearch));
			projectionStore.modifyUrl(Routes.AdminProjection, url);
		});
	}, [url, projectionStore, locationSearch]);

	const toggleModal = useCallback(() => {
		setOpened(!opened);
	}, [opened]);

	const applyFilters = useCallback(() => {
		runInAction(() => {
			projectionStore.setProjectionQueryParameters(QueryParameterOptions.Page, 1);
			projectionStore.modifyUrl(Routes.AdminProjection, url);
		});
		void projectionStore.getAll();
	}, [projectionStore, url]);

	return (
		<>
			<Modal
				centered
				onClose={toggleModal}
				opened={opened}
				overlayBlur={3}
				overlayColor={theme.colorScheme === "dark" ? theme.colors.dark[9] : theme.colors.gray[2]}
				overlayOpacity={0.55}
				title="Projection filter"
			>
				<Stack>
					<ProjectionMovieFilter />
					<ProjectionHallFilter />
					<ProjectionDateRangeFilter />

					<Button
						color="cyan"
						loading={projectionStore.loading.getAll}
						onClick={applyFilters}
						radius={6}
						size="md"
					>
						Search
					</Button>
				</Stack>
			</Modal>

			<Button onClick={toggleModal} radius="md" rightIcon={<MdFilterList />} variant="default">
				<Text>Filter</Text>
			</Button>
		</>
	);
};

export default observer(ProjectionFilter);
