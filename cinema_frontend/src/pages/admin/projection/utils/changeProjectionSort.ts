//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { runInAction } from "mobx";
import { type NavigateFunction } from "react-router-dom";

import { Routes } from "../../../../routes/Routes";
import { type IProjectionApiStore } from "../../../../stores/stores";
import { ProjectionOrderBy } from "../../../../types/enums/OrderBy";
import { QueryParameterOptions } from "../../../../types/enums/Stores";

const changeProjectionMovieSort = (projectionStore: IProjectionApiStore, url: NavigateFunction): void => {
	runInAction(() => {
		if (
			projectionStore.queryParameters.orderBy === ProjectionOrderBy.MovieTitleAscending ||
			projectionStore.queryParameters.orderBy === ProjectionOrderBy.MovieTitleDescending
		) {
			projectionStore.setQueryParameters(
				QueryParameterOptions.OrderBy,
				(projectionStore.queryParameters.orderBy % ProjectionOrderBy.MovieTitleDescending) +
					ProjectionOrderBy.MovieTitleAscending
			);
		} else {
			projectionStore.setQueryParameters(QueryParameterOptions.OrderBy, ProjectionOrderBy.MovieTitleAscending);
		}

		projectionStore.modifyUrl(Routes.AdminProjection, url);
		void projectionStore.getAll();
	});
};

const changeProjectionHallSort = (projectionStore: IProjectionApiStore, url: NavigateFunction): void => {
	runInAction(() => {
		if (
			projectionStore.queryParameters.orderBy === ProjectionOrderBy.HallNameAscending ||
			projectionStore.queryParameters.orderBy === ProjectionOrderBy.HallNameDescending
		) {
			projectionStore.setQueryParameters(
				QueryParameterOptions.OrderBy,
				(projectionStore.queryParameters.orderBy % (ProjectionOrderBy.HallNameDescending / 2)) +
					ProjectionOrderBy.HallNameAscending
			);
		} else {
			projectionStore.setQueryParameters(QueryParameterOptions.OrderBy, ProjectionOrderBy.HallNameAscending);
		}

		projectionStore.modifyUrl(Routes.AdminProjection, url);
		void projectionStore.getAll();
	});
};

const changeProjectionTimeSort = (projectionStore: IProjectionApiStore, url: NavigateFunction): void => {
	runInAction(() => {
		if (
			projectionStore.queryParameters.orderBy === ProjectionOrderBy.TimeAscending ||
			projectionStore.queryParameters.orderBy === ProjectionOrderBy.TimeDescending
		) {
			projectionStore.setQueryParameters(
				QueryParameterOptions.OrderBy,
				(projectionStore.queryParameters.orderBy % (ProjectionOrderBy.TimeDescending / 3)) +
					ProjectionOrderBy.TimeAscending
			);
		} else {
			projectionStore.setQueryParameters(QueryParameterOptions.OrderBy, ProjectionOrderBy.TimeAscending);
		}

		projectionStore.modifyUrl(Routes.AdminProjection, url);
		void projectionStore.getAll();
	});
};

export { changeProjectionMovieSort, changeProjectionHallSort, changeProjectionTimeSort };
