//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import debounce from "lodash.debounce";
import { runInAction } from "mobx";
import { type Dispatch, type SetStateAction } from "react";

import { type IHallGetStore, type IMovieGetStore } from "../../../../stores/stores";
import { QueryParameterOptions } from "../../../../types/enums/Stores";
import { type InputSelectOptions, type InputSelectOptionsWithPicture } from "../../../../types/forms";

const changeProjectionHallInputSearch = debounce(
	(store: IHallGetStore, setHalls: Dispatch<SetStateAction<InputSelectOptions[]>>, search: string) => {
		runInAction(() => {
			if (store.queryParameters.search === search) return;

			store.setQueryParameters(QueryParameterOptions.Search, search);
			void store.getAll();

			let hallOptions: InputSelectOptions[] = [];
			if (store.responses) {
				hallOptions = store.responses.data.map((hallResponse) => {
					return { value: hallResponse.id, label: hallResponse.name };
				});
			}

			setHalls(hallOptions);
		});
	},
	500
);

const changeProjectionMovieInputSearch = debounce(
	(store: IMovieGetStore, setMovies: Dispatch<SetStateAction<InputSelectOptionsWithPicture[]>>, search: string) => {
		runInAction(() => {
			if (store.queryParameters.search === search) return;

			store.setQueryParameters(QueryParameterOptions.Search, search);
			void store.getAll();

			let movieOptions: InputSelectOptionsWithPicture[] = [];
			if (store.responses) {
				movieOptions = store.responses.data.map((movieResponse) => {
					return {
						value: movieResponse.id,
						label: movieResponse.title,
						pictureUrl: movieResponse.posterUrl
					};
				});
			}

			setMovies(movieOptions);
		});
	},
	500
);

export { changeProjectionHallInputSearch, changeProjectionMovieInputSearch };
