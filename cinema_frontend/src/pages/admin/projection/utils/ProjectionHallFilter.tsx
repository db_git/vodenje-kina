//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Select } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback, useEffect, useRef, useState } from "react";
import { FaTheaterMasks } from "react-icons/fa";

import useApiStore from "../../../../hooks/useApiStore";
import useGetStore from "../../../../hooks/useGetStore";
import { ProjectionQueryParameterOptions, QueryParameterOptions } from "../../../../types/enums/Stores";
import { type InputSelectOptions } from "../../../../types/forms";

import { changeProjectionHallInputSearch } from "./changeProjectionSearch";

const ProjectionHallFilter = () => {
	const { hallGetStore } = useGetStore();
	const { projectionStore } = useApiStore();

	const changeHallSearch = useRef(changeProjectionHallInputSearch).current;
	const [hallSelectOptions, setHallSelectOptions] = useState<InputSelectOptions[]>([]);

	useEffect(() => {
		runInAction(() => {
			hallGetStore.setQueryParameters(QueryParameterOptions.Search, "");
			hallGetStore.setQueryParameters(QueryParameterOptions.Page, 1);
			hallGetStore.setQueryParameters(QueryParameterOptions.Size, 25);
			hallGetStore.setQueryParameters(QueryParameterOptions.OrderBy, 1);

			if (hallGetStore.responses) return;
			void hallGetStore.getAll();
		});

		return () => {
			runInAction(() => {
				hallGetStore.setQueryParameters(QueryParameterOptions.Size, 10);
				hallGetStore.setQueryParameters(QueryParameterOptions.Search, undefined);
				hallGetStore.setResponses(undefined);
			});
		};
	}, [hallGetStore]);

	useEffect(() => {
		let hallOptions: InputSelectOptions[] = [];

		runInAction(() => {
			if (hallGetStore.responses) {
				hallOptions = hallGetStore.responses.data.map((hallResponse) => {
					return { value: hallResponse.id, label: `${hallResponse.name} (${hallResponse.cinema.name})` };
				});
			}
		});

		setHallSelectOptions(hallOptions);
	}, [hallGetStore.responses]);

	const handleOnHallSearch = useCallback(
		(search: string): void => {
			changeHallSearch(hallGetStore, setHallSelectOptions, search);
		},
		[changeHallSearch, hallGetStore]
	);

	const handleOnChange = useCallback(
		(value: string | null) => {
			runInAction(() => {
				projectionStore.setProjectionQueryParameters(ProjectionQueryParameterOptions.Hall, value ?? undefined);
			});
		},
		[projectionStore]
	);

	return (
		<Select
			clearable
			data={hallSelectOptions}
			disabled={
				projectionStore.loading.get ||
				projectionStore.loading.post ||
				projectionStore.loading.put ||
				hallGetStore.loading.getAll ||
				hallGetStore.loading.get
			}
			dropdownPosition="bottom"
			icon={<FaTheaterMasks />}
			label="Hall"
			onChange={handleOnChange}
			onSearchChange={handleOnHallSearch}
			searchable
			value={projectionStore.queryParameters.hall ?? ""}
		/>
	);
};

export default observer(ProjectionHallFilter);
