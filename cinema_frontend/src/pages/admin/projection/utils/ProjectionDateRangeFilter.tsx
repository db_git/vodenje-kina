//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { DateRangePicker } from "@mantine/dates";
import dayjs from "dayjs";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback } from "react";
import { BsCalendarDay } from "react-icons/bs";

import useApiStore from "../../../../hooks/useApiStore";
import useLanguage from "../../../../hooks/useLanguage";
import { ProjectionQueryParameterOptions } from "../../../../types/enums/Stores";

const ProjectionDateRangeFilter = () => {
	const { projectionStore } = useApiStore();
	const locale = useLanguage();

	const handleOnChange = useCallback(
		(range: [Date | null, Date | null]) => {
			runInAction(() => {
				const [start, end] = range;

				if (start) {
					projectionStore.setProjectionQueryParameters(
						ProjectionQueryParameterOptions.StartDate,
						dayjs(start).format("YYYY-MM-DD")
					);
				} else {
					projectionStore.setProjectionQueryParameters(ProjectionQueryParameterOptions.StartDate, undefined);
				}

				if (end) {
					projectionStore.setProjectionQueryParameters(
						ProjectionQueryParameterOptions.EndDate,
						dayjs(end).format("YYYY-MM-DD")
					);
				} else {
					projectionStore.setProjectionQueryParameters(ProjectionQueryParameterOptions.EndDate, undefined);
				}
			});
		},
		[projectionStore]
	);

	return (
		<DateRangePicker
			clearable
			icon={<BsCalendarDay />}
			label="Date range"
			locale={locale}
			onChange={handleOnChange}
			value={[
				projectionStore.queryParameters.startDate ? new Date(projectionStore.queryParameters.startDate) : null,
				projectionStore.queryParameters.endDate ? new Date(projectionStore.queryParameters.endDate) : null
			]}
		/>
	);
};

export default observer(ProjectionDateRangeFilter);
