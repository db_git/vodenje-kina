//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { TextInput } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { MdTitle } from "react-icons/md";

import useApiStore from "../../../../hooks/useApiStore";
import { type GenreNameInputProps } from "../genre";

const GenreNameInput = ({ form }: GenreNameInputProps) => {
	const { genreStore } = useApiStore();

	return (
		<TextInput
			disabled={genreStore.loading.get || genreStore.loading.post || genreStore.loading.put}
			icon={<MdTitle />}
			label="Name"
			placeholder="Enter genre name"
			required
			{...form.getInputProps("name")}
		/>
	);
};

export default observer(GenreNameInput);
