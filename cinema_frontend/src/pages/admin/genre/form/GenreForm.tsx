//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { observer } from "mobx-react-lite";

import Form from "../../../../components/forms/Form";
import Loading from "../../../../components/status/Loading";
import useApiStore from "../../../../hooks/useApiStore";
import { genreFormSchema } from "../../../../schemas/adminSchemas";
import { type AdminFormProps } from "../../admin";
import { type GenreFormInputValues, type GenreFormSubmitValues } from "../genre";

import GenreNameInput from "./GenreNameInput";

const GenreForm = ({ id }: AdminFormProps) => {
	const { genreStore } = useApiStore();

	const handleSubmit = (values: GenreFormSubmitValues): void => {
		if (id) {
			void genreStore.put(id, values);
		} else {
			void genreStore.post(values);
		}
	};

	if (genreStore.loading.get || (id && !genreStore.response)) return <Loading />;

	const initialValues: GenreFormInputValues = {
		name: genreStore.response?.name ?? ""
	};

	return (
		<Form id="genreForm" initialValues={initialValues} onSubmit={handleSubmit} schema={genreFormSchema}>
			{(form) => {
				return <GenreNameInput form={form} />;
			}}
		</Form>
	);
};

export default observer(GenreForm);
