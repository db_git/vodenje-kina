//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Avatar, Badge, Text } from "@mantine/core";
import { observer } from "mobx-react-lite";

import useApiStore from "../../../../hooks/useApiStore";

const UserTableBody = () => {
	const { userStore } = useApiStore();

	return (
		<>
			{userStore.responses?.data.map((user) => {
				return (
					<tr key={user.id}>
						{userStore.showImage && (
							<td>
								<Avatar radius="xl" src={user.imageUrl} />
							</td>
						)}
						<td>
							<Text pr="1rem" weight={500}>
								{user.email}
							</Text>
						</td>
						<td>
							{user.roles.map((role) => {
								return (
									<Badge
										color={role.toUpperCase().includes("ADMIN") ? "red" : "green"}
										key={`${user.id}-${role}`}
										m="0.25rem"
										variant="outline"
									>
										{role}
									</Badge>
								);
							})}
						</td>
					</tr>
				);
			})}
		</>
	);
};

export default observer(UserTableBody);
