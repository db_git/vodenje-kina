//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Center, Container, PasswordInput, Stack } from "@mantine/core";
import { useForm, zodResolver } from "@mantine/form";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback, useEffect } from "react";
import { Navigate, useLocation } from "react-router-dom";

import { ReactComponent as ResetPasswordSvg } from "../../assets/illustrations/ResetPassword.svg";
import NotFound from "../../components/status/NotFound";
import ServerError from "../../components/status/ServerError";
import useApiStore from "../../hooks/useApiStore";
import useNavbarStore from "../../hooks/useNavbarStore";
import { Routes } from "../../routes/Routes";
import { passwordSchema } from "../../schemas/accountSchemas";
import { is2xxStatus, is5xxStatus } from "../../utils/isStatus";

const illustrationStyle = "h-full w-full max-w-sm pb-5";

const ResetPassword = () => {
	const { authStore } = useApiStore();
	const navbarStore = useNavbarStore();

	const query = useLocation().search;
	const url = new URLSearchParams(query);

	const userEmail = url.get("userEmail");
	const passwordResetCode = url.get("passwordResetCode");
	const showNotFound = !userEmail || !passwordResetCode;

	useEffect(() => {
		if (!showNotFound) document.title = "Reset password";
		runInAction(() => {
			navbarStore.setActive("*");
		});
	}, [navbarStore, showNotFound]);

	const form = useForm<{ password: string | null }>({
		initialValues: {
			password: ""
		},
		validateInputOnBlur: true,
		validateInputOnChange: true,
		validate: zodResolver(passwordSchema)
	});

	const handleOnSubmit = useCallback(() => {
		if (!form.values.password || form.errors.password) return;

		runInAction(() => {
			if (userEmail && passwordResetCode && form.values.password)
				void authStore.resetPassword(userEmail, form.values.password, passwordResetCode);
		});
	}, [form.values.password, form.errors.password, userEmail, passwordResetCode, authStore]);

	if (showNotFound) return <NotFound />;

	if (is5xxStatus(authStore.status)) return <ServerError />;
	if (authStore.passwordIsReset && is2xxStatus(authStore.status)) {
		return <Navigate to={Routes.Home} />;
	}

	return (
		<Container size="xs">
			<Stack>
				<Center>
					<ResetPasswordSvg className={illustrationStyle} />
				</Center>

				<PasswordInput
					className="break-all"
					disabled={authStore.loading}
					label="New password"
					required
					{...form.getInputProps("password")}
				/>
				<Button loading={authStore.loading} onClick={handleOnSubmit}>
					Change password
				</Button>
			</Stack>
		</Container>
	);
};

export default observer(ResetPassword);
