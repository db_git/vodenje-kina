//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Card, CloseButton, Divider, Flex, Grid, Group, Stack, Text, Title } from "@mantine/core";
import dayjs from "dayjs";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback } from "react";
import { BiTime } from "react-icons/bi";
import { GiFilmProjector, GiTheater } from "react-icons/gi";
import { MdOutlineLocationOn } from "react-icons/md";

import Heading from "../../../components/display/Heading";
import HorizontalScroll from "../../../components/display/HorizontalScroll";
import useCartStore from "../../../hooks/useCartStore";
import { useCurrencyFormatter } from "../../../utils/converters";
import MovieBackgroundImage from "../../movieDetails/components/MovieBackgroundImage";
import MoviePaper from "../../movieDetails/components/MoviePaper";
import OrderedGoodCard from "../../movieDetails/components/projections/OrderedGoodCard";
import { type OrderCardProps } from "../checkout";
import { calculateOrderPrice } from "../utils/calculatePrice";

const OrderCard = ({ order, index }: OrderCardProps) => {
	const cartStore = useCartStore();
	const currencyFormatter = useCurrencyFormatter();

	const orderPrice = calculateOrderPrice(order);

	const onDelete = useCallback(() => {
		runInAction(() => {
			cartStore.removeOrder(index);
		});
	}, [cartStore, index]);

	return (
		<Grid columns={24}>
			<Grid.Col span={2}>
				<Text fw={700}>{index + 1}.</Text>
			</Grid.Col>

			<Grid.Col span={21}>
				<Card p={0} radius="md" withBorder>
					<Card.Section>
						<MovieBackgroundImage movie={order.projection.movie}>
							<MoviePaper mih="10rem" radius={0}>
								<Stack align="center" spacing="xl">
									<Title mt="3rem" order={2}>
										{order.projection.movie.title}
									</Title>

									<Stack mb="3rem" spacing={0}>
										<Flex
											align="center"
											direction="row"
											gap="md"
											justify="start"
											mb="md"
											wrap="nowrap"
										>
											<BiTime size={21} />
											<Text className="text-base sm:text-lg" fw={500}>
												{dayjs(order.projection.time).format("LLLL")}
											</Text>
										</Flex>
										<Flex
											align="center"
											direction="row"
											gap="md"
											justify="start"
											mb="md"
											wrap="nowrap"
										>
											<GiTheater size={21} />
											<Text className="text-base sm:text-lg">{order.projection.hall.name}</Text>
										</Flex>
										<Flex
											align="center"
											direction="row"
											gap="md"
											justify="start"
											mb="md"
											wrap="nowrap"
										>
											<GiFilmProjector size={21} />
											<Text className="text-base sm:text-lg">
												{order.projection.hall.cinema.name}
											</Text>
										</Flex>
										<Flex
											align="center"
											direction="row"
											gap="md"
											justify="start"
											mb="md"
											wrap="nowrap"
										>
											<MdOutlineLocationOn size={21} />
											<Text className="text-base sm:text-lg">
												{order.projection.hall.cinema.city},&nbsp;
												{order.projection.hall.cinema.street}
											</Text>
										</Flex>
									</Stack>
								</Stack>
							</MoviePaper>
						</MovieBackgroundImage>
					</Card.Section>

					<Stack mb="1rem" px={10}>
						{order.foods && order.foods.length > 0 && (
							<>
								<Heading className="mt-4" size={3}>
									Foods
								</Heading>
								<HorizontalScroll slidesToScroll={1} withControls>
									{order.foods.map((selectedFood) => {
										return (
											<HorizontalScroll.Item key={selectedFood.food.id}>
												<OrderedGoodCard
													imageUrl={selectedFood.food.imageUrl}
													name={selectedFood.food.name}
													quantity={selectedFood.quantity}
												/>
											</HorizontalScroll.Item>
										);
									})}
								</HorizontalScroll>
							</>
						)}
						{order.souvenirs && order.souvenirs.length > 0 && (
							<>
								<Heading className="mt-4" size={3}>
									Souvenirs
								</Heading>
								<HorizontalScroll slidesToScroll={1} withControls>
									{order.souvenirs.map((selectedSouvenir) => {
										return (
											<HorizontalScroll.Item key={selectedSouvenir.souvenir.id}>
												<OrderedGoodCard
													imageUrl={selectedSouvenir.souvenir.imageUrl}
													name={selectedSouvenir.souvenir.name}
													quantity={selectedSouvenir.quantity}
												/>
											</HorizontalScroll.Item>
										);
									})}
								</HorizontalScroll>
							</>
						)}

						<Heading className="mt-4" size={3}>
							Seats
						</Heading>
						<Flex align="center" direction="row" gap="xs" justify="flex-start" wrap="wrap">
							{order.seats
								.map((seat) => {
									return seat;
								})
								.sort((s1, s2) => {
									return s1.number - s2.number;
								})
								.map((seat) => {
									return (
										<Button color="blue" compact key={seat.id} radius="xl" variant="filled">
											{seat.number}
										</Button>
									);
								})}
						</Flex>

						<Heading className="mt-4" size={3}>
							Price
						</Heading>
						<Text fw={500} size="lg">
							{currencyFormatter.format(orderPrice)}
						</Text>
					</Stack>

					<Group align="center" mb="0.5rem" ml="0.5rem" position="left">
						<Button className="block md:hidden" color="red" onClick={onDelete} size="xs" variant="filled">
							Remove
						</Button>
					</Group>
				</Card>
			</Grid.Col>

			<Grid.Col className="hidden md:block" span={1}>
				<CloseButton color="red" onClick={onDelete} variant="filled" />
			</Grid.Col>

			<Grid.Col span={24}>
				<Divider />
			</Grid.Col>
		</Grid>
	);
};

export default observer(OrderCard);
