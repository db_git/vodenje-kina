//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Button, Container, Group, Text } from "@mantine/core";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useCallback, useEffect } from "react";

import Heading from "../../components/display/Heading";
import useApiStore from "../../hooks/useApiStore";
import useCartStore from "../../hooks/useCartStore";
import useNavbarStore from "../../hooks/useNavbarStore";
import { Routes } from "../../routes/Routes";
import { useCurrencyFormatter } from "../../utils/converters";
import hashCode from "../../utils/hashCode";
import { is2xxStatus } from "../../utils/isStatus";

import OrderCard from "./components/OrderCard";
import { calculateTotalPrice } from "./utils/calculatePrice";

const Checkout = () => {
	const cartStore = useCartStore();
	const navbarStore = useNavbarStore();
	const { reservationStore } = useApiStore();
	const currencyFormatter = useCurrencyFormatter();

	useEffect(() => {
		document.title = "Checkout";
		runInAction(() => {
			navbarStore.setActive(Routes.Checkout);
		});
	}, [navbarStore]);

	const totalPrice = calculateTotalPrice(cartStore.orders);

	const checkout = useCallback(async () => {
		await runInAction(async () => {
			await reservationStore.postRange(cartStore.orders);

			if (is2xxStatus(reservationStore.status.post)) {
				cartStore.clearOrders();
			}
		});
	}, [cartStore, reservationStore]);

	return (
		<Container pr="30px" size="xl">
			<Heading className="mb-6" icon={false} size={2}>
				Checkout
			</Heading>

			{!cartStore.orders.length && <Text>You have no reservations in your cart.</Text>}
			{Boolean(cartStore.orders.length) && (
				<>
					{cartStore.orders.map((order, index) => {
						return <OrderCard index={index} key={hashCode(JSON.stringify(order))} order={order} />;
					})}

					<Container mt="0.5rem" size="xl">
						<Group align="center" position="right">
							<Button color="green" loading={reservationStore.loading.post} onClick={checkout}>
								Checkout
							</Button>
							<Text fw={700} size="lg">
								Total:
							</Text>
							<Text fw={500} size="xl">
								{currencyFormatter.format(totalPrice)}
							</Text>
						</Group>
					</Container>
				</>
			)}
		</Container>
	);
};

export default observer(Checkout);
