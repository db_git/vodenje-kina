//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type ColorScheme, ColorSchemeProvider, MantineProvider, type MantineThemeOverride } from "@mantine/core";
import { useLocalStorage } from "@mantine/hooks";
import { memo, useCallback } from "react";

import FontConfig from "./FontConfig";
import { type ThemeProviderProps } from "./theme";

const ThemeProvider = ({ children }: ThemeProviderProps) => {
	const [colorScheme, setColorScheme] = useLocalStorage<ColorScheme>({
		key: "color-scheme",
		defaultValue: "dark"
	});

	const toggleColorScheme = useCallback(
		(currentColorScheme?: ColorScheme) => {
			setColorScheme(currentColorScheme ?? (colorScheme === "dark" ? "light" : "dark"));
		},
		[colorScheme, setColorScheme]
	);

	const theme: MantineThemeOverride = { colorScheme, fontFamily: "Work Sans" };

	return (
		<ColorSchemeProvider colorScheme={colorScheme} toggleColorScheme={toggleColorScheme}>
			<MantineProvider theme={theme} withGlobalStyles withNormalizeCSS>
				<FontConfig />
				{children}
			</MantineProvider>
		</ColorSchemeProvider>
	);
};

export default memo(ThemeProvider);
