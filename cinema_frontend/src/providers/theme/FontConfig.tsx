//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Global } from "@mantine/core";

import Black from "../../assets/fonts/WorkSans-Black.woff2";
import BlackItalic from "../../assets/fonts/WorkSans-BlackItalic.woff2";
import Bold from "../../assets/fonts/WorkSans-Bold.woff2";
import BoldItalic from "../../assets/fonts/WorkSans-BoldItalic.woff2";
import ExtraBold from "../../assets/fonts/WorkSans-ExtraBold.woff2";
import ExtraBoldItalic from "../../assets/fonts/WorkSans-ExtraBoldItalic.woff2";
import ExtraLight from "../../assets/fonts/WorkSans-ExtraLight.woff2";
import ExtraLightItalic from "../../assets/fonts/WorkSans-ExtraLightItalic.woff2";
import Italic from "../../assets/fonts/WorkSans-Italic.woff2";
import Light from "../../assets/fonts/WorkSans-Light.woff2";
import LightItalic from "../../assets/fonts/WorkSans-LightItalic.woff2";
import Medium from "../../assets/fonts/WorkSans-Medium.woff2";
import MediumItalic from "../../assets/fonts/WorkSans-MediumItalic.woff2";
import Regular from "../../assets/fonts/WorkSans-Regular.woff2";
import SemiBold from "../../assets/fonts/WorkSans-SemiBold.woff2";
import SemiBoldItalic from "../../assets/fonts/WorkSans-SemiBoldItalic.woff2";
import Thin from "../../assets/fonts/WorkSans-Thin.woff2";
import ThinItalic from "../../assets/fonts/WorkSans-ThinItalic.woff2";

const fontFamilyName = "Work Sans";

const FontConfig = () => {
	return (
		<Global
			styles={[
				{
					"@font-face": {
						fontFamily: fontFamilyName,
						src: `url('${Thin}') format("woff2")`,
						fontWeight: 100,
						fontStyle: "normal"
					}
				},
				{
					"@font-face": {
						fontFamily: fontFamilyName,
						src: `url('${ThinItalic}') format("woff2")`,
						fontWeight: 100,
						fontStyle: "italic"
					}
				},
				{
					"@font-face": {
						fontFamily: fontFamilyName,
						src: `url('${ExtraLight}') format("woff2")`,
						fontWeight: 200,
						fontStyle: "normal"
					}
				},
				{
					"@font-face": {
						fontFamily: fontFamilyName,
						src: `url('${ExtraLightItalic}') format("woff2")`,
						fontWeight: 200,
						fontStyle: "italic"
					}
				},
				{
					"@font-face": {
						fontFamily: fontFamilyName,
						src: `url('${Light}') format("woff2")`,
						fontWeight: 300,
						fontStyle: "normal"
					}
				},
				{
					"@font-face": {
						fontFamily: fontFamilyName,
						src: `url('${LightItalic}') format("woff2")`,
						fontWeight: 300,
						fontStyle: "italic"
					}
				},
				{
					"@font-face": {
						fontFamily: fontFamilyName,
						src: `url('${Regular}') format("woff2")`,
						fontWeight: 400,
						fontStyle: "normal"
					}
				},
				{
					"@font-face": {
						fontFamily: fontFamilyName,
						src: `url('${Italic}') format("woff2")`,
						fontWeight: 400,
						fontStyle: "italic"
					}
				},
				{
					"@font-face": {
						fontFamily: fontFamilyName,
						src: `url('${Medium}') format("woff2")`,
						fontWeight: 500,
						fontStyle: "normal"
					}
				},
				{
					"@font-face": {
						fontFamily: fontFamilyName,
						src: `url('${MediumItalic}') format("woff2")`,
						fontWeight: 500,
						fontStyle: "italic"
					}
				},
				{
					"@font-face": {
						fontFamily: fontFamilyName,
						src: `url('${SemiBold}') format("woff2")`,
						fontWeight: 600,
						fontStyle: "normal"
					}
				},
				{
					"@font-face": {
						fontFamily: fontFamilyName,
						src: `url('${SemiBoldItalic}') format("woff2")`,
						fontWeight: 600,
						fontStyle: "italic"
					}
				},
				{
					"@font-face": {
						fontFamily: fontFamilyName,
						src: `url('${Bold}') format("woff2")`,
						fontWeight: 700,
						fontStyle: "normal"
					}
				},
				{
					"@font-face": {
						fontFamily: fontFamilyName,
						src: `url('${BoldItalic}') format("woff2")`,
						fontWeight: 700,
						fontStyle: "italic"
					}
				},
				{
					"@font-face": {
						fontFamily: fontFamilyName,
						src: `url('${ExtraBold}') format("woff2")`,
						fontWeight: 800,
						fontStyle: "normal"
					}
				},
				{
					"@font-face": {
						fontFamily: fontFamilyName,
						src: `url('${ExtraBoldItalic}') format("woff2")`,
						fontWeight: 800,
						fontStyle: "italic"
					}
				},
				{
					"@font-face": {
						fontFamily: fontFamilyName,
						src: `url('${Black}') format("woff2")`,
						fontWeight: 900,
						fontStyle: "normal"
					}
				},
				{
					"@font-face": {
						fontFamily: fontFamilyName,
						src: `url('${BlackItalic}') format("woff2")`,
						fontWeight: 900,
						fontStyle: "italic"
					}
				}
			]}
		/>
	);
};

export default FontConfig;
