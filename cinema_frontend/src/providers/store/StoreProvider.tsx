//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { createContext, memo } from "react";

import CartStore from "../../stores/CartStore";
import NavbarStore from "../../stores/NavbarStore";
import AuthStore from "../../stores/api/AuthStore";
import ApiRootStore from "../../stores/common/ApiRootStore";
import GetRootStore from "../../stores/common/GetRootStore";
import { type ICartStore, type IApiRootStore, type IGetRootStore, type INavbarStore } from "../../stores/stores";

import { type StoreProviderProps } from "./store";

const authStore = AuthStore.GetInstance();

const apiRootStore = ApiRootStore.GetInstance(authStore);
const ApiRootStoreContext = createContext<IApiRootStore>(apiRootStore);

const navbarStore = NavbarStore.GetInstance();
const NavbarStoreContext = createContext<INavbarStore>(navbarStore);

const getRootStore = GetRootStore.GetInstance(authStore);
const GetRootStoreContext = createContext<IGetRootStore>(getRootStore);

const cartStore = CartStore.GetInstance(authStore);
const CartStoreContext = createContext<ICartStore>(cartStore);

const StoreProvider = ({ children }: StoreProviderProps) => {
	return (
		<ApiRootStoreContext.Provider value={apiRootStore}>
			<NavbarStoreContext.Provider value={navbarStore}>
				<GetRootStoreContext.Provider value={getRootStore}>
					<CartStoreContext.Provider value={cartStore}>{children}</CartStoreContext.Provider>
				</GetRootStoreContext.Provider>
			</NavbarStoreContext.Provider>
		</ApiRootStoreContext.Provider>
	);
};

export { ApiRootStoreContext, NavbarStoreContext, GetRootStoreContext, CartStoreContext };
export default memo(StoreProvider);
