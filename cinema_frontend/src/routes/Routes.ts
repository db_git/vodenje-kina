//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

export enum Routes {
	AdminDashboard = "/admin",

	AdminCinema = "/admin/cinema",
	AdminCinemaCreate = "/admin/cinema/create",
	AdminCinemaEdit = "/admin/cinema/edit",

	AdminFilmRating = "/admin/filmrating",
	AdminFilmRatingCreate = "/admin/filmrating/create",
	AdminFilmRatingEdit = "/admin/filmrating/edit",

	AdminFood = "/admin/food",
	AdminFoodCreate = "/admin/food/create",
	AdminFoodEdit = "/admin/food/edit",

	AdminGenre = "/admin/genre",
	AdminGenreCreate = "/admin/genre/create",
	AdminGenreEdit = "/admin/genre/edit",

	AdminHall = "/admin/hall",
	AdminHallCreate = "/admin/hall/create",
	AdminHallEdit = "/admin/hall/edit",

	AdminPerson = "/admin/person",
	AdminPersonCreate = "/admin/person/create",
	AdminPersonEdit = "/admin/person/edit",

	AdminProjection = "/admin/projection",
	AdminProjectionCreate = "/admin/projection/create",
	AdminProjectionEdit = "/admin/projection/edit",

	AdminSeat = "/admin/seat",
	AdminSeatCreate = "/admin/seat/create",
	AdminSeatEdit = "/admin/seat/edit",

	AdminSouvenir = "/admin/souvenir",
	AdminSouvenirCreate = "/admin/souvenir/create",
	AdminSouvenirEdit = "/admin/souvenir/edit",

	AdminUser = "/admin/user",
	AdminUserComment = "/admin/comment",
	AdminUserRating = "/admin/rating",

	AdminMovie = "/admin/movie",
	AdminMoviesCreate = "/admin/movie/create",
	AdminMoviesEdit = "/admin/movie/edit",

	Movies = "/movies",
	MovieDetails = "/movie",
	PersonDetails = "/person",

	VerifyEmail = "/verify-email",
	ResetPassword = "/reset-password",
	Home = "/",
	Checkout = "/checkout",
	Account = "/account",
	Registration = "/registration",
	About = "/about"
}
