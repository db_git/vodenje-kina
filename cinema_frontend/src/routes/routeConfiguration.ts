//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type FunctionComponent } from "react";

import NotFound from "../components/status/NotFound";
import About from "../pages/about/About";
import Account from "../pages/account/Account";
import AdminCinema from "../pages/admin/cinema/AdminCinema";
import CreateCinema from "../pages/admin/cinema/CreateCinema";
import EditCinema from "../pages/admin/cinema/EditCinema";
import Admin from "../pages/admin/dashboard/Admin";
import AdminFilmRating from "../pages/admin/filmRating/AdminFilmRating";
import CreateFilmRating from "../pages/admin/filmRating/CreateFilmRating";
import EditFilmRating from "../pages/admin/filmRating/EditFilmRating";
import AdminFood from "../pages/admin/food/AdminFood";
import CreateFood from "../pages/admin/food/CreateFood";
import EditFood from "../pages/admin/food/EditFood";
import AdminGenre from "../pages/admin/genre/AdminGenre";
import CreateGenre from "../pages/admin/genre/CreateGenre";
import EditGenre from "../pages/admin/genre/EditGenre";
import AdminHall from "../pages/admin/hall/AdminHall";
import CreateHall from "../pages/admin/hall/CreateHall";
import EditHall from "../pages/admin/hall/EditHall";
import AdminMovie from "../pages/admin/movie/AdminMovie";
import CreateMovie from "../pages/admin/movie/CreateMovie";
import EditMovie from "../pages/admin/movie/EditMovie";
import AdminPerson from "../pages/admin/person/AdminPerson";
import CreatePerson from "../pages/admin/person/CreatePerson";
import EditPerson from "../pages/admin/person/EditPerson";
import AdminProjection from "../pages/admin/projection/AdminProjection";
import CreateProjection from "../pages/admin/projection/CreateProjection";
import EditProjection from "../pages/admin/projection/EditProjection";
import AdminSeat from "../pages/admin/seat/AdminSeat";
import CreateSeat from "../pages/admin/seat/CreateSeat";
import EditSeat from "../pages/admin/seat/EditSeat";
import AdminSouvenir from "../pages/admin/souvenir/AdminSouvenir";
import CreateSouvenir from "../pages/admin/souvenir/CreateSouvenir";
import EditSouvenir from "../pages/admin/souvenir/EditSouvenir";
import AdminUser from "../pages/admin/user/AdminUser";
import AdminUserComment from "../pages/admin/userComment/AdminUserComment";
import AdminUserRating from "../pages/admin/userRating/AdminUserRating";
import Checkout from "../pages/checkout/Checkout";
import Home from "../pages/home/Home";
import MovieDetails from "../pages/movieDetails/MovieDetails";
import Movies from "../pages/movies/Movies";
import PersonDetails from "../pages/person/PersonDetails";
import Registration from "../pages/registration/Registration";
import ResetPassword from "../pages/resetPassword/ResetPassword";
import VerifyEmail from "../pages/verifyEmail/VerifyEmail";
import { RouteVisibility } from "../types/enums/RouteVisibility";

import { Routes } from "./Routes";

type RouteConfigurationType = {
	readonly path: Routes | string;
	readonly component: FunctionComponent<object>;
	readonly visibility: RouteVisibility;
};

const routeConfiguration: RouteConfigurationType[] = [
	{ path: Routes.Home, component: Home, visibility: RouteVisibility.Everyone },

	{ path: Routes.AdminDashboard, component: Admin, visibility: RouteVisibility.Admin },

	{ path: Routes.AdminCinema, component: AdminCinema, visibility: RouteVisibility.Admin },
	{ path: Routes.AdminCinemaCreate, component: CreateCinema, visibility: RouteVisibility.Admin },
	{ path: Routes.AdminCinemaEdit + "/:id", component: EditCinema, visibility: RouteVisibility.Admin },

	{ path: Routes.AdminFilmRating, component: AdminFilmRating, visibility: RouteVisibility.Admin },
	{ path: Routes.AdminFilmRatingCreate, component: CreateFilmRating, visibility: RouteVisibility.Admin },
	{ path: Routes.AdminFilmRatingEdit + "/:id", component: EditFilmRating, visibility: RouteVisibility.Admin },

	{ path: Routes.AdminFood, component: AdminFood, visibility: RouteVisibility.Admin },
	{ path: Routes.AdminFoodCreate, component: CreateFood, visibility: RouteVisibility.Admin },
	{ path: Routes.AdminFoodEdit + "/:id", component: EditFood, visibility: RouteVisibility.Admin },

	{ path: Routes.AdminGenre, component: AdminGenre, visibility: RouteVisibility.Admin },
	{ path: Routes.AdminGenreCreate, component: CreateGenre, visibility: RouteVisibility.Admin },
	{ path: Routes.AdminGenreEdit + "/:id", component: EditGenre, visibility: RouteVisibility.Admin },

	{ path: Routes.AdminHall, component: AdminHall, visibility: RouteVisibility.Admin },
	{ path: Routes.AdminHallCreate, component: CreateHall, visibility: RouteVisibility.Admin },
	{ path: Routes.AdminHallEdit + "/:id", component: EditHall, visibility: RouteVisibility.Admin },

	{ path: Routes.AdminPerson, component: AdminPerson, visibility: RouteVisibility.Admin },
	{ path: Routes.AdminPersonCreate, component: CreatePerson, visibility: RouteVisibility.Admin },
	{ path: Routes.AdminPersonEdit + "/:id", component: EditPerson, visibility: RouteVisibility.Admin },

	{ path: Routes.AdminProjection, component: AdminProjection, visibility: RouteVisibility.Admin },
	{ path: Routes.AdminProjectionCreate, component: CreateProjection, visibility: RouteVisibility.Admin },
	{ path: Routes.AdminProjectionEdit + "/:id", component: EditProjection, visibility: RouteVisibility.Admin },

	{ path: Routes.AdminSeat, component: AdminSeat, visibility: RouteVisibility.Admin },
	{ path: Routes.AdminSeatCreate, component: CreateSeat, visibility: RouteVisibility.Admin },
	{ path: Routes.AdminSeatEdit + "/:id", component: EditSeat, visibility: RouteVisibility.Admin },

	{ path: Routes.AdminSouvenir, component: AdminSouvenir, visibility: RouteVisibility.Admin },
	{ path: Routes.AdminSouvenirCreate, component: CreateSouvenir, visibility: RouteVisibility.Admin },
	{ path: Routes.AdminSouvenirEdit + "/:id", component: EditSouvenir, visibility: RouteVisibility.Admin },

	{ path: Routes.AdminUser, component: AdminUser, visibility: RouteVisibility.Admin },
	{ path: Routes.AdminUserComment, component: AdminUserComment, visibility: RouteVisibility.Admin },
	{ path: Routes.AdminUserRating, component: AdminUserRating, visibility: RouteVisibility.Admin },

	{ path: Routes.AdminMovie, component: AdminMovie, visibility: RouteVisibility.Admin },
	{ path: Routes.AdminMoviesCreate, component: CreateMovie, visibility: RouteVisibility.Admin },
	{ path: Routes.AdminMoviesEdit + "/:id", component: EditMovie, visibility: RouteVisibility.Admin },

	{ path: Routes.Movies, component: Movies, visibility: RouteVisibility.Everyone },
	{ path: Routes.MovieDetails + "/:id", component: MovieDetails, visibility: RouteVisibility.Everyone },
	{ path: Routes.PersonDetails + "/:id", component: PersonDetails, visibility: RouteVisibility.Everyone },

	{ path: Routes.Checkout, component: Checkout, visibility: RouteVisibility.LoggedIn },
	{ path: Routes.Account, component: Account, visibility: RouteVisibility.LoggedIn },
	{ path: Routes.Registration, component: Registration, visibility: RouteVisibility.Everyone },
	{ path: Routes.About, component: About, visibility: RouteVisibility.Everyone },

	{ path: Routes.VerifyEmail, component: VerifyEmail, visibility: RouteVisibility.LoggedOut },
	{ path: Routes.ResetPassword, component: ResetPassword, visibility: RouteVisibility.Everyone },

	{ path: "*", component: NotFound, visibility: RouteVisibility.Everyone }
];

export default routeConfiguration;
