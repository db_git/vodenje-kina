//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type IAuthStore } from "../stores/stores";
import { RouteVisibility } from "../types/enums/RouteVisibility";

const canShowRoute = (authStore: IAuthStore, visibility: RouteVisibility): boolean => {
	switch (visibility) {
		case RouteVisibility.Everyone: {
			return true;
		}

		case RouteVisibility.LoggedOut: {
			return !authStore.isLoggedIn;
		}

		case RouteVisibility.LoggedIn: {
			return authStore.isLoggedIn;
		}

		case RouteVisibility.Admin: {
			return authStore.isAdmin;
		}

		default: {
			return false;
		}
	}
};

export default canShowRoute;
