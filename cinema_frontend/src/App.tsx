//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

// eslint-disable-next-line import/no-unassigned-import
import "dayjs/locale/en";
// eslint-disable-next-line import/no-unassigned-import
import "dayjs/locale/hr";

import dayjs from "dayjs";
import localeData from "dayjs/plugin/localeData";
import localizedFormat from "dayjs/plugin/localizedFormat";
import { configure } from "mobx";
import React from "react";
import { BrowserRouter } from "react-router-dom";

import Footer from "./components/footer/Footer";
import Navbar from "./components/navbar/Navbar";
import ScrollToTop from "./components/scrollToTop/ScrollToTop";
import useLanguage from "./hooks/useLanguage";
import DisplayRoutes from "./routes/DisplayRoutes";

if (import.meta.env.DEV) {
	configure({
		computedRequiresReaction: true,
		enforceActions: "always",
		observableRequiresReaction: true,
		reactionRequiresObservable: true
	});
}

const App = () => {
	const locale = useLanguage();

	dayjs.locale(locale);
	dayjs.extend(localeData);
	dayjs.extend(localizedFormat);

	return (
		<BrowserRouter>
			<Navbar />

			<div className="relative">
				<DisplayRoutes />
				<Footer />
			</div>

			<ScrollToTop />
		</BrowserRouter>
	);
};

export default App;
