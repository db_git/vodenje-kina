//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Buffer } from "buffer";

const parseJwt = (token: string | null, field?: "name" | "role" | "sub"): string[] | string | null => {
	if (!token) return null;

	const data = JSON.parse(Buffer.from(token.split(".")[1], "base64").toString());

	if (typeof field === "undefined") return data;
	// eslint-disable-next-line security/detect-object-injection
	return data[field];
};

export default parseJwt;
