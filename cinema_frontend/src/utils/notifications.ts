//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { showNotification as showMantineNotification } from "@mantine/notifications";
import { type ReactNode } from "react";
import { AiOutlineDelete } from "react-icons/ai";
import { HiOutlinePencil } from "react-icons/hi";
import { MdErrorOutline, MdOutlineAdd, MdOutlineCheckCircle, MdOutlineWarningAmber } from "react-icons/md";

type NotificationProps = {
	readonly severity: NotificationSeverity;
	readonly title?: string;
	readonly message?: ReactNode;
	readonly icon?: ReactNode;
};

enum NotificationSeverity {
	INFO = "Info",
	WARNING = "Warning",
	ERROR = "Error"
}

const showNotification = ({ title, severity, message, icon }: NotificationProps) => {
	let notificationColor: "blue" | "green" | "red" | "yellow";
	let notificationTitle: string | undefined;
	let notificationIcon: ReactNode;

	switch (severity) {
		case NotificationSeverity.INFO:
			notificationColor = "green";
			notificationTitle = NotificationSeverity.INFO;
			notificationIcon = MdOutlineCheckCircle({});
			break;
		case NotificationSeverity.WARNING:
			notificationColor = "yellow";
			notificationTitle = NotificationSeverity.WARNING;
			notificationIcon = MdOutlineWarningAmber({});
			break;
		case NotificationSeverity.ERROR:
			notificationColor = "red";
			notificationTitle = NotificationSeverity.ERROR;
			notificationIcon = MdErrorOutline({});
			break;
		default:
			notificationColor = "blue";
			notificationTitle = undefined;
			break;
	}

	if (title) notificationTitle = title;
	if (icon) notificationIcon = icon;

	showMantineNotification({
		id: `${severity}-${notificationColor}-${notificationTitle}-${Math.random() * 999_999}`,
		color: notificationColor,
		title: notificationTitle,
		icon: notificationIcon,
		radius: "md",
		message,
		className: "z-50"
	});
};

const showCreatedNotification = (message: ReactNode) => {
	showNotification({
		message,
		title: "Created successfully",
		icon: MdOutlineAdd({}),
		severity: NotificationSeverity.INFO
	});
};

const showUpdatedNotification = (message: ReactNode) => {
	showNotification({
		message,
		title: "Updated successfully",
		icon: HiOutlinePencil({}),
		severity: NotificationSeverity.INFO
	});
};

const showDeletedNotification = (message: ReactNode) => {
	showNotification({
		message,
		title: "Deleted successfully",
		icon: AiOutlineDelete({}),
		severity: NotificationSeverity.INFO
	});
};

export {
	showNotification,
	showCreatedNotification,
	showUpdatedNotification,
	showDeletedNotification,
	NotificationSeverity
};

export type { NotificationProps };
