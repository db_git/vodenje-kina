//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { HttpStatusCode } from "../types/enums/Http";

// noinspection JSUnusedGlobalSymbols
const is1xxStatus = (status: number) => {
	return status >= HttpStatusCode.Continue && status <= HttpStatusCode.EarlyHints;
};

// noinspection JSUnusedGlobalSymbols
const is2xxStatus = (status: number) => {
	return status >= HttpStatusCode.OK && status <= HttpStatusCode.IMUsed;
};

// noinspection JSUnusedGlobalSymbols
const is3xxStatus = (status: number) => {
	return status >= HttpStatusCode.MultipleChoices && status <= HttpStatusCode.PermanentRedirect;
};

const is4xxStatus = (status: number) => {
	return status >= HttpStatusCode.BadRequst && status <= HttpStatusCode.UnavailableForLegalReasons;
};

const is5xxStatus = (status: number) => {
	return status >= HttpStatusCode.InternalServerError && status <= HttpStatusCode.NetworkAuthenticationRequired;
};

const isNotFoundOrNoContent = (status: number) => {
	return status === HttpStatusCode.NotFound || status === HttpStatusCode.NoContent;
};

export { is1xxStatus, is2xxStatus, is3xxStatus, is4xxStatus, is5xxStatus, isNotFoundOrNoContent };
