//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import useLanguage from "../hooks/useLanguage";

const blobToBase64 = async (blob: Blob): Promise<string> => {
	return await new Promise((resolve, reject) => {
		const reader = new FileReader();
		reader.onload = () => {
			resolve((reader.result as string).slice(37));
		};

		reader.readAsDataURL(blob);
		reader.onerror = reject;
	});
};

const useCurrencyFormatter = () => {
	return new Intl.NumberFormat(useLanguage(), {
		style: "currency",
		currency: "EUR",
		minimumFractionDigits: 2,
		maximumFractionDigits: 2
	});
};

const useNumberFormatter = (maxDecimals: number, minDecimals: number = 0) => {
	return new Intl.NumberFormat(useLanguage(), {
		minimumFractionDigits: minDecimals,
		maximumFractionDigits: maxDecimals
	});
};

export { blobToBase64, useCurrencyFormatter, useNumberFormatter };
