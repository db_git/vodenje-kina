//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { z } from "zod";

import { imageUrl, password } from "./commonValidations";

const nameSchema = z
	.string({
		required_error: "Name is required.",
		invalid_type_error: "Name is required."
	})
	.trim()
	.min(1, "Minimum length is 1 character.")
	.max(256, "Maximum length is 256 characters.");

const imageSchema = z.object({
	imageUrl: imageUrl("Image is required.")
});

const passwordSchema = z.object({ password });

const passwordChangeSchema = z.object({
	oldPassword: password,
	newPassword: password
});

export { nameSchema, imageSchema, passwordSchema, passwordChangeSchema };
