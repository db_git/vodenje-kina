//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { z } from "zod";

import {
	firstCharacterUppercaseOrNumber,
	firstLetterUppercase,
	forbidNumbers,
	imageUrl,
	maxNumberLength,
	maxStringLength,
	minNumberLength,
	minStringLength,
	REQUIRED_FIELD,
	requiredNumberField,
	requiredStringField,
	url
} from "./commonValidations";

const DATE_FIELD = "Enter a date.";

// prettier-ignore
const cinemaFormSchema = z.object({
	name: z
		.string()
		.and(firstLetterUppercase)
		.and(minStringLength(2))
		.and(maxStringLength(64))
		.and(requiredStringField),
	city: z
		.string()
		.and(forbidNumbers)
		.and(firstLetterUppercase)
		.and(minStringLength(2))
		.and(maxStringLength(64))
		.and(requiredStringField),
	street: z
		.string()
		.and(firstLetterUppercase)
		.and(minStringLength(2))
		.and(maxStringLength(128))
		.and(requiredStringField),
	ticketPrice: z
		.number()
		.and(minNumberLength(1))
		.and(maxNumberLength(10e9))
		.and(requiredNumberField)
});

// prettier-ignore
const filmRatingFormSchema = z.object({
	type: z
		.string()
		.and(maxStringLength(8))
		.and(requiredStringField),
	description: z
		.string()
		.and(minStringLength(2))
		.and(maxStringLength(128))
		.and(requiredStringField)
});

// prettier-ignore
const souvenirFormSchema = z.object({
	name: z
		.string()
		.and(firstLetterUppercase)
		.and(minStringLength(2))
		.and(maxStringLength(64))
		.and(requiredStringField),
	price: z
		.number()
		.and(minNumberLength(1))
		.and(maxNumberLength(1_000))
		.and(requiredNumberField),
	availableQuantity: z
		.number()
		.and(minNumberLength(0))
		.and(maxNumberLength(10e9))
		.and(requiredNumberField),
	imageUrl: imageUrl("Image is required.")
});

// prettier-ignore
const foodFormSchema = souvenirFormSchema.extend({
	type: z
		.literal("Snack")
		.or(z.literal("Drink"))
		.and(requiredStringField),
	size: z
		.number()
		.and(minNumberLength(10e-4))
		.and(maxNumberLength(25))
		.and(requiredNumberField)
});

// prettier-ignore
const genreFormSchema = z.object({
	name: z
		.string()
		.and(maxStringLength(32))
		.and(minStringLength(2))
		.and(requiredStringField)
		.and(forbidNumbers)
		.and(firstLetterUppercase)
});

// prettier-ignore
const hallFormSchema = z.object({
	name: z
		.string()
		.and(minStringLength(2))
		.and(maxStringLength(50))
		.and(requiredStringField),
	cinema: z
		.string()
		.uuid("Select a cinema.")
		.and(requiredStringField)
});

// prettier-ignore
const movieFormSchema = z.object({
	title: z
		.string()
		.and(firstCharacterUppercaseOrNumber)
		.and(maxStringLength(256))
		.and(requiredStringField),
	year: z
		.number()
		.and(minNumberLength(1_900))
		.and(maxNumberLength(2_199))
		.and(requiredNumberField),
	duration: z
		.number()
		.and(minNumberLength(30))
		.and(maxNumberLength(360))
		.and(requiredNumberField),
	tagline: z
		.string()
		.and(firstCharacterUppercaseOrNumber)
		.and(minStringLength(8))
		.and(maxStringLength(256))
		.and(requiredStringField),
	summary: z
		.string()
		.and(firstCharacterUppercaseOrNumber)
		.and(minStringLength(32))
		.and(maxStringLength(2_048))
		.and(requiredStringField),
	inCinemas: z.boolean(),
	posterUrl: imageUrl("Poster is required."),
	backdropUrl: imageUrl("Backdrop is required."),
	trailerUrl: z
		.string()
		.and(url),
	filmRating: z
		.string()
		.uuid("Select a rating.")
		.and(requiredStringField),
	genres: z
		.array(z.string().uuid("Select at least one genre."))
		.min(1, "Select at least one genre."),
	actors: z
		.array(z
			.object({
				id: z
					.string()
					.and(requiredStringField),
				character: z
					.string()
					.and(requiredStringField)
					.and(maxStringLength(256))
			})
		)
		.min(1, "Select at least one actor."),
	directors: z
		.array(z
			.object({
				id: z.string().and(requiredStringField)
			}))
		.min(1, "Select at least one director.")
});

// prettier-ignore
const personFormSchema = z.object({
	name: z
		.string()
		.and(firstLetterUppercase)
		.and(minStringLength(2))
		.and(maxStringLength(256))
		.and(requiredStringField),
	dateOfBirth: z
		.date({
			required_error: REQUIRED_FIELD,
			invalid_type_error: DATE_FIELD
		}),
	dateOfDeath: z
		.date({
			invalid_type_error: DATE_FIELD
		})
		.nullable(),
	imageUrl: imageUrl("Image is required."),
	biography: z
		.string()
		.and(firstLetterUppercase)
		.and(maxStringLength(4_096))
		.and(requiredStringField)
});

// prettier-ignore
const projectionFormSchema = z.object({
	movie: z
		.string()
		.min(2, "Select a movie.")
		.and(requiredStringField),
	hall: z
		.string()
		.uuid("Select a hall.")
		.and(requiredStringField),
	date: z
		.date({
			required_error: REQUIRED_FIELD,
			invalid_type_error: DATE_FIELD
		}),
	time: z
		.date({
			required_error: REQUIRED_FIELD,
			invalid_type_error: "Enter time."
		})
});

// prettier-ignore
const seatFormSchema = z.object({
	number: z
		.number()
		.and(minNumberLength(1))
		.and(maxNumberLength(500))
		.and(requiredNumberField),
	halls: z
		.array(z.string().uuid("Select at least one hall."))
		.min(1, "Select at least one hall.")
});

export {
	DATE_FIELD,
	cinemaFormSchema,
	filmRatingFormSchema,
	foodFormSchema,
	genreFormSchema,
	hallFormSchema,
	movieFormSchema,
	personFormSchema,
	projectionFormSchema,
	seatFormSchema,
	souvenirFormSchema
};
