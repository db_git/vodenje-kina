//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type ZodNumber, type ZodString, z } from "zod";

export const REQUIRED_FIELD = "This field is required.";

export const requiredStringField = z.string().min(1, REQUIRED_FIELD);

export const requiredNumberField = z.number({
	required_error: REQUIRED_FIELD,
	invalid_type_error: REQUIRED_FIELD
});

export const maxStringLength = (max: number): ZodString => {
	return z.string().max(max, `Maximum length must not exceed ${max} characters.`);
};

export const maxNumberLength = (max: number): ZodNumber => {
	return z.number().max(max, `Maximum value is ${max}.`);
};

export const minStringLength = (min: number): ZodString => {
	return z.string().min(min, `Minimum length is ${min} characters.`);
};

export const minNumberLength = (min: number): ZodNumber => {
	return z.number().min(min, `Minimum value is ${min}.`);
};

export const forbidNumbers = z.string().refine((value) => {
	return !/\d/u.test(value);
}, "Numbers are not allowed.");

export const firstLetterUppercase = z.string().refine((value) => {
	if (value === null || value === undefined || value.length < 1) return true;

	return value.startsWith(value.charAt(0).toUpperCase());
}, "First letter must be uppercase.");

export const firstCharacterUppercaseOrNumber = z.string().refine((value) => {
	if (value === null || value === undefined || value.length < 1) return true;

	const firstCharacter = value.charAt(0);
	const parsedNumber = Number.parseInt(firstCharacter, 10);

	if (Number.isInteger(parsedNumber)) return true;

	return value.startsWith(firstCharacter.toUpperCase());
}, "First character must be an uppercase or a number.");

export const url = z
	.string()
	.url("Not a valid URL.")
	.and(minStringLength(8))
	.and(maxStringLength(2_048))
	.and(requiredStringField);

export const imageUrl = (message: string) => {
	return z.any().refine((value) => {
		if (value instanceof Blob) return true;
		if (typeof value === "string") {
			return url.safeParse(value).success;
		}

		return false;
	}, message);
};

export const password = z
	.string()
	.regex(/\d/u, "Password must have at least one digit.")
	.min(12, "Password must be at least 12 characters.")
	.and(requiredStringField);
