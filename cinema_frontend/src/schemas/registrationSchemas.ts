//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { z } from "zod";

import { maxStringLength, requiredStringField } from "./commonValidations";

// prettier-ignore
const loginFormValidationSchema = z.object({
	email: z
		.string()
		.email("Enter a valid e-mail address.")
		.and(maxStringLength(254))
		.and(requiredStringField),
	password: z
		.string()
		.regex(/\d/u, "Password must have at least one digit.")
		.min(12, "Password must be at least 12 characters.")
		.and(requiredStringField)
});

// prettier-ignore
const registerFormValidationSchema = loginFormValidationSchema.extend({
	name: z
		.string()
		.and(maxStringLength(256))
		.and(requiredStringField)
});

// prettier-ignore
const forgotPasswordFormValidationSchema = z.object({
	email: z
		.string()
		.email("Enter a valid e-mail address.")
		.and(maxStringLength(254))
		.and(requiredStringField)
});

export { loginFormValidationSchema, registerFormValidationSchema, forgotPasswordFormValidationSchema };
