//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

const endpointRoot = "/api";

const ApiPaths = {
	dashboard: `${endpointRoot}/dashboard`,
	cinemas: `${endpointRoot}/cinemas`,
	filmRatings: `${endpointRoot}/film-ratings`,
	foods: `${endpointRoot}/foods`,
	genres: `${endpointRoot}/genres`,
	halls: `${endpointRoot}/halls`,
	movies: `${endpointRoot}/movies`,
	people: `${endpointRoot}/people`,
	projections: `${endpointRoot}/projections`,
	reservations: `${endpointRoot}/reservations`,
	seats: `${endpointRoot}/seats`,
	souvenirs: `${endpointRoot}/souvenirs`,
	users: `${endpointRoot}/users`,
	userComments: `${endpointRoot}/comments`,
	userRatings: `${endpointRoot}/ratings`
};

export default ApiPaths;
