//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type AxiosResponse } from "axios";
import { type NavigateFunction } from "react-router-dom";

import { type CinemaFormSubmitValues } from "../pages/admin/cinema/cinema";
import { type FilmRatingFormSubmitValues } from "../pages/admin/filmRating/filmRating";
import { type FoodFormSubmitValues } from "../pages/admin/food/food";
import { type GenreFormSubmitValues } from "../pages/admin/genre/genre";
import { type HallFormSubmitValues } from "../pages/admin/hall/hall";
import { type MovieFormSubmitValues } from "../pages/admin/movie/movie";
import { type PersonFormSubmitValues } from "../pages/admin/person/person";
import { type ProjectionFormSubmitValues } from "../pages/admin/projection/projection";
import { type SeatFormSubmitValues } from "../pages/admin/seat/seat";
import { type SouvenirFormSubmitValues } from "../pages/admin/souvenir/souvenir";
import {
	type SelectedFood,
	type SelectedSouvenir,
	type UserRatingFormSubmitValues,
	type UserCommentFormSubmitValues
} from "../pages/movieDetails/movieDetails";
import {
	type SimpleMovie,
	type RatingStar,
	type IndividualSeat,
	type UserRating,
	type MovieRating,
	type UserComment,
	type IndividualMovie,
	type HomePageMovies,
	type Cinema,
	type FilmRating,
	type Food,
	type Genre,
	type Hall,
	type Movie,
	type Person,
	type Projection,
	type Reservation,
	type Seat,
	type Souvenir,
	type User
} from "../types/entities";
import { type HttpMethod } from "../types/enums/Http";
import {
	type SouvenirQueryParameterOptions,
	type FoodQueryParameterOptions,
	type SeatQueryParameterOptions,
	type MovieQueryParameterOptions,
	type PersonQueryParameterOptions,
	type ProjectionQueryParameterOptions,
	type QueryParameterOptions
} from "../types/enums/Stores";
import {
	type SouvenirQueryParameters,
	type FoodQueryParameters,
	type SeatQueryParameters,
	type HomePageMoviesQueryParameters,
	type MovieQueryParameters,
	type PersonQueryParameters,
	type ProjectionQueryParameters,
	type QueryParameters
} from "../types/query";
import { type JsonPatch } from "../types/shared";
import { type ApiStoreLoading, type ApiStoreStatus, type Page } from "../types/stores";
import { type NotificationSeverity } from "../utils/notifications";

export type IApiStore<TDto, TQuery extends QueryParameters, TIndividualDto, TCreateDto, TUpdateDto> = {
	get loading(): ApiStoreLoading;
	get queryParameters(): TQuery;
	get response(): TIndividualDto | undefined;
	get responses(): Page<TDto[]> | undefined;
	get status(): ApiStoreStatus;
	get urlSearchString(): string;

	readonly setLoading: (type: HttpMethod, state: boolean) => void;
	setQueryParameters(type: QueryParameterOptions, value: number | string | undefined): void;
	readonly setResponse: (response: TIndividualDto | undefined) => void;
	readonly setResponses: (responses: Page<TDto[]> | undefined) => void;
	readonly setStatus: (type: HttpMethod, status: number) => void;
	readonly setUrlSearchString: (query: URLSearchParams) => void;
	readonly modifyUrl: (location: string, url: NavigateFunction) => void;

	get(id: string): Promise<AxiosResponse<TIndividualDto>>;
	getAll(urlParameters?: URLSearchParams): Promise<AxiosResponse<Page<TDto[]>>>;
	patch(id: string, patch: Array<JsonPatch<TIndividualDto>>): Promise<AxiosResponse>;
	post(values: TCreateDto): Promise<AxiosResponse>;
	put(id: string, values: TUpdateDto): Promise<AxiosResponse>;
	delete(id: string): Promise<AxiosResponse>;
};

export type IGetStore<TDto, TQuery extends QueryParameters, TIndividualDto> = Omit<
	IApiStore<TDto, TQuery, TIndividualDto, Record<string, unknown>, Record<string, unknown>>,
	"delete" | "patch" | "post" | "put"
>;

export type IApiRootStore = {
	readonly authStore: IAuthStore;
	readonly dashboardStore: IDashboardStore;
	readonly cinemaStore: ICinemaApiStore;
	readonly filmRatingStore: IFilmRatingApiStore;
	readonly foodStore: IFoodApiStore;
	readonly genreStore: IGenreApiStore;
	readonly hallStore: IHallApiStore;
	readonly movieStore: IMovieApiStore;
	readonly personStore: IPersonApiStore;
	readonly projectionStore: IProjectionApiStore;
	readonly reservationStore: IReservationApiStore;
	readonly seatStore: ISeatApiStore;
	readonly souvenirStore: ISouvenirApiStore;
	readonly userStore: IUserApiStore;
	readonly userCommentStore: IUserCommentApiStore;
	readonly userRatingStore: IUserRatingApiStore;
};

export type IGetRootStore = {
	readonly cinemaGetStore: ICinemaGetStore;
	readonly filmRatingGetStore: IFilmRatingGetStore;
	readonly genreGetStore: IGenreGetStore;
	readonly hallGetStore: IHallGetStore;
	readonly movieGetStore: IMovieGetStore;
	readonly actorGetStore: IActorGetStore;
	readonly directorGetStore: IDirectorGetStore;
	readonly personGetStore: IPersonGetStore;
	readonly userCommentGetStore: IUserCommentGetStore;
	readonly userRatingGetStore: IUserRatingGetStore;
};

export type Order = {
	projection: Projection;
	seats: Seat[];
	foods: SelectedFood[] | undefined;
	souvenirs: SelectedSouvenir[] | undefined;
};

export type ICartStore = {
	get itemsCount(): number;
	get orders(): Order[];

	readonly addOrder(order: Order): void;
	readonly removeOrder(index: number): void;
	readonly clearOrders(): void;
};

export type ICinemaApiStore = IApiStore<
	Cinema,
	QueryParameters,
	Cinema,
	CinemaFormSubmitValues,
	CinemaFormSubmitValues
> & {
	get showAddress(): boolean;
	get showHalls(): boolean;

	readonly toggleShowAddress(): void;
	readonly toggleShowHalls(): void;
};

export type ICinemaGetStore = IGetStore<Cinema, QueryParameters, Cinema>;

export type IFilmRatingApiStore = IApiStore<
	FilmRating,
	QueryParameters,
	FilmRating,
	FilmRatingFormSubmitValues,
	FilmRatingFormSubmitValues
> & {
	get showDescription(): boolean;
	readonly toggleShowDescription(): void;
};

export type IFilmRatingGetStore = IGetStore<FilmRating, QueryParameters, FilmRating>;

export type IFoodApiStore = IApiStore<Food, FoodQueryParameters, Food, FoodFormSubmitValues, FoodFormSubmitValues> & {
	get showImage(): boolean;
	get showType(): boolean;
	get showSize(): boolean;

	readonly toggleShowImage(): void;
	readonly toggleShowType(): void;
	readonly toggleShowSize(): void;

	readonly setFoodQueryParameters: (
		type: FoodQueryParameterOptions | QueryParameterOptions,
		value: number | string | undefined
	) => void;
};

export type IGenreApiStore = IApiStore<Genre, QueryParameters, Genre, GenreFormSubmitValues, GenreFormSubmitValues>;
export type IGenreGetStore = IGetStore<Genre, QueryParameters, Genre>;
export type IHallApiStore = IApiStore<Hall, QueryParameters, Hall, HallFormSubmitValues, HallFormSubmitValues>;
export type IHallGetStore = IGetStore<Hall, QueryParameters, Hall>;

export type IMovieApiStore = IApiStore<
	Movie,
	MovieQueryParameters,
	IndividualMovie,
	MovieFormSubmitValues,
	MovieFormSubmitValues
> & {
	readonly setMovieQueryParameters: (
		type: MovieQueryParameterOptions | QueryParameterOptions,
		value: string[] | boolean | number | string | undefined
	) => void;

	get showYear(): boolean;
	get showPoster(): boolean;
	get showDuration(): boolean;
	get showTagline(): boolean;
	get showSummary(): boolean;
	get showInCinemas(): boolean;
	get showIsFeatured(): boolean;
	get showIsRecommended(): boolean;

	readonly toggleShowYear: () => void;
	readonly toggleShowPoster: () => void;
	readonly toggleShowDuration: () => void;
	readonly toggleShowTagline: () => void;
	readonly toggleShowSummary: () => void;
	readonly toggleShowInCinemas: () => void;
	readonly toggleShowIsFeatured: () => void;
	readonly toggleShowIsRecommended: () => void;
};

export type IMovieGetStore = IGetStore<Movie, MovieQueryParameters, IndividualMovie> &
	Omit<
		IMovieApiStore,
		| "showDuration"
		| "showInCinemas"
		| "showPoster"
		| "showSummary"
		| "showTagline"
		| "showYear"
		| "toggleShowDuration"
		| "toggleShowInCinemas"
		| "toggleShowPoster"
		| "toggleShowSummary"
		| "toggleShowTagline"
		| "toggleShowYear"
	> & {
		get homePageMovies(): HomePageMovies | undefined;
		get homePageMoviesLoading(): boolean;
		get homePageMoviesStatus(): number;

		get recommendations(): Movie[] | undefined;
		get recommendationsLoading(): boolean;
		get recommendationsStatus(): number;

		readonly setHomePageMovies(homePageMovies: HomePageMovies | undefined): void;
		readonly setHomePageMoviesLoading(loading: boolean): void;
		readonly setHomePageMoviesStatus(status: number): void;

		readonly setRecommendations(recommendations: Movie[] | undefined): void;
		readonly setRecommendationsLoading(loading: boolean): void;
		readonly setRecommendationsStatus(status: number): void;

		readonly getHomePageMovies(query: HomePageMoviesQueryParameters): Promise<void>;
		readonly getRecommendations(id: string): Promise<void>;
	};

export type INavbarStore = {
	get active(): string;
	get isVisible(): boolean;

	readonly setActive: (value: string) => void;
	readonly setVisible: (value: boolean) => void;
};

export type IPersonApiStore = IApiStore<
	Person,
	PersonQueryParameters,
	Person,
	PersonFormSubmitValues,
	PersonFormSubmitValues
> & {
	get showPicture(): boolean;
	get showBiography(): boolean;
	get showDateOfBirth(): boolean;
	get showDateOfDeath(): boolean;
	get showMoviesCount(): boolean;

	readonly toggleShowPicture: () => void;
	readonly toggleShowBiography: () => void;
	readonly toggleShowDateOfBirth: () => void;
	readonly toggleShowDateOfDeath: () => void;
	readonly toggleShowMoviesCount: () => void;

	readonly setPersonQueryParameters: (
		type: PersonQueryParameterOptions | QueryParameterOptions,
		value: number | string | undefined
	) => void;
};

export type IPersonGetStore = IGetStore<Person, PersonQueryParameters, Person> &
	Omit<
		IPersonApiStore,
		| "showBiography"
		| "showDateOfBirth"
		| "showDateOfDeath"
		| "showMoviesCount"
		| "showPicture"
		| "toggleShowBiography"
		| "toggleShowDateOfBirth"
		| "toggleShowDateOfDeath"
		| "toggleShowMoviesCount"
		| "toggleShowPicture"
	>;
export type IActorGetStore = IPersonGetStore;
export type IDirectorGetStore = IPersonGetStore;

export type IProjectionApiStore = IApiStore<
	Projection,
	ProjectionQueryParameters,
	Projection,
	ProjectionFormSubmitValues,
	ProjectionFormSubmitValues
> & {
	readonly setProjectionQueryParameters: (
		type: ProjectionQueryParameterOptions | QueryParameterOptions,
		value: number | string | undefined
	) => void;
	get takenSeats(): IndividualSeat[] | undefined;
	getTakenSeats: (id: string) => Promise<void>;
};

export type IReservationApiStore = IApiStore<
	Reservation,
	QueryParameters,
	Reservation,
	Record<string, unknown>,
	Record<string, unknown>
> & {
	getAllAccount(urlParameters?: URLSearchParams): Promise<AxiosResponse<Page<Reservation[]>>>;
	postRange(values: Order[]): Promise<AxiosResponse>;
};

export type ISeatApiStore = IApiStore<Seat, SeatQueryParameters, Seat, SeatFormSubmitValues, SeatFormSubmitValues> & {
	readonly setSeatQueryParameters: (
		type: QueryParameterOptions | SeatQueryParameterOptions,
		value: number | string | undefined
	) => void;
};

export type ISouvenirApiStore = IApiStore<
	Souvenir,
	SouvenirQueryParameters,
	Souvenir,
	SouvenirFormSubmitValues,
	SouvenirFormSubmitValues
> & {
	get showImage(): boolean;
	readonly toggleShowImage(): void;

	readonly setSouvenirQueryParameters: (
		type: QueryParameterOptions | SouvenirQueryParameterOptions,
		value: number | string | undefined
	) => void;
};

export type IUserApiStore = IApiStore<User, QueryParameters, User, Record<string, unknown>, Record<string, unknown>> & {
	get showImage(): boolean;
	readonly toggleShowImage(): void;
};

export type IUserCommentApiStore = Omit<
	IApiStore<UserComment, QueryParameters, UserComment, UserCommentFormSubmitValues, Record<string, unknown>>,
	"delete" | "put"
> & {
	get edit(): UserComment | undefined;
	readonly setEdit(review: UserComment | undefined): void;

	get canComment(): boolean;
	readonly getCanComment: (movieId: string) => Promise<void>;

	deleteComment(movieId: string, userId: string, showMessage: boolean = true): Promise<void>;

	get showImage(): boolean;
	readonly toggleShowImage(): void;
};

export type IUserRatingApiStore = Omit<
	IApiStore<UserRating, QueryParameters, MovieRating, UserRatingFormSubmitValues, Record<string, unknown>>,
	"delete" | "patch" | "put"
> & {
	deleteRating(movieId: string, userId: string, showMessage: boolean = true): Promise<void>;
	updateRating(id: string, patch: Array<JsonPatch<UserRating>>): Promise<AxiosResponse>;

	get showImage(): boolean;
	readonly toggleShowImage(): void;
};

export type IUserCommentGetStore = Omit<IGetStore<UserComment, QueryParameters, UserComment>, "get">;
export type IUserRatingGetStore = Omit<IGetStore<UserRating, QueryParameters, MovieRating>, "getAll">;

export type IAuthStore = {
	get getUserId(): string | null;
	get isAdmin(): boolean;
	get isLoggedIn(): boolean;
	get getUserName(): string | null;
	get verificationEmailSent(): boolean;
	get emailVerified(): boolean;
	get account(): User | undefined;
	get nameUpdateLoading(): boolean;
	get imageUpdateLoading(): boolean;
	get forgotPasswordEmailSent(): boolean;
	get passwordIsReset(): boolean;
	get passwordChangeLoading(): boolean;

	get getHeader(): { Authorization: string };
	get getToken(): string | null;
	readonly setHeader(token: string): void;
	readonly setToken(token: string): void;

	get loading(): boolean;
	get status(): number;
	readonly setLoading(loading: boolean): void;
	readonly setStatus(status: number): void;
	readonly setAccount(account: User | undefined): void;
	readonly setNameUpdateLoading(loading: boolean): void;
	readonly setImageUpdateLoading(loading: boolean): void;

	readonly updatePassword: (oldPassword: string, newPassword: string) => Promise<void>;
	readonly resetPassword: (email: string, password: string, passwordResetCode: string) => Promise<void>;
	readonly forgotPassword: (email: string) => Promise<void>;
	readonly updateName: (name: string) => Promise<void>;
	readonly updateImage: (image: string) => Promise<void>;
	readonly accountInformation: () => Promise<void>;
	readonly verifyEmail: (userId: string, verificationCode: string) => Promise<void>;
	readonly register: (values: Record<string, unknown>) => Promise<void>;
	readonly login: (values: Record<string, unknown>) => Promise<void>;
	readonly logout: () => void;
};

export type NotificationResponse = {
	title: string;
	severity: NotificationSeverity;
	message?: string;
};

export type DashboardLoading = {
	users: boolean;
	movies: boolean;
	revenue: boolean;
};

export type DashboardStatus = {
	users: number;
	movies: number;
	revenue: number;
};

export type UserStats = {
	totalUsers: number;
	confirmedEmailUsers: number;
	unconfirmedEmailUsers: number;
	usersWithReservations: number;
	usersWithoutReservations: number;
};

export type MovieStats = {
	totalMovies: number;
	bestRatedMovies: SimpleMovie[];
	worstRatedMovies: SimpleMovie[];
	ratingStars: RatingStar[];
};

type Revenue = {
	price: number;
	orders: number;
};

export type RevenueStats = {
	food: {
		previousMonth: {
			foods: Revenue[];
			total: number;
		};
		currentMonth: {
			foods: Revenue[];
			total: number;
		};
	};
	souvenir: {
		previousMonth: {
			foods: Revenue[];
			total: number;
		};
		currentMonth: {
			foods: Revenue[];
			total: number;
		};
	};
	reservation: {
		previousMonth: {
			foods: Revenue[];
			total: number;
		};
		currentMonth: {
			foods: Revenue[];
			total: number;
		};
	};
};

export type IDashboardStore = {
	get loading(): DashboardLoading;
	readonly setLoading: (type: keyof DashboardLoading, value: boolean) => void;

	get status(): DashboardStatus;
	readonly setStatus: (type: keyof DashboardStatus, value: number) => void;

	get movieStats(): MovieStats | undefined;
	readonly setMovieStats: (stats: MovieStats | undefined) => void;

	get userStats(): UserStats | undefined;
	readonly setUserStats: (stats: UserStats | undefined) => void;

	get revenueStats(): RevenueStats | undefined;
	readonly setRevenueStats: (stats: RevenueStats | undefined) => void;

	readonly getUserStats: () => Promise<void>;
	readonly getMovieStats: () => Promise<void>;
	readonly getRevenueStats: () => Promise<void>;
};
