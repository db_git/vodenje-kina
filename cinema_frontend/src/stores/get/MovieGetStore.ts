//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import axios, { type AxiosResponse } from "axios";
import { action, computed, makeObservable, observable } from "mobx";

import { type Movie, type HomePageMovies } from "../../types/entities";
import { type HomePageMoviesQueryParameters } from "../../types/query";
import { is5xxStatus } from "../../utils/isStatus";
import MovieApiStore from "../api/MovieApiStore";
import { type IAuthStore, type IMovieGetStore } from "../stores";

class MovieGetStore extends MovieApiStore implements IMovieGetStore {
	private _homePageMoviesLoading: boolean;
	private _homePageMoviesStatus: number;
	private _homePageMovies: HomePageMovies | undefined;

	private _recommendationsLoading: boolean;
	private _recommendationsStatus: number;
	private _recommendations: Movie[] | undefined;

	public constructor(authStore: IAuthStore) {
		super(authStore);

		this._homePageMoviesLoading = true;
		this._homePageMoviesStatus = 0;
		this._recommendationsLoading = true;
		this._recommendationsStatus = 0;

		makeObservable<
			IMovieGetStore,
			| "_homePageMovies"
			| "_homePageMoviesLoading"
			| "_homePageMoviesStatus"
			| "_recommendations"
			| "_recommendationsLoading"
			| "_recommendationsStatus"
		>(this, {
			_homePageMoviesLoading: observable,
			_homePageMoviesStatus: observable,
			_homePageMovies: observable,

			_recommendationsLoading: observable,
			_recommendationsStatus: observable,
			_recommendations: observable,

			homePageMovies: computed,
			homePageMoviesLoading: computed,
			homePageMoviesStatus: computed,

			recommendations: computed,
			recommendationsLoading: computed,
			recommendationsStatus: computed,

			setHomePageMovies: action,
			setHomePageMoviesLoading: action,
			setHomePageMoviesStatus: action,

			setRecommendations: action,
			setRecommendationsLoading: action,
			setRecommendationsStatus: action,

			getHomePageMovies: action,
			getRecommendations: action
		});
	}

	public get homePageMovies(): HomePageMovies | undefined {
		return this._homePageMovies;
	}

	public readonly setHomePageMovies = (homePageMovies: HomePageMovies | undefined): void => {
		this._homePageMovies = homePageMovies;
	};

	public get homePageMoviesLoading(): boolean {
		return this._homePageMoviesLoading;
	}

	public readonly setHomePageMoviesLoading = (loading: boolean): void => {
		this._homePageMoviesLoading = loading;
	};

	public get homePageMoviesStatus(): number {
		return this._homePageMoviesStatus;
	}

	public readonly setHomePageMoviesStatus = (status: number): void => {
		this._homePageMoviesStatus = status;
	};

	public get recommendations(): Movie[] | undefined {
		return this._recommendations;
	}

	public readonly setRecommendations = (recommendations: Movie[] | undefined): void => {
		this._recommendations = recommendations;
	};

	public get recommendationsLoading(): boolean {
		return this._recommendationsLoading;
	}

	public readonly setRecommendationsLoading = (loading: boolean): void => {
		this._recommendationsLoading = loading;
	};

	public get recommendationsStatus(): number {
		return this._recommendationsStatus;
	}

	public readonly setRecommendationsStatus = (status: number): void => {
		this._recommendationsStatus = status;
	};

	public readonly getHomePageMovies = async (query: HomePageMoviesQueryParameters): Promise<void> => {
		this.setHomePageMoviesLoading(true);

		await axios
			.get(this._endpoint + "/home-page", {
				params: query
			})
			.then((response: AxiosResponse<HomePageMovies>) => {
				this.setHomePageMoviesStatus(response.status);
				this.setHomePageMovies(response.data);
			})
			.catch((error) => {
				this.setHomePageMoviesStatus(error.response.status);
				if (is5xxStatus(error.response.status)) this.handle5xxStatus(error);
			})
			.finally(() => {
				this.setHomePageMoviesLoading(false);
			});
	};

	public readonly getRecommendations = async (id: string): Promise<void> => {
		this.setRecommendationsLoading(true);

		await axios
			.get(`${this._endpoint}/${id}/recommendations`)
			.then((response: AxiosResponse<Movie[]>) => {
				this.setRecommendationsStatus(response.status);
				this.setRecommendations(response.data);
			})
			.catch((error) => {
				this.setRecommendationsStatus(error.response.status);
				if (is5xxStatus(error.response.status)) this.handle5xxStatus(error);
			})
			.finally(() => {
				this.setRecommendationsLoading(false);
			});
	};
}

export default MovieGetStore;
