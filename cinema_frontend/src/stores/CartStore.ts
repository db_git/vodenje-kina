//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { action, computed, makeObservable, observable } from "mobx";
import * as superjson from "superjson";

import { type Order, type IAuthStore, type ICartStore } from "./stores";

const ORDERS_KEY = "orders";

class CartStore implements ICartStore {
	private static cartStore: ICartStore;

	private readonly _authStore: IAuthStore;

	private _itemsCount: number;
	private _orders: Order[];

	private constructor(authStore: IAuthStore) {
		this._authStore = authStore;

		this._itemsCount = 0;
		this._orders = [];

		const orders = sessionStorage.getItem(ORDERS_KEY);
		if (orders) {
			try {
				const parsedOrders = superjson.parse<Order[]>(orders);

				if (!Array.isArray(parsedOrders)) throw new Error("Invalid type");

				this._orders = parsedOrders;
				this._itemsCount = this._orders.length;
			} catch {
				this._itemsCount = 0;
				this._orders = [];
				sessionStorage.removeItem(ORDERS_KEY);
			}
		}

		makeObservable<ICartStore, "_authStore" | "_itemsCount" | "_orders">(this, {
			_authStore: false,

			_itemsCount: observable,
			_orders: observable,

			itemsCount: computed,
			orders: computed,

			addOrder: action,
			removeOrder: action,
			clearOrders: action
		});
	}

	public get itemsCount(): number {
		return this._itemsCount;
	}

	public get orders(): Order[] {
		return this._orders;
	}

	public readonly addOrder = (order: Order): void => {
		this._orders.push(order);
		this._itemsCount = this._orders.length;
		sessionStorage.setItem(ORDERS_KEY, superjson.stringify(this._orders));
	};

	public readonly removeOrder = (index: number): void => {
		this._orders.splice(index, 1);
		this._itemsCount = this._orders.length;
		sessionStorage.setItem(ORDERS_KEY, superjson.stringify(this._orders));
	};

	public readonly clearOrders = (): void => {
		this._orders = [];
		this._itemsCount = this._orders.length;
		sessionStorage.setItem(ORDERS_KEY, superjson.stringify(this._orders));
	};

	public static GetInstance(authStore: IAuthStore): ICartStore {
		if (this.cartStore === undefined) this.cartStore = new CartStore(authStore);
		return this.cartStore;
	}
}

export default CartStore;
