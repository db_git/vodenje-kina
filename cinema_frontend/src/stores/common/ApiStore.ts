//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import axios, { type AxiosError, type AxiosResponse } from "axios";
import { action, computed, makeObservable, observable } from "mobx";
import { type NavigateFunction } from "react-router-dom";

import { HttpMethod } from "../../types/enums/Http";
import { QueryParameterOptions } from "../../types/enums/Stores";
import { type QueryParameters } from "../../types/query";
import { type JsonPatch } from "../../types/shared";
import { type ApiStoreLoading, type ApiStoreStatus, type Page } from "../../types/stores";
import { is4xxStatus, is5xxStatus } from "../../utils/isStatus";
import { NotificationSeverity, showNotification } from "../../utils/notifications";
import { type IApiStore, type IAuthStore, type NotificationResponse } from "../stores";

abstract class ApiStore<TDto, TQuery extends QueryParameters, TIndividualDto, TCreateDto, TUpdateDto>
	implements IApiStore<TDto, TQuery, TIndividualDto, TCreateDto, TUpdateDto>
{
	protected readonly _endpoint: string;
	protected readonly _authStore: IAuthStore;
	private readonly _loading: ApiStoreLoading;
	private readonly _queryParameters: TQuery;
	private _response: TIndividualDto | undefined;
	private _responses: Page<TDto[]> | undefined;
	private readonly _status: ApiStoreStatus;

	protected constructor(authStore: IAuthStore, endpoint: string) {
		this._endpoint = endpoint;
		this._authStore = authStore;
		this._queryParameters = { page: 1, size: 10, orderBy: 1 } as TQuery;
		this._status = { get: 0, getAll: 0, patch: 0, post: 0, put: 0, delete: 0 };
		this._loading = { get: false, getAll: true, patch: false, post: false, put: false, delete: false };

		makeObservable<
			ApiStore<TDto, TQuery, TIndividualDto, TCreateDto, TUpdateDto>,
			| "_authStore"
			| "_endpoint"
			| "_loading"
			| "_queryParameters"
			| "_response"
			| "_responses"
			| "_status"
			| "handle4xxStatus"
			| "handle5xxStatus"
		>(this, {
			handle4xxStatus: false,
			handle5xxStatus: false,
			_authStore: false,

			_endpoint: observable,
			_loading: observable,
			_queryParameters: observable,
			_response: observable,
			_responses: observable,
			_status: observable,

			loading: computed,
			queryParameters: computed,
			response: computed,
			responses: computed,
			status: computed,
			urlSearchString: computed,

			setLoading: action,
			setQueryParameters: action,
			setResponse: action,
			setResponses: action,
			setStatus: action,
			setUrlSearchString: action,
			modifyUrl: action,

			get: action,
			getAll: action,
			patch: action,
			post: action,
			put: action,
			delete: action
		});
	}

	public get loading(): ApiStoreLoading {
		return this._loading;
	}

	public get response(): TIndividualDto | undefined {
		return this._response;
	}

	public get responses(): Page<TDto[]> | undefined {
		return this._responses;
	}

	public get queryParameters(): TQuery {
		return this._queryParameters;
	}

	public get status(): ApiStoreStatus {
		return this._status;
	}

	public get urlSearchString(): string {
		const queryParameters: string[] = [];

		queryParameters.push(`page=${this.queryParameters.page}`);
		queryParameters.push(`size=${this.queryParameters.size}`);
		queryParameters.push(`orderBy=${this.queryParameters.orderBy}`);
		if (this.queryParameters.search) queryParameters.push(`search=${this.queryParameters.search}`);

		return `?${queryParameters.join("&")}`;
	}

	public readonly setLoading = (type: HttpMethod, state: boolean): void => {
		switch (type) {
			case HttpMethod.Get:
				this._loading.get = state;
				break;
			case HttpMethod.GetAll:
				this._loading.getAll = state;
				break;
			case HttpMethod.Patch:
				this._loading.patch = state;
				break;
			case HttpMethod.Post:
				this._loading.post = state;
				break;
			case HttpMethod.Put:
				this._loading.put = state;
				break;
			case HttpMethod.Delete:
				this._loading.delete = state;
				break;
			default:
				break;
		}
	};

	public readonly setResponse = (response: TIndividualDto | undefined): void => {
		this._response = response;
	};

	public readonly setResponses = (responses: Page<TDto[]> | undefined): void => {
		this._responses = responses;
	};

	public readonly setQueryParameters = (type: QueryParameterOptions, value: number | string | undefined): void => {
		switch (type) {
			case QueryParameterOptions.Page:
				if (typeof value === "number") this.queryParameters.page = value;
				break;
			case QueryParameterOptions.Size:
				if (typeof value === "number") this.queryParameters.size = value;
				break;
			case QueryParameterOptions.OrderBy:
				if (typeof value === "number") this.queryParameters.orderBy = value;
				break;
			case QueryParameterOptions.Search:
				if (typeof value !== "number") this.queryParameters.search = value;
				break;
			default:
				break;
		}
	};

	public readonly setStatus = (type: HttpMethod, status: number): void => {
		switch (type) {
			case HttpMethod.Get:
				this._status.get = status;
				break;
			case HttpMethod.GetAll:
				this._status.getAll = status;
				break;
			case HttpMethod.Patch:
				this._status.patch = status;
				break;
			case HttpMethod.Post:
				this._status.post = status;
				break;
			case HttpMethod.Put:
				this._status.put = status;
				break;
			case HttpMethod.Delete:
				this._status.delete = status;
				break;

			default:
				break;
		}
	};

	public setUrlSearchString(query: URLSearchParams): void {
		const page = query.get("page");
		const size = query.get("size");
		const orderBy = query.get("orderBy");
		const search = query.get("search");

		if (page) this.setQueryParameters(QueryParameterOptions.Page, Number.parseInt(page, 10));
		if (size) this.setQueryParameters(QueryParameterOptions.Size, Number.parseInt(size, 10));
		if (orderBy) this.setQueryParameters(QueryParameterOptions.OrderBy, Number.parseInt(orderBy, 10));
		if (search) this.setQueryParameters(QueryParameterOptions.Search, search);
	}

	public readonly modifyUrl = (location: string, url: NavigateFunction): void => {
		url(`${location}/${this.urlSearchString}`);
	};

	protected readonly handle4xxStatus = (error: AxiosError<NotificationResponse>) => {
		if (!error.response || !error.response.data) return;

		showNotification({
			severity: error.response.data.severity,
			message: error.response.data.message,
			title: error.response.data.title
		});
	};

	protected readonly handle5xxStatus = (error: AxiosError<NotificationResponse>) => {
		if (!error.response || !error.response.data) return;

		showNotification({
			severity: error.response.data.severity ?? NotificationSeverity.ERROR,
			message: error.response.data.message ?? "An unknown error has occurred.",
			title: error.response.data.title ?? "Server error"
		});
	};

	public async get(id: string): Promise<AxiosResponse<TIndividualDto>> {
		this.setLoading(HttpMethod.Get, true);
		return await new Promise((resolve) => {
			axios
				.get(this._endpoint + "/" + id, { headers: this._authStore.getHeader })
				.then((response: AxiosResponse<TIndividualDto>) => {
					this.setResponse(response.data);
					this.setStatus(HttpMethod.Get, response.status);
					resolve(response);
				})
				.catch((error) => {
					this.setStatus(HttpMethod.Get, error.response.status);

					if (is4xxStatus(error.response.status)) {
						this.handle4xxStatus(error);
					} else if (is5xxStatus(error.response.status)) {
						this.handle5xxStatus(error);
					}
				})
				.finally(() => {
					this.setLoading(HttpMethod.Get, false);
				});
		});
	}

	public async getAll(urlParameters?: URLSearchParams): Promise<AxiosResponse<Page<TDto[]>>> {
		let queryParameters: TQuery | URLSearchParams;
		if (urlParameters === undefined) queryParameters = this.queryParameters;
		else queryParameters = urlParameters;

		this.setLoading(HttpMethod.GetAll, true);
		return await new Promise((resolve) => {
			axios
				.get(this._endpoint, {
					params: queryParameters,
					headers: this._authStore.getHeader
				})
				.then(async (response: AxiosResponse<Page<TDto[]>>) => {
					this.setResponses(response.data);
					this.setStatus(HttpMethod.GetAll, response.status);
					resolve(response);
				})
				.catch((error) => {
					this.setStatus(HttpMethod.GetAll, error.response.status);

					if (is4xxStatus(error.response.status)) {
						this.handle4xxStatus(error);
					} else if (is5xxStatus(error.response.status)) {
						this.handle5xxStatus(error);
					}
				})
				.finally(() => {
					this.setLoading(HttpMethod.GetAll, false);
				});
		});
	}

	public async patch(id: string, patch: Array<JsonPatch<TIndividualDto>>): Promise<AxiosResponse> {
		this.setLoading(HttpMethod.Patch, true);

		const headers = {
			Authorization: this._authStore.getHeader.Authorization,
			"Content-Type": "application/json-patch+json"
		};

		return await new Promise((resolve) => {
			axios
				.patch(this._endpoint + "/" + id, patch, { headers })
				.then(async (response) => {
					this.setStatus(HttpMethod.Patch, response.status);
					await this.getAll();
					resolve(response);
				})
				.catch((error) => {
					this.setStatus(HttpMethod.Patch, error.response.status);

					if (is4xxStatus(error.response.status)) {
						this.handle4xxStatus(error);
					} else if (is5xxStatus(error.response.status)) {
						this.handle5xxStatus(error);
					}
				})
				.finally(() => {
					this.setLoading(HttpMethod.Patch, false);
				});
		});
	}

	public async post(values: TCreateDto): Promise<AxiosResponse> {
		this.setLoading(HttpMethod.Post, true);
		return await new Promise((resolve) => {
			axios
				.post(this._endpoint, values, {
					headers: this._authStore.getHeader
				})
				.then(async (response) => {
					this.setStatus(HttpMethod.Post, response.status);
					await this.getAll();
					resolve(response);
				})
				.catch((error) => {
					this.setStatus(HttpMethod.Post, error.response.status);

					if (is4xxStatus(error.response.status)) {
						this.handle4xxStatus(error);
					} else if (is5xxStatus(error.response.status)) {
						this.handle5xxStatus(error);
					}
				})
				.finally(() => {
					this.setLoading(HttpMethod.Post, false);
				});
		});
	}

	public async put(id: string, values: TUpdateDto): Promise<AxiosResponse> {
		this.setLoading(HttpMethod.Put, true);
		return await new Promise((resolve) => {
			axios
				.put(this._endpoint + "/" + id, values, { headers: this._authStore.getHeader })
				.then(async (response) => {
					this.setStatus(HttpMethod.Put, response.status);
					await this.getAll();
					resolve(response);
				})
				.catch((error) => {
					this.setStatus(HttpMethod.Put, error.response.status);

					if (is4xxStatus(error.response.status)) {
						this.handle4xxStatus(error);
					} else if (is5xxStatus(error.response.status)) {
						this.handle5xxStatus(error);
					}
				})
				.finally(() => {
					this.setLoading(HttpMethod.Put, false);
				});
		});
	}

	public async delete(id: string): Promise<AxiosResponse> {
		this.setLoading(HttpMethod.Delete, true);
		return await new Promise((resolve) => {
			axios
				.delete(this._endpoint + "/" + id, {
					headers: this._authStore.getHeader
				})
				.then(async (response) => {
					this.setStatus(HttpMethod.Delete, response.status);
					await this.getAll();
					resolve(response);
				})
				.catch((error) => {
					this.setStatus(HttpMethod.Delete, error.response.status);

					if (is4xxStatus(error.response.status)) {
						this.handle4xxStatus(error);
					} else if (is5xxStatus(error.response.status)) {
						this.handle5xxStatus(error);
					}
				})
				.finally(() => {
					this.setLoading(HttpMethod.Delete, false);
				});
		});
	}
}

export default ApiStore;
