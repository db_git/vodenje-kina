//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { makeAutoObservable } from "mobx";

import ActorGetStore from "../get/ActorGetStore";
import CinemaGetStore from "../get/CinemaGetStore";
import DirectorGetStore from "../get/DirectorGetStore";
import FilmRatingGetStore from "../get/FilmRatingGetStore";
import GenreGetStore from "../get/GenreGetStore";
import HallGetStore from "../get/HallGetStore";
import MovieGetStore from "../get/MovieGetStore";
import PersonGetStore from "../get/PersonGetStore";
import UserCommentGetStore from "../get/UserCommentGetStore";
import UserRatingGetStore from "../get/UserRatingGetStore";
import {
	type IUserRatingGetStore,
	type IUserCommentGetStore,
	type IActorGetStore,
	type IAuthStore,
	type ICinemaGetStore,
	type IDirectorGetStore,
	type IFilmRatingGetStore,
	type IGenreGetStore,
	type IGetRootStore,
	type IHallGetStore,
	type IMovieGetStore,
	type IPersonGetStore
} from "../stores";

class GetRootStore implements IGetRootStore {
	private static rootStore: IGetRootStore | undefined;

	public cinemaGetStore: ICinemaGetStore;
	public filmRatingGetStore: IFilmRatingGetStore;
	public genreGetStore: IGenreGetStore;
	public hallGetStore: IHallGetStore;
	public movieGetStore: IMovieGetStore;
	public actorGetStore: IActorGetStore;
	public directorGetStore: IDirectorGetStore;
	public personGetStore: IPersonGetStore;
	public userCommentGetStore: IUserCommentGetStore;
	public userRatingGetStore: IUserRatingGetStore;

	private constructor(authStore: IAuthStore) {
		this.cinemaGetStore = new CinemaGetStore(authStore);
		this.filmRatingGetStore = new FilmRatingGetStore(authStore);
		this.genreGetStore = new GenreGetStore(authStore);
		this.hallGetStore = new HallGetStore(authStore);
		this.movieGetStore = new MovieGetStore(authStore);
		this.actorGetStore = new ActorGetStore(authStore);
		this.directorGetStore = new DirectorGetStore(authStore);
		this.personGetStore = new PersonGetStore(authStore);
		this.userCommentGetStore = new UserCommentGetStore(authStore);
		this.userRatingGetStore = new UserRatingGetStore(authStore);

		makeAutoObservable(this);
	}

	public static GetInstance(authStore: IAuthStore): IGetRootStore {
		if (this.rootStore === undefined) this.rootStore = new GetRootStore(authStore);
		return this.rootStore;
	}
}

export default GetRootStore;
