//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { makeAutoObservable } from "mobx";

import DashboardStore from "../DashboardStore";
import CinemaApiStore from "../api/CinemaApiStore";
import FilmRatingApiStore from "../api/FilmRatingApiStore";
import FoodApiStore from "../api/FoodApiStore";
import GenreApiStore from "../api/GenreApiStore";
import HallApiStore from "../api/HallApiStore";
import MovieApiStore from "../api/MovieApiStore";
import PersonApiStore from "../api/PersonApiStore";
import ProjectionApiStore from "../api/ProjectionApiStore";
import ReservationApiStore from "../api/ReservationApiStore";
import SeatApiStore from "../api/SeatApiStore";
import SouvenirApiStore from "../api/SouvenirApiStore";
import UserApiStore from "../api/UserApiStore";
import UserCommentApiStore from "../api/UserCommentApiStore";
import UserRatingApiStore from "../api/UserRatingApiStore";
import {
	type IDashboardStore,
	type IUserRatingApiStore,
	type IUserCommentApiStore,
	type IApiRootStore,
	type IAuthStore,
	type ICinemaApiStore,
	type IFilmRatingApiStore,
	type IFoodApiStore,
	type IGenreApiStore,
	type IHallApiStore,
	type IMovieApiStore,
	type IPersonApiStore,
	type IProjectionApiStore,
	type IReservationApiStore,
	type ISeatApiStore,
	type ISouvenirApiStore,
	type IUserApiStore
} from "../stores";

class ApiRootStore implements IApiRootStore {
	private static rootStore: IApiRootStore | undefined;

	public readonly authStore: IAuthStore;
	public readonly dashboardStore: IDashboardStore;
	public readonly cinemaStore: ICinemaApiStore;
	public readonly filmRatingStore: IFilmRatingApiStore;
	public readonly foodStore: IFoodApiStore;
	public readonly genreStore: IGenreApiStore;
	public readonly hallStore: IHallApiStore;
	public readonly movieStore: IMovieApiStore;
	public readonly personStore: IPersonApiStore;
	public readonly projectionStore: IProjectionApiStore;
	public readonly reservationStore: IReservationApiStore;
	public readonly seatStore: ISeatApiStore;
	public readonly souvenirStore: ISouvenirApiStore;
	public readonly userStore: IUserApiStore;
	public readonly userCommentStore: IUserCommentApiStore;
	public readonly userRatingStore: IUserRatingApiStore;

	private constructor(authStore: IAuthStore) {
		this.authStore = authStore;
		this.dashboardStore = new DashboardStore(this.authStore);
		this.cinemaStore = new CinemaApiStore(this.authStore);
		this.filmRatingStore = new FilmRatingApiStore(this.authStore);
		this.foodStore = new FoodApiStore(this.authStore);
		this.genreStore = new GenreApiStore(this.authStore);
		this.hallStore = new HallApiStore(this.authStore);
		this.movieStore = new MovieApiStore(this.authStore);
		this.personStore = new PersonApiStore(this.authStore);
		this.projectionStore = new ProjectionApiStore(this.authStore);
		this.reservationStore = new ReservationApiStore(this.authStore);
		this.seatStore = new SeatApiStore(this.authStore);
		this.souvenirStore = new SouvenirApiStore(this.authStore);
		this.userStore = new UserApiStore(this.authStore);
		this.userCommentStore = new UserCommentApiStore(this.authStore);
		this.userRatingStore = new UserRatingApiStore(this.authStore);

		makeAutoObservable(this);
	}

	public static GetInstance(authStore: IAuthStore): IApiRootStore {
		if (this.rootStore === undefined) this.rootStore = new ApiRootStore(authStore);
		return this.rootStore;
	}
}

export default ApiRootStore;
