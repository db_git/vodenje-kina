//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import axios, { type AxiosResponse } from "axios";
import { action, computed, makeObservable, observable, override, runInAction } from "mobx";

import { type UserRatingFormSubmitValues } from "../../pages/movieDetails/movieDetails";
import { type MovieRating, type UserRating } from "../../types/entities";
import { HttpMethod } from "../../types/enums/Http";
import { type QueryParameters } from "../../types/query";
import { type JsonPatch } from "../../types/shared";
import { is4xxStatus, is5xxStatus } from "../../utils/isStatus";
import {
	NotificationSeverity,
	showCreatedNotification,
	showDeletedNotification,
	showNotification,
	showUpdatedNotification
} from "../../utils/notifications";
import ApiPaths from "../ApiPaths";
import ApiStore from "../common/ApiStore";
import { type IAuthStore, type IUserRatingApiStore } from "../stores";

class UserRatingApiStore
	extends ApiStore<UserRating, QueryParameters, MovieRating, UserRatingFormSubmitValues, Record<string, unknown>>
	implements IUserRatingApiStore
{
	private _showImage: boolean;

	public constructor(authStore: IAuthStore) {
		super(authStore, ApiPaths.userRatings);

		this._showImage = false;

		makeObservable<IUserRatingApiStore, "_showImage">(this, {
			_showImage: observable,

			showImage: computed,
			toggleShowImage: action,

			deleteRating: action,
			updateRating: action,
			post: override
		});
	}

	public get showImage(): boolean {
		return this._showImage;
	}

	public readonly toggleShowImage = (): void => {
		this._showImage = !this._showImage;
	};

	public async post(values: UserRatingFormSubmitValues): Promise<AxiosResponse> {
		this.setLoading(HttpMethod.Post, true);
		return await new Promise((resolve) => {
			axios
				.post(this._endpoint, values, {
					headers: this._authStore.getHeader
				})
				.then(async (response) => {
					this.setStatus(HttpMethod.Post, response.status);
					showCreatedNotification("Rating created.");
					resolve(response);
				})
				.catch((error) => {
					this.setStatus(HttpMethod.Post, error.response.status);

					if (is4xxStatus(error.response.status)) {
						this.handle4xxStatus(error);
					} else if (is5xxStatus(error.response.status)) {
						this.handle5xxStatus(error);
					}
				})
				.finally(() => {
					this.setLoading(HttpMethod.Post, false);
				});
		});
	}

	public async updateRating(id: string, patch: Array<JsonPatch<UserRating>>): Promise<AxiosResponse> {
		this.setLoading(HttpMethod.Patch, true);

		const headers = {
			Authorization: this._authStore.getHeader.Authorization,
			"Content-Type": "application/json-patch+json"
		};

		return await new Promise((resolve) => {
			void axios
				.patch(this._endpoint + "/" + id, patch, { headers })
				.then((response) => {
					this.setStatus(HttpMethod.Patch, response.status);

					runInAction(() => {
						showUpdatedNotification("Rating updated.");
					});

					resolve(response);
				})
				.catch((error) => {
					this.setStatus(HttpMethod.Patch, error.response.status);

					runInAction(() => {
						showNotification({
							title: "Failed to update rating.",
							severity: NotificationSeverity.WARNING
						});
					});
				})
				.finally(() => {
					this.setLoading(HttpMethod.Patch, false);
				});
		});
	}

	public readonly deleteRating = async (
		movieId: string,
		userId: string,
		showMessage: boolean = true
	): Promise<void> => {
		this.setLoading(HttpMethod.Delete, true);

		await axios
			.delete(this._endpoint, {
				headers: this._authStore.getHeader,
				params: {
					movie: movieId,
					user: userId
				}
			})
			.then((response) => {
				this.setStatus(HttpMethod.Delete, response.status);

				runInAction(() => {
					if (showMessage) showDeletedNotification("Rating deleted.");
				});
			})
			.catch((error) => {
				this.setStatus(HttpMethod.Delete, error.response.status);

				runInAction(() => {
					showNotification({
						title: "Failed to delete rating.",
						severity: NotificationSeverity.WARNING
					});
				});
			})
			.finally(() => {
				this.setLoading(HttpMethod.Delete, false);
			});
	};
}

export default UserRatingApiStore;
