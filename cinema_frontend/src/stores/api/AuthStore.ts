//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import axios, { type AxiosError, type AxiosResponse } from "axios";
import { action, computed, makeObservable, observable, runInAction } from "mobx";

import { type User, type UserToken } from "../../types/entities";
import { JsonPatchOperation } from "../../types/enums/Http";
import { type JsonPatch } from "../../types/shared";
import { is4xxStatus, is5xxStatus } from "../../utils/isStatus";
import { NotificationSeverity, showNotification } from "../../utils/notifications";
import parseJwt from "../../utils/parseJwt";
import ApiPaths from "../ApiPaths";
import { type IAuthStore, type NotificationResponse } from "../stores";

class AuthStore implements IAuthStore {
	private static authStore: IAuthStore | undefined;
	private readonly _endpoint: string;
	private _loading: boolean;
	private _header: { Authorization: string };
	private _token: string;
	private _status: number;
	private _verificationEmailSent: boolean;
	private _emailVerified: boolean;
	private _account: User | undefined;
	private _nameUpdateLoading: boolean;
	private _imageUpdateLoading: boolean;
	private _forgotPasswordEmailSent: boolean;
	private _passwordIsReset: boolean;
	private _passwordChangeLoading: boolean;

	private constructor() {
		this._token = "";
		this._status = 0;
		this._loading = false;
		this._header = { Authorization: "" };
		this._endpoint = ApiPaths.users;
		this._verificationEmailSent = false;
		this._emailVerified = false;
		this._nameUpdateLoading = false;
		this._imageUpdateLoading = false;
		this._forgotPasswordEmailSent = false;
		this._passwordIsReset = false;
		this._passwordChangeLoading = false;

		makeObservable<
			IAuthStore,
			| "_account"
			| "_emailVerified"
			| "_endpoint"
			| "_forgotPasswordEmailSent"
			| "_header"
			| "_imageUpdateLoading"
			| "_instance"
			| "_loading"
			| "_nameUpdateLoading"
			| "_passwordChangeLoading"
			| "_passwordIsReset"
			| "_status"
			| "_token"
			| "_verificationEmailSent"
			| "handleCatch"
			| "handleLogin"
			| "setEmailVerified"
			| "setForgotPasswordEmailSent"
			| "setLoading"
			| "setPasswordChangeLoading"
			| "setPasswordIsReset"
			| "setVerificationEmailSent"
		>(this, {
			handleCatch: false,
			_instance: false,

			_endpoint: observable,
			_status: observable,
			_header: observable,
			_token: observable,
			_loading: observable,
			_verificationEmailSent: observable,
			_emailVerified: observable,
			_account: observable,
			_nameUpdateLoading: observable,
			_imageUpdateLoading: observable,
			_forgotPasswordEmailSent: observable,
			_passwordIsReset: observable,
			_passwordChangeLoading: observable,

			status: computed,
			loading: computed,
			isAdmin: computed,
			isLoggedIn: computed,
			getUserId: computed,
			getUserName: computed,
			getToken: computed,
			getHeader: computed,
			emailVerified: computed,
			verificationEmailSent: computed,
			account: computed,
			nameUpdateLoading: computed,
			imageUpdateLoading: computed,
			forgotPasswordEmailSent: computed,
			passwordIsReset: computed,
			passwordChangeLoading: computed,

			setVerificationEmailSent: action,
			setEmailVerified: action,
			setStatus: action,
			setToken: action,
			setLoading: action,
			setHeader: action,
			handleLogin: action,
			setAccount: action,
			setNameUpdateLoading: action,
			setImageUpdateLoading: action,
			setForgotPasswordEmailSent: action,
			setPasswordIsReset: action,
			setPasswordChangeLoading: action,

			updateImage: action,
			updateName: action,
			accountInformation: action,
			verifyEmail: action,
			login: action,
			register: action,
			logout: action,
			forgotPassword: action,
			resetPassword: action,
			updatePassword: action
		});

		const token = sessionStorage.getItem("token");
		if (token !== null && token !== "") {
			this.handleLogin(token);
		}
	}

	public get forgotPasswordEmailSent(): boolean {
		return this._forgotPasswordEmailSent;
	}

	private readonly setForgotPasswordEmailSent = (isSent: boolean): void => {
		this._forgotPasswordEmailSent = isSent;
	};

	public get passwordIsReset(): boolean {
		return this._passwordIsReset;
	}

	private readonly setPasswordIsReset = (isReset: boolean): void => {
		this._passwordIsReset = isReset;
	};

	public get loading(): boolean {
		return this._loading;
	}

	public readonly setLoading = (loading: boolean) => {
		this._loading = loading;
	};

	public get status(): number {
		return this._status;
	}

	public readonly setStatus = (status: number) => {
		this._status = status;
	};

	public get account(): User | undefined {
		return this._account;
	}

	public readonly setAccount = (account: User | undefined): void => {
		this._account = account;
	};

	public get nameUpdateLoading(): boolean {
		return this._nameUpdateLoading;
	}

	public readonly setNameUpdateLoading = (loading: boolean) => {
		this._nameUpdateLoading = loading;
	};

	public get imageUpdateLoading(): boolean {
		return this._imageUpdateLoading;
	}

	public readonly setImageUpdateLoading = (loading: boolean) => {
		this._imageUpdateLoading = loading;
	};

	private readonly handleCatch = (error: AxiosError<NotificationResponse>) => {
		if (!error || !error.response) return;

		this.setStatus(error.response.status);

		if (is4xxStatus(error.response.status) && Boolean(error.response.data)) {
			showNotification({
				severity: error.response.data.severity,
				message: error.response.data.message,
				title: error.response.data.title
			});
		} else if (is5xxStatus(error.response.status)) {
			showNotification({
				severity: error.response.data.severity ?? NotificationSeverity.ERROR,
				message: error.response.data.message ?? "An unknown error has occurred.",
				title: error.response.data.title
			});
		}
	};

	public readonly updateName = async (name: string): Promise<void> => {
		this.setNameUpdateLoading(true);

		const data: Array<JsonPatch<{ name: string }>> = [
			{
				path: "/name",
				op: JsonPatchOperation.Replace,
				value: name
			}
		];

		const headers = {
			Authorization: this.getHeader.Authorization,
			"Content-Type": "application/json-patch+json"
		};

		await new Promise((resolve) => {
			axios
				.patch(this._endpoint, data, { headers })
				.then((response) => {
					this.setStatus(response.status);

					showNotification({
						title: "Success",
						severity: NotificationSeverity.INFO,
						message: "Name updated successfully."
					});

					void this.accountInformation();
					resolve(undefined);
				})
				.catch(this.handleCatch)
				.finally(() => {
					this.setNameUpdateLoading(false);
				});
		});
	};

	public readonly updateImage = async (image: string): Promise<void> => {
		this.setImageUpdateLoading(true);

		const data: Array<JsonPatch<{ image: string }>> = [
			{
				path: "/image",
				op: JsonPatchOperation.Replace,
				value: image
			}
		];

		const headers = {
			Authorization: this.getHeader.Authorization,
			"Content-Type": "application/json-patch+json"
		};

		await new Promise((resolve) => {
			axios
				.patch(this._endpoint, data, { headers })
				.then((response) => {
					this.setStatus(response.status);

					showNotification({
						title: "Success",
						severity: NotificationSeverity.INFO,
						message: "Image updated successfully."
					});

					void this.accountInformation();
					resolve(undefined);
				})
				.catch(this.handleCatch)
				.finally(() => {
					this.setImageUpdateLoading(false);
				});
		});
	};

	public readonly accountInformation = async (): Promise<void> => {
		this.setLoading(true);

		await axios
			.get(`${this._endpoint}/account`, { headers: this.getHeader })
			.then((response: AxiosResponse<User>) => {
				this.setStatus(response.status);
				this.setAccount(response.data);
			})
			.catch(this.handleCatch)
			.finally(() => {
				this.setLoading(false);
			});
	};

	public readonly verifyEmail = async (userId: string, verificationCode: string): Promise<void> => {
		this.setLoading(true);

		await axios
			.post(`${this._endpoint}/verify-email`, { userId, verificationCode })
			.then(async (response) => {
				this.setStatus(response.status);
				this.setEmailVerified(true);

				runInAction(() => {
					showNotification({
						title: "Verification successful",
						message: "Thank you for registering.",
						severity: NotificationSeverity.INFO
					});
				});
			})
			.catch(this.handleCatch)
			.finally(() => {
				this.setLoading(false);
			});
	};

	public readonly login = async (values: Record<string, unknown>): Promise<void> => {
		this.setLoading(true);

		await axios
			.post(`${this._endpoint}/login`, values)
			.then(async (response: AxiosResponse<UserToken>) => {
				this.setStatus(response.status);
				this.handleLogin(response.data.token);

				runInAction(() => {
					showNotification({
						title: "Login successful",
						message: `Welcome back, ${this.getUserName}!`,
						severity: NotificationSeverity.INFO
					});
				});
			})
			.catch(this.handleCatch)
			.finally(() => {
				this.setLoading(false);
			});
	};

	public readonly register = async (values: Record<string, unknown>): Promise<void> => {
		this.setLoading(true);

		await axios
			.post(`${this._endpoint}/register`, values)
			.then(async (response) => {
				this.setStatus(response.status);
				this.setVerificationEmailSent(true);
			})
			.catch(this.handleCatch)
			.finally(() => {
				this.setLoading(false);
			});
	};

	public readonly resetPassword = async (
		email: string,
		password: string,
		passwordResetCode: string
	): Promise<void> => {
		this.setLoading(true);

		await axios
			.post(`${this._endpoint}/reset-password`, { email, password, passwordResetCode })
			.then(async (response: AxiosResponse<UserToken>) => {
				this.setStatus(response.status);
				this.setPasswordIsReset(true);
				this.handleLogin(response.data.token);

				runInAction(() => {
					showNotification({
						title: "Password reset successful",
						message: `Welcome back, ${this.getUserName}!`,
						severity: NotificationSeverity.INFO
					});
				});
			})
			.catch(this.handleCatch)
			.finally(() => {
				this.setLoading(false);
			});
	};

	public readonly forgotPassword = async (email: string): Promise<void> => {
		this.setLoading(true);

		await axios
			.post(`${this._endpoint}/forgot-password`, { email })
			.then((response) => {
				this.setStatus(response.status);
				this.setForgotPasswordEmailSent(true);
			})
			.catch(this.handleCatch)
			.finally(() => {
				this.setLoading(false);
			});
	};

	public readonly updatePassword = async (oldPassword: string, newPassword: string): Promise<void> => {
		this.setPasswordChangeLoading(true);

		await axios
			.post(
				`${this._endpoint}/change-password`,
				{ oldPassword, newPassword },
				{
					headers: this.getHeader
				}
			)
			.then((response) => {
				this.setStatus(response.status);

				runInAction(() => {
					showNotification({
						title: "Password changed successfully",
						severity: NotificationSeverity.INFO
					});
				});
			})
			.catch(this.handleCatch)
			.finally(() => {
				this.setPasswordChangeLoading(false);
			});
	};

	public get passwordChangeLoading(): boolean {
		return this._passwordChangeLoading;
	}

	private readonly setPasswordChangeLoading = (loading: boolean): void => {
		this._passwordChangeLoading = loading;
	};

	public logout = (): void => {
		this.setToken("");
		sessionStorage.removeItem("token");
	};

	public get verificationEmailSent(): boolean {
		return this._verificationEmailSent;
	}

	private readonly setVerificationEmailSent = (isSent: boolean) => {
		this._verificationEmailSent = isSent;
	};

	public get emailVerified(): boolean {
		return this._emailVerified;
	}

	private readonly setEmailVerified = (isVerified: boolean) => {
		this._emailVerified = isVerified;
	};

	public get isAdmin(): boolean {
		const role = parseJwt(this.getToken, "role");
		if (role === null) return false;

		if (Array.isArray(role)) {
			for (const userRole of role) {
				if (userRole.toLowerCase().includes("admin")) return true;
			}
		} else if (role) {
			return role.toLowerCase().includes("admin");
		} else {
			return false;
		}

		return false;
	}

	public get isLoggedIn(): boolean {
		return !(this.getToken === null || this.getToken === "");
	}

	public get getUserId(): string | null {
		const id = parseJwt(this.getToken, "sub");
		if (id !== null && !Array.isArray(id)) return id;
		return null;
	}

	public get getUserName(): string | null {
		const name = parseJwt(this.getToken, "name");
		if (name !== null && !Array.isArray(name)) return name;
		return null;
	}

	public get getToken(): string | null {
		return this._token;
	}

	public setToken = (token: string) => {
		this._token = token;
	};

	public get getHeader(): { Authorization: string } {
		return this._header;
	}

	public setHeader = (token: string): void => {
		this._header = { Authorization: "Bearer " + token };
	};

	public handleLogin = (token: string | null): void => {
		if (token !== null && token !== "") {
			this.setToken(token);
			sessionStorage.setItem("token", token);
			this.setHeader(token);
		}
	};

	public static GetInstance(): IAuthStore {
		if (this.authStore === undefined) this.authStore = new AuthStore();
		return this.authStore;
	}
}

export default AuthStore;
