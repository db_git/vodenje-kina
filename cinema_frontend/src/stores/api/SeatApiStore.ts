//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type AxiosResponse } from "axios";
import { action, makeObservable, override } from "mobx";

import { type SeatFormSubmitValues } from "../../pages/admin/seat/seat";
import { type Seat } from "../../types/entities";
import { type QueryParameterOptions, SeatQueryParameterOptions } from "../../types/enums/Stores";
import { type SeatQueryParameters } from "../../types/query";
import { showCreatedNotification, showUpdatedNotification } from "../../utils/notifications";
import ApiPaths from "../ApiPaths";
import ApiStore from "../common/ApiStore";
import { type IAuthStore, type ISeatApiStore } from "../stores";

class SeatApiStore
	extends ApiStore<Seat, SeatQueryParameters, Seat, SeatFormSubmitValues, SeatFormSubmitValues>
	implements ISeatApiStore
{
	public constructor(authStore: IAuthStore) {
		super(authStore, ApiPaths.seats);

		makeObservable(this, {
			setSeatQueryParameters: action,
			post: override,
			put: override
		});
	}

	public readonly setSeatQueryParameters = (
		type: QueryParameterOptions | SeatQueryParameterOptions,
		value?: number | string | undefined
	): void => {
		if (type === SeatQueryParameterOptions.Hall) {
			if (typeof value === "string" || value === undefined) this.queryParameters.hall = value;
			return;
		}

		this.setQueryParameters(type as QueryParameterOptions, value);
	};

	public async post(values: SeatFormSubmitValues): Promise<AxiosResponse> {
		return await new Promise((resolve) => {
			void super.post(values).then((response) => {
				showCreatedNotification(`Seat '${values.number}' created successfully.`);
				resolve(response);
			});
		});
	}

	public async put(id: string, values: SeatFormSubmitValues): Promise<AxiosResponse> {
		return await new Promise((resolve) => {
			void super.put(id, values).then((response) => {
				showUpdatedNotification(`Seat '${values.number}' updated successfully.`);
				resolve(response);
			});
		});
	}
}

export default SeatApiStore;
