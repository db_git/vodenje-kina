//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type AxiosResponse } from "axios";
import { action, computed, makeObservable, observable, override } from "mobx";

import { type FilmRatingFormSubmitValues } from "../../pages/admin/filmRating/filmRating";
import { type FilmRating } from "../../types/entities";
import { type QueryParameters } from "../../types/query";
import { showCreatedNotification, showUpdatedNotification } from "../../utils/notifications";
import ApiPaths from "../ApiPaths";
import ApiStore from "../common/ApiStore";
import { type IAuthStore, type IFilmRatingApiStore } from "../stores";

class FilmRatingApiStore
	extends ApiStore<FilmRating, QueryParameters, FilmRating, FilmRatingFormSubmitValues, FilmRatingFormSubmitValues>
	implements IFilmRatingApiStore
{
	private _showDescription: boolean;

	public constructor(authStore: IAuthStore) {
		super(authStore, ApiPaths.filmRatings);

		this._showDescription = true;

		makeObservable<FilmRatingApiStore, "_showDescription">(this, {
			_showDescription: observable,
			showDescription: computed,
			toggleShowDescription: action,
			post: override,
			put: override
		});
	}

	public get showDescription() {
		return this._showDescription;
	}

	public readonly toggleShowDescription = (): void => {
		this._showDescription = !this._showDescription;
	};

	public async post(values: FilmRatingFormSubmitValues): Promise<AxiosResponse> {
		return await new Promise((resolve) => {
			void super.post(values).then((response) => {
				showCreatedNotification(`Film rating '${values.type}' created successfully.`);
				resolve(response);
			});
		});
	}

	public async put(id: string, values: FilmRatingFormSubmitValues): Promise<AxiosResponse> {
		return await new Promise((resolve) => {
			void super.put(id, values).then((response) => {
				showUpdatedNotification(`Film rating '${values.type}' updated successfully.`);
				resolve(response);
			});
		});
	}
}

export default FilmRatingApiStore;
