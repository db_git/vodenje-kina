//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type AxiosResponse } from "axios";
import { action, computed, makeObservable, observable, override } from "mobx";

import { type PersonFormSubmitValues } from "../../pages/admin/person/person";
import { type Person } from "../../types/entities";
import { PersonQueryParameterOptions, type QueryParameterOptions } from "../../types/enums/Stores";
import { type PersonQueryParameters } from "../../types/query";
import { showCreatedNotification, showUpdatedNotification } from "../../utils/notifications";
import ApiPaths from "../ApiPaths";
import ApiStore from "../common/ApiStore";
import { type IAuthStore, type IPersonApiStore } from "../stores";

class PersonApiStore
	extends ApiStore<Person, PersonQueryParameters, Person, PersonFormSubmitValues, PersonFormSubmitValues>
	implements IPersonApiStore
{
	private _showPicture: boolean;
	private _showBiography: boolean;
	private _showDateOfBirth: boolean;
	private _showDateOfDeath: boolean;
	private _showMoviesCount: boolean;

	public constructor(authStore: IAuthStore) {
		super(authStore, ApiPaths.people);

		this._showPicture = false;
		this._showBiography = false;
		this._showDateOfBirth = true;
		this._showDateOfDeath = false;
		this._showMoviesCount = true;

		makeObservable<
			PersonApiStore,
			"_showBiography" | "_showDateOfBirth" | "_showDateOfDeath" | "_showMoviesCount" | "_showPicture"
		>(this, {
			_showPicture: observable,
			_showBiography: observable,
			_showDateOfBirth: observable,
			_showDateOfDeath: observable,
			_showMoviesCount: observable,

			showPicture: computed,
			showBiography: computed,
			showDateOfBirth: computed,
			showDateOfDeath: computed,
			showMoviesCount: computed,

			toggleShowPicture: action,
			toggleShowBiography: action,
			toggleShowDateOfBirth: action,
			toggleShowDateOfDeath: action,
			toggleShowMoviesCount: action,

			setPersonQueryParameters: action,

			post: override,
			put: override
		});
	}

	public readonly setPersonQueryParameters = (
		type: PersonQueryParameterOptions | QueryParameterOptions,
		value: number | string | undefined
	): void => {
		if (typeof value === "number" || value === undefined) {
			switch (type) {
				case PersonQueryParameterOptions.Day:
					this.queryParameters.day = value;
					break;
				case PersonQueryParameterOptions.Month:
					this.queryParameters.month = value;
					break;
				default:
					break;
			}
		}

		this.setQueryParameters(type as QueryParameterOptions, value);
	};

	public get showPicture() {
		return this._showPicture;
	}

	public get showBiography(): boolean {
		return this._showBiography;
	}

	public get showDateOfBirth(): boolean {
		return this._showDateOfBirth;
	}

	public get showDateOfDeath(): boolean {
		return this._showDateOfDeath;
	}

	public get showMoviesCount(): boolean {
		return this._showMoviesCount;
	}

	public readonly toggleShowPicture = (): void => {
		this._showPicture = !this._showPicture;
	};

	public readonly toggleShowBiography = (): void => {
		this._showBiography = !this._showBiography;
	};

	public readonly toggleShowDateOfBirth = (): void => {
		this._showDateOfBirth = !this._showDateOfBirth;
	};

	public readonly toggleShowDateOfDeath = (): void => {
		this._showDateOfDeath = !this._showDateOfDeath;
	};

	public readonly toggleShowMoviesCount = (): void => {
		this._showMoviesCount = !this._showMoviesCount;
	};

	public async post(values: PersonFormSubmitValues): Promise<AxiosResponse> {
		return await new Promise((resolve) => {
			void super.post(values).then((response) => {
				showCreatedNotification(`'${values.name}' created successfully.`);
				resolve(response);
			});
		});
	}

	public async put(id: string, values: PersonFormSubmitValues): Promise<AxiosResponse> {
		return await new Promise((resolve) => {
			void super.put(id, values).then((response) => {
				showUpdatedNotification(`'${values.name}' updated successfully.`);
				resolve(response);
			});
		});
	}
}

export default PersonApiStore;
