//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type AxiosResponse } from "axios";
import { action, computed, makeObservable, observable, override } from "mobx";

import { type SouvenirFormSubmitValues } from "../../pages/admin/souvenir/souvenir";
import { type Souvenir } from "../../types/entities";
import { type QueryParameterOptions, SouvenirQueryParameterOptions } from "../../types/enums/Stores";
import { type SouvenirQueryParameters } from "../../types/query";
import { showCreatedNotification, showUpdatedNotification } from "../../utils/notifications";
import ApiPaths from "../ApiPaths";
import ApiStore from "../common/ApiStore";
import { type IAuthStore, type ISouvenirApiStore } from "../stores";

class SouvenirApiStore
	extends ApiStore<Souvenir, SouvenirQueryParameters, Souvenir, SouvenirFormSubmitValues, SouvenirFormSubmitValues>
	implements ISouvenirApiStore
{
	private _showImage: boolean;

	public constructor(authStore: IAuthStore) {
		super(authStore, ApiPaths.souvenirs);

		this._showImage = false;

		makeObservable<SouvenirApiStore, "_showImage">(this, {
			_showImage: observable,

			showImage: computed,

			toggleShowImage: action,
			setSouvenirQueryParameters: action,

			post: override,
			put: override
		});
	}

	public get showImage() {
		return this._showImage;
	}

	public readonly toggleShowImage = (): void => {
		this._showImage = !this._showImage;
	};

	public readonly setSouvenirQueryParameters = (
		type: QueryParameterOptions | SouvenirQueryParameterOptions,
		value: number | string | undefined
	): void => {
		if (type === SouvenirQueryParameterOptions.AvailableOnly) {
			if (value === undefined || typeof value === "number") this.queryParameters.availableOnly = value;
			return;
		}

		this.setQueryParameters(type as QueryParameterOptions, value);
	};

	public async post(values: SouvenirFormSubmitValues): Promise<AxiosResponse> {
		return await new Promise((resolve) => {
			void super.post(values).then((response) => {
				showCreatedNotification(`Souvenir '${values.name}' created successfully.`);
				resolve(response);
			});
		});
	}

	public async put(id: string, values: SouvenirFormSubmitValues): Promise<AxiosResponse> {
		return await new Promise((resolve) => {
			void super.put(id, values).then((response) => {
				showUpdatedNotification(`Souvenir '${values.name}' updated successfully.`);
				resolve(response);
			});
		});
	}
}

export default SouvenirApiStore;
