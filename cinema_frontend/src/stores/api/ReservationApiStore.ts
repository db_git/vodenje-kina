//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import axios, { type AxiosResponse } from "axios";
import { action, makeObservable } from "mobx";

import { type Reservation } from "../../types/entities";
import { HttpMethod } from "../../types/enums/Http";
import { type QueryParameters } from "../../types/query";
import { type Page } from "../../types/stores";
import { is4xxStatus, is5xxStatus } from "../../utils/isStatus";
import { NotificationSeverity, showNotification } from "../../utils/notifications";
import ApiPaths from "../ApiPaths";
import ApiStore from "../common/ApiStore";
import { type Order, type IAuthStore, type IReservationApiStore } from "../stores";

class ReservationApiStore
	extends ApiStore<Reservation, QueryParameters, Reservation, Record<string, unknown>, Record<string, unknown>>
	implements IReservationApiStore
{
	public constructor(authStore: IAuthStore) {
		super(authStore, ApiPaths.reservations);

		makeObservable<IReservationApiStore>(this, {
			getAllAccount: action,
			postRange: action
		});
	}

	public async getAllAccount(urlParameters?: URLSearchParams): Promise<AxiosResponse<Page<Reservation[]>>> {
		let queryParameters: QueryParameters | URLSearchParams;
		if (urlParameters === undefined) queryParameters = this.queryParameters;
		else queryParameters = urlParameters;

		this.setLoading(HttpMethod.GetAll, true);
		return await new Promise((resolve) => {
			axios
				.get(`${this._endpoint}/account`, {
					params: queryParameters,
					headers: this._authStore.getHeader
				})
				.then(async (response: AxiosResponse<Page<Reservation[]>>) => {
					this.setResponses(response.data);
					this.setStatus(HttpMethod.GetAll, response.status);
					resolve(response);
				})
				.catch((error) => {
					this.setStatus(HttpMethod.GetAll, error.response.status);

					if (is4xxStatus(error.response.status)) {
						this.handle4xxStatus(error);
					} else if (is5xxStatus(error.response.status)) {
						this.handle5xxStatus(error);
					}
				})
				.finally(() => {
					this.setLoading(HttpMethod.GetAll, false);
				});
		});
	}

	public async postRange(values: Order[]): Promise<AxiosResponse> {
		this.setLoading(HttpMethod.Post, true);

		const reservations: unknown[] = [];
		for (const order of values) {
			const reservation = {
				projection: order.projection.id,
				seats: order.seats.map((seat) => {
					return seat.id;
				}),
				foodOrders: order.foods
					? order.foods.map((food) => {
							return { id: food.food.id, quantity: food.quantity };
					  })
					: undefined,
				souvenirOrders: order.souvenirs
					? order.souvenirs.map((souvenir) => {
							return { id: souvenir.souvenir.id, quantity: souvenir.quantity };
					  })
					: undefined
			};

			reservations.push(reservation);
		}

		return await new Promise((resolve) => {
			axios
				.post(this._endpoint, reservations, {
					headers: this._authStore.getHeader
				})
				.then(async (response) => {
					this.setStatus(HttpMethod.Post, response.status);

					showNotification({
						severity: NotificationSeverity.INFO,
						title: "Order created",
						message: "Check your email for more information."
					});

					resolve(response);
				})
				.catch((error) => {
					this.setStatus(HttpMethod.Post, error.response.status);

					if (is4xxStatus(error.response.status)) {
						this.handle4xxStatus(error);
					} else if (is5xxStatus(error.response.status)) {
						this.handle5xxStatus(error);
					}
				})
				.finally(() => {
					this.setLoading(HttpMethod.Post, false);
				});
		});
	}
}

export default ReservationApiStore;
