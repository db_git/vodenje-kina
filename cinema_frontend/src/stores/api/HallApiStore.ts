//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type AxiosResponse } from "axios";
import { makeObservable, override } from "mobx";

import { type HallFormSubmitValues } from "../../pages/admin/hall/hall";
import { type Hall } from "../../types/entities";
import { type QueryParameters } from "../../types/query";
import { showCreatedNotification, showUpdatedNotification } from "../../utils/notifications";
import ApiPaths from "../ApiPaths";
import ApiStore from "../common/ApiStore";
import { type IAuthStore, type IHallApiStore } from "../stores";

class HallApiStore
	extends ApiStore<Hall, QueryParameters, Hall, HallFormSubmitValues, HallFormSubmitValues>
	implements IHallApiStore
{
	public constructor(authStore: IAuthStore) {
		super(authStore, ApiPaths.halls);

		makeObservable(this, {
			post: override,
			put: override
		});
	}

	public async post(values: HallFormSubmitValues): Promise<AxiosResponse> {
		return await new Promise((resolve) => {
			void super.post(values).then((response) => {
				showCreatedNotification(`Hall '${values.name}' created successfully.`);
				resolve(response);
			});
		});
	}

	public async put(id: string, values: HallFormSubmitValues): Promise<AxiosResponse> {
		return await new Promise((resolve) => {
			void super.put(id, values).then((response) => {
				showUpdatedNotification(`Hall '${values.name}' updated successfully.`);
				resolve(response);
			});
		});
	}
}

export default HallApiStore;
