//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import axios, { type AxiosResponse } from "axios";
import { action, computed, makeObservable, observable, override } from "mobx";

import { type ProjectionFormSubmitValues } from "../../pages/admin/projection/projection";
import { type Projection, type IndividualSeat } from "../../types/entities";
import { HttpMethod, HttpStatusCode } from "../../types/enums/Http";
import { ProjectionQueryParameterOptions, type QueryParameterOptions } from "../../types/enums/Stores";
import { type ProjectionQueryParameters } from "../../types/query";
import { showCreatedNotification, showUpdatedNotification } from "../../utils/notifications";
import ApiPaths from "../ApiPaths";
import ApiStore from "../common/ApiStore";
import { type IAuthStore, type IProjectionApiStore } from "../stores";

class ProjectionApiStore
	extends ApiStore<
		Projection,
		ProjectionQueryParameters,
		Projection,
		ProjectionFormSubmitValues,
		ProjectionFormSubmitValues
	>
	implements IProjectionApiStore
{
	private _takenSeats: IndividualSeat[] | undefined;

	public constructor(authStore: IAuthStore) {
		super(authStore, ApiPaths.projections);

		this._takenSeats = undefined;

		makeObservable<IProjectionApiStore, "_takenSeats" | "setTakenSeats">(this, {
			_takenSeats: observable,

			takenSeats: computed,

			post: override,
			put: override,
			urlSearchString: override,
			setUrlSearchString: override,

			setTakenSeats: action,
			setProjectionQueryParameters: action,
			getTakenSeats: action
		});
	}

	public get takenSeats(): IndividualSeat[] | undefined {
		return this._takenSeats;
	}

	private readonly setTakenSeats = (takenSeats: IndividualSeat[] | undefined): void => {
		this._takenSeats = takenSeats;
	};

	public get urlSearchString(): string {
		const queryParameters: string[] = [];

		const movie = this.queryParameters.movie;
		const hall = this.queryParameters.hall;
		const startDate = this.queryParameters.startDate;
		const endDate = this.queryParameters.endDate;

		if (movie) queryParameters.push(`movie=${movie}`);
		if (hall) queryParameters.push(`hall=${hall}`);
		if (startDate) queryParameters.push(`startDate=${startDate}`);
		if (endDate) queryParameters.push(`endDate=${endDate}`);

		const parametersCombined = queryParameters.join("&");
		if (parametersCombined) return `${super.urlSearchString}&${parametersCombined}`;
		return `${super.urlSearchString}`;
	}

	public readonly setProjectionQueryParameters = (
		type: ProjectionQueryParameterOptions | QueryParameterOptions,
		value: number | string | undefined
	): void => {
		switch (type) {
			case ProjectionQueryParameterOptions.Movie:
				if (typeof value === "string" || value === undefined) this.queryParameters.movie = value;
				break;
			case ProjectionQueryParameterOptions.Hall:
				if (typeof value === "string" || value === undefined) this.queryParameters.hall = value;
				break;
			case ProjectionQueryParameterOptions.StartDate:
				if (typeof value === "string" || value === undefined) this.queryParameters.startDate = value;
				break;
			case ProjectionQueryParameterOptions.EndDate:
				if (typeof value === "string" || value === undefined) this.queryParameters.endDate = value;
				break;
			default:
				this.setQueryParameters(type as QueryParameterOptions, value);
				break;
		}
	};

	public readonly setUrlSearchString = (query: URLSearchParams): void => {
		super.setUrlSearchString(query);

		const movie = query.get("movie");
		const hall = query.get("hall");
		const startDate = query.get("startDate");
		const endDate = query.get("endDate");

		if (movie) this.setProjectionQueryParameters(ProjectionQueryParameterOptions.Movie, movie);
		if (hall) this.setProjectionQueryParameters(ProjectionQueryParameterOptions.Hall, hall);
		if (startDate) this.setProjectionQueryParameters(ProjectionQueryParameterOptions.StartDate, startDate);
		if (endDate) this.setProjectionQueryParameters(ProjectionQueryParameterOptions.EndDate, endDate);
	};

	public readonly getTakenSeats = async (id: string): Promise<void> => {
		this.setLoading(HttpMethod.Get, true);

		await axios
			.get(`${this._endpoint}/${id}/taken-seats`)
			.then((response: AxiosResponse<IndividualSeat[]>) => {
				this.setStatus(HttpMethod.Get, response.status);

				if (response.status === HttpStatusCode.OK) {
					this.setTakenSeats(response.data);
				} else {
					this.setTakenSeats(undefined);
				}
			})
			.catch((error) => {
				this.setStatus(HttpMethod.Get, error.response.status);
				this.setTakenSeats(undefined);
			})
			.finally(() => {
				this.setLoading(HttpMethod.Get, false);
			});
	};

	public async post(values: ProjectionFormSubmitValues): Promise<AxiosResponse> {
		return await new Promise((resolve) => {
			void super.post(values).then((response) => {
				showCreatedNotification("Projection created successfully.");
				resolve(response);
			});
		});
	}

	public async put(id: string, values: ProjectionFormSubmitValues): Promise<AxiosResponse> {
		return await new Promise((resolve) => {
			void super.put(id, values).then((response) => {
				showUpdatedNotification("Projection updated successfully.");
				resolve(response);
			});
		});
	}
}

export default ProjectionApiStore;
