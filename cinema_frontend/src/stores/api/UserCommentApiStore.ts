//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import axios, { type AxiosResponse } from "axios";
import { action, computed, makeObservable, observable, override, runInAction } from "mobx";

import { type UserCommentFormSubmitValues } from "../../pages/movieDetails/movieDetails";
import { type UserComment } from "../../types/entities";
import { HttpMethod } from "../../types/enums/Http";
import { type QueryParameters } from "../../types/query";
import { type JsonPatch } from "../../types/shared";
import {
	NotificationSeverity,
	showCreatedNotification,
	showDeletedNotification,
	showNotification,
	showUpdatedNotification
} from "../../utils/notifications";
import ApiPaths from "../ApiPaths";
import ApiStore from "../common/ApiStore";
import { type IAuthStore, type IUserCommentApiStore } from "../stores";

class UserCommentApiStore
	extends ApiStore<UserComment, QueryParameters, UserComment, UserCommentFormSubmitValues, Record<string, unknown>>
	implements IUserCommentApiStore
{
	private _canComment: boolean;
	private _edit: UserComment | undefined;
	private _showImage: boolean;

	public constructor(authStore: IAuthStore) {
		super(authStore, ApiPaths.userComments);

		this._canComment = false;
		this._edit = undefined;
		this._showImage = false;

		makeObservable<IUserCommentApiStore, "_canComment" | "_edit" | "_showImage" | "setCanComment">(this, {
			_showImage: observable,
			_canComment: observable,
			_edit: observable,

			canComment: computed,
			edit: computed,
			showImage: computed,

			toggleShowImage: action,
			setEdit: action,
			setCanComment: action,
			getCanComment: action,

			deleteComment: action,
			post: override,
			patch: override
		});
	}

	public get showImage(): boolean {
		return this._showImage;
	}

	public readonly toggleShowImage = (): void => {
		this._showImage = !this._showImage;
	};

	public get canComment(): boolean {
		return this._canComment;
	}

	private readonly setCanComment = (canComment: boolean): void => {
		this._canComment = canComment;
	};

	public get edit(): UserComment | undefined {
		return this._edit;
	}

	public readonly setEdit = (review: UserComment | undefined): void => {
		this._edit = review;
	};

	public async getCanComment(movieId: string): Promise<void> {
		this.setLoading(HttpMethod.Get, true);

		await axios
			.get(`${this._endpoint}/can-comment`, {
				headers: this._authStore.getHeader,
				params: { movie: movieId }
			})
			.then((response) => {
				this.setStatus(HttpMethod.Get, response.status);
				this.setCanComment(true);
			})
			.catch((error) => {
				this.setStatus(HttpMethod.Get, error.response.status);
				this.setCanComment(false);
			})
			.finally(() => {
				this.setLoading(HttpMethod.Get, false);
			});
	}

	public readonly deleteComment = async (
		movieId: string,
		userId: string,
		showMessage: boolean = true
	): Promise<void> => {
		this.setLoading(HttpMethod.Delete, true);

		await axios
			.delete(this._endpoint, {
				headers: this._authStore.getHeader,
				params: {
					movie: movieId,
					user: userId
				}
			})
			.then((response) => {
				this.setStatus(HttpMethod.Delete, response.status);

				runInAction(() => {
					if (showMessage) showDeletedNotification("Comment deleted.");
				});
			})
			.catch((error) => {
				this.setStatus(HttpMethod.Delete, error.response.status);

				runInAction(() => {
					showNotification({
						title: "Failed to delete comment.",
						severity: NotificationSeverity.WARNING
					});
				});
			})
			.finally(() => {
				this.setLoading(HttpMethod.Delete, false);
			});
	};

	public async post(values: UserCommentFormSubmitValues): Promise<AxiosResponse> {
		return await new Promise((resolve) => {
			void super.post(values).then((response) => {
				showCreatedNotification("Comment created.");
				resolve(response);
			});
		});
	}

	public async patch(id: string, patch: Array<JsonPatch<UserComment>>): Promise<AxiosResponse> {
		this.setLoading(HttpMethod.Patch, true);

		const headers = {
			Authorization: this._authStore.getHeader.Authorization,
			"Content-Type": "application/json-patch+json"
		};

		return await new Promise((resolve) => {
			void axios
				.patch(this._endpoint + "/" + id, patch, { headers })
				.then((response) => {
					this.setStatus(HttpMethod.Patch, response.status);

					runInAction(() => {
						showUpdatedNotification("Comment updated.");
					});

					resolve(response);
				})
				.catch((error) => {
					this.setStatus(HttpMethod.Patch, error.response.status);

					runInAction(() => {
						showNotification({
							title: "Failed to update comment.",
							severity: NotificationSeverity.WARNING
						});
					});
				})
				.finally(() => {
					this.setLoading(HttpMethod.Patch, false);
				});
		});
	}
}

export default UserCommentApiStore;
