//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type AxiosResponse } from "axios";
import { action, computed, makeObservable, observable, override } from "mobx";

import { type FoodFormSubmitValues } from "../../pages/admin/food/food";
import { type Food } from "../../types/entities";
import { type QueryParameterOptions, FoodQueryParameterOptions } from "../../types/enums/Stores";
import { type FoodQueryParameters } from "../../types/query";
import { showCreatedNotification, showUpdatedNotification } from "../../utils/notifications";
import ApiPaths from "../ApiPaths";
import ApiStore from "../common/ApiStore";
import { type IAuthStore, type IFoodApiStore } from "../stores";

class FoodApiStore
	extends ApiStore<Food, FoodQueryParameters, Food, FoodFormSubmitValues, FoodFormSubmitValues>
	implements IFoodApiStore
{
	private _showImage: boolean;
	private _showType: boolean;
	private _showSize: boolean;

	public constructor(authStore: IAuthStore) {
		super(authStore, ApiPaths.foods);

		this._showImage = false;
		this._showType = false;
		this._showSize = true;

		makeObservable<FoodApiStore, "_showImage" | "_showSize" | "_showType">(this, {
			_showImage: observable,
			_showType: observable,
			_showSize: observable,

			showImage: computed,
			showType: computed,
			showSize: computed,

			toggleShowImage: action,
			toggleShowType: action,
			toggleShowSize: action,
			setFoodQueryParameters: action,

			post: override,
			put: override
		});
	}

	public get showImage() {
		return this._showImage;
	}

	public get showType() {
		return this._showType;
	}

	public get showSize() {
		return this._showSize;
	}

	public readonly toggleShowImage = (): void => {
		this._showImage = !this._showImage;
	};

	public readonly toggleShowType = (): void => {
		this._showType = !this._showType;
	};

	public readonly toggleShowSize = (): void => {
		this._showSize = !this._showSize;
	};

	public readonly setFoodQueryParameters = (
		type: FoodQueryParameterOptions | QueryParameterOptions,
		value: number | string | undefined
	): void => {
		if (type === FoodQueryParameterOptions.AvailableOnly) {
			if (value === undefined || typeof value === "number") this.queryParameters.availableOnly = value;
			return;
		}

		this.setQueryParameters(type as QueryParameterOptions, value);
	};

	public async post(values: FoodFormSubmitValues): Promise<AxiosResponse> {
		return await new Promise((resolve) => {
			void super.post(values).then((response) => {
				showCreatedNotification(`Food '${values.name}' created successfully.`);
				resolve(response);
			});
		});
	}

	public async put(id: string, values: FoodFormSubmitValues): Promise<AxiosResponse> {
		return await new Promise((resolve) => {
			void super.put(id, values).then((response) => {
				showUpdatedNotification(`Food '${values.name}' updated successfully.`);
				resolve(response);
			});
		});
	}
}

export default FoodApiStore;
