//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { action, computed, makeObservable, observable } from "mobx";

import { type User } from "../../types/entities";
import { type QueryParameters } from "../../types/query";
import ApiPaths from "../ApiPaths";
import ApiStore from "../common/ApiStore";
import { type IAuthStore, type IUserApiStore } from "../stores";

class UserApiStore
	extends ApiStore<User, QueryParameters, User, Record<string, unknown>, Record<string, unknown>>
	implements IUserApiStore
{
	private _showImage: boolean;

	public constructor(authStore: IAuthStore) {
		super(authStore, ApiPaths.users);

		this._showImage = false;

		makeObservable<IUserApiStore, "_showImage">(this, {
			_showImage: observable,

			showImage: computed,

			toggleShowImage: action
		});
	}

	public get showImage(): boolean {
		return this._showImage;
	}

	public readonly toggleShowImage = (): void => {
		this._showImage = !this._showImage;
	};
}

export default UserApiStore;
