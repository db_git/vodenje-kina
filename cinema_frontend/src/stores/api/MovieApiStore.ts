//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { type AxiosResponse } from "axios";
import { action, computed, makeObservable, observable, override, runInAction } from "mobx";

import { type MovieFormSubmitValues } from "../../pages/admin/movie/movie";
import { type IndividualMovie, type Movie } from "../../types/entities";
import { HttpMethod } from "../../types/enums/Http";
import { MovieOrderBy } from "../../types/enums/OrderBy";
import { MovieQueryParameterOptions, QueryParameterOptions } from "../../types/enums/Stores";
import { type MovieQueryParameters } from "../../types/query";
import { type Page } from "../../types/stores";
import { showCreatedNotification, showUpdatedNotification } from "../../utils/notifications";
import ApiPaths from "../ApiPaths";
import ApiStore from "../common/ApiStore";
import { type IAuthStore, type IMovieApiStore } from "../stores";

class MovieApiStore
	extends ApiStore<Movie, MovieQueryParameters, IndividualMovie, MovieFormSubmitValues, MovieFormSubmitValues>
	implements IMovieApiStore
{
	private _showYear: boolean;
	private _showPoster: boolean;
	private _showDuration: boolean;
	private _showTagline: boolean;
	private _showSummary: boolean;
	private _showInCinemas: boolean;
	private _showIsFeatured: boolean;
	private _showIsRecommended: boolean;

	public constructor(authStore: IAuthStore) {
		super(authStore, ApiPaths.movies);
		this.setQueryParameters(QueryParameterOptions.Size, 15);
		this.setMovieQueryParameters(MovieQueryParameterOptions.OrderBy, MovieOrderBy.YearDescending);
		this.setMovieQueryParameters(MovieQueryParameterOptions.ShowUpcoming, 0);

		this._showYear = true;
		this._showPoster = false;
		this._showDuration = true;
		this._showTagline = true;
		this._showSummary = false;
		this._showInCinemas = true;
		this._showIsFeatured = false;
		this._showIsRecommended = false;

		makeObservable<
			MovieApiStore,
			| "_showDuration"
			| "_showInCinemas"
			| "_showIsFeatured"
			| "_showIsRecommended"
			| "_showPoster"
			| "_showSummary"
			| "_showTagline"
			| "_showYear"
		>(this, {
			showIsFeatured: true,
			showIsRecommended: true,
			urlSearchString: override,
			setUrlSearchString: override,
			getAll: override,
			post: override,
			put: override,

			_showYear: observable,
			_showPoster: observable,
			_showDuration: observable,
			_showTagline: observable,
			_showSummary: observable,
			_showInCinemas: observable,
			_showIsFeatured: observable,
			_showIsRecommended: observable,

			showYear: computed,
			showPoster: computed,
			showDuration: computed,
			showTagline: computed,
			showSummary: computed,
			showInCinemas: computed,

			toggleShowYear: action,
			toggleShowPoster: action,
			toggleShowDuration: action,
			toggleShowTagline: action,
			toggleShowSummary: action,
			toggleShowInCinemas: action,
			toggleShowIsFeatured: action,
			toggleShowIsRecommended: action,
			setMovieQueryParameters: action
		});
	}

	public get showYear(): boolean {
		return this._showYear;
	}

	public get showPoster(): boolean {
		return this._showPoster;
	}

	public get showDuration(): boolean {
		return this._showDuration;
	}

	public get showTagline(): boolean {
		return this._showTagline;
	}

	public get showSummary(): boolean {
		return this._showSummary;
	}

	public get showInCinemas(): boolean {
		return this._showInCinemas;
	}

	public get showIsFeatured(): boolean {
		return this._showIsFeatured;
	}

	public get showIsRecommended(): boolean {
		return this._showIsRecommended;
	}

	public readonly toggleShowYear = (): void => {
		this._showYear = !this._showYear;
	};

	public readonly toggleShowPoster = (): void => {
		this._showPoster = !this._showPoster;
	};

	public readonly toggleShowDuration = (): void => {
		this._showDuration = !this._showDuration;
	};

	public readonly toggleShowTagline = (): void => {
		this._showTagline = !this._showTagline;
	};

	public readonly toggleShowSummary = (): void => {
		this._showSummary = !this._showSummary;
	};

	public readonly toggleShowInCinemas = (): void => {
		this._showInCinemas = !this._showInCinemas;
	};

	public readonly toggleShowIsFeatured = (): void => {
		this._showIsFeatured = !this._showIsFeatured;
	};

	public readonly toggleShowIsRecommended = (): void => {
		this._showIsRecommended = !this._showIsRecommended;
	};

	public get urlSearchString(): string {
		const queryParameters: string[] = [];

		const showUpcoming = this.queryParameters.showUpcoming;
		const year = this.queryParameters.year;
		const yearStart = this.queryParameters.yearStart;
		const yearEnd = this.queryParameters.yearEnd;
		const durationUnder = this.queryParameters.durationUnder;
		const durationOver = this.queryParameters.durationOver;
		const filmRating = this.queryParameters.filmRating;
		const hasProjections = this.queryParameters.hasProjections;
		const genres = this.queryParameters.genres;

		if (showUpcoming) queryParameters.push(`showUpcoming=${showUpcoming}`);
		if (year) queryParameters.push(`year=${year}`);
		if (yearStart) queryParameters.push(`yearStart=${yearStart}`);
		if (yearEnd) queryParameters.push(`yearEnd=${yearEnd}`);
		if (durationUnder) queryParameters.push(`durationUnder=${durationUnder}`);
		if (durationOver) queryParameters.push(`durationOver=${durationOver}`);
		if (filmRating) queryParameters.push(`filmRating=${filmRating}`);
		if (hasProjections) queryParameters.push(`hasProjections=${hasProjections}`);
		if (genres) for (const genre of genres) queryParameters.push(`genres=${genre}`);

		const parametersCombined = queryParameters.join("&");
		if (parametersCombined) return `${super.urlSearchString}&${parametersCombined}`;
		return `${super.urlSearchString}`;
	}

	public readonly setMovieQueryParameters = (
		type: MovieQueryParameterOptions | QueryParameterOptions,
		value: string[] | boolean | number | string | undefined
	): void => {
		if (typeof value === "number" || value === undefined) {
			switch (type) {
				case MovieQueryParameterOptions.Year:
					this.queryParameters.year = value;
					break;

				case MovieQueryParameterOptions.YearStart:
					this.queryParameters.yearStart = value;
					break;

				case MovieQueryParameterOptions.YearEnd:
					this.queryParameters.yearEnd = value;
					break;

				case MovieQueryParameterOptions.DurationOver:
					this.queryParameters.durationOver = value;
					break;

				case MovieQueryParameterOptions.DurationUnder:
					this.queryParameters.durationUnder = value;
					break;

				case MovieQueryParameterOptions.ShowUpcoming:
					this.queryParameters.showUpcoming = value;
					break;

				default:
					break;
			}
		}

		if ((typeof value === "boolean" || value === undefined) && type === MovieQueryParameterOptions.HasProjections) {
			this.queryParameters.hasProjections = value;
		}

		switch (type) {
			case MovieQueryParameterOptions.FilmRating:
				if (typeof value === "string" || value === undefined) this.queryParameters.filmRating = value;
				break;

			case MovieQueryParameterOptions.Genres:
				if (Array.isArray(value) || value === undefined) this.queryParameters.genres = value;
				break;

			default:
				this.setQueryParameters(type as QueryParameterOptions, value as number | string | undefined);
				break;
		}
	};

	public readonly setUrlSearchString = (query: URLSearchParams): void => {
		super.setUrlSearchString(query);

		const showUpcoming = query.get("showUpcoming");
		const year = query.get("year");
		const yearStart = query.get("yearStart");
		const yearEnd = query.get("yearEnd");
		const durationUnder = query.get("durationUnder");
		const durationOver = query.get("durationOver");
		const filmRating = query.get("filmRating");
		const hasProjections = query.get("hasProjections");
		const genres = query.getAll("genres");

		if (showUpcoming)
			this.setMovieQueryParameters(MovieQueryParameterOptions.ShowUpcoming, Number.parseInt(showUpcoming, 10));
		if (year) this.setMovieQueryParameters(MovieQueryParameterOptions.Year, Number.parseInt(year, 10));
		if (yearStart)
			this.setMovieQueryParameters(MovieQueryParameterOptions.YearStart, Number.parseInt(yearStart, 10));
		if (yearEnd) this.setMovieQueryParameters(MovieQueryParameterOptions.YearEnd, Number.parseInt(yearEnd, 10));
		if (durationUnder)
			this.setMovieQueryParameters(MovieQueryParameterOptions.DurationUnder, Number.parseInt(durationUnder, 10));
		if (durationOver)
			this.setMovieQueryParameters(MovieQueryParameterOptions.DurationUnder, Number.parseInt(durationOver, 10));
		if (filmRating) this.setMovieQueryParameters(MovieQueryParameterOptions.FilmRating, filmRating);
		if (hasProjections)
			this.setMovieQueryParameters(MovieQueryParameterOptions.HasProjections, hasProjections === "true");
		if (genres.length > 0) this.setMovieQueryParameters(MovieQueryParameterOptions.Genres, genres);
	};

	public readonly getAll = async (urlParameters?: URLSearchParams): Promise<AxiosResponse<Page<Movie[]>>> => {
		return await runInAction(async () => {
			if (urlParameters) return await super.getAll(urlParameters);

			this.setLoading(HttpMethod.GetAll, true);

			const parameters = new URLSearchParams();
			const movieFilterProperties: string[] = [];

			for (const key of Object.values(MovieQueryParameterOptions) as number[] | string[]) {
				if (typeof key === "number") break;
				movieFilterProperties.push(key.charAt(0).toLowerCase() + key.slice(1));
			}

			movieFilterProperties.pop();

			for (const property of movieFilterProperties) {
				// eslint-disable-next-line security/detect-object-injection
				const movieProperty: string | undefined = (this.queryParameters as never)[property];
				if (typeof movieProperty !== "undefined") parameters.append(property, movieProperty);
			}

			if (Array.isArray(this.queryParameters.genres))
				for (const genre of this.queryParameters.genres) parameters.append("genres", genre);

			return await super.getAll(parameters);
		});
	};

	public async post(values: MovieFormSubmitValues): Promise<AxiosResponse> {
		return await new Promise((resolve) => {
			void super.post(values).then((response) => {
				showCreatedNotification(`Movie '${values.title}' created successfully.`);
				resolve(response);
			});
		});
	}

	public async put(id: string, values: MovieFormSubmitValues): Promise<AxiosResponse> {
		return await new Promise((resolve) => {
			void super.put(id, values).then((response) => {
				showUpdatedNotification(`Movie '${values.title}' updated successfully.`);
				resolve(response);
			});
		});
	}
}

export default MovieApiStore;
