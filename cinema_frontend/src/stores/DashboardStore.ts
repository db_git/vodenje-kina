//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import axios, { type AxiosResponse } from "axios";
import { action, computed, makeObservable, observable } from "mobx";

import apiPaths from "./ApiPaths";
import {
	type RevenueStats,
	type MovieStats,
	type DashboardStatus,
	type UserStats,
	type IAuthStore,
	type DashboardLoading,
	type IDashboardStore
} from "./stores";

class DashboardStore implements IDashboardStore {
	private readonly _endpoint: string;
	private readonly _authStore: IAuthStore;

	private readonly _loading: DashboardLoading;
	private readonly _status: DashboardStatus;
	private _movieStats: MovieStats | undefined;
	private _userStats: UserStats | undefined;
	private _revenueStats: RevenueStats | undefined;

	public constructor(authStore: IAuthStore) {
		this._endpoint = apiPaths.dashboard;
		this._authStore = authStore;

		this._loading = {
			users: true,
			movies: true,
			revenue: true
		};

		this._status = {
			users: 0,
			movies: 0,
			revenue: 0
		};

		makeObservable<
			IDashboardStore,
			"_authStore" | "_endpoint" | "_loading" | "_movieStats" | "_revenueStats" | "_status" | "_userStats"
		>(this, {
			_authStore: false,
			_endpoint: false,

			_loading: observable,
			loading: computed,
			setLoading: action,

			_status: observable,
			status: computed,
			setStatus: action,

			_movieStats: observable,
			movieStats: computed,
			setMovieStats: action,

			_userStats: observable,
			userStats: computed,
			setUserStats: action,

			_revenueStats: observable,
			revenueStats: computed,
			setRevenueStats: action,

			getUserStats: action,
			getMovieStats: action,
			getRevenueStats: action
		});
	}

	public get loading(): DashboardLoading {
		return this._loading;
	}

	public readonly setLoading = (type: keyof DashboardLoading, value: boolean): void => {
		// eslint-disable-next-line security/detect-object-injection
		this._loading[type] = value;
	};

	public get status(): DashboardStatus {
		return this._status;
	}

	public readonly setStatus = (type: keyof DashboardStatus, value: number): void => {
		// eslint-disable-next-line security/detect-object-injection
		this._status[type] = value;
	};

	public get movieStats(): MovieStats | undefined {
		return this._movieStats;
	}

	public readonly setMovieStats = (stats: MovieStats | undefined): void => {
		this._movieStats = stats;
	};

	public get userStats(): UserStats | undefined {
		return this._userStats;
	}

	public readonly setUserStats = (stats: UserStats | undefined): void => {
		this._userStats = stats;
	};

	public get revenueStats(): RevenueStats | undefined {
		return this._revenueStats;
	}

	public readonly setRevenueStats = (stats: RevenueStats | undefined): void => {
		this._revenueStats = stats;
	};

	public readonly getMovieStats = async (): Promise<void> => {
		this.setLoading("movies", true);

		await axios
			.get(`${this._endpoint}/movies`, {
				headers: this._authStore.getHeader
			})
			.then((response: AxiosResponse<MovieStats>) => {
				this.setStatus("movies", response.status);
				this.setMovieStats(response.data);
			})
			.catch((error) => {
				this.setStatus("movies", error.response.status);
			})
			.finally(() => {
				this.setLoading("movies", false);
			});
	};

	public readonly getUserStats = async (): Promise<void> => {
		this.setLoading("users", true);

		await axios
			.get(`${this._endpoint}/users`, {
				headers: this._authStore.getHeader
			})
			.then((response: AxiosResponse<UserStats>) => {
				this.setStatus("users", response.status);
				this.setUserStats(response.data);
			})
			.catch((error) => {
				this.setStatus("users", error.response.status);
			})
			.finally(() => {
				this.setLoading("users", false);
			});
	};

	public readonly getRevenueStats = async (): Promise<void> => {
		this.setLoading("revenue", true);

		await axios
			.get(`${this._endpoint}/revenue`, {
				headers: this._authStore.getHeader
			})
			.then((response: AxiosResponse<RevenueStats>) => {
				this.setStatus("revenue", response.status);
				this.setRevenueStats(response.data);
			})
			.catch((error) => {
				this.setStatus("revenue", error.response.status);
			})
			.finally(() => {
				this.setLoading("revenue", false);
			});
	};
}

export default DashboardStore;
