//    Vođenje kina
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { action, computed, makeObservable, observable } from "mobx";

import { type INavbarStore } from "./stores";

class NavbarStore implements INavbarStore {
	private static navbarStore: INavbarStore | undefined;

	private _active: string;
	private _isVisible: boolean;

	private constructor() {
		this._active = location.pathname;
		this._isVisible = true;

		makeObservable<INavbarStore, "_active" | "_isVisible">(this, {
			_active: observable,
			_isVisible: observable,

			active: computed,
			isVisible: computed,

			setActive: action,
			setVisible: action
		});
	}

	public get active(): string {
		return this._active;
	}

	public get isVisible(): boolean {
		return this._isVisible;
	}

	public readonly setActive = (value: string): void => {
		this._active = value;
	};

	public readonly setVisible = (value: boolean): void => {
		this._isVisible = value;
	};

	public static GetInstance(): INavbarStore {
		if (this.navbarStore === undefined) this.navbarStore = new NavbarStore();
		return this.navbarStore;
	}
}

export default NavbarStore;
