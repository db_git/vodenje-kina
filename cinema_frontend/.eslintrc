{
	"parser": "@typescript-eslint/parser",
	"parserOptions": {
		"ecmaVersion": "latest",
		"sourceType": "module",
		"jsx": true,
		"project": [
			"tsconfig.json",
			"vite.config.ts"
		]
	},
	"plugins": [
		"@typescript-eslint",
		"react",
		"eslint-plugin-tsdoc",
		"sonarjs",
		"prettier"
	],
	"extends": [
		"eslint:recommended",
		"plugin:react/recommended",
		"plugin:@typescript-eslint/recommended",
		"plugin:sonarjs/recommended",
		"plugin:security/recommended",
		"canonical",
		"canonical/browser",
		"canonical/json",
		"canonical/module",
		"canonical/react",
		"canonical/typescript",
		"plugin:mobx/recommended",
		"prettier",
		"eslint-config-prettier"
	],
	"rules": {
		"tsdoc/syntax": 1,
		"jsdoc/check-tag-names": 0,
		"@typescript-eslint/unbound-method": 0,
		"@typescript-eslint/method-signature-style": 0,
		"@typescript-eslint/lines-between-class-members": 0,
		"@typescript-eslint/no-unused-vars": 1,
		"typescript-sort-keys/interface": 0,
		"typescript-sort-keys/string-enum": 0,
		"react/iframe-missing-sandbox": 0,
		"react/no-unescaped-entities": 2,
		"canonical/id-match": 0,
		"canonical/export-specifier-newline": 0,
		"canonical/sort-keys": 0,
		"canonical/import-specifier-newline": 0,
		"canonical/destructuring-property-newline": 0,
		"prettier/prettier": 1,
		"mobx/no-anonymous-observer": 0,
		"mobx/missing-observer": 0,
		"react/forbid-component-props": 0,
		"react/jsx-handler-names": 0,
		"node/no-process-env": 0,
		"sonarjs/cognitive-complexity": 0,
		"import/no-unassigned-import": [
			2,
			{
				"allow": [
					"**/*.*css"
				]
			}
		],
		"import/order": [
			"error",
			{
				"newlines-between": "always",
				"alphabetize": {
					"order": "asc"
				},
				"groups": [
					"builtin",
					"external",
					"parent",
					"sibling",
					"index"
				]
			}
		]
	},
	"env": {
		"browser": false,
		"es2021": true
	},
	"settings": {
		"react": {
			"pragma": "React",
			"version": "detect"
		}
	},
	"root": true
}
