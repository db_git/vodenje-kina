[![pipeline status](https://gitlab.com/db_git/vodenje-kina/badges/main/pipeline.svg)](https://gitlab.com/db_git/vodenje-kina/-/commits/main)

## Web application for running a cinema - Final thesis

### Project goal

The goal of this final thesis is to enable unregistered and registered users to view information about the movies and to buy a ticket for the selected movie.
The information includes the release year of the movie, duration of the movie, genres, actors and directors.
Also, the user has an overview of the halls in which the film is shown and an overview of occupied seats.
Registered users can book seats, buy drinks, snacks, souvenirs and leave a comment on the movie they watched.
Administrators have the ability to remove, edit and add genres, movies, actors, seats, halls, drinks and snacks.

### Technologies used

- .net core
- ef core
- postgresql
- noda time
- autofac
- automapper
- docker
- react
- typescript
- axios
- mobx
- react-router-dom
- mantine ui
- tailwindcss
- scss
- prettier
- eslint
- vite

### Starting

To start the project, you need to install [Docker](https://www.docker.com/).

After cloning the project, it is necessary to switch to the directory where the `docker-compose.yml` file is located.

Copy the `.env.Example` file to `.env` and edit the settings.

In the same directory, enter the following command in the terminal:
```
docker compose up
```

The project is then available at the URL `http://localhost`.

### Documentation

#### Required tools

A LaTeX text editor, such as [Kile](https://kile.sourceforge.io/), is required to compile the document. _biber_ and _pdflatex_ tools are required instead of editor.

#### Compilation

First you need to generate the necessary files with the _pdflatex_ tool. This can be done with the following command:
```
pdflatex documentation.tex
```

Ignore the errors. Next, you need to translate the references with the _biber_ tool:
```
biber documentation
```

Finally, you need to translate the file again with _pdflatex_ tool at least 2 times to add references. The generated PDF document is located in the same directory.
