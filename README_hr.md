[![pipeline status](https://gitlab.com/db_git/vodenje-kina/badges/main/pipeline.svg)](https://gitlab.com/db_git/vodenje-kina/-/commits/main)

## Web aplikacija za vođenje kina - Završni rad

### Zadatak projekta

Zadatak završnog rada je omogućiti neregistriranim i registriranim korisnicima prikaz informacije o filmovima i rezervaciju ulaznice za odabrani film.
Informacije uključuju prikaz godine izlaska filma, trajanje filma, žanrove, glumce i redatelje.
Također, korisnik ima pregled dvorana u kojima se film prikazuje te pregled zauzetih sjedala.
Registrirani korisnici mogu rezervirati sjedala, kupovati piće, grickalice, suvenire te ostaviti komentar na film koji su pogledali.
Administratori imaju mogućnosti uklanjanja, uređivanja i dodavanja žanrova, filmova, glumaca, sjedala, dvorana te pića i grickalica.

### Korištene tehnologije

- .net core
- ef core
- postgresql
- noda time
- autofac
- automapper
- docker
- react
- typescript
- axios
- mobx
- react-router-dom
- mantine ui
- tailwindcss
- scss
- prettier
- eslint
- vite

### Pokretanje

Za pokretanje projekta potrebno je intalirati [Docker](https://www.docker.com/).

Nakon kloniranja projekta potrebno je prebaciti se u direktorij gdje se nalazi `docker-compose.yml` datoteka.

Kopirajte datoteku `.env.Example` u `.env` i uredite postavke.

U istom direktoriju u terminal je potrebno unijeti slijedeću naredbu:
```
docker compose up
```

Project je tada dostupan na URL-u `http://localhost`.

### Dokumentacija

#### Potrebni alati

Za kompilaciju dokumenta potreban je LaTeX uređivač teksta, poput [Kile](https://kile.sourceforge.io/). Umjesto uređivača potrebni su _biber_ i _pdflatex_ alati.

#### Kompilacija

Prvo je potrebno generirati potrebne datoteke s _pdflatex_ alatom. To se može učiniti sljedećom naredbom:
```
pdflatex documentation.tex
```

Zanemarite nastale greške. Zatim, potrebno je prevesti reference s _biber_ alatom:
```
biber documentation
```

Na kraju, potrebno je ponovno barem dva puta prevesti datoteku s _pdflatex_ alatom kako bi se dodale reference. U istom direktoriju nalazi se generirani PDF dokument.
